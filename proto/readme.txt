이 경로에 있는 프로토 파일을
사용하고자 하는 프로젝트로 복사는 gradle task는
paraphrase/build.gradle에 있는 아래의 코드를 복사한다.

def mltRoot = "${projectDir}/.." ;  // 자신의 프로젝트에 따라 mlt root를 찾는 방법이 다르다.
// 아래는 프로젝트와 무관하게 동일하다.
// 나의 프로토(paraphrase.proto)를 mlt/proto폴더에서 복사한다.
// submodule에서 복사하는 위의 함수와는 경로구조가 달라, 아래와 같이 단순하게 구현하였다.
task copyMyProto(type: Copy) {
    destinationDir = file(protoDest)    // 3. 나의 proto 경로로
    from("${projectDir}/.."+"/proto") {    // 1. mlt/proto폴더에 있는
        includes.add('maum/mlt/paraphrase/*.proto');    // 2. 이 파일을
    }
    includeEmptyDirs = false
}
