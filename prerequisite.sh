#!/usr/bin/env bash

OS=
if [ -f /etc/lsb-release ]; then
  OS=ubuntu
elif [ -f /etc/centos-release ]; then
  OS=centos
elif [ -f /etc/redhat-release ]; then
  OS=centos
else
  echo "Illegal OS, use ubuntu or centos"
  exit 1
fi

function get_requirements() {
  if [ "${OS}" = "centos" ]; then
    sudo yum -y install epel-release
    sudo yum -y groupinstall 'Development Tools'
    sudo yum -y install \
      gcc gcc-c++ \
      java-1.8.0-openjdk-devel.x86_64 \
      python-devel.x86_64 \
      glibc-devel.x86_64 \
      autoconf automake libtool make \
      cmake cmake3 \
      libarchive-devel.x86_64 \
      atlas-devel.x86_64 \
      lapack-devel.x86_64 \
      libcurl-devel.x86_64 \
      openssl-devel.x86_64 \
      policycoreutils-python \
      httpd nginx \
      sox \
      libpng12 libpng12-devel
    # install pip & package
    curl "https://bootstrap.pypa.io/get-pip.py" -o "get-pip.py"
    sudo python get-pip.py
    rm get-pip.py

    # mysql
    #sudo yum -y install http://dev.mysql.com/get/mysql-community-release-el7-5.noarch.rpm
    #sudo yum -y install mysql-community-server
    #sudo systemctl enable mysqld
    #sudo systemctl start mysqld

    # mariaDB install
    ##  maridDB repo 파일 자동 생성
    curl -sS https://downloads.mariadb.com/MariaDB/mariadb_repo_setup | sudo bash
    yum install MariaDB-server
    systemctl start mariadb
    systemctl enable mariadb
    ## 초기 mysql root pass 설정 필요시 사용
    mysql_secure_installation

    # node
    ## web은 maum-admin에서 관리하기 때문에 제거
    # curl --silent --location https://rpm.nodesource.com/setup_8.x | sudo bash -
    #sudo yum install -y nodejs
  else
    sudo apt-get install \
    build-essential \
    g++ g++-4.8 \
    autoconf automake libtool make cmake \
    libssl-dev \
    libarchive-dev \
    libatlas-dev \
    python-pip python-dev \
    libcurl4-openssl-dev \
    libatlas-base-dev \
    libatlas-dev \
    unzip \
    openjdk-8-jdk \
    nginx \
    sox \
    libpng12-dev
    # node
    ## web은 maum-admin에서 관리하기 때문에 제거
    # curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
    # sudo apt install -y nodejs

    # mariaDB install
    sudo apt update
    sudo apt-get install software-properties-common
    sudo apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8
    sudo add-apt-repository "deb [arch=amd64,arm64,ppc64el] http://mariadb.mirror.liquidtelecom.com/repo/10.4/ubuntu $(lsb_release -cs) main"
    sudo apt update
    sudo apt -y install mariadb-server mariadb-client
    sudo systemctl enable mysqld
    sudo systemctl start mysqld

    ## 초기 mysql root pass 설정 필요시 사용
    #sudo mysql_secure_installation
    ## 설치 잘됐는지 확인할때 사용
    sudo systemctl status mysql
    ## 궁금하면 들어가서 보세요
    # https://computingforgeeks.com/how-to-install-mariadb-on-ubuntu/

  fi
  sudo pip install --upgrade pip
  sudo pip install --upgrade virtualenv
  sudo pip install boto3
  sudo pip install grpcio==1.13.0
  sudo pip install requests
  sudo pip install numpy
  sudo pip install theano==0.9.0
  sudo pip install gensim
  sudo pip install workerpool

  sudo npm install -g @angular/cli@1.0.0
}

GLOB_BUILD_DIR=${HOME}/.maum-build
test -d ${GLOB_BUILD_DIR} || mkdir -p ${GLOB_BUILD_DIR}
sha1=$(git log -n 1 --pretty=format:%H ./prerequisite.sh)
echo "Last commit: ${sha1}"

if [ "$1" = "force" ]; then
  get_requirements
  exit 0
fi

if [ -z "${DOCKER_MAUM_BUILD}" ] && [ ! -f ${GLOB_BUILD_DIR}/${sha1}.done ]; then
  get_requirements
  if [ "$?" = "0" ]; then
    touch ${GLOB_BUILD_DIR}/${sha1}.done
  fi
else
  echo "prerequisite had been done!"
fi
