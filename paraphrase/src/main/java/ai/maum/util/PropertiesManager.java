package ai.maum.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class PropertiesManager {

  public static Properties pp() {
    return getProperties();
  }

  private static Properties pp;

  private static Logger logger = LoggerFactory.getLogger(PropertiesManager.class);

  private PropertiesManager() {
  }

  public static void trace() {
    Properties props = pp; // System.getProperties();
    Enumeration e = props.propertyNames();

    while (e.hasMoreElements()) {
      String key = (String) e.nextElement();
      System.out.println(key + " -- " + props.getProperty(key));
    }
  }

  public static Properties getProperties() {
    if (pp == null) {
      pp = new Properties();
      InputStream is = null;
      try {
        String propertiesPath = System.getProperty("propertiesPath");
        if (propertiesPath != null) {
          is = new FileInputStream(propertiesPath);
        } else {
          logger.warn("System Property is Null. PropertyName is 'propertiesPath'");
          propertiesPath = System.getenv("MAUM_ROOT");
          propertiesPath += "/etc/maum-admin-server.conf";
          try {
            is = new FileInputStream(propertiesPath);
          } catch (FileNotFoundException e) {
            logger.warn(String.format("file[%s] not found", propertiesPath));
          } finally {
          }
        }
        pp.load(is);
        {
          if ( is != null ) is.close() ;
          is = PropertiesManager.class.getClassLoader()
              .getResourceAsStream("paraphrase.properties");
          if ( is != null ) pp.load(is);
        }
      } catch (FileNotFoundException e) {
        logger.error("getProperties e : " , e);
      } catch (IOException e) {
        logger.error("getProperties e : " , e);
      } finally {
        try {
          if ( is != null ) is.close();
        } catch (IOException e) {
          logger.error("getProperties e : " , e);
        }
      }
    }
    return pp;
  }

  public static String resolveValueWithProVars(String value) {
    if (null == value) {
      return null;
    }

    Pattern p = Pattern.compile("\\$\\{(\\w+)\\}|\\$(\\w+)");
    Matcher m = p.matcher(value);
    StringBuffer sb = new StringBuffer();
    while (m.find()) {
      String proVarName = null == m.group(1) ? m.group(2) : m.group(1);
      String proVarValue = System.getProperty(proVarName);
      m.appendReplacement(sb,
          null == proVarValue ? "" : Matcher.quoteReplacement(proVarValue));
    }
    m.appendTail(sb);
    return sb.toString();
  }
}
