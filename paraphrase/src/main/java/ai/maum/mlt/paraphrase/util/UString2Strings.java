package ai.maum.mlt.paraphrase.util;

import java.util.HashMap;

// String -> UStringArray
/** 스트링 -> 스트링 배열
 *  this는 length가 큰 순서로 정렬되어 있다.
 *  문장속에서 찾기를 할 때, 길이가 큰 단어를 먼저 찾아야, 작은 단어가 먼저 교체되는 것을 방지할 수 있다.
 */
public class UString2Strings extends UStringArray {
  HashMap<String, UStringArray> s2ss = new HashMap<>() ; // key(string) -> value(string array)
  public void addStr2Strs(String key, UStringArray ss)
  {
    s2ss.put(key,ss) ;
    add(key) ;
  }
  public void trace()
  {
    for ( String s: this )  {
      Util.trace(String.format("[%s]->[%s]", s, s2ss.get(s).cat(",")));
    }
  }
  UStringArray get(String k)  {
    return s2ss.get(k) ;
  }
}
