package ai.maum.mlt.paraphrase;

// import org.apache.commons.lang.StringUtils;

import ai.maum.mlt.paraphrase.util.UHashSetStr;
import ai.maum.mlt.paraphrase.util.UHashSetStrArray;
import ai.maum.mlt.paraphrase.util.UIntArray;
import ai.maum.mlt.paraphrase.util.UIntArrayArray;
import ai.maum.mlt.paraphrase.util.UString2String;
import ai.maum.mlt.paraphrase.util.UStringArray;
import ai.maum.mlt.paraphrase.util.UStringArrayArray;
import ai.maum.mlt.paraphrase.util.Util;
import ai.maum.util.PropertiesManager;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.List;
import java.util.Properties;

import maum.mlt.paraphrase.Paraphrase.*;

/** 당 프로젝트의 확장기능을 구현한 실제 클래스
 */
public class ParaphraseUtil {
  private static boolean bDbLogged  = false  ; // server뜨면서 최초 한번 남길 로그를 위해
  private static Properties pp = PropertiesManager.getProperties();
  private static Logger logger = LoggerFactory.getLogger(ParaphraseUtil.class);
  // logger.info("NLP Start Time: {} ", simpleDateFormat.format(new Date()));
  // logger.info("NLP End Time: {} ", simpleDateFormat.format(new Date()));
  static UHashSetStrArray hssSynm;  // 동의어 사전
  static UHashSetStrArray hssTail;  // 어미변화 사전
  static UHashSetStrArray hssJosa;  // 어미변화 사전
  static UString2String s2sNlpType; // s2sNlpType.init4nlpFromFile("/home/test1/a.txt");
  public float weMoreThan=0.9f ; // 워드 임베딩 결과, 유사도가 이 값 이상이면 동의어로 간주한다.

  /** 사전 정보를 삭제한다. 즉, 사전 정보가 필요하면 db에서 사전 정보를 다시 읽어들인다.
   */
  static public void dictClear()
  {
    hssSynm = null ;
  }

  /** db에서 동의어, 어미, 조사 사전을 초기화
   */
  public void initDict() {
    if ( hssSynm!= null ) return ;
    // hssSynm = new UHashSetStrArray();		hssSynm.init4syn() ;
    // hssTail = new UHashSetStrArray();		hssTail.init4omi() ;
    String dbType = pp.getProperty("mlt.db.type", "mysql");
    // String url = pp.getProperty("mlt.db.url."+dbType);
    // String driver = pp.getProperty("mlt.db.driver-class-name."+dbType);
    // String usr = pp.getProperty("mlt.db.username."+dbType);
    // String pwd = pp.getProperty("mlt.db.password."+dbType);
    String url = pp.getProperty("datasource.primary.url");
    String driver = pp.getProperty("datasource.primary.driver-class-name");
    String usr = pp.getProperty("datasource.primary.username");
    String pwd = pp.getProperty("datasource.primary.password");
    hssSynm = new UHashSetStrArray();
    hssTail = new UHashSetStrArray();
    hssJosa = new UHashSetStrArray();
    UHashSetStrArray hh[] = {hssSynm, hssTail, hssJosa};
    String qq[] = {"synm.", "tail.", "josa."};
    DbUtils.loadDriver(driver);
    Connection conn = null;
    try {
      conn = DriverManager.getConnection(url, usr, pwd);
    } catch (Exception e) {
      logger.error("initDict e : " , e);
      return;
    }
    for (int i = 0; i < hh.length; i++) {
      QueryRunner run = new QueryRunner();
      try {
        ResultSetHandler<List<Pojo4dictionary>> h = new BeanListHandler<Pojo4dictionary>(Pojo4dictionary.class);
        String sql = pp.getProperty("mlt.db." + qq[i] + dbType);
        List<Pojo4dictionary> ee = run.query(conn, sql, h);  // System.out.println(ee);
        for (Pojo4dictionary pj : ee) {
          hh[i].add(UHashSetStr.sCreateByCommaSeperate(pj.getStr()));
        }
      } catch (Exception e) {
        logger.error("initDict e : " , e);
      } finally {
      }
    }
    try {
      DbUtils.close(conn);
    } catch (Exception e) {
    }
    // if ( !bDbLogged )
    {
      bDbLogged = true ;  // db table log는 최초 한번만 남긴다.
      for ( UHashSetStr hs : hssSynm) hs.expandBySpace();
      for ( UHashSetStr hs : hssTail) hs.expandBySpace();
      for ( UHashSetStr hs : hssJosa) hs.expandBySpace();

      //hssSynm.trace("동의어 사전");
      //hssTail.trace("어미 사전");
      //hssJosa.trace("조사 사전");
      hssSynm.init4search();
      hssTail.init4search();
      hssJosa.init4search();
    }
  }

  /** 자신을 초기화한다.
   */
  public void init() {
    String sMoreThan = pp.getProperty("mlt.paraphrase.word_embedding.similarity","0.9");
    weMoreThan = Float.valueOf(sMoreThan) ; // Util.trace(String.format("%.2f",weMoreThan)) ;
    initDict(); // 동의어 사전을 읽어 들이고
    if ( s2sNlpType==null ) {
      s2sNlpType = new UString2String();  // MNG->일반명사, NNP->고유명사....map
      s2sNlpType.init4nlp();  // s2sNlpType.init4nlpFromFile("/home/test1/a.txt");
      // s2sNlpType.trace();
    }
  }

  // 도치를 위한 기능들 /////////////////////////////////////////////////////////////
  /** 원문 s를 도치확장한 문장들을 배열에 담아 리턴한다.
   */
  public UStringArray inverse(String s) {
    UStringArray ss = split4inverse(s); // 원문을 도치를 위한 단위로 자른다.
    // ss.trace("도치를 위해 자른 요소들") ;
    UIntArray ii = new UIntArray();
    ii.setByRange(ss.size()); // 도치 단위 갯수만큼 정수 배열을 생성하고 [0,1,2]
    UIntArrayArray pp = new UIntArrayArray();
    pp.permuteFrom(ii);       // 퍼뮤테이션을 수행 [[0,1,2],[0,2,1],[1,0,2],[1,2,0],[2,0,1],[2,1,0]]
    // pp.trace();
    UStringArray rr = new UStringArray();
    for (UIntArray jj : pp) { // 퍼뮤테이션 각 요소를 인덱스로 구절들을 대입하여 도치문장을 생성한다.
      rr.add(ss.catByIndex(jj));
    }
    return rr;
  }

  /** nlp 기능을 이용해 원문을 도치할 요소들로 잘라낸다.
   */
  public UStringArray split4inverseByNlp(String s) {
    return GrpcNlp.strings4inversion(s, s2sNlpType);
  }

  /** 문장 s를 도치를 위한 요소들로 잘라, 배열을 리턴한다.
   */
  public UStringArray split4inverse(String s) {
    // GrpcNlp.test(s, s2sNlpType);
    UStringArray ss = null;
    String sp = ",,";
    if (s.indexOf(sp) >= 0) {  // 문장속에 [,,]가 포함된 경우, 단순하게 [,,]로 split한 요소들을 도치한다.
      return UStringArray.splitBy(s, sp);
    }
    ss = split4inverseByNlp(s);
    if (ss != null) {
      return ss;
    }

    ss = new UStringArray();  // 여기로 코드수행이 도달하지 않을 것이지만,
    ss.splitBySpace(s);       //  혹시 도달한다면, space를 구분자로 split하여 도치단위로 간주한다.
    return ss;
  }

  // 동의어 확장을 위한 기능들 /////////////////////////////////////////////////////////////
  /** 원문 s를 동의어 확장한 문장들을 배열에 담아 리턴한다.
   */
  public UStringArray synm(String s) {
    UStringArrayArray ss2 = new UStringArrayArray();  // [[s1,s2],[t1,t2,t3],...]
    // UStringArray ss = findAsSynmByNlp(s, ss2);   // nlp를 호출하여, 명사들에 대해 동의어 집합을 구한다.
    UStringArray ss = findAsSynmBySimple(s, ss2);     // 단순히 원문속에서 찾기를 통해, 동의어 집합을 구한다.
    // 위 수행 결과,
    //  원문이 [회사에서 고기를 먹었다]이고, 회사의 동의어가 [직장,업소], 고기의 동의어가 [돼지,닭,소]인 경우
    // ss=[회사,고기], ss2=[[직장,업소], [돼지,닭,소]]
    UStringArrayArray tt2 = ss2.product();  // ss2에 대해 프라덕트를 수행한다.
    // 프라덕트 수행 결과, tt2=[[직장,돼지],[직장,닭],[직장,소],[업소,돼지],[업소,닭],[업소,소]]
    // [s1,s2,s3]*[t1,t2,t3,t4]*[u1,u2,u3,u4,u5]=[[s1,t1,u1],...] : 3*4*5개의 요소 생성
    // tt2.trace();
    return replace(s, ss, tt2); // 프라덕트 각 요소에 대해 replace를 통해 확장된 문장들을 생성한다.
  }

  /** 문장 s에 있는 명사들중 동의어 확장이 가능한 명사들을 ss2에 구한다.
   * 원문이 [회사에서 고기를 먹었다]이고, 회사의 동의어가 [직장,업소], 고기의 동의어가 [돼지,닭,소]인 경우
   * [회사,고기]를 리턴하고, ss2=[[직장,업소], [돼지,닭,소]] 에 담아 넘긴다.
   */
  // [[카드,Card],[집,숙소],...]
  public UStringArray findAsSynmByNlp(String s, UStringArrayArray ss2) {
    UStringArray nn = GrpcNlp.stringsOfMng(s, s2sNlpType);
    if (nn == null || nn.size() == 0) {
      return new UStringArray();  // 빈 배열을 넘긴다.
    }
    // nn.trace("일반명사들:");
    s = nn.cat(",");
    return hssSynm.findInSentence(s,ss2) ;
  }

  /** 문장 s를 단순히 찾기를 통해 동의어 사전에 있는 애들로 동의어를 구한다.
   * 원문이 [회사에서 고기를 먹었다]이고, 회사의 동의어가 [직장,업소], 고기의 동의어가 [돼지,닭,소]인 경우
   * [회사,고기]를 리턴하고, ss2=[[직장,업소], [돼지,닭,소]] 에 담아 넘긴다.
   */
  public UStringArray findAsSynmBySimple(String s, UStringArrayArray ss2) {
    UStringArray ss = hssSynm.findInSentence(s,ss2) ;
    // ss.trace("동의어 확장") ;
    return ss ;
  }

  /* 원문 s에서 ss를 각각 찾아, ss2의 요소 단위로 교체한 문장을 생성한다.
   * s=회사에서 당신과 함께 오늘 고기를 먹었다
   * ss=[회사,고기], ss2=[[직장,업소], [돼지,닭,소]]
   * s에서 [회사,고기]->[직장,돼지], [직장,닭], [직장,소], [업소,돼지], [업소,닭], [업소,소] 로
   * 각각 교체한 6개의 문장을 생성해서 넘긴다.
   * 동의어 확장/워드 임베딩 확장에 대해 호출된다.
   */
  public UStringArray replace(String s, UStringArray ss, UStringArrayArray ss2) {
    UStringArray rr = new UStringArray(); // rr.add(s) ; // 원문을 넣고
    for (UStringArray tt : ss2) {
      String r = s;
      for (int i = 0; i < tt.size(); i++) {
        r = r.replaceAll(ss.get(i), tt.get(i));
      }
      rr.add(r);
    }
    return rr;
  }

  // 워드 임베딩을 위한 기능들 /////////////////////////////////////////////////////////////
  /** 원문 s를 워드 임베딩을 통해 확장한 문장들을 배열에 담아 리턴한다.
   * 동의어를 사전에서 찾지 않고, 워드 임베딩을 찾는 것으로 생각할 수 있다.
   * 메인 로직은 동의어 확장과 동일한다.
   */
  public UStringArray we(String s) {
    UStringArrayArray ss2 = new UStringArrayArray();
    UStringArray ss = findAsWe(s, ss2); //
    UStringArrayArray tt2 = ss2.product(); // tt2.trace();
    return replace(s, ss, tt2);
  }

  public UStringArray findAsWe(String s, UStringArrayArray ss2) {
    UStringArray ss = new UStringArray();
    UStringArray nn = GrpcNlp.stringsOfMng(s, s2sNlpType);
    if (nn == null || nn.size() == 0) {
      return ss;
    }
    // nn.trace("일반명사들:");
    GrpcWordEmbedding we = new GrpcWordEmbedding();
    we.init();
    float than = weMoreThan;  // 워드 임베딩 결과 유사도가 이 값 이상인 애들을 동의어로 간주한다.
    for (int i = 0; i < nn.size(); i++) {
      we.we(nn.get(i));
      UStringArray ww = we.getWeMoreThan(than);
      if (ww != null && ww.size() > 0) {
        ss.add(nn.get(i));
        ss2.add(ww);
      }
    }
    if ( false )  {
      Util.trace("----- 워드 임베딩에 의한 확장");
      for (int i = 0; i < ss.size(); i++) {
        Util.trace(String.format("%s->[%s]", ss.get(i), ss2.get(i).cat(",")));
      }
    }
    return ss;
  }

  // 어미 확장을 위한 기능들 /////////////////////////////////////////////////////////////
  /** 원문 s를 어미확장(어미 동의어 확장) 문장들을 배열에 담아 리턴한다.
   */
  public UStringArray eow(String s) {
    Util.trace("----- 어미 확장");
    return hssTail.replaceBySimpleSearch(s) ;
/*
    UStringArray ss = new UStringArray();
    ss.add(s);  // 원문을 넣고
    for (UHashSetStr hs : hssTail) {  // 문장속에 어미 사전에 있는 단어가 나타나면 단순 replace
      String k = hs.findIn(s);
      if (k == null) {
        continue;
      }
      Util.trace("k:"+k);
      Util.trace(hs) ;
      return hs.replace(s, k, ss);
    }
    return ss;
 */
  }

  public UStringArray paraphrase(ParaphraseType t, String s) {
    s = s.replaceAll("[.]", "");
    if (t.equals(ParaphraseType.PARAPHRASE_INVERSION)) {
      return inverse(s);
    }
    if (t.equals(ParaphraseType.PARAPHRASE_SYNONYM)) {
      return synm(s);
    }
    if (t.equals(ParaphraseType.PARAPHRASE_WE)) {
      return we(s);
    }
    if (t.equals(ParaphraseType.PARAPHRASE_EOW)) {
      return eow(s);
    }
    return new UStringArray();
  }

  public static void main(String[] args) {
    // UStringArray aa=new UStringArray() ; aa.addMulti("단어1", "단어2", "단어3", "단어4") ;
    // UStringArray bb=aa.catBySpaceAll() ; bb.trace() ;
    // if ( true ) return ;
    // logger.debug("debug");
    // logger.info("info");
    // logger.warn("info");
    // if(true)return;
    PropertiesManager.trace();
    ParaphraseUtil se = new ParaphraseUtil();
    ParaphraseUtil.dictClear(); // grpc 호출 단위로 사전 table를 새로 읽어 들인다.
    se.init();  se.weMoreThan = 0.8f ;
    GrpcWordEmbedding.test("날씨",se.weMoreThan);
    GrpcNlp.test("따뜻해 날씨가 무척", se.s2sNlpType);
    // if(true)return;
    UStringArray ss = null;
    ss = se.inverse("나는,,당신이 어제 분실한 카드를,,집에서,,찾았습니다");
    //ss.trace();
    ss = se.inverse("카드를   회사에서   며칠전에 분실했습니다...");
    ss.trace();
    // ss = se.synm("카드를   일하는 곳에서   며칠전에 분실했습니다...");
    ss = se.synm("예금 계좌를 반납하고 환매했습니다...");
    ss.trace();
    // ss = se.we("카드를   회사에서   며칠전에 분실했습니다...");
    ss = se.we("좋다 햇살이 오늘");
    ss.trace();
    // ss = se.eow("카드를   회사에서   며칠전에 분실했죠");
    ss = se.eow("나는 고양이입니다");
    ss.trace();
  }

  // 각종 테스트
  public static void test1() {
    ParaphraseUtil se = new ParaphraseUtil();
    ParaphraseUtil.dictClear(); // grpc 호출 단위로 사전 table를 새로 읽어 들인다.
    se.init();  // se.test1();
    // PropertiesManager.trace();
    // String s9 = System.getenv("MAUM_ROOT");	Util.trace(s9);
    // String s9 = pp.getProperty("brain-ta.nlp.3.kor.port"); // , "9823") ;
    // Util.trace(s9);
    // testSql() ;
    // if(true)return;
    // UHangul.cat9test() ;	if ( true )	return;
    // if ( true )return;
    // UHangul.test(); if ( true )return;
    // GrpcNlp.test("박근혜 대통령이 내가 어제 잡은 고기를 먹었다", se.s2sNlpType);
    {
      UStringArray gg = new UStringArray();
      gg.addMulti("나는 어제 통장분실에 대하여 문의했다.");
      gg.addMulti("아름답게 행동하는 고객은 우리의 자랑입니다");
      gg.addMulti("고객이 바라는 것이 우리가 원하는 것입니다");
/*
			gg.addMulti("나는 문의했다.");
			gg.addMulti("나는 어제 문의했다.");
			gg.addMulti("나는 어제 통장분실을 문의했다.");
			gg.addMulti("나는 갔다");
			gg.addMulti("나는 집에 갔다");
			gg.addMulti("나는 편안한 집에 갔다");
			gg.addMulti("나는 아주 편안한 집에 갔다");
			gg.addMulti("나는 갓잡은 싱싱한 생선을 좋아한다");
			gg.addMulti("우리는 고객을 사랑합니다");
			gg.addMulti("우리는 아름다운 고객을 사랑합니다");
			gg.addMulti("우리는 고객이 아름답게 행동하는 것을 사랑합니다");
			gg.addMulti("우리는 아름답게 행동하는 고객을 사랑합니다");
			gg.addMulti("무엇보다 고객이 우선입니다");
			gg.addMulti("아름다운 우리는 훌륭한 고객을 매우 사랑합니다");
 */
      for (String s : gg) {
        GrpcNlp.test(s, se.s2sNlpType);
      }
      // GrpcNlp.test("나는 당신이 어제 잡은 고기를 오늘 먹었다", se.s2sNlpType);
      // GrpcNlp.test("나는 고기를 오늘 먹었다", se.s2sNlpType);
      // GrpcNlp.test("나는 고기를", se.s2sNlpType);
      // GrpcNlp.test("고기를", se.s2sNlpType);
      // GrpcNlp.test("당신은 어제 고기를 잡았고, 나는 당신이 어제 잡은 고기를 먹었다", se.s2sNlpType);
    }
    if (true) {
      return;
    }
    UStringArray ss = new UStringArray();
    {
      UStringArray uu = new UStringArray();
      uu.add("공부하다,먹다,막다,가다,서다,구하다,부수다,사오다,견디다,보내다");
      uu.add("먹었다,막았다,갔다,섰다,구했다,부쉈다,사왔다,견뎠다,보냈다");
      uu.add("먹어,막아,가,서,구해,부숴,사와,견뎌,보내");
      uu.add("먹어라,막아라,가라,서라,구해라,부숴라,사와라,견뎌라,보내라");
      uu.add("먹으세요,막으세요,가세요,서세요,구하세요,부수세요,사오세요,견드세요,보내세요");
      uu.add("먹을래요,막을래요,갈래요,설래요,구할래요,부술래요,사올래요,견딜래요,보낼래요");
      uu.add("먹으시겠어요,막으시겠어요,가시겠어요,서시겠어요,구하시겠어요,부수시겠어요,사오시겠어요,견디시겠어요,보내시겠어요");
      uu.add("먹으십시오,막으십시오,가십시오,서십시오,구하십시오,부수십시오,사오십시오,견디십시오,보내십시오");
      uu.add("먹어요,막아요,가요,서요,구해요,부숴요,사와요,견뎌요,보내요");
      uu.add("먹네요,막네요,가네요,서네요,구하네요,부수네요,사오네요,견디네요,보내네요");
      uu.add("먹습니다,막습니다,갑니다,섭니다,구합니다,부숩니다,사옵니다,견딥니다,보냅니다");
      uu.add("먹는데요,막는데요,가는데요,서는데요,구하는데요,부수는데요,사오는데요,견디는데요,보내는데요");
      uu.add("먹을래,막을래,갈래,설래,구할래,부술래,사올래,견딜래,보낼래");
      uu.add("먹겠니,막겠니,가니,서니,구하니,부수니,사오니,견디니,보내니");
      uu.add("먹었어,막았어,갔어,섰어,구했어,부쉈어,사왔어,견뎠어,보냈어");
      uu.add("먹었니,막았니,갔니,섰니,구했니,부쉈니,사왔니,견뎠니,보냈니");
      uu.add("먹었냐,막았냐,갔냐,섰냐,구했냐,부쉈냐,사왔냐,견뎠냐,보냈냐");
      uu.add("먹었지,막았지,갔지,섰지,구했지,부쉈지,사왔지,견뎠지,보냈지");
      for (int u = 0; u < uu.size(); u++) {
        Util.trace("**" + uu.get(u) + "**");
        UStringArray tt = new UStringArray();
        tt.addBySplit(uu.get(u), ",");  // if ( true )return;
        for (int i = 0; i < tt.size(); i++) {
          tt.set(i, "나는 빨리 " + tt.get(i));
        }
        for (String s : tt) {
          GrpcNlp.test(s, se.s2sNlpType);
        }
      }
      if (true) {
        return;
      }
    }
    // 받다,모자라다,마치다,믿다,따르다,잘못되다,알다,뭉치다,흐르다,떠돌다,계시다,놓다,쌓다,터지다,말하다,심다,오르다,꾸미다,
    // 끼다,나누다,울다,울리다,살다,말다,태우다,타다,그리다,빼앗다,가지다,좋아하다,만들다,받치다,낫다,내리다,깊다,깨끗하다,아름답다,
    // ss.addBySplit("생각했다,생각했어,생각했니,생각했냐,생각했지",",");
    // ss.addBySplit("공부했다,공부했어,공부했니,공부했냐,공부했지",",");

    ss.addBySplit("먹다,먹었다,먹어,먹어라,먹으세요,먹을래요,먹으시겠어요,먹으십시오,먹어요,먹네요,먹습니다", ",");
    ss.addBySplit("먹는데요,먹을래,먹겠니,먹었어,먹었니,먹었냐,먹었지", ",");  // ,먹어줄래,먹어주겠니
    int n = ss.size();
    ss.addBySplit("막다,막았다,막아,막아라,막으세요,막을래요,막으시겠어요,막으십시오,막아요,막네요,막습니다", ",");
    ss.addBySplit("막는데요,막을래,막겠니,막았어,막았니,막았냐,막았지", ",");  // ,막아줄래,막아주겠니
    ss.addBySplit("가다,갔다,가,가라,가세요,갈래요,가시겠어요,가십시오,가요,가네요,갑니다", ",");
    ss.addBySplit("가는데요,갈래,가니,갔어,갔니,갔냐,갔지", ",");  // ,가줄래,가주겠니
    ss.addBySplit("서다,섰다,서,서라,서세요,설래요,서시겠어요,서십시오,서요,서네요,섭니다", ",");
    ss.addBySplit("서는데요,설래,서니,섰어,섰니,섰냐,섰지", ",");  // ,서줄래,서주겠니
    ss.addBySplit("구하다,구했다,구해,구해라,구하세요,구할래요,구하시겠어요,구하십시오,구해요,구하네요,구합니다", ",");
    ss.addBySplit("구하는데요,구할래,구하니,구했어,구했니,구했냐,구했지", ",");  // ,구해줄래,구해주겠니
    ss.addBySplit("부수다,부쉈다,부숴,부숴라,부수세요,부술래요,부수시겠어요,부수십시오,부숴요,부수네요,부숩니다", ",");
    ss.addBySplit("부수는데요,부술래,부수니,부쉈어,부쉈니,부쉈냐,부쉈지", ",");  // ,부숴줄래,부숴주겠니
    ss.addBySplit("사오다,사왔다,사와,사와라,사오세요,사올래요,사오시겠어요,사오십시오,사와요,사오네요,사옵니다", ",");
    ss.addBySplit("사오는데요,사올래,사오니,사왔어,사왔니,사왔냐,사왔지", ",");  // ,사와줄래,사와주겠니
    ss.addBySplit("견디다,견뎠다,견뎌,견뎌라,견드세요,견딜래요,견디시겠어요,견디십시오,견뎌요,견디네요,견딥니다", ",");
    ss.addBySplit("견디는데요,견딜래,견디니,견뎠어,견뎠니,견뎠냐,견뎠지", ",");  // ,견뎌줄래,견뎌주겠니
    ss.addBySplit("보내다,보냈다,보내,보내라,보내세요,보낼래요,보내시겠어요,보내십시오,보내요,보내네요,보냅니다", ",");
    ss.addBySplit("보내는데요,보낼래,보내니,보냈어,보냈니,보냈냐,보냈지", ",");// ,보내줄래,보내주겠니
    for (int m = 0; m < n; m++) {
      // int m=3 ;
      UStringArray tt = new UStringArray();
      for (int i = m; i < ss.size(); i += n) {
        tt.add(ss.get(i));
      }
      Util.trace(String.format("tt.addBySplit(\"%s\", \"%s\");", tt.cat(","), ","));
      if (true) {
        continue;
      }
      // tt.traceByFormat("tt.add(\"%s\");");
      tt = new UStringArray();
      // tt.addBySplit("먹어라,막아라,가라,서라,구해라,부숴라,사와라,견뎌라,보내라", ",");
      // tt.addBySplit("먹어라,막아라,가라,서라,구해라,부숴라,사와라,견뎌라,보내라", ",");
      for (int i = 0; i < tt.size(); i++) {
        tt.set(i, "나는 빨리 " + tt.get(i));
      }
      // ss.addBySplit("인출하다,인출했다,인출해,인출해라,인출하세요,인출할래요,인출하시겠어요,인출하십시오,인출해요,인출하네요,인출합니다",",");
      // ss.addBySplit("인출하는데요,인출할래,인출하겠니,인출해줄래,인출해주겠니,인출했어,인출했니,인출했냐,인출했지",",");
      // ss.addBySplit("생각하다,생각했다,생각해,생각해라,생각하세요,생각할래요,생각하시겠어요,생각하십시오,생각해요,생각하네요,생각합니다",",");
      // ss.addBySplit("생각하는데요,생각할래,생각하겠니,생각해줄래,생각해주겠니,생각했어,생각했니,생각했냐,생각했지",",");
      // ss.addBySplit("공부하다,공부했다,공부해,공부해라,공부하세요,공부할래요,공부하시겠어요,공부하십시오,공부해요,공부하네요,공부합니다",",");
      // ss.addBySplit("공부하는데요,공부할래,공부하겠니,공부해줄래,공부해주겠니,공부했어,공부했니,공부했냐,공부했지",",");
      for (String s : tt) {
        GrpcNlp.test(s, se.s2sNlpType);
      }
    }
    // GrpcNlp.test("먹었다", se.s2sNlpType);
    // GrpcNlp.test("먹지요", se.s2sNlpType);
    // GrpcNlp.test("먹었나", se.s2sNlpType);
    // GrpcNlp.test("먹었나요", se.s2sNlpType);
    // GrpcNlp.test("먹겠나요", se.s2sNlpType);
    // GrpcNlp.test("먹을걸", se.s2sNlpType);
/*
		// GrpcNlp.test("나는 어제 잡은 고기를 먹었다", se.s2sNlpType);
		// GrpcNlp.test("카드를   회사에서   며칠전에 분실했습니다...", se.s2sNlpType);
		// GrpcNlp.test("박근혜 대통령이 내가 어제 잡은 고기를 먹었다", se.s2sNlpType);
		GrpcWordEmbedding.test("카드", 0.75f);
		GrpcWordEmbedding.test("고양이", 0.75f);
		GrpcWordEmbedding.test("결제", 0.75f);

		Util.trace("end");
		if ( true )return;
*/
  }

}

/*
계좌 잔액 조회해줘
계좌 잔액이 얼마에요
내 잔액 알려줘
내 계좌에 얼마가 있어
통장 잔고 알려줘
잔액이 궁금해
계좌 잔액 체크해줘
계좌에 남은 돈이 얼마야
통장에 얼마 있는지 보여줘
남아 있는 잔액이 얼마있는지 확인해줘
어제 잔액과 비교해줘
잔액을 어제와 비교해봐
잔액이 어제보다 얼마나 늘었어
어제 잔고와 비교해봐
어제 잔액과 얼마나 차이나는지 궁금해
어제 보유 잔액과 비교한 것 체크해줘
어제 잔액과 비교해서 조회해줘
현재 통장 잔액을 어제 잔액과 비교해서 확인해줘
거래내역 알려줘
거래한 내역 확인해줘
계좌 거래 내역 체크해줘
계좌 사용 내역 궁금해
입출금 내역 알려줘
계좌 입출금 내역 확인해줘
최근 입출금 내역 확인해줘
사람 의자 꽃 사
 */
// mlt, pull request, add your review
/*
  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid")
  @Column(name = "id", length = 50)
  private String id;

  @Column(name = "title", length = 40)
  private String title;

  @Column(name = "currentStep", length = 10)
  private String currentStep;

  @Column(name = "allStep", length = 10)
  private String allStep;

  @Column(name = "creatorId", length = 20)
  private String creatorId;

  @Column(name = "createdAt")
  private Date createdAt;

  @Column(name = "workspaceId", nullable = false, length = 10)
  private String workspaceId;
 */
