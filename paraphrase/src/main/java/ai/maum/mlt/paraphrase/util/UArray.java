package ai.maum.mlt.paraphrase.util;

import java.util.ArrayList;

public class UArray<T> extends ArrayList<T> {

  public void trace() {
    for (T v : this) {
      Util.trace(v);
    }
  }

  public void trace(String s) {
    Util.trace("----- " + s);
    if ( size()==0 )  Util.trace("Empty");
    for (T v : this) {
      Util.trace(v);
    }
  }

  public void traceByFormat(String s) {
    for (T v : this) {
      Util.trace(String.format(s, v));
    }
  }

  public UArray() {
    super();
  }

  @SafeVarargs
  public UArray(T... aa) {
    super();
    addMulti(aa);
  }

  /*
    public UArray<UArray<T>> permute()
    {
      UArray<UArray<T>> pp = new UArray<UArray<T>>() ;
      pp.add((UArray<T>)this.clone()) ;
      pp.add((UArray<T>)this.clone()) ;
      return pp ;
    }
   */
  @SuppressWarnings("unchecked")
  public UArray<T> addMulti(T... aa) {
    for (T a : aa) {
      add(a);
    }
    return this;
  }

  public void swap(int i, int j) {
    T t = get(j);
    set(j, get(i));
    set(i, t);
  }
/*	
	public UArray<T> addMulti(T a)				{ add(a); return this; }
	public UArray<T> addMulti(T a, T b)			{ add(a); return this; }
	public UArray<T> addMulti(T a, T b, T c)	{ add(a); return this; }
 */
}
