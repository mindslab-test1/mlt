package ai.maum.mlt.paraphrase.util;

import ai.maum.mlt.paraphrase.ParaphraseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;

public class UStringArray extends UArray<String> {

  private static Logger logger = LoggerFactory.getLogger(ParaphraseUtil.class);

  public boolean equals(String s) {
    // return StringUtils.equalsAny(ss);
    for (int i = 0; i < size(); i++) {
      if (get(i).equalsIgnoreCase(s)) {
        return true;
      }
    }
    return false;
  }

  public String cat(String sp) {
    if (size() == 0) {
      return "";
    }
    if (size() == 1) {
      return get(0);
    }
    String s = get(0);
    for (int i = 1; i < size(); i++) {
      s += sp + get(i);
    }
    return s;
  }

  public String catByIndex(UIntArray ii) {
    if (ii.size() == 0) {
      return "";
    }
    String s = get(ii.get(0));
    for (int i = 1; i < ii.size(); i++) {
      s += " " + get(ii.get(i));
    }
    return s;
  }

  public void addBySplit(String s, String sp) {
    // Util.trace(String.format("s=\"%s\";", s));
    // Util.trace(String.format("uu.add(\"%s\");", s));
    StringTokenizer tk = new StringTokenizer(s, sp);
    for (; tk.hasMoreTokens(); ) {
      add(tk.nextToken());
    }
  }

  public void splitBySpace(String s) {
    // String[] tt = s.split(" ");	for(String t: tt)add(t.trim());
    s = s.replace('.', ' ');
    s = s.replace(',', ' ');
    s = s.trim();
    StringTokenizer tk = new StringTokenizer(s, " ");
    for (; tk.hasMoreTokens(); ) {
      add(tk.nextToken());
    }
  }

  public void loadFromFile(String fn) {
    Scanner sc;
    try {
      sc = new Scanner(new File(fn));
    } catch (FileNotFoundException e) {
      logger.error("loadFromFile e : " , e);
      return;
    }
    ArrayList<String> lines = new ArrayList<String>();
    while (sc.hasNextLine()) {
      add(sc.nextLine());
    }
    // String[] arr = lines.toArray(new String[0]);
  }

  public boolean saveToFile(String fn) {
    PrintWriter o = null;
    boolean r = true;
    try {
      o = new PrintWriter(new FileWriter(fn));
      for (String s : this) {
        o.println(s);
      }
    } catch (Exception e) {
      r = false;
    } finally {
      if (o != null) {
        o.close();
      }
    }
    return r;
  }

  public void findFiles(String dir, String sExt) {
    findFiles(new File(dir), sExt);
  }

  public void findFiles(File file, String sExt) {
    if (file == null || file.isHidden()) {
      return;
    }
    if (file.isDirectory()) {
      // if (!"out".equals(file.getName()))
      {
        File[] files = file.listFiles();
        for (File f : files) {
          findFiles(f, sExt);
        }
      }
      return;
    }
    if (sExt == null || file.getName().endsWith(sExt)) {
      add(file.getPath());
    }
    // if ( sExt==null || file.getName().endsWith(sExt) )	add(file.getName()) ;
    // if (file.getName().endsWith(sExt))	add(file.getAbsolutePath()) ;
		/*
		{
			String path = findPackage(file);
			if (path != null) {
				path = path.replace('.', File.separatorChar);
				createDirs(path);
				copyFile(file.getAbsolutePath(), TargetPath + File.separatorChar + path + File.separatorChar + file.getName());
			}
		}
		 */
  }
  public boolean hasSub(String sub)
  {
    for ( String s: this )  {
      if ( s.indexOf(sub)>=0 ) return true ;
    }
    return false ;
  }

  /*나의 요소들 사이에 space를 넣으면서, 모든 가능한 조합에 대한 단어들을 구해 넘긴다.
   * [대출, 거래, 명세서] - > [대출거래명세서, 대출거래 명세서, 대출 거래 명세서, 대출 거래명세서]
   *
   * @param fn
   * @return
   */
  public UStringArray catBySpaceAll()
  {
    UIntArrayArray ii2 = new UIntArrayArray() ;
    UIntArray ii = new UIntArray() ; ii.addMulti(0,1) ;
    for ( int i=1 ; i<size() ; i++ )  ii2.add(ii) ;
    UIntArrayArray pp2 = ii2.product() ;
    UStringArray ss = new UStringArray() ;
    for ( int i=0 ; i<pp2.size() ; i++ )  {
      String s=get(0) ;
      for ( int j=0 ; j<pp2.get(i).size() ; j++ ) {
        if ( pp2.get(i).get(j)==1 ) s += " " ;
        s += get(j+1) ;
      }
      ss.add(s) ;
    }
    return ss ;
  }

  static public UStringArray sLoadFromFile(String fn) {
    UStringArray ss = new UStringArray();
    ss.loadFromFile(fn);
    return ss;
  }

  // s를 sp를 구분자로 자른 배열을 구한다.
  static public UStringArray splitBy(String s, String sp) {
    UStringArray ss = new UStringArray();
    s = s.trim();
    StringTokenizer tk = new StringTokenizer(s, sp);
    for (; tk.hasMoreTokens(); ) {
      ss.add(tk.nextToken().trim());
    }
    return ss;
  }
}
