package ai.maum.mlt.paraphrase.util;

// 정수 배열을 구현한 클래스
public class UIntArray extends UArray<Integer> {

  public UIntArray() {
    super();
  }

  // 배열에 원소들을 여러개 한꺼번에 추가한다.
  public UIntArray(Integer... aa) {
    super(aa);
  }

  // 0~m(미만)사이의 정수로 배열을 초기화한다.
  public void setByRange(int m) {
    for (int i = 0; i < m; i++) {
      add(i);
    }
  }
}

