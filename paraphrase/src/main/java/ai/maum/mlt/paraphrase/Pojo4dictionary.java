package ai.maum.mlt.paraphrase;

// select str from ....을 담기 위한 클래스
public class Pojo4dictionary {

  private String str;

  public String getStr() {
    return str;
  }

  public void setStr(String str) {
    this.str = str;
  }

  public String toString() {
    return str;
  }

}
