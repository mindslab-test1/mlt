package ai.maum.mlt.paraphrase.util;

import java.util.HashSet;
import java.util.Iterator;

// set(집합)을 나타내는 클래스
//   java 템플릿 클래스에서 상속하여, 유용한 기능들을 구현
// { a1, a2, a3 } : 동의어 셋
public class UHashSetStr extends HashSet<String> {

  // set에 추가하는 함수의 인자를 여러개 받아 들이기 위해 구현
  public UHashSetStr addMulti(String... aa) {
    for (String a : aa) {
      add(a);
    }
    return this;
  }

  // s(문장)속에 set의 원소가 포함되어 있는가? 포함된 원소를 넘긴다.
  public String findIn(String s) {
    Iterator<String> it = iterator();
    while (it.hasNext()) {
      String k = it.next();
      int i = s.indexOf(k);
      if (i < 0) {
        continue;
      }
      return k;
    }
    return null;
  }

  // set에서 sEx를 제외한 원소들을 스트링 배열로 담아 넘긴다.
  public UStringArray toStrings(String sEx) {
    UStringArray ss = new UStringArray();
    Iterator<String> it = iterator();
    while (it.hasNext()) {
      String k = it.next();
      // if (k.equals(sEx)) {  continue; }
      ss.add(k);
    }
    return ss;
  }

  /**대출 거래 명세서->[대출거래명세서, 대출 거래명세서, ....]
   *
   */
  public void expandBySpace()
  {
    UHashSetStr dd = new UHashSetStr() ;
    Iterator<String> it = iterator();
    int i = 0  ;
    while (it.hasNext()) {
      String k = it.next();
      i = k.indexOf(" ") ;
      if ( i<0 ) {
        dd.add(k) ; continue ;
      }
      UStringArray ss = UStringArray.splitBy(k, " ") ;
      UStringArray tt = ss.catBySpaceAll() ;
      for ( String t: tt ) dd.add(t) ;
    }
    this.clear();
    for ( String s: dd) add(s) ;
  }

  //
  public UStringArray replace(String s, String sEx, UStringArray ss) {
    // UStringArray ss = new UStringArray() ;
    Iterator<String> it = iterator();
    while (it.hasNext()) {
      String k = it.next();
      if (k.equals(sEx)) {
        continue;
      }
      ss.add(s.replaceAll(sEx, k));
    }
    return ss;
  }

  // s=동어의1,동의어2,동의어3,...
  public static UHashSetStr sCreateByCommaSeperate(String s) {
    UStringArray ss = UStringArray.splitBy(s, ",");
    UHashSetStr hs = new UHashSetStr();
    ss.forEach(t -> hs.add(t));
    return hs;
  }
}
