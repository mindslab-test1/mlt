package ai.maum.mlt.paraphrase;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import maum.brain.nlp.NaturalLanguageProcessingServiceGrpc;
import maum.brain.nlp.NaturalLanguageProcessingServiceGrpc.NaturalLanguageProcessingServiceBlockingStub;
import maum.brain.nlp.Nlp.*;
import maum.common.LangOuterClass.LangCode;
import ai.maum.mlt.paraphrase.util.UStringArray;
import ai.maum.mlt.paraphrase.util.Util;
import ai.maum.mlt.paraphrase.util.UString2String;
import ai.maum.mlt.paraphrase.util.UTreeNodeInt;
import ai.maum.util.PropertiesManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
// import org.apache.commons.lang.StringUtils;

/** NLP3 GRPC call을 해서 결과를 파싱하는 클래스
 */
public class GrpcNlp {

  ManagedChannel ch = null;
  NaturalLanguageProcessingServiceBlockingStub stub = null;
  Document doc = null;
  NlpAnalysisLevel al = NlpAnalysisLevel.forNumber(0);
  UString2String s2s; // MNG->일반명사, .....

  /** NLP3 분석레벨을 정수로 설정한다.
   */
  public void setAnalysisLevel(int i) {
    al = NlpAnalysisLevel.forNumber(i);
  }

  /** NLP3 분석레벨을 Enum으로 설정한다.
   */
  public void setAnalysisLevel(NlpAnalysisLevel i) {
    al = i;
  }
  /** NLP3 형태소 분석 축어를 의미있는 한글로 맵핑하는 hash map을 초기화한다.
   */
  public void setS2s(UString2String _s2s) {
    s2s = _s2s;
  }

  public void close() {
    if (ch != null) {
      ch.shutdown();
    }
  }

  public void init(String ip, int port) {
    // ip = "125.132.250.244/data2/ETRI_WiseNLU_20180306/source/lang/lang_test/" ;
    ch = ManagedChannelBuilder.forAddress(ip, port).usePlaintext(true).build();
    stub = NaturalLanguageProcessingServiceGrpc.newBlockingStub(ch);
  }

  public void init() {
    String ip = PropertiesManager.pp().getProperty("brain-ta.nlp3kor.ip", "localhost");
    String port = PropertiesManager.pp().getProperty("brain-ta.nlp3kor.port", "9823");
    init(ip, Integer.parseInt(port));
  }

  public void init(Properties pp) {
    String ip = pp.getProperty("NLP_SERVER", "localhost");
    String port = pp.getProperty("NLP_PORT", "9823");
    init(ip, Integer.parseInt(port));
  }

  /** NLP3를 문장 s를 입력으로 호출한다.
   */
  public void analyize(String s) {
    InputText it = InputText.newBuilder().setText(s)
        .setLang(LangCode.kor)
        .setSplitSentence(true)
        .setUseTokenizer(true)
        .setLevel(al)
        // .setUseSpace(StringUtils.equalsIgnoreCase("Y",properties.getProperty("SPACE_YN", "N")))
        .setKeywordFrequencyLevel(KeywordFrequencyLevel.KEYWORD_FREQUENCY_NONE)
        .build();
    doc = stub.analyze(it);
  }

  /** NLP3 호출결과 다양한 스트럭쳐에 대해 디버깅 목적으로 트레이스 출력을 생성한다.
   */
  public void traceSentence() {
    List<Sentence> senn = doc.getSentencesList();
    UStringArray ss = null;
    for (Sentence sen : senn) {
      Util.trace(sen);
      ss = stringsByWord(sen);	if ( ss!=null )	ss.trace("words trace") ;
      // ss = stringsByMorp(sen);	if ( ss!=null )	Util.trace(ss.cat(",")) ;
      // ss = stringsByMorp(sen);	if ( ss!=null )	ss.trace("morps trace") ;
      // ss = stringsByMorphEval(sen);	if ( ss!=null )	ss.trace("morphEvals trace") ;
      // ss = stringsByNes(sen);	if ( ss!=null )	ss.trace("nes trace") ;
      // ss = stringByWsds(sen);	if ( ss!=null )	ss.trace("wsds trace") ;
      ss = stringByDependency(sen);  // if ( ss!=null )	ss.trace("DependencyParser trace") ;
      // ss = stringsBySrls(sen);	if ( ss!=null )	ss.trace("srls trace") ;
      // ss = basicQa(sen);	if ( ss!=null )	ss.trace("basicQA trace") ;
    }
  }

  /** NLP3 호출결과 Words 스트럭쳐에 대해 디버깅 목적으로 트레이스 출력을 생성한다.
   */
  public UStringArray stringsByWord() {
    List<Sentence> senn = doc.getSentencesList();
    if (senn == null || senn.size() == 0) {
      return null;
    }
    return stringsByWord(senn.get(0));
  }
  public UStringArray stringsByWord(Sentence sen) {
    UStringArray ss = new UStringArray();
    List<Word> mm = sen.getWordsList();
    for (int i = 0; i < mm.size(); i++) {
      Word m = mm.get(i);
      ss.add(String.format("%s", m.getText()));
    }
    return ss;
  }
  static public UStringArray stringsByWord(String s, UString2String s2s) {
    Util.trace("NLP Called");
    GrpcNlp nlp = new GrpcNlp();
    nlp.init();
    nlp.setAnalysisLevel(0);
    nlp.setS2s(s2s);
    nlp.analyize(s);
    nlp.close();
    return nlp.stringsByWord();
  }
  /** NLP3 호출결과 MNG(일반명사)에 해당하는 애들만 구한다.
   */
  public UStringArray stringsOfMng() {
    List<Sentence> senn = doc.getSentencesList();
    if (senn == null || senn.size() == 0) {
      return null;
    }
    return stringsOfMng(senn.get(0));
  }
  public UStringArray stringsOfMng(Sentence sen) {
    UStringArray ss = new UStringArray();
    List<Morpheme> mm = sen.getMorpsList();
    for (int i = 0; i < mm.size(); i++) {
      Morpheme m = mm.get(i);
      if ("NNG".equals(m.getType())) {
        ss.add(m.getLemma());
      }
    }
    return ss;
  }
  static public UStringArray stringsOfMng(String s, UString2String s2s) {
    Util.trace("NLP Called");
    GrpcNlp nlp = new GrpcNlp();
    nlp.init();
    nlp.setAnalysisLevel(0);
    nlp.setS2s(s2s);
    nlp.analyize(s);
    nlp.close();
    // nlp.traceSentence();
    return nlp.stringsOfMng();
  }

  /** NLP3 호출결과 Morps 스트럭쳐에 대해 스트링 배열에 담아 넘긴다.
   */
  public UStringArray stringsByMorp(Sentence sen) {
    UStringArray ss = new UStringArray();
    List<Morpheme> mm = sen.getMorpsList();
    UStringArray tt = new UStringArray();
    tt.addMulti("VV", "VA", "VX", "EP", "EF", "EC", "XSV", "XSA");
    for (int i = 0; i < mm.size(); i++) {
      Morpheme m = mm.get(i);
      if (!tt.equals(m.getType())) {
        continue;
      }
      ss.add(String.format("%s:%s(%s)", m.getLemma(), m.getType(), s2s.get(m.getType())));
    }
    return ss;
  }

  /** NLP3 호출결과 MorphEvals 스트럭쳐에 대해 스트링 배열에 담아 넘긴다.
   */
  public UStringArray stringsByMorphEval(Sentence sen) {
    UStringArray ss = new UStringArray();
    List<MorphemeEval> mm = sen.getMorphEvalsList();
    for (int i = 0; i < mm.size(); i++) {
      MorphemeEval m = mm.get(i);
      ss.add(String.format("%s", m.getTarget()));
    }
    return ss;
  }

  /** NLP3 호출결과 Nes 스트럭쳐에 대해 스트링 배열에 담아 넘긴다.
   */
  public UStringArray stringsByNes(Sentence sen) {
    UStringArray ss = new UStringArray();
    List<NamedEntity> mm = sen.getNesList();
    for (int i = 0; i < mm.size(); i++) {
      NamedEntity m = mm.get(i);
      ss.add(String.format("%s:%s:%.2f", m.getText(), m.getType(), m.getWeight()));
    }
    return ss;
  }

  /** NLP3 호출결과 wsds 스트럭쳐에 대해 스트링 배열에 담아 넘긴다.
   */
  public UStringArray stringByWsds(Sentence sen) {
    UStringArray ss = new UStringArray();
    List<WordSenseDisambiguation> mm = sen.getWsdsList();
    for (int i = 0; i < mm.size(); i++) {
      WordSenseDisambiguation m = mm.get(i);
      ss.add(String
          .format("%s:%s(%s):%.2f", m.getText(), m.getType(), s2s.get(m.getType()), m.getWeight()));
    }
    return ss;
  }

  /** NLP3 호출결과 dependency parser 스트럭쳐로 트리를 구성한 후, 도치에 의미있는 단위로 잘라 스트링 배열에 담아 넘긴다.
   */
  public UStringArray stringByDependency(Sentence sen) {
    UStringArray ss = new UStringArray();
    List<DependencyParser> mm = sen.getDependencyParsersList();
    String s;
    for (int i = 0; i < mm.size(); i++) {
      DependencyParser m = mm.get(i);
      s = String
          .format("%d:%d:%s:%s:%.2f", i, m.getHead(), m.getText(), m.getLabel(), m.getWeight());
      s += ":";
      for (int mod : m.getModsList()) {
        s += mm.get(mod).getText() + ",";
      }
      ss.add(s);
    }
    {
      UStringArray tt = new UStringArray();
      UTreeNodeInt tn = treeNodeByDependency(mm, tt);
      Util.trace("----- Dependency Tree");
      tn.trace(0, tt);
      UStringArray dd = tn.catOfRoot(tt);  // dd.trace("도치를 위해 자른 문장성분") ;
      Util.trace(dd.cat("**"));
    }
    return ss;
  }

  /** NLP3 호출결과 dependency parser 스트럭쳐로 트리를 구성한다.
   */
  public UTreeNodeInt treeNodeByDependency(List<DependencyParser> mm, UStringArray ss) {
    ArrayList<UTreeNodeInt> nn = new ArrayList<UTreeNodeInt>();
    for (int i = 0; i < mm.size(); i++) {
      DependencyParser m = mm.get(i);
      nn.add(new UTreeNodeInt(i));
      ss.add(m.getText());
    }
    UTreeNodeInt tnRoot = null;
    for (int i = 0; i < mm.size(); i++) {
      DependencyParser m = mm.get(i);
      int head = m.getHead();
      if (head == -1) {
        tnRoot = nn.get(i);
      } else {
        nn.get(i).setParent(nn.get(head));
      }
    }
    return tnRoot;
  }

  /** NLP3 호출결과 Srls 스트럭쳐 정보를 스트링 배열에 담아 넘긴다.
   */
  public UStringArray stringsBySrls(Sentence sen) {
    UStringArray ss = new UStringArray();
    List<SRL> mm = sen.getSrlsList();
    for (int i = 0; i < mm.size(); i++) {
      SRL m = mm.get(i);
      ss.add(String.format("%s:%.2f", m.getVerb(), m.getWeight()));
    }
    return ss;
  }

  /** NLP3 호출결과 dependency parser를 트리로 구성하고, 1레벨 자식들에 대해 후손들을 합쳐 도치 단위를 생성한다.
   */
  public UStringArray strings4inversion(Sentence sen) {
    UStringArray ss = new UStringArray();
    List<DependencyParser> mm = sen.getDependencyParsersList();
    {
      UStringArray tt = new UStringArray();
      UTreeNodeInt tn = treeNodeByDependency(mm, tt);
      Util.trace("----- Dependency Tree");
      tn.trace(0, tt);
      ss = tn.catOfRoot(tt);  // dd.trace("도치를 위해 자른 문장성분") ;
    }
    return ss;
  }

  public UStringArray strings4inversion() {
    List<Sentence> senn = doc.getSentencesList();
    if (senn == null || senn.size() == 0) {
      return null;
    }
    return strings4inversion(senn.get(0));
  }

  static public UStringArray strings4inversion(String s, UString2String s2s) {
    Util.trace("NLP Called");
    GrpcNlp nlp = new GrpcNlp();
    nlp.init();
    nlp.setAnalysisLevel(0);
    nlp.setS2s(s2s);
    nlp.analyize(s);
    nlp.close();
    return nlp.strings4inversion();
  }

  /** 테스트 목적으로 s를 입력으로 NLP3를 호출하고, 결과를 트레이스한다.
   */
  static public void test(String s, UString2String s2s) {
    GrpcNlp nlp = new GrpcNlp();
    nlp.init();
    nlp.setAnalysisLevel(0);
    nlp.setS2s(s2s);
    nlp.analyize(s);
    nlp.close();
    nlp.traceSentence();
  }
  /** basicQa 모둘에서 nlp3 호출 결과를 해석하는 코드를 참조목적으로 가져왔다.
   */
  public UStringArray basicQa(Sentence sen) {
    String sMorpTypes = ""; // properties.getProperty("MORPH_TYPES", "NNG,NNP,NNB,NP,NR,VV,VA,MM,SL,NF,SN")
    sMorpTypes = "NNG,NNP,NNB,NP,NR,VV,VA,MM,SL,NF,SN"; // properties.getProperty("MORPH_TYPES",
    // String[] ttM = StringUtils.split(sMorpTypes, ",");	s2sNlpType.trace(ttM);
    UStringArray ttM = UStringArray.splitBy(sMorpTypes, ",");  // s2sNlpType.trace(ttM);
    UStringArray ss = new UStringArray();
    List<Morpheme> mm = sen.getMorpsList();
    for (int i = 0; i < mm.size(); i++) {
      Morpheme m = mm.get(i);  // Util.trace(morpheme);	일반명사,고유명사,의존명사,대명사,수사
      if (i > 0 && ((Util.isChe(mm.get(i - 1).getType()) && Util.equals(m.getType(), "XSN"))  // 명사파생접미사
          || (Util.isYong(mm.get(i - 1).getType()) && Util.equals(m.getType(), "ETN")) // 명사형전성 어미
          || (Util.equals(mm.get(i - 1).getType(), "XPN") && Util.isChe(m.getType())))) {  // 체언 접두사
        String t = ss.get(ss.size() - 1);
        if (ss.size() > 0) {
          ss.remove(ss.size() - 1);
        }
        ss.add(t + m.getLemma());
        // if (nn.size() > 0) nn.remove(nn.size() - 1);
        // nn.add(t + m.getLemma());
      } else if (Util.equalsAny(m.getType(), ttM)) {
        ss.add(m.getLemma());
        // if (!equalsAny(m.getType(), "VV", "VA")) nn.add(m.getLemma());
      }
    }
    return ss;
  }
}
