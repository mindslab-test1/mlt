package ai.maum.mlt.paraphrase.util;

import java.util.ArrayList;
import java.util.List;

// tree node
public class UTreeNode<T> {

  public T d; // data
  public UTreeNode<T> p; // parent node
  public List<UTreeNode<T>> cc; // children

  public UTreeNode() {
    p = null;
    cc = null;
  }

  public UTreeNode(T d) {
    this();
    this.d = d;
  }

  public UTreeNode(T d, UTreeNode<T> p) {
    this();
    this.d = d;
    this.p = p;
  }

  public void setParent(UTreeNode<T> p) {
    this.p = p;
    p.addChild(this);
  }

  public void addChild(UTreeNode<T> n) {
    if (cc == null) {
      cc = new ArrayList<UTreeNode<T>>();
    }
    cc.add(n);
  }

  /*
      UTreeNode<T> addChild(T d) {
          UTreeNode<T> n = new UTreeNode<T>(d, this);
          if (cc == null)	cc = new ArrayList<UTreeNode<T>>();
          cc.add(n);
          return n;
      }
   */
  public void trace(int level) {
    String s = "";
    for (int i = 0; i < level; i++) {
      s += "\t";
    }
    Util.trace(s + d);
    if (cc != null) {
      for (UTreeNode<T> n : cc) {
        n.trace(level + 1);
      }
    }
  }
}
