package ai.maum.mlt.paraphrase;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import ai.maum.mlt.paraphrase.util.UStringArray;
import ai.maum.mlt.paraphrase.util.Util;
import java.util.List;
import maum.brain.we.WordEmbeddingServiceGrpc;
import maum.brain.we.WordEmbeddingServiceGrpc.WordEmbeddingServiceBlockingStub;
import maum.brain.we.Wordembedding.WordEmbeddingAnalysisLevel;
import maum.brain.we.Wordembedding.WordEmbeddingDocument;
import maum.brain.we.Wordembedding.WordEmbeddingInputText;
import ai.maum.util.PropertiesManager;

import java.util.Properties;
import maum.brain.we.Wordembedding.WordEmbeddingResult;

/** WordEmbedding GRPC call을 해서 결과를 파싱하는 클래스
 */
public class GrpcWordEmbedding {
  String mMe ; // 워드 임베딩을 요청한 단어
  ManagedChannel ch = null;
  WordEmbeddingServiceBlockingStub stub = null;
  WordEmbeddingDocument doc = null;

  public void close() {
    if (ch != null) {
      ch.shutdown();
    }
  }

  public void init(String ip, int port) {
    ch = ManagedChannelBuilder.forAddress(ip, port).usePlaintext(true).build();
    stub = WordEmbeddingServiceGrpc.newBlockingStub(ch);
  }

  public void init() {
    String ip = PropertiesManager.pp().getProperty("brain-ta.nlp.word_embedding.ip", "localhost");
    String port = PropertiesManager.pp().getProperty("brain-ta.nlp.word_embedding.port", "9822");
    init(ip, Integer.parseInt(port));
  }

  public void init(Properties pp) {
    String ip = pp.getProperty("WE_SERVER", "localhost");
    String port = pp.getProperty("WE_PORT", "9822");
    init(ip, Integer.parseInt(port));
  }

  /** 단어 하나를 입력으로 WordEmbedding 서버를 호출하고, 결과를 doc에 담는다.
   */
  public void we(String s) {
    mMe = s ;
    WordEmbeddingInputText it = WordEmbeddingInputText.newBuilder().setText(s)
        .build();
    doc = stub.getWordEmbedding(it);  // Util.trace(doc);
/*  // 워드 임베딩을 호출하는 파이슨 코드를 참조용으로 복사해 두었다.
    input_text = wordembedding_pb2.WordEmbeddingInputText()
    input_text.text = text
    document = self.stub.GetWordEmbedding(input_text)
    word_embedding_analysis_level = wordembedding_pb2.WordEmbeddingAnalysisLevel.DESCRIPTOR
    al = "" ;  ww = [] ;  similarity_result = []
    if not document: print("document is empty") ;
    else:            al = document.analysis_level ; ww = document.word ; similarity_result = document.similarity

    if al == word_embedding_analysis_level.values_by_name['WORD_EMBEDDING_SINGLE_WORD'].number:
        result = list()
        for i in range(0, len(ww)):
            if similarity_result[i] >= 0.85:
                # print ww[i], ww[i].find('ㄴ')
                if not ww[i].find('ㄴ') > -1: result.append(ww[i])
    return result
 */
  }
  /** 워드 임베딩의 결과를 단어:유사도,....로 출력한다. 디버깅/분석 목정
   */
  public void trace() {
    WordEmbeddingAnalysisLevel al = doc.getAnalysisLevel(); //
    if (al == WordEmbeddingAnalysisLevel.WORD_EMBEDDING_SINGLE_WORD) {
      List<WordEmbeddingResult> wes = doc.getWesList() ;
      for (WordEmbeddingResult we : wes ) {
        Util.trace(String.format("%s:%.2f:%s", we.getWord(), we.getSimilarity(), we.getMorp()));
      }
    }
  }

  /** 워드 임베딩 결과중 유사도가 than이상인 단어들을 스트링 배열에 담아 넘긴다.
   */
  public UStringArray getWeMoreThan(float than) {
    UStringArray ss = new UStringArray();
    ss.add(mMe) ;
    WordEmbeddingAnalysisLevel al = doc.getAnalysisLevel();
    if (al != WordEmbeddingAnalysisLevel.WORD_EMBEDDING_SINGLE_WORD) {
      return ss;
    }
    List<WordEmbeddingResult> wes = doc.getWesList() ;
    for (WordEmbeddingResult we : wes ) {
      if ( we.getSimilarity()<than )continue;
      if ( !"nc".equals(we.getMorp()) )  continue;
      ss.add(we.getWord());
    }
    return ss;
  }

  /** s를 입력으로한 워드 임베딩 결과중 유사도가 than이상인 단어들을 분석목적으로 출력한다.
   */
  static public void test(String s, float than) {
    GrpcWordEmbedding we = new GrpcWordEmbedding();
    we.init();
    we.we(s);
    // we.trace();
    UStringArray ss = we.getWeMoreThan(than);
    if (ss != null) {
      ss.trace();
    }
  }
}

/* // 워드 임베딩 서버에 대해 GRPC 자바 콜을 하는 함수를 참조목적으로 복사해 두었다.
  public maum.brain.we.Wordembedding.WordEmbeddingDocument getWordEmbedding(
      maum.brain.we.Wordembedding.WordEmbeddingInputText request) throws InterruptedException {
    logger.info("==========TaGrpcInterfaceManager.split : " + request);
    try {
      return wordEmbeddingServiceStub.getWordEmbedding(request);
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        e1.printStackTrace();
      } finally {
        this.shutdown();
      }
      return null;
    }
  }
rr = we.word_embedding.similar_by_word('강아지');
 */

