package ai.maum.mlt.paraphrase.util;

@SuppressWarnings("serial")
public class UIntArrayArray extends UArray<UIntArray> {

  // [11,12],[21,22,23]->[11,21],[11,22],[11,23], [12,21],[12,22],[12,23]
  public UIntArrayArray product() {
    int n = 1;
    for (UIntArray v : this) {
      n *= v.size();
    }
    int m = size(), n2 = n, n3 = 0;
    UIntArray[] qq = new UIntArray[n];
    for (int j = 0; j < n; j++) {
      qq[j] = new UIntArray();
    }
    for (int i = 0; i < m; i++) {
      n3 = get(i).size();
      n2 /= n3;
      for (int j = 0; j < n; j++) {
        UIntArray ii = qq[j];
        ii.add(get(i).get((j / n2) % n3));
      }
    }
    UIntArrayArray pp = new UIntArrayArray();
    for (UIntArray ii : qq) {
      pp.add(ii);
    }
    return pp;
  }

  // [1,2,3]->[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1],
  public void permuteFrom(UIntArray ii) {
    permute_(ii, 0);
  }

  private void permute_(UIntArray ii, int i) {
    if (i >= ii.size() - 1) { // If we are at the last element - nothing left toStrings permute
      UIntArray jj = new UIntArray();
      add(jj);
      for (int j = 0; j < ii.size() - 1; j++) {
        jj.add(ii.get(j));
      }
      if (ii.size() > 0) {
        jj.add(ii.get(ii.size() - 1));
      }
      return;
    }
    for (int j = i; j < ii.size(); j++) { // For each index in the sub array arr[i...end]
      ii.swap(i, j);
      permute_(ii, i + 1);
      ii.swap(i, j);
    }
  }
}

/*
 * 
 * 

		// UIntArray ii = new UIntArray(); ii.add(0)
		// UIntArrayArray qq = new UIntArrayArray(); qq.addMulti(new UIntArray(11,12,13), new UIntArray(21,22), new UIntArray(31,32,33,34));
		// UIntArrayArray pp = qq.product(); pp.trace();
		// UStringArray ss = new UStringArray(); 	ss.add(s) ;	return ss ;


public static void permute(int[] arr){
    permuteHelper(arr, 0);
}

private static void permuteHelper(int[] arr, int index){
    if(index >= arr.length - 1){ //If we are at the last element - nothing left toStrings permute
        System.out.print("[");
        for(int i = 0; i < arr.length - 1; i++)	{	System.out.print(arr[i] + ", ");	}
        if(arr.length > 0)	System.out.print(arr[arr.length - 1]);
        System.out.println("]");
        return;
    }
    for(int i = index; i < arr.length; i++){ //For each index in the sub array arr[index...end]
        //Swap the elements at indices index and i
        int t = arr[index]; arr[index] = arr[i]; arr[i] = t;
        //Recurse on the sub array arr[index+1...end]
        permuteHelper(arr, index+1);
        //Swap the elements back
        t = arr[index]; arr[index] = arr[i]; arr[i] = t;
    }
}
 */