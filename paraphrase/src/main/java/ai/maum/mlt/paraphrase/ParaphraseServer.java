package ai.maum.mlt.paraphrase;

import ai.maum.mlt.paraphrase.util.Util;
import com.google.protobuf.ProtocolStringList;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;

import java.io.IOException;
import maum.mlt.paraphrase.Paraphrase.*;
import maum.mlt.paraphrase.ParaphraseServiceGrpc.ParaphraseServiceImplBase;
import ai.maum.mlt.paraphrase.util.UStringArray;
import ai.maum.util.PropertiesManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 당 프로젝트의 GRPC 서버 코드이다. 기능은 paraphrase.proto를 참조한다.
 */
public class ParaphraseServer {

  private static Logger logger = LoggerFactory.getLogger(ParaphraseUtil.class);
  private Server server;

  private void start() throws IOException {
    /* The port on which the server should run */
    String sPort = PropertiesManager.pp().getProperty("mlt.paraphrase.port", "50051");
    int port = Integer.parseInt(sPort);
    server = ServerBuilder
        .forPort(port)
        .maxInboundMessageSize(64 * 1024  *  1024)
        .addService(new ParaphraseImpl()).build().start();
    logger.info("MLT-Paraphrase Server started, listening on " + port);
    Runtime.getRuntime().addShutdownHook(new Thread() {
      @Override
      public void run() {
        // Use stderr here since the logger may have been reset by its JVM shutdown hook.
        System.err.println("*** shutting down gRPC server since JVM is shutting down");
        ParaphraseServer.this.stop();
        System.err.println("*** server shut down");
      }
    });
  }

  private void stop() {
    if (server != null) {
      server.shutdown();
    }
  }

  /**
   * Await termination on the main thread since the grpc library uses daemon threads.
   */
  private void blockUntilShutdown() throws InterruptedException {
    if (server != null) {
      server.awaitTermination();
    }
  }

  /**
   * Main launches the server from the command line.
   */
  public static void main(String[] args) throws IOException, InterruptedException {
    // for ( String s: args )Util.trace(s);
    // Util.trace(args); if(true)return;
    if ( args.length==1 ) {
      if ( "gui".equals(args[0]) ) {
        ParaphraseGui.main(args);
        return ;
      }
      if ( "client".equals(args[0]) ) {
        try {
          ParaphraseClient.main(args);
        }
        catch(Exception e)  {
          logger.error("main e : " , e);
        }
        return ;
      }
      if ( "util".equals(args[0]) ) {
        ParaphraseUtil.main(args);
        return ;
      }
      return ;
    }
    final ParaphraseServer server = new ParaphraseServer();
    server.start();
    server.blockUntilShutdown();
  }

  static class ParaphraseImpl extends ParaphraseServiceImplBase {

    public void getParaphrase(ParaphraseRequest req,
        StreamObserver<ParaphraseResponse> responseObserver) {
      ParaphraseType t = req.getType(); // 확장을 적용할 타입:도치,워드임베딩,동의어 확장,어미확장
      ProtocolStringList oo = req.getOriginsList(); // 확장을 적용한 원문 배열
      ParaphraseResponse.Builder bd = ParaphraseResponse.newBuilder();
      ParaphraseUtil se = new ParaphraseUtil(); // 확장을 구현한 클래스
      ParaphraseUtil.dictClear(); // grpc 호출 단위로 사전 table를 새로 읽어 들인다.
      se.init();
      for (String o : oo) { // 원문 리스트 각각에 대해
        UStringArray pp = se.paraphrase(t, o);  // 확장을 수행하고
        ParaphraseResult res = ParaphraseResult.newBuilder()
            .setOrigin(o) // 원문을 돌려주고
            .addAllParaphrases(pp)  // 확장된 결과를 돌려준다.
            .build();
        bd.addResults(res);
      }
      ParaphraseResponse reply = bd.build();
      responseObserver.onNext(reply);
      responseObserver.onCompleted();
    }
  }
}
