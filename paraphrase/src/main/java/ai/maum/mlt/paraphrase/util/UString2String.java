package ai.maum.mlt.paraphrase.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeMap;

// 다중 thread환경에서 HashTable은 동기화를 지원
// but java5이상부터는 ConcurrentHashMap사용 권고
public class UString2String extends HashMap<String, String> {

  public void init4nlp() {
    put("E", "의존형태");
    put("EC", "연결어미");
    put("EF", "종결어미");
    put("EM", "어미");
    put("EP", "선어말어미");
    put("ETM", "관형형전성어미");
    put("ETN", "명사형전성어미");
    put("I", "독립언");
    put("IC", "감탄사");
    put("J", "관계언");
    put("JC", "접속조사");
    put("JK", "격조사");
    put("JKB", "부사격조사");
    put("JKC", "보격조사");
    put("JKG", "관형격조사");
    put("JKO", "목적격조사");
    put("JKQ", "인용격조사");
    put("JKS", "주격조사");
    put("JKV", "호격조사");
    put("JX", "보조사");
    put("M", "수식언");
    put("MA", "부사");
    put("MAG", "일반부사");
    put("MAJ", "접속부사");
    put("MM", "관형사");
    put("MMA", "성상 관형사");
    put("MMD", "지시 관형사");
    put("MMN", "수 관형사");
    put("N", "체언");
    put("NA", "분석불능범주");
    put("NN", "명사");
    put("NNB", "의존명사");
    put("NNG", "일반명사");
    put("NNP", "고유명사");
    put("NP", "대명사");
    put("NR", "수사");
    put("S", "기호");
    put("SE", "줄임표");
    put("SF", "마침표, 물음표, 느낌표");
    put("SH", "한자");
    put("SL", "외국어");
    put("SN", "숫자");
    put("SO", "붙임표, 물결");
    put("SP", "쉼표, 가운뎃점, 콜론, 빗금");
    put("SS", "따옴표, 괄호표, 줄표");
    put("ST", "일반기호");
    put("SW", "기타 기호");
    put("V", "용언");
    put("VA", "형용사");
    put("VC", "지정사");
    put("VCN", "부정지정사");
    put("VCP", "긍정지정사");
    put("VV", "동사");
    put("VX", "보조용언");
    put("XP", "접두사");
    put("XPN", "체언접두사");
    put("XR", "어근");
    put("XS", "접미사");
    put("XSA", "형용사파생접미사");
    put("XSN", "명사파생접미사");
    put("XSV", "동사파생접미사");

    put("NF", "명사추정범주");
    put("NV", "용언추정범주");
    put("RB", "??");
    put("JJ", "??");
  }

  public void init4nlpFromFile(String fn) {
    UStringArray ss = UStringArray.sLoadFromFile(fn); // ss.trace();
    init4nlp(ss);
  }

  public void init4nlp(UStringArray ss) {
    for (String s : ss) {
      UStringArray tt = UStringArray.splitBy(s, "|");
      if (tt.size() < 2) {
        continue;
      }
      put(tt.get(1), tt.get(0));
    }
  }

  public void trace() {
    TreeMap<String, String> tm = new TreeMap<String, String>(this);
    Iterator<String> it = tm.keySet().iterator(); // 키값 오름차순 정렬(기본)
    // Iterator<String> it = tm.descendingKeySet().iterator(); //키값 내림차순 정렬
    while (it.hasNext()) {
      String k = it.next();
      // Util.trace(k+":"+get(k));
      Util.trace(String.format("**%s**:%s", k, get(k)));
    }
    if (true) {
      return;
    }
    // Util.trace(this);
    Iterator<String> kk = keySet().iterator();
    while (kk.hasNext()) {
      String k = kk.next();
      Util.trace(k + ":" + get(k));
    }
  }

  public void tr3put() {
    TreeMap<String, String> tm = new TreeMap<String, String>(this);
    Iterator<String> it = tm.keySet().iterator(); // 키값 오름차순 정렬(기본)
    // Iterator<String> it = tm.descendingKeySet().iterator(); //키값 내림차순 정렬
    while (it.hasNext()) {
      String k = it.next();
      // Util.trace(k+":"+get(k));
      Util.trace(String.format("put(\"%s\", \"%s\");", k, get(k)));
    }
  }

  public void trace(String[] ttM) {
    String s = "";
    for (String t : ttM) {
      s += String.format("%s(%s)", t, get(t));
    }
    Util.trace(s);
  }

  public void trace(UStringArray ttM) {
    String s = "";
    for (String t : ttM) {
      s += String.format("%s(%s)", t, get(t));
    }
    Util.trace(s);
  }
}

/*
체언(N)
	명사(NN)
		일반명사(NNG)
		고유명사(NNP)
		의존명사(NNB)
	대명사(NP) 
		대명사(NP)
	수사(NR) 
		수사(NR)
용언(V)
	동사(VV) 
		동사(VV)
	형용사(VA) 
		형용사(VA)
	보조용언(VX) 
		보조용언(VX)
	지정사(VC)
		긍정지정사(VCP)
		부정지정사(VCN)
수식언(M)
	관형사(MM)
		성상 관형사(MMA)
		지시 관형사(MMD)
		수 관형사(MMN)
	부사(MA)
		일반부사(MAG)
		접속부사(MAJ)
독립언(I) 
	감탄사(IC) 
		감탄사(IC)
관계언(J)
	격조사(JK)
		주격조사(JKS)
		보격조사(JKC)
		관형격조사(JKG)
		목적격조사(JKO)
		부사격조사(JKB)
		호격조사(JKV)
		인용격조사(JKQ)
	보조사(JX) 
		보조사(JX)
	접속조사(JC) 
		접속조사(JC)
의존형태(E)
	어미(EM)
		선어말어미(EP)
		종결어미(EF)
		연결어미(EC)
		명사형전성어미(ETN)
		관형형전성어미(ETM)
	접두사(XP) 
		체언접두사(XPN)
	접미사(XS)
		명사파생접미사(XSN)
		동사파생접미사(XSV)
		형용사파생접미사(XSA)
	어근(XR) 
		어근(XR)
기호(S)
	일반기호(ST)
		마침표, 물음표, 느낌표(SF)
		쉼표, 가운뎃점, 콜론, 빗금(SP)
		따옴표, 괄호표, 줄표(SS)
		줄임표(SE)
		붙임표(물결)(SO)
		기타 기호(SW)
	외국어(SL) 
		외국어(SL)
	한자(SH)
		한자(SH)
	숫자(SN) 
		숫자(SN)
	분석불능범주(NA) 
		분석불능범주(NA)
*/
/*
체언|N
	명사|NN
		일반명사|NNG
		고유명사|NNP
		의존명사|NNB
	대명사|NP 
		대명사|NP
	수사|NR 
		수사|NR
용언|V
	동사|VV 
		동사|VV
	형용사|VA 
		형용사|VA
	보조용언|VX 
		보조용언|VX
	지정사|VC
		긍정지정사|VCP
		부정지정사|VCN
수식언|M
	관형사|MM
		성상 관형사|MMA
		지시 관형사|MMD
		수 관형사|MMN
	부사|MA
		일반부사|MAG
		접속부사|MAJ
독립언|I 
	감탄사|IC 
		감탄사|IC
관계언|J
	격조사|JK
		주격조사|JKS
		보격조사|JKC
		관형격조사|JKG
		목적격조사|JKO
		부사격조사|JKB
		호격조사|JKV
		인용격조사|JKQ
	보조사|JX 
		보조사|JX
	접속조사|JC 
		접속조사|JC
의존형태|E
	어미|EM
		선어말어미|EP
		종결어미|EF
		연결어미|EC
		명사형전성어미|ETN
		관형형전성어미|ETM
	접두사|XP 
		체언접두사|XPN
	접미사|XS
		명사파생접미사|XSN
		동사파생접미사|XSV
		형용사파생접미사|XSA
	어근|XR 
		어근|XR
기호|S
	일반기호|ST
		마침표, 물음표, 느낌표|SF
		쉼표, 가운뎃점, 콜론, 빗금|SP
		따옴표, 괄호표, 줄표|SS
		줄임표|SE
		붙임표, 물결|SO
		기타 기호|SW
	외국어|SL 
		외국어|SL
	한자|SH
		한자|SH
	숫자|SN 
		숫자|SN
	분석불능범주|NA 
		분석불능범주|NA
 */