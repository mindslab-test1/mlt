package ai.maum.mlt.paraphrase.util;

import java.util.Iterator;

// [{카드,card}, {직장,업소}, ...]
// 동의어 셋에 대한 배열
public class UHashSetStrArray extends UArray<UHashSetStr> {
  UString2Strings mS2ss; // 단어 -> 동의어들

  public void init4syn() {
    init4synm4test();  // 실제로 db에서 사전정보 초기화
  }

  public void init4omi() {
    init4eow4test();  // 실제로 db에서 사전정보 초기화
  }

  // 동어의 사전을 테스트를 위해 초기화한다.
  public void init4synm4test() {
    UHashSetStr hs = null;
    hs = new UHashSetStr();
    add(hs);
    hs.addMulti("직장", "회사", "일하는 곳");
    hs = new UHashSetStr();
    add(hs);
    hs.addMulti("카드", "CARD", "통장");
    hs = new UHashSetStr();
    add(hs);
    hs.addMulti("며칠", "얼마", "조금");
  }

  // 어미 사전을 테스트를 위해 초기화한다.
  public void init4eow4test() {
    UHashSetStr hs = null;
    hs = new UHashSetStr();
    add(hs);
    hs.addMulti("했습니다", "하였습니다", "했죠");
  }
/*	
	// s의 동의어 리스트를 반환
	public UStringArray  find(String s)
	{
	}
 */
  /** 문장 s를 단순히 찾기를 통해 동의어 사전에 있는 애들로 동의어를 구한다.
   * 원문이 [회사에서 고기를 먹었다]이고, 회사의 동의어가 [직장,업소], 고기의 동의어가 [돼지,닭,소]인 경우
   * [회사,고기]를 리턴하고, ss2=[[직장,업소], [돼지,닭,소]] 에 담아 넘긴다.
   */
  public UStringArray findInSentence(String s, UStringArrayArray ss2) {
    if ( mS2ss ==null ) {
      UStringArray ss = new UStringArray();
      UHashSetStrArray hss = new UHashSetStrArray();
      for (UHashSetStr hs : this) {    // 각 동의어 집합에 대해
        String k = hs.findIn(s);  // 문장속에 포함된 단어가 있으면
        if (k == null) {
          continue;
        }
        ss.add(k);
        hss.add(hs);  // 문장속의 단어, 동의어 set
      }
      for (int i = 0; i < hss.size(); i++) {
        ss2.add(hss.get(i).toStrings(ss.get(i)));  // 동의어 set에서 문장속에 포함된 단어를 제외한 배열
      }
      return ss;
    }
    else {
      UStringArray ss = new UStringArray();
      for ( String k : mS2ss) {
        if ( ss.hasSub(k) ) continue;
        if ( s.indexOf(k)>=0 )  {
          ss.add(k) ;
          ss2.add(mS2ss.get(k)) ;
        }
      }
      return ss ;
    }
  }

  /** 문장속에서 동의어를 찾을 때, 길이가 긴 단어를 먼저 찾아야 한다.
   *  예를 들어, {계좌 이체, 계좌 전송, ....}, {계좌, 통장,...}등으로 동의의 셋이 설정된 경우
   *  [계좌 이체를 하였습니다.]라는 문장을 동의어 확장을 할 때,
   *  계좌를 먼저 통장으로 동의어 처리를 해 버리면, 계좌 이체라는 단어를 찾을 수 없어 동의어 확장을 할 수 없다.
   *  따라서, 단어->[동의어들]로 map을 만들고, 단어들을 길이가 긴 단어순으로 정렬을 해 놓고,
   *  이 순서로 동의어 처리를 해야 한다.
   *  이를 위해, 동의어 셋을 위 자료구조로 만드는 작업을 수행한다.
   */
  public void init4search()
  {
    mS2ss = new UString2Strings() ;
    for (UHashSetStr hs : this) {    // 각 동의어 집합에 대해
      Iterator<String> it = hs.iterator();
      while (it.hasNext()) {
        String k = it.next();
        UStringArray ss = hs.toStrings(k) ;
        mS2ss.addStr2Strs(k, ss);
      }
    }
    mS2ss.sort ( (String o1, String o2) -> (o2.length()-o1.length()) ) ;
    mS2ss.trace() ;
  }

  /** 문장속 단어를 단순 교체를 통해 확장한다.
   *
   */
  public UStringArray replaceBySimpleSearch(String s)
  {
    UStringArray ss = new UStringArray();
    ss.add(s);  // 원문을 넣고
    for ( String k : mS2ss) {
      if ( s.indexOf(k)>=0 )  {
        UStringArray tt = mS2ss.get(k) ;
        for ( String t: tt )  ss.add(s.replaceAll(k,t)) ;
        break ;
      }
    }
    return ss ;
  }
}