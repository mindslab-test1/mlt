package ai.maum.mlt.paraphrase.util;

import java.util.HashMap;

// 한글 처리를 위한 클래스 : 초성/중성/종성 분리 및 음절의 받침 유무를 판단하기 위한 기능
public class UHangul {

  // 유니코드 한글 시작 : 44032, 끝 : 55199
  // 초성 [i], 중성 [j], 종성 [k] 일 때,  [ 0xAC00 + (28*21*i) + (28*j) + k ]
  private static final int b0 = 44032;
  private static final int vCho = 588;  // 초성
  private static final int vJun = 28;    // 중성

  // 초성 리스트. 00 ~ 18
  private static final char[] cc1 = {
      'ㄱ', 'ㄲ', 'ㄴ', 'ㄷ', 'ㄸ', 'ㄹ', 'ㅁ', 'ㅂ', 'ㅃ', 'ㅅ', 'ㅆ', 'ㅇ', 'ㅈ', 'ㅉ', 'ㅊ', 'ㅋ', 'ㅌ', 'ㅍ',
      'ㅎ'};
  // 중성 리스트. 00 ~ 20
  private static final char[] cc2 = {
      'ㅏ', 'ㅐ', 'ㅑ', 'ㅒ', 'ㅓ', 'ㅔ', 'ㅕ', 'ㅖ', 'ㅗ', 'ㅘ', 'ㅙ', 'ㅚ', 'ㅛ', 'ㅜ', 'ㅝ', 'ㅞ', 'ㅟ', 'ㅠ', 'ㅡ',
      'ㅢ', 'ㅣ'};
  // 종성 리스트. 00 ~ 27 + 1(1개 없음)
  private static final char[] cc3 = {
      ' ', 'ㄱ', 'ㄲ', 'ㄳ', 'ㄴ', 'ㄵ', 'ㄶ', 'ㄷ', 'ㄹ', 'ㄺ', 'ㄻ', 'ㄼ', 'ㄽ', 'ㄾ', 'ㄿ', 'ㅀ',
      'ㅁ', 'ㅂ', 'ㅄ', 'ㅅ', 'ㅆ', 'ㅇ', 'ㅈ', 'ㅊ', 'ㅋ', 'ㅌ', 'ㅍ', 'ㅎ'};
  public static final HashMap<Character, Integer> cc1i = new HashMap<Character, Integer>();
  public static final HashMap<Character, Integer> cc2i = new HashMap<Character, Integer>();
  public static final HashMap<Character, Integer> cc3i = new HashMap<Character, Integer>();

  static {
    for (int i = 0; i < cc1.length; i++) {
      cc1i.put(cc1[i], i);
    }
    for (int i = 0; i < cc2.length; i++) {
      cc2i.put(cc2[i], i);
    }
    for (int i = 0; i < cc3.length; i++) {
      cc3i.put(cc3[i], i);
    }
  }

  public static void test() {
/*		
		{
			for ( int i=0 ; i< cc1.length ; i++ )	Util.trace(String.format("cc1i.put('%c', %d);",cc1[i],i));
			for ( int i=0 ; i< cc2.length ; i++ )	Util.trace(String.format("cc2i.put('%c', %d);",cc2[i],i));
			for ( int i=0 ; i< cc3.length ; i++ )	Util.trace(String.format("cc3i.put('%c', %d);",cc3[i],i));
		}
 */
    char ch = b0 + (0 * vCho + 2 * vJun); // 한글 유니코드 조합방식
    Util.trace("한글 : " + ch + "  /  유니코드 : " + (int) ch);
    // 한글을 초성, 중성, 종성으로 분리하기.
    String str = "강갂낼홀가하";
    int c0, c1, c2, c3;
    for (int i = 0; i < str.length(); i++) {
      ch = str.charAt(i);
      Util.trace("\n\n한글 : " + ch + "  /  유니코드 : " + (int) ch);
      c0 = ch - b0;  // b0(4403244) 제거
      c1 = c0 / vCho;
      Util.trace("초성 : " + cc1[c1] + "  /  유니코드 : " + (int) c1);  // 초성
      c2 = (c0 - (vCho * c1)) / vJun;
      Util.trace("중성 : " + cc2[c2] + "  /  유니코드 : " + (int) c2); // 중성
      c3 = (c0 - (vCho * c1) - (vJun * c2));
      Util.trace("종성 : " + cc3[c3] + "  /  유니코드 : " + (int) c3);  // 종성
    }
  }

  public static String cat9(String s1, String s2) {
    String t = s1 + s2;
    if (s2.equals("합니다")) {
      String cc[] = new String[]{"습니다", "ㅂ니다"};
      char ch = s1.charAt(s1.length() - 1);
      boolean bJong = hasJong(ch);  // Util.trace(String.format("%c:%s", ch, bJong?"받침있슴":"받침없슴"));
      if (bJong) {
        t = s1 + cc[0];
      } else {
        int c = ch;
        c += cc3i.get(cc[1].charAt(0)); // (cc[1].charAt(0)-b0);
        t = s1.substring(0, s1.length() - 1);
        t += (char) c;
        t += cc[1].substring(1);
      }
      Util.trace(String.format("\"%s\"+\"%s\"=\"%s\"", s1, s2, t));
    }
    return t;
  }

  public static void cat9test() {
    test();
    cat9("가", "합니다");  // 갑니다
    cat9("서", "합니다");  // 섭니다
    cat9("먹", "합니다");  // 먹습니다
    cat9("막", "합니다");  // 막습니다

  }

  public static boolean hasJong(char c) {
    int[] cc = split(c);
    return cc[2] != 0;
  }

  public static int[] split(char c) {
    int c0 = 0, c1, c2, c3;
    c0 = c - b0;  // b0(4403244) 제거
    c1 = c0 / vCho;
    Util.trace("초성 : " + cc1[c1] + "  /  유니코드 : " + (int) c1);  // 초성
    c2 = (c0 - (vCho * c1)) / vJun;
    Util.trace("중성 : " + cc2[c2] + "  /  유니코드 : " + (int) c2); // 종성
    c3 = (c0 - (vCho * c1) - (vJun * c2));
    Util.trace("종성 : " + cc3[c3] + "  /  유니코드 : " + (int) c3);  // 종성
    return new int[]{c1, c2, c3};
  }
}
// http://gnujava.com/board/article_view.jsp?article_no=6605&menu_cd=14&board_no=1&table_cd=EPAR01&table_no=01
/*

초성 [i], 중성 [j], 종성 [k] 일 때,  [ 0xAC00 + (28*21*i) + (28*j) + k ]

유니코드 기반의 한글 조합 방식은 아래와 같다.
 "낼"  =  44032  +  ( (2 * 588)  +  (1 * 28)  +  (8) )
빨간색 : 초성
파란색 : 중성
녹색 : 종성

초성과 중성에 각각 글자에 해당하는 index를 곱한 후 유니코드에서 한글 시작부분(44032)에 더하면 한글이 조합된다.
아래 표는 한글에 해당하는 index 리스트이다.

유니코드에서 한글의 특징은 아래와 같다.
한글 시작 코드 : 44032 (0xAC00)
한글 끝 코드    : 55199 (0xD79F)

초성 코드 : 588
중성 코드 : 28
종성 코드 : 없음. (종성 리스트에서 첫번째 index는 아무 것도 없는 공백을 의미한다.)
 */

/*
http://harryp.tistory.com/596
1. wine 설치
$ sudo apt-get install software-properties-common
$ sudo dpkg --add-architecture i386
$ sudo add-apt-repository ppa:wine/wine-builds
$ sudo apt-get update
$ sudo apt-get install --install-recommends winehq-staging

2. PlayOnLinux 설치
$ su root (아래의 wget 명령어를 실행하기 위해 root로 진입합니다.)
# wget -q "http://deb.playonlinux.com/public.gpg" -O- | apt-key add -
# exit (root에서 빠져나갑니다.)
$ sudo wget http://deb.playonlinux.com/playonlinux_wheezy.list -O /etc/apt/sources.list.d/playonlinux.list
$ sudo apt-get update
$ sudo apt-get install playonlinux

3. PlayOnLinux 실행
상단 설치 클릭
처음에는 설치 가능한 프로그램들을 검색하느라 시간이 조금 걸립니다.
왼쪽 하단에 'Install a non-listed program'을 클릭합니다.
이후 과정 중에서는 특별한 내용이 없다면 캡쳐 화면 처럼 체크하시고 '다음'을 누르시면 됩니다.
Install a program in a new virtual drive(처음이므로)
가상드라이브명 입력(공백들어가면 안된다)
install some libraries'를 체크 합니다.
64비트 환경이라도 32비트로 설치해주어야 오류가 나지 않습니다.
와인에서 필요한 내용 자동 설치

라이브러리를 선택하는 부분에서 다음의 내용들을 선택합니다.
* POL_install_gdiplus
* POL_install_riched20
* POL_install_riched30
* POL_install_wmp9

찾아보기'를 통해 카카오톡 PC 설치파일 (exe 파일) 을 선택 해주고 '다음'을 누릅니다.
설치화면에서 체크박스 모두 해제
*중요* 설치종료후 '카카오톡 실행' 체크 해제 후 '마침'을 누릅니다.

카카오톡 실행을 위한 바로가기 아이콘을 생성하 줍니다.
'KakaoTalk.exe'를 선택 후, 바로가기 아이콘 이름을 입력하면 바탕화면에 바로가기 아이콘이 생성되어 있습니다.
해당 아이콘을 클릭하면 PlayOnLinux를 따로 실행하지 않더라도 프로그램이 실행됩니다.
다른 것들은 필요 없으므로 'I don't want toStrings make another shortcut'을 선택 후 '다음'을 누르면 모든 설치가 종료됩니다

이후 바탕화면의 아이콘으로 카카오톡 실행이 가능합니다.
폰트가 깨져서 보이면
$ sudo apt-get install fonts-nanum*
명령어를 통해 나눔글꼴을 설치해 주시면 됩니다.
 */

/*
우선 사용하고 싶은 마우스 커서 테마를 설치 한다.
sudo apt-get install oxygen-cursor-theme-extra
그리고 다음을 입력한다.
sudo update-alternatives --config x-cursor-theme
 */

/*
    - (a) bug: 기존에 만들어진 기능의 오동작
    - (a) feature : 새로운 기능 요구사항
    - (a) improvement: 기존 기능의 개선
    - (a) todo: 코드의 변경과는 상관없는 각종 개발 외적인 작업들
    - (a) performance: 제품의 재구조화 작업으로 제품의 개발 내부 작업

버그인 경우에는 이 내용을 넣어주세요.
## 문제점 및 증상
- 문제점1
- 증상1

## 재현 과정(Steps toStrings reproduce)
1. step1
1. step2
1. step3

## 해결 후 결과
- [ ] AAA 기능이 정상 동작해야 한다.
- [ ] 화면에는 [ ]이 제대로 표시되어야 한다.
- [ ] AAA 조건에서도 프로그램이 정상적으로 동작해야 한다.

새로운 기능이나 개선일 경우에는 다음 내용을 넣어주세요.
## 완료 조건 (Acceptance Criteria)
- [ ] 사용자가 .... 할 수 있다.
- [ ] 시스템은 ...를 위해서 ...를 해야 한다.


 */