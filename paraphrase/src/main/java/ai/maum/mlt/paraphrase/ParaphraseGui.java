package ai.maum.mlt.paraphrase;

import ai.maum.mlt.paraphrase.util.UStringArray;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

/** 테스트 목적으로 당 프로젝트의 서버에 대해 GRPC 클라이언트 콜을 하는 GUI main을 구현하였다.
 */
// http://www.java2s.com/Code/Java/Swing-JFC/LayingOutComponentsinaFlowLefttoRightToptoBottom.htm
public class ParaphraseGui {

  public static void main(String[] args) {
    Runnable r = new Runnable() {
      JFrame jFr; // main frame
      JTextArea taa[] = {null, null, null, null, null};  // sentence
      JTextArea tdd[] = {null, null, null};  // dictionary

      public void hBtn(String s) {
        // JDialog d = new JDialog(jFr, s, true);d.setLocationRelativeTo(jFr);d.setVisible(true);
        String t = taa[0].getText();  // JOptionPane.showMessageDialog(jFr, t );
        UStringArray oo = UStringArray.splitBy(t, "\n"), tt;
        try {
          tt = ParaphraseClient.paraphrase(0, oo);
          taa[1].setText(tt.cat("\n"));
          tt = ParaphraseClient.paraphrase(1, oo);
          taa[2].setText(tt.cat("\n"));
          tt = ParaphraseClient.paraphrase(2, oo);
          taa[3].setText(tt.cat("\n"));
          tt = ParaphraseClient.paraphrase(3, oo);
          taa[4].setText(tt.cat("\n"));
        } catch (Exception e) {
          taa[1].setText(e.getMessage());
        }
      }

      @Override
      public void run() {
        int scV = JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, scH = JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS;
        // the GUI as seen by the user (without frame)
        JPanel jPn = new JPanel(new BorderLayout());
        jPn.setBorder(new EmptyBorder(2, 3, 2, 3));
        JPanel jPnC;
        // jPn.setLayout(new FlowLayout());
        {
          Container cp = jPn;
          //cp.add(BorderLayout.NORTH, 	new JButton("North"));
          //cp.add(BorderLayout.SOUTH, 	new JButton("South"));
          //cp.add(BorderLayout.EAST, 	new JButton("East"));
          //cp.add(BorderLayout.WEST, 	new JButton("West"));
          jPnC = new JPanel(new BorderLayout());
          jPn.setBorder(new EmptyBorder(2, 3, 2, 3));
          cp.add(BorderLayout.CENTER, jPnC);
        }
        // jPnC.setLayout(new FlowLayout());
        if (1 == 1) {
          Container cp = jPnC;
          if (false) {
            Box bv = Box.createVerticalBox();
            cp.add(BorderLayout.WEST, bv);
            for (int i = 0; i < 5; i++) {
              bv.add(new JButton("bv " + i));
              bv.add(Box.createVerticalStrut(i * 10));
            }
          }
          Box bh = Box.createHorizontalBox();
          cp.add(BorderLayout.NORTH, bh);
          String bb[] = {"도치", "워드 임베딩", "동의어 확장", "어미 확장"};
          for (int i = 0; i < bb.length; i++) {
            JButton btn = new JButton(bb[i]);
            bh.add(btn);
            bh.add(Box.createHorizontalStrut(10));
            btn.addActionListener(new ActionListener() {
              public void actionPerformed(ActionEvent e) {
                JButton btn = (JButton) e.getSource();
                hBtn(btn.getText());
                // display/center the jdialog when the button is pressed
                // JDialog d = new JDialog(frame, "Hello", true);
                // d.setLocationRelativeTo(frame);
                // d.setVisible(true);
              }
            });
          }
        }
        if (1 == 1) {
          JPanel pn = new JPanel(new BorderLayout());
          pn.setBorder(new EmptyBorder(2, 3, 2, 3));
          pn.setLayout(new GridLayout(1, taa.length));
          int w = 40, h = 20;
          Container cp = pn;
          JTextArea jTa;
          for (int i = 0; i < taa.length; i++) {
            jTa = new JTextArea(h, w);
            jTa.setFont(new Font(Font.MONOSPACED, Font.PLAIN, jTa.getFont().getSize()));
            cp.add(new JScrollPane(jTa, scV, scH));
            taa[i] = jTa;
          }
          jPnC.add(pn);
        }
        if (1 == 1) {
          JPanel pn = new JPanel(new BorderLayout());
          pn.setBorder(new EmptyBorder(2, 3, 2, 3));
          pn.setLayout(new GridLayout(1, 4));
          int w = 40, h = 10;
          Container cp = pn;
          JTextArea jTa;
          for (int i = 0; i < tdd.length; i++) {
            jTa = new JTextArea(h, w);
            jTa.setFont(new Font(Font.MONOSPACED, Font.PLAIN, jTa.getFont().getSize()));
            cp.add(new JScrollPane(jTa, scV, scH));
            tdd[i] = jTa;
          }
          jPnC.add(BorderLayout.SOUTH, pn);
        }
        taa[0].setText("나는 고양이입니다.\n카드를   회사에서   며칠전에 분실했습니다.");
        jFr = new JFrame("MLT-Paraphrase GRPC Client");
        jFr.add(jPn);
        jFr.setDefaultCloseOperation(
            JFrame.DISPOSE_ON_CLOSE);  // Ensures JVM closes after frame(s) closed and all non-daemon threads are finished
        jFr.setLocationByPlatform(true);  // See http://stackoverflow.com/a/7143398/418556 for demo.
        jFr.setSize(800, 800);
        jFr.pack();  // ensures the frame is the minimum size it needs toStrings be in order display the components within it
        jFr.setVisible(
            true);  // should be done last, toStrings avoid flickering, moving, resizing artifacts.
      }
    };
    // Swing GUIs should be created and updated on the EDT
    // http://docs.oracle.com/javase/tutorial/uiswing/concurrency
    SwingUtilities.invokeLater(r);
  }
}

