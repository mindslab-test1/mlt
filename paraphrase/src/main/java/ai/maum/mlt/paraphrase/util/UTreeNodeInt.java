package ai.maum.mlt.paraphrase.util;

public class UTreeNodeInt extends UTreeNode<Integer> {

  public UTreeNodeInt() {
    super();
  }

  public UTreeNodeInt(int d) {
    super(d);
  }

  public UTreeNodeInt(int d, UTreeNodeInt p) {
    super(d, p);
  }

  public void trace(int level, UStringArray ss) {
    String s = "";
    for (int i = 0; i < level; i++) {
      s += "\t";
    }
    Util.trace(s + d + ":" + ss.get(d));
    if (cc != null) {
      for (UTreeNode<Integer> n : cc) {
        ((UTreeNodeInt) n).trace(level + 1, ss);
      }
    }
  }

  public void cat(UStringArray dd, UStringArray ss) {
    if (cc != null) {
      for (UTreeNode<Integer> n : cc) {
        ((UTreeNodeInt) n).cat(dd, ss);
      }
    }
    dd.add(ss.get(d));
  }

  public UStringArray catOfRoot(UStringArray ss) {
    UStringArray dd = new UStringArray();
    if (cc != null) {
      for (UTreeNode<Integer> n : cc) {
        UStringArray tt = new UStringArray();
        ((UTreeNodeInt) n).cat(tt, ss);
        dd.add(tt.cat(" "));
      }
    }
    dd.add(ss.get(d));
    return dd;
  }
}
