package ai.maum.mlt.paraphrase;

import ai.maum.mlt.paraphrase.util.UStringArray;
import ai.maum.mlt.paraphrase.util.Util;
import ai.maum.util.PropertiesManager;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import maum.mlt.paraphrase.Paraphrase.*;
import maum.mlt.paraphrase.ParaphraseServiceGrpc;
import maum.mlt.paraphrase.ParaphraseServiceGrpc.ParaphraseServiceBlockingStub;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/** 테스트 목적으로 당 프로젝트의 서버에 대해 GRPC 클라이언트 콜을 하는 main을 구현하였다.
 */
public class ParaphraseClient {
  private static final Logger logger = Logger.getLogger(ParaphraseClient.class.getName());

  private final ManagedChannel channel;
  private final ParaphraseServiceBlockingStub stub;

  public ParaphraseClient(String host, int port) {
    this(ManagedChannelBuilder.forAddress(host, port)
        // Channels are secure by default (via SSL/TLS).
        // For the example we disable TLS toStrings avoid needing certificates.
        .usePlaintext(true)
        .build());
  }

  ParaphraseClient(ManagedChannel channel) {
    this.channel = channel;
    stub = ParaphraseServiceGrpc.newBlockingStub(channel);
  }

  public void shutdown() throws InterruptedException {
    channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
  }

  public List<ParaphraseResult> getParaphrase(ParaphraseType type,
      Iterable<String> values) {
    logger.info("Will try toStrings Paraphrase");
    logger.info(type.toString());
    logger.info(values.toString());
    ParaphraseRequest request = ParaphraseRequest.newBuilder()
        .setType(type)  // ParaphraseType.PARAPHRASE_EOW
        .addAllOrigins(values)
        .build();
    ParaphraseResponse response;
    try {
      response = stub.getParaphrase(request);
    } catch (StatusRuntimeException e) {
      logger.log(Level.WARNING, "RPC failed: {0}", e.getStatus());
      return null;
    }
    return response.getResultsList();
  }

  public static void trace(ParaphraseResult r) {
    Util.trace("-----");
    Util.trace(r.getOrigin());
    Util.trace("-----");
    for (String s : r.getParaphrasesList()) {
      Util.trace(s);
    }
  }

  public static UStringArray paraphrase(int iType, UStringArray ss) throws Exception {
    String ip = PropertiesManager.pp().getProperty("mlt.paraphrase.ip", "localhost");
    String port = PropertiesManager.pp().getProperty("mlt.paraphrase.port", "50051");
    ParaphraseClient client = new ParaphraseClient(ip, Integer.parseInt(port));
    UStringArray tt = new UStringArray();
    try {
      List<ParaphraseResult> rr = client.getParaphrase(ParaphraseType.forNumber(iType), ss);
      for (ParaphraseResult r : rr) {
        for (String s : r.getParaphrasesList()) {
          tt.add(s);
        }
      }
    } finally {
      client.shutdown();
    }
    return tt;
  }

  public static void paraphrase(ParaphraseType t, UStringArray ss, String s) throws Exception {
    Util.trace("----- " + s);
    String ip = PropertiesManager.pp().getProperty("mlt.paraphrase.ip", "localhost");
    String port = PropertiesManager.pp().getProperty("mlt.paraphrase.port", "50051");
    ParaphraseClient client = new ParaphraseClient(ip, Integer.parseInt(port));
    try {
      List<ParaphraseResult> rr = client.getParaphrase(t, ss);
        for (ParaphraseResult r : rr) {
            trace(r);
        }
      // logger.info("Paraphrase: " + rr);
    } finally {
      client.shutdown();
    }
  }

  public static void main(String[] args) throws Exception {
    UStringArray ss = new UStringArray();
    ss.addMulti("나는 고양이입니다.", "나는 당신이 어제 잡은 고기를 먹었다.");
    paraphrase(ParaphraseType.PARAPHRASE_INVERSION, ss, "도치");
    paraphrase(ParaphraseType.PARAPHRASE_SYNONYM, ss, "동의어 확장");
    paraphrase(ParaphraseType.PARAPHRASE_WE, ss, "워드 임베딩");
    paraphrase(ParaphraseType.PARAPHRASE_EOW, ss, "어미 확장");
  }
}
