package ai.maum.mlt.paraphrase.util;

public class UStringArrayArray extends UArray<UStringArray> {

  public UStringArrayArray product() {
    int n = 1;
    for (UStringArray v : this) {
      n *= v.size();
    }
    int m = size(), n2 = n, n3 = 0;
    UStringArray[] qq = new UStringArray[n];
    for (int j = 0; j < n; j++) {
      qq[j] = new UStringArray();
    }
    for (int i = 0; i < m; i++) {
      n3 = get(i).size();
      n2 /= n3;
      for (int j = 0; j < n; j++) {
        UStringArray ii = qq[j];
        ii.add(get(i).get((j / n2) % n3));
      }
    }
    UStringArrayArray pp = new UStringArrayArray();
    for (UStringArray ii : qq) {
      pp.add(ii);
    }
    return pp;
  }
}
