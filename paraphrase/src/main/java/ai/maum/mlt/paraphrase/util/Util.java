package ai.maum.mlt.paraphrase.util;

// import org.apache.commons.lang.StringUtils;

import ai.maum.mlt.paraphrase.ParaphraseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// static 함수들만 포함하고 있는 유틸리티 클래스
public class Util {
  private static Logger logger = LoggerFactory.getLogger(ParaphraseUtil.class);

  public static void trace(Object o) {
    logger.info(o.toString());
    // System.out.println(o);
  }

  public static boolean equals(String s, String t) {
    // return StringUtils.equals(s, t);
    if (s == null) {
      return false;
    }
    return s.equals(t);
  }

  public static boolean equalsAny(String s, String... ss) {
    // return StringUtils.equalsAny(ss);
    for (int i = 0; i < ss.length; i++) {
      if (ss[i].equalsIgnoreCase(s)) {
        return true;
      }
    }
    return false;
  }

  public static boolean equalsAny(String s, UStringArray ss) {
    // return StringUtils.equalsAny(ss);
    for (int i = 0; i < ss.size(); i++) {
      if (ss.get(i).equalsIgnoreCase(s)) {
        return true;
      }
    }
    return false;
  }

  // 체언인가? 일반명사,고유명사,의존명사,대명사,수사
  static public boolean isChe(String t) {
    return Util.equalsAny(t, "NNG", "NNP", "NNB", "NP", "NR");
  }

  // 용언인가? 동사,형용사,보조용언,긍정지정사,부정지정사
  static public boolean isYong(String t) {
    return Util.equalsAny(t, "VV", "VA", "VX", "VCP", "VCN");
  }
  // static public UStringArray newSs()	{	return new UStringArray(); }
}
