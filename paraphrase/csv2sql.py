// 동의어, 어미/조사 동의어 .csv file을 읽어, 초기 동의어 사전에 테이타를 추가하는 파이슨 코드
//  테스트 table을 만드는데 사용하였습니다.
// 테이블 명/필드명은 최신 테이블과 약간 다를 수 있습니다.
def syn():
    ll = list()
    f = open("/home/test1/tmp/data/synm.txt", "r")
    tt = f.readlines()
    for l in tt:
        if l != '\n':    ll.append(l.replace('\n', '').replace('\r', ''))
    f.close()
    for l in ll:
        # print(l);
        vv = l.split(',');
        if ( len(vv)<2 ):
            continue;
        i = 1 ;
        print ( 'INSERT SynonymDicEntity(representation)  VALUES("%s");SELECT @i:=LAST_INSERT_ID();' % vv[0] ) ; 
        while i < len(vv) :
            print ( 'INSERT SynonymDicRelEntity(representationId,synonym)  VALUES(@i,"%s");' % vv[i] ) ; 
            i += 1 ;

def jo():
    ll = list()
    f = open("/home/test1/tmp/data/jo.txt", "r")
    tt = f.readlines()
    for l in tt:
        if l != '\n':    ll.append(l.replace('\n', '').replace('\r', ''))
    f.close()
    for l in ll:
        # print(l);
        vv = l.split(',');
        if ( len(vv)<2 ):
            continue;
        i = 1 ;
        print ( 'INSERT EndingPostPositionDicEntity(representation,type)  VALUES("%s","jo");SELECT @i:=LAST_INSERT_ID();' % vv[0] ) ; 
        while i < len(vv) :
            print ( 'INSERT EndingPostPositionDicRelEntity(representationId,word)  VALUES(@i,"%s");' % vv[i] ) ; 
            i += 1 ;

def tail():
    ll = list()
    f = open("/home/test1/tmp/data/tail.txt", "r")
    tt = f.readlines()
    for l in tt:
        if l != '\n':    ll.append(l.replace('\n', '').replace('\r', ''))
    f.close()
    for l in ll:
        # print(l);
        vv = l.split(',');
        if ( len(vv)<2 ):
            continue;
        i = 1 ;
        print ( 'INSERT EndingPostPositionDicEntity(representation,type)  VALUES("%s","tail");SELECT @i:=LAST_INSERT_ID();' % vv[0] ) ; 
        while i < len(vv) :
            print ( 'INSERT EndingPostPositionDicRelEntity(representationId,word)  VALUES(@i,"%s");' % vv[i] ) ; 
            i += 1 ;
        
syn();
jo();
tail();
