<!---
 MAJOR.MINOR 패치는 # 즉, H1으로 표시
 MAJOR.MINOR.PATCH는 ## 즉, H2로 표시한다.
--->

<a name="2.1.0"></a>
# [2.1.0](https://github.com/mindslab-ai/mlt/compare/v2.0.0...v2.1.0) (2018-12-20)

### Bug Fixes
- *server*: PrimaryKey 값 auto increment 안되는 현상 수정
- *mrc*: MRC > Training 버그수정
- *client*: fileUpload시 동일파일 선택하면 동작안함 문제 수정
- *client*: 배포용 mlt-web이 api 요청을 개발용 url로 하는 에러수정
- *server*: mlt 로그 파일이 maum 루트에 생성되는 현상 수정
- *server*: git path에 git관련 파일없을때 getCommitList호출시 에러 발생 수정
- *mrc*: MRC Training Error 발생시 에러 알림창 안뜨는 문제해결
- *paraphrase*: paraphrase oracle query 에러 수정
- *basicqa*: MLT > bqa item excel upload 버그 해결
- *client*: workspace 변경시 url 에러 수정
- *dictionary*: Dictionary 입력시 null 에러 수정
- *server*: git 초기화 빈값으로 하도록 변경
- *mrc*: MRC TrainingData QA 입력안되는 문제수정
- *ta*: MLT > HMD Analysis 화면 테스트 및 전체 버그 수정
- *server*: db connection 끊기는 현상발생 수정 
- *ta*: MLT > DNN Analysis, NLP Test 화면 버그 수정
- *mrc*: MRC TrainingData Apply 시 에러 발생 수정
- *mrc*: MRC Train 안되는 문제
- *ta*: MLT HMD Analyze, Test시 실패 카운트 0인 버그 해결
- *dictionary*: wiseNlu 사전 정렬 미작동 문제 해결
- *paraphrase*: paraphrase시 10만건 이상일때 grpc 에러 해결
- *mrc*: MRC TrainingData Wiki 등록수정 문제
- *paraphrase*: Paraphrase 화면 로딩 문제 해결
- *basicqa*: Category 연속 2개 수정안되는 문제수정
- *client*: table에 시간표시 정확하게 표시
- *server*: MLT GC OutOfMemoery Error 해결
- *ta*: dnn training 화면 error 발생시 alert 창 띄어주기
- *mrc*: MRC Category 화면 UI 버그 수정
- *basicqa*: BQA Category 화면 UI 버그 수정
- *server*: excel upload file upload 빈 object 읽어 들이는 버그 해결
- *server*: grpc shutdown 에러 수정
- *mrc*: Passage 관리 버그수정 및 기능 추가
- *basicqa*: BasicQA 버그수정 및 기능추가
- *basicqa*: BasicQA Item 등록시 쿼리 버그 수정
- *stt*: stt audio url 수정
- *server*: Microsoft Excel 업로드시 셀서식에따라 값을 못읽는 문제수정
- *client*: web 빌드 오류 수정
- *ta*: TA Nlp Test 페이지 파일다운로드시 내용안보이는문제수정
- *client*: 서버와 client 시간이 다를경우 로그인 안되는 문제수정
- *client*: angular 양방향 바인드시 한글짤림문제
- *ta*: DNN Analyze, Test 안되는 문제 수정
- *ta*: TA FileManagement Selectbox 검색 초기화 안되는 문제수정

### New Features
- *server*: git Controller 구현
- *client*: Git Download 기능 구현
- *ta*: nlp test 화면 구현
- *basicqa*: basicQA Category 조회, 추가, 수정, 삭제 기능 추가
- *ta*: HMD Apply시 git commit Id로 History 남기기
- *basicqa*: BasicQA 관리 화면에서 채널별로 검색할 수 있도록 기능 추가
- *paraphrase*: 동의어 사전관리기능 ui,rest 구현
- *paraphrase*: paraphrase step 화면 download 기능 구현
- *dictionary*: 어미 사전관리기능 ui,rest 구현
- *paraphrase*: 유사질의 확장 서버화
- *dictionary*: NER 태깅(Wise Nlu Ner)UI 개발
- *basicqa*: BasicQA item 에 mainYn추가
- *sso*: sso login api 추가
- *dictionary*: 전처리 사전, 오류 개체명 사전추가
- *client*: MLT 메뉴권한에 따라 화면의 메뉴 다르게 표시
- *ta*; HMD 실행 분석 및 결과 버그 및 RESULT RULE 추가 수정
- *mrc*: MRC TrainingData 화면 개발
- *sso*: MLT sso 연동
- *basicqa*: Indexing 결과에 따른 테스트 화면 구현
- *sso*: sso login 이후 다른 화면 이동 로직 추가
- *dictionary*: Wise NLU NER Dictionary 문장 자동 형태소 분석 UI 도구 개발
- *mrc*: MRC passage 관리기능 구현
- *mrc*: MRC Passage 관리화면 질문검색추가
- *mrc*: MRC Passage 색인 기능 추가
- *paraphrase*: 동의어, 어미조사 사전 전체삭제 기능추가
- *mrc*: PassageSearch 테스트화면 구현
- *mrc*: Passage 등록시 특정문자 변경
- *basicqa*: Indexing 시 History에 엔진 ip,port 저장
- *mrc*: Indexing 시 History에 엔진 ip,port 저장
- *ta*: TA HMD Apply 시 setModel 함수를 SetMatrix로 변경
- *client*: select box 안에 검색으로 filter할수있도록 변경
- *ci*: mlt CI 환경 연동
- *stt*: STT 언어학습 Transcription 페이지 구현
- *stt*: STT AM, LM 학습기능 구현
 

### Enhancements
- *ta*: DNN, HMD Category 안에 sentence , rule 개수 표시
- *ta*: Client HmdDictionary Entity 중복되는 값 제거
- *ta*: DNN, HMD CSV -> TSV 파일 업로드로 변경
- *server*: Table 명 앞에 `MLT` 추가 및 스네이크 케이스로 변경
- *basicqa*: BasicQA Excel upload, download에 category 추가
- *server*: entity 정리
- *ta*: NLP Test 화면 langCode에 따라 nlp 선택 조건 초기화
- *client*: tableComponent 에서 데이터 삭제 후 리스트 조회해오기
- *client*: Angular HttpModule 을 HttpClientModule로 변경
- *dashboard*: Dashboard Route 추가, Route 옵션에 hasNoSidebar추가, api_url import 경로변경
- *ta*: MLT > dictionary, hmd, dnn 기능 개선
- *build*: 빌드환경 일부 정비
- *basicqa*: MLT > build.sh 에 basic-qa proto copy 로직 추가
- *basicqa*: BasicQA IndexingStatus 표시변경
- *server*: mlt logback.xml error 레벨로 변경
- *mrc*: MRC dev.json, train.json 데이터 다르게 설정
- *mrc*: MRC UI 수정
- *client*: MLT material icon assets폴더에서 가져오도록 수정
- *dashboard*: MLT Dashboard scorll 추가 및 폐이징 처리
- *management*: Management user edit 시 add로 보이는 버그, role edit시 alert창 추가
- *mrc*: MRC Context 등록시 Season 값도 받도록 변경
- *server*: 파일 생성위치 upload에서 run으로 변경
- *mrc*: MRC Context 문자수 제한
- *server*: grpc 1.13.1 update
- *server*: grpc 통신시 max size 증가
- *mrc*: MRC TrainingData Edit시 Wiki값 표시 필요
- *mrc*: mrc traning data add시 wiki표시
- *paraphrase*: paraphrase Synonym Dictionary 검색시 검색조건 세분화
- *basicqa*: BQA Items, Paraphrase 화면 검색 기능 세분화
- *mrc*: Datagroup delete시 첫번째 datagroup 선택되도록 변경
- *ta*: 등록한 모델 순서대로 화면에 보여주기
- *server*: Grpc 통신시 명시적으로 shutdown 하도록 변경
- *mrc*: Passage Nlp분석 레벨 변경
- *mrc*: Passage NLP 분석요청시 끊어서 보내도록 변경
- *mrc*: Passage Morph 태그정보 소문자로 저장하도록 수정 
- *mrc*: Mrc TrainingData 관리화면 개선
- *mrc*: MRC TrainingData Excel upload관련 mlt.conf 추가
- *mrc*: Mrc TrainingData Datagroup 버그수정 및 검색조건추가
- *server*: JPA DB generationType Sequence로 변경
- *basicqa*: BasicQA Category 등록,수정시 trim적용
- *mrc*: MRC Category 등록,수정시 trim적용
- *mrc*: Passage NLP Analyze 진행 상태를 socket으로 받도록 구현 
- *mrc*: MLT MRC TrainingData 관리 화면개선
- *ta*: TA HMD Apply시 setModel도 호출하도록 변경
- *build*: npm install시 package-lock.json 삭제후 install 하도록 변경
- *config*: logback.xml 외부에서 설정하도록 변경


### Breaking Changes
- *build*: mlt-shared 파일 npm install 시 생성해서 사용하도록 변경
- *client*: angular cli6 으로 변경 및 ng-packagr 추가