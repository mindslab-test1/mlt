package ai.maum.m2u.common.portable;

import com.hazelcast.nio.serialization.Portable;
import com.hazelcast.nio.serialization.PortableReader;
import com.hazelcast.nio.serialization.PortableWriter;
import java.io.IOException;
import maum.m2u.console.DaInstance.DialogAgentInstanceExecutionInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
message DialogAgentInstanceExecutionInfo {
  string dam_name = 1;
  string ip = 2;
  int32 port = 3;
  bool active = 4;
  int32 cnt = 5;

  repeated string key = 6;
}
 */
public class DialogAgentInstanceExecutionInfoPortable implements Portable {

  static final Logger logger = LoggerFactory
      .getLogger(DialogAgentInstanceExecutionInfoPortable.class);

  private DialogAgentInstanceExecutionInfo protobufObj;

  public DialogAgentInstanceExecutionInfoPortable() {
    protobufObj = DialogAgentInstanceExecutionInfo.newBuilder().build();
  }

  public DialogAgentInstanceExecutionInfo getProtobufObj() {
    return protobufObj;
  }

  public void setProtobufObj(DialogAgentInstanceExecutionInfo protobufObj) {
    this.protobufObj = protobufObj;
  }

  @Override
  public int getFactoryId() {
    return PortableClassId.FACTORY_ID;
  }

  @Override
  public int getClassId() {
    return PortableClassId.DIALOG_AGENT_INSTANCE_EXECUTION_INFO;
  }

  @Override
  public void writePortable(PortableWriter writer) throws IOException {
    if (this.getProtobufObj().getDaiId() != null) {
      writer.writeUTF("dai_id", this.getProtobufObj().getDaiId());
    }
    if (this.getProtobufObj() != null) {
      writer.writeByteArray("_msg", this.getProtobufObj().toByteArray());
    }
    if (this.getProtobufObj().getDamName() != null) {
      writer.writeUTF("dam_name", this.getProtobufObj().getDamName());
    }
    writer.writeBoolean("active", this.getProtobufObj().getActive());
    writer.writeInt("cnt", this.getProtobufObj().getCnt());

    try {
      logger.trace("writePortable : {}", this.toString());
    } catch (Exception e) {
      logger.error("writePortable e : " , e);
    }
  }

  @Override
  public void readPortable(PortableReader reader) throws IOException {
    this.setProtobufObj(DialogAgentInstanceExecutionInfo.parseFrom(reader.readByteArray("_msg")));

    try {
      logger.trace("readPortable : {}", this.toString());
    } catch (Exception e) {
      logger.error("readPortable e : " , e);
    }
  }

  @Override
  public String toString() {
    return String
        .format("[%s] [%s]", this.getProtobufObj().getClass().getSimpleName(),
            this.getProtobufObj().toString());
  }

}
