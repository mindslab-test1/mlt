package ai.maum.m2u.common.portable;

import com.hazelcast.nio.serialization.Portable;
import com.hazelcast.nio.serialization.PortableReader;
import com.hazelcast.nio.serialization.PortableWriter;
import maum.m2u.router.v3.Intentfinder.ClassificationRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;

/*
maum.m2u.router.v3
// 의도파악 기록 저장 객체
message ClassificationRecord {
  // 세션 ID
  int64 session_id = 1;
  // x-operation-sync-id
  string operation_sync_id = 2;
  // 의도 파악 정책의 이름
  string policy_name = 3;
  // 호출하는 챗봇의 이름
  string chatbot = 4;
  // 의도파악하기 위한 발화
  string utter = 5;

  oneof classified {
    // 식별된 스킬 및 intent의 목록
    FoundIntent skill = 11;
    // 다른 챗봇으로 전이하는 명령어의 구조
    ChatbotTransition transit = 12;
  }

  // 의도파악 과정에서 대체한 문장에 대한 기록
  Replacement replace = 21;

  // 의도파악를 수행한 시간
  google.protobuf.Timestamp classified_at = 100;
}
*/

@Service
public class ClassificationRecordPortable implements Portable {

  static final Logger logger = LoggerFactory.getLogger(ClassificationRecordPortable.class);

  private ClassificationRecord protobufObj;

  public ClassificationRecordPortable() {
    protobufObj = ClassificationRecord.newBuilder().build();
  }

  public ClassificationRecord getProtobufObj() {
    return protobufObj;
  }

  public void setProtobufObj(ClassificationRecord protobufObj) {
    this.protobufObj = protobufObj;
  }

  @Override
  public int getFactoryId() {
    return PortableClassId.FACTORY_ID;
  }

  @Override
  public int getClassId() {
    return PortableClassId.CLASSIFICATION_RECORD;
  }

  @Override
  public void writePortable(PortableWriter writer) throws IOException {
    if (this.getProtobufObj() != null) {
      writer.writeByteArray("_msg", this.getProtobufObj().toByteArray());
    }
    writer.writeLong("session_id", this.getProtobufObj().getSessionId());
    if (this.getProtobufObj().getOperationSyncId() != null) {
      writer.writeUTF("operation_sync_id", this.getProtobufObj().getOperationSyncId());
    }
    if (this.getProtobufObj().getPolicyName() != null) {
      writer.writeUTF("policy_name", this.getProtobufObj().getPolicyName());
    }
    if (this.getProtobufObj().getChatbot() != null) {
      writer.writeUTF("chatbot", this.getProtobufObj().getChatbot());
    }

    try {
      logger.trace("writePortable : {}", this.toString());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public void readPortable(PortableReader reader) throws IOException {
    this.setProtobufObj(ClassificationRecord.parseFrom(reader.readByteArray("_msg")));

    try {
      logger.trace("readPortable : {}", this.toString());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public String toString() {
    return String
        .format("[%s] [%s]", this.getProtobufObj().getClass().getSimpleName(),
            this.getProtobufObj().toString());
  }

}
