package ai.maum.m2u.common.portable;

import com.hazelcast.nio.serialization.Portable;
import com.hazelcast.nio.serialization.PortableReader;
import com.hazelcast.nio.serialization.PortableWriter;
import maum.m2u.console.ChatbotOuterClass.Chatbot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;

/*
message Chatbot {
  string name = 1;
  string title = 2;
  string description = 3;
  MediaType input = 4;
  MediaType output = 5;
  bool is_active = 6;
  string auth_provider = 7;
  string intent_finder = 8;

  ChatbotDetail detail = 200;
}
 */

@Service
public class ChatbotPortable implements Portable {

  static final Logger logger = LoggerFactory.getLogger(ChatbotPortable.class);

  private Chatbot protobufObj;

  public ChatbotPortable() {
    protobufObj = Chatbot.newBuilder().build();
  }

  public Chatbot getProtobufObj() {
    return protobufObj;
  }

  public void setProtobufObj(Chatbot protobufObj) {
    this.protobufObj = protobufObj;
  }

  @Override
  public int getFactoryId() {return PortableClassId.FACTORY_ID;}

  @Override
  public int getClassId() {
    return PortableClassId.CHATBOT;
  }


  @Override
  public void writePortable(PortableWriter writer) throws IOException {
    if (this.getProtobufObj().getName() != null) {
      writer.writeUTF("name", this.getProtobufObj().getName());
    }
    if (this.getProtobufObj() != null) {
      writer.writeByteArray("_msg", this.getProtobufObj().toByteArray());
    }
    if (this.getProtobufObj().getTitle() != null) {
      writer.writeUTF("title", this.getProtobufObj().getTitle());
    }
    if (this.getProtobufObj().getDescription() != null) {
      writer.writeUTF("description", this.getProtobufObj().getDescription());
    }
    if (this.getProtobufObj().getInput() != null) {
      writer.writeInt("input", this.getProtobufObj().getInput().getNumber());
    }
    if (this.getProtobufObj().getOutput() != null) {
      writer.writeInt("output", this.getProtobufObj().getOutput().getNumber());
    }
    writer.writeBoolean("active", this.getProtobufObj().getActive());
    if (this.getProtobufObj().getAuthProvider() != null) {
      writer.writeUTF("auth_provider", this.getProtobufObj().getAuthProvider());
    }
    if (this.getProtobufObj().getIntentFinderPolicy() != null) {
      writer.writeUTF("intent_finder_policy", this.getProtobufObj().getIntentFinderPolicy());
    }

    try {
      logger.trace("writePortable : {}", this.toString());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public void readPortable(PortableReader reader) throws IOException {
    this.setProtobufObj(Chatbot.parseFrom(reader.readByteArray("_msg")));

    try {
      logger.trace("readPortable : {}", this.toString());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public String toString() {
    return String
        .format("[%s] [%s]", this.getProtobufObj().getClass().getSimpleName(),
            this.getProtobufObj().toString());
  }

}
