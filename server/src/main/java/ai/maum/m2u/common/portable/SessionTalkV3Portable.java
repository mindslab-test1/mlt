package ai.maum.m2u.common.portable;

import com.hazelcast.nio.serialization.Portable;
import com.hazelcast.nio.serialization.PortableReader;
import com.hazelcast.nio.serialization.PortableWriter;
import maum.m2u.router.v3.Session.SessionTalk;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;


/*
// 한턴의 대화에 대한 상세 기록
message SessionTalk {
    // 순서
    uint32 seq = 1;
    // DialogSession.id
    int64 session_id = 2;
    // 언어
    maum.common.Lang lang = 3;

    // 스킬
    string skill = 11;
    // 현재의 intent
    string intent = 12;
    // 현재 스킬의 마지막 대화인가?
    bool close_skill = 13;

    // 입력
    // 입력 텍스트
    string in = 21;
    // 입력 유형
    maum.m2u.common.Utter.InputType input_type = 22;
    // 입력 메타, IMAGE와 같은 데이터일 경우에 필요함.
    google.protobuf.Struct input_meta = 23;

    // 출력

    // 출력 텍스트
    string out = 31;
    // TTS로 나가는 내용이 다를 때
    string speech_out = 32;
    // 재질의 상태인 경우
    bool reprompt = 33;

    // 출력 메타 데이터
    map<string, string> output_meta = 41;

    // 출력 context 업데이트
    google.protobuf.Struct update_context = 42;

    // grpc 최종 status
    int32 status_code = 51;
    // 내부 처리 코드
    DialogResult dialog_result = 52;

    // 텍스트 분석, 이건 정말 들어갈 것은 아닌 것 같군요. 제거
    // 복수의 대화 에이전트 정보
    repeated TalkAgent agents = 61;

    // 시작 시간
    google.protobuf.Timestamp start = 71;
    // 끝 시간
    google.protobuf.Timestamp end = 72;
    // 각 구간별 시간
    map<string, google.protobuf.Duration> elapsed_time = 73;

    // *optional* 사용자 피드백
    // 사용자가 매기는 점수, 5점척도 또는 10점 척도, 2점 척도 등 다양하게 정의할 수 있다.
    // 매기지지 않으면 -1 이다.
    // 통계에서 뺀다.
    int32 feedback = 91;

    // *optional* 대화 정확도
    // 관리자가 큐레이션 과정에서 매기는 점수이다.
    int32 accuracy = 92;

    // *optional* 다른 챗봇으로 전달을 위한 챗봇 이름
    // FIXME: 다른 챗봇으로 이전하는 경우에만 사용
    string transit_chatbot = 101;
}
*/

@Service
public class SessionTalkV3Portable implements Portable {

  static final Logger logger = LoggerFactory.getLogger(SessionTalkV3Portable.class);

  private SessionTalk protobufObj;

  public SessionTalkV3Portable() {
    protobufObj = SessionTalk.newBuilder().build();
  }

  public SessionTalk getProtobufObj() {
    return protobufObj;
  }

  public void setProtobufObj(SessionTalk protobufObj) {
    this.protobufObj = protobufObj;
  }

  @Override
  public int getFactoryId() {
    return PortableClassId.FACTORY_ID;
  }

  @Override
  public int getClassId() {
    return PortableClassId.SESSION_TALK_V3;
  }

  @Override
  public void writePortable(PortableWriter writer) throws IOException {
    writer.writeLong("seq", this.getProtobufObj().getSeq());
    writer.writeLong("session_id", this.getProtobufObj().getSessionId());
    if (this.getProtobufObj() != null) {
      writer.writeByteArray("_msg", this.getProtobufObj().toByteArray());
    }
    if (this.getProtobufObj().getLang() != null) {
      writer.writeInt("lang", this.getProtobufObj().getLangValue());
    }
    if (this.getProtobufObj().getIn() != null) {
      writer.writeUTF("_in", this.getProtobufObj().getIn());
    }
    if (this.getProtobufObj().getOut() != null) {
      writer.writeUTF("out", this.getProtobufObj().getOut());
    }
    writer.writeInt("status_code", this.getProtobufObj().getStatusCode());
    writer.writeInt("dialog_result", this.getProtobufObj().getDialogResult().getNumber());
    if (this.getProtobufObj().getSkill() != null) {
      writer.writeUTF("skill", this.getProtobufObj().getSkill());
    }
    if (this.getProtobufObj().getIntent() != null) {
      writer.writeUTF("intent", this.getProtobufObj().getIntent());
    }
    if (this.getProtobufObj().getStart() != null) {
      writer.writeLong("start", this.getProtobufObj().getStart().getSeconds());
    }
    if (this.getProtobufObj().getEnd() != null) {
      writer.writeLong("end", this.getProtobufObj().getEnd().getSeconds());
    }

//    속도 너무 느려지셔 주석 처리
//    try {
//      logger.debug("writePortable : {}", this.toString());
//    } catch (Exception e) {
//      e.printStackTrace();
//    }
  }

  @Override
  public void readPortable(PortableReader reader) throws IOException {
    this.setProtobufObj(SessionTalk.parseFrom(reader.readByteArray("_msg")));

//    속도 너무 느려지셔 주석 처리
//    try {
//      logger.debug("readPortable : {}", this.toString());
//    } catch (Exception e) {
//      e.printStackTrace();
//    }
  }

  @Override
  public String toString() {
    return String
        .format("[%s] [%s]", this.getProtobufObj().getClass().getSimpleName(),
            this.getProtobufObj().toString());
  }

}
