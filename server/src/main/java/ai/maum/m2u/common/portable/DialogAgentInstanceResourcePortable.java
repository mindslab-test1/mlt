package ai.maum.m2u.common.portable;

import com.hazelcast.nio.serialization.Portable;
import com.hazelcast.nio.serialization.PortableReader;
import com.hazelcast.nio.serialization.PortableWriter;
import maum.m2u.server.Pool.DialogAgentInstanceResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;

/*
message DialogAgentInstanceResource {
  // registred program information by admin
  string name = 1;
  string description = 2;
  string version = 3;

  // PoolAgent: IMDG(redis, hazelcast), finding or registrering
  string chatbot = 11;
  string skill = 12;
  maum.common.LangCode lang = 13;
  string key = 14;

  // server access info used by TalkImpl
  string server_ip = 31;
  int32 server_port = 32;

  // RUN information by launcher
  DialogAgentLaunchType launch_type = 41;
  string launcher = 42;
  int32 pid = 43;
  google.protobuf.Timestamp started_at = 44;

  maum.m2u.da.DialogAgentProviderParam param = 100;
}
 */

@Service
public class DialogAgentInstanceResourcePortable implements Portable {

  static final Logger logger = LoggerFactory.getLogger(DialogAgentInstanceResourcePortable.class);

  private DialogAgentInstanceResource protobufObj;

  public DialogAgentInstanceResourcePortable() {
    protobufObj = DialogAgentInstanceResource.newBuilder().build();
  }

  public DialogAgentInstanceResource getProtobufObj() {
    return protobufObj;
  }

  public void setProtobufObj(DialogAgentInstanceResource protobufObj) {
    this.protobufObj = protobufObj;
  }

  @Override
  public int getClassId() {
    return PortableClassId.DIALOG_AGENT_INSTANCE_RESOURCE;
  }

  @Override
  public int getFactoryId() {
    return PortableClassId.FACTORY_ID;
  }

  @Override
  public void writePortable(PortableWriter writer) throws IOException {
    if (this.getProtobufObj().toByteArray() != null) {
      writer.writeByteArray("_msg", this.getProtobufObj().toByteArray());
    }
    if (this.getProtobufObj().getKey() != null) {
      writer.writeUTF("key", this.getProtobufObj().getKey());
    }
    if (this.getProtobufObj().getDaiId() != null) {
      writer.writeUTF("dai_id", this.getProtobufObj().getDaiId());
    }
    if (this.getProtobufObj().getName() != null) {
      writer.writeUTF("name", this.getProtobufObj().getName());
    }
    if (this.getProtobufObj().getChatbot() != null) {
      writer.writeUTF("chatbot", this.getProtobufObj().getChatbot());
    }
    if (this.getProtobufObj().getLang() != null) {
      writer.writeInt("lang", this.getProtobufObj().getLangValue());
    }
    if (this.getProtobufObj().getDamName() != null) {
      writer.writeUTF("dam_name", this.getProtobufObj().getDamName());
    }
    if (this.getProtobufObj().getServerIp() != null) {
      writer.writeUTF("server_ip", this.getProtobufObj().getServerIp());
    }
    writer.writeInt("server_port", this.getProtobufObj().getServerPort());
    if (this.getProtobufObj().getLaunchType() != null) {
      writer.writeInt("launch_type", this.getProtobufObj().getLaunchTypeValue());
    }
    if (this.getProtobufObj().getLauncher() != null) {
      writer.writeUTF("launcher", this.getProtobufObj().getLauncher());
    }
    writer.writeInt("pid", this.getProtobufObj().getPid());
    if (this.getProtobufObj().getStartedAt() != null) {
      writer.writeLong("started_at", this.getProtobufObj().getStartedAt().getSeconds());
    }

    try {
      logger.trace("writePortable : {}", this.toString());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public void readPortable(PortableReader reader) throws IOException {
    this.setProtobufObj(DialogAgentInstanceResource.parseFrom(reader.readByteArray("_msg")));

    try {
      logger.trace("readPortable : {}", this.toString());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public String toString() {
    return String
        .format("[%s] [%s]", this.getProtobufObj().getClass().getSimpleName(),
            this.getProtobufObj().toString());
  }

}
