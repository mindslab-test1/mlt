package ai.maum.m2u.common.portable;

import com.hazelcast.nio.serialization.Portable;
import com.hazelcast.nio.serialization.PortableReader;
import com.hazelcast.nio.serialization.PortableWriter;
import maum.m2u.server.Pool.DialogAgentInstanceResourceSkill;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;

/*
// MAP 저장 시 Key: DAIR UUID key + "_" + skill
message DialogAgentInstanceResourceSkill {
  // DAIR의 고유 키
  string dair_key = 1;
  // DAIR + skill이 처리하는 Chatbot
  string chatbot = 2;
  // DAIR + skill의 skill
  string skill = 3;
  // DAIR + skill이 처리하는 언어
  maum.common.LangCode lang = 4;
}
 */

@Service
public class DialogAgentInstanceResourceSkillPortable implements Portable {

  static final Logger logger = LoggerFactory.getLogger(DialogAgentInstanceResourceSkillPortable.class);

  private DialogAgentInstanceResourceSkill protobufObj;

  public DialogAgentInstanceResourceSkillPortable() {
    protobufObj = DialogAgentInstanceResourceSkill.newBuilder().build();
  }

  public DialogAgentInstanceResourceSkill getProtobufObj() {
    return protobufObj;
  }

  public void setProtobufObj(DialogAgentInstanceResourceSkill protobufObj) {
    this.protobufObj = protobufObj;
  }

  @Override
  public int getClassId() {
    return PortableClassId.DIALOG_AGENT_INSTANCE_RESOURCE_SKILL;
  }

  @Override
  public int getFactoryId() {
    return PortableClassId.FACTORY_ID;
  }

  @Override
  public void writePortable(PortableWriter writer) throws IOException {
    if (this.getProtobufObj().getDairKey() != null) {
      writer.writeUTF("dair_key", this.getProtobufObj().getDairKey());
    }
    if (this.getProtobufObj().getChatbot() != null) {
      writer.writeUTF("chatbot", this.getProtobufObj().getChatbot());
    }
    if (this.getProtobufObj().getSkill() != null) {
      writer.writeUTF("skill", this.getProtobufObj().getSkill());
    }
    if (this.getProtobufObj().getLang() != null) {
      writer.writeInt("lang", this.getProtobufObj().getLangValue());
    }

    if (this.getProtobufObj().toByteArray() != null) {
      writer.writeByteArray("_msg", this.getProtobufObj().toByteArray());
    }

    try {
      logger.trace("writePortable : {}", this.toString());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public void readPortable(PortableReader reader) throws IOException {
    this.setProtobufObj(DialogAgentInstanceResourceSkill.parseFrom(reader.readByteArray("_msg")));

    try {
      logger.trace("readPortable : {}", this.toString());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public String toString() {
    return String
        .format("[%s] [%s]", this.getProtobufObj().getClass().getSimpleName(),
            this.getProtobufObj().toString());
  }

}
