package ai.maum.m2u.common.portable;

import com.hazelcast.nio.serialization.Portable;
import com.hazelcast.nio.serialization.PortableReader;
import com.hazelcast.nio.serialization.PortableWriter;
import java.io.IOException;
import maum.m2u.console.Da.DialogAgentActivationInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
message DialogAgentActivationInfo {
  string da_name = 1;
  string dam_name = 2;
  bool active = 3;
}
 */

public class DialogAgentActivationInfoPortable implements Portable {

  static final Logger logger = LoggerFactory.getLogger(DialogAgentActivationInfoPortable.class);

  private DialogAgentActivationInfo protobufObj;

  public DialogAgentActivationInfoPortable() {
    protobufObj = DialogAgentActivationInfo.newBuilder().build();
  }

  public DialogAgentActivationInfo getProtobufObj() {
    return protobufObj;
  }

  public void setProtobufObj(DialogAgentActivationInfo protobufObj) {
    this.protobufObj = protobufObj;
  }

  @Override
  public int getFactoryId() {
    return PortableClassId.FACTORY_ID;
  }

  @Override
  public int getClassId() {
    return PortableClassId.DIALOG_AGENT_ACTIVATION_INFO;
  }

  @Override
  public void writePortable(PortableWriter writer) throws IOException {
    //writer.writeUTF("key", this.getD());
    if (this.getProtobufObj() != null) {
      writer.writeByteArray("_msg", this.getProtobufObj().toByteArray());
    }
    if (this.getProtobufObj().getDaName() != null) {
      writer.writeUTF("da_name", this.getProtobufObj().getDaName());
    }
    if (this.getProtobufObj().getDamName() != null) {
      writer.writeUTF("dam_name", this.getProtobufObj().getDamName());
    }
    writer.writeBoolean("active", this.getProtobufObj().getActive());

    try {
      logger.trace("writePortable : {}", this.toString());
    } catch (Exception e) {
      logger.error("writePortable e : " , e);
    }
  }

  @Override
  public void readPortable(PortableReader reader) throws IOException {
    this.setProtobufObj(DialogAgentActivationInfo.parseFrom(reader.readByteArray("_msg")));

    try {
      logger.trace("readPortable : {}", this.toString());
    } catch (Exception e) {
      logger.error("readPortable e : " , e);
    }
  }

  @Override
  public String toString() {
    return String
        .format("[%s] [%s]", this.getProtobufObj().getClass().getSimpleName(),
            this.getProtobufObj().toString());
  }

}
