package ai.maum.m2u.common.portable;

import com.hazelcast.nio.serialization.Portable;
import com.hazelcast.nio.serialization.PortableReader;
import com.hazelcast.nio.serialization.PortableWriter;
import java.io.IOException;
import maum.m2u.console.Da.DialogAgent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
message DialogAgent {
  string name = 1;
  string description = 2;
  int32 version = 3;
  string da_executable = 4;
  string type =5;
  string da_spec = 6;
  string fullCommand = 7;

  string lang = 8;
  bool require_user_privacy = 23;
}
 */
public class DialogAgentPortable implements Portable {

  static final Logger logger = LoggerFactory.getLogger(DialogAgentPortable.class);

  private DialogAgent protobufObj;

  public DialogAgentPortable() {
    protobufObj = DialogAgent.newBuilder().build();
  }

  public DialogAgent getProtobufObj() {
    return protobufObj;
  }

  public void setProtobufObj(DialogAgent protobufObj) {
    this.protobufObj = protobufObj;
  }

  @Override
  public int getFactoryId() {
    return PortableClassId.FACTORY_ID;
  }

  @Override
  public int getClassId() {
    return PortableClassId.DIALOG_AGENT;
  }


  @Override
  public void writePortable(PortableWriter writer) throws IOException {
    if (this.getProtobufObj().getName() != null) {
      writer.writeUTF("name", this.getProtobufObj().getName());
    }
    if (this.getProtobufObj() != null) {
      writer.writeByteArray("_msg", this.getProtobufObj().toByteArray());
    }
    if (this.getProtobufObj().getDescription() != null) {
      writer.writeUTF("description", this.getProtobufObj().getDescription());
    }
    if (this.getProtobufObj().getVersion() != null) {
      writer.writeUTF("version", this.getProtobufObj().getVersion());
    }
    if (this.getProtobufObj().getDaExecutable() != null) {
      writer.writeUTF("da_executable", this.getProtobufObj().getDaExecutable());
    }
    if (this.getProtobufObj().getType() != null) {
      writer.writeUTF("type", this.getProtobufObj().getType());
    }
    /*if (this.getProtobufObj().getDaSpec() != null) {
      writer.writeUTF("da_spec", this.getProtobufObj().getDaSpec());
    }*/
    if (this.getProtobufObj().getDaProdSpec() != null) {
      writer.writeInt("da_prod_spec", this.getProtobufObj().getDaProdSpec().getNumber());
    }

    try {
      logger.trace("writePortable : {}", this.toString());
    } catch (Exception e) {
      logger.error("writePortable e : " , e);
    }
  }

  @Override
  public void readPortable(PortableReader reader) throws IOException {
    this.setProtobufObj(DialogAgent.parseFrom(reader.readByteArray("_msg")));

    try {
      logger.trace("readPortable : {}", this.toString());
    } catch (Exception e) {
      logger.error("readPortable e : " , e);
    }
  }

  @Override
  public String toString() {
    return String
        .format("[%s] [%s]", this.getProtobufObj().getClass().getSimpleName(),
            this.getProtobufObj().toString());
  }

}
