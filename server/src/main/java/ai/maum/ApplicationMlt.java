package ai.maum;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.common.config.RequestUriRole;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.event.EventListener;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

@Slf4j
@SpringBootApplication
@Configuration
@EnableTransactionManagement
@PropertySource(value = "file:${MAUM_ROOT}/etc/maum-admin-server.conf", encoding = "UTF-8")
public class ApplicationMlt extends SpringBootServletInitializer {

  @Autowired
  private ApplicationContext applicationContext;

  @Autowired
  private RequestUriRole requestUriRole;

  @Value("#{'${mlt.permit.api.modules}'.split(',')}")
  private List<String> apiModuls;

  public static void main(String[] args) {
    SpringApplication.run(ApplicationMlt.class, args);
  }

  // spring boot가 ready 상태가 되었을때 동작하는 Listener
  @EventListener(ApplicationReadyEvent.class)
  public void doSomethingAfterStartup() {
    RequestMappingHandlerMapping mapping = applicationContext
        .getBean(RequestMappingHandlerMapping.class);
    Map<RequestMappingInfo, HandlerMethod> map = mapping.getHandlerMethods();

    for (Map.Entry<RequestMappingInfo, HandlerMethod> elem : map.entrySet()) {
      Method method = elem.getValue().getMethod();

      // @UriRoleDesc에 정의된 Role을 기준으로 RoleMap을 생성(Role 규칙은 MAI_ROLE 테이블과 동일)
      if (method.isAnnotationPresent(UriRoleDesc.class)) {
        UriRoleDesc uriRoleDesc = method.getAnnotation(UriRoleDesc.class);

        for (String pattern : elem.getKey().getPatternsCondition().getPatterns()) {
          try {
            requestUriRole.setUri(uriRoleDesc.role(), pattern);
          } catch (Exception e) {
            log.error("{}, role : {}, uri : {}", e.getMessage(), requestUriRole, pattern);
          }
        }
      }
    }
  }
}
