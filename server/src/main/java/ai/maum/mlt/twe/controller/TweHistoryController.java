package ai.maum.mlt.twe.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.twe.entity.TweHistoryEntity;
import ai.maum.mlt.twe.service.TweHistoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("api/twe/triplewiki")
public class TweHistoryController {

  static final Logger logger = LoggerFactory.getLogger(TweHistoryController.class);

  @Autowired
  private TweHistoryService historyService;

  @UriRoleDesc(role = "twe^R")
  @RequestMapping(
      value = "/gethistorypage",
      method = RequestMethod.POST,
      produces = MediaType.APPLICATION_JSON_VALUE,
      consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> getHistoryList(@RequestBody TweHistoryEntity historyParam) {
    try {
      Page<TweHistoryEntity> historyEntity = historyService.getHistoryList(historyParam);
      return new ResponseEntity<>(historyEntity, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}
