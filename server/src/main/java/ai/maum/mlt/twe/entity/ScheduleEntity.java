package ai.maum.mlt.twe.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Entity
@Data
@Getter
@Setter
@Table(name = "MAI_TWE_SCHEDULE")
public class ScheduleEntity implements Serializable {

  @Id
  @Column(name = "SEQ", length = 40, nullable = false)
  private Integer seq;

  @Column(name = "TYPE", length = 10)
  private String type;

  @Column(name = "PERIOD", length = 10)
  private String period;

  @Column(name = "HOUR", length = 10)
  private String hour;

  @Column(name = "MINUTE", length = 10)
  private String minute;

  public void setSeq(Integer seq) {
    this.seq = seq;
  }
}
