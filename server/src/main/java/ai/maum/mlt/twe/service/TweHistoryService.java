package ai.maum.mlt.twe.service;

import ai.maum.mlt.twe.entity.TweHistoryEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface TweHistoryService {

  Page<TweHistoryEntity> getHistoryList(TweHistoryEntity historyEntity);
}
