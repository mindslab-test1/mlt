package ai.maum.mlt.twe.entity;


import ai.maum.mlt.common.pagenation.PageParameters;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;

@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
@Table(name = "MAI_TWE_INDEXING_HISTORY")
public class TweHistoryEntity extends PageParameters {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TWE_HISTORY_GEN")
  @SequenceGenerator(name = "TWE_HISTORY_GEN", sequenceName = "MAI_TWE_HISTORY_SEQ", allocationSize = 5)
  @Column(name = "ID", length = 40, nullable = false)
  private Integer id;

  @CreatedDate
  @Column(name = "CREATED_AT")
  private Date createdAt;

  @Column(name = "LEAD_TIME")
  private String leadTime;

  @Column(name = "INSERT_CNT")
  private Integer insertCnt;

  @Column(name = "DELETE_CNT")
  private Integer deleteCnt;

  @Column(name = "UPDATE_CNT")
  private Integer updateCnt;

  @Transient
  private Date fromDate;

  @Transient
  private Date toDate;

}
