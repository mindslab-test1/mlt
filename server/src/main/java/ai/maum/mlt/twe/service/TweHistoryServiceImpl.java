package ai.maum.mlt.twe.service;

import ai.maum.mlt.twe.entity.TweHistoryEntity;
import ai.maum.mlt.twe.repository.TweHistoryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class TweHistoryServiceImpl implements TweHistoryService {
  static final Logger logger = LoggerFactory.getLogger(TweHistoryServiceImpl.class);

  @Autowired
  private TweHistoryRepository historyRepository;

  @Override
  public Page<TweHistoryEntity> getHistoryList(TweHistoryEntity historyEntity) {
    return historyRepository.findByCreatedAtBetween(historyEntity.getFromDate(), historyEntity.getToDate(), historyEntity.getPageRequest());
  }
}
