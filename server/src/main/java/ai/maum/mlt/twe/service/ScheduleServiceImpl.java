package ai.maum.mlt.twe.service;

import ai.maum.mlt.twe.entity.ScheduleEntity;
import ai.maum.mlt.twe.repository.ScheduleRepository;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class ScheduleServiceImpl implements ScheduleService {

  static final Logger logger = LoggerFactory.getLogger(ScheduleServiceImpl.class);

  @Autowired
  private ScheduleRepository scheduleRepository;

  @Override
  public Page<ScheduleEntity> getScheduleList(PageRequest page) {
    return scheduleRepository.findAll(page);
  }

  @Override
  public ScheduleEntity getSchedule(ScheduleEntity scheduleEntity) {
    return scheduleRepository.findOne(scheduleEntity.getSeq());
  }


  @Override
  public ScheduleEntity insertSchedule(ScheduleEntity scheduleEntity) {
    scheduleRepository.deleteAllInBatch();
    scheduleRepository.saveAndFlush(scheduleEntity);
    return scheduleEntity;
  }

  @Override
  public ScheduleEntity deleteSchedule(ScheduleEntity scheduleEntity){
    scheduleRepository.delete(scheduleEntity.getSeq());
    return scheduleEntity;
  }

  @Override
  public ScheduleEntity manualExecuteSchedule() {
    return null;
  }

  @Override
  public ScheduleEntity manualStopSchedule() {
    return null;
  }

}
