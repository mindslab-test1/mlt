package ai.maum.mlt.twe.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.twe.entity.ScheduleEntity;
import ai.maum.mlt.twe.service.ScheduleService;
import java.util.HashMap;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/twe/schedule")
//@CrossOrigin
public class ScheduleController {

  static final Logger logger = LoggerFactory.getLogger(ScheduleController.class);

  @Autowired
  private ScheduleService scheduleService;

  @UriRoleDesc(role = "twe^R")
  @RequestMapping(
      value = "/getScheduleList",
      method = RequestMethod.POST)
  public ResponseEntity<?> getScheduleList() {
    logger.info("======= call api  [[/api/schedule/getScheduleList]] =======");

    try {
      PageRequest page = new PageRequest(0, 100);
      Page<ScheduleEntity> schedulePage = scheduleService.getScheduleList(page);

      return new ResponseEntity<>(schedulePage.getContent(), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

  }

  @UriRoleDesc(role = "twe^R")
  @RequestMapping(
      value = "/getSchedule",
      method = RequestMethod.POST)
  public ResponseEntity<?> getSchedule(@RequestBody ScheduleEntity scheduleEntity) {
    logger.info("======= call api  [[/api/schedule/getSchedule]] =======");

    try {
      HashMap<String, Object> result = new HashMap<>();
      ScheduleEntity resultSchedule = null;

      //-1이 아니면 seqNo에 해당하는 스케줄 조회
      if (!scheduleEntity.getSeq().toString().equals("-1")) {
        ScheduleEntity paramScheduleEntity = new ScheduleEntity();
        paramScheduleEntity.setSeq(scheduleEntity.getSeq());
        resultSchedule = scheduleService.getSchedule(paramScheduleEntity);
      } else {
        //-1이면 있는 데이터 조회
        PageRequest page = new PageRequest(0, 100);
        Page<ScheduleEntity> schedulePage = scheduleService.getScheduleList(page);

        resultSchedule = new ScheduleEntity();
        if (schedulePage.getContent().size() > 0) {
          resultSchedule = schedulePage.getContent().get(0);
        } else {
          resultSchedule.setSeq(-1);
        }
      }
      result.put("resultSchedule", resultSchedule);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

  }

  @UriRoleDesc(role = "twe^C")
  @RequestMapping(
      value = "/insertSchedule",
      method = RequestMethod.POST)
  public ResponseEntity<?> insertSchedule(@RequestBody ScheduleEntity schedule) {
    logger.info("======= call api  [[/api/schedule/insertSchedule]] =======");
    logger.info("======= api param [[{}]] =======", schedule.toString());
    try {
      scheduleService.insertSchedule(schedule);
      return new ResponseEntity<>(schedule, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "twe^D")
  @RequestMapping(
      value = "/deleteSchedule",
      method = RequestMethod.POST)
  public ResponseEntity<?> deleteSchedule(@RequestBody ScheduleEntity schedule) {
    logger.info("======= call api  [[/api/schedule/deleteSchedule]] =======");
    logger.info("======= api param [[{}]] =======", schedule.toString());
    try {
      if(schedule.getSeq() != -1){
        scheduleService.deleteSchedule(schedule);
        return new ResponseEntity<>(null, HttpStatus.OK);
      }
      else{
        return new ResponseEntity<>(null, HttpStatus.OK);
      }

    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @RequestMapping(
      value = "/manualExecuteSchedule",
      method = RequestMethod.POST
  )
  public ResponseEntity<?> manualExecuteSchedule() {
    logger.info("======= call api  [[/api/schedule/manualExecuteSchedule]] =======");
    try {

      /*
      * Add logic
      *
      */
      return new ResponseEntity<>(null, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }


  @RequestMapping(
      value = "/manualStopSchedule",
      method = RequestMethod.POST
  )
  public ResponseEntity<?> manualStopSchedule() {
    logger.info("======= call api  [[/api/schedule/manualStopSchedule]] =======");
    try {

      /*
      * Add logic
      *
      */
      return new ResponseEntity<>(null, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
