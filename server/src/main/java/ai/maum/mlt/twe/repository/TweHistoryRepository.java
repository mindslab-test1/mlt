package ai.maum.mlt.twe.repository;

import ai.maum.mlt.twe.entity.TweHistoryEntity;
import java.util.Date;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface TweHistoryRepository extends JpaRepository<TweHistoryEntity, Date>{
  Page<TweHistoryEntity> findByCreatedAtBetween(Date from, Date end,
      Pageable pageable);
}
