package ai.maum.mlt.twe.service;

import ai.maum.mlt.twe.entity.ScheduleEntity;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public interface ScheduleService {

  Page<ScheduleEntity> getScheduleList(PageRequest page);

  ScheduleEntity getSchedule(ScheduleEntity scheduleEntity);

  ScheduleEntity insertSchedule(ScheduleEntity scheduleEntity);

  ScheduleEntity deleteSchedule(ScheduleEntity scheduleEntity);

  ScheduleEntity manualExecuteSchedule();

  ScheduleEntity manualStopSchedule();

}
