package ai.maum.mlt.twe.repository;

import ai.maum.mlt.twe.entity.ScheduleEntity;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface ScheduleRepository extends JpaRepository<ScheduleEntity, Integer> {

}
