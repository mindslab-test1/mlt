package ai.maum.mlt.itfc;

import ai.maum.mlt.common.util.GrpcUtils;
import ai.maum.mlt.nqa.entity.Category;
import com.google.protobuf.Empty;
import io.grpc.ManagedChannel;
import io.grpc.StatusRuntimeException;
import maum.brain.qa.nqa.Admin.*;
import maum.brain.qa.nqa.NQaAdminServiceGrpc;
import maum.brain.qa.nqa.NQaAdminServiceGrpc.*;
import org.slf4j.LoggerFactory;

public class NQAGrpcInterfaceManager {

  static final org.slf4j.Logger logger = LoggerFactory.getLogger(NQAGrpcInterfaceManager.class);

  private String host;
  private int port;

  public NQAGrpcInterfaceManager(String host, int port) {
    this.host = host;
    this.port = port;
  }

  public IndexStatus getIndexingStatus(Empty request) {
    logger.debug("===== NQAGrpcInterfaceManager getIndexingStatus");
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      NQaAdminServiceBlockingStub stub = NQaAdminServiceGrpc.newBlockingStub(channel);
      IndexStatus result = stub.getIndexingStatus(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== NQAGrpcInterfaceManager getIndexingStatus :: RPC failed: {}",
          e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("getIndexingStatus e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  public GetIndexedKeywordsResponse getIndexedKeywords(GetIndexedKeywordsRequest request) {
    logger
        .debug("===== NQAGrpcInterfaceManager getIndexedKeywords :: GetIndexedKeywordsResponse {}",
            request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      NQaAdminServiceBlockingStub stub = NQaAdminServiceGrpc.newBlockingStub(channel);
      GetIndexedKeywordsResponse result = stub.getIndexedKeywords(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== NQAGrpcInterfaceManager getIndexedKeywords :: RPC failed: {}",
          e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("getIndexedKeywords e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  public IndexStatus fullIndexing(IndexingRequest request) {
    logger.debug("===== NQAGrpcInterfaceManager fullIndexing :: IndexingRequest {}", request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      NQaAdminServiceBlockingStub stub = NQaAdminServiceGrpc.newBlockingStub(channel);
      IndexStatus result = stub.indexing(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== NQAGrpcInterfaceManager fullIndexing :: RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("fullIndexing e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  public IndexStatus additionalIndexing(IndexingRequest request) {
    logger.debug("===== NQAGrpcInterfaceManager additionalIndexing :: IndexingRequest {}", request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      NQaAdminServiceBlockingStub stub = NQaAdminServiceGrpc.newBlockingStub(channel);
      IndexStatus result = stub.indexing(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== NQAGrpcInterfaceManager additionalIndexing :: RPC failed: {}",
          e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("additionalIndexing e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  public IndexStatus abortIndexing(IndexingRequest request) {
    logger.debug("===== NQAGrpcInterfaceManager abortIndexing :: IndexingRequest {}", request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      NQaAdminServiceBlockingStub stub = NQaAdminServiceGrpc.newBlockingStub(channel);
      IndexStatus result = stub.abortIndexing(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== NQAGrpcInterfaceManager abortIndexing :: RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("abortIndexing e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  public AnswerList getAnswerList(GetAnswerListByCategoryRequest request) {
    logger.debug("===== NQAGrpcInterfaceManager getAnswerList :: GetAnswerListByCategoryRequest {}",
        request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      NQaAdminServiceBlockingStub stub = NQaAdminServiceGrpc.newBlockingStub(channel);
      AnswerList result = stub.getAnswerListByCategory(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== NQAGrpcInterfaceManager getAnswerList :: RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("getAnswerList e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  public ChannelList getChannelList(com.google.protobuf.Empty request) {
    logger.debug("===== NQAGrpcInterfaceManager getChannelList");
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      NQaAdminServiceBlockingStub stub = NQaAdminServiceGrpc.newBlockingStub(channel);
      ChannelList result = stub.getChannelList(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== NQAGrpcInterfaceManager getChannelList :: RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("getChannelList e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  public NQaAdminChannel getChannelById(int channelId) {
    logger.debug("===== NQAGrpcInterfaceManager getChannelById");
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      NQaAdminServiceBlockingStub stub = NQaAdminServiceGrpc.newBlockingStub(channel);
      NQaAdminChannel.Builder getChannelByIdRequest = NQaAdminChannel.newBuilder();
      getChannelByIdRequest.setId(channelId);
      NQaAdminChannel result = stub.getChannelById(getChannelByIdRequest.build());
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== NQAGrpcInterfaceManager getChannelById :: RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        logger.error("getChannelById e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  public CategoryList getCategoryListByChannelId(int channelId) {
    logger.debug("===== NQAGrpcInterfaceManager getCategoryListByChannelId");
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      NQaAdminServiceBlockingStub stub = NQaAdminServiceGrpc.newBlockingStub(channel);
      NQaAdminChannel.Builder channelRequest = NQaAdminChannel.newBuilder();
      channelRequest.setId(channelId);
      CategoryList result = stub.getCategoryListByChannelId(channelRequest.build());
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== NQAGrpcInterfaceManager getCategoryListByWorkspace :: RPC failed: {}",
          e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("getCategoryListByChannelId e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  public CategoryList getCategoryListByName(Category category) {
    logger.debug("===== NQAGrpcInterfaceManager getCategoryListByName");
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      NQaAdminServiceBlockingStub stub = NQaAdminServiceGrpc.newBlockingStub(channel);
      CategoryList result = stub.getCategoryListByName(category.makeProto());
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== NQAGrpcInterfaceManager getCategoryListByName :: RPC failed: {}",
          e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("getCategoryListByName e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  public LayerList getLayerListByCategory(NQaAdminCategory request) {
    logger.debug("===== NQAGrpcInterfaceManager getLayerList");
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      NQaAdminServiceBlockingStub stub = NQaAdminServiceGrpc.newBlockingStub(channel);
      LayerList result = stub.getLayerListByCategory(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== NQAGrpcInterfaceManager getLayerList :: RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("getLayerListByCategory e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  public AnswerList getAnswerById(GetAnswerByIdRequest request) {
    logger.debug("===== NQAGrpcInterfaceManager getAnswerById :: GetAnswerByIdRequest {}", request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      NQaAdminServiceBlockingStub stub = NQaAdminServiceGrpc.newBlockingStub(channel);
      AnswerList result = stub.getAnswerById(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== NQAGrpcInterfaceManager getAnswerById :: RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("getAnswerById e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  public GetCategoryByIdResponse getCategoryById(GetCategoryByIdRequest request) {
    logger.debug("===== NQAGrpcInterfaceManager getCategoryById :: GetCategoryByIdRequest {}",
        request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      NQaAdminServiceBlockingStub stub = NQaAdminServiceGrpc.newBlockingStub(channel);
      GetCategoryByIdResponse result = stub.getCategoryById(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger
          .error("===== NQAGrpcInterfaceManager getCategoryById :: RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("getCategoryById e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  public AddAnswerResponse addAnswer(AddAnswerRequest request) {
    logger.debug("===== NQAGrpcInterfaceManager addAnswer :: AddAnswerRequest {}", request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      NQaAdminServiceBlockingStub stub = NQaAdminServiceGrpc.newBlockingStub(channel);
      AddAnswerResponse result = stub.addAnswer(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== NQAGrpcInterfaceManager addAnswer :: RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("addAnswer e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  public NQaAdminChannel addChannel(NQaAdminChannel request) {
    logger.debug("===== NQAGrpcInterfaceManager addChannel :: AddChannelRequest {}", request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      NQaAdminServiceBlockingStub stub = NQaAdminServiceGrpc.newBlockingStub(channel);
      NQaAdminChannel result = stub.addChannel(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== NQAGrpcInterfaceManager addChannel :: RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        logger.error("addChannel e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  public AddCategoryResponse addCategory(AddCategoryRequest request) {
    logger.debug("===== NQAGrpcInterfaceManager addCategory :: AddCategoryRequest {}", request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      NQaAdminServiceBlockingStub stub = NQaAdminServiceGrpc.newBlockingStub(channel);
      AddCategoryResponse result = stub.addCategory(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== NQAGrpcInterfaceManager addCategory :: RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("addCategory e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }


  public AnswerList editAnswer(EditAnswerRequest request) {
    logger.debug("===== NQAGrpcInterfaceManager editAnswer");
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      NQaAdminServiceBlockingStub stub = NQaAdminServiceGrpc.newBlockingStub(channel);
      AnswerList result = stub.editAnswer(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== NQAGrpcInterfaceManager editAnswer :: RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("editAnswer e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  public EditCategoryResponse editCategory(EditCategoryRequest request) {
    logger.debug("===== NQAGrpcInterfaceManager editCategory");
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      NQaAdminServiceBlockingStub stub = NQaAdminServiceGrpc.newBlockingStub(channel);
      EditCategoryResponse result = stub.editCategory(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== NQAGrpcInterfaceManager editCategory :: RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("editCategory e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  public NQaAdminChannel editChannel(NQaAdminChannel request) {
    logger.debug("===== NQAGrpcInterfaceManager editChannel");
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      NQaAdminServiceBlockingStub stub = NQaAdminServiceGrpc.newBlockingStub(channel);
      NQaAdminChannel result = stub.editChannel(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== NQAGrpcInterfaceManager editChannel :: RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        logger.error("editChannel e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  public RemoveAnswerResponse removeAnswer(RemoveAnswerRequest request) {
    logger.debug("===== NQAGrpcInterfaceManager removeAnswer");
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      NQaAdminServiceBlockingStub stub = NQaAdminServiceGrpc.newBlockingStub(channel);
      RemoveAnswerResponse result = stub.removeAnswer(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== NQAGrpcInterfaceManager removeAnswer :: RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("removeAnswer e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  public RemoveCategoryResponse removeCategory(RemoveCategoryRequest request) {
    logger.debug("===== NQAGrpcInterfaceManager removeCategory");
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      NQaAdminServiceBlockingStub stub = NQaAdminServiceGrpc.newBlockingStub(channel);
      RemoveCategoryResponse result = stub.removeCategory(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== NQAGrpcInterfaceManager removeCategory:: RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("removeCategory e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  public RemoveChannelResponse removeChannel(int channelId) {
    logger.debug("===== NQAGrpcInterfaceManager removeChannel");
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      NQaAdminServiceBlockingStub stub = NQaAdminServiceGrpc.newBlockingStub(channel);
      RemoveChannelRequest.Builder request = RemoveChannelRequest.newBuilder();
      request.setId(channelId);
      RemoveChannelResponse result = stub.removeChannel(request.build());
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== NQAGrpcInterfaceManager removeChannel:: RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("removeChannel e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  public UploadAnswerListResponse uploadAnswerList(UploadAnswerListRequest request) {
    logger.debug("===== NQAGrpcInterfaceManager uploadAnswerList");
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      NQaAdminServiceBlockingStub stub = NQaAdminServiceGrpc.newBlockingStub(channel);
      UploadAnswerListResponse result = stub.uploadAnswerList(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger
          .error("===== NQAGrpcInterfaceManager uploadAnswerList:: RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        logger.error("===== Thread Sleep failed: {}", e1.getMessage());
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }
}
