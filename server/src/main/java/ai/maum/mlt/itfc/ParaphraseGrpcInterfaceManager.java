package ai.maum.mlt.itfc;

import ai.maum.mlt.common.util.GrpcUtils;
import io.grpc.ManagedChannel;
import io.grpc.StatusRuntimeException;
import maum.mlt.paraphrase.ParaphraseServiceGrpc;
import org.slf4j.LoggerFactory;

public class ParaphraseGrpcInterfaceManager {

  static final org.slf4j.Logger logger = LoggerFactory
      .getLogger(ParaphraseGrpcInterfaceManager.class);

  private String host;
  private int port;

  public ParaphraseGrpcInterfaceManager(String host, int port) {
    this.host = host;
    this.port = port;
  }

  /**
   * paraphrase grpc server call paraphrase 해야 할 데이터를 보낸 후, paraphrase 된 결과를 리턴 받는다.
   *
   * @return ParaphraseResponse => {repeated ParaphraseResult}
   */
  public maum.mlt.paraphrase.Paraphrase.ParaphraseResponse getParaphrase(
      maum.mlt.paraphrase.Paraphrase.ParaphraseRequest request) throws InterruptedException {
    logger.info("start : {} ", request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      ParaphraseServiceGrpc.ParaphraseServiceBlockingStub stub = ParaphraseServiceGrpc.newBlockingStub(channel);
      maum.mlt.paraphrase.Paraphrase.ParaphraseResponse result = stub.getParaphrase(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("getParaphrase e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

}
