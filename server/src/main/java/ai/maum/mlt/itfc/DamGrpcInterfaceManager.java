package ai.maum.mlt.itfc;

import ai.maum.mlt.common.util.GrpcUtils;
import io.grpc.ManagedChannel;
import io.grpc.StatusRuntimeException;
import lombok.extern.slf4j.Slf4j;
import maum.m2u.server.Dam.DialogAgentManagerStat;
import maum.m2u.server.DialogAgentManagerGrpc;
import maum.m2u.server.DialogAgentManagerGrpc.DialogAgentManagerBlockingStub;

@Slf4j
public class DamGrpcInterfaceManager {

  private String host;
  private int port;

  public DamGrpcInterfaceManager(String host, int port) {
    this.host = host;
    this.port = port;
  }

  public DialogAgentManagerStat setDamName(maum.m2u.server.Dam.DamKey request) {
    log.debug("===== DamGrpcInterfaceManager setDamName");
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      DialogAgentManagerBlockingStub stub = DialogAgentManagerGrpc.newBlockingStub(channel);
      DialogAgentManagerStat result = stub.setDamName(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      log.error("===== DamGrpcInterfaceManager setDamName :: RPC failed: {}",
          e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        log.error("setDamName e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }
}
