package ai.maum.mlt.itfc;

import ai.maum.mlt.common.util.GrpcUtils;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import maum.brain.mrc.train.MrcTrainerGrpc;
import maum.brain.mrc.train.TrainerMonitorGrpc;
import org.slf4j.LoggerFactory;

public class MrcGrpcInterfaceManager {

  static final org.slf4j.Logger logger = LoggerFactory.getLogger(MrcGrpcInterfaceManager.class);

  private String host;
  private int port;

  public MrcGrpcInterfaceManager(String host, int port) {
    this.host = host;
    this.port = port;
  }

  /**
   */
  public maum.brain.mrc.train.MrcTrainerOuterClass.MrcTrainKey open(
      maum.brain.mrc.train.MrcTrainerOuterClass.MrcModel request) throws InterruptedException {
    logger.info("==========MrcGrpcInterfaceManager.open : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      MrcTrainerGrpc.MrcTrainerBlockingStub stub = MrcTrainerGrpc.newBlockingStub(channel);
      maum.brain.mrc.train.MrcTrainerOuterClass.MrcTrainKey result = stub.open(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("open e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /**
   */
  public maum.brain.mrc.train.MrcTrainerOuterClass.MrcTrainStatus getProgress(
      maum.brain.mrc.train.MrcTrainerOuterClass.MrcTrainKey request) throws InterruptedException {
    logger.info("==========MrcGrpcInterfaceManager.getProgress : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      MrcTrainerGrpc.MrcTrainerBlockingStub stub = MrcTrainerGrpc.newBlockingStub(channel);
      maum.brain.mrc.train.MrcTrainerOuterClass.MrcTrainStatus result = stub.getProgress(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("getProgress e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /**
   */
  public java.util.Iterator<maum.brain.mrc.train.MrcTrainerOuterClass.MrcTrainBinary> getBinary(
      maum.brain.mrc.train.MrcTrainerOuterClass.MrcTrainKey request) throws InterruptedException {
    logger.info("==========MrcGrpcInterfaceManager.getBinary : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      MrcTrainerGrpc.MrcTrainerBlockingStub stub = MrcTrainerGrpc.newBlockingStub(channel);
      java.util.Iterator<maum.brain.mrc.train.MrcTrainerOuterClass.MrcTrainBinary> result = stub.getBinary(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("getBinary e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /**
   */
  public maum.brain.mrc.train.MrcTrainerOuterClass.MrcTrainStatus close(
      maum.brain.mrc.train.MrcTrainerOuterClass.MrcTrainKey request) throws InterruptedException {
    logger.info("==========MrcGrpcInterfaceManager.close : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      MrcTrainerGrpc.MrcTrainerBlockingStub stub = MrcTrainerGrpc.newBlockingStub(channel);
      maum.brain.mrc.train.MrcTrainerOuterClass.MrcTrainStatus result = stub.close(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("close e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /**
   */
  public maum.brain.mrc.train.MrcTrainerOuterClass.MrcTrainStatus stop(
      maum.brain.mrc.train.MrcTrainerOuterClass.MrcTrainKey request) throws InterruptedException {
    logger.info("==========MrcGrpcInterfaceManager.stop : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      MrcTrainerGrpc.MrcTrainerBlockingStub stub = MrcTrainerGrpc.newBlockingStub(channel);
      maum.brain.mrc.train.MrcTrainerOuterClass.MrcTrainStatus result = stub.stop(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("stop e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /**
   */
  public maum.brain.mrc.train.MrcTrainerOuterClass.MrcTrainStatusList getAllProgress(
      com.google.protobuf.Empty request) throws InterruptedException {
    logger.info("==========MrcGrpcInterfaceManager.getAllProgress : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      MrcTrainerGrpc.MrcTrainerBlockingStub stub = MrcTrainerGrpc.newBlockingStub(channel);
      maum.brain.mrc.train.MrcTrainerOuterClass.MrcTrainStatusList result = stub.getAllProgress(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("getAllProgress e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /**
   */
  public com.google.protobuf.Empty removeBinary(
      maum.brain.mrc.train.MrcTrainerOuterClass.MrcTrainKey request) throws InterruptedException {
    logger.info("==========MrcGrpcInterfaceManager.removeBinary : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      MrcTrainerGrpc.MrcTrainerBlockingStub stub = MrcTrainerGrpc.newBlockingStub(channel);
      com.google.protobuf.Empty result = stub.removeBinary(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("removeBinary e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  public com.google.protobuf.Empty notify(
      maum.brain.mrc.train.MrcTrainerOuterClass.TrainProcStatus request)
      throws InterruptedException {
    logger.info("==========MrcGrpcInterfaceManager.notify : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      TrainerMonitorGrpc.TrainerMonitorBlockingStub stub = TrainerMonitorGrpc.newBlockingStub(channel);
      com.google.protobuf.Empty result = stub.notify(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("notify e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }
}
