package ai.maum.mlt.itfc;

import ai.maum.mlt.common.util.GrpcUtils;
import com.google.protobuf.ByteString;
import com.google.protobuf.Empty;
import io.grpc.ManagedChannel;
import io.grpc.Metadata;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.MetadataUtils;
import io.grpc.stub.StreamObserver;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import maum.brain.cl.ClassifierGrpc;
import maum.brain.cl.ClassifierOuterClass.ClassInputText;
import maum.brain.cl.ClassifierOuterClass.ClassifiedSummary;
import maum.brain.cl.ClassifierOuterClass.ServerStatusList;
import maum.brain.cl.ClassifierOuterClass.SetModelResponse;
import maum.brain.cl.ClassifierResolverGrpc;
import maum.brain.cl.ClassifierResolverGrpc.ClassifierResolverBlockingStub;
import maum.brain.cl.ClassifierServiceGrpc;
import maum.brain.cl.train.ClassifierTrainerGrpc;
import maum.brain.cl.train.Cltrainer.ClTrainBinary;
import maum.brain.han.run.HanClassifierGrpc;
import maum.brain.han.run.HanClassifierGrpc.HanClassifierBlockingStub;
import maum.brain.han.run.HanRun.*;
import maum.brain.han.train.HanTrainerGrpc;
import maum.brain.han.train.HanTrain;
import maum.brain.cl.train.TrainerMonitorGrpc;
import maum.brain.hmd.HmdClassifierGrpc;
import maum.brain.nlp.NaturalLanguageProcessingServiceGrpc;
import maum.brain.nlp.NaturalLanguageProcessingServiceGrpc.NaturalLanguageProcessingServiceBlockingStub;
import maum.brain.nlp.Nlp.Document;
import maum.brain.nlp.Nlp.InputText;
import maum.brain.nlp.Nlp.KeywordFrequencyLevel;
import maum.brain.nlp.Nlp.NlpAnalysisLevel;
import maum.brain.nlp.Nlp.Sentence;
import maum.brain.nlp.Nlp3CustomizeServiceGrpc;
import maum.brain.nlp.SplitSentenceServiceGrpc;
import maum.brain.we.WordEmbeddingServiceGrpc;
import maum.common.LangOuterClass.LangCode;
import maum.common.Types.FilePart;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.AsyncResult;

public class TaGrpcInterfaceManager {

  static final org.slf4j.Logger logger = LoggerFactory.getLogger(TaGrpcInterfaceManager.class);
  private String host;
  private int port;

  public TaGrpcInterfaceManager(String host, int port) {
    this.host = host;
    this.port = port;
  }

  /**
   * service ClassifierResolver { rpc Find (Model) returns (ServerStatus); rpc GetModels
   * (google.protobuf.Empty) returns (ModelList); rpc GetServers (google.protobuf.Empty) returns
   * (ServerStatusList); rpc Stop (Model) returns (ServerStatus); rpc Restart (Model) returns
   * (ServerStatus); rpc Ping (Model) returns (ServerStatus); rpc SetModel (stream minds.FilePart)
   * returns (SetModelResponse); rpc DeleteModel (Model) returns (ServerStatus); rpc
   * GetModelCategories (Model) returns (ModelCategories); }
   */
  public maum.brain.cl.ClassifierOuterClass.ServerStatus find(
      maum.brain.cl.ClassifierOuterClass.Model request) throws InterruptedException {

    logger.info("==========TaGrpcInterfaceManager.find : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      ClassifierResolverGrpc.ClassifierResolverBlockingStub stub = ClassifierResolverGrpc
          .newBlockingStub(channel);
      maum.brain.cl.ClassifierOuterClass.ServerStatus result = stub.find(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("find e1 : ", e1);
      } finally {
        GrpcUtils.closeChannel(channel);
      }
      return null;
    }
  }

  /**
   *
   */
  public maum.brain.cl.ClassifierOuterClass.ModelList getModels(com.google.protobuf.Empty request)
      throws InterruptedException {

    logger.info("==========TaGrpcInterfaceManager.getModels : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      ClassifierResolverGrpc.ClassifierResolverBlockingStub stub = ClassifierResolverGrpc
          .newBlockingStub(channel);
      maum.brain.cl.ClassifierOuterClass.ModelList result = stub.getModels(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("getModels e1 : ", e1);
      } finally {
        GrpcUtils.closeChannel(channel);
      }
      return null;
    }
  }

  /**
   *
   */
  public ServerStatusList getServers(
      Empty request) throws InterruptedException {

    logger.info("==========TaGrpcInterfaceManager.getServers : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      ClassifierResolverBlockingStub stub = ClassifierResolverGrpc.newBlockingStub(channel);
      ServerStatusList result = stub.getServers(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("getServers e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /**
   *
   */
  public maum.brain.cl.ClassifierOuterClass.ServerStatus stop(
      maum.brain.cl.ClassifierOuterClass.Model request) throws InterruptedException {

    logger.info("==========TaGrpcInterfaceManager.stop : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      ClassifierResolverGrpc.ClassifierResolverBlockingStub stub = ClassifierResolverGrpc
          .newBlockingStub(channel);
      maum.brain.cl.ClassifierOuterClass.ServerStatus result = stub.stop(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("stop e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /**
   *
   */
  public maum.brain.cl.ClassifierOuterClass.ServerStatus restart(
      maum.brain.cl.ClassifierOuterClass.Model request) throws InterruptedException {

    logger.info("==========TaGrpcInterfaceManager.restart : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      ClassifierResolverGrpc.ClassifierResolverBlockingStub stub = ClassifierResolverGrpc
          .newBlockingStub(channel);
      maum.brain.cl.ClassifierOuterClass.ServerStatus result = stub.restart(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("restart e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /**
   *
   */
  public maum.brain.cl.ClassifierOuterClass.ServerStatus ping(
      maum.brain.cl.ClassifierOuterClass.Model request) throws InterruptedException {

    logger.info("==========TaGrpcInterfaceManager.ping : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      ClassifierResolverGrpc.ClassifierResolverBlockingStub stub = ClassifierResolverGrpc
          .newBlockingStub(channel);
      maum.brain.cl.ClassifierOuterClass.ServerStatus result = stub.ping(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("ping e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  public List<SetModelResponse> setModel(File clientBinary, Metadata metadata)
      throws InterruptedException, IOException {
    logger.info("==========TaGrpcInterfaceManager.setModel : " + clientBinary);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      List<SetModelResponse> setModelResponses = new ArrayList<>();
      final CountDownLatch finishLatch = new CountDownLatch(1);
      ClassifierResolverGrpc.ClassifierResolverStub stub = ClassifierResolverGrpc.newStub(channel);
      stub = MetadataUtils.attachHeaders(stub, metadata);
      io.grpc.stub.StreamObserver<SetModelResponse> responseObserver =
          new io.grpc.stub.StreamObserver<SetModelResponse>() {
            @Override
            public void onNext(SetModelResponse value) {
              logger.info("A message received from server: {}", value);
              setModelResponses.add(value);
            }

            @Override
            public void onError(Throwable t) {
              logger.error("RecordRoute Failed: {}", io.grpc.Status.fromThrowable(t));
              finishLatch.countDown();
              if (channel != null) {
                GrpcUtils.closeChannel(channel);
              }
            }

            @Override
            public void onCompleted() {
              finishLatch.countDown();
              if (channel != null) {
                GrpcUtils.closeChannel(channel);
              }
            }
          };

      io.grpc.stub.StreamObserver<maum.common.Types.FilePart> requestObserver =
          stub.setModel(responseObserver);
      FileInputStream fis = null;
      try {
        fis = new FileInputStream(clientBinary);
        byte[] buffer = new byte[40960];
        while ((fis.read(buffer)) > 0) {
          FilePart filePart = FilePart.newBuilder().setPart(ByteString.copyFrom(buffer)).build();
          requestObserver.onNext(filePart);
        }
        // 메시지 전송이 완료되면 onCompleted()를 호출하여 서버에 종료 상황을 알립니다.
        requestObserver.onCompleted();
      } catch (RuntimeException e) {
        requestObserver.onError(e);
        throw e;
      }
      if (!finishLatch.await(3, TimeUnit.HOURS)) {
        logger.warn("Can not finish within 3 hours");
      }

      return setModelResponses;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("setModel e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
    }
    return null;
  }


  /**
   *
   */
  public maum.brain.cl.ClassifierOuterClass.ServerStatus deleteModel(
      maum.brain.cl.ClassifierOuterClass.Model request) throws InterruptedException {

    logger.info("==========TaGrpcInterfaceManager.deleteModel : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      ClassifierResolverGrpc.ClassifierResolverBlockingStub stub = ClassifierResolverGrpc
          .newBlockingStub(channel);
      maum.brain.cl.ClassifierOuterClass.ServerStatus result = stub.deleteModel(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("deleteModel e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /**
   * <pre>
   * get model categories
   * </pre>
   */
  public maum.brain.cl.ClassifierOuterClass.ModelCategories getModelCategories(
      maum.brain.cl.ClassifierOuterClass.Model request) throws InterruptedException {

    logger.info("==========TaGrpcInterfaceManager.getModelCategories : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      ClassifierResolverGrpc.ClassifierResolverBlockingStub stub = ClassifierResolverGrpc
          .newBlockingStub(channel);
      maum.brain.cl.ClassifierOuterClass.ModelCategories result = stub.getModelCategories(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("getModelCategories e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }
/*
  service ClassifierService {
    rpc GetClass (ClassInputText) returns (ClassifiedSummary);
    rpc GetClassMultiple (stream ClassInputText) returns (stream ClassifiedSummary);
  }
  */

  public maum.brain.cl.ClassifierOuterClass.ClassifiedSummary getClass(
      maum.brain.cl.ClassifierOuterClass.ClassInputText request) throws InterruptedException {
    logger.info("==========TaGrpcInterfaceManager.ClassifierService.getClass : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      ClassifierServiceGrpc.ClassifierServiceBlockingStub stub = ClassifierServiceGrpc
          .newBlockingStub(channel);
      maum.brain.cl.ClassifierOuterClass.ClassifiedSummary result = stub.getClass(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("ClassifierService.getClass e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /*
service Classifier {
  rpc Ping(Model) returns (ServerStatus);
  rpc GetClass (ClassInputText) returns (ClassifiedSummary);
  rpc GetClassMultiple (stream ClassInputText) returns (stream ClassifiedSummary);
}
   */
  public maum.brain.cl.ClassifierOuterClass.ServerStatus classifierPing(
      maum.brain.cl.ClassifierOuterClass.Model request) throws InterruptedException {
    logger.info("==========TaGrpcInterfaceManager.Classifier.classifierPing : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      ClassifierGrpc.ClassifierBlockingStub stub = ClassifierGrpc.newBlockingStub(channel);
      maum.brain.cl.ClassifierOuterClass.ServerStatus result = stub.ping(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("classifierPing e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  public maum.brain.cl.ClassifierOuterClass.ClassifiedSummary classifierGetClass(
      maum.brain.cl.ClassifierOuterClass.ClassInputText request) throws InterruptedException {
    logger.info("==========TaGrpcInterfaceManager.classifierGetClass : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      ClassifierGrpc.ClassifierBlockingStub stub = ClassifierGrpc.newBlockingStub(channel);
      maum.brain.cl.ClassifierOuterClass.ClassifiedSummary result = stub.getClass(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("classifierGetClass e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  public List<ClassifiedSummary> getClassMultiple(String[] lines, LangCode langCode,
      String modelName)
      throws InterruptedException {
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      ClassifierGrpc.ClassifierStub stub = ClassifierGrpc.newStub(channel);
      final CountDownLatch finishLatch = new CountDownLatch(1);
      List<ClassifiedSummary> classifiedSummaryList = new ArrayList<>();

      StreamObserver<ClassifiedSummary> responseObserver =
          new StreamObserver<ClassifiedSummary>() {

            @Override
            public void onNext(ClassifiedSummary value) {
              logger.info("A message received from server: {}", value);
              classifiedSummaryList.add(value);
            }

            @Override
            public void onError(Throwable t) {
              Status status = Status.fromThrowable(t);
              logger.error("DNN Failed : " + status.getCode());
              finishLatch.countDown();
              if (channel != null) {
                GrpcUtils.closeChannel(channel);
              }
            }

            @Override
            public void onCompleted() {
              logger.info("TextObserver");
              finishLatch.countDown();
              if (channel != null) {
                GrpcUtils.closeChannel(channel);
              }
            }
          };
      StreamObserver<ClassInputText> requestObserver = stub.getClassMultiple(responseObserver);

      try {
        for (String line : lines) {
          ClassInputText.Builder classInputText = ClassInputText.newBuilder();
          classInputText.setLang(langCode);
          classInputText.setModel(modelName);
          classInputText.setText(line);
          requestObserver.onNext(classInputText.build());
        }
        requestObserver.onCompleted();
      } catch (RuntimeException e) {
        requestObserver.onError(e);
        throw e;
      }

      if (!finishLatch.await(1, TimeUnit.MINUTES)) {
        logger.warn("DNN Can not finish within 1 minutes");
      }

      return classifiedSummaryList;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("getClassMultiple e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
    }
    return null;
  }

  public List<ClassifiedSummary> getClassMultiple(Document document, LangCode langCode,
      String modelName)
      throws InterruptedException {
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      ClassifierGrpc.ClassifierStub stub = ClassifierGrpc.newStub(channel);
      final CountDownLatch finishLatch = new CountDownLatch(1);
      List<ClassifiedSummary> classifiedSummaryList = new ArrayList<>();

      StreamObserver<ClassifiedSummary> responseObserver =
          new StreamObserver<ClassifiedSummary>() {

            @Override
            public void onNext(ClassifiedSummary value) {
              logger.info("A message received from server: {}", value);
              classifiedSummaryList.add(value);
            }

            @Override
            public void onError(Throwable t) {
              Status status = Status.fromThrowable(t);
              logger.error("DNN Failed : " + status.getCode());
              finishLatch.countDown();
              if (channel != null) {
                GrpcUtils.closeChannel(channel);
              }
            }

            @Override
            public void onCompleted() {
              logger.info("TextObserver");
              finishLatch.countDown();
              if (channel != null) {
                GrpcUtils.closeChannel(channel);
              }
            }
          };

      StreamObserver<ClassInputText> requestObserver = stub.getClassMultiple(responseObserver);

      try {
        for (Sentence sentence : document.getSentencesList()) {
          ClassInputText.Builder classInputText = ClassInputText.newBuilder();
          classInputText.setLang(langCode);
          classInputText.setModel(modelName);
          classInputText.setText(sentence.getText());
          requestObserver.onNext(classInputText.build());
        }
        requestObserver.onCompleted();
      } catch (RuntimeException e) {
        requestObserver.onError(e);
        throw e;
      }

      if (!finishLatch.await(1, TimeUnit.MINUTES)) {
        logger.warn("DNN Can not finish within 1 minutes");
      }

      return classifiedSummaryList;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("getClassMultiple e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
    }
    return null;
  }


  /*
    service ClassifierTrainer {
      rpc Open(ClModel) returns (ClTrainKey);
      rpc GetProgress(ClTrainKey) returns(ClTrainStatus);
      rpc GetBinary(ClTrainKey) returns (stream ClTrainBinary);
      rpc Close(ClTrainKey) returns (ClTrainStatus);
      rpc Stop(ClTrainKey) returns (ClTrainStatus);
      rpc GetAllProgress(google.protobuf.Empty) returns (ClTrainStatusList);
      rpc RemoveBinary(ClTrainKey) returns (google.protobuf.Empty);
    }
  */
  public maum.brain.cl.train.Cltrainer.ClTrainKey open(
      maum.brain.cl.train.Cltrainer.ClModel request) throws InterruptedException {
    logger.info("==========TaGrpcInterfaceManager.open : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      ClassifierTrainerGrpc.ClassifierTrainerBlockingStub stub = ClassifierTrainerGrpc
          .newBlockingStub(channel);
      maum.brain.cl.train.Cltrainer.ClTrainKey result = stub.open(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("open e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /**
   *
   */
  public maum.brain.cl.train.Cltrainer.ClTrainStatus getProgress(
      maum.brain.cl.train.Cltrainer.ClTrainKey request) {
    logger.info("==========TaGrpcInterfaceManager.ClassifierTrainer.getProgress : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      ClassifierTrainerGrpc.ClassifierTrainerBlockingStub stub = ClassifierTrainerGrpc
          .newBlockingStub(channel);
      maum.brain.cl.train.Cltrainer.ClTrainStatus result = stub.getProgress(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("getProgress e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /**
   *
   */
  public List<maum.brain.cl.train.Cltrainer.ClTrainBinary> getBinary(
      maum.brain.cl.train.Cltrainer.ClTrainKey request) throws InterruptedException {
    logger.info("==========TaGrpcInterfaceManager.getBinary : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      ClassifierTrainerGrpc.ClassifierTrainerStub stub = ClassifierTrainerGrpc.newStub(channel);
      List<maum.brain.cl.train.Cltrainer.ClTrainBinary> result = new ArrayList<>();
      final CountDownLatch finishLatch = new CountDownLatch(1);

      io.grpc.stub.StreamObserver<maum.brain.cl.train.Cltrainer.ClTrainBinary> responseObserver =
          new io.grpc.stub.StreamObserver<maum.brain.cl.train.Cltrainer.ClTrainBinary>() {
            @Override
            public void onNext(maum.brain.cl.train.Cltrainer.ClTrainBinary value) {
              logger.info("A message received from server: {}", value);
//              result.setTrainId(value.getTrainId());
              result.add(value);
            }

            @Override
            public void onError(Throwable t) {
              logger.error("RecordRoute Failed: {}", io.grpc.Status.fromThrowable(t));
              finishLatch.countDown();
              if (channel != null) {
                GrpcUtils.closeChannel(channel);
              }
            }

            @Override
            public void onCompleted() {
              finishLatch.countDown();
              if (channel != null) {
                GrpcUtils.closeChannel(channel);
              }
            }
          };

      stub.getBinary(request, responseObserver);

      if (!finishLatch.await(50, TimeUnit.MINUTES)) {
        logger.error("analyzeMultiple can not finish within 1 minutes");
      }

      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("getBinary e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /**
   *
   */
  public maum.brain.cl.train.Cltrainer.ClTrainStatus close(
      maum.brain.cl.train.Cltrainer.ClTrainKey request) throws InterruptedException {
    logger.info("==========TaGrpcInterfaceManager.close : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      ClassifierTrainerGrpc.ClassifierTrainerBlockingStub stub = ClassifierTrainerGrpc
          .newBlockingStub(channel);
      maum.brain.cl.train.Cltrainer.ClTrainStatus result = stub.close(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("close e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /**
   *
   */
  public maum.brain.cl.train.Cltrainer.ClTrainStatus stop(
      maum.brain.cl.train.Cltrainer.ClTrainKey request) throws InterruptedException {
    logger.info("==========TaGrpcInterfaceManager.stop : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      ClassifierTrainerGrpc.ClassifierTrainerBlockingStub stub = ClassifierTrainerGrpc
          .newBlockingStub(channel);
      maum.brain.cl.train.Cltrainer.ClTrainStatus result = stub.stop(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("stop e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /**
   *
   */
  public maum.brain.cl.train.Cltrainer.ClTrainStatusList getAllProgress(
      com.google.protobuf.Empty request) throws InterruptedException {
    logger.info("==========TaGrpcInterfaceManager.getAllProgress : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      ClassifierTrainerGrpc.ClassifierTrainerBlockingStub stub = ClassifierTrainerGrpc
          .newBlockingStub(channel);
      maum.brain.cl.train.Cltrainer.ClTrainStatusList result = stub.getAllProgress(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("getAllProgress e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /**
   *
   */
  public com.google.protobuf.Empty removeBinary(maum.brain.cl.train.Cltrainer.ClTrainKey request)
      throws InterruptedException {
    logger.info("==========TaGrpcInterfaceManager.removeBinary : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      ClassifierTrainerGrpc.ClassifierTrainerBlockingStub stub = ClassifierTrainerGrpc
          .newBlockingStub(channel);
      com.google.protobuf.Empty result = stub.removeBinary(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("removeBinary e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /*
    service TrainerMonitor {
      rpc Notify(TrainProcStatus) returns (google.protobuf.Empty);
    }
  */
  public com.google.protobuf.Empty notify(maum.brain.cl.train.Cltrainer.TrainProcStatus request)
      throws InterruptedException {
    logger.info("==========TaGrpcInterfaceManager.notify : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      TrainerMonitorGrpc.TrainerMonitorBlockingStub stub = TrainerMonitorGrpc
          .newBlockingStub(channel);
      com.google.protobuf.Empty result = stub.notify(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("notify e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /*
    service HmdClassifier {
      rpc SetModel (HmdModel) returns (google.protobuf.Empty);
      rpc SetMatrix (HmdModel) returns (google.protobuf.Empty);
      rpc GetModel (ModelKey) returns (HmdModel);
      rpc GetModels (google.protobuf.Empty) returns (HmdModelList);
      rpc GetClass (HmdInputDocument) returns (HmdResult);
      rpc GetClassMultiple (stream HmdInputDocument) returns (stream HmdResultDocument);
      rpc GetClassByText (HmdInputText) returns (HmdResultDocument);
      rpc GetClassMultipleByText (stream HmdInputText) returns (stream HmdResultDocument);
    }
  */
  public com.google.protobuf.Empty setModel(maum.brain.hmd.Hmd.HmdModel request)
      throws InterruptedException {
    logger.info("==========TaGrpcInterfaceManager.find : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      HmdClassifierGrpc.HmdClassifierBlockingStub stub = HmdClassifierGrpc.newBlockingStub(channel);
      com.google.protobuf.Empty result = stub.setModel(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("setModel e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  public com.google.protobuf.Empty setMatrix(maum.brain.hmd.Hmd.HmdModel request)
      throws InterruptedException {
    logger.info("==========TaGrpcInterfaceManager.HMD.setMatrix : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      HmdClassifierGrpc.HmdClassifierBlockingStub stub = HmdClassifierGrpc.newBlockingStub(channel);
      com.google.protobuf.Empty result = stub.setMatrix(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("setMatrix e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }


  /**
   *
   */
  public maum.brain.hmd.Hmd.HmdModel getModel(maum.brain.hmd.Hmd.ModelKey request)
      throws InterruptedException {
    logger.info("==========TaGrpcInterfaceManager.find : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      HmdClassifierGrpc.HmdClassifierBlockingStub stub = HmdClassifierGrpc.newBlockingStub(channel);
      maum.brain.hmd.Hmd.HmdModel result = stub.getModel(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("getModel e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /**
   *
   */
  public maum.brain.hmd.Hmd.HmdModelList hmdClassifierGetModels(com.google.protobuf.Empty request)
      throws InterruptedException {
    logger.info("==========TaGrpcInterfaceManager.hmdClassifierGetModels : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      HmdClassifierGrpc.HmdClassifierBlockingStub stub = HmdClassifierGrpc.newBlockingStub(channel);
      maum.brain.hmd.Hmd.HmdModelList result = stub.getModels(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("hmdClassifierGetModels e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /**
   *
   */
  public maum.brain.hmd.Hmd.HmdResult getClass(maum.brain.hmd.Hmd.HmdInputDocument request)
      throws InterruptedException {
    logger.info("==========TaGrpcInterfaceManager.HmdClassifier.getClass : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      HmdClassifierGrpc.HmdClassifierBlockingStub stub = HmdClassifierGrpc.newBlockingStub(channel);
      maum.brain.hmd.Hmd.HmdResult result = stub.getClass(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("getClass e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /**
   *
   */
  public maum.brain.hmd.Hmd.HmdResultDocument getClassByText(
      maum.brain.hmd.Hmd.HmdInputText request) throws InterruptedException {
    logger.error("==========TaGrpcInterfaceManager.getClassByText : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      HmdClassifierGrpc.HmdClassifierBlockingStub stub = HmdClassifierGrpc.newBlockingStub(channel);
      maum.brain.hmd.Hmd.HmdResultDocument result = stub.getClassByText(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("getClassByText e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

/*
  service NaturalLanguageProcessingService {
    rpc GetProvider (google.protobuf.Empty) returns (NlpProvider);
    rpc Analyze (InputText) returns (Document);
    rpc AnalyzeMultiple (stream InputText) returns (stream Document);
    rpc HasSupport (NlpFeatures) returns (NlpFeatureSupportList);
    rpc ApplyDict (NlpDict) returns (google.protobuf.Empty);
    rpc GetNamedEntityTagList(google.protobuf.Empty) returns (NamedEntityTagList);
    rpc GetPosTaggedList (Document) returns (TaggedList);
  }
*/

  public maum.brain.nlp.Nlp.NlpProvider getProvider(com.google.protobuf.Empty request)
      throws InterruptedException {
    logger.info("==========TaGrpcInterfaceManager.getProvider : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      NaturalLanguageProcessingServiceGrpc.NaturalLanguageProcessingServiceBlockingStub stub = NaturalLanguageProcessingServiceGrpc
          .newBlockingStub(channel);
      maum.brain.nlp.Nlp.NlpProvider result = stub.getProvider(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("getProvider e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /**
   * nlp Analyze
   *
   * @param request => {maum.brain.nlp.Nlp.InputText request}
   * @return => {maum.brain.nlp.Nlp.Document}
   */
  public maum.brain.nlp.Nlp.Document analyze(maum.brain.nlp.Nlp.InputText request)
      throws InterruptedException {
    logger.info("==========TaGrpcInterfaceManager.analyze : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      NaturalLanguageProcessingServiceBlockingStub stub = NaturalLanguageProcessingServiceGrpc
          .newBlockingStub(channel)
          .withWaitForReady();
      maum.brain.nlp.Nlp.Document result = stub.analyze(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("analyze e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  //  rpc AnalyzeMultiple (stream InputText) returns (stream Document);
  public List<Document> analyzeMultiple(List<String> texts, LangCode langCode,
      boolean splitSentence,
      boolean useTokenizer, NlpAnalysisLevel nlpAnalysisLevel,
      KeywordFrequencyLevel keywordFrequencyLevel) throws InterruptedException {
    logger.info("==========TaGrpcInterfaceManager.analyzeMultiple : ");
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      NaturalLanguageProcessingServiceGrpc.NaturalLanguageProcessingServiceStub stub = NaturalLanguageProcessingServiceGrpc
          .newStub(channel);
      final CountDownLatch finishLatch = new CountDownLatch(1);
      List<Document> result = new ArrayList<>();

      StreamObserver<Document> responseObserver = new StreamObserver<Document>() {
        @Override
        public void onNext(Document res) {
          logger.trace("#@ callEvent onNext :" + res.toString());
          result.add(res);
//          finishLatch.countDown();
        }

        @Override
        public void onError(Throwable t) {
          logger.error("#@ callEvent onError [{}]", t.toString());
          finishLatch.countDown();
          if (channel != null) {
            GrpcUtils.closeChannel(channel);
          }
        }

        @Override
        public void onCompleted() {
          logger.info("#@ callEvent Complete.");
          finishLatch.countDown();
          if (channel != null) {
            GrpcUtils.closeChannel(channel);
          }
        }
      };
      StreamObserver<InputText> requestObserver =
          stub.analyzeMultiple(responseObserver);

      try {
        for (String text : texts) {
          InputText.Builder inputText = InputText.newBuilder();
          inputText.setText(text);
          inputText.setLang(langCode);
          inputText.setSplitSentence(splitSentence);
          inputText.setUseTokenizer(useTokenizer);
//          inputText.setUseSpace(useSpace);
          inputText.setLevel(nlpAnalysisLevel);
          inputText.setKeywordFrequencyLevel(keywordFrequencyLevel);
          requestObserver.onNext(inputText.build());
        }
        requestObserver.onCompleted();
      } catch (RuntimeException e) {
        requestObserver.onError(e);
        throw e;
      }
      if (!finishLatch.await(50, TimeUnit.MINUTES)) {
        logger.error("analyzeMultiple can not finish within 1 minutes");
      }

      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("analyzeMultiple e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
    }
    return null;
  }

  /**
   * nlp AnalyzeWithSpace
   *
   * @param request => {maum.brain.nlp.Nlp.InputText request}
   * @return => {maum.brain.nlp.Nlp.Document}
   */
  public maum.brain.nlp.Nlp.Document analyzeWithSpace(maum.brain.nlp.Nlp.InputText request)
      throws InterruptedException {
    logger.info("==========TaGrpcInterfaceManager.analyzeWithSpace : {}", request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      NaturalLanguageProcessingServiceGrpc.NaturalLanguageProcessingServiceBlockingStub stub
          = NaturalLanguageProcessingServiceGrpc.newBlockingStub(channel);
      maum.brain.nlp.Nlp.Document result = stub.analyzeWithSpace(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        logger.error("() => ", e, e.getMessage());
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /**
   *
   */
  public maum.brain.nlp.Nlp.NlpFeatureSupportList hasSupport(
      maum.brain.nlp.Nlp.NlpFeatures request) throws InterruptedException {
    logger.info("==========TaGrpcInterfaceManager.hasSupport : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      NaturalLanguageProcessingServiceGrpc.NaturalLanguageProcessingServiceBlockingStub stub = NaturalLanguageProcessingServiceGrpc
          .newBlockingStub(channel);
      maum.brain.nlp.Nlp.NlpFeatureSupportList result = stub.hasSupport(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("hasSupport e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /**
   *
   */
  public com.google.protobuf.Empty applyDict(maum.brain.nlp.Nlp.NlpDict request)
      throws InterruptedException {
    logger.info("==========TaGrpcInterfaceManager.applyDict : {}", request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      NaturalLanguageProcessingServiceGrpc.NaturalLanguageProcessingServiceBlockingStub stub = NaturalLanguageProcessingServiceGrpc
          .newBlockingStub(channel);
      com.google.protobuf.Empty result = stub.applyDict(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("applyDict e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  public com.google.protobuf.Empty updatePreNerDict(maum.brain.nlp.Nlp.NerDict request)
      throws InterruptedException {
    logger.info("start : {}", request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      NaturalLanguageProcessingServiceGrpc.NaturalLanguageProcessingServiceBlockingStub stub = NaturalLanguageProcessingServiceGrpc
          .newBlockingStub(channel);
      com.google.protobuf.Empty result = stub.updatePreNerDict(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        logger.error("updatePreNerDict e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  public com.google.protobuf.Empty updatePostNerDict(maum.brain.nlp.Nlp.NerDict request)
      throws InterruptedException {
    logger.info("start : {}", request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      NaturalLanguageProcessingServiceGrpc.NaturalLanguageProcessingServiceBlockingStub stub = NaturalLanguageProcessingServiceGrpc
          .newBlockingStub(channel);
      com.google.protobuf.Empty result = stub.updatePostNerDict(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        logger.error("updatePostNerDict e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  public com.google.protobuf.Empty updatePostChangeNerDict(
      maum.brain.nlp.Nlp.ChangedNerDict request)
      throws InterruptedException {
    logger.info("start : {}", request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      NaturalLanguageProcessingServiceGrpc.NaturalLanguageProcessingServiceBlockingStub stub = NaturalLanguageProcessingServiceGrpc
          .newBlockingStub(channel);
      com.google.protobuf.Empty result = stub.updatePostChangeNerDict(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        logger.error("updatePostChangeNerDict e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  public com.google.protobuf.Empty updateNerDict(
      maum.brain.nlp.Nlp3Custom.NamedEntityDictList request) throws InterruptedException {
    logger.info("start : {}", request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      Nlp3CustomizeServiceGrpc.Nlp3CustomizeServiceBlockingStub stub = Nlp3CustomizeServiceGrpc
          .newBlockingStub(channel);
      com.google.protobuf.Empty result = stub.updateNamedEntityDictList(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        logger.error("updateNerDict e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  public com.google.protobuf.Empty updatePosDict(
      maum.brain.nlp.Nlp3Custom.MorpDictList request) throws InterruptedException {
    logger.info("start : {}", request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      Nlp3CustomizeServiceGrpc.Nlp3CustomizeServiceBlockingStub stub = Nlp3CustomizeServiceGrpc
          .newBlockingStub(channel);
      com.google.protobuf.Empty result = stub.updateMorpDictList(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        logger.error("updatePosDict e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  public com.google.protobuf.Empty updateReplacementDict(
      maum.brain.nlp.Nlp3Custom.ReplacementDictList request) throws Exception {
    logger.info("start : {}", request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      Nlp3CustomizeServiceGrpc.Nlp3CustomizeServiceBlockingStub stub = Nlp3CustomizeServiceGrpc
          .newBlockingStub(channel);
      com.google.protobuf.Empty result = stub.updateReplacementDict(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("RPC failed: {}", e.getStatus());
      throw e;
    } finally {
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
    }
  }

  /**
   *
   */
  public maum.brain.nlp.Nlp.NamedEntityTagList getNamedEntityTagList(
      com.google.protobuf.Empty request) throws InterruptedException {
    logger.info("==========TaGrpcInterfaceManager.getNamedEntityTagList : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      NaturalLanguageProcessingServiceGrpc.NaturalLanguageProcessingServiceBlockingStub stub = NaturalLanguageProcessingServiceGrpc
          .newBlockingStub(channel);
      maum.brain.nlp.Nlp.NamedEntityTagList result = stub.getNamedEntityTagList(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("getNamedEntityTagList e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }


  /*
    service SplitSentenceService {
      rpc Split(Text) returns (TextArray);
    }
  */
  public maum.brain.nlp.Nlp.TextArray split(maum.brain.nlp.Nlp.Text request)
      throws InterruptedException {
    logger.info("==========TaGrpcInterfaceManager.split : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      SplitSentenceServiceGrpc.SplitSentenceServiceBlockingStub stub = SplitSentenceServiceGrpc
          .newBlockingStub(channel);
      maum.brain.nlp.Nlp.TextArray result = stub.split(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("split e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /*
    service WordEmbeddingService {
      rpc GetWordEmbedding (InputText) returns (WordEmbeddingDocument);
    }
  */
  public maum.brain.we.Wordembedding.WordEmbeddingDocument getWordEmbedding(
      maum.brain.we.Wordembedding.WordEmbeddingInputText request) throws InterruptedException {
    logger.info("==========TaGrpcInterfaceManager.split : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      WordEmbeddingServiceGrpc.WordEmbeddingServiceBlockingStub stub = WordEmbeddingServiceGrpc
          .newBlockingStub(channel);
      maum.brain.we.Wordembedding.WordEmbeddingDocument result = stub.getWordEmbedding(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("getWordEmbedding e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }


  /*
    service HanTrainer{
      rpc Open(HanModel) returns (HanTrainKey);
      rpc GetProgress(HanTrainKey) returns(HanTrainStatus);
      rpc GetBinary(HanTrainKey) returns (stream HanTrainBinary);
      rpc Close(HanTrainKey) returns (HanTrainStatus);
      rpc Stop(HanTrainKey) returns (HanTrainStatus);
      rpc GetAllProgress(google.protobuf.Empty) returns (HanTrainStatusList);
      rpc RemoveBinary(HanTrainKey) returns (google.protobuf.Empty);
    }
  */
  public maum.brain.han.train.HanTrain.HanTrainKey open(
      maum.brain.han.train.HanTrain.HanModel request) throws InterruptedException {
    logger.info("==========TaGrpcInterfaceManager.open : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      HanTrainerGrpc.HanTrainerBlockingStub stub = HanTrainerGrpc.newBlockingStub(channel)
          .withWaitForReady().withDeadlineAfter(5, TimeUnit.SECONDS);
      maum.brain.han.train.HanTrain.HanTrainKey result = stub.open(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("split e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /**
   *
   */
  public maum.brain.han.train.HanTrain.HanTrainStatus getProgress(
      maum.brain.han.train.HanTrain.HanTrainKey request) {
    logger.info("==========TaGrpcInterfaceManager.getProgress : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      HanTrainerGrpc.HanTrainerBlockingStub stub = HanTrainerGrpc.newBlockingStub(channel).withWaitForReady();
      maum.brain.han.train.HanTrain.HanTrainStatus result = stub.getProgress(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("split e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /**
   *
   */
  public List<maum.brain.han.train.HanTrain.HanTrainBinary> getBinary(
      maum.brain.han.train.HanTrain.HanTrainKey request) throws InterruptedException {
    logger.info("==========TaGrpcInterfaceManager.getBinary : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      HanTrainerGrpc.HanTrainerStub stub = HanTrainerGrpc.newStub(channel).withWaitForReady();
      List<maum.brain.han.train.HanTrain.HanTrainBinary> result = new ArrayList<>();
      final CountDownLatch finishLatch = new CountDownLatch(1);

      io.grpc.stub.StreamObserver<maum.brain.han.train.HanTrain.HanTrainBinary> responseObserver =
          new io.grpc.stub.StreamObserver<maum.brain.han.train.HanTrain.HanTrainBinary>() {
            @Override
            public void onNext(maum.brain.han.train.HanTrain.HanTrainBinary value) {
              logger.info("A message received from server: {}", value);
//              result.setTrainId(value.getTrainId());
              result.add(value);
            }

            @Override
            public void onError(Throwable t) {
              logger.error("RecordRoute Failed: {}", io.grpc.Status.fromThrowable(t));
              finishLatch.countDown();
              if (channel != null) {
                GrpcUtils.closeChannel(channel);
              }
            }

            @Override
            public void onCompleted() {
              finishLatch.countDown();
              if (channel != null) {
                GrpcUtils.closeChannel(channel);
              }
            }
          };

      stub.getBinary(request, responseObserver);

      if (!finishLatch.await(50, TimeUnit.MINUTES)) {
        logger.error("analyzeMultiple can not finish within 1 minutes");
      }

      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("split e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /**
   *
   */
  public maum.brain.han.train.HanTrain.HanTrainStatus close(
      maum.brain.han.train.HanTrain.HanTrainKey request) throws InterruptedException {
    logger.info("==========TaGrpcInterfaceManager.close : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      HanTrainerGrpc.HanTrainerBlockingStub stub = HanTrainerGrpc.newBlockingStub(channel).withWaitForReady();
      maum.brain.han.train.HanTrain.HanTrainStatus result = stub.close(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("split e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /**
   *
   */
  public maum.brain.han.train.HanTrain.HanTrainStatus stop(
      maum.brain.han.train.HanTrain.HanTrainKey request) throws InterruptedException {
    logger.info("==========TaGrpcInterfaceManager.stop : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      HanTrainerGrpc.HanTrainerBlockingStub stub = HanTrainerGrpc.newBlockingStub(channel).withWaitForReady();
      maum.brain.han.train.HanTrain.HanTrainStatus result = stub.stop(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("split e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /**
   * DNN rpc와 구분하기위해 이름 getXdcAllProgress로 변경
   */
  public maum.brain.han.train.HanTrain.HanTrainStatusList getXdcAllProgress(
      com.google.protobuf.Empty request) throws InterruptedException {
    logger.info("==========TaGrpcInterfaceManager.getAllProgress : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      HanTrainerGrpc.HanTrainerBlockingStub stub = HanTrainerGrpc.newBlockingStub(channel).withWaitForReady();
      maum.brain.han.train.HanTrain.HanTrainStatusList result = stub.getAllProgress(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("split e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /**
   *
   */
  public com.google.protobuf.Empty removeBinary(maum.brain.han.train.HanTrain.HanTrainKey request)
      throws InterruptedException {
    logger.info("==========TaGrpcInterfaceManager.removeBinary : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      HanTrainerGrpc.HanTrainerBlockingStub stub = HanTrainerGrpc.newBlockingStub(channel)
          .withWaitForReady().withDeadlineAfter(5, TimeUnit.SECONDS);
      com.google.protobuf.Empty result = stub.removeBinary(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("split e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /**
   * service HanClassifier{ rpc GetClass(HanInputDocument) returns (HanResult); rpc
   * GetClassByText(HanInputText) returns (HanResult); rpc GetLabelSimilarityList(google.protobuf.Empty)
   * returns (LabelSimilarityList); rpc GetStructuredFeatureList(google.protobuf.Empty) returns
   * (StructuredFeatureList); rpc EmptyCache(google.protobuf.Empty) returns (google.protobuf.Empty);
   * rpc GetLabelList(google.protobuf.Empty) returns (LabelList); }
   */
  public HanResult getClass(HanInputDocument request)
      throws InterruptedException {
    logger.error("==========TaGrpcInterfaceManager.HanClassifier.getClass : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      HanClassifierBlockingStub stub = HanClassifierGrpc
          .newBlockingStub(channel)
          .withWaitForReady();
      HanResult result = stub.getClass(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("getClass e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }
}
