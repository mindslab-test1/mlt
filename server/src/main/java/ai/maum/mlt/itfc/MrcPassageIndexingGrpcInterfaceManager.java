package ai.maum.mlt.itfc;

import ai.maum.mlt.common.util.GrpcUtils;
import io.grpc.ManagedChannel;
import io.grpc.StatusRuntimeException;
import maum.brain.qa.passage.PassageSearchServiceGrpc;
import org.slf4j.LoggerFactory;

public class MrcPassageIndexingGrpcInterfaceManager {


  static final org.slf4j.Logger logger = LoggerFactory.getLogger(MrcGrpcInterfaceManager.class);

  private String host;
  private int port;

  public MrcPassageIndexingGrpcInterfaceManager(String host, int port) {
    this.host = host;
    this.port = port;
  }

  /**
   * int32 ntop = 1;			//best top 몇개? result의 갯수 Type type = 10;			//default : ALL, DB, Search
   * string skill = 20;			//지금은 Not Use...향후  Use... Workspace 개념 string question = 30;			//질문
   * map<string, string> meta = 40;	//reserve....
   */
  public maum.brain.qa.passage.PassageSearch.PassageResponse findPassage(maum.brain.qa.passage.PassageSearch.PassageRequest request)
      throws InterruptedException {
    logger.info("==========MrcPassageSearchGrpcInterfaceManager.findPassage : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      PassageSearchServiceGrpc.PassageSearchServiceBlockingStub stub = PassageSearchServiceGrpc.newBlockingStub(channel);
      maum.brain.qa.passage.PassageSearch.PassageResponse result = stub.findPassage(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("findPassage e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /**
   * bool clean = 10;		// true , false ... false가 안전함 string target_date = 20;	// yyyyMMddHH24mmss :
   * 넣지않는다
   */
  public maum.brain.qa.passage.PassageSearch.IndexingStatus fullIndexing(
      maum.brain.qa.passage.PassageSearch.IndexingInput request) throws InterruptedException {
    logger.info("==========MrcPassageSearchGrpcInterfaceManager.fullIndexing : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      PassageSearchServiceGrpc.PassageSearchServiceBlockingStub stub = PassageSearchServiceGrpc.newBlockingStub(channel);
      maum.brain.qa.passage.PassageSearch.IndexingStatus result = stub.fullIndexing(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("fullIndexing e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /**
   * bool clean = 10;		// true , false ... false가 안전함 string target_date = 20;	// yyyyMMddHH24mmss :
   * 넣지않는다
   */
  public maum.brain.qa.passage.PassageSearch.IndexingStatus additionalIndexing(
      maum.brain.qa.passage.PassageSearch.IndexingInput request) throws InterruptedException {
    logger.info("==========MrcPassageSearchGrpcInterfaceManager.additionalIndexing : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      PassageSearchServiceGrpc.PassageSearchServiceBlockingStub stub = PassageSearchServiceGrpc.newBlockingStub(channel);
      maum.brain.qa.passage.PassageSearch.IndexingStatus result = stub.additionalIndexing(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("additionalIndexing e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /**
   */
  public maum.brain.qa.passage.PassageSearch.IndexingStatus getIndexingStatus(com.google.protobuf.Empty request)
      throws InterruptedException {
    logger.info("==========MrcPassageSearchGrpcInterfaceManager.getIndexingStatus : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      PassageSearchServiceGrpc.PassageSearchServiceBlockingStub stub = PassageSearchServiceGrpc.newBlockingStub(channel);
      maum.brain.qa.passage.PassageSearch.IndexingStatus result = stub.getIndexingStatus(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("getIndexingStatus e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /**
   * 색인중지
   */
  public maum.brain.qa.passage.PassageSearch.IndexingStatus abortIndexing(com.google.protobuf.Empty request)
      throws InterruptedException {
    logger.info("==========MrcPassageSearchGrpcInterfaceManager.abortIndexing : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      PassageSearchServiceGrpc.PassageSearchServiceBlockingStub stub = PassageSearchServiceGrpc.newBlockingStub(channel);
      maum.brain.qa.passage.PassageSearch.IndexingStatus result = stub.abortIndexing(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("abortIndexing e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /**
   * 표현식은 아래와 같이 설정 ex) cronExpression: * * * * * ? 초(Seconds): 0-59, * 분(Minutes): 0-59, *
   * 시(Hours): 0-23, * 일(Day-of-Month): 1-31, * (?로 대체 가능) 월(Months): 1-12, * 요일(Days-of-Week): 1-7,
   * * (?로 대체 가능) 연도(Year) - optional: 1979-2099 (생략 가능)
   */
  public maum.brain.qa.passage.PassageSearch.ScheduleInfo setSchedule(
      maum.brain.qa.passage.PassageSearch.ScheduleInfo request) {
    logger.info("==========MrcPassageSearchGrpcInterfaceManager.setSchedule : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      PassageSearchServiceGrpc.PassageSearchServiceBlockingStub stub = PassageSearchServiceGrpc.newBlockingStub(channel);
      maum.brain.qa.passage.PassageSearch.ScheduleInfo result = stub.setSchedule(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("setSchedule e1 : " , e1);
      }
      return null;
    }
  }

  /**
   */
  public maum.brain.qa.passage.PassageSearch.ScheduleInfo getSchedule(com.google.protobuf.Empty request) {
    logger.info("==========MrcPassageSearchGrpcInterfaceManager.getSchedule : " + request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      PassageSearchServiceGrpc.PassageSearchServiceBlockingStub stub = PassageSearchServiceGrpc.newBlockingStub(channel);
      maum.brain.qa.passage.PassageSearch.ScheduleInfo result = stub.getSchedule(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("getSchedule e1 : " , e1);
      }
      return null;
    }
  }


}
