package ai.maum.mlt.itfc;

import ai.maum.mlt.common.util.GrpcUtils;
import ai.maum.mlt.stt.entity.STTTranscriptEntity;
import com.google.protobuf.ByteString;
import io.grpc.ManagedChannel;
import io.grpc.Metadata;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.MetadataUtils;
import io.grpc.stub.StreamObserver;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import maum.brain.stt.Stt.SetModelResponse;
import maum.brain.stt.Stt.Speech;
import maum.brain.stt.SttModelResolverGrpc;
import maum.brain.stt.SttRealServiceGrpc;
import maum.brain.stt.train.Eval.EvalRequest;
import maum.brain.stt.train.Eval.EvalResult;
import maum.brain.stt.train.S3Train.DeepLearningType;
import maum.brain.stt.train.S3Train.SttStreamModel;
import maum.brain.stt.train.S3Train.SttStreamModel.SttModelParam;
import maum.brain.stt.train.S3Train.TrainModelType;
import maum.brain.stt.train.SttEvalModelResolverGrpc;
import maum.brain.stt.train.SttTrainerGrpc;
import maum.brain.stt.train.TrainerMonitorGrpc;
import maum.common.LangOuterClass.LangCode;
import maum.common.Types.FilePart;
import org.slf4j.LoggerFactory;

public class SttGrpcInterfaceManager {

  static final org.slf4j.Logger logger = LoggerFactory.getLogger(SttGrpcInterfaceManager.class);

  private String host;
  private int port;

  public SttGrpcInterfaceManager(String host, int port) {
    this.host = host;
    this.port = port;
  }

  /* s3evaluation.proto
    service SttEvalModelResolver {
      rpc RequestEvaluation (EvalRequest) returns (EvalResult)
    }
 */
  public EvalResult requestEvaluation(EvalRequest request) throws InterruptedException {
    logger.debug("===== SttEvalModelResolver requestEvaluation :: EvalRequest {}", request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      SttEvalModelResolverGrpc.SttEvalModelResolverBlockingStub stub = SttEvalModelResolverGrpc
          .newBlockingStub(channel);
      EvalResult result = stub.requestEvaluation(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== SttEvalModelResolver requestEvaluation :: RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("requestEvaluation e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /* s3train.proto
    service SttTrainer {
      rpc Open(SttModel) returns (TrainKey);
      rpc OpenStream (stream SttModel) returns (TrainKey);
      rpc GetProgress (TrainKey) returns(TrainStatus);
      rpc GetBinary (TrainKey) returns (stream TrainBinary);
      rpc Close (TrainKey) returns (TrainStatus);
      rpc Stop (TrainKey) returns (TrainStatus);
      rpc GetAllProgress (google.protobuf.Empty) returns (TrainStatusList);
      rpc RemoveBinary (TrainKey) returns (google.protobuf.Empty);
      rpc RemakeTrainModel (RemakeTrainModelReq) returns (RemakeTrainModelRes);
    }
 */
  public maum.brain.stt.train.S3Train.TrainKey open(maum.brain.stt.train.S3Train.SttModel request)
      throws InterruptedException {
    logger.debug("===== SttTrainer open :: {}", request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      SttTrainerGrpc.SttTrainerBlockingStub stub = SttTrainerGrpc.newBlockingStub(channel);
      maum.brain.stt.train.S3Train.TrainKey result = stub.open(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== SttTrainer open :: RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("open e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }


  public maum.brain.stt.train.S3Train.TrainKey openStream(String model,
      TrainModelType trainModelType,
      DeepLearningType deepLearningType, LangCode lang, int sampleRate,
      List<STTTranscriptEntity> sttTranscriptEntities, String callBackUrl, String filePath)
      throws InterruptedException {
    logger.debug(
        "====== SttTrainer openStream :: model {}, trainModelType {}, deepLearningType {}, lang {}, sampleRate {}, callBackUrl {}, filePath {}",
        model, trainModelType, deepLearningType, lang, sampleRate, callBackUrl, filePath);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      SttTrainerGrpc.SttTrainerStub stub = SttTrainerGrpc.newStub(channel);
      final CountDownLatch finishLatch = new CountDownLatch(1);
      maum.brain.stt.train.S3Train.TrainKey.Builder result = maum.brain.stt.train.S3Train.TrainKey
          .newBuilder();

      io.grpc.stub.StreamObserver<maum.brain.stt.train.S3Train.TrainKey> responseObserver =
          new io.grpc.stub.StreamObserver<maum.brain.stt.train.S3Train.TrainKey>() {
            @Override
            public void onNext(maum.brain.stt.train.S3Train.TrainKey value) {
              logger.debug("====== SttTrainer openStream :: A message received from server: {}",
                  value);
              result.setTrainId(value.getTrainId());
            }

            @Override
            public void onError(Throwable t) {
              logger.error("====== SttTrainer openStream :: RecordRoute Failed: {}",
                  io.grpc.Status.fromThrowable(t));
              finishLatch.countDown();
              if (channel != null) {
                GrpcUtils.closeChannel(channel);
              }
            }

            @Override
            public void onCompleted() {
              finishLatch.countDown();
              if (channel != null) {
                GrpcUtils.closeChannel(channel);
              }
            }
          };

      StreamObserver<SttStreamModel> requestObserver =
          stub.openStream(responseObserver);

      SttStreamModel.Builder sttStreamModel = SttStreamModel.newBuilder();

      SttModelParam.Builder sttModelParam = SttModelParam.newBuilder();
      sttModelParam.setModel(model);
      sttModelParam.setTrainModelType(trainModelType);
      sttModelParam.setDeepLearningType(deepLearningType);
      sttModelParam.setLang(lang);
      sttModelParam.setCallbackUrl(callBackUrl);
      sttModelParam.setSampleRate(sampleRate);
      sttStreamModel.setParam(sttModelParam);
      requestObserver.onNext(sttStreamModel.build());

      if (trainModelType.equals(TrainModelType.TRAIN_MODEL_LM)) {
        for (STTTranscriptEntity sttTranscriptEntity : sttTranscriptEntities) {
          maum.brain.stt.train.S3Train.TextFile.Builder textFile = maum.brain.stt.train.S3Train.TextFile
              .newBuilder();
          textFile.setFilename(sttTranscriptEntity.getId());
          textFile.setTranscript(sttTranscriptEntity.getVersion());
          sttStreamModel.setTextFiles(textFile);
          requestObserver.onNext(sttStreamModel.build());
        }
      } else {
        for (STTTranscriptEntity sttTranscriptEntity : sttTranscriptEntities) {
          // am
          maum.brain.stt.train.S3Train.AudioFile.Builder audioFile = maum.brain.stt.train.S3Train.AudioFile
              .newBuilder();
          audioFile.setFilename(filePath + "/" + sttTranscriptEntity.getFileId() + ".wav");
          audioFile.setFormat(sttTranscriptEntity.getFileEntity().getType().split("/")[1]);
          audioFile.setTranscript(sttTranscriptEntity.getVersion());
          sttStreamModel.setAudioFiles(audioFile);
          requestObserver.onNext(sttStreamModel.build());
        }
      }
      logger.debug("===== SttTrainer openStream :: requestObserver onCompleted called");
      requestObserver.onCompleted();

      if (!finishLatch.await(60, TimeUnit.MINUTES)) {
        logger.error("===== SttTrainer openStream :: can not finish within 60 minutes");
      }

      return result.build();
    } catch (StatusRuntimeException e) {
      logger.error("===== SttTrainer openStream :: RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("openStream e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  public maum.brain.stt.train.S3Train.TrainStatus getProgress(
      maum.brain.stt.train.S3Train.TrainKey request) throws InterruptedException {
    logger.debug("===== SttTrainer getProgress :: TrainKey {}", request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      SttTrainerGrpc.SttTrainerBlockingStub stub = SttTrainerGrpc.newBlockingStub(channel);
      maum.brain.stt.train.S3Train.TrainStatus result = stub.getProgress(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== SttTrainer getProgress :: RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("getProgress e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  public List<maum.brain.stt.train.S3Train.TrainBinary> getBinary(
      maum.brain.stt.train.S3Train.TrainKey request) throws InterruptedException {
    logger.debug("===== SttTrainer getBinary :: request {}", request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      SttTrainerGrpc.SttTrainerStub stub = SttTrainerGrpc.newStub(channel);
      List<maum.brain.stt.train.S3Train.TrainBinary> result = new ArrayList<>();
      final CountDownLatch finishLatch = new CountDownLatch(1);

      io.grpc.stub.StreamObserver<maum.brain.stt.train.S3Train.TrainBinary> responseObserver =
          new io.grpc.stub.StreamObserver<maum.brain.stt.train.S3Train.TrainBinary>() {
            @Override
            public void onNext(maum.brain.stt.train.S3Train.TrainBinary value) {
              logger
                  .debug("===== SttTrainer getBinary :: A message received from server: {}", value);
              result.add(value);
            }

            @Override
            public void onError(Throwable t) {
              logger.error("===== SttTrainer getBinary :: RecordRoute Failed: {}",
                  io.grpc.Status.fromThrowable(t));
              finishLatch.countDown();
              if (channel != null) {
                GrpcUtils.closeChannel(channel);
              }
            }

            @Override
            public void onCompleted() {
              logger.debug("===== SttTrainer getBinary :: responseObserver onCompleted called");
              finishLatch.countDown();
              if (channel != null) {
                GrpcUtils.closeChannel(channel);
              }
            }
          };

      stub.getBinary(request, responseObserver);

      if (!finishLatch.await(50, TimeUnit.MINUTES)) {
        logger.error("===== SttTrainer getBinary :: getBinary can not finish within 50 minutes");
      }

      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== SttTrainer getBinary :: RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("getBinary e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  public maum.brain.stt.train.S3Train.TrainStatus close(
      maum.brain.stt.train.S3Train.TrainKey request) throws InterruptedException {
    logger.info("===== SttTrainer close :: TrainKey {} ", request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      SttTrainerGrpc.SttTrainerBlockingStub stub = SttTrainerGrpc.newBlockingStub(channel);
      maum.brain.stt.train.S3Train.TrainStatus result = stub.close(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== SttTrainer close :: RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("close e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  public maum.brain.stt.train.S3Train.TrainStatus stop(
      maum.brain.stt.train.S3Train.TrainKey request) throws InterruptedException {
    logger.debug("===== SttTrainer stop :: trainKey {}", request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      SttTrainerGrpc.SttTrainerBlockingStub stub = SttTrainerGrpc.newBlockingStub(channel);
      maum.brain.stt.train.S3Train.TrainStatus result = stub.stop(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== SttTrainer stop :: RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("stop e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  public maum.brain.stt.train.S3Train.TrainStatusList getAllProgress(
      com.google.protobuf.Empty request) throws InterruptedException {
    logger.debug("===== SttTrainer getAllProgress");
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      SttTrainerGrpc.SttTrainerBlockingStub stub = SttTrainerGrpc.newBlockingStub(channel);
      maum.brain.stt.train.S3Train.TrainStatusList result = stub.getAllProgress(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== SttTrainer getAllProgress :: RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("getAllProgress e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  public com.google.protobuf.Empty removeBinary(maum.brain.stt.train.S3Train.TrainKey request)
      throws InterruptedException {
    logger.debug("===== SttTrainer removeBinary :: {}", request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      SttTrainerGrpc.SttTrainerBlockingStub stub = SttTrainerGrpc.newBlockingStub(channel);
      com.google.protobuf.Empty result = stub.removeBinary(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== SttTrainer removeBinary :: RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("removeBinary e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /*
      service TrainerMonitor {
        rpc Notify(TrainProcStatus) returns (google.protobuf.Empty);
      }
    */
  public com.google.protobuf.Empty notify(maum.brain.stt.train.S3Train.TrainProcStatus request)
      throws InterruptedException {
    logger.debug("===== TrainerMonitor notify :: {}", request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      TrainerMonitorGrpc.TrainerMonitorBlockingStub stub = TrainerMonitorGrpc
          .newBlockingStub(channel);
      com.google.protobuf.Empty result = stub.notify(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== TrainerMonitor notify :: RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("notify e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /* stt.proto */
/*
  service SpeechToTextService {
    rpc SimpleRecognize (stream Speech) returns (Text);
    rpc DetailRecognize (stream Speech) returns (DetailText);
    rpc StreamRecognize (stream Speech) returns (stream Segment);
  }
*/
/*
  service SttModelResolver {
    rpc Find (Model) returns (ServerStatus);
    rpc GetModels (google.protobuf.Empty) returns (ModelList);
    rpc GetServers (google.protobuf.Empty) returns (ServerStatusList);
    rpc Stop (Model) returns (ServerStatus);
    rpc Restart (Model) returns (ServerStatus);
    rpc Ping (Model) returns (ServerStatus);
    rpc SetModel (stream minds.FilePart) returns (SetModelResponse);
    rpc DeleteModel (Model) returns (ServerStatus);
  }
*/

  public maum.brain.stt.Stt.ServerStatus find(maum.brain.stt.Stt.Model request)
      throws InterruptedException {
    logger.info("====== SttModelResolver find :: Model {}", request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      SttModelResolverGrpc.SttModelResolverBlockingStub stub = SttModelResolverGrpc
          .newBlockingStub(channel);
      maum.brain.stt.Stt.ServerStatus result = stub.find(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== SttModelResolver find :: RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("find e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /**
   */
  public maum.brain.stt.Stt.ModelList getModels(com.google.protobuf.Empty request)
      throws InterruptedException {
    logger.debug("===== SttModelResolver getModels");
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      SttModelResolverGrpc.SttModelResolverBlockingStub stub = SttModelResolverGrpc
          .newBlockingStub(channel);
      maum.brain.stt.Stt.ModelList result = stub.getModels(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== SttModelResolver getModels :: RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("getModels e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /**
   */
  public maum.brain.stt.Stt.ServerStatusList getServers(com.google.protobuf.Empty request)
      throws InterruptedException {
    logger.debug("===== SttModelResolver getServers");
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      SttModelResolverGrpc.SttModelResolverBlockingStub stub = SttModelResolverGrpc
          .newBlockingStub(channel);
      maum.brain.stt.Stt.ServerStatusList result = stub.getServers(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== SttModelResolver getServers :: RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("getServers e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /**
   */
  public maum.brain.stt.Stt.ServerStatus stop(maum.brain.stt.Stt.Model request)
      throws InterruptedException {
    logger.debug("===== SttModelResolver stop :: {}", request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      SttModelResolverGrpc.SttModelResolverBlockingStub stub = SttModelResolverGrpc
          .newBlockingStub(channel);
      maum.brain.stt.Stt.ServerStatus result = stub.stop(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== SttModelResolver stop :: RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("stop e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /**
   */
  public maum.brain.stt.Stt.ServerStatus restart(maum.brain.stt.Stt.Model request)
      throws InterruptedException {
    logger.debug("===== SttModelResolver restart :: {}", request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      SttModelResolverGrpc.SttModelResolverBlockingStub stub = SttModelResolverGrpc
          .newBlockingStub(channel);
      maum.brain.stt.Stt.ServerStatus result = stub.restart(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== SttModelResolver restart :: RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("restart e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /**
   */
  public maum.brain.stt.Stt.ServerStatus ping(maum.brain.stt.Stt.Model request)
      throws InterruptedException {
    logger.debug("===== SttModelResolver ping :: {}", request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      SttModelResolverGrpc.SttModelResolverBlockingStub stub = SttModelResolverGrpc
          .newBlockingStub(channel);
      maum.brain.stt.Stt.ServerStatus result = stub.ping(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== SttModelResolver ping :: RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("ping e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }


  public List<SetModelResponse> setModel(File clientBinary, Metadata metadata)
      throws InterruptedException, IOException {
    logger.debug("===== SttModelResolver setModel :: metadata {}, clientBinary {}", metadata,
        clientBinary.getName());
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      SttModelResolverGrpc.SttModelResolverStub stub = SttModelResolverGrpc.newStub(channel);
      stub = MetadataUtils.attachHeaders(stub, metadata);

      List<SetModelResponse> setModelResponses = new ArrayList<>();
      final CountDownLatch finishLatch = new CountDownLatch(1);
      io.grpc.stub.StreamObserver<SetModelResponse> responseObserver =
          new io.grpc.stub.StreamObserver<SetModelResponse>() {
            @Override
            public void onNext(SetModelResponse value) {
              logger.debug("===== SttModelResolver setModel :: A message received from server: {}",
                  value);
              setModelResponses.add(value);
            }

            @Override
            public void onError(Throwable t) {
              logger.error("===== SttModelResolver setModel :: RecordRoute Failed: {}",
                  io.grpc.Status.fromThrowable(t));
              finishLatch.countDown();
              if (channel != null) {
                GrpcUtils.closeChannel(channel);
              }
            }

            @Override
            public void onCompleted() {
              logger
                  .debug("===== SttModelResolver setModel :: responseObserver oncompleted called");
              finishLatch.countDown();
              if (channel != null) {
                GrpcUtils.closeChannel(channel);
              }
            }
          };

      io.grpc.stub.StreamObserver<maum.common.Types.FilePart> requestObserver =
          stub.setModel(responseObserver);
      FileInputStream fis = null;
      try {
        fis = new FileInputStream(clientBinary);
        byte[] buffer = new byte[40960];
        while ((fis.read(buffer)) > 0) {
          FilePart filePart = FilePart.newBuilder().setPart(ByteString.copyFrom(buffer)).build();
          requestObserver.onNext(filePart);
        }
        // 메시지 전송이 완료되면 onCompleted()를 호출하여 서버에 종료 상황을 알립니다.
        logger.debug("===== SttModelResolver setModel :: requestObserver oncompleted called");
        requestObserver.onCompleted();
      } catch (RuntimeException e) {
        logger
            .error("===== SttModelResolver setModel :: requestObserver onerror {}", e.getMessage());
        requestObserver.onError(e);
        throw e;
      }
      if (!finishLatch.await(3, TimeUnit.HOURS)) {
        logger.error("===== SttModelResolver setModel :: Can not finish within 3 hours");
      }

      return setModelResponses;
    } catch (StatusRuntimeException e) {
      logger.error("===== SttModelResolver setModel :: RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("setModel e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
    }
    return null;
  }


  /**
   */
  public maum.brain.stt.Stt.ServerStatus deleteModel(maum.brain.stt.Stt.Model request)
      throws InterruptedException {
    logger.debug("===== SttModelResolver deleteModel :: {}", request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      SttModelResolverGrpc.SttModelResolverBlockingStub stub = SttModelResolverGrpc
          .newBlockingStub(channel);
      maum.brain.stt.Stt.ServerStatus result = stub.deleteModel(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== SttModelResolver deleteModel :: RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("deleteModel e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  /*
service SttRealService {
  rpc Ping(Model) returns (ServerStatus);
  rpc SimpleRecognize (stream Speech) returns (Text);
  rpc DetailRecognize (stream Speech) returns (DetailText);
  rpc StreamRecognize (stream Speech) returns (stream Segment);
}
*/
  public maum.brain.stt.Stt.ServerStatus sttRealServicePing(maum.brain.stt.Stt.Model request)
      throws InterruptedException {
    logger.debug("===== SttRealService ping :: {}", request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      SttRealServiceGrpc.SttRealServiceBlockingStub stub = SttRealServiceGrpc
          .newBlockingStub(channel);
      maum.brain.stt.Stt.ServerStatus result = stub.ping(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== SttRealService ping :: RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("sttRealServicePing e1 : " , e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }
  /*public void simpleRecognize(io.grpc.stub.StreamObserver<maum.brain.stt.Stt.Speech> request) {
    logger.info("==========SttGrpcInterfaceManager.ping : " + request);
    try {
      return sttRealServiceStub.simpleRecognize(request);
    } catch (StatusRuntimeException e) {
      logger.warn("RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("simpleRecognize e1 : " , e1);
      }
      return null;
    }
  }*/

/*
  public io.grpc.stub.StreamObserver<maum.brain.stt.Stt.Speech> simpleRecognize(
      io.grpc.stub.StreamObserver<maum.brain.stt.Stt.Text> responseObserver) {
    return asyncClientStreamingCall(
        getChannel().newCall(getSimpleRecognizeMethod(), getCallOptions()), responseObserver);
  }*/

  /**
   * client-to-server streaming RPC
   */
  public maum.brain.stt.Stt.Text simpleRecognize(File file) throws InterruptedException {
    logger.debug("===== SttRealService simpleRecognize :: {}", file.getName());
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    SttRealServiceGrpc.SttRealServiceStub stub = SttRealServiceGrpc.newStub(channel);
    maum.brain.stt.Stt.Text.Builder textResult = maum.brain.stt.Stt.Text.newBuilder();
    // 비동기 처리를 위해서 CountDownLatch를 사용하였습니다.
    final CountDownLatch finishLatch = new CountDownLatch(1);

    // 서버에서 보내는 메시지를 처리하기 위한 옵져버를 생성합니다.
    io.grpc.stub.StreamObserver<maum.brain.stt.Stt.Text> responseObserver =
        new io.grpc.stub.StreamObserver<maum.brain.stt.Stt.Text>() {

          @Override
          public void onNext(maum.brain.stt.Stt.Text summary) {
            // 여기서는 양방향 스트림이 아니기때문에 정상적인 경우, onNext()가 한번씩만 호출됩니다.
            logger
                .debug("===== SttRealService simpleRecognize :: A message received from server: {}",
                    summary);
            textResult.setTxt(summary.getTxt());
          }

          @Override
          public void onError(Throwable t) {
            // 서버에서 onError()를 호출하거나 내부 통신장애가 발생하면 호출됩니다.
            logger.error("===== SttRealService simpleRecognize :: RecordRoute Failed: {}",
                io.grpc.Status.fromThrowable(t));
            finishLatch.countDown();
            if (channel != null) {
              GrpcUtils.closeChannel(channel);
            }
          }

          @Override
          public void onCompleted() {
            // 메시지 전송이 완료되면 서버에서 onCompleted()를 호출합니다.
            logger.debug(
                "===== SttRealService simpleRecognize :: responseObserver oncompleted called");
            finishLatch.countDown();
            if (channel != null) {
              GrpcUtils.closeChannel(channel);
            }
          }

        };

    // non-blocking/async stub을 사용하여 서버의 recordRoute()를 호출하였습니다.
    // 서버의 응답을 기다리기 위해서 비동기 stub을 사용해야 합니다.
    io.grpc.stub.StreamObserver<maum.brain.stt.Stt.Speech> requestObserver = stub
        .simpleRecognize(responseObserver);
    FileInputStream fis = null;
    try {
      fis = new FileInputStream(file);
      byte[] buffer = new byte[40960];
      while ((fis.read(buffer)) > 0) {
        Speech speech = Speech.newBuilder().setBin(ByteString.copyFrom(buffer)).build();
        requestObserver.onNext(speech);
      }
      // 메시지 전송이 완료되면 onCompleted()를 호출하여 서버에 종료 상황을 알립니다.
      logger.debug("===== SttRealService simpleRecognize :: requestObserver oncompleted called");
      requestObserver.onCompleted();

      // finishLatch를 사용하여 최대 1분간 blocking 하도록 합니다.
      if (!finishLatch.await(3, TimeUnit.HOURS)) {
        logger.error("===== SttRealService simpleRecognize :: Can not finish within 3 hours");
      }
      return textResult.build();

    } catch (RuntimeException e) {
      logger.error("===== SttRealService simpleRecognize :: requestObserver error {}",
          e.getMessage());
      requestObserver.onError(e);
      return null;
    } catch (FileNotFoundException e) {
      logger.error("===== SttRealService simpleRecognize :: RPC error {}", e.getMessage());
    } catch (IOException e) {
      logger.error("===== SttRealService simpleRecognize :: RPC error {}", e.getMessage());
    } finally {
      try {
        if (fis != null) {
          fis.close();
        }
      } catch (IOException e) {
        logger.error("simpleRecognize e : " , e);
      }
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
    }
    return null;
  }
}