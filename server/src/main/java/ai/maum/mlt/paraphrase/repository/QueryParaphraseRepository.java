package ai.maum.mlt.paraphrase.repository;

import ai.maum.mlt.paraphrase.entity.QueryParaphraseEntity;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Transactional
@Repository
public interface QueryParaphraseRepository extends JpaRepository<QueryParaphraseEntity, String> {

  @Query(value = "SELECT qpe "
      + "         FROM QueryParaphraseEntity qpe "
      + "         WHERE qpe.workspaceId = :workspaceId "
      + "         AND (qpe.title LIKE CONCAT('%', :title, '%') OR :title IS NULL)")
  Page<QueryParaphraseEntity> getQueryParaphrases(@Param("workspaceId") String workspaceId,
      @Param("title") String title, Pageable pageable);

  QueryParaphraseEntity findByWorkspaceIdAndId(@Param("workspaceId") String workspaceId,
      @Param("id") String id);

  void deleteByIdAndWorkspaceId(@Param("id") String id, @Param("workspaceId") String workspaceId);

  @Modifying
  @Query("UPDATE QueryParaphraseEntity qpe "
      + " SET qpe.allStep = :allStep "
      + " WHERE qpe.id = :id and qpe.workspaceId = :workspaceId")
  void updateSetAllStep(@Param("allStep") String allStep,
      @Param("id") String id, @Param("workspaceId") String workspaceId);

  @Modifying
  @Query("UPDATE QueryParaphraseEntity qpe "
      + " SET qpe.currentStep = :currentStep "
      + " WHERE qpe.id = :id and qpe.workspaceId = :workspaceId")
  void updateSetCurrentStep(@Param("currentStep") String currentStep,
      @Param("id") String id, @Param("workspaceId") String workspaceId);
}
