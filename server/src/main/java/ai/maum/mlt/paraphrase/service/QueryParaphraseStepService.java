package ai.maum.mlt.paraphrase.service;

import ai.maum.mlt.paraphrase.entity.QueryParaphraseEntity;
import ai.maum.mlt.paraphrase.entity.QueryParaphraseStepEntity;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface QueryParaphraseStepService {

  QueryParaphraseEntity insertWithQueryParaphrase(QueryParaphraseStepEntity queryParaphraseStepEntity,
      String[] sentenceList);

  void insertAfterGetParaphrase(List<QueryParaphraseStepEntity> queryParaphraseStepEntities);

  Page<QueryParaphraseStepEntity> getSentences(
      String queryParaphraseId, String workspaceId, String stepCd, Pageable pageable,
      String sentence);

  List<QueryParaphraseStepEntity> getSentences(String queryParaphraseId, String workspaceId,
      String stepCd);

  void deleteSentences(List<QueryParaphraseStepEntity> queryParaphraseStepEntities);

}