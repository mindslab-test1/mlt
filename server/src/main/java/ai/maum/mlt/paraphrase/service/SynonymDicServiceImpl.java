package ai.maum.mlt.paraphrase.service;

import ai.maum.mlt.common.util.FileUtils;
import ai.maum.mlt.paraphrase.entity.SynonymDicEntity;
import ai.maum.mlt.paraphrase.entity.SynonymDicRelEntity;
import ai.maum.mlt.paraphrase.repository.SynonymDicRelRepository;
import ai.maum.mlt.paraphrase.repository.SynonymDicRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

@Service
public class SynonymDicServiceImpl implements SynonymDicService {

  @Autowired
  private SynonymDicRepository synonymDicRepository;

  @Autowired
  private SynonymDicRelRepository synonymDicRelRepository;

  @Autowired
  private FileUtils fileUtils;

  @Value("#{'${excel.paraphrase.synonym.columns}'.split(',')}")
  private List<String> synonymColumns;

  @Value("#{'${excel.paraphrase.synonym.headers}'.split(',')}")
  private List<String> synonymHeaders;

  @Value("${excel.paraphrase.synonym.filename}")
  private String synonymFileName;


  @Override
  public List<SynonymDicEntity> selectListByWordsAndWorkspaceId(List<String> wordList,
      String workspaceId) {
    return synonymDicRepository.findAllByInKeywordAndWorkspaceId(wordList, workspaceId);
  }

  @Override
  public Page<SynonymDicEntity> selectListByWorkspaceId(String workspaceId,
      String searchRepresentativeWord, String searchSynonym, Pageable pageable) {
    return synonymDicRepository
        .findAllByWorkspaceIdAndKeyword(workspaceId, searchRepresentativeWord, searchSynonym,
            pageable);
  }

  @Override
  @Transactional
  public SynonymDicEntity insertSynonymDic(SynonymDicEntity synonymDicEntity) {
    synonymDicEntity.setCreatedAt(new Date());
    synonymDicEntity.setUpdatedAt(new Date());
    return synonymDicRepository.save(synonymDicEntity);
  }

  @Override
  @Transactional
  public SynonymDicEntity updateSynonymDic(SynonymDicEntity synonymDicEntity) {
    return synonymDicRepository.save(synonymDicEntity);
  }

  @Override
  @Transactional
  public void deleteSynonymDics(List<SynonymDicEntity> synonymDicEntities) {
    synonymDicRepository.delete(synonymDicEntities);
  }

  @Override
  @Transactional
  public void deleteSynonymDicsByWorkspaceId(String workspaceId) {
    synonymDicRepository.deleteAllByWorkspaceId(workspaceId);
  }

  @Override
  @Transactional
  public void uploadFiles(List<MultipartFile> files, String workspaceId) throws Exception {
    Date now = new Date();

    List<SynonymDicEntity> synonymDicEntities = new ArrayList<>();
    List<SynonymDicRelEntity> synonymDicRelEntities = new ArrayList<>();

    List<String> representationWordList = new ArrayList<>();
    List<String> synonymWordList = new ArrayList<>();
    List<String> duplicatedWordList = new ArrayList<>();
    for (MultipartFile file : files) {
      List<List> fileObjectList = fileUtils.excelFileToObject(file, synonymColumns);
      for (List<HashMap> fileObjects : fileObjectList) {
        for (HashMap<String, Object> object : fileObjects) {
          if (object.size() > 0) {
            if (!representationWordList.contains(object.get("representationWord"))) {
              SynonymDicEntity synonymDicEntity = new SynonymDicEntity();
              synonymDicEntity.setRepresentation((String) object.get("representationWord"));
              synonymDicEntity.setWorkspaceId(workspaceId);
              synonymDicEntity.setCreatedAt(now);
              synonymDicEntity.setUpdatedAt(now);
              synonymDicEntities.add(synonymDicEntity);
              representationWordList.add((String) object.get("representationWord"));
            }
            if ((!representationWordList.contains(object.get("synonymWord"))) && (!object
                .get("synonymWord").equals(""))
                && (object.get("synonymWord") != null)) {
              SynonymDicRelEntity synonymDicRelEntity = new SynonymDicRelEntity();
              synonymDicRelEntity.setSynonymWord((String) object.get("synonymWord"));
              synonymDicRelEntity.setWorkspaceId(workspaceId);
              synonymDicRelEntity.setUpdatedAt(now);
              synonymDicRelEntity.setCreatedAt(now);
              synonymDicRelEntity.setRepresentationWord((String) object.get("representationWord"));
              synonymDicRelEntities.add(synonymDicRelEntity);
              if (synonymWordList.contains(object.get("synonymWord"))) {
                duplicatedWordList.add((String) object.get("synonymWord"));
              } else {
                synonymWordList.add((String) object.get("synonymWord"));
              }
            }
          }
        }
      }
    }
    synonymDicRepository.delete(synonymDicRepository.findAllByWorkspaceId(workspaceId));
    List<SynonymDicEntity> insertedSynonymDicEntities = synonymDicRepository
        .save(synonymDicEntities);
    for (SynonymDicEntity entity : insertedSynonymDicEntities) {
      for (SynonymDicRelEntity rel : synonymDicRelEntities) {
        if (rel.getRepresentationWord().equals(entity.getRepresentation())) {
          rel.setRepresentationId(entity.getId());
        }
      }
    }
    synonymDicRelRepository.save(synonymDicRelEntities);

  }

  @Override
  public InputStreamResource downloadFile(String workspaceId) throws Exception {
    List<SynonymDicEntity> synonymDicEntities = synonymDicRepository.findAll();

    List<SynonymDicRelEntity> synonymDicRelEntities = new ArrayList<>();
    for (SynonymDicEntity entity : synonymDicEntities) {
      for (SynonymDicRelEntity rel : entity.getSynonymDicRelEntities()) {
        rel.setRepresentationWord(entity.getRepresentation());
        synonymDicRelEntities.add(rel);
      }
    }

    return fileUtils
        .ObjectToExcel(synonymDicRelEntities, synonymHeaders, synonymColumns, synonymFileName);
  }
}
