package ai.maum.mlt.paraphrase.repository;


import ai.maum.mlt.paraphrase.entity.EndingPostPositionDicEntity;
import ai.maum.mlt.paraphrase.entity.SynonymDicEntity;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.method.P;
import org.springframework.stereotype.Repository;

@Transactional
@Repository
public interface EndingPostPositionDicRepository extends
    JpaRepository<EndingPostPositionDicEntity, String> {

  @Query(value = "SELECT EPPDE "
      + "         FROM EndingPostPositionDicEntity EPPDE "
      + "         WHERE EPPDE.representation in(:keyword)"
      + "         AND EPPDE.workspaceId = :workspaceId"
  )
  List<EndingPostPositionDicEntity> findAllByInKeywordAndWorkspaceId(@Param("keyword") List keyword,
      @Param("workspaceId") String workspaceId);

  Page<EndingPostPositionDicEntity> findAllByWorkspaceId(String workspaceId, Pageable pageable);

  @Query(value = "SELECT DISTINCT EPPDE "
      + "         FROM EndingPostPositionDicEntity EPPDE "
      + "         LEFT OUTER JOIN EPPDE.endingPostPositionDicRelEntities EPPDRE "
      + "         WHERE (EPPDE.workspaceId = :workspaceId) "
      + "         AND (EPPDE.type = :type OR :type IS NULL) "
      + "         AND (EPPDE.representation LIKE CONCAT('%',:searchRepresentativeWord, '%') OR :searchRepresentativeWord IS NULL) "
      + "         AND (EPPDRE.word LIKE CONCAT('%',:searchWord, '%') OR :searchWord IS NULL) "
  )
  Page<EndingPostPositionDicEntity> findAllByWorkspaceId(
      @Param("workspaceId") String workspaceId,
      @Param("type") String type,
      @Param("searchRepresentativeWord") String searchRepresentativeWord,
      @Param("searchWord") String searchWord,
      Pageable pageable);

  List<EndingPostPositionDicEntity> findAllByWorkspaceId(String workspaceId);

  void deleteAllByWorkspaceId(String WorkspaceId);
}
