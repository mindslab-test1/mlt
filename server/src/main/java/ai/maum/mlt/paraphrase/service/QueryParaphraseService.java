package ai.maum.mlt.paraphrase.service;

import ai.maum.mlt.paraphrase.entity.QueryParaphraseEntity;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface QueryParaphraseService {

  Page<QueryParaphraseEntity> getQueryParaphrases(String workspaceId, String title,
      Pageable pageable);

  QueryParaphraseEntity getQueryParaphrase(String workspaceId, String id);

  void updateSetAllStep(String allStep, String id, String workspaceId);

  void updateSetCurrentStep(String currentStep, String id, String workspaceId);

  void deleteQueryParaphrases(List<QueryParaphraseEntity> queryParaphraseEntities);

}
