package ai.maum.mlt.paraphrase.controller;

import static ai.maum.mlt.common.system.SystemCode.PARAPHRASE_STEP_COMPLETE;
import static ai.maum.mlt.common.system.SystemCode.PARAPHRASE_STEP_ENDING_POSTPOSITION;
import static ai.maum.mlt.common.system.SystemCode.PARAPHRASE_STEP_INVERSION;
import static ai.maum.mlt.common.system.SystemCode.PARAPHRASE_STEP_READY;
import static ai.maum.mlt.common.system.SystemCode.PARAPHRASE_STEP_SYNONYM;
import static ai.maum.mlt.common.system.SystemCode.PARAPHRASE_STEP_WORDEMBEDDING;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.itfc.ParaphraseGrpcInterfaceManager;
import ai.maum.mlt.paraphrase.entity.QueryParaphraseEntity;
import ai.maum.mlt.paraphrase.entity.QueryParaphraseStepEntity;
import ai.maum.mlt.paraphrase.service.QueryParaphraseService;
import ai.maum.mlt.paraphrase.service.QueryParaphraseStepService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import maum.mlt.paraphrase.Paraphrase.ParaphraseRequest;
import maum.mlt.paraphrase.Paraphrase.ParaphraseResponse;
import maum.mlt.paraphrase.Paraphrase.ParaphraseResult;
import maum.mlt.paraphrase.Paraphrase.ParaphraseType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/paraphrase/qps")
public class QueryParaphraseStepController {

  static final Logger logger = LoggerFactory.getLogger(QueryParaphraseStepController.class);

  @Value("${mlt.paraphrase.ip}")
  private String paraphraseIp;

  @Value("${mlt.paraphrase.port}")
  private int paraphrasePort;

  @Autowired
  private QueryParaphraseService queryParaphraseService;

  @Autowired
  private QueryParaphraseStepService queryParaphraseStepService;

  /**
   * Query Paraphrase 객체 1개의 정보 조회
   */
  @UriRoleDesc(role = "paraphrase^R")
  @RequestMapping(
      value = "/get-paraphrase",
      method = RequestMethod.POST)
  public ResponseEntity<?> getParaphrase(
      @RequestBody QueryParaphraseStepEntity queryParaphraseStepEntity) {
    logger.debug("start : {}", queryParaphraseStepEntity);

    try {
      QueryParaphraseEntity queryParaphraseEntity =
          this.queryParaphraseService.getQueryParaphrase(queryParaphraseStepEntity.getWorkspaceId(),
              queryParaphraseStepEntity.getId());
      return new ResponseEntity<>(queryParaphraseEntity, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * 파일 업로드 함수 파일 업로드 이후 DB에 데이터를 Insert 한다.
   *
   * @return => QueryParaphraseStepEntity List를 반환한다.
   */
  @UriRoleDesc(role = "paraphrase^C")
  @RequestMapping(
      value = "/upload-files",
      method = RequestMethod.POST)
  public ResponseEntity<?> uploadFiles(
      @RequestBody QueryParaphraseStepEntity queryParaphraseStepEntity) {
    logger.debug("start : {}", queryParaphraseStepEntity);

    try {
      String[] sentenceList = queryParaphraseStepEntity.getSentence().split("\n");

      QueryParaphraseEntity result = this.queryParaphraseStepService
          .insertWithQueryParaphrase(queryParaphraseStepEntity, sentenceList);

      Page<QueryParaphraseStepEntity> resultList = this.queryParaphraseStepService
          .getSentences(result.getId(), queryParaphraseStepEntity.getWorkspaceId(),
              queryParaphraseStepEntity.getStepCd(), queryParaphraseStepEntity.getPageRequest(),
              queryParaphraseStepEntity.getSearchKeyword());

      return new ResponseEntity<>(resultList, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * GetParaphrase grpc call 을 하기 위한 함수
   *
   * @return => HTTPStatus
   */
  @UriRoleDesc(role = "paraphrase^X")
  @RequestMapping(
      value = "/paraphrase",
      method = RequestMethod.POST)
  public ResponseEntity<?> paraphrase(
      @RequestBody QueryParaphraseStepEntity queryParaphraseStepEntity) {
    logger.debug("start : {}", queryParaphraseStepEntity);

    try {
      // 0. Inversion, WordEmbedding, Synonym, Endig/Postposition 일 때만 GetParaphrase(grpc) 호출
      if (!queryParaphraseStepEntity.getStepCd().equals(PARAPHRASE_STEP_READY) &&
          !queryParaphraseStepEntity.getStepCd().equals(PARAPHRASE_STEP_COMPLETE)) {

        // 1. GetParapharase 할 데이터를 DataBase 에서 불러온다.
        List<QueryParaphraseStepEntity> resultList = this.queryParaphraseStepService
            .getSentences(queryParaphraseStepEntity.getQueryParaphraseId(),
                queryParaphraseStepEntity.getWorkspaceId(),
                queryParaphraseStepEntity.getPrevStepCd());

        ParaphraseGrpcInterfaceManager client = new ParaphraseGrpcInterfaceManager(paraphraseIp,
            paraphrasePort);
        ParaphraseRequest.Builder paraphraseRequest = ParaphraseRequest.newBuilder();

        //2. ParaphraseRequest 객체 세팅
        switch (queryParaphraseStepEntity.getStepCd()) {
          case PARAPHRASE_STEP_INVERSION:
            paraphraseRequest.setType(ParaphraseType.valueOf("PARAPHRASE_INVERSION"));
            break;
          case PARAPHRASE_STEP_WORDEMBEDDING:
            paraphraseRequest.setType(ParaphraseType.valueOf("PARAPHRASE_WE"));
            break;
          case PARAPHRASE_STEP_SYNONYM:
            paraphraseRequest.setType(ParaphraseType.valueOf("PARAPHRASE_SYNONYM"));
            break;
          case PARAPHRASE_STEP_ENDING_POSTPOSITION:
            paraphraseRequest.setType(ParaphraseType.valueOf("PARAPHRASE_EOW"));
            break;
        }

        for (QueryParaphraseStepEntity result : resultList) {
          paraphraseRequest.addOrigins(result.getSentence());
        }

        // 3. ParaphraseRequest 객체 세팅 후 grpc call
        ParaphraseResponse paraphraseResponse = client.getParaphrase(paraphraseRequest.build());

        /* 4. GetParaphrase grpc 호출 후 결과를
             MLT_QUERY_PRPRS_STEP 테이블에 Insert, MLT_QUERY_PRPRS 테이블 Update */
        List<ParaphraseResult> paraphraseResultList = paraphraseResponse.getResultsList();
        List<QueryParaphraseStepEntity> queryParaphraseEntities = new ArrayList<>();
        for (ParaphraseResult paraphraseResult : paraphraseResultList) {
          for (String paraphrase : paraphraseResult.getParaphrasesList()) {
            QueryParaphraseStepEntity insertEntity = new QueryParaphraseStepEntity();
            insertEntity.setQueryParaphraseId(queryParaphraseStepEntity.getQueryParaphraseId());
            insertEntity.setWorkspaceId(queryParaphraseStepEntity.getWorkspaceId());
            insertEntity.setStepCd(queryParaphraseStepEntity.getStepCd());
            insertEntity.setCreatedAt(new Date());

            // ParaphraseResponse 데이터들
            insertEntity.setOrgSentence(paraphraseResult.getOrigin());
            insertEntity.setSentence(paraphrase);
            queryParaphraseEntities.add(insertEntity);
          }
        }

        this.queryParaphraseStepService.insertAfterGetParaphrase(queryParaphraseEntities);

        // MLT_QUERY_PRPRS AllStep을 Update 한다.
        if (queryParaphraseStepEntity.isFirstParaphrase()) {
          this.queryParaphraseService.updateSetAllStep(queryParaphraseStepEntity.getAllStep(),
              queryParaphraseStepEntity.getQueryParaphraseId(),
              queryParaphraseStepEntity.getWorkspaceId());
        }
      }
      return new ResponseEntity<>(HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * 마지막 단계가 끝날 때 실행되는 함수
   *
   * @return => HttpStatus
   */
  @UriRoleDesc(role = "paraphrase^U")
  @RequestMapping(
      value = "/complete",
      method = RequestMethod.POST)
  public ResponseEntity<?> complete(
      @RequestBody QueryParaphraseEntity queryParaphraseEntity) {
    logger.debug("start : {}", queryParaphraseEntity);

    try {
      this.queryParaphraseService.updateSetCurrentStep(queryParaphraseEntity.getCurrentStep(),
          queryParaphraseEntity.getId(), queryParaphraseEntity.getWorkspaceId());
      return new ResponseEntity<>(HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * Sentence List 조회 함수
   *
   * @return Page<queryParaphraseStepEntity>
   */
  @UriRoleDesc(role = "paraphrase^R")
  @RequestMapping(
      value = "/get-sentences",
      method = RequestMethod.POST)
  public ResponseEntity<?> getSentences(
      @RequestBody QueryParaphraseStepEntity queryParaphraseStepEntity) {
    logger.debug("start : {}", queryParaphraseStepEntity);

    try {
      Page<QueryParaphraseStepEntity> resultList = this.queryParaphraseStepService
          .getSentences(queryParaphraseStepEntity.getQueryParaphraseId(),
              queryParaphraseStepEntity.getWorkspaceId(),
              queryParaphraseStepEntity.getStepCd(),
              queryParaphraseStepEntity.getPageRequest(),
              queryParaphraseStepEntity.getSearchKeyword());

      return new ResponseEntity<>(resultList, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * 복수개의 sentence를 삭제 하는 함수
   *
   * @return => HttpStatus
   */
  @UriRoleDesc(role = "paraphrase^D")
  @RequestMapping(
      value = "/delete-sentences",
      method = RequestMethod.POST)
  public ResponseEntity<?> deleteSentences(
      @RequestBody List<QueryParaphraseStepEntity> queryParaphraseStepEntities) {
    logger.debug("start : {}", queryParaphraseStepEntities);

    try {
      this.queryParaphraseStepService.deleteSentences(queryParaphraseStepEntities);
      return new ResponseEntity<>(HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "paraphrase^R")
  @RequestMapping(
      value = "/get-all-sentences",
      method = RequestMethod.POST)
  public ResponseEntity<?> getAllSentences(
      @RequestBody QueryParaphraseStepEntity queryParaphraseStepEntity) {
    logger.debug("start : {}", queryParaphraseStepEntity);

    try {
      List<QueryParaphraseStepEntity> queryParaphraseStepEntities =
          this.queryParaphraseStepService
              .getSentences(queryParaphraseStepEntity.getQueryParaphraseId(),
                  queryParaphraseStepEntity.getWorkspaceId(),
                  queryParaphraseStepEntity.getStepCd());

      return new ResponseEntity<>(queryParaphraseStepEntities, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}
