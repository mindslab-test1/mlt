package ai.maum.mlt.paraphrase.service;

import ai.maum.mlt.common.util.FileUtils;
import ai.maum.mlt.paraphrase.entity.EndingPostPositionDicEntity;
import ai.maum.mlt.paraphrase.entity.EndingPostPositionDicRelEntity;
import ai.maum.mlt.paraphrase.repository.EndingPostPositionDicRelRepository;
import ai.maum.mlt.paraphrase.repository.EndingPostPositionDicRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

@Service
public class EndingPostPositionDicServiceImpl implements EndingPostPositionDicService {

  @Autowired
  private EndingPostPositionDicRepository endingPostPositionDicRepository;

  @Autowired
  private EndingPostPositionDicRelRepository endingPostPositionDicRelRepository;

  @Autowired
  private FileUtils fileUtils;

  @Value("#{'${excel.paraphrase.endingpostposition.columns}'.split(',')}")
  private List<String> endingPostPositionColumns;

  @Value("#{'${excel.paraphrase.endingpostposition.headers}'.split(',')}")
  private List<String> endingPostPositionHeaders;

  @Value("${excel.paraphrase.endingpostposition.filename}")
  private String endingPostPositionFileName;


  @Override
  public List<EndingPostPositionDicEntity> selectListByWordsAndWorkspaceId(List<String> wordList,
      String workspaceId) {
    return endingPostPositionDicRepository.findAllByInKeywordAndWorkspaceId(wordList, workspaceId);
  }

  @Override
  public Page<EndingPostPositionDicEntity> selectListByWorkspaceId(String workspaceId,
      String type, String searchRepresentativeWord, String searchWord, Pageable pageable) {
    return endingPostPositionDicRepository
        .findAllByWorkspaceId(workspaceId, type, searchRepresentativeWord, searchWord, pageable);
  }

  @Override
  @Transactional
  public EndingPostPositionDicEntity insertEndingPostPositionDic(
      EndingPostPositionDicEntity endingPostPositionDicEntity) {
    return endingPostPositionDicRepository.save(endingPostPositionDicEntity);
  }

  @Override
  @Transactional
  public EndingPostPositionDicEntity updateEndingPostPositionDic(
      EndingPostPositionDicEntity endingPostPositionDicEntity) {
    return endingPostPositionDicRepository.save(endingPostPositionDicEntity);
  }

  @Override
  @Transactional
  public void deleteEndingPostPositionDics(
      List<EndingPostPositionDicEntity> endingPostPositionDicEntities) {
    endingPostPositionDicRepository.delete(endingPostPositionDicEntities);
  }

  @Override
  @Transactional
  public void deleteEndingPostPositionDicsByWorkspaceId(String workspaceId) {
    endingPostPositionDicRepository.deleteAllByWorkspaceId(workspaceId);
  }

  @Override
  @Transactional
  public void uploadFiles(List<MultipartFile> files, String workspaceId) throws Exception {
    Date now = new Date();

    List<EndingPostPositionDicEntity> endingPostPositionDicEntities = new ArrayList<>();
    List<EndingPostPositionDicRelEntity> endingPostPositionDicRelEntities = new ArrayList<>();

    List<String> representationWordList = new ArrayList<>();
    List<String> wordList = new ArrayList<>();
    List<String> duplicatedWordList = new ArrayList<>();
    for (MultipartFile file : files) {
      List<List> fileObjectList = fileUtils.excelFileToObject(file, endingPostPositionColumns);
      for (List<HashMap> fileObjects : fileObjectList) {
        for (HashMap<String, Object> object : fileObjects) {
          if (object.size() > 0) {
            if (!representationWordList.contains(object.get("representationWord"))) {
              EndingPostPositionDicEntity endingPostPositionDicEntity = new EndingPostPositionDicEntity();
              endingPostPositionDicEntity
                  .setRepresentation((String) object.get("representationWord"));
              endingPostPositionDicEntity.setWorkspaceId(workspaceId);
              endingPostPositionDicEntity.setCreatedAt(now);
              endingPostPositionDicEntity.setUpdatedAt(now);
              endingPostPositionDicEntity.setType((String) object.get("type"));
              endingPostPositionDicEntities.add(endingPostPositionDicEntity);
              representationWordList.add((String) object.get("representationWord"));
            }
            if ((!representationWordList.contains(object.get("word"))) && (!object
                .get("word").equals(""))
                && (object.get("word") != null)) {
              EndingPostPositionDicRelEntity endingPostPositionDicRelEntity = new EndingPostPositionDicRelEntity();
              endingPostPositionDicRelEntity.setWord((String) object.get("word"));
              endingPostPositionDicRelEntity.setWorkspaceId(workspaceId);
              endingPostPositionDicRelEntity.setUpdatedAt(now);
              endingPostPositionDicRelEntity.setCreatedAt(now);
              endingPostPositionDicRelEntity
                  .setRepresentationWord((String) object.get("representationWord"));
              endingPostPositionDicRelEntities.add(endingPostPositionDicRelEntity);
              if (wordList.contains(object.get("word"))) {
                duplicatedWordList.add((String) object.get("word"));
              } else {
                wordList.add((String) object.get("word"));
              }
            }
          }
        }
      }
    }
    endingPostPositionDicRepository
        .delete(endingPostPositionDicRepository.findAllByWorkspaceId(workspaceId));
    List<EndingPostPositionDicEntity> insertedEndingPostPositionDicEntities = endingPostPositionDicRepository
        .save(endingPostPositionDicEntities);
    for (EndingPostPositionDicEntity entity : insertedEndingPostPositionDicEntities) {
      for (EndingPostPositionDicRelEntity rel : endingPostPositionDicRelEntities) {
        if (rel.getRepresentationWord().equals(entity.getRepresentation())) {
          rel.setRepresentationId(entity.getId());
        }
      }
    }
    endingPostPositionDicRelRepository.save(endingPostPositionDicRelEntities);

  }

  @Override
  public InputStreamResource downloadFile(String workspaceId) throws Exception {
    List<EndingPostPositionDicEntity> endingPostPositionDicEntities = endingPostPositionDicRepository
        .findAll();

    List<EndingPostPositionDicRelEntity> endingPostPositionDicRelEntities = new ArrayList<>();
    for (EndingPostPositionDicEntity entity : endingPostPositionDicEntities) {
      for (EndingPostPositionDicRelEntity rel : entity.getEndingPostPositionDicRelEntities()) {
        rel.setRepresentationWord(entity.getRepresentation());
        rel.setType(entity.getType());
        endingPostPositionDicRelEntities.add(rel);
      }
    }
    return fileUtils
        .ObjectToExcel(endingPostPositionDicRelEntities, endingPostPositionHeaders,
            endingPostPositionColumns,
            endingPostPositionFileName);
  }
}
