package ai.maum.mlt.paraphrase.service;

import ai.maum.mlt.paraphrase.entity.QueryParaphraseEntity;
import ai.maum.mlt.paraphrase.repository.QueryParaphraseRepository;
import ai.maum.mlt.paraphrase.repository.QueryParaphraseStepRepository;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class QueryParaphraseServiceImpl implements QueryParaphraseService {

  @Autowired
  private QueryParaphraseRepository queryParaphraseRepository;

  @Autowired
  private QueryParaphraseStepRepository queryParaphraseStepRepository;

  @Override
  public Page<QueryParaphraseEntity> getQueryParaphrases(String workspaceId, String title,
      Pageable pageable) {
    return this.queryParaphraseRepository.getQueryParaphrases(workspaceId, title, pageable);
  }

  @Override
  public QueryParaphraseEntity getQueryParaphrase(String workspaceId, String id) {
    return this.queryParaphraseRepository.findByWorkspaceIdAndId(workspaceId, id);
  }

  @Override
  @Transactional
  public void updateSetAllStep(String allStep, String id, String workspaceId) {
    this.queryParaphraseRepository.updateSetAllStep(allStep, id, workspaceId);
  }

  @Override
  @Transactional
  public void updateSetCurrentStep(String currentStep, String id, String workspaceId) {
    this.queryParaphraseRepository.updateSetCurrentStep(currentStep, id, workspaceId);
  }

  @Override
  @Transactional
  public void deleteQueryParaphrases(List<QueryParaphraseEntity> queryParaphraseEntities) {
    for (QueryParaphraseEntity queryParaphraseEntity : queryParaphraseEntities) {
      this.queryParaphraseStepRepository.deleteByQueryParaphraseIdAndWorkspaceId(
          queryParaphraseEntity.getId(), queryParaphraseEntity.getWorkspaceId()
      );
      this.queryParaphraseRepository.delete(queryParaphraseEntity);
    }
  }
}
