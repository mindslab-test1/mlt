package ai.maum.mlt.paraphrase.repository;


import ai.maum.mlt.paraphrase.entity.SynonymDicEntity;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Transactional
@Repository
public interface SynonymDicRepository extends JpaRepository<SynonymDicEntity, String> {

  @Query(value = "SELECT SDE "
      + "         FROM SynonymDicEntity SDE "
      + "         WHERE SDE.representation in(:keyword)"
      + "         AND SDE.workspaceId = :workspaceId"
  )
  List<SynonymDicEntity> findAllByInKeywordAndWorkspaceId(@Param("keyword") List keyword,
      @Param("workspaceId") String workspaceId);

  @Query(value = "SELECT DISTINCT SDE "
      + "         FROM SynonymDicEntity SDE "
      + "         LEFT OUTER JOIN SDE.synonymDicRelEntities SDRE "
      + "         WHERE (SDE.workspaceId = :workspaceId) "
      + "         AND (SDE.representation LIKE CONCAT('%', :searchRepresentativeWord, '%') OR :searchRepresentativeWord IS NULL) "
      + "         AND (SDRE.synonymWord LIKE CONCAT('%', :searchSynonym, '%') OR :searchSynonym IS NULL)"
  )
  Page<SynonymDicEntity> findAllByWorkspaceIdAndKeyword(@Param("workspaceId") String workspaceId,
      @Param("searchRepresentativeWord") String searchRepresentativeWord,
      @Param("searchSynonym") String searchSynonym, Pageable pageable);

  List<SynonymDicEntity> findAllByWorkspaceId(String workspaceId);

  void deleteAllByWorkspaceId(String WorkspaceId);
}
