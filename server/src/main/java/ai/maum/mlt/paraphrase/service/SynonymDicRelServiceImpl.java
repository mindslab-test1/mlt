package ai.maum.mlt.paraphrase.service;

import ai.maum.mlt.paraphrase.entity.SynonymDicRelEntity;
import ai.maum.mlt.paraphrase.repository.SynonymDicRelRepository;
import ai.maum.mlt.paraphrase.repository.SynonymDicRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SynonymDicRelServiceImpl implements SynonymDicRelService {

  @Autowired
  private SynonymDicRepository synonymDicRepository;

  @Autowired
  private SynonymDicRelRepository synonymDicRelRepository;

  @Override
  public List<SynonymDicRelEntity> selectListByWordsAndWorkspaceId(List<String> wordList,
      String workspaceId) {
    return synonymDicRelRepository.findAllByInKeywordAndWorkspaceId(wordList, workspaceId);
  }

  @Override
  @Transactional
  public List<SynonymDicRelEntity> insertSynonymDicRels(
      List<SynonymDicRelEntity> synonymDicRelEntities) {
    return synonymDicRelRepository.save(synonymDicRelEntities);
  }

  @Override
  @Transactional
  public List<SynonymDicRelEntity> updateSynonymDicRels(
      List<SynonymDicRelEntity> synonymDicRelEntities) {
    return synonymDicRelRepository.save(synonymDicRelEntities);
  }

  @Override
  @Transactional
  public void deleteSynonymDicRels(List<SynonymDicRelEntity> synonymDicRelEntities) {
    synonymDicRelRepository.delete(synonymDicRelEntities);
  }
}
