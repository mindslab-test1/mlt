package ai.maum.mlt.paraphrase.repository;

import ai.maum.mlt.paraphrase.entity.QueryParaphraseStepEntity;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Transactional
@Repository
public interface QueryParaphraseStepRepository extends
    JpaRepository<QueryParaphraseStepEntity, String> {

  @Query(value = "SELECT q "
      + "         FROM QueryParaphraseStepEntity q "
      + "         WHERE q.queryParaphraseId = :queryParaphraseId"
      + "         AND q.workspaceId = :workspaceId "
      + "         AND q.stepCd = :stepCd "
      + "         AND (q.sentence LIKE CONCAT('%', :sentence, '%') OR :sentence IS NULL) ")
  Page<QueryParaphraseStepEntity> getSentences(
      @Param("queryParaphraseId") String queryParaphraseId,
      @Param("workspaceId") String workspaceId,
      @Param("stepCd") String stepCd, Pageable pageable,
      @Param("sentence") String sentence);

  @Query(value = "SELECT q "
      + "         FROM QueryParaphraseStepEntity  q "
      + "         WHERE q.queryParaphraseId = :queryParaphraseId "
      + "         AND q.workspaceId = :workspaceId "
      + "         AND q.stepCd = :stepCd")
  List<QueryParaphraseStepEntity> getSentences(
      @Param("queryParaphraseId") String queryParaphraseId,
      @Param("workspaceId") String workspaceId,
      @Param("stepCd") String stepCd);

  void deleteByQueryParaphraseIdAndWorkspaceId(@Param("queryParaphraseId") String queryParaphraseId,
      @Param("workspaceId") String workspaceId);

}
