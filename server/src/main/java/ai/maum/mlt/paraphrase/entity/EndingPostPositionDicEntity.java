package ai.maum.mlt.paraphrase.entity;

import ai.maum.mlt.common.auth.entity.UserEntity;
import ai.maum.mlt.common.auth.entity.WorkspaceEntity;
import ai.maum.mlt.common.pagenation.PageParameters;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.LastModifiedDate;

@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "MAI_END_POST_DIC")
public class EndingPostPositionDicEntity extends PageParameters implements Serializable {

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid")
  @Column(name = "ID", length = 40)
  private String id;

  @Column(name = "WORKSPACE_ID", length = 40, nullable = false)
  private String workspaceId;

  @Column(name = "REPRESENTATION", length = 40, nullable = false)
  private String representation;

  @Column(name = "TYPE", length = 5, nullable = false)
  private String type;

  @Column(name = "CREATOR_ID", length = 40)
  private String creatorId;

  @CreatedDate
  @Column(name = "CREATED_AT")
  private Date createdAt;

  @Column(name = "UPDATER_ID", length = 40)
  private String updaterId;

  @LastModifiedDate
  @Column(name = "UPDATED_AT")
  private Date updatedAt;

  @OneToMany(mappedBy = "representationId", cascade = CascadeType.REMOVE, orphanRemoval = true)
  private List<EndingPostPositionDicRelEntity> endingPostPositionDicRelEntities;

  @ManyToOne
  @JoinColumn(name = "WORKSPACE_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private WorkspaceEntity workspaceEntity;

  @ManyToOne
  @JoinColumn(name = "CREATOR_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity createUserEntity;

  @ManyToOne
  @JoinColumn(name = "UPDATER_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity updateUserEntity;

  @Transient
  private String searchRepresentativeWord;

  @Transient
  private String searchWord;

  @Transient
  private List<EndingPostPositionDicRelEntity> toBeDeletedEndingPostPositionDicRelEntities;
}
