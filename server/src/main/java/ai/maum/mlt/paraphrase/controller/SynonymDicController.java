package ai.maum.mlt.paraphrase.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.paraphrase.entity.SynonymDicEntity;
import ai.maum.mlt.paraphrase.entity.SynonymDicRelEntity;
import ai.maum.mlt.paraphrase.service.SynonymDicRelService;
import ai.maum.mlt.paraphrase.service.SynonymDicService;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController()
@RequestMapping("api/paraphrase/synonymDic")
public class SynonymDicController {

  static final Logger logger = LoggerFactory.getLogger(
      SynonymDicController.class);

  @Autowired
  private SynonymDicService synonymDicService;

  @Autowired
  private SynonymDicRelService synonymDicRelService;

  @Value("${excel.paraphrase.synonym.filename}")
  private String synonymFileName;

  @UriRoleDesc(role = "paraphrase^R")
  @RequestMapping(
      value = "/checkWordExist/{workspaceId}",
      method = RequestMethod.POST)
  public ResponseEntity<?> checkWordExist(@RequestBody List<String> wordList,
      @PathVariable String workspaceId) {
    logger.info(
        "======= call api  [[/api/paraphrase/synonymDic/checkWordExist/{workspaceId}]] =======");
    try {
      List<SynonymDicEntity> synonymDicEntities = synonymDicService
          .selectListByWordsAndWorkspaceId(wordList, workspaceId);
      List<SynonymDicRelEntity> synonymDicRelEntities = synonymDicRelService
          .selectListByWordsAndWorkspaceId(wordList, workspaceId);
      HashMap<String, Object> result = new HashMap<>();

      if (synonymDicEntities.size() + synonymDicRelEntities.size() == 0) {
        result.put("result", true);
        return new ResponseEntity<>(result, HttpStatus.OK);
      } else {
        result.put("result", false);
        return new ResponseEntity<>(result, HttpStatus.OK);
      }
    } catch (Exception e) {
      logger.error("checkWordExist e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "paraphrase^R")
  @RequestMapping(
      value = "/getSynonymDicListByWorkspaceId",
      method = RequestMethod.POST)
  public ResponseEntity<?> getSynonymDicListByWorkspaceId(
      @RequestBody SynonymDicEntity synonymDicEntity) {
    logger.info("======= call api  [[/api/paraphrase/synonymDic/getSynonymDicList]] =======");
    try {
      Page<SynonymDicEntity> result = synonymDicService
          .selectListByWorkspaceId(synonymDicEntity.getWorkspaceId(),
              synonymDicEntity.getSearchRepresentativeWord(),
              synonymDicEntity.getSearchSynonym(),
              synonymDicEntity.getPageRequest());

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getSynonymDicListByWorkspaceId e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "paraphrase^C")
  @RequestMapping(
      value = "/addSynonymDic",
      method = RequestMethod.POST)
  public ResponseEntity<?> addSynonymDic(@RequestBody SynonymDicEntity synonymDicEntityParam) {
    logger.info("======= call api  [[/api/paraphrase/synonymDic/addSynonymDic]] =======");
    try {
      Date now = new Date();
      synonymDicEntityParam.setCreatedAt(now);
      synonymDicEntityParam.setUpdatedAt(now);
      SynonymDicEntity insertedResult = synonymDicService.insertSynonymDic(synonymDicEntityParam);

      for (SynonymDicRelEntity rel : synonymDicEntityParam.getSynonymDicRelEntities()) {
        rel.setRepresentationId(insertedResult.getId());
        rel.setCreatedAt(now);
        rel.setUpdatedAt(now);
      }
      insertedResult.setSynonymDicRelEntities(synonymDicRelService
          .insertSynonymDicRels(synonymDicEntityParam.getSynonymDicRelEntities()));

      return new ResponseEntity<>(insertedResult, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("addSynonymDic e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "paraphrase^U")
  @RequestMapping(
      value = "/editSynonymDic",
      method = RequestMethod.POST)
  public ResponseEntity<?> editSynonymDic(@RequestBody SynonymDicEntity synonymDicEntityParam) {
    logger.info("======= call api  [[/api/paraphrase/synonymDic/editSynonymDic]] =======");
    try {
      Date now = new Date();
      synonymDicEntityParam.setUpdatedAt(now);
      SynonymDicEntity updatedResult = synonymDicService.updateSynonymDic(synonymDicEntityParam);
      synonymDicRelService
          .deleteSynonymDicRels(synonymDicEntityParam.getToBeDeletedSynonymDicRelEntities());

      for (SynonymDicRelEntity rel : synonymDicEntityParam.getSynonymDicRelEntities()) {
        if (rel.getRepresentationId() == null) {
          rel.setCreatedAt(now);
          rel.setRepresentationId(updatedResult.getId());
        }
      }
      updatedResult.setSynonymDicRelEntities(synonymDicRelService
          .updateSynonymDicRels(synonymDicEntityParam.getSynonymDicRelEntities()));

      return new ResponseEntity<>(updatedResult, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("editSynonymDic e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "paraphrase^D")
  @RequestMapping(
      value = "removeSynonymDics",
      method = RequestMethod.POST)
  public ResponseEntity<?> removeSynonymDics(
      @RequestBody List<SynonymDicEntity> synonymDicEntitiesParam) {
    logger.info("======= call api  [[/api/paraphrase/synonymDic/removeSynonymDics]] =======");
    try {
      synonymDicService.deleteSynonymDics(synonymDicEntitiesParam);
      return new ResponseEntity<>(HttpStatus.OK);
    } catch (Exception e) {
      logger.error("removeSynonymDics e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "paraphrase^D")
  @RequestMapping(
      value = "removeSynonymDics/all",
      method = RequestMethod.POST)
  public ResponseEntity<?> removeSynonymDicAll(@RequestBody String workspaceId) {
    logger.info("======= call api  [[/api/paraphrase/synonymDic/removeSynonymDics/all]] =======");
    try {
      synonymDicService.deleteSynonymDicsByWorkspaceId(workspaceId);
      return new ResponseEntity<>(HttpStatus.OK);
    } catch (Exception e) {
      logger.error("removeSynonymDicAll e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "paraphrase^C")
  @RequestMapping(
      value = "/uploadFiles/{workspaceId}",
      method = RequestMethod.POST)
  public ResponseEntity<?> uploadFiles(@RequestParam("file") List<MultipartFile> files,
      @PathVariable String workspaceId) {
    logger
        .info("======= call api  [[/api/paraphrase/synonymDic/uploadFile/{workspaceId}]] =======");
    try {
      synonymDicService.uploadFiles(files, workspaceId);

      HashMap<String, Object> result = new HashMap<>();
      result.put("status", "success");

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("uploadFiles e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "paraphrase^R")
  @RequestMapping(
      value = "/downloadFile/{workspaceId}",
      method = RequestMethod.GET)
  public ResponseEntity<?> downloadFile(@PathVariable String workspaceId) throws IOException {
    logger
        .info(
            "======= call api  [[/api/paraphrase/synonymDic/downloadFile/{workspaceId}]] =======");

    try {
      HttpHeaders httpHeaders = new HttpHeaders();
      httpHeaders
          .add(HttpHeaders.CONTENT_DISPOSITION,
              "attachment;filename=" + synonymFileName + ".xlsx");
      httpHeaders.add(HttpHeaders.CONTENT_TYPE, "application/octet-stream");

      return new ResponseEntity<>(synonymDicService.downloadFile(workspaceId), httpHeaders,
          HttpStatus.OK);
    } catch (Exception e) {
      logger.error("downloadFile e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
