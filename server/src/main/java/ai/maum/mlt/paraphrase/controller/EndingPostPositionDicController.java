package ai.maum.mlt.paraphrase.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.paraphrase.entity.EndingPostPositionDicEntity;
import ai.maum.mlt.paraphrase.entity.EndingPostPositionDicRelEntity;
import ai.maum.mlt.paraphrase.service.EndingPostPositionDicRelService;
import ai.maum.mlt.paraphrase.service.EndingPostPositionDicService;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import static ai.maum.mlt.common.system.SystemCode.PARAPHRASE_ENDING;
import static ai.maum.mlt.common.system.SystemCode.PARAPHRASE_POST_POSITION;

@RestController()
@RequestMapping("api/paraphrase/endingPostPositionDic")
public class EndingPostPositionDicController {

  static final Logger logger = LoggerFactory.getLogger(
      EndingPostPositionDicController.class);

  @Autowired
  private EndingPostPositionDicService endingPostPositionDicService;

  @Autowired
  private EndingPostPositionDicRelService endingPostPositionDicRelService;

  @Value("${excel.paraphrase.endingpostposition.filename}")
  private String endingPostPositionFileName;

  @UriRoleDesc(role = "paraphrase^R")
  @RequestMapping(
      value = "/checkWordExist/{workspaceId}",
      method = RequestMethod.POST)
  public ResponseEntity<?> checkWordExist(@RequestBody List<String> wordList,
      @PathVariable String workspaceId) {
    logger.info(
        "======= call api  [[/api/paraphrase/endingPostPositionDic/checkWordExist/{workspaceId}]] =======");
    try {
      List<EndingPostPositionDicEntity> endingPostPositionDicEntities = endingPostPositionDicService
          .selectListByWordsAndWorkspaceId(wordList, workspaceId);
      List<EndingPostPositionDicRelEntity> endingPostPositionDicRelEntities = endingPostPositionDicRelService
          .selectListByWordsAndWorkspaceId(wordList, workspaceId);
      HashMap<String, Object> result = new HashMap<>();

      if (endingPostPositionDicEntities.size() + endingPostPositionDicRelEntities.size() == 0) {
        result.put("result", true);
        return new ResponseEntity<>(result, HttpStatus.OK);
      } else {
        result.put("result", false);
        return new ResponseEntity<>(result, HttpStatus.OK);
      }
    } catch (Exception e) {
      logger.error("checkWordExist e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  @UriRoleDesc(role = "paraphrase^R")
  @RequestMapping(
      value = "/getEndingPostPositionDicListByWorkspaceId",
      method = RequestMethod.POST)
  public ResponseEntity<?> getEndingPostPositionDicListByWorkspaceId(
      @RequestBody EndingPostPositionDicEntity endingPostPositionDicEntityParam) {
    logger.info(
        "======= call api  [[/api/paraphrase/endingPostPositionDic/getEndingPostPositionDicListByWorkspaceId]] =======");
    try {
      Page<EndingPostPositionDicEntity> result = endingPostPositionDicService
          .selectListByWorkspaceId(
              endingPostPositionDicEntityParam.getWorkspaceId(),
              endingPostPositionDicEntityParam.getType(),
              endingPostPositionDicEntityParam.getSearchRepresentativeWord(),
              endingPostPositionDicEntityParam.getSearchWord(),
              endingPostPositionDicEntityParam.getPageRequest()
          );

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getEndingPostPositionDicListByWorkspaceId e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "paraphrase^C")
  @RequestMapping(
      value = "/addEndingPostPositionDic",
      method = RequestMethod.POST)
  public ResponseEntity<?> addEndingPostPositionDic(
      @RequestBody EndingPostPositionDicEntity endingPostPositionDicEntityParam) {
    logger.info(
        "======= call api  [[/api/paraphrase/endingPostPositionDic/addEndingPostPositionDic]] =======");
    try {
      Date now = new Date();
      endingPostPositionDicEntityParam.setCreatedAt(now);
      endingPostPositionDicEntityParam.setUpdatedAt(now);
      EndingPostPositionDicEntity insertedResult = endingPostPositionDicService
          .insertEndingPostPositionDic(endingPostPositionDicEntityParam);

      for (EndingPostPositionDicRelEntity rel : endingPostPositionDicEntityParam
          .getEndingPostPositionDicRelEntities()) {
        rel.setRepresentationId(insertedResult.getId());
        rel.setCreatedAt(now);
        rel.setUpdatedAt(now);
      }
      insertedResult.setEndingPostPositionDicRelEntities(endingPostPositionDicRelService
          .insertEndingPostPositionDicRels(
              endingPostPositionDicEntityParam.getEndingPostPositionDicRelEntities()));

      return new ResponseEntity<>(insertedResult, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("addEndingPostPositionDic e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "paraphrase^U")
  @RequestMapping(
      value = "/editEndingPostPositionDic",
      method = RequestMethod.POST)
  public ResponseEntity<?> editEndingPostPositionDic(
      @RequestBody EndingPostPositionDicEntity endingPostPositionDicEntityParam) {
    logger.info(
        "======= call api  [[/api/paraphrase/endingPostPositionDic/editEndingPostPositionDic]] =======");
    try {
      Date now = new Date();
      endingPostPositionDicEntityParam.setUpdatedAt(now);
      EndingPostPositionDicEntity updatedResult = endingPostPositionDicService
          .updateEndingPostPositionDic(endingPostPositionDicEntityParam);
      endingPostPositionDicRelService
          .deleteEndingPostPositionDicRels(
              endingPostPositionDicEntityParam.getToBeDeletedEndingPostPositionDicRelEntities());

      for (EndingPostPositionDicRelEntity rel : endingPostPositionDicEntityParam
          .getEndingPostPositionDicRelEntities()) {
        if (rel.getRepresentationId() == null) {
          rel.setCreatedAt(now);
          rel.setRepresentationId(updatedResult.getId());
        }
      }
      updatedResult.setEndingPostPositionDicRelEntities(endingPostPositionDicRelService
          .updateEndingPostPositionDicRels(
              endingPostPositionDicEntityParam.getEndingPostPositionDicRelEntities()));

      return new ResponseEntity<>(updatedResult, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("editEndingPostPositionDic e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "paraphrase^D")
  @RequestMapping(
      value = "removeEndingPostPositionDics",
      method = RequestMethod.POST)
  public ResponseEntity<?> removeEndingPostPositionDics(
      @RequestBody List<EndingPostPositionDicEntity> endingPostPositionDicEntitiesParam) {
    logger.info(
        "======= call api  [[/api/paraphrase/endingPostPositionDic/removeEndingPostPositionDics]] =======");
    try {
      endingPostPositionDicService.deleteEndingPostPositionDics(endingPostPositionDicEntitiesParam);
      return new ResponseEntity<>(HttpStatus.OK);
    } catch (Exception e) {
      logger.error("removeEndingPostPositionDics e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "paraphrase^D")
  @RequestMapping(
      value = "removeEndingPostPositionDics/all",
      method = RequestMethod.POST)
  public ResponseEntity<?> removeEndingPostPositionDicsAll(
      @RequestBody String workspaceId) {
    logger.info(
        "======= call api  [[/api/paraphrase/endingPostPositionDic/removeEndingPostPositionDics/all]] =======");
    try {
      endingPostPositionDicService.deleteEndingPostPositionDicsByWorkspaceId(workspaceId);
      return new ResponseEntity<>(HttpStatus.OK);
    } catch (Exception e) {
      logger.error("removeEndingPostPositionDicsAll e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "paraphrase^C")
  @RequestMapping(
      value = "/uploadFiles/{workspaceId}",
      method = RequestMethod.POST)
  public ResponseEntity<?> uploadFiles(@RequestParam("file") List<MultipartFile> files,
      @PathVariable String workspaceId) {
    logger.info(
        "======= call api  [[/api/paraphrase/endingPostPositionDic/uploadFile/{workspaceId}]] =======");
    try {
      endingPostPositionDicService.uploadFiles(files, workspaceId);

      HashMap<String, Object> result = new HashMap<>();
      result.put("status", "success");

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("uploadFiles e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "paraphrase^R")
  @RequestMapping(
      value = "/downloadFile/{workspaceId}",
      method = RequestMethod.GET)
  public ResponseEntity<?> downloadFile(@PathVariable String workspaceId) throws IOException {
    logger
        .info(
            "======= call api  [[/api/paraphrase/endingPostPositionDic/downloadFile/{workspaceId}]] =======");

    try {
      HttpHeaders httpHeaders = new HttpHeaders();
      httpHeaders
          .add(HttpHeaders.CONTENT_DISPOSITION,
              "attachment;filename=" + endingPostPositionFileName + ".xlsx");
      httpHeaders.add(HttpHeaders.CONTENT_TYPE, "application/octet-stream");

      return new ResponseEntity<>(endingPostPositionDicService.downloadFile(workspaceId),
          httpHeaders,
          HttpStatus.OK);
    } catch (Exception e) {
      logger.error("downloadFile e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
