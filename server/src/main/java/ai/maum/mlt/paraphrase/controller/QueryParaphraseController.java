package ai.maum.mlt.paraphrase.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.paraphrase.entity.QueryParaphraseEntity;
import ai.maum.mlt.paraphrase.service.QueryParaphraseService;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/paraphrase/qp")
public class QueryParaphraseController {

  static final Logger logger = LoggerFactory.getLogger(QueryParaphraseController.class);

  @Autowired
  private QueryParaphraseService queryParaphraseService;

  /**
   * Paraphrase 리스트 목록을 조회 한다.
   * @param queryParaphraseEntity
   * @return => Page<QueryParaphraseEntity>
   */
  @UriRoleDesc(role = "paraphrase^R")
  @RequestMapping(
      value = "/get-paraphrases",
      method = RequestMethod.POST)
  public ResponseEntity<?> getParaphrases(
      @RequestBody QueryParaphraseEntity queryParaphraseEntity) {
    logger.debug("start : {}", queryParaphraseEntity);

    try {
      Page<QueryParaphraseEntity> resultList = this.queryParaphraseService
          .getQueryParaphrases(queryParaphraseEntity.getWorkspaceId(),
              queryParaphraseEntity.getTitle(),
              queryParaphraseEntity.getPageRequest());
      return new ResponseEntity<>(resultList, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * delete paraphrases API
   * @param queryParaphraseEntities
   * @return => HttpStatus
   */
  @UriRoleDesc(role = "paraphrase^D")
  @RequestMapping(
      value = "/delete-paraphrases",
      method = RequestMethod.POST)
  public ResponseEntity<?> deleteParaphrases(
      @RequestBody List<QueryParaphraseEntity> queryParaphraseEntities) {
    logger.debug("start : {}", queryParaphraseEntities);

    try {
      this.queryParaphraseService.deleteQueryParaphrases(queryParaphraseEntities);
      return new ResponseEntity<>(HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}
