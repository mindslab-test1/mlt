package ai.maum.mlt.paraphrase.service;

import ai.maum.mlt.paraphrase.entity.EndingPostPositionDicRelEntity;
import ai.maum.mlt.paraphrase.repository.EndingPostPositionDicRelRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class EndingPostPositionDicRelServiceImpl implements EndingPostPositionDicRelService {

  @Autowired
  private EndingPostPositionDicRelRepository endingPostPositionDicRelRepository;

  @Override
  public List<EndingPostPositionDicRelEntity> selectListByWordsAndWorkspaceId(List<String> wordList,
      String workspaceId) {
    return endingPostPositionDicRelRepository
        .findAllByInKeywordAndWorkspaceId(wordList, workspaceId);
  }

  @Override
  @Transactional
  public List<EndingPostPositionDicRelEntity> insertEndingPostPositionDicRels(
      List<EndingPostPositionDicRelEntity> endingPostPositionDicRelEntities) {
    return endingPostPositionDicRelRepository.save(endingPostPositionDicRelEntities);
  }

  @Override
  @Transactional
  public List<EndingPostPositionDicRelEntity> updateEndingPostPositionDicRels(
      List<EndingPostPositionDicRelEntity> endingPostPositionDicRelEntities) {
    return endingPostPositionDicRelRepository.save(endingPostPositionDicRelEntities);
  }

  @Override
  @Transactional
  public void deleteEndingPostPositionDicRels(
      List<EndingPostPositionDicRelEntity> endingPostPositionDicRelEntities) {
    endingPostPositionDicRelRepository.delete(endingPostPositionDicRelEntities);
  }
}
