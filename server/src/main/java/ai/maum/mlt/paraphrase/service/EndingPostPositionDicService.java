package ai.maum.mlt.paraphrase.service;


import ai.maum.mlt.paraphrase.entity.EndingPostPositionDicEntity;
import java.util.List;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

public interface EndingPostPositionDicService {

  List<EndingPostPositionDicEntity> selectListByWordsAndWorkspaceId(List<String> wordList,
      String workspaceId);

  Page<EndingPostPositionDicEntity> selectListByWorkspaceId(String workspaceId, String type,
      String searchRepresentativeWord, String searchWord, Pageable pageable);

  EndingPostPositionDicEntity insertEndingPostPositionDic(
      EndingPostPositionDicEntity endingPostPositionDicEntity);

  EndingPostPositionDicEntity updateEndingPostPositionDic(
      EndingPostPositionDicEntity endingPostPositionDicEntity);

  void deleteEndingPostPositionDics(
      List<EndingPostPositionDicEntity> endingPostPositionDicEntities);

  void deleteEndingPostPositionDicsByWorkspaceId(String workspaceId);

  void uploadFiles(List<MultipartFile> files, String workspaceId) throws Exception;

  InputStreamResource downloadFile(String workspaceId) throws Exception;
}
