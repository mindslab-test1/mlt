package ai.maum.mlt.paraphrase.repository;


import ai.maum.mlt.paraphrase.entity.SynonymDicRelEntity;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Transactional
@Repository
public interface SynonymDicRelRepository extends JpaRepository<SynonymDicRelEntity, String> {

  @Query(value = "SELECT SDRE "
      + "         FROM SynonymDicRelEntity SDRE "
      + "         WHERE SDRE.synonymWord in(:keyword)"
      + "         AND SDRE.workspaceId = :workspaceId"
  )
  List<SynonymDicRelEntity> findAllByInKeywordAndWorkspaceId(@Param("keyword") List keyword,
      @Param("workspaceId") String workspaceId);


}
