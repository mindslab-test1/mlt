package ai.maum.mlt.paraphrase.service;


import ai.maum.mlt.paraphrase.entity.SynonymDicEntity;
import java.util.List;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

public interface SynonymDicService {

  List<SynonymDicEntity> selectListByWordsAndWorkspaceId(List<String> wordList, String workspaceId);

  Page<SynonymDicEntity> selectListByWorkspaceId(String workspaceId,
      String searchRepresentativeWord, String searchSynonym, Pageable pageable);

  SynonymDicEntity insertSynonymDic(SynonymDicEntity synonymDicEntity);

  SynonymDicEntity updateSynonymDic(SynonymDicEntity synonymDicEntity);

  void deleteSynonymDics(List<SynonymDicEntity> synonymDicEntities);

  void deleteSynonymDicsByWorkspaceId(String workspaceId);

  void uploadFiles(List<MultipartFile> files, String workspaceId) throws Exception;

  InputStreamResource downloadFile(String workspaceId) throws Exception;
}
