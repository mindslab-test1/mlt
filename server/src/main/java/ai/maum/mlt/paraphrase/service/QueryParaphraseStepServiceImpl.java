package ai.maum.mlt.paraphrase.service;

import ai.maum.mlt.paraphrase.entity.QueryParaphraseEntity;
import ai.maum.mlt.paraphrase.entity.QueryParaphraseStepEntity;
import ai.maum.mlt.paraphrase.repository.QueryParaphraseRepository;
import ai.maum.mlt.paraphrase.repository.QueryParaphraseStepRepository;
import java.util.Date;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class QueryParaphraseStepServiceImpl implements QueryParaphraseStepService {

  @Autowired
  private QueryParaphraseRepository queryParaphraseRepository;

  @Autowired
  private QueryParaphraseStepRepository queryParaphraseStepRepository;

  @Override
  @Transactional
  public QueryParaphraseEntity insertWithQueryParaphrase(
      QueryParaphraseStepEntity queryParaphraseStepEntity, String[] sentenceList) {

    if (queryParaphraseStepEntity.getQueryParaphraseId() != null) {
      // 1. 파일 업로드 시 기존에 올린 파일
      this.queryParaphraseRepository
          .deleteByIdAndWorkspaceId(queryParaphraseStepEntity.getQueryParaphraseId(),
              queryParaphraseStepEntity.getWorkspaceId());
      this.queryParaphraseStepRepository
          .deleteByQueryParaphraseIdAndWorkspaceId(queryParaphraseStepEntity.getQueryParaphraseId(),
              queryParaphraseStepEntity.getWorkspaceId());
    }

    QueryParaphraseEntity queryParaphraseEntity = new QueryParaphraseEntity();
    queryParaphraseEntity.setTitle(queryParaphraseStepEntity.getTitle());
    queryParaphraseEntity.setCurrentStep(queryParaphraseStepEntity.getStepCd());
    queryParaphraseEntity.setWorkspaceId(queryParaphraseStepEntity.getWorkspaceId());
    queryParaphraseEntity.setCreatedAt(new Date());

    // MLT_QUERY_PRPRS 테이블에 데이터 INSERT
    QueryParaphraseEntity result =
        this.queryParaphraseRepository.save(queryParaphraseEntity);

    // MLT_QUERY_PRPRS_STEP 테이블에 데이터 INSERT
    for (String sentence : sentenceList) {
      QueryParaphraseStepEntity temp = new QueryParaphraseStepEntity();
      temp.setCreatedAt(new Date());
      temp.setQueryParaphraseId(result.getId());
      temp.setWorkspaceId(queryParaphraseEntity.getWorkspaceId());
      temp.setSentence(sentence);
      temp.setStepCd(queryParaphraseEntity.getCurrentStep());

      this.queryParaphraseStepRepository.save(temp);
    }

    return result;
  }

  @Override
  @Transactional
  public void insertAfterGetParaphrase(
      List<QueryParaphraseStepEntity> queryParaphraseStepEntities) {

    // insert
    this.queryParaphraseStepRepository.save(queryParaphraseStepEntities);

    // update
    this.queryParaphraseRepository
        .updateSetCurrentStep(queryParaphraseStepEntities.get(0).getStepCd(),
            queryParaphraseStepEntities.get(0).getQueryParaphraseId(),
            queryParaphraseStepEntities.get(0).getWorkspaceId());
  }

  @Override
  public Page<QueryParaphraseStepEntity> getSentences(
      String queryParaphraseId, String workspaceId, String stepCd,
      Pageable pageable, String sentence) {
    return this.queryParaphraseStepRepository
        .getSentences(queryParaphraseId, workspaceId, stepCd, pageable, sentence);
  }

  @Override
  public List<QueryParaphraseStepEntity> getSentences(String queryParaphraseId, String workspaceId,
      String stepCd) {
    return this.queryParaphraseStepRepository.getSentences(queryParaphraseId, workspaceId, stepCd);
  }

  @Override
  @Transactional
  public void deleteSentences(List<QueryParaphraseStepEntity> queryParaphraseStepEntities) {
    this.queryParaphraseStepRepository.delete(queryParaphraseStepEntities);
  }
}