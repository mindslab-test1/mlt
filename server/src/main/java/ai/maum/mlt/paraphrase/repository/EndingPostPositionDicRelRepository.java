package ai.maum.mlt.paraphrase.repository;


import ai.maum.mlt.paraphrase.entity.EndingPostPositionDicRelEntity;
import ai.maum.mlt.paraphrase.entity.SynonymDicRelEntity;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Transactional
@Repository
public interface EndingPostPositionDicRelRepository extends JpaRepository<EndingPostPositionDicRelEntity, String> {

  @Query(value = "SELECT EPPDRE "
      + "         FROM EndingPostPositionDicRelEntity EPPDRE "
      + "         WHERE EPPDRE.word in(:keyword)"
      + "         AND EPPDRE.workspaceId = :workspaceId"
  )
  List<EndingPostPositionDicRelEntity> findAllByInKeywordAndWorkspaceId(@Param("keyword") List keyword,
      @Param("workspaceId") String workspaceId);


}
