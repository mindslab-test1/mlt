package ai.maum.mlt.paraphrase.service;

import ai.maum.mlt.paraphrase.entity.EndingPostPositionDicRelEntity;
import ai.maum.mlt.paraphrase.entity.SynonymDicRelEntity;
import java.util.List;

public interface EndingPostPositionDicRelService {

  List<EndingPostPositionDicRelEntity> selectListByWordsAndWorkspaceId(List<String> wordList,
      String workspaceId);

  List<EndingPostPositionDicRelEntity> insertEndingPostPositionDicRels(List<EndingPostPositionDicRelEntity> endingPostPositionDicRelEntities);

  List<EndingPostPositionDicRelEntity> updateEndingPostPositionDicRels(List<EndingPostPositionDicRelEntity> endingPostPositionDicRelEntities);

  void deleteEndingPostPositionDicRels(List<EndingPostPositionDicRelEntity> endingPostPositionDicRelEntities);
}
