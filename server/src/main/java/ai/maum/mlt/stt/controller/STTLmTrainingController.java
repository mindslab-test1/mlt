package ai.maum.mlt.stt.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.common.file.entity.FileGroupEntity;
import ai.maum.mlt.common.file.service.FileGroupService;
import ai.maum.mlt.common.system.SystemCode;
import ai.maum.mlt.stt.entity.STTModelEntity;
import ai.maum.mlt.stt.entity.STTTrainingEntity;
import ai.maum.mlt.stt.service.STTLmTrainingService;
import ai.maum.mlt.stt.service.STTModelService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import maum.brain.stt.train.S3Train.TrainResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/stt/lm/training")
public class STTLmTrainingController {

  private static final Logger logger = LoggerFactory.getLogger(STTLmTrainingController.class);

  @Autowired
  private STTModelService modelService;

  @Autowired
  private FileGroupService fileGroupService;

  @Autowired
  private STTLmTrainingService sttLmTrainingService;

  @UriRoleDesc(role = "stt^R")
  @RequestMapping(
      value = "/getFileGroups",
      method = RequestMethod.POST)
  public ResponseEntity<?> getFileGroups(@RequestBody FileGroupEntity param) {
    logger.info("===== call api [[/api/stt/lm/training/getFileGroups]] :: {}", param);
    try {
      List<FileGroupEntity> lmFileGroupEntities = fileGroupService
          .getFileGroupList(SystemCode.FILE_GRP_PPOS_STT_LM, param.getWorkspaceId());

      List<FileGroupEntity> amFileGroupEntities = fileGroupService
          .getFileGroupList(SystemCode.FILE_GRP_PPOS_STT_AM, param.getWorkspaceId());

      List<FileGroupEntity> result = new ArrayList<>();
      result.addAll(lmFileGroupEntities);
      result.addAll(amFileGroupEntities);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "stt^R")
  @RequestMapping(
      value = "/sttOpen",
      method = RequestMethod.POST)
  public ResponseEntity<?> open(@RequestBody HashMap<String, Object> param) {
    logger.info("===== call api  [[/api/stt/lm/training/sttOpen]] :: {}", param);
    HashMap<String, Object> result = new HashMap<>();
    try {
      sttLmTrainingService.open(param.get("fileGroupId").toString(),
          param.get("workspaceId").toString(), param.get("trainType").toString());
      result.put("message", "SUCCESS");
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("open e : " , e);
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "stt^R")
  @RequestMapping(
      value = "/getTrainedSttModels",
      method = RequestMethod.POST)
  public ResponseEntity<?> getTrainedSttModels(@RequestBody String workspaceId) {
    logger.info("===== call api  [[/api/stt/lm/training/getTrainedSttModels]] :: workspaceId {} ",
        workspaceId);
    HashMap<String, Object> result = new HashMap<>();
    try {
      List<STTModelEntity> tmList = modelService
          .selectTrainedModelList(workspaceId, TrainResult.success.toString(),
              SystemCode.FILE_GRP_PPOS_STT_LM);
      result.put("tmList", tmList);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getTrainedSttModels e : " , e);
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "stt^U")
  @RequestMapping(
      value = "/{id}/{workspaceId}/update/{key}",
      method = RequestMethod.POST)
  public void update(
      @PathVariable("key") String key,
      @PathVariable("id") String id,
      @PathVariable("workspaceId") String workspaceId,
      @RequestBody Object data) {
    logger.info(
        "===== call api  [[/api/stt/lm/training/{id}/{workspaceId}/update/{key}]] :: key {}, id {}, workspaceId {}, data {}",
        key, id, workspaceId, data);
    try {
      sttLmTrainingService.update(key, id, workspaceId, data);
    } catch (Exception e) {
      logger.error("update e : " , e);
    }
  }

  @UriRoleDesc(role = "stt^X")
  @RequestMapping(
      value = "/getAllProgress",
      method = RequestMethod.POST)
  public ResponseEntity<?> getAllProgress(@RequestBody String workspaceId) {
    logger.info("===== call api  [[/api/stt/lm/training/getAllProgress]] :: workspaceId{}",
        workspaceId);
    HashMap<String, Object> result = new HashMap<>();
    try {
      List<STTTrainingEntity> sttTrainingEnties = sttLmTrainingService.getAllProgress(workspaceId);
      result.put("tmList", sttTrainingEnties);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getAllProgress e : " , e);
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "stt^X")
  @RequestMapping(
      value = "/stop",
      method = RequestMethod.POST)
  public ResponseEntity<?> stop(@RequestBody String val) {
    logger.info("===== call api  [[/api/stt/lm/training/stop]]:: val {}", val);
    HashMap<String, Object> result = new HashMap<>();
    try {
      sttLmTrainingService.stop(val);
      result.put("message", "SUCCESS");
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("stop e : " , e);
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "stt^X")
  @RequestMapping(
      value = "/close",
      method = RequestMethod.POST)
  public ResponseEntity<?> close(@RequestBody String val) {
    logger.info("===== call api  [[/api/stt/lm/training/close]] :: val {}", val);
    HashMap<String, Object> result = new HashMap<String, Object>();
    try {
      sttLmTrainingService.close(val);
      result.put("message", "SUCCESS");
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("close e : " , e);
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "stt^D")
  @RequestMapping(
      value = "/delete",
      method = RequestMethod.POST)
  public ResponseEntity<?> delete(@RequestBody String val) {
    logger.info("===== call api  [[/api/stt/lm/training/delete]] :: val {}", val);
    HashMap<String, Object> result = new HashMap<>();
    try {
      sttLmTrainingService.delete(val);
      result.put("message", "success");
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("delete e : " , e);
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
