package ai.maum.mlt.stt.service;

import ai.maum.mlt.stt.entity.STTTranscriptEntity;
import ai.maum.mlt.stt.repository.STTTranscriptRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
public class STTTranscriptServiceImpl implements STTTranscriptService {

  private static final Logger logger = LoggerFactory.getLogger(STTTranscriptServiceImpl.class);

  @Autowired
  private STTTranscriptRepository sttTranscriptRepository;

  /**
   * name, workspaceId, fileGroupId 로 STTTranscriptEntity 페이징하여 가져오는 로직
   *
   * @param param STTTranscriptEntity
   * @return Page<STTTranscriptEntity>
   */
  @Override
  public Page<STTTranscriptEntity> selectTranscriptList(STTTranscriptEntity param)
      throws Exception {
    logger.debug("===== selectTranscriptList :: STTTranscriptEntity {}", param);
    try {
      return sttTranscriptRepository
          .findAllbyName(param.getName(), param.getWorkspaceId(), param.getFileGroupId(),
              param.getPageRequest());
    } catch (Exception e) {
      logger.error("===== selectTranscriptList :: {}", e.getMessage());
      throw e;
    }
  }

  /**
   * Id로 STTTranscriptEntity 조회하는 로직
   *
   * @param param STTTranscriptEntity
   * @return STTTranscriptEntity
   */
  @Override
  public STTTranscriptEntity selectTranscript(STTTranscriptEntity param) throws Exception {
    logger.debug("===== selectTranscript :: STTTranscriptEntity {}", param);
    try {
      return sttTranscriptRepository.findOne(param.getId());
    } catch (Exception e) {
      logger.error("===== selectTranscript :: {}", e.getMessage());
      throw e;
    }
  }

  /**
   * 전사완료된 Transcript 개수 조회하는 로직
   *
   * @param purpose String
   * @param workspaceId String
   * @return Integer
   */
  @Override
  public Integer selectTranscriptCompleteCount(String purpose, String workspaceId)
      throws Exception {
    logger.debug("===== selectTranscriptCompleteCount :: purpose {}, workspaceId {}", purpose,
        workspaceId);
    try {
      return sttTranscriptRepository.countAllByTranscriptComplete(purpose, workspaceId);
    } catch (Exception e) {
      logger.error("===== selectTranscriptCompleteCount :: {}", e.getMessage());
      throw e;
    }
  }

  /**
   * QA완료된 Transcript 개수 조회하는 로직
   *
   * @param purpose String
   * @param workspaceId String
   * @return Integer
   */
  @Override
  public Integer selectQualityAssuranceCompleteCount(String purpose, String workspaceId)
      throws Exception {
    logger.debug("===== selectQualityAssuranceCompleteCount :: purpose {}, workspaceId {}", purpose,
        workspaceId);
    try {
      return sttTranscriptRepository.countAllByQualityAssuranceComplete(purpose, workspaceId);
    } catch (Exception e) {
      logger.error("===== selectQualityAssuranceCompleteCount :: {}", e.getMessage());
      throw e;
    }
  }

  /**
   * FileID, FileGroupId, WorkspaceId로 STTTranscriptEntity 조회하는 로직
   *
   * @param fileId String
   * @param fileGroupId String
   * @param workspaceId String
   * @return STTTranscriptEntity
   */
  @Override
  public STTTranscriptEntity selectTranscriptByFileIdAndFileGroupIdAndWorkspaceId(String fileId,
      String fileGroupId, String workspaceId) throws Exception {
    logger.debug(
        "===== selectTranscriptByFileIdAndFileGroupIdAndWorkspaceId ::fileId {}, fileGroupId {}, workspaceId {}",
        fileId, fileGroupId, workspaceId);
    try {
      return sttTranscriptRepository
          .findByFileIdAndFileGroupIdAndWorkspaceId(fileId, fileGroupId, workspaceId);
    } catch (Exception e) {
      logger.error("===== selectTranscriptByFileIdAndFileGroupIdAndWorkspaceId :: {}",
          e.getMessage());
      throw e;
    }
  }

  /**
   * STTTranscriptEntity insert하는 로직
   *
   * @param param STTTranscriptEntity
   * @return STTTranscriptEntity
   */
  @Override
  @Transactional
  public STTTranscriptEntity insertTranscript(STTTranscriptEntity param) throws Exception {
    logger.debug("===== insertTranscript :: STTTranscriptEntity {}", param);
    try {
      Date date = new Date();
      param.setCreatedAt(date);
      param.setUpdatedAt(date);
      return sttTranscriptRepository.save(param);
    } catch (Exception e) {
      logger.error("===== insertTranscript :: {}", e.getMessage());
      throw e;
    }
  }

  /**
   * STTTranscriptEntity update 하는 로직
   *
   * @param param STTTranscriptEntity
   * @return STTTranscriptEntity
   */
  @Override
  @Transactional
  public STTTranscriptEntity updateTranscript(STTTranscriptEntity param) throws Exception {
    logger.debug("===== updateTranscript :: STTTranscriptEntity {}", param);
    try {
      sttTranscriptRepository.updateVersion(param.getId(), param.getVersion());
      return sttTranscriptRepository.findOne(param.getId());
    } catch (Exception e) {
      logger.error("===== updateTranscript :: {}", e.getMessage());
      throw e;
    }
  }

  /**
   * STTTranscriptEntity alert와 함께 update하는 로직
   *
   * @param param STTTranscriptEntity
   * @return STTTranscriptEntity
   */
  @Override
  @Transactional
  public STTTranscriptEntity updateAlert(STTTranscriptEntity param) throws Exception {
    logger.debug("===== updateAlert :: STTTranscriptEntity {}", param);
    try {
      sttTranscriptRepository.updateAlert(param.getId(), param.getAlert());
      return sttTranscriptRepository.findOne(param.getId());
    } catch (Exception e) {
      logger.error("===== updateAlert :: {}", e.getMessage());
      throw e;
    }
  }

  /**
   * STTTranscriptEntity delete하는 로직
   *
   * @param param STTTranscriptEntity Ids에 삭제할 STTTranscriptEntity id 배열로 넣음
   */
  @Override
  @Transactional
  public void deleteTranscript(STTTranscriptEntity param) throws Exception {
    logger.debug("===== deleteTranscript :: STTTranscriptEntity {}", param);
    try {
      List<STTTranscriptEntity> sttTranscriptEntityList = new ArrayList<>();
      for (String id : param.getIds()) {
        STTTranscriptEntity sttTranscriptEntity = new STTTranscriptEntity();
        sttTranscriptEntity.setId(id);
        sttTranscriptEntityList.add(sttTranscriptEntity);
      }
      sttTranscriptRepository.delete(sttTranscriptEntityList);
    } catch (Exception e) {
      logger.error("===== deleteTranscript :: {}", e.getMessage());
      throw e;
    }
  }

  /**
   * FileGroupId로 STTTranscriptEntity delete하는 로직
   *
   * @param fileGroupId String
   */
  @Override
  @Transactional
  public void deleteTranscriptByFileGroupId(String fileGroupId) throws Exception {
    logger.debug("===== deleteTranscriptByFileGroupId :: fileGroupId {}", fileGroupId);
    try {
      sttTranscriptRepository.deleteAllByFileGroupId(fileGroupId);
    } catch (Exception e) {
      logger.error("===== deleteTranscriptByFileGroupId :: {}", e.getMessage());
      throw e;
    }
  }
}
