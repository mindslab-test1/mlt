package ai.maum.mlt.stt.service;

import ai.maum.mlt.stt.entity.STTAnalysisResultEntity;
import ai.maum.mlt.stt.entity.STTEvalResultEntity;
import java.util.List;

public interface STTEvaluationService {

  List<STTEvalResultEntity> evaluate(List<STTAnalysisResultEntity> param) throws Exception;

}
