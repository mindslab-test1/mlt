package ai.maum.mlt.stt.service;

import ai.maum.mlt.stt.entity.STTAnalysisResultEntity;
import ai.maum.mlt.stt.entity.STTModelEntity;
import ai.maum.mlt.stt.repository.STTModelRepository;
import java.util.List;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class STTModelServiceImpl implements STTModelService {

  private static final Logger logger = LoggerFactory.getLogger(STTModelServiceImpl.class);

  @Autowired
  private STTModelRepository sttModelRepository;

  @Autowired
  private STTAnalysisResultService sttAnalysisResultService;

  /**
   * STT Key 값으로 STTModelEntity 조회하는 로
   *
   * @param key String
   * @return STTModelEntity
   */
  @Override
  public STTModelEntity selectTrainingModel(String key) {
    logger.debug("===== selectTrainingModel :: key {}", key);
    try {
      return sttModelRepository.findBySttKey(key);
    } catch (Exception e) {
      logger.error("===== selectTrainingModel :: {}", e.getMessage());
      throw e;
    }
  }

  /**
   * workspaceId와 status로 STTModelEntity 조회하는 로직
   *
   * @param workspaceId String
   * @param status String
   * @return List<STTModelEntity>
   */
  @Override
  public List<STTModelEntity> selectTrainedModelList(String workspaceId, String status)
      throws Exception {
    logger.debug("===== selectTrainedModelList :: workspaceId {}, status {}", workspaceId, status);
    try {
      List<STTModelEntity> sttModelEntities = sttModelRepository
          .findByWorkspaceIdAndStatus(workspaceId, status);
      return sttModelEntities;
    } catch (Exception e) {
      logger.error("===== selectTrainedModelList :: {}", e.getMessage());
      throw e;
    }
  }

  /**
   * workspaceId, status, purpose 로 STTModelEntity 조회하는 로직
   *
   * @param workspaceId String
   * @param status String
   * @param purpose String
   * @return List<STTModelEntity>
   */
  @Override
  public List<STTModelEntity> selectTrainedModelList(String workspaceId, String status,
      String purpose) throws Exception {
    logger
        .debug("===== selectTrainedModelList :: workspaceId {}, status {}, purpose {}", workspaceId,
            status, purpose);
    try {
      List<STTModelEntity> sttModelEntities = sttModelRepository
          .findByWorkspaceIdAndStatusAndPurpose(workspaceId, status, purpose);
      return sttModelEntities;
    } catch (Exception e) {
      logger.error("===== selectTrainedModelList :: {}", e.getMessage());
      throw e;
    }
  }

  /**
   * name, purpose, rate, workspaceId 로 STTModelEntity 조회하는 로직
   *
   * @param name String
   * @param purpose String
   * @param rate int
   * @param workspaceId String
   * @return STTModelEntity
   */
  @Override
  public STTModelEntity selectByNameAndPurposeAndRateAndWorkspaceId(String name, String purpose,
      int rate, String workspaceId) throws Exception {
    logger.debug(
        "===== selectByNameAndPurposeAndRateAndWorkspaceId :: name {}, purpose {}, rate {}, workspaceId {}",
        name, purpose, rate, workspaceId);
    try {
      return sttModelRepository
          .findByNameAndPurposeAndRateAndWorkspaceId(name, purpose, rate, workspaceId);
    } catch (Exception e) {
      logger.error("===== selectByNameAndPurposeAndRateAndWorkspaceId :: {}", e.getMessage());
      throw e;
    }
  }

  /**
   * STTModelEntity insert하는 로직
   *
   * @return STTModelEntity
   */
  @Override
  public STTModelEntity insertTrainingModel(STTModelEntity sttModelEntity) throws Exception {
    logger.debug("===== insertTrainingModel :: STTModelEntity {}", sttModelEntity);
    try {
      return sttModelRepository.save(sttModelEntity);
    } catch (Exception e) {
      logger.error("===== insertTrainingModel :: {}", e.getMessage());
      throw e;
    }
  }

  /**
   * STTModelEntity update 하는 로직
   *
   * @return STTModelEntity
   */
  @Override
  public STTModelEntity updateTrainingModel(STTModelEntity sttModelEntity) throws Exception {
    logger.debug("===== updateTrainingModel :: STTModelEntity {}", sttModelEntity);
    try {
      return sttModelRepository.saveAndFlush(sttModelEntity);
    } catch (Exception e) {
      logger.error("===== updateTrainingModel :: {}", e.getMessage());
      throw e;
    }
  }

  /**
   * STTModelEntity 삭제하는 로직 STTAnalysisResultEntity가 하위 Entity이기 때문에 먼저 석제후 STTModelEntity 삭제
   */
  @Override
  @Transactional
  public void deleteTrainingModel(STTModelEntity sttModelEntity) throws Exception {
    logger.debug("===== deleteTrainingModel :: STTModelEntity {}", sttModelEntity);
    try {
      // STTAnalysisResultEntity 가 child라 삭제 해주고 삭제해야함
      List<STTAnalysisResultEntity> analysisResultEntityList = sttAnalysisResultService
          .selectResultByModelId(sttModelEntity.getId());
      sttAnalysisResultService.deleteResult(analysisResultEntityList);
      sttModelRepository.deleteById(sttModelEntity.getId());
    } catch (Error e) {
      logger.error("===== deleteTrainingModel :: {}", e.getMessage());
      throw e;
    }
  }
}
