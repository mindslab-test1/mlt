package ai.maum.mlt.stt.repository;

import ai.maum.mlt.stt.entity.STTEvalResultEntity;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface STTEvalResultRepository extends JpaRepository<STTEvalResultEntity, String> {

  @Query(value = "SELECT a "
      + "         FROM STTEvalResultEntity a"
      + "         WHERE (a.sttAnalysisResultEntity.fileGroupId in (SELECT g.id FROM FileGroupEntity g WHERE g.name LIKE CONCAT('%', :fileGroupName, '%')) OR :fileGroupName IS NULL)"
      + "         AND (a.sttAnalysisResultEntity.fileEntity.name LIKE CONCAT('%', :fileName, '%') OR :fileName IS NULL)"
      + "         AND (a.sttAnalysisResultEntity.amModelId in (SELECT am.id FROM STTModelEntity am WHERE am.purpose = 'STT_AM'"
      + "               AND am.name LIKE CONCAT('%', :amModelName ,'%')) OR :amModelName IS NULL)"
      + "         AND (a.sttAnalysisResultEntity.lmModelId in (SELECT lm.id FROM STTModelEntity lm WHERE lm.purpose = 'STT_LM' "
      + "               AND lm.name LIKE CONCAT('%', :lmModelName ,'%')) OR :lmModelName IS NULL)")
  Page<STTEvalResultEntity> findAllByParam(@Param("fileGroupName") String fileGroupName,
      @Param("fileName") String fileName,
      @Param("amModelName") String amModelName, @Param("lmModelName") String lmModelName,
      Pageable pageable);

  Page<STTEvalResultEntity> findAllByWorkspaceId(String workspaceId, Pageable pageable);

  void deleteByWorkspaceId(String workspaceId);

  void deleteAllByAnalysisResultId(String analysisResultId);

  void deleteAllByAnalysisResultId(List<String> analysisResultIdList);

//  void deleteAllByAnalysisResultIdIn(List<String> analysisResultIdList);
}
