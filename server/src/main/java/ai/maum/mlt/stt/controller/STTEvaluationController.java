package ai.maum.mlt.stt.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.common.file.entity.FileGroupEntity;
import ai.maum.mlt.common.file.service.FileGroupService;
import ai.maum.mlt.common.system.SystemCode;
import ai.maum.mlt.stt.entity.STTAnalysisResultEntity;
import ai.maum.mlt.stt.entity.STTEvalResultEntity;
import ai.maum.mlt.stt.entity.STTModelEntity;
import ai.maum.mlt.stt.service.STTAnalysisResultService;
import ai.maum.mlt.stt.service.STTEvalResultService;
import ai.maum.mlt.stt.service.STTEvaluationService;
import ai.maum.mlt.stt.service.STTModelService;
import java.util.HashMap;
import java.util.List;
import maum.brain.stt.train.S3Train.TrainResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/stt/evaluation")
public class STTEvaluationController {

  private static final Logger logger = LoggerFactory.getLogger(STTEvaluationController.class);

  @Value("${stt.eval.server.ip}")
  private String ip;
  @Value("${stt.eval.server.port}")
  private int port;

  @Autowired
  private STTEvalResultService sttEvalResultService;

  @Autowired
  private STTAnalysisResultService sttAnalysisResultService;

  @Autowired
  private STTModelService sttModelService;

  @Autowired
  private FileGroupService fileGroupService;

  @Autowired
  private STTEvaluationService sttEvaluationService;

  @UriRoleDesc(role = "stt^R")
  @RequestMapping(
      value = "/getFileGroups",
      method = RequestMethod.POST)
  public ResponseEntity<?> getFileGroups(@RequestBody FileGroupEntity param) {
    logger.info("===== call api [[api/stt/analysis/getFileGroups]] :: {}", param);
    try {
      List<FileGroupEntity> fileGroupEntities = fileGroupService
          .getFileGroupList(SystemCode.FILE_GRP_PPOS_STT_AM, param.getWorkspaceId());

      return new ResponseEntity<>(fileGroupEntities, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getFileGroups e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "stt^R")
  @RequestMapping(
      value = "/getLmModels",
      method = RequestMethod.POST)
  public ResponseEntity<?> getLmModels(@RequestBody STTModelEntity param) {
    logger.info("===== call api [[api/stt/evaluation/getLmModels]] :: {}", param);
    try {
      List<STTModelEntity> resultList = sttModelService
          .selectTrainedModelList(param.getWorkspaceId(), TrainResult.success.toString(),
              SystemCode.FILE_GRP_PPOS_STT_LM);

      return new ResponseEntity<>(resultList, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getLmModels e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "stt^R")
  @RequestMapping(
      value = "/getAmModels",
      method = RequestMethod.POST)
  public ResponseEntity<?> getAmModels(@RequestBody STTModelEntity param) {
    logger.info("===== call api [[api/stt/evaluation/getAmModels]] :: {}", param);
    try {
      List<STTModelEntity> resultList = sttModelService
          .selectTrainedModelList(param.getWorkspaceId(), TrainResult.success.toString(),
              SystemCode.FILE_GRP_PPOS_STT_AM);

      return new ResponseEntity<>(resultList, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getAmModels e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "stt^R")
  @RequestMapping(
      value = "/getAnalysisResultList",
      method = RequestMethod.POST)
  public ResponseEntity<?> getAnalysisResultList(@RequestBody STTAnalysisResultEntity param) {
    logger.info("===== call api [[api/stt/evaluation/getAnalysisResultList]] :: {}", param);
    try {
      Page<STTAnalysisResultEntity> resultList = sttAnalysisResultService
          .selectResultByFileGroupIdAndAmModelIdAndLmModelIdAndName(param);
      return new ResponseEntity<>(resultList, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getAnalysisResultList e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "stt^R")
  @RequestMapping(
      value = "/get-evaluation-result",
      method = RequestMethod.POST)
  public ResponseEntity<?> getEvaluationResultList(@RequestBody STTEvalResultEntity param) {
    logger.info("===== call api [[api/stt/evaluation/get-evaluation-result]] :: {}", param);
    try {
      Page<STTEvalResultEntity> sttEvalResultEntities =
          sttEvalResultService.selectEvaluationResultList(param);
      return new ResponseEntity<>(sttEvalResultEntities, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getEvaluationResultList e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "stt^D")
  @RequestMapping(
      value = "/delete-evaluation-result",
      method = RequestMethod.POST)
  public ResponseEntity<?> deleteEvaluationResultList(
      @RequestBody List<STTEvalResultEntity> param) {
    logger.info("===== call api [[api/stt/evaluation/delete-evaluation-result]] :: {}", param);
    try {
      sttEvalResultService.deleteEvaluationResultList(param);
      return new ResponseEntity<>(HttpStatus.OK);
    } catch (Exception e) {
      logger.error("deleteEvaluationResultList e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "stt^X")
  @RequestMapping(
      value = "/evaluate",
      method = RequestMethod.POST)
  public ResponseEntity<?> analyze(@RequestBody List<STTAnalysisResultEntity> param) {
    logger.info("===== call api [[api/stt/evaluation/evaluate]] :: {}", param);
    try {
      List<STTEvalResultEntity> result = sttEvaluationService.evaluate(param);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("analyze e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
