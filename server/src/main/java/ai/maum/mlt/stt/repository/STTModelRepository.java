package ai.maum.mlt.stt.repository;

import ai.maum.mlt.stt.entity.STTModelEntity;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface STTModelRepository extends JpaRepository<STTModelEntity, String> {

  STTModelEntity findBySttKey(String key);

  List<STTModelEntity> findAllByWorkspaceId(String workspaceId);

  void deleteById(String id);

  List<STTModelEntity> findByWorkspaceIdAndStatusAndPurpose(String workspaceId, String status,
      String purpose);

  List<STTModelEntity> findByWorkspaceIdAndStatus(String workspaceId, String status);

  STTModelEntity findByNameAndPurposeAndRateAndWorkspaceId(String name, String purpose, int rate,
      String workspaceId);
}
