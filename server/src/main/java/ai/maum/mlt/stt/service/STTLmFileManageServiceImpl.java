package ai.maum.mlt.stt.service;

import ai.maum.mlt.common.file.entity.FileEntity;
import ai.maum.mlt.common.file.entity.FileGroupEntity;
import ai.maum.mlt.common.file.entity.FileGroupRelEntity;
import ai.maum.mlt.common.file.repository.FileGroupRelRepository;
import ai.maum.mlt.common.file.repository.FileRepository;
import ai.maum.mlt.common.file.service.FileGroupService;
import ai.maum.mlt.common.file.service.FileService;
import ai.maum.mlt.common.system.SystemCode;
import ai.maum.mlt.common.system.SystemErrMsg;
import ai.maum.mlt.common.util.FileUtils;
import ai.maum.mlt.stt.entity.STTTranscriptEntity;
import ai.maum.mlt.stt.repository.STTTranscriptRepository;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class STTLmFileManageServiceImpl implements STTLmFileManageService {

  private static final Logger logger = LoggerFactory.getLogger(STTLmFileManageServiceImpl.class);

  @Value("${stt.file.path}")
  private String sttPath;

  @Autowired
  private FileUtils fileUtils;

  @Autowired
  private FileService fileService;

  @Autowired
  private FileGroupService fileGroupService;

  @Autowired
  private FileRepository fileRepository;

  @Autowired
  private FileGroupRelRepository fileGroupRelRepository;

  @Autowired
  private STTTranscriptRepository sttTranscriptRepository;

  @Autowired
  private STTTranscriptService sttTranscriptService;

  /**
   * STT LM 파일(transcript txt) 업로드 처리 로직 업로드를 진행하고 나서 STTTranscriptEntity에 내용을 저장함
   *
   * @param file 업로드 할 파일
   * @return HashMap (msg: 결과 메세지, name: 업로드 파일명, id: fileEntity ID값, isSuccess: 파일업로드 성공 여부)
   */
  @Override
  @Transactional
  public HashMap<String, Object> uploadFile(MultipartFile file, String workspaceId,
      String fileGroupId) throws Exception {
    logger.debug("===== uploadFile :: file {}, workspaceId {}, fileGroupId {}",
        file.getOriginalFilename(), workspaceId, fileGroupId);
    try {
      HashMap<String, Object> resultMap = fileService
          .insertSttFile(file, workspaceId, sttPath, SystemCode.FILE_GRP_PPOS_STT_LM,
              SystemCode.FILE_EXTENSION_TEXT, fileGroupId);
      if ((boolean) resultMap.get("isSuccess")) {
        insertFileContentToTranscript(workspaceId, fileGroupId, resultMap.get("id").toString());
      }
      return resultMap;
    } catch (Exception e) {
      logger.error("===== uploadFile :: {}", e.getMessage());
      throw e;
    }
  }

  /**
   * 파일 업로드는 하지 않고 기존에 업로드 되어있는 파일을 파일그룹에 포합시키는 로직 include 하면 STTTranscriptEntity도 같이 생성해줌
   *
   * @param fileEntity FileEntity의 workspaceId 값과 fileGroupId 값과 Ids에 include할 File Id값이 있는 배열값 필요
   */
  @Override
  @Transactional
  public void includeFile(FileEntity fileEntity) throws Exception {
    logger.debug("===== includeFile :: FileEntity {}", fileEntity);
    try {
      List<FileGroupRelEntity> fileGroupRelEntities = new ArrayList<>();
      for (String id : fileEntity.getIds()) {
        FileGroupRelEntity fileGroupRelEntity = new FileGroupRelEntity();
        fileGroupRelEntity.setFileId(id);
        fileGroupRelEntity.setFileGroupId(fileEntity.getFileGroupId());
        fileGroupRelEntities.add(fileGroupRelEntity);
        insertFileContentToTranscript(fileEntity.getWorkspaceId(), fileEntity.getFileGroupId(), id);
      }
      fileGroupService.insertFileGroupRel(fileGroupRelEntities);
    } catch (Exception e) {
      logger.error("===== includeFile :: {}", e.getMessage());
      throw e;
    }
  }

  /**
   * 파일 그룹에 포함된 파일중 파일을 삭제하지 않고 그룹에서 제외시키는 로직 관계가 있는 STTTranscriptEntity 삭제후 FileGroupRel삭제 진행
   *
   * @param fileEntity FileEntity의 workspaceId 값과 fileGroupId 값과 Ids에 exclude할 File Id값이 있는 배열값 필요
   */
  @Override
  @Transactional
  public void excludeFile(FileEntity fileEntity) throws Exception {
    logger.debug("===== excludeFile :: FileEntity {}", fileEntity);
    try {
      List<FileGroupRelEntity> fileGroupRelEntities = new ArrayList<>();

      for (String id : fileEntity.getIds()) {
        FileGroupRelEntity fileGroupRelEntity = new FileGroupRelEntity();
        fileGroupRelEntity.setFileId(id);
        fileGroupRelEntity.setFileGroupId(fileEntity.getFileGroupId());
        fileGroupRelEntities.add(fileGroupRelEntity);
        sttTranscriptRepository
            .deleteAllByFileIdAndWorkspaceIdAndFileGroupId(id, fileEntity.getWorkspaceId(),
                fileEntity.getFileGroupId());
      }
      fileGroupService.deleteFileGroupRel(fileGroupRelEntities);
    } catch (Exception e) {
      logger.error("===== excludeFile :: {}", e.getMessage());
      throw e;
    }
  }

  /**
   * 파일 삭제 하는 로직 FileEntity와 관계있는 Entity(STTTranscriptEntity, FileGroupRelEntity) 먼저 삭제후 FileEntity
   * 삭제함
   */
  @Override
  @Transactional
  public void deleteFile(FileEntity fileEntity) throws Exception {
    logger.debug("===== deleteFile :: FileEntity {}", fileEntity);
    try {
      for (String id : fileEntity.getIds()) {
        if (fileGroupRelRepository.countAllByFileId(id) > 1) {
          throw new Exception(SystemErrMsg.FILE_INCLUDED_IN_OTHER_FILE_GROUP);
        } else {
          sttTranscriptRepository
              .deleteAllByFileIdAndWorkspaceIdAndFileGroupId(id, fileEntity.getWorkspaceId(),
                  fileEntity.getFileGroupId());
        }
      }
      fileService.deleteFile(fileEntity);
      fileUtils.deleteFile(fileEntity.getIds(), sttPath, SystemCode.FILE_EXTENSION_TEXT);
    } catch (Exception e) {
      logger.error("===== deleteFile :: {}", e.getMessage());
      throw e;
    }
  }

  /**
   * 저장된 txt파일을 읽어와서 내용을 transcript 에 넣어주는 로직
   */
  private void insertFileContentToTranscript(String workspaceId, String fileGroupId, String fileId)
      throws Exception {
    logger.debug("===== insertFileContentToTranscript :: workspaceId {}, fileGroupId {}, fileId {}",
        workspaceId, fileGroupId, fileId);
    try {
      String totalLine;

      FileEntity file = fileRepository.findOne(fileId);
      STTTranscriptEntity sttTranscriptEntity = new STTTranscriptEntity();
      sttTranscriptEntity.setWorkspaceId(workspaceId);
      sttTranscriptEntity.setFileId(fileId);
      sttTranscriptEntity.setName(file.getName());
      // txt파일을 내용을 바로 db에 insert함
      String fileName = fileId + SystemCode.FILE_EXTENSION_TEXT;
      String path =
          fileUtils.getSystemConfPath(sttPath + "/" + workspaceId) + "/"
              + fileName;
      File readFile = new File(path);
      totalLine = "";
      BufferedReader br;

      br = new BufferedReader(new FileReader(readFile));
      String line;
      while ((line = br.readLine()) != null) {
        if (!(line.equals(""))) {
          line = line.trim().replaceAll("([^.])$", "$1.") + '\n';
          totalLine += line;
        }
      }
      sttTranscriptEntity.setVersion(totalLine);
      sttTranscriptEntity.setFileGroupId(fileGroupId);
      sttTranscriptEntity.setCreatedAt(new Date());
      sttTranscriptEntity.setUpdatedAt(new Date());
      sttTranscriptRepository.save(sttTranscriptEntity);
    } catch (Exception e) {
      logger.error("===== insertFileContentToTranscript :: {}", e.getMessage());
      throw e;
    }
  }

  /**
   * 파일그룹 삭제하는 로직 FileGroupEntity와 관계있는 Entity(STTTranscriptEntity, FileGroupRelEntity) 먼저 삭제후
   * FileGroupEntity 삭제 처리
   */
  @Override
  @Transactional
  public void deleteFileGroup(FileGroupEntity fileGroupEntity) throws Exception {
    logger.debug("===== deleteFileGroup :: fileGroupEntity {}", fileGroupEntity);
    try {
      sttTranscriptService.deleteTranscriptByFileGroupId(fileGroupEntity.getId());
      fileGroupService.deleteFileGroup(fileGroupEntity.getId());
    } catch (Exception e) {
      logger.error("===== deleteFileGroup :: {}", e.getMessage());
      throw e;
    }
  }
}
