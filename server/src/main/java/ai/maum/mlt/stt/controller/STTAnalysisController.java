package ai.maum.mlt.stt.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.common.file.entity.FileEntity;
import ai.maum.mlt.common.file.entity.FileGroupEntity;
import ai.maum.mlt.common.file.service.FileGroupService;
import ai.maum.mlt.common.file.service.FileService;
import ai.maum.mlt.common.system.SystemCode;
import ai.maum.mlt.stt.entity.STTAnalysisResultEntity;
import ai.maum.mlt.stt.entity.STTModelEntity;
import ai.maum.mlt.stt.service.STTAnalysisResultService;
import ai.maum.mlt.stt.service.STTAnalysisService;
import ai.maum.mlt.stt.service.STTModelService;
import java.util.HashMap;
import java.util.List;
import maum.brain.stt.train.S3Train.TrainResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/stt/analysis")
public class STTAnalysisController {

  private static final Logger logger = LoggerFactory.getLogger(STTAnalysisController.class);

  @Value("${stt.analysis.server.ip}")
  private String ip;

  @Value("${stt.analysis.server.port}")
  private int port;

  @Value("${stt.file.path}")
  private String sttPath;

  @Autowired
  private FileGroupService fileGroupService;

  @Autowired
  private STTModelService sttModelService;

  @Autowired
  private FileService fileService;

  @Autowired
  private STTAnalysisResultService sttAnalysisResultService;

  @Autowired
  private STTAnalysisService sttAnalysisService;

  @UriRoleDesc(role = "stt^R")
  @RequestMapping(
      value = "/getFileGroups",
      method = RequestMethod.POST)
  public ResponseEntity<?> getFileGroups(@RequestBody FileGroupEntity param) {
    logger.info("===== call api  GET [[api/stt/analysis/getFileGroups]] :: {}", param);
    try {
      List<FileGroupEntity> fileGroupEntities = fileGroupService
          .getFileGroupList(SystemCode.FILE_GRP_PPOS_STT_AM, param.getWorkspaceId());

      return new ResponseEntity<>(fileGroupEntities, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getFileGroups e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "stt^R")
  @RequestMapping(
      value = "/getLmModels",
      method = RequestMethod.POST)
  public ResponseEntity<?> getLmModels(@RequestBody STTModelEntity param) {
    logger.info("===== call api  GET [[api/stt/analysis/getLmModels]] :: {}", param);
    try {
      List<STTModelEntity> resultList = sttModelService
          .selectTrainedModelList(param.getWorkspaceId(), TrainResult.success.toString(),
              SystemCode.FILE_GRP_PPOS_STT_LM);

      return new ResponseEntity<>(resultList, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getLmModels e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "stt^R")
  @RequestMapping(
      value = "/getAmModels",
      method = RequestMethod.POST)
  public ResponseEntity<?> getAmModels(@RequestBody STTModelEntity param) {
    logger.info("===== call api  GET [[api/stt/analysis/getAmModels]] :: {}", param);
    try {
      List<STTModelEntity> resultList = sttModelService
          .selectTrainedModelList(param.getWorkspaceId(), TrainResult.success.toString(),
              SystemCode.FILE_GRP_PPOS_STT_AM);

      return new ResponseEntity<>(resultList, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getAmModels e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "stt^X")
  @RequestMapping(
      value = "/analyze",
      method = RequestMethod.POST)
  public ResponseEntity<?> analyze(@RequestBody STTAnalysisResultEntity param) {
    logger.info("===== call api  GET [[api/stt/analysis/analyze]] :: {}", param);
    try {
      sttAnalysisService.analyze(param);
      return new ResponseEntity<>(HttpStatus.OK);
    } catch (Exception e) {
      logger.error("analyze e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "stt^R")
  @RequestMapping(
      value = "/getGroupFileList",
      method = RequestMethod.POST)
  public ResponseEntity<?> getGroupFileList(@RequestBody FileEntity param) {
    logger.info("===== call api  [[api/stt/analysis/getGroupFileList]] :: {}", param);
    try {
      param.setPurpose(SystemCode.FILE_GRP_PPOS_STT_AM);
      Page<FileEntity> pageResult = fileService.getGroupFileList(param);

      return new ResponseEntity<>(pageResult, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getGroupFileList e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "stt^R")
  @RequestMapping(
      value = "/getAnalysisResultList",
      method = RequestMethod.POST)
  public ResponseEntity<?> getAnalysisResultList(@RequestBody STTAnalysisResultEntity param) {
    logger.info("===== call api  [[api/stt/analysis/getAnalysisResultList]] :: {}", param);
    try {
      Page<STTAnalysisResultEntity> resultList = sttAnalysisResultService
          .selectResultList(param);
      return new ResponseEntity<>(resultList, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getAnalysisResultList e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "stt^D")
  @RequestMapping(
      value = "/delete-result",
      method = RequestMethod.POST)
  public ResponseEntity<?> deleteResult(@RequestBody List<STTAnalysisResultEntity> param) {
    logger.info("===== call api  [[api/stt/analysis/deleteResult]] :: {}", param);
    try {
      sttAnalysisResultService.deleteResult(param);
      return new ResponseEntity<>(HttpStatus.OK);
    } catch (Exception e) {
      logger.error("deleteResult e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "stt^R")
  @RequestMapping(
      value = "/get-result",
      method = RequestMethod.POST)
  public ResponseEntity<?> getResult(@RequestBody STTAnalysisResultEntity param) {
    logger.info("===== call api  [[api/stt/analysis/getResult]] :: {}", param);
    try {
      STTAnalysisResultEntity sttAnalysisResultEntity = sttAnalysisResultService
          .selectResult(param);
      return new ResponseEntity<>(sttAnalysisResultEntity, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getResult e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
