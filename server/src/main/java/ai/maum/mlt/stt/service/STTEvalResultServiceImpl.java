package ai.maum.mlt.stt.service;

import ai.maum.mlt.stt.entity.STTEvalResultEntity;
import ai.maum.mlt.stt.repository.STTEvalResultRepository;
import java.util.List;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
public class STTEvalResultServiceImpl implements STTEvalResultService {

  private static final Logger logger = LoggerFactory.getLogger(STTEvalResultServiceImpl.class);

  @Autowired
  private STTEvalResultRepository sttEvalResultRepository;

  /**
   * FileGroup 명 과 이름, AM 모델이름, LM 모델 이름 으로 STTEvalResultEntity를 페이징해서 조회하는 로직
   *
   * @param param STTEvalResultEntity중 FileGroupName, Name, AmModelName, LmModelName 필요, 필수값아님
   * @return Page<STTEvalResultEntity>
   */
  @Override
  public Page<STTEvalResultEntity> selectEvaluationResultList(STTEvalResultEntity param) {
    logger.debug("===== selectEvaluationResultList :: STTEvalResultEntity {}", param);
    try {
      Page<STTEvalResultEntity> sttEvalResultEntities =
          sttEvalResultRepository
              .findAllByParam(param.getFileGroupName(), param.getName(), param.getAmModelName(),
                  param.getLmModelName(), param.getPageRequest());
      return sttEvalResultEntities;
    } catch (Exception e) {
      logger.error("===== selectEvaluationResultList :: {}", e.getMessage());
      throw e;
    }
  }

  /**
   * STTEvalResultEntity insert하는 로직
   *
   * @param param List<STTEvalResultEntity>
   * @return List<STTEvalResultEntity>
   */
  @Override
  @Transactional
  public List<STTEvalResultEntity> insertEvaluationResultList(List<STTEvalResultEntity> param) {
    logger.debug("===== insertEvaluationResultList :: List<STTEvalResultEntity> {}", param);
    try {
      return sttEvalResultRepository.save(param);
    } catch (Exception e) {
      logger.error("===== insertEvaluationResultList :: {}", e.getMessage());
      throw e;
    }
  }

  /**
   * STTEvalResultEntity 삭제하는 로직
   *
   * @param param List<STTEvalResultEntity>
   */
  @Override
  @Transactional
  public void deleteEvaluationResultList(List<STTEvalResultEntity> param) {
    logger.debug("===== deleteEvaluationResultList :: List<STTEvalResultEntity> {}", param);
    try {
      sttEvalResultRepository.delete(param);
    } catch (Exception e) {
      logger.error("===== deleteEvaluationResultList :: {}", e.getMessage());
      throw e;
    }
  }

  /**
   * analysisResultId로 STTEvalResultEntity 삭제 하는 로직
   *
   * @param analysisResultId String
   */
  @Override
  @Transactional
  public void deleteEvaluationResultByAnalysisResultId(String analysisResultId) {
    logger.debug("===== deleteEvaluationResultByAnalysisResultId :: analysisResultId {}",
        analysisResultId);
    try {
      sttEvalResultRepository.deleteAllByAnalysisResultId(analysisResultId);
    } catch (Exception e) {
      logger.error("===== deleteEvaluationResultByAnalysisResultId :: {}", e.getMessage());
      throw e;
    }
  }

  /**
   * analysisResultId List로 STTEvalResultEntity 삭제 하는 로직
   *
   * @param analysisResultIdList List<String>
   */
  @Override
  @Transactional
  public void deleteEvaluationResultByAnalysisResultId(List<String> analysisResultIdList) {
    logger.debug("===== deleteEvaluationResultByAnalysisResultId :: analysisResultIdList {}",
        analysisResultIdList);
    try {
      sttEvalResultRepository.deleteAllByAnalysisResultId(analysisResultIdList);
    } catch (Exception e) {
      logger.error("===== deleteEvaluationResultByAnalysisResultId :: {}", e.getMessage());
      throw e;
    }
  }
}
