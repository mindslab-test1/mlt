package ai.maum.mlt.stt.service;

import ai.maum.mlt.stt.entity.STTAnalysisResultEntity;

public interface STTAnalysisService {

  void analyze(STTAnalysisResultEntity param) throws Exception;

}
