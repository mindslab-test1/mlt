package ai.maum.mlt.stt.service;

import ai.maum.mlt.stt.entity.STTEvalResultEntity;
import java.util.List;
import org.springframework.data.domain.Page;

public interface STTEvalResultService {

  Page<STTEvalResultEntity> selectEvaluationResultList(STTEvalResultEntity param) throws Exception;

  List<STTEvalResultEntity> insertEvaluationResultList(List<STTEvalResultEntity> param)
      throws Exception;

  void deleteEvaluationResultList(List<STTEvalResultEntity> param) throws Exception;

  void deleteEvaluationResultByAnalysisResultId(String analysisResultId) throws Exception;

  void deleteEvaluationResultByAnalysisResultId(List<String> analysisResultIdList) throws Exception;
}
