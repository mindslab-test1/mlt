package ai.maum.mlt.stt.repository;

import ai.maum.mlt.stt.entity.STTAnalysisResultEntity;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface STTAnalysisResultRepository extends
    JpaRepository<STTAnalysisResultEntity, String> {

  @Query(value = "SELECT a "
      + "         FROM STTAnalysisResultEntity a"
      + "         WHERE (a.fileGroupId in (SELECT g.id FROM FileGroupEntity g WHERE g.name LIKE CONCAT('%', :fileGroupName, '%')) OR :fileGroupName IS NULL)"
      + "         AND (a.fileEntity.name LIKE CONCAT('%', :fileName, '%') OR :fileName IS NULL)"
      + "         AND (a.amModelId in (SELECT am.id FROM STTModelEntity am WHERE am.purpose = 'STT_AM'"
      + "               AND am.name LIKE CONCAT('%', :amModelName ,'%')) OR :amModelName IS NULL)"
      + "         AND (a.lmModelId in (SELECT lm.id FROM STTModelEntity lm WHERE lm.purpose = 'STT_LM' "
      + "               AND lm.name LIKE CONCAT('%', :lmModelName ,'%')) OR :lmModelName IS NULL)")
  Page<STTAnalysisResultEntity> findAllByParam(@Param("fileGroupName") String fileGroupName,
      @Param("fileName") String fileName,
      @Param("amModelName") String amModelName, @Param("lmModelName") String lmModelName,
      Pageable pageable);

  @Query(value = "SELECT a "
      + "         FROM STTAnalysisResultEntity a"
      + "         WHERE (a.fileGroupId = :fileGroupId OR :fileGroupId IS NULL)"
      + "         AND (a.fileEntity.name LIKE CONCAT('%', :fileName, '%') OR :fileName IS NULL)"
      + "         AND (a.amModelId = :amModelId OR :amModelId IS NULL)"
      + "         AND (a.lmModelId = :lmModelId OR :lmModelId IS NULL)")
  Page<STTAnalysisResultEntity> findAllByFileGroupIdAndAmModelIdAndLmModelIdAndName(
      @Param("fileGroupId") String fileGroupId, @Param("fileName") String fileName,
      @Param("amModelId") String amModelId, @Param("lmModelId") String lmModelId,
      Pageable pageable);

  List<STTAnalysisResultEntity> findAllByAmModelIdOrLmModelId(String amModelId, String lmModelId);

  List<STTAnalysisResultEntity> findAllByFileGroupId(String fileGroupId);

  List<STTAnalysisResultEntity> findAllByFileId(String fileId);

  void deleteByFileIdAndWorkspaceId(String fileId, String workspaceId);
}
