package ai.maum.mlt.stt.service;

import ai.maum.mlt.stt.entity.STTAnalysisResultEntity;
import ai.maum.mlt.stt.repository.STTAnalysisResultRepository;
import java.util.ArrayList;
import java.util.List;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
public class STTAnalysisResultServiceImpl implements STTAnalysisResultService {

  private static final Logger logger = LoggerFactory.getLogger(STTAnalysisResultServiceImpl.class);

  @Autowired
  private STTAnalysisResultRepository sttAnalysisResultRepository;

  @Autowired
  private STTEvalResultService sttEvalResultService;

  /**
   * ID 값으로 STTAnalysisResultEntity를 조회하는 로직
   *
   * @param param STTAnalysisResultEntity 중 ID 값 필요
   * @return STTAnalysisResultEntity
   */
  @Override
  public STTAnalysisResultEntity selectResult(STTAnalysisResultEntity param) throws Exception {
    logger.debug("===== selectResult :: STTAnalysisResultEntity {}", param);
    try {
      STTAnalysisResultEntity sttAnalysisResultEntity =
          sttAnalysisResultRepository.getOne(param.getId());
      return sttAnalysisResultEntity;
    } catch (Exception e) {
      logger.error("===== selectResult :: {}", e.getMessage());
      throw e;
    }
  }

  /**
   * FileGroup 명 과 이름, AM 모델이름, LM 모델 이름 으로 STTAnalysisResultEntity를 페이징해서 조회하는 로직
   *
   * @param param STTAnalysisResultEntity중 FileGroupName, Name, AmModelName, LmModelName 필요, 필수값아님
   * @return Page<STTAnalysisResultEntity>
   */
  @Override
  public Page<STTAnalysisResultEntity> selectResultList(STTAnalysisResultEntity param)
      throws Exception {
    logger.debug("===== selectResultList :: STTAnalysisResultEntity {}", param);
    try {
      Page<STTAnalysisResultEntity> pages = sttAnalysisResultRepository
          .findAllByParam(param.getFileGroupName(), param.getName(), param.getAmModelName(),
              param.getLmModelName(), param.getPageRequest());
      return pages;
    } catch (Exception e) {
      logger.error("===== selectResultList :: {}", e.getMessage());
      throw e;
    }
  }

  /**
   * FileGroupId, AmModelId, LmModelId, Name으로 STTAnalysisResultEntity를 페이징해서 조회하는 로직
   *
   * @param param STTAnalysisResultEntity중 FileGroupId, Name, AmModelId, LmModelId 필요, 필수값아님
   * @return Page<STTAnalysisResultEntity>
   */
  @Override
  public Page<STTAnalysisResultEntity> selectResultByFileGroupIdAndAmModelIdAndLmModelIdAndName(
      STTAnalysisResultEntity param) throws Exception {
    logger.debug(
        "===== selectResultByFileGroupIdAndAmModelIdAndLmModelIdAndName :: STTAnalysisResultEntity {}",
        param);
    try {
      Page<STTAnalysisResultEntity> pages = sttAnalysisResultRepository
          .findAllByFileGroupIdAndAmModelIdAndLmModelIdAndName(param.getFileGroupId(),
              param.getName(), param.getAmModelId(), param.getLmModelId(), param.getPageRequest());
      return pages;
    } catch (Exception e) {
      logger.error("===== selectResultByFileGroupIdAndAmModelIdAndLmModelIdAndName :: {}",
          e.getMessage());
      throw e;
    }
  }

  /**
   * ModelID(AM,LM)으로 STTAnalysisResultEntity 조회 하는 로직
   *
   * @return List<STTAnalysisResultEntity>
   */
  @Override
  public List<STTAnalysisResultEntity> selectResultByModelId(String modelId) throws Exception {
    logger.debug("===== selectResultByModelId :: modelId {}", modelId);
    try {
      return sttAnalysisResultRepository.findAllByAmModelIdOrLmModelId(modelId, modelId);
    } catch (Error e) {
      logger.error("===== selectResultByAmModelId :: {}", e.getMessage());
      throw e;
    }
  }

  /**
   * STTAnalysisResultEntity insert하는 로직
   *
   * @param param STTAnalysisResultEntity
   */
  @Override
  @Transactional
  public void insertResult(STTAnalysisResultEntity param) throws Exception {
    logger.debug("===== insertResult :: STTAnalysisResultEntity {}", param);
    try {
      sttAnalysisResultRepository.save(param);
    } catch (Exception e) {
      logger.error("===== insertResult :: {}", e.getMessage());
      throw e;
    }
  }

  /**
   * STTAnalysisResultEntity Id 값으로 삭제하는 로직 STTEvalResultEntity가 하위 Entity이기 때문에 먼저 삭제후
   * STTAnalysisResultEntity 삭제처리함
   *
   * @param param List<STTAnalysisResultEntity>
   */
  @Override
  @Transactional
  public void deleteResult(List<STTAnalysisResultEntity> param) throws Exception {
    logger.debug("===== deleteResult :: List<STTAnalysisResultEntity> {}", param);
    try {
      // STTEvalResultEntity 가 child 이기때문에 먼저 삭제를 해야함.
      List<String> analysisResultIdList = new ArrayList<>();
      for (STTAnalysisResultEntity sttAnalysisResultEntity : param) {
        analysisResultIdList.add(sttAnalysisResultEntity.getId());
      }
      sttEvalResultService.deleteEvaluationResultByAnalysisResultId(analysisResultIdList);
      sttAnalysisResultRepository.delete(param);
    } catch (Exception e) {
      logger.error("===== deleteResult :: {}", e.getMessage());
      throw e;
    }
  }

  /**
   * fileGroupId로 STTAnalysisResultEntity 삭제하는 로직 STTEvalResultEntity가 하위 Entity이기 때문에 먼저 삭제후
   * STTAnalysisResultEntity 삭제처리함
   */
  @Override
  @Transactional
  public void deleteResultByFileGroupId(String fileGroupId) throws Exception {
    logger.debug("===== deleteResultByFileGroupId :: fileGroupId {}", fileGroupId);
    try {
      List<STTAnalysisResultEntity> list = sttAnalysisResultRepository
          .findAllByFileGroupId(fileGroupId);
      // STTEvalResultEntity 가 child 이기때문에 먼저 삭제를 해야함.
      List<String> analysisResultIdList = new ArrayList<>();
      for (STTAnalysisResultEntity sttAnalysisResultEntity : list) {
        analysisResultIdList.add(sttAnalysisResultEntity.getId());
      }
      sttEvalResultService.deleteEvaluationResultByAnalysisResultId(analysisResultIdList);
      sttAnalysisResultRepository.delete(list);
    } catch (Exception e) {
      logger.error("===== deleteResultByFileGroupId :: {}", e.getMessage());
      throw e;
    }
  }

  /**
   * fileId로 STTAnalysisResultEntity 삭제하는 로직 STTEvalResultEntity가 하위 Entity이기 때문에 먼저 삭제후
   * STTAnalysisResultEntity 삭제처리함
   */
  @Override
  @Transactional
  public void deleteResultByFileId(String fileId) throws Exception {
    logger.debug("===== deleteResultByFileId :: fileId {}", fileId);
    try {
      List<STTAnalysisResultEntity> list = sttAnalysisResultRepository.findAllByFileId(fileId);
      // STTEvalResultEntity 가 child 이기때문에 먼저 삭제를 해야함.
      List<String> analysisResultIds = new ArrayList<>();
      for (STTAnalysisResultEntity sttAnalysisResultEntity : list) {
        analysisResultIds.add(sttAnalysisResultEntity.getId());
      }
      sttEvalResultService.deleteEvaluationResultByAnalysisResultId(analysisResultIds);
      sttAnalysisResultRepository.delete(list);
    } catch (Exception e) {
      logger.error("===== deleteResultByFileId :: {}", e.getMessage());
      throw e;
    }
  }
}
