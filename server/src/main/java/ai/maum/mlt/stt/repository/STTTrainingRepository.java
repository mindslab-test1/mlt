package ai.maum.mlt.stt.repository;

import ai.maum.mlt.stt.entity.STTTrainingEntity;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
@Transactional
public interface STTTrainingRepository extends JpaRepository<STTTrainingEntity, String> {

  STTTrainingEntity findBySttKey(String key);

  void deleteBySttKey(String key);
}
