package ai.maum.mlt.stt.service;

import ai.maum.mlt.common.auth.entity.WorkspaceEntity;
import ai.maum.mlt.common.auth.service.WorkspaceService;
import ai.maum.mlt.common.system.SystemCode;
import ai.maum.mlt.common.util.FileUtils;
import ai.maum.mlt.itfc.SttGrpcInterfaceManager;
import ai.maum.mlt.stt.entity.STTAnalysisResultEntity;
import ai.maum.mlt.stt.entity.STTModelEntity;
import ai.maum.mlt.stt.repository.STTModelRepository;
import io.grpc.Metadata;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.transaction.Transactional;
import maum.brain.stt.Stt.SetModelResponse;
import maum.common.LangOuterClass.LangCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class STTAnalysisServiceImpl implements STTAnalysisService {

  private static final Logger logger = LoggerFactory.getLogger(STTAnalysisServiceImpl.class);

  @Value("${stt.file.path}")
  private String sttPath;

  @Value("${stt.8k.resource.path}")
  private String stt8kResourcePath;

  @Value("${stt.16k.resource.path}")
  private String stt16kResourcePath;

  @Value("${stt.8k.baseline.resource.path}")
  private String stt8kBaselineResourcePath;

  @Value("${temp.file.path}")
  private String tempPath;

  @Value("${stt.train.server.ip}")
  private String trainServerIp;

  @Value("${stt.train.server.port}")
  private int trainServerPort;

  @Value("${stt.analysis.server.ip}")
  private String analysisServerIp;

  @Value("${stt.analysis.server.port}")
  private int analysisServerPort;

  @Autowired
  private WorkspaceService workspaceService;

  @Autowired
  private STTTranscriptService sttTranscriptService;

  @Autowired
  private STTAnalysisResultService sttAnalysisResultService;

  @Autowired
  private STTModelRepository sttModelRepository;

  @Autowired
  private FileUtils fileUtils;

  /**
   * STT Analyze 하는 로직 요청 받은 AM,LM Model을 압축풀어서 안에 내용(각각 2개의 파일)과 공통파일 sam.bin,lda.bin파일들 총 6개 파일로
   * 하나의 STT 모델을 새로 생성합니다. 이때 sam.bin, lda.bin은 brain-stt-train 리소스($MAUM_ROOT/resources/stt-training-kor-16k)에서
   * 가져오며 리소스 위치는 mlt.conf에서 stt.resource.path를 설정할수 있습니다. 그 후 brain-stt에 setModel을 호출해서 엔진이 위 모델을
   * 다운받고 실행되도록 합니다. 그후 find로 해당 모델로 뜬 STT 서버 ip,port를 받아오고 sttRealServicePing를 호출해서 제대로 실행될때 까지
   * 기다린뒤 요청한 모델이 뜬 STT서버에 simpleRecognize를 통해 Analyze할 파일들 STT를 요청해서 요청한 결과를
   * STTAnalysisResultEntity에 저장합니다.
   *
   * @param param STTAnalysisResultEntity
   */
  @Override
  @Transactional
  public void analyze(STTAnalysisResultEntity param) throws Exception {
    logger.debug("===== analyze :: start {}", param);
    try {
      SttGrpcInterfaceManager sttAnalysisClient = new SttGrpcInterfaceManager(analysisServerIp,
          analysisServerPort); // brain-stt

      WorkspaceEntity workspaceEntity = workspaceService.getWorkspace(param.getWorkspaceId());
      STTModelEntity sttLmModelEntity = null;
      STTModelEntity sttAmModelEntity = null;

      try {
        sttLmModelEntity = sttModelRepository.findOne(param.getLmModelId());
      } catch (Exception e) {
        logger.error("===== analyze :: LM Model not exist, id {}", param.getLmModelId());
        throw e;
      }
      try {
        sttAmModelEntity = sttModelRepository.findOne(param.getAmModelId());
      } catch (Exception e) {
        logger.error("===== analyze :: AM Model not exist, id {}", param.getAmModelId());
        throw e;
      }

      maum.brain.stt.Stt.Model.Builder sttModel = maum.brain.stt.Stt.Model.newBuilder();

      if (!SystemCode.STT_BASE_LINE_DNN.equals(sttLmModelEntity.getName())
          && !SystemCode.STT_BASE_LINE_DNN.equals(sttAmModelEntity.getName())
          && !SystemCode.STT_BASE_LINE_LSTM.equals(sttLmModelEntity.getName())
          && !SystemCode.STT_BASE_LINE_LSTM.equals(sttAmModelEntity.getName())) {
        // 모델 파일 압축 해제
        Format formatter = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        String modelName = formatter.format(new Date());
        String fileName = modelName + "-" + LangCode.kor + "-" + String
            .valueOf(sttLmModelEntity.getRate()); // modelname-lang-rate
        if (sttLmModelEntity.getName().equals("baseline")) {
          // lm baseline 8k

          // 디렉토리 없으면 먼저 디렉토리 생성
          File d = new File(fileUtils.getSystemConfPath(tempPath + "/stt-model/" + fileName));
          if (!d.isDirectory()) {
            d.mkdirs();
          }

          fileUtils.copyFile(
              fileUtils.getSystemConfPath(stt8kBaselineResourcePath + "/sfsm.bin"),
              fileUtils.getSystemConfPath(tempPath + "/stt-model/" + fileName + "/sfsm.bin"));
          fileUtils.copyFile(
              fileUtils.getSystemConfPath(stt8kBaselineResourcePath + "/sym.bin"),
              fileUtils.getSystemConfPath(tempPath + "/stt-model/" + fileName + "/sym.bin"));
        } else {
          fileUtils.uncompressTarGZ(new File(
                  fileUtils.getSystemConfPath(
                      sttPath + "/" + param.getWorkspaceId() + "/stt-model/lm/" + sttLmModelEntity
                          .getSttBinary())),
              new File(fileUtils.getSystemConfPath(tempPath + "/stt-model/" + fileName)));
        }
        if (sttAmModelEntity.getName().equals("baseline")) {
          // am baseline 8k

          // 디렉토리 없으면 먼저 디렉토리 생성
          File d = new File(fileUtils.getSystemConfPath(tempPath + "/stt-model/" + fileName));
          if (!d.isDirectory()) {
            d.mkdirs();
          }

          fileUtils.copyFile(
              fileUtils.getSystemConfPath(stt8kBaselineResourcePath + "/dnn.bin"),
              fileUtils.getSystemConfPath(tempPath + "/stt-model/" + fileName + "/dnn.bin"));
          fileUtils.copyFile(
              fileUtils.getSystemConfPath(stt8kBaselineResourcePath + "/dnn.prior.bin"),
              fileUtils.getSystemConfPath(tempPath + "/stt-model/" + fileName + "/dnn.prior.bin"));
        } else {
          fileUtils.uncompressTarGZ(new File(
                  fileUtils.getSystemConfPath(
                      sttPath + "/" + param.getWorkspaceId() + "/stt-model/am/" + sttAmModelEntity
                          .getSttBinary())),
              new File(fileUtils.getSystemConfPath(tempPath + "/stt-model/" + fileName)));
        }
        logger.debug("===== analyze :: uncompress lm,am binary finish");

        try {
          fileUtils
              .moveFileToRootDir(fileUtils.getSystemConfPath(tempPath + "/stt-model/" + fileName));
        } catch (Exception e) {
          logger.error("===== analyze :: move file to root error {}", e.getMessage());
          throw e;
        }

        // sam, lda.bin 이 있어야 정상작동한다고 해서 복사
        try {
          if (sttLmModelEntity.getRate() == 8000) {
            fileUtils.copyFile(
                fileUtils
                    .getSystemConfPath(stt8kResourcePath + "/dnn_image_out/stt_release.sam.bin"),
                fileUtils.getSystemConfPath(tempPath + "/stt-model/" + fileName + "/sam.bin"));
            fileUtils.copyFile(
                fileUtils.getSystemConfPath(stt8kResourcePath + "/dnn_image_out/final.dnn.lda.bin"),
                fileUtils.getSystemConfPath(tempPath + "/stt-model/" + fileName + "/lda.bin"));
            fileUtils.copyFile(
                fileUtils.getSystemConfPath(stt8kBaselineResourcePath + "/model.cfg"),
                fileUtils.getSystemConfPath(tempPath + "/stt-model/" + fileName + "/model.cfg"));
          } else { // 16k
            fileUtils.copyFile(
                fileUtils
                    .getSystemConfPath(
                        stt16kResourcePath + "/dnn_image_out/stt_baseline_16k.sam.bin"),
                fileUtils.getSystemConfPath(tempPath + "/stt-model/" + fileName + "/sam.bin"));
            fileUtils.copyFile(
                fileUtils
                    .getSystemConfPath(stt16kResourcePath + "/dnn_image_out/final.dnn.lda.bin"),
                fileUtils.getSystemConfPath(tempPath + "/stt-model/" + fileName + "/lda.bin"));
            // TODO 필요한경우 8k 모델과 같이 model.cfg 파일 copy 해줘야한다.
          }

          //      STT 특정버전에 따라 필요할수있음 (ex 법원..)
                Runtime runtime = Runtime.getRuntime();
                Process process = runtime.exec("convDNN2DLNet-8.0 " + fileUtils
                    .getSystemConfPath(tempPath + "/stt-model/" + fileName + "/dnn"));
                InputStream is = process.getInputStream();
                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader br = new BufferedReader(isr);
                String line;
                while ((line = br.readLine()) != null) {
                  logger.debug(line);
                }
        } catch (Exception e) {
          logger.error("===== analyze :: Copy sam,lda bin Error {}", e.getMessage());
          throw e;
        }

        // lm, am 모델 파일 합쳐서 한개의 파일로 압축
        fileUtils.compressTarGZ(fileUtils.getSystemConfPath(tempPath + "/stt-model/" + fileName),
            fileUtils.getSystemConfPath(tempPath + "/stt-model/" + fileName + ".sttimage.tar.gz"));

        logger.debug("====== analyze :: compressTargz lm,am binary finish");

        // stt에 setModel을 호출해서 STT엔진에서 해당모델로 실행하도록 해야함
        // ta 같은 다른 엔진의 경우 training callback (update)로직에서 setModel을 호출해서 해당 엔진에 model을 다운로드해놓지만
        // stt 같은경우 LM,AM 엔진을 각각 조합할수있기때문에 Analyze에서 LM,AM 모델 각각 선택후 각각 모델을 하나로 합친후 SetModel 호출함

        File clientBinary = new File(
            fileUtils.getSystemConfPath(
                tempPath + "/stt-model/" + fileName + ".sttimage.tar.gz"));
        Metadata metadata = new Metadata();
        metadata.put(Metadata.Key.of("in.lang", Metadata.ASCII_STRING_MARSHALLER), String.valueOf(LangCode.kor));
        metadata.put(Metadata.Key.of("in.filename", Metadata.ASCII_STRING_MARSHALLER),
            clientBinary.getName());
        metadata
            .put(Metadata.Key.of("in.model", Metadata.ASCII_STRING_MARSHALLER), modelName);
        metadata.put(Metadata.Key.of("in.samplerate", Metadata.ASCII_STRING_MARSHALLER),
            String.valueOf(sttLmModelEntity.getRate()));

        List<SetModelResponse> setModelResponses = sttAnalysisClient
            .setModel(clientBinary, metadata);

        logger.debug("===== analyze :: setModel called, setModelResponse {}", setModelResponses);

        // 생성한 임시 파일들 삭제 (다운받은 am,lm 모델, 압축푼 am, lm모델 , 압축한 합쳐진 모델)
        try {
          File mergedModelFile = new File(
              fileUtils
                  .getSystemConfPath(tempPath + "/stt-model/" + fileName + ".sttimage.tar.gz"));
          mergedModelFile.delete();
          File mergedModelFolder = new File(
              fileUtils.getSystemConfPath(tempPath + "/stt-model/" + fileName));
          File[] mergedModelFolderFiles = mergedModelFolder.listFiles();
          for (int i = 0; i < mergedModelFolderFiles.length; i++) {
            mergedModelFolderFiles[i].delete();
          }
          mergedModelFolder.delete();
          logger.debug("===== analyze :: temp file delete finish");
        } catch (Exception e) {
          logger.error("===== analyze :: temp file delete error {}", e.getMessage());
          throw e;
        }
        sttModel.setModel(setModelResponses.get(0).getModel());
        sttModel.setSampleRate(setModelResponses.get(0).getSampleRate());
        sttModel.setLang(setModelResponses.get(0).getLang());
      } else {
        int rate = 8000;
        String modelName = SystemCode.STT_BASE_LINE_BINARY_DNN;

        if (sttLmModelEntity.getRate() == 16000 && sttAmModelEntity.getRate() == 16000) {
          rate = 16000;
        }

        if (SystemCode.STT_TRAINING_TYPE_LSTM.equals(sttLmModelEntity.getTrainType())
            && SystemCode.STT_TRAINING_TYPE_LSTM.equals(sttAmModelEntity.getTrainType())) {
          modelName = SystemCode.STT_BASE_LINE_BINARY_LSTM;
        }

        sttModel.setModel(modelName);
        sttModel.setSampleRate(rate);
        sttModel.setLang(LangCode.valueOf("kor"));

        logger.debug("set baseline model :: {} :: {}", modelName, rate);
      }

      maum.brain.stt.Stt.ServerStatus findResult = sttAnalysisClient.find(sttModel.build());

      logger.debug("===== analyze :: find called, findResult {}", findResult);

      String serverAddress = findResult.getServerAddress();
      String[] host = serverAddress.split(":");

      SttGrpcInterfaceManager newModelSttClient = new SttGrpcInterfaceManager(host[0],
          Integer.parseInt(host[1]));

      maum.brain.stt.Stt.ServerStatus pingResult = null;
      Integer pingCount = 0;
      while (true) {
        try {
          pingResult = newModelSttClient.sttRealServicePing(sttModel.build());
          logger.debug("===== analyze :: sttRealServicePing called, pingResult {}", pingResult);
          if (pingResult != null && pingResult.getRunning()) {
            break;
          }
          if (pingCount > 60) {
            break;
          }
          Thread.sleep(1000);
          pingCount++;
        } catch (InterruptedException e) {
          logger
              .error("===== analyze :: sttRealServicePing InterruptedException {}", e.getMessage());
          throw e;
        } catch (Exception e) {
          logger.error("===== analyze :: sttRealServicePing Error {}", e.getMessage());
          throw e;
        }
      }

      int fileNameIndex = 0;

      if (pingResult.getRunning()) {
        logger.debug("===== analyze :: pingResult getRunning true, pingCount {} , pingResult {}",
            pingCount,
            pingResult);
        for (String id : param.getIds()) {
          try {
            File pcmFile = fileUtils
                .getPcmFile(id, sttPath + "/" + param.getWorkspaceId().toString());

            String text = newModelSttClient.simpleRecognize(pcmFile).getTxt();
            logger.debug("===== analyze :: simpleRecognize result, fileId {}, text {}", id, text);
            STTAnalysisResultEntity sttAnalysisResultEntity = new STTAnalysisResultEntity();
            sttAnalysisResultEntity.setWorkspaceId(param.getWorkspaceId());
            sttAnalysisResultEntity.setAmModelId(param.getAmModelId());
            sttAnalysisResultEntity.setLmModelId(param.getLmModelId());
            sttAnalysisResultEntity.setFileGroupId(param.getFileGroupId());
            sttAnalysisResultEntity.setCreatedAt(new Date());
            sttAnalysisResultEntity.setUpdatedAt(new Date());
            sttAnalysisResultEntity.setFileId(id);
            sttAnalysisResultEntity
                .setTranscriptId(sttTranscriptService
                    .selectTranscriptByFileIdAndFileGroupIdAndWorkspaceId(id,
                        param.getFileGroupId(),
                        param.getWorkspaceId()).getId());
            sttAnalysisResultEntity.setName(param.getFileNames().get(fileNameIndex));
            sttAnalysisResultEntity
                .setText(text);
            sttAnalysisResultService.insertResult(sttAnalysisResultEntity);
            fileNameIndex++;
          } catch (Exception e) {
            logger.error("===== analyze :: insert analysisResult error {}", e.getMessage());
            throw e;
          }
        }
      }
    } catch (Exception e) {
      logger.error("===== analyze :: {}", e.getMessage());
      throw e;
    }
  }

}
