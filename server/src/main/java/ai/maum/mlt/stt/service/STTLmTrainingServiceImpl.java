package ai.maum.mlt.stt.service;

import ai.maum.mlt.common.auth.entity.WorkspaceEntity;
import ai.maum.mlt.common.auth.service.WorkspaceService;
import ai.maum.mlt.common.file.entity.FileGroupEntity;
import ai.maum.mlt.common.file.repository.FileGroupRepository;
import ai.maum.mlt.common.system.SystemCode;
import ai.maum.mlt.common.util.FileUtils;
import ai.maum.mlt.itfc.SttGrpcInterfaceManager;
import ai.maum.mlt.management.entity.HistoryEntity;
import ai.maum.mlt.management.service.HistoryService;
import ai.maum.mlt.stt.entity.STTModelEntity;
import ai.maum.mlt.stt.entity.STTTrainingEntity;
import ai.maum.mlt.stt.entity.STTTranscriptEntity;
import ai.maum.mlt.stt.repository.STTTrainingRepository;
import ai.maum.mlt.stt.repository.STTTranscriptRepository;
import com.google.protobuf.Empty;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.transaction.Transactional;
import maum.brain.stt.train.S3Train.DeepLearningType;
import maum.brain.stt.train.S3Train.TrainBinary;
import maum.brain.stt.train.S3Train.TrainKey;
import maum.brain.stt.train.S3Train.TrainModelType;
import maum.brain.stt.train.S3Train.TrainResult;
import maum.brain.stt.train.S3Train.TrainStatus;
import maum.brain.stt.train.S3Train.TrainStatusList;
import maum.common.LangOuterClass.LangCode;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class STTLmTrainingServiceImpl implements STTLmTrainingService {

  private static final Logger logger = LoggerFactory.getLogger(STTLmTrainingServiceImpl.class);

  @Value("${stt.file.path}")
  private String sttPath;

  @Value("${stt.train.server.ip}")
  private String trainServerIp;

  @Value("${stt.train.server.port}")
  private int trainServerPort;

  @Value("${server.protocol}")
  private String serverProtocol;

  @Value("${server.host}")
  private String serverHost;

  @Value("${server.port}")
  private int serverPort;

  @Value("${mlt.api.key}")
  private String apiKey;

  @Autowired
  private STTTranscriptRepository sttTranscriptRepository;

  @Autowired
  private STTTrainingRepository sttTrainingRepository;

  @Autowired
  private STTModelService sttModelService;

  @Autowired
  private HistoryService historyService;

  @Autowired
  private WorkspaceService workspaceService;

  @Autowired
  private FileGroupRepository fileGroupRepository;

  @Autowired
  private FileUtils fileUtils;

  /**
   * STT LM 학습 실행 요청하는 로직 brain-stt-train에 grpc로 openstream 요청 HistoryEntity에는 STT_AM_TRAINING 이
   * Started 되었다고 저장 STTTrainingEntity에는 openStream 결과로 받은 trainKey와 workspaceId 저장 STTModelEntity에는
   * 모델 이름과 rate로 조회해서 저장된 Entity가 있는경우에는 재학습인 경우이므로 sttKey와 status만 변경해주고 저장된 Entity가 없을경우 새로
   * 생성해준다.
   *
   * @param trainType 학습 방법이 LSTM 일경우 "L" 값, DNN 일경우 "D"
   */
  @Override
  @Transactional
  public void open(String fileGroupId, String workspaceId, String trainType)
      throws Exception {
    logger.debug("===== open :: fileGroupId {}, workspaceId {}, trainType {}", fileGroupId,
        workspaceId, trainType);
    try {
      FileGroupEntity fg = fileGroupRepository.findOne(fileGroupId);
      String modelName = fg.getName();
      JSONParser parser = new JSONParser();
      JSONObject fileGroupMeta = (JSONObject) parser.parse(fg.getMeta());
      int rate = Integer.parseInt(fileGroupMeta.get("rate").toString());
      List<STTTranscriptEntity> sttTranscriptEntities = sttTranscriptRepository
          .findAllByFileGroupId(fileGroupId);

      HistoryEntity historyEntity = new HistoryEntity();
      historyEntity.setWorkspaceId(workspaceId);
      historyEntity.setStartedAt(new Date());
      historyEntity.setCode("STT_LM_TRAINING");
      historyEntity.setMessage("Started");
      historyEntity.setData(modelName);
      historyEntity = historyService.insertHistory(historyEntity);

      WorkspaceEntity workspaceEntity = workspaceService
          .getWorkspace(workspaceId);

      /* sttFileGroup 으로 정보조회 */
      String callBackUrl = this.serverProtocol + "://" + this.serverHost + ":" + this.serverPort
          + "/api/stt/lm/training/"
          + historyEntity.getId() + "/" + historyEntity.getWorkspaceId() + "/update/";

      SttGrpcInterfaceManager client = new SttGrpcInterfaceManager(trainServerIp, trainServerPort);
      DeepLearningType deepLearningType = null;
      if (trainType.equals("L")) {
        deepLearningType = DeepLearningType.DEEP_LEARNING_LSTM;
      } else {
        deepLearningType = DeepLearningType.DEEP_LEARNING_DNN;
      }
      TrainKey trainKey = client
          .openStream(modelName, TrainModelType.TRAIN_MODEL_LM, deepLearningType,
              LangCode.valueOf(0), rate, sttTranscriptEntities,
              callBackUrl, "");
      logger.debug("===== open :: trainKey {}", trainKey);
      // Training Entity 에 저장
      STTTrainingEntity sttTrainingEnity = new STTTrainingEntity();
      sttTrainingEnity.setSttKey(trainKey.getTrainId());
      sttTrainingEnity.setWorkspaceId(workspaceId);
      sttTrainingRepository.save(sttTrainingEnity);

      STTModelEntity sttModelEntity = sttModelService
          .selectByNameAndPurposeAndRateAndWorkspaceId(modelName, SystemCode.FILE_GRP_PPOS_STT_AM,
              rate, workspaceId);
      if (sttModelEntity != null) {
        // 기존에 학습된 모델이 있는 경우
        // STT Key 는 학습이 완료된후 저장
        sttModelEntity.setSttKey(trainKey.getTrainId());
        sttModelEntity.setStatus(TrainResult.training.toString());
        sttModelEntity.setUpdatedAt(new Date());
        if (trainType.equals("L")) {
          sttModelEntity.setTrainType(SystemCode.STT_TRAINING_TYPE_LSTM);
        } else {
          sttModelEntity.setTrainType(SystemCode.STT_TRAINING_TYPE_DNN);
        }
      } else {
        // 기존에 학습된 모델이 없는 경우
        sttModelEntity = new STTModelEntity();
        // STT Key 는 학습이 완료된후 저장
        sttModelEntity.setSttKey(trainKey.getTrainId());
        sttModelEntity.setStatus(TrainResult.training.toString());
        sttModelEntity.setCreatedAt(new Date());
        sttModelEntity.setUpdatedAt(new Date());
        sttModelEntity.setWorkspaceId(workspaceId);
        sttModelEntity.setName(modelName);
        sttModelEntity.setPurpose(SystemCode.FILE_GRP_PPOS_STT_LM);
        if (trainType.equals("L")) {
          sttModelEntity.setTrainType(SystemCode.STT_TRAINING_TYPE_LSTM);
        } else {
          sttModelEntity.setTrainType(SystemCode.STT_TRAINING_TYPE_DNN);
        }
        sttModelEntity.setRate(rate);

      }
      sttModelService.insertTrainingModel(sttModelEntity);
    } catch (Exception e) {
      logger.error("===== open :: {}", e.getMessage());
      throw e;
    }
  }

  /**
   * 현재 STT LM 학습 실행 상태 가져오는 로직 brain-stt-train에 grpc에 getAllProgress 요청 grpc 요청시 값에는 LM,AM 모두
   * 포함되어있음으로 그중 TrainModelType이 LM 인것들만 필터링해줌 또한 result가 `cancelled` 인경우 brain-stt-train에 close 호출후
   * sttTrainingEntity도 삭제함
   *
   * @return List<STTTrainingEntity> 학습중인 STTTrainingEntity List
   */
  @Override
  @Transactional
  public List<STTTrainingEntity> getAllProgress(String workspaceId) throws Exception {
    logger.debug("===== getAllProgress :: workspaceId {}", workspaceId);
    List<STTTrainingEntity> sttTrainingEnties = new ArrayList<>();
    try {
      SttGrpcInterfaceManager client = new SttGrpcInterfaceManager(trainServerIp, trainServerPort);

      Empty.Builder req = Empty.newBuilder();
      TrainStatusList trainStatusList = client.getAllProgress(req.build());
      logger.debug("===== getAllProgress :: trainStatusList {}", trainStatusList);
      /*
      * train_id db save
      * */
      for (TrainStatus trainStatus : trainStatusList.getTrainsList()) {
        if (trainStatus.getTrainModelType().equals(TrainModelType.TRAIN_MODEL_LM)) {
          String temp = trainStatus.getKey();
          STTTrainingEntity sttTrainingEntity = sttTrainingRepository.findBySttKey(temp);
          if (sttTrainingEntity != null) {
            STTModelEntity sttModelEntity = sttModelService
                .selectTrainingModel(trainStatus.getKey());
            sttTrainingEntity.setProgress(trainStatus.getValue() * 100 / trainStatus.getMaximum());
            sttTrainingEntity.setName(sttModelEntity.getName());
            if (trainStatus.getResult().equals(TrainResult.cancelled)) {
              TrainKey.Builder tk = TrainKey.newBuilder();
              tk.setTrainId(trainStatus.getKey());
              TrainStatus trainCloaseStatus = client.close(tk.build());
              logger.debug("===== getAllProgress :: trainCloseStatus {}", trainCloaseStatus);
              sttTrainingRepository.deleteBySttKey(sttTrainingEntity.getSttKey());
            } else if (sttTrainingEntity.getWorkspaceId().equals(workspaceId)) {
              sttTrainingEnties.add(sttTrainingEntity);
            }
          }
        }
      }
      return sttTrainingEnties;
    } catch (Exception e) {
      logger.error("===== getAllProgress :: {}", e.getMessage());
      throw e;
    }
  }

  /**
   * brain-stt-train에서 open시 전달한 callback_url을 통해 학습완료시 호출되는 로직 HistoryEntity, STTModelEntity 업데이트 후
   * brain-stt-train에 getBinary 를 통해 학습 모델 파일을 받아와서 sttPath($MAUM_ROOT/run/storage/mlt/stt) +
   * /{workspaceId}/stt-model/lm 에 저장함 그후 close 호출을 함.
   *
   * @param data (message, data)
   */
  @Override
  @Transactional
  public void update(String key, String id, String workspaceId, Object data) throws Exception {
    logger.debug("===== update :: key {}, id {}, workspaceId {}, data {}", key, id, workspaceId,
        data);
    try {
      HistoryEntity historyEntity = new HistoryEntity();
      HashMap<String, Object> dataMap = (HashMap) data;

      if (apiKey.equals(key)) {
        HashMap<String, String> dataModelMap = (HashMap) dataMap.get("data");
        historyEntity.setData(dataModelMap.toString());
        historyEntity.setId(id);
        historyEntity.setMessage((String) dataMap.get("message"));
        historyEntity.setEndedAt(new Date());
        historyEntity.setUpdatedAt(new Date());
        historyService.updateHistory(historyEntity);

        if (!dataModelMap.get("result").equals("failed")) {
          SttGrpcInterfaceManager client = new SttGrpcInterfaceManager(trainServerIp,
              trainServerPort);
          TrainKey.Builder req = TrainKey.newBuilder();
          req.setTrainId(dataModelMap.get("key"));
          TrainStatus trainStatus = client.getProgress(req.build());
          logger.debug("===== update :: TrainStatus {}", trainStatus);

          STTModelEntity sttModelEntity = sttModelService
              .selectTrainingModel(dataModelMap.get("key"));
          sttModelEntity.setUpdatedAt(new Date());
          sttModelEntity.setStatus(trainStatus.getResult().toString());
          sttModelEntity.setSttBinary(dataModelMap.get("binary"));
          sttModelEntity.setName(dataModelMap.get("name"));
          sttModelEntity.setSttKey(dataModelMap.get("key"));
          sttModelEntity.setWorkspaceId(workspaceId);
          sttModelService.updateTrainingModel(sttModelEntity);

          if (trainStatus.getResult().equals(TrainResult.success)) {
            logger.debug("===== update :: LM getBinary start, key {}", trainStatus.getKey());
            TrainKey.Builder lmReq = TrainKey.newBuilder();
            lmReq.setTrainId(trainStatus.getKey());
            List<TrainBinary> lmModelBinary = client.getBinary(lmReq.build());

            fileUtils
                .mkdirs(fileUtils.getSystemConfPath(sttPath + "/" + workspaceId + "/stt-model/lm"));

            FileOutputStream lmModelStream = new FileOutputStream(
                fileUtils.getSystemConfPath(
                    sttPath + "/" + workspaceId + "/stt-model/lm/" + sttModelEntity
                        .getSttBinary()));
            try {
              for (TrainBinary tar : lmModelBinary) {
                lmModelStream.write(tar.getTar().toByteArray());
              }
            } finally {
              lmModelStream.close();
              client.close(lmReq.build());
              logger.debug("===== update :: LM getBinary finish");
            }
          }
        }
      }
    } catch (Exception e) {
      logger.error("===== update :: {}", e.getMessage());
      throw e;
    }
  }

  /**
   * STT LM 학습 취소하는 로직 brain-stt-train에 close 호출 후 STTTrainingEntity, STTModelEntity 삭제 처리
   */
  @Override
  @Transactional
  public void stop(String trainId) throws Exception {
    logger.debug("===== stop :: trainid {}", trainId);
    try {
      SttGrpcInterfaceManager client = new SttGrpcInterfaceManager(trainServerIp, trainServerPort);
      TrainKey.Builder req = TrainKey.newBuilder();
      req.setTrainId(trainId);
      TrainStatus trainStopStatus = client.stop(req.build());
      logger.debug("===== stop :: trainStopStatus {}", trainStopStatus);
      client.close(req.build());

      sttTrainingRepository.deleteBySttKey(trainId);
      STTModelEntity modelEntity = sttModelService.selectTrainingModel(trainId);
      sttModelService.deleteTrainingModel(modelEntity);
    } catch (Exception e) {
      logger.error("===== stop :: {}", e.getMessage());
      throw e;
    }
  }

  /**
   * brain-stt-train에 close 요청 하는 로직
   */
  @Override
  @Transactional
  public void close(String trainId) throws Exception {
    logger.debug("===== close :: trainId {}", trainId);
    try {
      SttGrpcInterfaceManager client = new SttGrpcInterfaceManager(trainServerIp, trainServerPort);
      TrainKey.Builder req = TrainKey.newBuilder();
      req.setTrainId(trainId);
      sttTrainingRepository.deleteBySttKey(trainId);
      TrainStatus trainCloseStatus = client.close(req.build());
      logger.debug("===== close :: trainCloseStatus {}", trainCloseStatus);
    } catch (Exception e) {
      logger.error("===== close :: {}", e.getMessage());
      throw e;
    }
  }

  /**
   * STTModelEntity 삭제 로직
   */
  @Override
  @Transactional
  public void delete(String trainId) throws Exception {
    logger.debug("===== delete :: trainId {}", trainId);
    try {
      STTModelEntity sttModelEntity = new STTModelEntity();
      sttModelEntity.setId(trainId);
      sttModelService.deleteTrainingModel(sttModelEntity);
    } catch (Exception e) {
      logger.error("===== delete :: {}", e.getMessage());
      throw e;
    }
  }
}
