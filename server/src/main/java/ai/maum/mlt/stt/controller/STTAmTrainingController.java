package ai.maum.mlt.stt.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.common.file.entity.FileGroupEntity;
import ai.maum.mlt.common.file.service.FileGroupService;
import ai.maum.mlt.common.system.SystemCode;
import ai.maum.mlt.stt.entity.STTModelEntity;
import ai.maum.mlt.stt.entity.STTTrainingEntity;
import ai.maum.mlt.stt.service.STTAmTrainingService;
import ai.maum.mlt.stt.service.STTModelService;
import java.util.HashMap;
import java.util.List;
import maum.brain.stt.train.S3Train.TrainResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/stt/am/training")
public class STTAmTrainingController {

  private static final Logger logger = LoggerFactory.getLogger(STTAmTrainingController.class);

  @Autowired
  private STTModelService modelService;

  @Autowired
  private FileGroupService fileGroupService;

  @Autowired
  private STTAmTrainingService sttAmTrainingService;

  @UriRoleDesc(role = "stt^R")
  @RequestMapping(
      value = "/getFileGroups",
      method = RequestMethod.POST)
  public ResponseEntity<?> getFileGroups(@RequestBody FileGroupEntity param) {
    logger.info("===== call api [[/api/stt/am/training/getFileGroups]] :: {} ", param);
    try {
      List<FileGroupEntity> result = fileGroupService
          .getFileGroupList(SystemCode.FILE_GRP_PPOS_STT_AM, param.getWorkspaceId());
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getFileGroups e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "stt^X")
  @RequestMapping(
      value = "/sttOpen",
      method = RequestMethod.POST)
  public ResponseEntity<?> open(@RequestBody HashMap<String, Object> param) {
    logger.info("===== call api  [[/api/stt/am/training/sttOpen]] :: {} ", param);
    HashMap<String, Object> result = new HashMap<String, Object>();
    try {
      sttAmTrainingService.open(param.get("fileGroupId").toString(),
          param.get("workspaceId").toString(), param.get("trainType").toString());
      result.put("message", "SUCCESS");
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("open e : " , e);
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "stt^R")
  @RequestMapping(
      value = "/getTrainedSttModels",
      method = RequestMethod.POST)
  public ResponseEntity<?> getTrainedSttModels(@RequestBody String workspaceId) {
    logger.info("===== call api  [[/api/stt/am/training/getTrainedSttModels]] :: workspaceId {} ",
        workspaceId);
    HashMap<String, Object> result = new HashMap<String, Object>();
    try {
      List<STTModelEntity> tmList = modelService
          .selectTrainedModelList(workspaceId, TrainResult.success.toString(),
              SystemCode.FILE_GRP_PPOS_STT_AM);
      result.put("tmList", tmList);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getTrainedSttModels e : " , e);
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "stt^U")
  @RequestMapping(
      value = "/{id}/{workspaceId}/update/{key}",
      method = RequestMethod.POST)
  public void update(
      @PathVariable("key") String key,
      @PathVariable("id") String id,
      @PathVariable("workspaceId") String workspaceId,
      @RequestBody Object data) {
    logger.info(
        "===== call api  [[/api/stt/am/training/{id}/{workspaceId}/update/{key}]] :: key {}, id {}, workspaceId {}, data {}",
        key, id, workspaceId, data);
    try {
      sttAmTrainingService.update(key, id, workspaceId, data);
    } catch (Exception e) {
      logger.error("update e : " , e);
    }
  }

  @UriRoleDesc(role = "stt^R")
  @RequestMapping(
      value = "/getAllProgress",
      method = RequestMethod.POST)
  public ResponseEntity<?> getAllProgress(@RequestBody String workspaceId) {
    logger.info("===== call api  [[/api/stt/am/training/sttGetAllProgress]] :: workspaceId {} ",
        workspaceId);
    HashMap<String, Object> result = new HashMap<String, Object>();
    try {
      List<STTTrainingEntity> sttTrainingEnties = sttAmTrainingService.getAllProgress(workspaceId);
      result.put("tmList", sttTrainingEnties);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getAllProgress e : " , e);
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "stt^X")
  @RequestMapping(
      value = "/stop",
      method = RequestMethod.POST)
  public ResponseEntity<?> stop(@RequestBody String val) {
    logger.info("===== call api  [[/api/stt/am/training/stop]] :: val {}", val);
    HashMap<String, Object> result = new HashMap<String, Object>();
    try {
      sttAmTrainingService.stop(val);
      result.put("message", "SUCCESS");
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("stop e : " , e);
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "stt^X")
  @RequestMapping(
      value = "/close",
      method = RequestMethod.POST)
  public ResponseEntity<?> close(@RequestBody String val) {
    logger.info("===== call api  [[/api/stt/am/training/close]] :: val {}", val);
    HashMap<String, Object> result = new HashMap<String, Object>();
    try {
      sttAmTrainingService.close(val);
      result.put("message", "SUCCESS");
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("close e : " , e);
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "stt^D")
  @RequestMapping(
      value = "/delete",
      method = RequestMethod.POST)
  public ResponseEntity<?> delete(@RequestBody String val) {
    logger.info("===== call api  [[/api/stt/am/training/delete]] :: val {} ", val);
    HashMap<String, Object> result = new HashMap<String, Object>();
    try {
      sttAmTrainingService.delete(val);
      result.put("message", "success");
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("delete e : " , e);
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
