package ai.maum.mlt.stt.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.common.file.entity.FileGroupEntity;
import ai.maum.mlt.common.file.service.FileGroupService;
import ai.maum.mlt.common.git.GitManager;
import ai.maum.mlt.common.system.SystemCode;
import ai.maum.mlt.stt.entity.STTTranscriptEntity;
import ai.maum.mlt.stt.service.STTTranscriptService;
import java.util.HashMap;
import java.util.List;
import org.eclipse.jgit.lib.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/stt/am/quality-assurance")
public class STTAmQualityAssuranceController {

  private static final Logger logger = LoggerFactory
      .getLogger(STTAmQualityAssuranceController.class);

  @Value("${git.file.path}")
  private String gitPath;

  @Autowired
  private STTTranscriptService sttTranscriptService;

  @Autowired
  private FileGroupService fileGroupService;

  @UriRoleDesc(role = "stt^R")
  @RequestMapping(
      value = "/getFileGroups",
      method = RequestMethod.POST)
  public ResponseEntity<?> getFileGroups(@RequestBody FileGroupEntity param) {
    logger.info("===== call api [[/api/stt/am/quality-assurance/getFileGroups]] :: {}", param);
    try {
      List<FileGroupEntity> fileGroupEntities = fileGroupService
          .getFileGroupList(SystemCode.FILE_GRP_PPOS_STT_AM, param.getWorkspaceId());
      return new ResponseEntity<>(fileGroupEntities, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getFileGroups e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "stt^R")
  @RequestMapping(
      value = "/getTranscriptionList",
      method = RequestMethod.POST)
  public ResponseEntity<?> getFileList(@RequestBody STTTranscriptEntity param) {
    logger.info("===== call api  GET [[/api/stt/am/quality-assurance/getTranscriptionList]] :: {}",
        param);
    try {
      Page<STTTranscriptEntity> pageResult = sttTranscriptService.selectTranscriptList(param);
      return new ResponseEntity<>(pageResult, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getFileList e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "stt^R")
  @RequestMapping(
      value = "/getTranscript",
      method = RequestMethod.POST)
  public ResponseEntity<?> getTranscript(@RequestBody STTTranscriptEntity param) {
    logger.info("===== call api  GET [[/api/stt/am/quality-assurance/getTranscript]] :: {}", param);
    try {
      STTTranscriptEntity sttTranscriptEntity = sttTranscriptService.selectTranscript(param);
      return new ResponseEntity<>(sttTranscriptEntity, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getTranscript e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "stt^U")
  @RequestMapping(
      value = "/update-alert",
      method = RequestMethod.POST)
  public ResponseEntity<?> updateAlert(@RequestBody STTTranscriptEntity param) {
    logger.info("===== call api  GET [[/api/stt/am/quality-assurance/update-alert]] :: {}", param);
    try {
      STTTranscriptEntity result = sttTranscriptService.updateAlert(param);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("updateAlert e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "stt^D")
  @RequestMapping(
      value = "/delete-alert",
      method = RequestMethod.POST)
  public ResponseEntity<?> deleteAlert(@RequestBody STTTranscriptEntity param) {
    logger.info("===== call api  GET [[/api/stt/am/quality-assurance/delete-alert]] :: {}", param);
    try {
      sttTranscriptService.deleteTranscript(param);
      return new ResponseEntity<>(HttpStatus.OK);
    } catch (Exception e) {
      logger.error("deleteAlert e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "stt^U")
  @RequestMapping(
      value = "/commit",
      method = RequestMethod.POST)
  public ResponseEntity<?> commit(@RequestBody STTTranscriptEntity param) {
    logger.info("===== call api  GET [[/api/stt/am/quality-assurance/commit]] :: {}", param);
    try {
      STTTranscriptEntity sttTranscriptEntity = sttTranscriptService.updateTranscript(param);

      if (sttTranscriptEntity != null) {
        GitManager gs = new GitManager();
        HashMap<String, String> value = new HashMap<String, String>();
        value.put("gitPath", gitPath);
        value.put("workspaceId", sttTranscriptEntity.getWorkspaceId());
        value.put("name", "admin");
        value.put("email", "mlt@ai");
        value.put("fileName", param.getId());
        value.put("content", param.getVersion());
        value.put("messsage", "");
        gs.updateFile(value);
      }

      return new ResponseEntity<>(sttTranscriptEntity, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("commit e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "stt^R")
  @RequestMapping(
      value = "/getCommitList",
      method = RequestMethod.POST)
  public ResponseEntity<?> getCommitList(@RequestBody STTTranscriptEntity param) {
    logger.info("===== call api  GET [[/api/stt/am/quality-assurance/getCommitList]] :: {}", param);
    try {
      GitManager gs = new GitManager();
      gs.setWorkingDirectory(gitPath);
      List<HashMap<String, Object>> result = gs
          .getCommitList(param.getWorkspaceId(), param.getId());

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getCommitList e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "stt^R")
  @RequestMapping(
      value = "/getDiff",
      method = RequestMethod.POST)
  public ResponseEntity<?> getDiff(@RequestBody HashMap<String, String> param) {
    logger.info("===== call api  GET [[/api/stt/am/quality-assurance/getDiff]] :: {}", param);
    try {
      GitManager gs = new GitManager();
      gs.setWorkingDirectory(gitPath);

      String result = gs.diffFile(param.get("workspaceId"), param.get("id"), param.get("source"),
          param.get("target"));

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getDiff e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "stt^R")
  @RequestMapping(
      value = "/getFileByCommit",
      method = RequestMethod.POST)
  public ResponseEntity<?> getFileByCommit(@RequestBody HashMap<String, String> param) {
    logger
        .info("===== call api  GET [[/api/stt/am/quality-assurance/getFileByCommit]] :: {}", param);

    try {
      GitManager gs = new GitManager();
      gs.setWorkingDirectory(gitPath);

      String result = gs.readFileFromCommit(param.get("workspaceId"), param.get("id"),
          ObjectId.fromString(param.get("source")));

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getFileByCommit e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
