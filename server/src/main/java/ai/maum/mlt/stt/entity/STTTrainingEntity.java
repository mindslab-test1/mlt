package ai.maum.mlt.stt.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "MAI_STT_TRAINING")
public class STTTrainingEntity implements Serializable {

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid")
  @Column(name = "ID", length = 40, nullable = false)
  private String id;

  @Column(name = "WORKSPACE_ID", length = 40, nullable = false)
  private String workspaceId;

  @Column(name = "STT_KEY", length = 255)
  private String sttKey;

  @Transient
  private Integer progress;

  @Transient
  private String name;
}
