package ai.maum.mlt.stt.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "MAI_STT_TRANSCRIPT_LINE")
public class STTTranscriptLineEntity implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "STT_TRANSCRIPT_LINE_GEN")
  @SequenceGenerator(name = "STT_TRANSCRIPT_LINE_GEN", sequenceName = "MAI_STT_TRANSCRIPT_SEQ", allocationSize = 5)
  @Column(name = "SEQ", length = 40, nullable = false)
  private Integer seq;

  @Column(name = "VERSION_ID", length = 40, nullable = false)
  private String versionId;

  @Lob
  @Column(name = "TEXT")
  private String text;

  @ManyToOne
  @JoinColumn(name = "VERSION_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private STTTranscriptEntity sttTranscriptEntity;
}
