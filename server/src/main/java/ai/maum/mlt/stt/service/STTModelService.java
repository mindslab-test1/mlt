package ai.maum.mlt.stt.service;

import ai.maum.mlt.stt.entity.STTModelEntity;
import java.util.List;

public interface STTModelService {

  STTModelEntity selectTrainingModel(String key) throws Exception;

  List<STTModelEntity> selectTrainedModelList(String workspaceId, String status) throws Exception;

  List<STTModelEntity> selectTrainedModelList(String workspaceId, String status, String purpose)
      throws Exception;

  STTModelEntity selectByNameAndPurposeAndRateAndWorkspaceId(String name, String purpose, int rate,
      String workspaceId) throws Exception;

  STTModelEntity insertTrainingModel(STTModelEntity sttModelEntity) throws Exception;

  STTModelEntity updateTrainingModel(STTModelEntity sttModelEntity) throws Exception;

  void deleteTrainingModel(STTModelEntity sttModelEntity) throws Exception;
}
