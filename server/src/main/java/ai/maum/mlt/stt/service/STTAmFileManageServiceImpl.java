package ai.maum.mlt.stt.service;

import ai.maum.mlt.common.file.entity.FileEntity;
import ai.maum.mlt.common.file.entity.FileGroupEntity;
import ai.maum.mlt.common.file.entity.FileGroupRelEntity;
import ai.maum.mlt.common.file.repository.FileGroupRelRepository;
import ai.maum.mlt.common.file.repository.FileGroupRepository;
import ai.maum.mlt.common.file.repository.FileRepository;
import ai.maum.mlt.common.file.service.FileGroupService;
import ai.maum.mlt.common.file.service.FileService;
import ai.maum.mlt.common.system.SystemCode;
import ai.maum.mlt.common.system.SystemErrMsg;
import ai.maum.mlt.common.util.FileUtils;
import ai.maum.mlt.stt.entity.STTTranscriptEntity;
import ai.maum.mlt.stt.repository.STTTranscriptRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.transaction.Transactional;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class STTAmFileManageServiceImpl implements STTAmFileManageService {

  private static final Logger logger = LoggerFactory.getLogger(STTAmFileManageServiceImpl.class);

  @Value("${stt.file.path}")
  private String sttPath;

  @Autowired
  private FileUtils fileUtils;

  @Autowired
  private FileService fileService;

  @Autowired
  private FileGroupService fileGroupService;

  @Autowired
  private FileRepository fileRepository;

  @Autowired
  private FileGroupRepository fileGroupRepository;

  @Autowired
  private FileGroupRelRepository fileGroupRelRepository;

  @Autowired
  private STTTranscriptRepository sttTranscriptRepository;

  @Autowired
  private STTTranscriptService sttTranscriptService;

  @Autowired
  private STTAnalysisResultService sttAnalysisResultService;

  /**
   * STT AM 파일(wav, transcript txt) 업로드 처리 로직 transcript txt 파일의 경우 동일한 이름의 wav파일이 업로드 되어있을 경우에만
   * 업로드를 진행하고 그리고 나서 해당 wav 파일의 STTTranscriptEntity에 내용을 저장함 wav 파일의 경우 fileGroup의 Rate 와 맞는지 체크후
   * 업로드 진행함
   *
   * @param file 업로드 할 파일
   * @return HashMap (msg: 결과 메세지, name: 업로드 파일명, id: fileEntity ID값, isSuccess: 파일업로드 성공 여부)
   */
  @Override
  @Transactional
  public HashMap<String, Object> uploadFile(MultipartFile file, String workspaceId,
      String fileGroupId) throws Exception {
    logger.debug("===== uploadFile :: file {}, workspaceId {}, fileGroupId {}",
        file.getOriginalFilename(), workspaceId, fileGroupId);
    try {
      HashMap<String, Object> resultMap = new HashMap<>();
      String[] fileNameArr = file.getOriginalFilename().split("\\.");
      String fileExtension = "." + fileNameArr[fileNameArr.length - 1].toLowerCase();
      if (fileExtension.equals(".txt")) {
        // transcript 파일 업로드 처리 로직, wav 파일과 이름 동일해야함
        String name = "";
        for (int i = 0; i < fileNameArr.length - 1; i++) {
          name += fileNameArr[i];
        }
        name += ".wav";
        FileEntity fileEntity = fileRepository
            .findByWorkspaceIdAndPurposeAndFileGroupIdAndName(workspaceId,
                SystemCode.FILE_GRP_PPOS_STT_AM, fileGroupId, name);

        if (fileEntity != null) {
          STTTranscriptEntity sttTranscriptEntity =
              sttTranscriptRepository
                  .findByFileGroupIdAndFileIdAndWorkspaceId(fileGroupId, fileEntity.getId(),
                      workspaceId);
          if (sttTranscriptEntity.getVersion() == null || sttTranscriptEntity.getVersion()
              .equals("")) {
            String content = new String(file.getBytes());
            sttTranscriptEntity.setVersion(content);
            sttTranscriptEntity.setUpdatedAt(new Date());
            sttTranscriptRepository.save(sttTranscriptEntity);
          } else {
            throw new Exception(SystemErrMsg.TRANSCRIPT_EXIST);
          }
        } else {
          throw new Exception(SystemErrMsg.WAV_DOES_NOT_EXIST);
        }
      } else {
        resultMap = fileService
            .insertSttFile(file, workspaceId, sttPath, SystemCode.FILE_GRP_PPOS_STT_AM,
                SystemCode.FILE_EXTENSION_WAV, fileGroupId);
        STTTranscriptEntity sttTranscriptEntity = new STTTranscriptEntity();
        sttTranscriptEntity.setName(file.getOriginalFilename());
        sttTranscriptEntity.setWorkspaceId(workspaceId);
        sttTranscriptEntity.setFileId(resultMap.get("id").toString());
        sttTranscriptEntity.setVersion("");
        sttTranscriptEntity.setFileGroupId(fileGroupId);
        sttTranscriptEntity.setCreatedAt(new Date());
        sttTranscriptEntity.setUpdatedAt(new Date());
        sttTranscriptRepository.save(sttTranscriptEntity);
      }

      return resultMap;
    } catch (Exception e) {
      logger.error("===== uploadFile :: {}", e.getMessage());
      throw e;
    }
  }

  /**
   * STT_AM 파일이면서 fileGroup의 rate와 동일한 파일중 fileGroup에 속해있지 않은 파일리스트들 가져오는 로직
   *
   * @param fileEntity FileEntity의 fileGroupId 값 필요
   * @return Page<FileEntity>
   */
  @Override
  public Page<FileEntity> getFilesOutOfGroup(FileEntity fileEntity) throws Exception {
    logger.debug("===== getFilesOutOfGroup :: fileEntity {}", fileEntity);
    try {
      Page<FileEntity> result = null;
      if (fileEntity.getFileGroupId() != null) {
        fileEntity.setPurpose(SystemCode.FILE_GRP_PPOS_STT_AM);
        FileGroupEntity fileGroupEntity = fileGroupRepository.findOne(fileEntity.getFileGroupId());
        JSONParser parser = new JSONParser();
        JSONObject fileGroupMeta = (JSONObject) parser.parse(fileGroupEntity.getMeta());
        fileEntity.setMeta("\"rate\":\"" + fileGroupMeta.get("rate").toString() + "\"");
        result = fileRepository
            .findAllByNotInGroupId(fileEntity.getWorkspaceId(), fileEntity.getPurpose(),
                fileEntity.getFileGroupId(), fileEntity.getName(), fileEntity.getPageRequest(),
                fileEntity.getMeta());
      }
      return result;
    } catch (Exception e) {
      logger.error("===== getFilesOutOfGroup :: {}", e.getMessage());
      throw e;
    }
  }

  /**
   * 파일 업로드는 하지 않고 기존에 업로드 되어있는 파일을 파일그룹에 포합시키는 로직 include 하면 STTTranscriptEntity도 같이 생성해줌
   *
   * @param fileEntity FileEntity의 workspaceId 값과 fileGroupId 값과 Ids에 include할 File Id값이 있는 배열값 필요
   */
  @Override
  @Transactional
  public void includeFile(FileEntity fileEntity) throws Exception {
    logger.debug("===== includeFile :: fileEntity", fileEntity);
    List<FileGroupRelEntity> fileGroupRelEntities = new ArrayList<>();
    List<STTTranscriptEntity> sttTranscriptEntities = new ArrayList<>();
    try {
      for (String id : fileEntity.getIds()) {
        FileEntity f = fileRepository.getOne(id);
        FileGroupRelEntity fileGroupRelEntity = new FileGroupRelEntity();
        fileGroupRelEntity.setFileId(id);
        fileGroupRelEntity.setFileGroupId(fileEntity.getFileGroupId());
        fileGroupRelEntities.add(fileGroupRelEntity);

        STTTranscriptEntity sttTranscriptEntity = new STTTranscriptEntity();
        sttTranscriptEntity.setName(f.getName());
        sttTranscriptEntity.setWorkspaceId(fileEntity.getWorkspaceId());
        sttTranscriptEntity.setFileId(id);
        sttTranscriptEntity.setVersion("");
        sttTranscriptEntity.setFileGroupId(fileEntity.getFileGroupId());
        sttTranscriptEntity.setCreatedAt(new Date());
        sttTranscriptEntity.setUpdatedAt(new Date());
        sttTranscriptEntities.add(sttTranscriptEntity);
      }

      fileGroupService.insertFileGroupRel(fileGroupRelEntities);
      sttTranscriptRepository.save(sttTranscriptEntities);
    } catch (Exception e) {
      logger.error("===== includeFile :: {}", e.getMessage());
      throw e;
    }
  }

  /**
   * 파일 그룹에 포함된 파일중 파일을 삭제하지 않고 그룹에서 제외시키는 로직 관계가 있는 STTAnalysisResultEntity, STTTranscriptEntity
   * 삭제후 FileGroupRel삭제 진행
   *
   * @param fileEntity FileEntity의 workspaceId 값과 fileGroupId 값과 Ids에 exclude할 File Id값이 있는 배열값 필요
   */
  @Override
  @Transactional
  public void excludeFile(FileEntity fileEntity) throws Exception {
    logger.debug("===== excludeFile :: fileEntity {}", fileEntity);
    try {
      List<FileGroupRelEntity> fileGroupRelEntities = new ArrayList<>();

      for (String id : fileEntity.getIds()) {
        FileGroupRelEntity fileGroupRelEntity = new FileGroupRelEntity();
        fileGroupRelEntity.setFileId(id);
        fileGroupRelEntity.setFileGroupId(fileEntity.getFileGroupId());
        fileGroupRelEntities.add(fileGroupRelEntity);
        sttTranscriptRepository
            .deleteAllByFileIdAndWorkspaceIdAndFileGroupId(id, fileEntity.getWorkspaceId(),
                fileEntity.getFileGroupId());
        sttAnalysisResultService.deleteResultByFileId(id);
      }

      fileGroupService.deleteFileGroupRel(fileGroupRelEntities);
    } catch (Exception e) {
      logger.error("===== excludeFile :: {}", e.getMessage());
      throw e;
    }
  }

  /**
   * 파일 삭제 하는 로직 FileEntity와 관계있는 Entity(STTAnalysisResultEntity, STTTranscriptEntity,
   * FileGroupRelEntity) 먼저 삭제후 FileEntity 삭제함
   *
   * @param fileEntity FileEntity의 workspaceId 값과 fileGroupId 값과 Ids에 삭제할 File Id값이 있는 배열값 필요
   */
  @Override
  @Transactional
  public void deleteFile(FileEntity fileEntity) throws Exception {
    logger.debug("===== deleteFile :: fileEntity {}", fileEntity);
    try {
      for (String id : fileEntity.getIds()) {
        if (fileGroupRelRepository.countAllByFileId(id) > 1) {
          throw new Exception(SystemErrMsg.FILE_INCLUDED_IN_OTHER_FILE_GROUP);
        } else {
          sttAnalysisResultService.deleteResultByFileId(fileEntity.getId());
          sttTranscriptRepository
              .deleteAllByFileIdAndWorkspaceIdAndFileGroupId(id, fileEntity.getWorkspaceId(),
                  fileEntity.getFileGroupId());
          fileService.deleteFile(fileEntity);
          fileUtils.deleteFile(fileEntity.getIds(), sttPath, SystemCode.FILE_EXTENSION_TEXT);
        }
      }
    } catch (Exception e) {
      logger.error("===== deleteFile :: {}", e.getMessage());
      throw e;
    }
  }

  /**
   * 파일그룹 삭제하는 로직 FileGroupEntity와 관계있는 Entity(STTAnalysisResultEntity, STTTranscriptEntity,
   * FileGroupRelEntity) 먼저 삭제후 FileGroupEntity 삭제 처리
   *
   * @param fileGroupEntity FileGroupEntity의 id 값 필요
   */
  @Override
  @Transactional
  public void deleteFileGroup(FileGroupEntity fileGroupEntity) throws Exception {
    logger.debug("===== deleteFileGroup :: fileGroupEntity {}", fileGroupEntity);
    try {
      sttAnalysisResultService.deleteResultByFileGroupId(fileGroupEntity.getId());
      sttTranscriptService.deleteTranscriptByFileGroupId(fileGroupEntity.getId());
      fileGroupService.deleteFileGroup(fileGroupEntity.getId());
    } catch (Exception e) {
      logger.error("===== deleteFileGroup ERROR :: {}", e.getMessage());
      throw e;
    }
  }
}
