package ai.maum.mlt.stt.service;

import ai.maum.mlt.itfc.SttGrpcInterfaceManager;
import ai.maum.mlt.stt.entity.STTAnalysisResultEntity;
import ai.maum.mlt.stt.entity.STTEvalResultEntity;
import ai.maum.mlt.stt.entity.STTTranscriptEntity;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.transaction.Transactional;
import maum.brain.stt.train.Eval.EvalPair;
import maum.brain.stt.train.Eval.EvalRequest;
import maum.brain.stt.train.Eval.EvalResult;
import maum.brain.stt.train.Eval.EvalUnit;
import net.minidev.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class STTEvaluationServiceImpl implements STTEvaluationService {

  private static final Logger logger = LoggerFactory.getLogger(STTEvaluationServiceImpl.class);

  @Value("${stt.eval.server.ip}")
  private String ip;
  @Value("${stt.eval.server.port}")
  private int port;

  @Autowired
  private STTTranscriptService sttTranscriptService;

  @Autowired
  private STTAnalysisResultService sttAnalysisResultService;

  @Autowired
  private STTEvalResultService sttEvalResultService;

  /**
   * STTAnalysisResultEntity로 brain-stt-eval 통해 인식률 요청 하고 STTEvalResultEntity에 저장하는 로직
   *
   * @param param List<STTAnalysisResultEntity>
   * @return List<STTEvalResultEntity>
   */
  @Override
  @Transactional
  public List<STTEvalResultEntity> evaluate(List<STTAnalysisResultEntity> param) throws Exception {
    logger.debug("===== evaluate :: List<STTAnalysisResultEntity> {}", param);
    try {

      SttGrpcInterfaceManager client = new SttGrpcInterfaceManager(ip, port);
      List<STTEvalResultEntity> list = new ArrayList<>();
      for (STTAnalysisResultEntity sttAnalysisResultEntity : param) {
        String textFromEngine = sttAnalysisResultService.selectResult(sttAnalysisResultEntity).
            getText();

        STTTranscriptEntity sttTranscriptEntity = new STTTranscriptEntity();
        sttTranscriptEntity.setId(sttAnalysisResultEntity.getTranscriptId());
        sttTranscriptEntity = sttTranscriptService.selectTranscript(sttTranscriptEntity);
        String textFromTranscript = sttTranscriptEntity.getVersion();

        EvalRequest.Builder request = EvalRequest.newBuilder();
        EvalPair.Builder evalPair = EvalPair.newBuilder();

        evalPair.setSolveContents(textFromEngine); // from engine
        evalPair.setAnswerContents(textFromTranscript); // from transcript

        request.addPairs(evalPair);
        request.setUnitType(EvalUnit.EVAL_SYLLABLE_UNIT);

        EvalResult evalResult = client.requestEvaluation(request.build());
        logger.debug("===== evaluate :: evalResult {}", evalResult);
        JSONObject object = new JSONObject();
        float resultCorrVal =
            (float) evalResult.getHitCount() / (float) evalResult.getTotalCount() * 100;
        float forAccVal1 = evalResult.getSwitchCount() + evalResult.getDeleteCount() +
            evalResult.getInsertCount();
        float forAccVal2 = evalResult.getDeleteCount() + evalResult.getDeleteCount() +
            evalResult.getHitCount();
        float resultAccVal = forAccVal1 / forAccVal2;

        object.put("correctness", String.format("%.2f", resultCorrVal) + "%");
        object.put("accuracy", String.format("%.2f", 100 - 100.0 * resultAccVal) + "%");
        object.put("total", evalResult.getTotalCount());
        object.put("hit", evalResult.getHitCount());
        object.put("deletion", evalResult.getDeleteCount());
        object.put("insertion", evalResult.getInsertCount());
        object.put("substitution", evalResult.getSwitchCount());

        STTEvalResultEntity sttEvalResultEntity = new STTEvalResultEntity();
        sttEvalResultEntity.setResult(object.toString());
        sttEvalResultEntity.setCreatedAt(new Date());
        sttEvalResultEntity.setUpdatedAt(new Date());
        sttEvalResultEntity.setWorkspaceId(sttAnalysisResultEntity.getWorkspaceId());
        sttEvalResultEntity.setAnalysisResultId(sttAnalysisResultEntity.getId());
        list.add(sttEvalResultEntity);
      }
      return sttEvalResultService.insertEvaluationResultList(list);
    } catch (Exception e) {
      logger.error("===== evaluate :: {}", e.getMessage());
      throw e;
    }
  }

}