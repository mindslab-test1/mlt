package ai.maum.mlt.stt.service;

import ai.maum.mlt.common.file.entity.FileEntity;
import ai.maum.mlt.common.file.entity.FileGroupEntity;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

public interface STTAmFileManageService {

  HashMap<String, Object> uploadFile(MultipartFile file, String workspaceId, String fileGroupId)
      throws Exception;

  Page<FileEntity> getFilesOutOfGroup(FileEntity fileEntity) throws Exception;

  void includeFile(FileEntity fileEntity) throws Exception;

  void excludeFile(FileEntity fileEntity) throws Exception;

  void deleteFile(FileEntity fileEntity) throws Exception;

  void deleteFileGroup(FileGroupEntity fileGroupEntity) throws Exception;
}
