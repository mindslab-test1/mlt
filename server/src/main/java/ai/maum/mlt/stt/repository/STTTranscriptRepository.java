package ai.maum.mlt.stt.repository;

import ai.maum.mlt.stt.entity.STTTranscriptEntity;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface STTTranscriptRepository extends JpaRepository<STTTranscriptEntity, String> {

  @Query(value = "SELECT t "
      + "         FROM STTTranscriptEntity t join t.fileEntity f"
      + "         WHERE (t.workspaceId = :workspaceId OR :workspaceId IS NULL)"
      + "         AND (f.name LIKE CONCAT('%', :name ,'%') OR :name IS NULL)"
      + "         AND (t.fileGroupId = :fileGroupId OR :fileGroupId IS NULL) ")
  Page<STTTranscriptEntity> findAllbyName(@Param("name") String name,
      @Param("workspaceId") String workspaceId,
      @Param("fileGroupId") String fileGroupId, Pageable pageable);

  List<STTTranscriptEntity> findAllByFileGroupId(String fileGroupId);

  STTTranscriptEntity findByFileGroupIdAndFileIdAndWorkspaceId(String fileGroupId, String fileId,
      String workspaceId);

  @Modifying
  @Transactional
  @Query(value = "UPDATE STTTranscriptEntity f "
      + "         SET f.version =:version "
      + "         WHERE f.id =:id")
  void updateVersion(@Param("id") String id, @Param("version") String version);

  @Modifying
  @Transactional
  @Query(value = "UPDATE STTTranscriptEntity f "
      + "         SET f.alert =:alert "
      + "         WHERE f.id =:id")
  void updateAlert(@Param("id") String id, @Param("alert") String alert);

  @Query(value = "SELECT count(t.id)"
      + "         FROM FileEntity f,STTTranscriptEntity t "
      + "         WHERE f.purpose =:purpose"
      + "         AND f.workspaceId =:workspaceId"
      + "         AND f.id = t.fileId"
      + "         AND t.workspaceId =:workspaceId"
      + "         AND t.transcriberId is not null")
  Integer countAllByTranscriptComplete(@Param("purpose") String purpose,
      @Param("workspaceId") String workspaceId);

  @Query(value = "SELECT count(t.id)"
      + "         FROM FileEntity f,STTTranscriptEntity t "
      + "         WHERE f.purpose =:purpose"
      + "         AND f.workspaceId =:workspaceId"
      + "         AND f.id = t.fileId"
      + "         AND t.workspaceId =:workspaceId"
      + "         AND t.qaerId is not null")
  Integer countAllByQualityAssuranceComplete(@Param("purpose") String purpose,
      @Param("workspaceId") String workspaceId);

  void deleteByFileIdAndWorkspaceId(String fileId, String workspaceId);

  STTTranscriptEntity findByFileIdAndFileGroupIdAndWorkspaceId(String fileId, String fileGroupId,
      String workspaceId);

  void deleteAllByFileIdAndWorkspaceIdAndFileGroupId(String fileId, String workspaceId,
      String fileGroupId);

  void deleteAllByFileGroupId(String fileGroupId);
}
