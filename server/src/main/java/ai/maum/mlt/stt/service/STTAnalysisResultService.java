package ai.maum.mlt.stt.service;

import ai.maum.mlt.stt.entity.STTAnalysisResultEntity;
import java.util.List;
import org.springframework.data.domain.Page;

public interface STTAnalysisResultService {

  STTAnalysisResultEntity selectResult(STTAnalysisResultEntity param) throws Exception;

  Page<STTAnalysisResultEntity> selectResultList(STTAnalysisResultEntity param) throws Exception;

  Page<STTAnalysisResultEntity> selectResultByFileGroupIdAndAmModelIdAndLmModelIdAndName(
      STTAnalysisResultEntity param) throws Exception;

  List<STTAnalysisResultEntity> selectResultByModelId(String modelId) throws Exception;

  void insertResult(STTAnalysisResultEntity param) throws Exception;

  void deleteResult(List<STTAnalysisResultEntity> param) throws Exception;

  void deleteResultByFileGroupId(String fileGroupId) throws Exception;

  void deleteResultByFileId(String fileId) throws Exception;
}
