package ai.maum.mlt.stt.repository;

import ai.maum.mlt.stt.entity.STTTranscriptLineEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface STTTranscriptLineRepository extends
    JpaRepository<STTTranscriptLineEntity, Integer> {

}
