package ai.maum.mlt.stt.service;

import ai.maum.mlt.stt.entity.STTTranscriptEntity;
import org.springframework.data.domain.Page;

public interface STTTranscriptService {

  Page<STTTranscriptEntity> selectTranscriptList(STTTranscriptEntity param) throws Exception;

  STTTranscriptEntity selectTranscript(STTTranscriptEntity param) throws Exception;

  Integer selectTranscriptCompleteCount(String purpose, String workspaceId) throws Exception;

  Integer selectQualityAssuranceCompleteCount(String purpose, String workspaceId) throws Exception;

  STTTranscriptEntity selectTranscriptByFileIdAndFileGroupIdAndWorkspaceId(String fileId,
      String fileGroupId, String workspaceId) throws Exception;

  STTTranscriptEntity insertTranscript(STTTranscriptEntity param) throws Exception;

  STTTranscriptEntity updateTranscript(STTTranscriptEntity param) throws Exception;

  STTTranscriptEntity updateAlert(STTTranscriptEntity param) throws Exception;

  void deleteTranscript(STTTranscriptEntity param) throws Exception;

  void deleteTranscriptByFileGroupId(String fileGroupId) throws Exception;
}
