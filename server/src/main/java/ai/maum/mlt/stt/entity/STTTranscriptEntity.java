package ai.maum.mlt.stt.entity;

import ai.maum.mlt.common.auth.entity.UserEntity;
import ai.maum.mlt.common.auth.entity.WorkspaceEntity;
import ai.maum.mlt.common.file.entity.FileEntity;
import ai.maum.mlt.common.file.entity.FileGroupEntity;
import ai.maum.mlt.common.pagenation.PageParameters;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.LastModifiedDate;

@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode(callSuper=false)
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "MAI_STT_TRANSCRIPT")
public class STTTranscriptEntity extends PageParameters implements Serializable{

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid")
  @Column(name = "ID", length = 40, nullable = false)
  private String id;

  @Column(name = "FILE_ID", length = 40, nullable = false)
  private String fileId;

  @Column(name = "TRANSCRIBER_ID")
  private Integer transcriberId;

  @Column(name = "WORKSPACE_ID", length = 40, nullable = false)
  private String workspaceId;

  @Column(name = "QAER_ID", length = 50)
  private String qaerId;

  @Column(name = "ALERT", length = 200)
  private String alert;

  @Column(name = "NAME", length = 200)
  private String name;

  @Lob
  @Column(name = "VERSION" )
  private String version;

  @Column(name = "APPLIED", length = 200)
  private String applied;

  @Column(name = "FILE_GROUP_ID")
  private String fileGroupId;

  @Column(name = "CREATOR_ID", length = 40)
  private String creatorId;

  @CreatedDate
  @Column(name = "CREATED_AT")
  private Date createdAt;

  @Column(name = "UPDATER_ID", length = 40)
  private String updaterId;

  @LastModifiedDate
  @Column(name = "UPDATED_AT")
  private Date updatedAt;

  @ManyToOne
  @JoinColumn(name = "FILE_GROUP_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private FileGroupEntity fileGroupEntity;

  @ManyToOne
  @JoinColumn(name = "CREATOR_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity createUserEntity;

  @ManyToOne
  @JoinColumn(name = "UPDATER_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity updateUserEntity;

  @ManyToOne
  @JoinColumn(name = "QAER_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity qaUserEntity;

  @ManyToOne
  @JoinColumn(name = "WORKSPACE_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private WorkspaceEntity workspaceEntity;

  @ManyToOne
  @JoinColumn(name = "FILE_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private FileEntity fileEntity;

  @Transient
  private List<String> ids;
}
