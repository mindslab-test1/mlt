package ai.maum.mlt.stt.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.common.file.entity.FileEntity;
import ai.maum.mlt.common.file.entity.FileGroupEntity;
import ai.maum.mlt.common.file.service.FileGroupService;
import ai.maum.mlt.common.file.service.FileService;
import ai.maum.mlt.common.system.SystemCode;
import ai.maum.mlt.common.util.FileUtils;
import ai.maum.mlt.stt.service.STTAmFileManageService;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("api/stt/am/file-manage")
public class STTAmFileManagementController {

  private static final Logger logger = LoggerFactory.getLogger(STTAmFileManagementController.class);

  @Autowired
  private FileService fileService;

  @Autowired
  private FileGroupService fileGroupService;

  @Autowired
  private STTAmFileManageService sttAmFileManageService;

  @Autowired
  private FileUtils fileUtils;

  @Value("${stt.file.path}")
  private String sttPath;

  @Value("${multipart.maxFileSize}")
  private int maxFileSize;

  @UriRoleDesc(role = "stt^R")
  @RequestMapping(
      value = "/getFileGroups",
      method = RequestMethod.POST)
  public ResponseEntity<?> getFileGroups(@RequestBody FileGroupEntity param) {
    logger.info("===== call api [[/api/stt/am/file-manage/getFileGroups]] :: {}", param);
    try {
      List<FileGroupEntity> fileGroupEntities = fileGroupService
          .getFileGroupList(SystemCode.FILE_GRP_PPOS_STT_AM, param.getWorkspaceId());
      return new ResponseEntity<>(fileGroupEntities, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getFileGroups e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "stt^C")
  @RequestMapping(
      value = "/insertGroup",
      method = RequestMethod.POST)
  public ResponseEntity<?> insertGroup(@RequestBody FileGroupEntity param) {
    logger.info("======= call api  POST [[/api/ta/insertGroup]] =======");

    try {

      fileGroupService.insertFileGroup(param, SystemCode.FILE_GRP_PPOS_STT_AM);
      return new ResponseEntity<>(HttpStatus.OK);
    } catch (Exception e) {
      logger.error("insertGroup e : " , e);
      return new ResponseEntity<>( HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "stt^U")
  @RequestMapping(
      value = "/updateGroup",
      method = RequestMethod.POST)
  public ResponseEntity<?> updateGroup(@RequestBody FileGroupEntity param) {
    logger.info("===== call api [[/api/stt/am/file-manage/updateGroup]] :: {}", param);
    try {
      fileGroupService.updateFileGroup(param);
      return new ResponseEntity<>(HttpStatus.OK);
    } catch (Exception e) {
      logger.error("updateGroup e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "stt^D")
  @RequestMapping(
      value = "/deleteGroup",
      method = RequestMethod.POST)
  public ResponseEntity<?> deleteGroup(@RequestBody FileGroupEntity param) {
    logger.info("===== call api [[/api/stt/am/file-manage/deleteGroup]] :: {}", param);
    try {
      sttAmFileManageService.deleteFileGroup(param);
      return new ResponseEntity<>(HttpStatus.OK);
    } catch (Exception e) {
      logger.error("deleteGroup e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "stt^R")
  @RequestMapping(
      value = "/getGroupFileList",
      method = RequestMethod.POST)
  public ResponseEntity<?> getGroupFileList(@RequestBody FileEntity param) {
    logger.info("===== call api [[/api/stt/am/file-manage/getGroupFileList]] :: {}", param);
    try {
      param.setPurpose(SystemCode.FILE_GRP_PPOS_STT_AM);
      Page<FileEntity> pageResult = fileService.getGroupFileList(param);
      return new ResponseEntity<>(pageResult, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getGroupFileList e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "stt^R")
  @RequestMapping(
      value = "/getFileList",
      method = RequestMethod.POST)
  public ResponseEntity<?> getFileList(@RequestBody FileEntity param) {
    logger.info("===== call api [[/api/stt/am/file-manage/getFileList]] :: {}", param);
    try {
      Page<FileEntity> pageResult = sttAmFileManageService.getFilesOutOfGroup(param);
      return new ResponseEntity<>(pageResult, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getFileList e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "stt^C")
  @RequestMapping(
      value = "/upload-files",
      method = RequestMethod.POST)
  public ResponseEntity<?> insertFile(@RequestParam("workspaceId") String workspaceId,
      @RequestParam("groupId") String groupId,
      @RequestParam("file") MultipartFile file) {
    logger.info(
        "===== call api  [[/api/stt/am/file-manage/upload-files]] :: workspaceId {}, groupId {}, MultipartFile {}",
        workspaceId, groupId, file.getName());
    try {
      HashMap<String, Object> result = sttAmFileManageService
          .uploadFile(file, workspaceId, groupId);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("insertFile e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "stt^R")
  @RequestMapping(
      value = "/download-file/{workspaceId}/{id}",
      method = RequestMethod.GET)
  public ResponseEntity<?> downloadFile(@PathVariable("workspaceId") String workspaceId,
      @PathVariable("id") String id) throws IOException {
    logger.info(
        "===== call api  [[/api/stt/am/file-manage/download-file/{workspaceId}/{id}]] :: workspaceId {}, id {}",
        workspaceId, id);
    try {
      String fileName = id + SystemCode.FILE_EXTENSION_WAV;
      String path = fileUtils.getSystemConfPath(sttPath + "/" + workspaceId) + "/" + fileName;
      File file = new File(path);
      InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
      HttpHeaders httpHeaders = new HttpHeaders();
      httpHeaders.add(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + fileName);
      httpHeaders.add(HttpHeaders.CONTENT_TYPE, "application/octet-stream");
      return new ResponseEntity<>(resource, httpHeaders, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("downloadFile e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "stt^D")
  @RequestMapping(
      value = "/deleteFiles",
      method = RequestMethod.POST)
  public ResponseEntity<?> deleteFiles(@RequestBody FileEntity param) {
    logger.info("===== call api  [[/api/stt/am/file-manage/deleteFiles]] :: {}", param);
    try {
      sttAmFileManageService.deleteFile(param);
      return new ResponseEntity<>(HttpStatus.OK);
    } catch (Exception e) {
      logger.error("deleteFiles e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "stt^C")
  @RequestMapping(
      value = "/include-files",
      method = RequestMethod.POST)
  public ResponseEntity<?> includeFiles(@RequestBody FileEntity fileEntity) {
    logger.info("===== call api  [[/api/stt/am/file-manage/include-files]] :: {}", fileEntity);
    try {
      sttAmFileManageService.includeFile(fileEntity);
      return new ResponseEntity<>(HttpStatus.OK);
    } catch (Exception e) {
      logger.error("includeFiles e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }

  }

  @UriRoleDesc(role = "stt^D")
  @RequestMapping(
      value = "/exclude-files",
      method = RequestMethod.POST)
  public ResponseEntity<?> excludeFiles(@RequestBody FileEntity fileEntity) {
    logger.info("===== call api  [[/api/stt/am/file-manage/exclude-files]] :: {}", fileEntity);
    try {
      sttAmFileManageService.excludeFile(fileEntity);
      return new ResponseEntity<>(HttpStatus.OK);
    } catch (Exception e) {
      logger.error("excludeFiles e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
