package ai.maum.mlt.dictionary.service;

import ai.maum.mlt.dictionary.entity.NerDictionaryLineEntity;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface NerDictionaryLineService {
    List<NerDictionaryLineEntity> getDicLines(String versionId);
    void deleteDicLines(List<NerDictionaryLineEntity> nerDictionaryEntities);
    void insertLines(List<NerDictionaryLineEntity> nerDictionaryEntities);
    void insertLinesWithDate(List<NerDictionaryLineEntity> nerDictionaryEntities);
    int updateLine(NerDictionaryLineEntity entity);
    NerDictionaryLineEntity insertLine(NerDictionaryLineEntity entity);
    List<NerDictionaryLineEntity> getList();
    int getCntDicLines(String versionId, int dictType);
    Page<NerDictionaryLineEntity> getDicLinesWithPage(String versionId, int dictType, Pageable pageable);
    void deleteDicLinesWithIdx(List<NerDictionaryLineEntity> posDictionaryEntities);
    int updateIdx(NerDictionaryLineEntity entity);
    int updateIdxMinus(NerDictionaryLineEntity entity);
    void deleteDicLinesAllByVersionId(String versionId);
    List<Integer> getDicTypes(String dictionaryId);
}
