package ai.maum.mlt.dictionary.service;

import ai.maum.mlt.dictionary.entity.PosDictionaryEntity;
import ai.maum.mlt.dictionary.entity.PosDictionaryLineEntity;
import ai.maum.mlt.dictionary.repository.PosDictionaryLineRepository;
import java.util.Date;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;

@Service
public class PosDictionaryLineServiceImpl implements PosDictionaryLineService {

  enum DicType {
    NE_PRE_PATTERN, // 전처리 패턴 사전
    NE_POST_PATTERN, // 후처리 패전 사전
    NE_POST_CHANGE, // 후처리 태그 변환 사전
    NE_FILTER, // 예외 처리 사전
    NE_NEW_TAG, // 태그 추가 사전
    NE_ADD_PRE_DIC, // 전처리 추가 사전
    NE_ADD_POST_DIC, // 추처리 추가 사전
    NE_ETC_DIC // 기타 사전
  }

  @Autowired
  PosDictionaryLineRepository posDictionaryLineRepository;

  @Override
  public List<PosDictionaryLineEntity> getDicLines(String versionId) {
    return posDictionaryLineRepository.findAllByVersionId(versionId);
  }

  @Transactional
  @Override
  public void insertLines(List<PosDictionaryLineEntity> entities) {
    for (PosDictionaryLineEntity entity : entities) {
      entity.setCreatedAt(new Date());
      posDictionaryLineRepository.save(entity);
    }
    return;
  }

  @Transactional
  @Override
  public void insertLinesWithDate(List<PosDictionaryLineEntity> entities) {
    try {
      posDictionaryLineRepository.save(entities);
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
    return;
  }

  @Transactional
  @Override
  public PosDictionaryLineEntity insertLine(PosDictionaryLineEntity entity) {
    entity.setCreatedAt(new Date());
    return posDictionaryLineRepository.save(entity);
  }

  @Override
  public int getCntDicLines(String versionId, int dictType) {
    return posDictionaryLineRepository.countAllByVersionIdandAndDicType(versionId, dictType);
  }

  @Override
  public Page<PosDictionaryLineEntity> getDicLinesWithPage(String versionId, int dictType, Pageable pageable) {
    return posDictionaryLineRepository.findPageAllByVersionIdAndDictType(versionId, dictType, pageable);
  }

  @Transactional
  @Override
  public int updateIdx(PosDictionaryLineEntity entity) {
    int res = 0;
    res = posDictionaryLineRepository
        .updateLineIdxByLindIdxAndVersionIdAndWorkspaceIdAndDicType(entity.getLineIdx(),
            entity.getVersionId(), entity.getWorkspaceId(), entity.getDicType());
    return res;
  }

  @Transactional
  @Override
  public int updateIdxMinus(PosDictionaryLineEntity entity) {
    int res = 0;
    res = posDictionaryLineRepository
        .updateLineIdxMinusByLindIdxAndVersionIdAndWorkspaceIdAndDicType(entity.getLineIdx(),
            entity.getVersionId(), entity.getWorkspaceId(), entity.getDicType());
    return res;
  }

  @Transactional
  @Override
  public void deleteDicLinesWithIdx(List<PosDictionaryLineEntity> posDictionaryLineEntities) {
    for (PosDictionaryLineEntity entity : posDictionaryLineEntities) {
      posDictionaryLineRepository.deleteByIdAndVersionId(entity.getId(), entity.getVersionId());
      posDictionaryLineRepository
          .updateLineIdxMinusByLindIdxAndVersionIdAndWorkspaceIdAndDicType(entity.getLineIdx(),
              entity.getVersionId(), entity.getWorkspaceId(), entity.getDicType());
    }
    return;
  }

  @Transactional
  @Override
  public int updateLine(PosDictionaryLineEntity entity) {
    entity.setCreatedAt(new Date());
    return posDictionaryLineRepository
        .updateByTypeAndVersionIdAndId(entity.getWord(), entity.getCreatedAt(),
            entity.getPattern(), entity.getSrcPos(), entity.getDestPos(), entity.getCompoundType(), entity.getDescription(),
            entity.getDicType(), entity.getVersionId(), entity.getId());
  }

  @Transactional
  @Override
  public void deleteDicLines(List<PosDictionaryLineEntity> posDictionaryLineEntities) {
    for (PosDictionaryLineEntity entity : posDictionaryLineEntities) {
      posDictionaryLineRepository.deleteByIdAndVersionId(entity.getId(), entity.getVersionId());
    }
    return;
  }

  @Override
  public List<PosDictionaryLineEntity> getList() {
    return posDictionaryLineRepository.findAll();
  }

  @Transactional
  @Modifying
  @Override
  public void deleteDicLinesAllByVersionId(String versionId) {
    posDictionaryLineRepository.deleteAllByVersionId(versionId);
  }

  @Override
  public List<Integer> getDicTypes(String versionId) {
    List<Integer> list = posDictionaryLineRepository.getDistinctTypeByVersionId(versionId);
    return list;
  }
}
