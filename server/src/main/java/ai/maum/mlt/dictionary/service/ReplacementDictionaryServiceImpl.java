package ai.maum.mlt.dictionary.service;

import ai.maum.mlt.dictionary.entity.ReplacementDictionaryEntity;
import ai.maum.mlt.dictionary.entity.ReplacementDictionaryLineEntity;
import ai.maum.mlt.dictionary.repository.ReplacementDictionaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Service
public class ReplacementDictionaryServiceImpl implements ReplacementDictionaryService {

  @Autowired
  ReplacementDictionaryRepository replacementDictionaryRepository;

  @Override
  public List<ReplacementDictionaryEntity> getDictionaries() {
    return replacementDictionaryRepository.findAll();
  }

  @Override
  public ReplacementDictionaryEntity getDictionary(String name) {
    ReplacementDictionaryEntity res = new ReplacementDictionaryEntity();
    List<ReplacementDictionaryEntity> list = replacementDictionaryRepository.findAllByName(name);
    if (!list.isEmpty()) {
      res = list.get(0);
    } else {
      res.setId("");
    }
    return res;
  }

  @Override
  public String getDictionaryName(String id) {
    String res;
    String name = replacementDictionaryRepository.findNameById(id);
    res = name.isEmpty() ? "" : name;
    return res;
  }

  @Override
  public String getDictionaryId(String name) {
    String res = new String();
    List<String> list = replacementDictionaryRepository.findIdByName(name);
    if (!list.isEmpty()) {
      res = list.get(0);
    }
    return res;
  }

  @Override
  public ReplacementDictionaryEntity insertDictionary(ReplacementDictionaryEntity entity) {
    entity.setCreatedAt(new Date());
    return replacementDictionaryRepository.save(entity);
  }

  @Transactional
  @Override
  public int updateDictionary(ReplacementDictionaryEntity entity) {
    return replacementDictionaryRepository.updateDictionary(entity.getId(), entity.getUpdaterId(), entity.getName(),
            entity.getAppliedYn(), entity.getDescription());
  }


  @Override
  public void deleteDictionaries(List<String> idList) {
    for (String id: idList) {
      replacementDictionaryRepository.deleteById(id);
    }
    return;
  }

  @Override
  public void deleteDictionary(String id) {
    replacementDictionaryRepository.deleteById(id);
    return;
  }
}
