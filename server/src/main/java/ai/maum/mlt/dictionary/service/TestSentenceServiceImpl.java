package ai.maum.mlt.dictionary.service;

import ai.maum.mlt.dictionary.entity.TestSentenceEntity;
import ai.maum.mlt.dictionary.repository.TestSentenceRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TestSentenceServiceImpl implements TestSentenceService {

  @Autowired
  TestSentenceRepository testSentenceRepository;

  @Override
  public List<TestSentenceEntity> getSentences(String workspaceId, DictionaryType type) {
    return testSentenceRepository.findAllByTestTypeAndWorkspaceId(type.ordinal(), workspaceId);
  }

  @Override
  public List<TestSentenceEntity> getSentencesByFileId(String workspaceId, long fileId) {
    return testSentenceRepository.findAllByTestFileIdAndWorkspaceId(fileId, workspaceId);
  }

  @Override
  public void deleteSentences(String workspaceId, List<Integer> ids) {
    for (long id : ids) {
      testSentenceRepository.deleteByIdAndWorkspaceId(id, workspaceId);
    }
  }

  @Override
  public void deleteSentence(String workspaceId, long id) {
    testSentenceRepository.deleteByIdAndWorkspaceId(id, workspaceId);
  }
}
