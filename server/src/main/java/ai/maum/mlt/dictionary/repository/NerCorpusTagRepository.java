package ai.maum.mlt.dictionary.repository;

import ai.maum.mlt.dictionary.entity.NerCorpusTagEntity;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
@Transactional
public interface NerCorpusTagRepository extends JpaRepository<NerCorpusTagEntity, String> {

}
