package ai.maum.mlt.dictionary.entity;

import ai.maum.mlt.common.auth.entity.UserEntity;
import ai.maum.mlt.common.auth.entity.WorkspaceEntity;
import ai.maum.mlt.common.pagenation.PageParameters;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;

@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "MAI_TEST_RESULT_DETAIL")
public class TestResultDetailEntity extends PageParameters implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_RESULT_DT_INDEX_HISTORY_GEN")
  @SequenceGenerator(name = "TEST_RESULT_DT_INDEX_HISTORY_GEN", sequenceName = "MAI_TEST_RST_DT_IDX_HSTY_SEQ", allocationSize = 5)
  @Column(name = "ID", length = 40)
  private long id;

  @Column(name = "WORKSPACE_ID", length = 40, nullable = false)
  private String workspaceId;

  @Column(name = "DICTIONARY_ID", length = 60, nullable = false)
  private String dictionaryId;

  @Column(name = "TEST_ID", length = 50)
  private long testId;

  @Column(name = "TEST_TYPE", length = 40, nullable = false)
  private int testType;

  @Lob
  @Column(name = "TEST_RESULT", length = 40)
  private String testResult;

  @Column(name = "DESCRIPTION", length = 40)
  private String description;

  @Column(name = "TESTER_ID", length = 40)
  private String testerId;

  @CreatedDate
  @Column(name = "CREATED_AT")
  private Date createdAt;

  @ManyToOne
  @JoinColumn(name = "TESTER_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity createUserEntity;

  @ManyToOne
  @JoinColumn(name = "WORKSPACE_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private WorkspaceEntity workspaceEntity;
}
