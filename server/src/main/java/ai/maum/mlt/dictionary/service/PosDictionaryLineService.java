package ai.maum.mlt.dictionary.service;

import ai.maum.mlt.dictionary.entity.PosDictionaryLineEntity;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PosDictionaryLineService {
    List<PosDictionaryLineEntity> getDicLines(String versionId);
    void deleteDicLines(List<PosDictionaryLineEntity> posDictionaryEntities);
    int getCntDicLines(String versionId, int dictType);
    Page<PosDictionaryLineEntity> getDicLinesWithPage(String versionId, int dictType, Pageable pageable);
    void deleteDicLinesWithIdx(List<PosDictionaryLineEntity> posDictionaryEntities);
    int updateIdx(PosDictionaryLineEntity entity);
    int updateIdxMinus(PosDictionaryLineEntity entity);
    void insertLines(List<PosDictionaryLineEntity> posDictionaryEntities);
    void insertLinesWithDate(List<PosDictionaryLineEntity> entities);
    int updateLine(PosDictionaryLineEntity entity);
    PosDictionaryLineEntity insertLine(PosDictionaryLineEntity entity);
    List<PosDictionaryLineEntity> getList();
    void deleteDicLinesAllByVersionId(String versionId);
    List<Integer> getDicTypes(String dictionaryId);
}
