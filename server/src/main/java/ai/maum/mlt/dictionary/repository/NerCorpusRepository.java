package ai.maum.mlt.dictionary.repository;

import ai.maum.mlt.dictionary.entity.NerCorpusEntity;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface NerCorpusRepository extends JpaRepository<NerCorpusEntity, String> {

}
