package ai.maum.mlt.dictionary.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.common.auth.service.WorkspaceService;
import ai.maum.mlt.common.system.SystemCode;
import ai.maum.mlt.dictionary.entity.NerCorpusEntity;
import ai.maum.mlt.dictionary.entity.NerCorpusTagEntity;
import ai.maum.mlt.dictionary.service.NerCorpusService;
import ai.maum.mlt.dictionary.service.NerCorpusTagService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import maum.brain.nlp.Nlp.NamedEntity;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

class NluNe {
  private String word;
  private String tag;

  public NluNe(String word, String tag) {
    this.word = word;
    this.tag = tag;
  }

  public boolean check(String word, String tag) {
    if (this.word.equals(word) && this.tag.equals(tag)) {
      return true;
    } else return false;
  }

  public String toString() {
    return word + "(" + tag + ")";
  }
}

class NluResult {
  private String sentence;
  private String taggingSentence;
  private List<NluNe> nes;

  public NluResult() {
    nes = new ArrayList<NluNe>();
    sentence = "";
  }

  public NluResult(String text) {
    nes = new ArrayList<NluNe>();
    sentence = text;
  }

  public void setSentence(String sentence) {
    this.sentence = sentence;
  }

  public void setTaggingSentence(String taggingSentence) {
    this.taggingSentence = taggingSentence;
  }

  public void put(String word, String tag) {
    NluNe ne = new NluNe(word, tag);
    this.nes.add(ne);
  }

  public void puts(List<NamedEntity> nes) {
    if (!nes.isEmpty()) {
      for (NamedEntity n: nes) {
        NluNe ne = new NluNe(n.getText(), n.getType());
        this.nes.add(ne);
      }
    }
  }

  public String toString() {
    JSONObject jsonObj = new JSONObject();
    jsonObj.put("sentence", sentence);
    jsonObj.put("taggingSentence", taggingSentence);
    jsonObj.put("nes", nes.toString());
    return jsonObj.toString();
  }
}

@RestController
@RequestMapping("api/dictionary/ner/corpus")
public class NerCorpusController {
  static final Logger logger = LoggerFactory.getLogger(NerCorpusController.class);

  @Autowired
  private NerCorpusService nerCorpusService;

  @Autowired
  private NerCorpusTagService nerCorpusTagService;

  @Value("${brain-ta.nlp3kor.ip}")
  private String brainTaNlp3korIp;

  @Value("${brain-ta.nlp3kor.port}")
  private int brainTaNlp3korPort;

  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(
      value = "/get-corpusList",
      method = RequestMethod.POST)
  public ResponseEntity<?> getCorpusList() {
    logger.info("======= call api  POST [[api/dictionary/ner/corpus/getCorpusList]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {
      List<NerCorpusEntity> nerCorpusEntities = nerCorpusService.getList();
      result.put("corpus_list", nerCorpusEntities);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getCorpusList e : " , e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^C")
  @RequestMapping(
      value = "/insert-corpusList",
      method = RequestMethod.POST)
  public ResponseEntity<?> insertCorpusList(@RequestBody  List<NerCorpusEntity> list) {
    logger.info("======= call api  POST [[api/dictionary/ner/corpus/insertCorpusList]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {
      nerCorpusService.insertLines(list);
      result.put("list", list);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("insertCorpusList e : " , e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^C")
  @RequestMapping(
      value = "/insert-corpusItem",
      method = RequestMethod.POST)
  public ResponseEntity<?> insertCorpusList(@RequestBody  NerCorpusEntity item) {
    logger.info("======= call api  POST [[api/dictionary/ner/corpus/insertCorpusOne]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {
      nerCorpusService.insertLine(item);
      return new ResponseEntity<>(null, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("insertCorpusList e : " , e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^D")
  @RequestMapping(
      value = "/delete-corpusList",
      method = RequestMethod.POST)
  public ResponseEntity<?> deleteCorpusList() {
    logger.info("======= call api  POST [[api/dictionary/ner/corpus/deleteCorpusList]] =======");
    try {
      nerCorpusService.deleteAll();
      return new ResponseEntity<>(null, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("deleteCorpusList e : " , e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/replaceList", method = RequestMethod.POST)
  public ResponseEntity<?> replaceCorpusList() {
    logger.info("======= call api  POST [[api/dictionary/ner/corpus/replaceList]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {
      List<String> result_sentence = replaceSentences();
      result.put("replaced_sentences", result_sentence);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("replaceCorpusList e : " , e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  public List<String> replaceSentences() {
    List<String> result_sentence = new ArrayList<String>();
    try {
      List<NerCorpusEntity> nerCorpusEntities = nerCorpusService.getList();
      List<NerCorpusTagEntity> nerCorpusTagEntities = nerCorpusTagService.getList();
      Pattern pattern = Pattern.compile(SystemCode.NER_DICTIONARY_CORPUS_TAG_PATTERN);
      if(!nerCorpusEntities.isEmpty()) {
        for(NerCorpusEntity entity: nerCorpusEntities) {
          boolean b_match = false;
          for(NerCorpusTagEntity tagSet: nerCorpusTagEntities) {
            String sentence = entity.getSentence();
            Matcher matcher = pattern.matcher(sentence);
            if(matcher.find())
            {
              String group = matcher.group();
              if (group.equals("<ne>" + tagSet.getTag() + "</ne>")) {
                sentence = sentence.replaceAll(matcher.group(), tagSet.getWord());
                sentence = sentence.replaceAll("<", "&lt;");
                sentence = sentence.replaceAll(">", "&gt;");
                result_sentence.add(sentence);
                b_match = true;
              }
            }
          }
          if (!b_match) {
            String sentence = entity.getSentence();
            sentence = sentence.replaceAll("<", "&lt;");
            sentence = sentence.replaceAll(">", "&gt;");
            result_sentence.add(sentence);
          }
        }
      }
      return result_sentence;
    } catch (Exception e) {
      logger.error("replaceSentences e : " , e);
      return result_sentence;
    }
  }
}

