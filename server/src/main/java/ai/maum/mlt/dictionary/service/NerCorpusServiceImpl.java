package ai.maum.mlt.dictionary.service;

import ai.maum.mlt.dictionary.entity.NerCorpusEntity;
import ai.maum.mlt.dictionary.repository.NerCorpusRepository;
import java.util.List;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NerCorpusServiceImpl implements NerCorpusService {

  @Autowired
  NerCorpusRepository nerCorpusRepository;

  @Override
  public List<NerCorpusEntity> getList() {
    return nerCorpusRepository.findAll();
  }

  @Override
  public void insertLine(NerCorpusEntity nerCorpusEntity) {
    nerCorpusRepository.save(nerCorpusEntity);
    return;
  }

  @Override
  public void insertLines(List<NerCorpusEntity> nerCorpusEntities) {
    for (NerCorpusEntity entity : nerCorpusEntities) {
      if (entity.getSentence().isEmpty()) {
        continue;
      }
      entity.setCreatedAt(new Date());
      nerCorpusRepository.save(entity);
    }
    return;
  }

  @Override
  public void deleteAll() {
    nerCorpusRepository.deleteAll();
    return;
  }
}
