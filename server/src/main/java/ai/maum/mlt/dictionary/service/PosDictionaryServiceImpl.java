package ai.maum.mlt.dictionary.service;

import ai.maum.mlt.dictionary.entity.PosDictionaryEntity;
import ai.maum.mlt.dictionary.repository.PosDictionaryRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PosDictionaryServiceImpl implements PosDictionaryService {

  @Autowired
  PosDictionaryRepository posDictionaryRepository;

  @Override
  public List<PosDictionaryEntity> getDictionaries(String wid) {
    List<Map> tmp = new ArrayList<>();
    List<PosDictionaryEntity> res = new ArrayList<>();
    tmp = posDictionaryRepository.findDictionaryWorkSpaceId(wid);
    for (int i = 0; i< tmp.size(); i++) {
      PosDictionaryEntity pos = new PosDictionaryEntity();
      pos.setId(tmp.get(i).get("id").toString());
      pos.setWorkspaceId(tmp.get(i).get("workspaceId").toString());
      pos.setCreatedAt((Date)tmp.get(i).get("createdAt"));
      pos.setDescription(tmp.get(i).get("description") == null ? "" : tmp.get(i).get("description").toString());
      pos.setName(tmp.get(i).get("name").toString());
      pos.setCreatorId(tmp.get(i).get("creatorId").toString());
      res.add(pos);
    }
    return res;
  }

  @Override
  public List<PosDictionaryEntity> getDictionaries() {
    return posDictionaryRepository.findAll();
  }

  @Override
  public PosDictionaryEntity getDictionary(String name) {
    PosDictionaryEntity res = new PosDictionaryEntity();
    List<PosDictionaryEntity> list = posDictionaryRepository.findAllByName(name);
    if (!list.isEmpty()) {
      res = list.get(0);
    } else {
      res.setId("");
    }
    return res;
  }

  @Override
  public String getDictionaryName(String id) {
    String res = new String();
    String name = posDictionaryRepository.findNameById(id);
    res = name.isEmpty() ? "" : name;
    return res;
  }

  @Override
  public String getDictionaryId(String name) {
    String res = new String();
    List<String> list = posDictionaryRepository.findIdByName(name);
    if (!list.isEmpty()) {
      res = list.get(0);
    }
    return res;
  }

  @Override
  public PosDictionaryEntity insertDictionary(PosDictionaryEntity entity) {
    entity.setCreatedAt(new Date());
    return posDictionaryRepository.save(entity);
  }

  @Transactional
  @Override
  public int updateDictionary(PosDictionaryEntity entity) {
    return posDictionaryRepository.updateDescription(entity.getId(), entity.getDescription());
  }


  @Override
  public void deleteDictionaries(List<String> idList) {
    for (String id: idList) {
      posDictionaryRepository.deleteById(id);
    }
    return;
  }

  @Override
  public void deleteDictionary(String id) {
    posDictionaryRepository.deleteById(id);
    return;
  }
}
