package ai.maum.mlt.dictionary.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.common.auth.entity.UserEntity;
import ai.maum.mlt.common.auth.service.WorkspaceService;
import ai.maum.mlt.common.git.GitManager;
import ai.maum.mlt.common.util.FileUtils;
import ai.maum.mlt.common.version.entity.CommitHistoryEntity;
import ai.maum.mlt.common.version.service.CommitHistoryService;
import ai.maum.mlt.dictionary.entity.ReplacementDictionaryEntity;
import ai.maum.mlt.dictionary.entity.ReplacementDictionaryLineEntity;
import ai.maum.mlt.dictionary.repository.ReplacementDictionaryLineRepository;
import ai.maum.mlt.dictionary.service.ReplacementDictionaryLineService;
import ai.maum.mlt.dictionary.service.ReplacementDictionaryService;
import ai.maum.mlt.dictionary.service.TestResultService;
import ai.maum.mlt.dictionary.service.TestSentenceService;
import ai.maum.mlt.itfc.TaGrpcInterfaceManager;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.protobuf.util.JsonFormat;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.stream.Collectors;
import maum.brain.nlp.Nlp.*;
import maum.brain.nlp.Nlp3Custom;
import maum.brain.nlp.Nlp3Custom.ReplacementDictList;
import maum.brain.nlp.Nlp3Custom.ReplacementDictList.Builder;
import maum.brain.nlp.Nlp3Custom.ReplacementDictList.ReplacementDict;
import org.eclipse.jgit.lib.ObjectId;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.text.SimpleDateFormat;
import java.util.*;

class ReplacementResult {

  private String sentence;
  private List<String> morphs;

  public ReplacementResult() {
    sentence = "";
    morphs = new ArrayList<String>();
  }

  public ReplacementResult(String sent) {
    sentence = sent;
    morphs = new ArrayList<String>();
  }

  public void setSentence(String sentence) {
    this.sentence = sentence;
  }

  public boolean hasMorphList() {
    return !morphs.isEmpty();
  }

  public void put(String tag) {
    this.morphs.add(tag);
  }

  public String toString() {
    JSONObject jsonObj = new JSONObject();
    jsonObj.put("sentence", sentence);
    jsonObj.put("morphs", morphs.toString());
    return jsonObj.toString();
  }
}

@RestController
@RequestMapping("api/dictionary/replacement")
public class ReplacementDictionaryController {

  static final Logger logger = LoggerFactory.getLogger(ReplacementDictionaryController.class);
  SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss", Locale.KOREA);

  @Autowired
  private ReplacementDictionaryLineService replacementDictionaryLineService;

  @Autowired
  private ReplacementDictionaryService replacementDictionaryService;

  @Autowired
  private ReplacementDictionaryLineRepository replacementDictionaryLineRepository;

  @Autowired
  private TestSentenceService testSentenceService;

  @Autowired
  private TestResultService testResultService;

  @Autowired
  private WorkspaceService workspaceService;

  @Autowired
  private CommitHistoryService commitHistoryService;

  @Value("${ta.file.path}")
  private String taPath;

  @Value("${git.file.path}")
  private String gitPath;

  @Value("${brain-ta.nlp3kor.ip}")
  private String brainTaNlp3korIp;

  @Value("${brain-ta.nlp3kor.port}")
  private int brainTaNlp3korPort;

  @Value("${temp.rep.dict.file.path}")
  private String repDictPath;

  private String nlpAuthKey = "7c3a778e29ac522de62c4e22aa90c9";

  @Value("${temp.use.dict}")
  private String useInit;

  @Autowired
  private FileUtils fileUtils;
  // dictionary 관련.

  /*Dictionary list 조회해오기 (commit count 포함)*/
  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/get-dictionaryList", method = RequestMethod.POST)
  public ResponseEntity<?> getDictionaryList() {
    logger.info("======= call api  POST [[api/dictionary/replacement/get-dictionaryList]] =======");
    List<HashMap<String, Object>> result = new ArrayList<HashMap<String, Object>>();
    try {
      List<ReplacementDictionaryEntity> replacementdictionaryentities = new ArrayList<ReplacementDictionaryEntity>();
      replacementdictionaryentities = replacementDictionaryService.getDictionaries();
      if (!replacementdictionaryentities.isEmpty()) {
        for (ReplacementDictionaryEntity entity : replacementdictionaryentities) {
          int cnt;
          CommitHistoryEntity cs = new CommitHistoryEntity();

          cs.setWorkspaceId(entity.getWorkspaceId());
          cs.setType("REPLACEMENT");
          cs.setFileId(entity.getId());
          cnt = commitHistoryService.getCountCommit(cs);
          HashMap<String, Object> elem = new HashMap<String, Object>();
          elem.put("entity", entity);
          elem.put("cnt", cnt);
          result.add(elem);
        }
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/get-dictionaryName", method = RequestMethod.POST)
  public ResponseEntity<?> getDictionaryName(@RequestBody String id) {
    logger.info("======= call api  POST [[api/dictionary/replacement/get-dictionaryName]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {
      String name = "";
      name = replacementDictionaryService.getDictionaryName(id);
      result.put("dictionary_name", name);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /*Dictionary 의 name으로 id를 조회해오기*/
  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/get-dictionary", method = RequestMethod.POST)
  public ResponseEntity<?> getDictionary(@RequestBody String name) {
    logger.info("======= call api  POST [[api/dictionary/replacement/get-dictionary]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {
      String res = replacementDictionaryService.getDictionaryId(name);
      result.put("dictionary_id", res);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /*Dictionary INSERT 하기*/
  @UriRoleDesc(role = "dictionary^C")
  @RequestMapping(value = "/insert-dictionary", method = RequestMethod.POST)
  public ResponseEntity<?> insertDictionary(@RequestBody ReplacementDictionaryEntity entity) {
    logger
        .info("======= call api  POST [[api/dictionary/replacement/insert-dictionary]] ======= {}",
            entity);
    HashMap<String, Object> result = new HashMap<>();
    try {
      ReplacementDictionaryEntity dic = replacementDictionaryService
          .getDictionary(entity.getName());
      if (dic.getId().isEmpty()) {
        result.put("message", "INSERT_SUCCESS");
        result.put("dictionary", replacementDictionaryService.insertDictionary(entity));
      } else {
        result.put("message", "INSERT_DUPLICATED");
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^U")
  @RequestMapping(value = "/update-dictionary", method = RequestMethod.POST)
  public ResponseEntity<?> updateDictionary(@RequestBody ReplacementDictionaryEntity entity) {
    logger
        .info("======= call api  POST [[api/dictionary/replacement/update-dictionary]] ======= {}",
            entity);
    HashMap<String, Object> result = new HashMap<>();
    try {
      ReplacementDictionaryEntity dic = replacementDictionaryService
          .getDictionary(entity.getName());
      if (!dic.getId().isEmpty()) {
        replacementDictionaryService.updateDictionary(entity);
      }
      return new ResponseEntity<>(null, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^D")
  @RequestMapping(value = "/delete-dictionaries", method = RequestMethod.POST)
  public ResponseEntity<?> deleteDictionaries(
      @RequestBody List<ReplacementDictionaryEntity> entities) {
    logger
        .info("======= call api  POST [[api/dictionary/replacement/delete-dictionaries]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {
      for (ReplacementDictionaryEntity entity : entities) {
        replacementDictionaryService.deleteDictionary(entity.getId());

        CommitHistoryEntity cs = new CommitHistoryEntity();
        cs.setFileId(entity.getId());
        cs.setWorkspaceId(entity.getWorkspaceId());
        cs.setType("REPLACEMENT");
        commitHistoryService.deleteCommit(cs);
      }
      result.put("resultCode", "SUCCESS");
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /*Dictionary Type list 조회해오기*/
  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/get-dictionaryTypeList")
  public ResponseEntity<?> getDictionaryTypeList() {
    logger.info(
        "======= call api  POST [[api/dictionary/replacement/get-dictionaryTypeList]] =======");
    try {
      List<String> typeList = replacementDictionaryLineService.getDictionaryTypeList();
      return new ResponseEntity<>(typeList, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * DB 혹은 git 저장소에서 Replacement dictionary 데이터 가져오기 - db에 값이 있으면 가져오고, 없으면 최신 commit을 읽어온다.
   *
   * @param param workspaceId, id(Replacement dictionary id)
   */
  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/get-dictionaryContents", method = RequestMethod.POST)
  public ResponseEntity<?> getDictionaryContents(@RequestBody HashMap<String, String> param) {
    logger.info(
        "======= call api  POST [[api/dictionary/replacement/get-dictionaryContents]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {
      List<ReplacementDictionaryLineEntity> posDictionaryEntities = getContentsFromDbOrGit(
          param.get("id"),
          param.get("workspaceId"));
      if (posDictionaryEntities.size() != 0) {
        result.put("dictionary_contents", posDictionaryEntities);
      }
      if (useInit.replaceAll("\"","").equals("true")) {
        result.put("use_init", true);
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /*Edit 모드에서 각 Line을 삭제하기*/
  @UriRoleDesc(role = "dictionary^D")
  @RequestMapping(value = "/delete-dictionaryContents", method = RequestMethod.POST)
  public ResponseEntity<?> deleteDictionaryContents(
      @RequestBody List<ReplacementDictionaryLineEntity> list) {
    logger.info(
        "======= call api  POST [[api/dictionary/replacement/delete-dictionaryContents]] =======");
    try {
      replacementDictionaryLineService.deleteDicLines(list);
      return new ResponseEntity<>(null, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /*해당 Dictionary의 모든 Line을 일괄 삭제 (업로드 시 사용)*/
  @UriRoleDesc(role = "dictionary^D")
  @RequestMapping(value = "/delete-dictionaryContentsAll", method = RequestMethod.POST)
  public ResponseEntity<?> deleteDictionaryContentsAll(@RequestBody String versionId) {
    logger.info(
        "======= call api  POST [[api/dictionary/replacement/delete-dictionaryContentsAll]] =======");
    try {
      replacementDictionaryLineService.deleteDicLinesAllByVersionId(versionId);
      return new ResponseEntity<>(null, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /* Edit 모드로 변환 시 UI의 치환 사전 단어 list 를 읽어 DB에 일괄 INSERT*/
  @UriRoleDesc(role = "dictionary^C")
  @RequestMapping(value = "/insert-dictionaryContents", method = RequestMethod.POST)
  public ResponseEntity<?> insertDictionaryList(
      @RequestBody List<ReplacementDictionaryLineEntity> list) {
    logger.info(
        "======= call api  POST [[api/dictionary/replacement/insert-dictionaryContents]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {
      if (!list.isEmpty()) {
        replacementDictionaryLineService.insertLines(list);
        result.put("dict_list", list);
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /* empty Line 를 우선 INSERT 하기*/
  @UriRoleDesc(role = "dictionary^C")
  @RequestMapping(value = "/insert-dictionaryLine", method = RequestMethod.POST)
  public ResponseEntity<?> insertDictionaryLine(
      @RequestBody ReplacementDictionaryLineEntity entity) {
    logger.info(
        "======= call api  POST [[api/dictionary/replacement/insert-dictionaryLine]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {
      // type 체크
      if (entity.getType() == null || entity.getType().isEmpty()) {
        List<String> dicTypeList = replacementDictionaryLineService.getDictionaryTypeList();
        if (dicTypeList.size() == 0) {
          result.put("message", "NO_VALID_DICTIONARY_TYPE");
          return new ResponseEntity<>(result, HttpStatus.OK);
        } else {
          entity.setType(dicTypeList.get(0));
        }
      }

      if (entity.getCreatedAt() == null) {
        String date = formatter.format(new Date());
        entity.setCreatedAt(formatter.parse(date));
      }

      ReplacementDictionaryLineEntity ReplacementDictionaryLineEntity = replacementDictionaryLineService
          .insertLine(entity);
      result.put("dictionary_line", ReplacementDictionaryLineEntity);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /* empty Line 를 우선 INSERT 하기*/
  @UriRoleDesc(role = "dictionary^C")
  @RequestMapping(value = "/insert-dictionaryLineWithIdx", method = RequestMethod.POST)
  public ResponseEntity<?> insertDictionaryLineWithIdx(
      @RequestBody ReplacementDictionaryLineEntity entity) {
    logger.info(
        "======= call api  POST [[api/dictionary/replacement/insert-dictionaryLine]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {
      // type 체크
      if (entity.getType() == null || entity.getType().isEmpty()) {
        List<String> dicTypeList = replacementDictionaryLineService.getDictionaryTypeList();
        if (dicTypeList.size() == 0) {
          result.put("message", "NO_VALID_DICTIONARY_TYPE");
          return new ResponseEntity<>(result, HttpStatus.OK);
        } else {
          entity.setType(dicTypeList.get(0));
        }
      }

      if (entity.getCreatedAt() == null) {
        String date = formatter.format(new Date());
        entity.setCreatedAt(formatter.parse(date));
      }

      replacementDictionaryLineService.updateIdx(entity);
      ReplacementDictionaryLineEntity ReplacementDictionaryLineEntity = replacementDictionaryLineService
          .insertLine(entity);
      result.put("dictionary_line", ReplacementDictionaryLineEntity);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /*각 Line에 데이터 입력 시 DB에 즉시 UPDATE 하기*/
  @UriRoleDesc(role = "dictionary^U")
  @RequestMapping(value = "/update-dictionaryLine", method = RequestMethod.POST)
  public ResponseEntity<?> updateDictionaryLine(
      @RequestBody ReplacementDictionaryLineEntity entity) {
    logger.info(
        "======= call api  POST [[api/dictionary/replacement/update-dictionaryLine]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {
      boolean findSrcStr = false;
      ReplacementDictionaryLineEntity duplEntity = null;
      if (entity.getSrcStr() != null) {
        duplEntity = replacementDictionaryLineService.getDicLineEntity(entity.getSrcStr());
        if (duplEntity != null && !duplEntity.getId().equals(entity.getId())
            && duplEntity.getSrcStr().equals(entity.getSrcStr())) {
          findSrcStr = true;
        }
      }

      if (findSrcStr) {
        result.put("success", "duplicated");
      } else {
        int cnt = replacementDictionaryLineService.updateLine(entity);
        if (cnt > 0) {
          result.put("success", true);
        } else {
          result.put("success", false);
        }
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  // 특정 commit의 file 내용 가져오기 (for Download File)
  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/getFileByCommit", method = RequestMethod.POST)
  public ResponseEntity<?> getFileByCommit(@RequestBody HashMap<String, String> param) {
    logger.info("======= call api  POST [[api/dictionary/replacement/getFileByCommit]] =======",
        param);
    logger.debug("start : {}", param);

    try {
      GitManager gs = new GitManager();
      gs.setWorkingDirectory(gitPath + "/replacement");

      String result = gs.readFileFromCommit(param.get("workspaceId"), param.get("id"),
          ObjectId.fromString(param.get("source")));

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      return new ResponseEntity<>(HttpStatus.FAILED_DEPENDENCY);
    }
  }

  /*git에서 git Difference 읽어오기*/
  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/getDiff", method = RequestMethod.POST)
  public ResponseEntity<?> getDiff(@RequestBody HashMap<String, String> param) {
    logger.info("======= call api  POST [[api/dictionary/replacement/getDiff]] =======", param);
    logger.debug("start : {}", param);
    try {
      GitManager gs = new GitManager();
      gs.setWorkingDirectory(gitPath + "/replacement");

      String result = gs.diffFile(param.get("workspaceId"), param.get("id"), param.get("source"),
          param.get("target"));
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /*git에서 Commit List 가져오기*/
  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/getCommitList", method = RequestMethod.POST)
  public ResponseEntity<?> getCommitList(@RequestBody ReplacementDictionaryEntity param) {
    logger
        .info("======= call api  POST [[api/dictionary/replacement/getCommitList]] =======", param);
    logger.debug("start : {}", param);

    try {
      GitManager gs = new GitManager();
      gs.setWorkingDirectory(gitPath + "/replacement");

      List<HashMap<String, Object>> result = gs
          .getCommitList(param.getWorkspaceId(), param.getId());
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

//  @UriRoleDesc(role = "dictionary^R")
//  @RequestMapping(value = "/get-dictionary-from-git", method = RequestMethod.POST)
//  public ResponseEntity<?> getDictionaryByCommit(@RequestBody ReplacementDictionaryEntity param) {
//    logger.info("======= call api  POST [[api/dictionary/replacement/get-dictionary-from-git]] =======",
//        param);
//    logger.debug("start : {}", param);
//    HashMap<String, Object> result = new HashMap<>();
//    try {
//      List<ReplacementDictionaryLineEntity> replacementDictionaryEntities = getReplacementContentsFromGit(
//          param.getWorkspaceId(), param.getId());
//      if (replacementDictionaryEntities.size() > 0) {
//        result.put("dictionary_contents", replacementDictionaryEntities);
//      }
//      return new ResponseEntity<>(result, HttpStatus.OK);
//    } catch (Exception e) {
//      logger.error("{} => ", e.getMessage(), e);
//      return new ResponseEntity<>(HttpStatus.FAILED_DEPENDENCY);
//    }
//  }

  /*Dictionary Contents를 돌려주기 (for Download File)*/
  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/download-dictionary", method = RequestMethod.POST)
  public ResponseEntity<?> downloadDictionary(
      @RequestBody List<ReplacementDictionaryLineEntity> param) {
    logger.info("======= call api  POST [[api/dictionary/replacement/download-dictionary]] =======",
        param);
    logger.debug("start : {}", param);
    HashMap<String, Object> result = new HashMap<>();
    try {
      // todo : proto 형식 -> db 형식으로 변경
      result.put("dictionary_contents",
          JsonFormat.printer().includingDefaultValueFields()
              .print(entityToProto(param, false, false)));
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      return new ResponseEntity<>(HttpStatus.FAILED_DEPENDENCY);
    }
  }

  /*특정 Dictionary의 Contents를 일괄 INSERT*/
  @UriRoleDesc(role = "dictionary^C")
  @RequestMapping(value = "/upload-dictionary", method = RequestMethod.POST)
  public ResponseEntity<?> uploadDictionary(@RequestBody ArrayList<String> param) {
    logger.info("======= call api  POST [[api/dictionary/replacement/upload-dictionary]] =======",
        param);
    // [data, dictionaryId]
    logger.debug("start : {}", param);
    HashMap<String, Object> result = new HashMap<>();
    try {
      List<ReplacementDictionaryLineEntity> list = jsonStringToEntity(param.get(0));
      if (list.isEmpty()) {
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
      }
      for (ReplacementDictionaryLineEntity elem : list) {
        // 각 line의 dictionary ID를 채워줌
        elem.setVersionId(param.get(1));
        elem.setCreatorId(param.get(3));
        elem.setCreatedAt(new Date());
      }
      replacementDictionaryLineService.insertLines(list);
      List<ReplacementDictionaryLineEntity> result_list = replacementDictionaryLineService
          .getDicLines(param.get(1));
      result.put("dictionary_contents", result_list);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }


  /* Git Commit 하기
   - DB 에서 해당 Dictionary의 Line data를 읽어와 git file에 update
   - 읽어온 DB data는 삭제 */
  @Transactional
  @Modifying
  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/commit-dictionary", method = RequestMethod.POST)
  public ResponseEntity<?> commitDictionary(@RequestBody ReplacementDictionaryEntity dic) {
    logger.info("======= call api  POST [[api/dictionary/replacement/commit-dictionary]] =======",
        dic);
    logger.debug("start : {}", dic);
    HashMap<String, Object> result = new HashMap<>();
    try {
      GitManager gs = new GitManager();
      gs.setWorkingDirectory(gitPath + "/replacement");

      String content = "";
      List<ReplacementDictionaryLineEntity> entities = replacementDictionaryLineService
          .getDicLines(dic.getId());
      ObjectMapper om = new ObjectMapper();

      List<HashMap<String, Object>> contentEntities = new ArrayList<>();

      // json에 포함시키고싶은 데이터들만 copy
      for (ReplacementDictionaryLineEntity line : entities) {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("type", line.getType());
        hashMap.put("createdAt", formatter.format(line.getCreatedAt()));
        hashMap.put("creatorId", line.getCreatorId());
        hashMap.put("useYn", line.getUseYn());
        hashMap.put("versionId", line.getVersionId());
        hashMap.put("description", line.getDescription());
        hashMap.put("srcStr", line.getSrcStr());
        hashMap.put("destStr", line.getDestStr());
        hashMap.put("createUserEntity", line.getCreateUserEntity());
        contentEntities.add(hashMap);
      }

      // Map or List Object 를 JSON 문자열로 변환
      content = om.writerWithDefaultPrettyPrinter().writeValueAsString(contentEntities);
//      content = JsonFormat.printer().includingDefaultValueFields()
//              .print(entityToProto(entities, false, false));

      HashMap<String, String> value = new HashMap<>();
      value.put("gitPath", gitPath + "/replacement");
      value.put("workspaceId", dic.getWorkspaceId());
      value.put("name", dic.getCreatorId());
      value.put("email", "mlt@ai");
      value.put("fileName", dic.getId());
      value.put("content", content);
      value.put("messsage", "");
      gs.updateFile(value);

      // 커밋히스토리에 적재
      CommitHistoryEntity ch = new CommitHistoryEntity();
      ch.setFileId(dic.getId());
      ch.setWorkspaceId(dic.getWorkspaceId());
      ch.setType("REPLACEMENT");
      commitHistoryService.insertCommit(ch);

      result.put("entities", entities);

      // commit 후 사전데이터 삭제
//      replacementDictionaryLineService.deleteDicLinesAllByVersionId(dic.getId());
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  public List<ReplacementDictionaryLineEntity> jsonStringToEntity(String content) {

    ReplacementDictList.Builder result = ReplacementDictList.newBuilder();
    List<ReplacementDictionaryLineEntity> entities = new ArrayList<>();

    if (content.isEmpty()) {
      return entities;
    }
    try {
      if (content.contains("entries")) {
        // 기존 json -> proto로 변환하던 코드
        JsonFormat.parser().merge(content, result);

        List<ReplacementDict> list = result.getEntriesList();
        for (ReplacementDict dict : list) {
          ReplacementDictionaryLineEntity entity = new ReplacementDictionaryLineEntity();
          entity.setSrcStr(dict.getSrcStr());
          entity.setDestStr(dict.getDestStr());
          entity.setUseYn(dict.getUsed());
          entity.setType(dict.getType());
          entities.add(entity);
        }
      } else {
        // json -> entity로 변환
        ObjectMapper mapper = new ObjectMapper();
        List<HashMap<String, Object>> dataObject = new ArrayList<>();
        dataObject = mapper
            .readValue(content, new TypeReference<ArrayList<HashMap<String, Object>>>() {
            });

        for (HashMap<String, Object> line : dataObject) {
          ReplacementDictionaryLineEntity entity = new ReplacementDictionaryLineEntity();
          UserEntity userEntity = new UserEntity();

          if (line.containsKey("createUserEntity")) {
            LinkedHashMap<String, Object> userObject = (LinkedHashMap<String, Object>) line
                .get("createUserEntity");
            userEntity.setId((String) userObject.get("id"));
            userEntity.setUsername((String) userObject.get("username"));
            userEntity.setPassword((String) userObject.get("password"));
            userEntity.setEmail((String) userObject.get("email"));
            userEntity.setActivated((String) userObject.get("activated"));
            userEntity.setLastUpdated(new Date((Long) userObject.get("lastUpdated")));

            entity.setCreateUserEntity(userEntity);
          }

          entity.setDestStr((String) line.get("destStr"));
          entity.setSrcStr((String) line.get("srcStr"));
          entity.setDescription((String) line.get("description"));
          entity.setVersionId((String) line.get("versionId"));
          entity.setUseYn((String) line.get("useYn"));
          entity.setCreatorId((String) line.get("creatorId"));
          entity.setCreatedAt(formatter.parse((String) line.get("createdAt")));
          entity.setType((String) line.get("type"));
          entities.add(entity);
        }
      }

      return entities;
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      return entities;
    }
  }


  public ReplacementDictList entityToProto(List<ReplacementDictionaryLineEntity> entities,
      boolean isClear, boolean useAuthKey) {
    Builder list = ReplacementDictList.newBuilder();

    if (useAuthKey) {
      list.setAuthKey(nlpAuthKey);
    }

    try {

      if (isClear) { // 사전 초기화용 데이터
        // todo
      } else { // 사전 적용 데이터
        for (ReplacementDictionaryLineEntity entity : entities) {
          ReplacementDict.Builder replacementDict = ReplacementDict.newBuilder();
          replacementDict.setSrcStr(entity.getSrcStr());
          replacementDict.setDestStr(entity.getDestStr());
          replacementDict.setUsed(entity.getUseYn());
          replacementDict.setType(entity.getType());
          list.addEntries(replacementDict.build());
        }
      }
      return list.build();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      return list.build();
    }
  }

//  // 보류
//  @UriRoleDesc(role = "dictionary^D")
//  @RequestMapping(value = "/delete-dictionary", method = RequestMethod.POST)
//  public ResponseEntity<?> deleteDictionary(@RequestBody String id) {
//    logger.info("======= call api  POST [[api/dictionary/replacement/delete-dictionary]] =======");
//    try {
//      replacementDictionaryService.deleteDictionary(id);
//      return new ResponseEntity<>(null, HttpStatus.OK);
//    } catch (Exception e) {
//      logger.error("{} => ", e.getMessage(), e);
//      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
//    }
//  }
//

  /**
   * 특정 replacement 사전 적용 기능
   */
  @UriRoleDesc(role = "dictionary^X")
  @RequestMapping(value = "/apply-dictionary/{workspaceId}/{dictionaryId}", method = RequestMethod.GET)
  public ResponseEntity<?> applyReplacementDictionary(
      @PathVariable("dictionaryId") String dictionaryId,
      @PathVariable("workspaceId") String workspaceId) {
    logger.info(
        "======= call api  POST [[api/dictionary/replacement/apply-dictionary/{workspaceId}/{dictionaryId}]]"
            +
            " ======= \n workspaceId: {}, dictionaryId: {}", workspaceId, dictionaryId);
    HashMap<String, Object> result = new HashMap<>();
    try {
      // nlp apply
      applyDictionary(workspaceId, dictionaryId, false);

      // db column 'applied_yn' update
      List<ReplacementDictionaryEntity> dictionaryList = replacementDictionaryService
          .getDictionaries();
      for (ReplacementDictionaryEntity dictionary : dictionaryList) {
        if (dictionary.getId().equals(dictionaryId)) {
          dictionary.setAppliedYn("Y");
        } else {
          dictionary.setAppliedYn("N");
        }
        replacementDictionaryService.updateDictionary(dictionary);
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  private ReplacementDictList applyDictionary(String workspaceId, String dictionaryId,
      boolean isClear) throws Exception {
    try {
      TaGrpcInterfaceManager client = new TaGrpcInterfaceManager(brainTaNlp3korIp,
          brainTaNlp3korPort);
      // dictionaryId 와 일치하는 치환 사전 데이터 가져오기
      List<ReplacementDictionaryLineEntity> replacementDictionaryLineEntities = getContentsFromDbOrGit(
          dictionaryId,
          workspaceId);

      ReplacementDictList list = entityToProto(replacementDictionaryLineEntities, isClear, true);
      client.updateReplacementDict(list);
      return list;
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      throw e;
    }
  }


  /**
   * 특정 replacement 사전 적용과 테스트 기능
   *
   * @param param checkedList 적용할 사전 종류 리스트, fileList Test 할 File 리스트
   */
  @UriRoleDesc(role = "dictionary^X")
  @RequestMapping(value = "/apply-and-test/{workspaceId}/{dictionaryId}/{testerId}", method = RequestMethod.POST)
  public ResponseEntity<?> testSentenceList(@PathVariable("dictionaryId") String dictionaryId,
      @PathVariable("workspaceId") String workspaceId, @PathVariable("testerId") String testerId,
      @RequestBody HashMap<String, Object> param) {
    logger.info("======= call api  POST [[api/dictionary/replacement/apply-and-test]] =======");
    HashMap<String, Object> result = new HashMap<>();
    List<String> resultList = new ArrayList<>();
    try {
//      List<Integer> checkedList = (List<Integer>) param.get("checkedList");
      applyDictionary(workspaceId, dictionaryId, false);
//      List<Integer> fileList = (List<Integer>) param.get("fileList");
//
//      if (fileList.size() > 0) { // 문장 test
//        // 사전 적용
//        applyDictionary(workspaceId, dictionaryId, checkedList, false);
//
//        // 선택한 파일의 문장 가져오기
//        List<String> sentenceList = new ArrayList<String>();
//        for (int i = 0; i < fileList.size(); i++) {
//          long fileId = fileList.get(i).longValue();
//          List<TestSentenceEntity> sentences = testSentenceService
//              .getSentencesByFileId(workspaceId, fileId);
//          for (TestSentenceEntity entity : sentences) {
//            sentenceList.add(entity.getSentence());
//          }
//        }
//
//        // 사전적용 후 Test 내용 저장
//        TestResultEntity testResultEntity = new TestResultEntity();
//        testResultEntity.setApplyCheck(checkedList.toString());
//        testResultEntity.setFileId(fileList.toString());
//        testResultEntity.setCreatedAt(new Date());
//        testResultEntity.setDictionaryId(dictionaryId);
//        testResultEntity.setTesterId(testerId);
//        testResultEntity.setWorkspaceId(workspaceId);
//        testResultEntity.setTestType(DictionaryType.MORPH.ordinal());
//
//        // 테스트
//        resultList = testDictionary(workspaceId, dictionaryId, sentenceList, testerId, testResultEntity);
//        result.put("result", resultList);
//      } else { // 문장 테스트 안함
//        // 사전 초기화
//        applyDictionary(workspaceId, dictionaryId, checkedList, true);
//      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/get-dictionaryContents-type/{workspaceId}/{dictionaryId}", method = RequestMethod.POST)
  public ResponseEntity<?> getDictionaryContentsTypeList(
      @PathVariable("dictionaryId") String dictionaryId,
      @PathVariable("workspaceId") String workspaceId) {
    logger.info(
        "======= call api  POST [[api/dictionary/replacement/get-dictionaryContents-type]] =======");
    HashMap<String, Object> result = new HashMap<>();
    List<Integer> types;
    try {
      List<ReplacementDictionaryLineEntity> replacementDictionaryEntities = replacementDictionaryLineService
          .getDicLines(dictionaryId);
      if (replacementDictionaryEntities.isEmpty()) { // DB에 값이 없을 경우 Commit에서 가져오기
        replacementDictionaryEntities = getReplacementContentsFromGit(workspaceId, dictionaryId);
        for (ReplacementDictionaryLineEntity elem : replacementDictionaryEntities) {
          elem.setVersionId(dictionaryId);
        }
        replacementDictionaryLineService.insertLines(replacementDictionaryEntities);
      }
      types = replacementDictionaryLineService.getDicTypes(dictionaryId);
      result.put("list", types);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^D")
  @RequestMapping(value = "/delete-dictionaryContentsWithIdx", method = RequestMethod.POST)
  public ResponseEntity<?> deleteDictionaryContentsWithIdx(
      @RequestBody List<ReplacementDictionaryLineEntity> list) {
    logger
        .info("======= call api  POST [[api/dictionary/replacement/delete-dictionaryContentsWithIdx]] =======");
    try {
      replacementDictionaryLineService.deleteDicLinesWithIdx(list);
      return new ResponseEntity<>(null, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("delete-dictionaryContentsWithIdx e : ", e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }


//  public List<String> testDictionary(String workspaceId, String dictionaryId,
//      List<String> sentenceList, String testerId, TestResultEntity testResultEntity) {
//    List<String> resultList = new ArrayList<String>();
//    try {
//      long start = System.currentTimeMillis();
//      TaGrpcInterfaceManager client = new TaGrpcInterfaceManager(brainTaNlp3korIp,
//          brainTaNlp3korPort);
//
//      // pos 사전 컨텐츠 가져오기
//      List<ReplacementDictionaryLineEntity> replacementDictionaryEntities = getContentsFromDbOrGit(dictionaryId,
//          workspaceId);
//
//      WorkspaceEntity workspaceEntity = workspaceService.getWorkspace(workspaceId);
//
//      // 문장 별 테스트
//      for (String line : sentenceList) {
//        line = line.trim();
//        InputText.Builder inputText = InputText.newBuilder();
//        // Lang은 일단 항상 한국어로만 사용하도록 코딩
//        // inputText.setLang(LangCode.valueOf(workspaceEntity.getLangCode()));
//        inputText.setLang(LangCode.kor);
//        inputText.setSplitSentence(true);
//        inputText.setText(line);
//        Document document = client.analyzeWithSpace(inputText.build());
//        if (!document.getSentencesList().isEmpty()) {
//          for (Sentence sentence : document.getSentencesList()) {
//            ReplacementResult replacementResult = new ReplacementResult(sentence.getText());
//            String taggedStr = "";
//            String noramlStr = "";
//            for (MorphemeEval morph : sentence.getMorphEvalsList()) {
//              if (noramlStr != "") {
//                noramlStr += ' ';
//                taggedStr += '+';
//              }
//              noramlStr += morph.getResult();
//              taggedStr += morph.getResult();
//            }
//            for (ReplacementDictionaryLineEntity entity : replacementDictionaryEntities) {
//
//              if (pos.getDicType() == Nlp3Custom.MorpDictList.MorpDictType.MORP_CUSTOM_VALUE) {
//                if (pos.getDescription() == null || pos.getDescription() == "") {
//                  continue;
//                }
//                if (taggedStr.contains(pos.getDescription())) {
//                  //매칭되는 모든 결과를 보여주기위해 주석처리.
//                  //noramlStr = noramlStr.replaceAll(pos.getDescription(), "");
//                  //taggedStr = taggedStr.replaceAll(pos.getDescription(), "");
//                  replacementResult.put(pos.getDescription());
//                }
//              } else {
//                if (noramlStr.contains(pos.getDestPos())) {
//                  //매칭되는 모든 결과를 보여주기위해 주석처리.
//                  //taggedStr = taggedStr.replaceAll(pos.getDestPos(), "");
//                  //noramlStr = noramlStr.replaceAll(pos.getDestPos().replaceAll("\\+", " "), "");
//                  replacementResult.put(pos.getDestPos());
//                }
//              }
//              if (taggedStr == "" || taggedStr == " ") {
//                continue;
//              }
//            }
//            resultList.add(replacementResult.toString());
//          }
//        }
//      }
//
//      // 테스트 결과 저장
//      long testId = testResultService.insertTestResult(testResultEntity);
//      // 테스트 세부 결과 저장
//      if (testId > -1  && testerId != null) {
//        TestResultDetailEntity entity = new TestResultDetailEntity();
//        entity.setCreatedAt(new Date());
//        entity.setTestId(testId);
//        entity.setDictionaryId(dictionaryId);
//        entity.setTesterId(testerId);
//        entity.setWorkspaceId(workspaceId);
//        entity.setTestResult(resultList.toString());
//        testResultService.insertTestResultDetail(entity);
//      }
//      logger.trace("morpheme dictionary test, sentence:[" + resultList.size() + "]/sentence:["
//          + posDictionaryEntities.size() + "]/ time:[" + (System.currentTimeMillis() - start)
//          + "]");
//      return resultList;
//    } catch (Exception e) {
//      logger.error("{} => ", e.getMessage(), e);
//      throw new RuntimeException();
//    }
//  }

  public List<ReplacementDictionaryLineEntity> getReplacementContentsFromGit(String wid,
      String id) {
    try {
      GitManager gs = new GitManager();
      gs.setWorkingDirectory(gitPath + "/replacement");
      List<HashMap<String, Object>> list = gs.getCommitList(wid, id);

      Date last_time = null;
      HashMap<String, Object> last_item = new HashMap<String, Object>();
      boolean first = true;
      for (HashMap<String, Object> item : list) {
        if (first) {
          last_time = (Date) item.get("commitTime");
          last_item = item;
          first = false;
        }
        if (last_time.compareTo((Date) item.get("commitTime")) < 0) {
          last_time = (Date) item.get("commitTime");
          last_item = item;
        }
      }
      String contents = null;
      if (last_item.size() > 0) {
        contents = gs
            .readFileFromCommit(wid, id, ObjectId.fromString(last_item.get("hashKey").toString()));
      }

      return jsonStringToEntity(contents);
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      return new ArrayList<ReplacementDictionaryLineEntity>();
    }
  }

  private List<ReplacementDictionaryLineEntity> getContentsFromDbOrGit(String id,
      String workspace) {
    List<ReplacementDictionaryLineEntity> replacementDictionaryEntities = new ArrayList<ReplacementDictionaryLineEntity>();
    try {
      replacementDictionaryEntities = replacementDictionaryLineService.getDicLines(id);
      if (replacementDictionaryEntities.isEmpty()) { // DB에 값이 없을 경우 Commit에서 가져오기
        replacementDictionaryEntities = getReplacementContentsFromGit(workspace, id);
      }
      return replacementDictionaryEntities;
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      return replacementDictionaryEntities;
    }
  }

  private Page<ReplacementDictionaryLineEntity> getContentsFromDbOrGitWithPage(ReplacementDictionaryLineEntity entity, String wid) {
    List<ReplacementDictionaryLineEntity> tempEntities = new ArrayList<>();
    try {
      Page<ReplacementDictionaryLineEntity> posDictionaryEntities = replacementDictionaryLineService
      .getDicLinesWithPage(entity.getVersionId(), entity.getPageRequest());

      if (posDictionaryEntities.getNumberOfElements() < 1) { // DB에 값이 없을 경우 Commit에서 가져오기
        tempEntities = getReplacementContentsFromGit(wid, entity.getId());
        List<ReplacementDictionaryLineEntity> typeEntities = tempEntities.stream().collect(
            Collectors.toList());
        posDictionaryEntities = new PageImpl<>(typeEntities, entity.getPageRequest(), typeEntities.size());
      }
      return posDictionaryEntities;
    } catch (Exception e) {
      logger.error("getContentsFromDbOrGitWithPage e : ", e.getMessage());
      tempEntities = new ArrayList<>();
      Page<ReplacementDictionaryLineEntity> res = new PageImpl<ReplacementDictionaryLineEntity>(
          tempEntities, entity.getPageRequest(), tempEntities.size());
      return res;
    }
  }


  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/get-dictionaryContentsWithPage", method = RequestMethod.POST)
  public ResponseEntity<?> getDictionaryContentsWithPage(@RequestBody HashMap<String, String> param) {
    logger.info("======= call api  POST [[api/dictionary/morph/get-dictionaryContents]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {
      String id = param.get("id").toString();
      String workspaceId = param.get("workspaceId");
      int pageIndex = Integer.valueOf(param.get("pageIndex"));
      int pageSize = Integer.valueOf(param.get("pageSize"));
      ReplacementDictionaryLineEntity entity = new ReplacementDictionaryLineEntity();
      entity.setPageIndex(pageIndex);
      entity.setPageSize(pageSize);
      entity.setVersionId(id);

      List<ReplacementDictionaryLineEntity> repDictionaryEntities = getContentsFromDbOrGit(id, workspaceId);
      if (repDictionaryEntities.size() != 0) {
        result.put("dictionary_contents", repDictionaryEntities);
      }
      if (useInit.replaceAll("\"","").equals("true")) {
        result.put("use_init", true);
      }
      int cnt = 0;
      cnt = replacementDictionaryLineService.getCntDicLines(param.get("id"));
      Page<ReplacementDictionaryLineEntity> repDictionaryLineEntities = getContentsFromDbOrGitWithPage(entity, param.get("workspaceId"));
      if (repDictionaryLineEntities.getNumberOfElements() > 0) {
        result.put("dictionary_contents", repDictionaryLineEntities);
        if (cnt == 0) {
          cnt = repDictionaryLineEntities.getNumberOfElements();
        }
        result.put("dictionary_contents_size", cnt);
      }
      if (useInit.replaceAll("\"","").equals("true")) {
        result.put("use_init", true);
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("dictionaryContentsWithPage e : ", e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }


  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/get-nlp-resource", method = RequestMethod.POST)
  public ResponseEntity<?> getNlpResource(@RequestBody ArrayList<String> param) {
    logger.info("======= call api  POST [[api/dictionary/morph/get-nlp-resource]] =======");
    HashMap<String, List<ReplacementDictionaryLineEntity>> result = new HashMap<>();
    List<ReplacementDictionaryLineEntity> replacementDictionaryLineEntities = new ArrayList<>();
    try {
      //  파일읽어오기
      String dir = fileUtils.getSystemConfPath(repDictPath);
      String rep_dic = "space_dic.txt";
      String id = param.get(0);
      String workspaceId = param.get(1);
      String userId = param.get(2);
      if (dir != "") {
        File file = new File(dir + rep_dic);
        if (file.exists()) {
          BufferedReader inFile = new BufferedReader(new FileReader(file));
          String line = null;
          long idx = 1;
          while ((line = inFile.readLine()) != null) {
            if (line.contains("\t")) {
              ReplacementDictionaryLineEntity entity = new ReplacementDictionaryLineEntity();
              String[] data = line.split("\t");
              entity.setSrcStr(data[0]);
              entity.setDestStr(data[1]);
              entity.setCreatedAt(new Date());
              entity.setCreatorId(userId);
              entity.setVersionId(id);
              if (data.length > 2) {
                entity.setUseYn(data[2]); //사용여부 Y,N
                entity.setType(data[3]); //사전종류(NE, POS)
              }
              entity.setLineIdx(idx);
              replacementDictionaryLineEntities.add(entity);
              idx++;
            }
          }
        }
      }
      if (!replacementDictionaryLineEntities.isEmpty()) {
        replacementDictionaryLineService.deleteDicLinesAllByVersionId(id);
        result.put("dictionary_contents", replacementDictionaryLineEntities);
        replacementDictionaryLineService.insertLinesWithDate(replacementDictionaryLineEntities);

      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("get Nlp replacement dictionary data e : " , e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
