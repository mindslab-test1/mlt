package ai.maum.mlt.dictionary.service;

import ai.maum.mlt.dictionary.entity.NerCorpusEntity;
import java.util.List;

public interface NerCorpusService {
    List<NerCorpusEntity> getList ();
    void insertLine(NerCorpusEntity nerCorpusEntity);
    void insertLines(List<NerCorpusEntity> nerCorpusEntities);
    void deleteAll();
}
