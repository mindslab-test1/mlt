package ai.maum.mlt.dictionary.service;

import ai.maum.mlt.dictionary.entity.NerDictionaryLineEntity;
import ai.maum.mlt.dictionary.repository.NerDictionaryLineRepository;
import java.util.Date;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class NerDictionaryLineServiceImpl implements NerDictionaryLineService {

  enum DicType {
    NE_PRE_PATTERN, // 전처리 패턴 사전
    NE_POST_PATTERN, // 후처리 패전 사전
    NE_POST_CHANGE, // 후처리 태그 변환 사전
    NE_FILTER, // 예외 처리 사전
    NE_NEW_TAG, // 태그 추가 사전
    NE_ADD_PRE_DIC, // 전처리 추가 사전
    NE_ADD_POST_DIC, // 추처리 추가 사전
    NE_ETC_DIC // 기타 사전
  }

  @Autowired
  NerDictionaryLineRepository nerDictionaryLineRepository;

  @Override
  public List<NerDictionaryLineEntity> getDicLines(String versionId) {
    return nerDictionaryLineRepository.findAllByVersionId(versionId);
  }

  @Override
  public void insertLines(List<NerDictionaryLineEntity> nerDictionaryEntities) {
    for (NerDictionaryLineEntity entity : nerDictionaryEntities) {
      entity.setCreatedAt(new Date());
      nerDictionaryLineRepository.save(entity);
    }
    return;
  }

  @Override
  public void insertLinesWithDate(List<NerDictionaryLineEntity> nerDictionaryEntities) {
    nerDictionaryLineRepository.save(nerDictionaryEntities);
    return;
  }

  @Override
  public NerDictionaryLineEntity insertLine(NerDictionaryLineEntity entity) {
    entity.setCreatedAt(new Date());
    return nerDictionaryLineRepository.save(entity);
  }

  @Transactional
  @Override
  public int updateLine(NerDictionaryLineEntity entity) {
    entity.setCreatedAt(new Date());
    return nerDictionaryLineRepository
        .updateByTypeAndVersionIdAndId(entity.getChangeTag(), entity.getCreatedAt(),
            entity.getOriginTag(), entity.getPattern(), entity.getTag(), entity.getWord(),
            entity.getDicType(), entity.getVersionId(), entity.getId());
  }

  @Transactional
  @Override
  public void deleteDicLines(List<NerDictionaryLineEntity> nerDictionaryEntities) {
    for (NerDictionaryLineEntity entity : nerDictionaryEntities) {
      nerDictionaryLineRepository.deleteByIdAndVersionId(entity.getId(), entity.getVersionId());
    }
    return;
  }

  @Override
  public int getCntDicLines(String versionId, int dictType) {
    return nerDictionaryLineRepository.countAllByVersionIdandAndDicType(versionId, dictType);
  }

  @Override
  public Page<NerDictionaryLineEntity> getDicLinesWithPage(String versionId, int dictType, Pageable pageable) {
    return nerDictionaryLineRepository.findPageAllByVersionIdAndDictType(versionId, dictType, pageable);
  }

  @Transactional
  @Override
  public void deleteDicLinesWithIdx(List<NerDictionaryLineEntity> nerDictionaryLineEntities) {
    for (NerDictionaryLineEntity entity : nerDictionaryLineEntities) {
      nerDictionaryLineRepository.deleteByIdAndVersionId(entity.getId(), entity.getVersionId());
      nerDictionaryLineRepository
          .updateLineIdxMinusByLindIdxAndVersionIdAndWorkspaceIdAndDicType(entity.getLineIdx(),
              entity.getVersionId(), entity.getWorkspaceId(), entity.getDicType());
    }
    return;
  }

  @Transactional
  @Override
  public int updateIdx(NerDictionaryLineEntity entity) {
    int res = 0;
    res = nerDictionaryLineRepository
        .updateLineIdxByLindIdxAndVersionIdAndWorkspaceIdAndDicType(entity.getLineIdx(),
            entity.getVersionId(), entity.getWorkspaceId(), entity.getDicType());
    return res;
  }

  @Transactional
  @Override
  public int updateIdxMinus(NerDictionaryLineEntity entity) {
    int res = 0;
    res = nerDictionaryLineRepository
        .updateLineIdxMinusByLindIdxAndVersionIdAndWorkspaceIdAndDicType(entity.getLineIdx(),
            entity.getVersionId(), entity.getWorkspaceId(), entity.getDicType());
    return res;
  }

  @Override
  public List<NerDictionaryLineEntity> getList() {
    return nerDictionaryLineRepository.findAll();
  }

  @Override
  public void deleteDicLinesAllByVersionId(String versionId) {
    nerDictionaryLineRepository.deleteAllByVersionId(versionId);
    return;
  }

  @Override
  public List<Integer> getDicTypes(String versionId) {
    List<Integer> list = nerDictionaryLineRepository.getDistinctTypeByVersionId(versionId);
    return list;
  }
}
