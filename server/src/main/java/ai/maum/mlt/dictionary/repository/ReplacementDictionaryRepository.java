package ai.maum.mlt.dictionary.repository;

import ai.maum.mlt.dictionary.entity.ReplacementDictionaryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface ReplacementDictionaryRepository extends JpaRepository<ReplacementDictionaryEntity, String> {

  @Query(value = "SELECT d "
      + "         FROM ReplacementDictionaryEntity d"
      + "         WHERE d.name= :name "
      + "         ORDER BY d.createdAt DESC")
  List<ReplacementDictionaryEntity> findAllByName(@Param("name") String name);

  @Query(value = "SELECT d.id "
      + "         FROM ReplacementDictionaryEntity d"
      + "         WHERE d.name= :name "
      + "         ORDER BY d.createdAt DESC")
  List<String> findIdByName(@Param("name") String name);

  @Query(value = "SELECT d.name "
      + "         FROM ReplacementDictionaryEntity d"
      + "         WHERE d.id= :id "
      + "         ORDER BY d.createdAt DESC")
  String findNameById(@Param("id") String id);

  @Modifying
  @Transactional
  @Query(value = "UPDATE ReplacementDictionaryEntity d "
      + " SET "
      + " d.name = :name,"
      + " d.updaterId = :updaterId,"
      + " d.appliedYn = :appliedYn,"
      + " d.description = :description"
      + " WHERE d.id = :id ")
  int updateDictionary(@Param("id") String id,
                        @Param("updaterId") String updaterId,
                        @Param("name") String name,
                        @Param("appliedYn") String appliedYn,
                        @Param("description") String description);

  void deleteById(String Id);
}
