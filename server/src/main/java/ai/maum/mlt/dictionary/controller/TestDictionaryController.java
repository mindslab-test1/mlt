package ai.maum.mlt.dictionary.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.common.file.entity.FileEntity;
import ai.maum.mlt.common.file.service.FileService;
import ai.maum.mlt.common.system.SystemCode;
import ai.maum.mlt.common.util.FileUtils;
import ai.maum.mlt.dictionary.entity.TestFileEntity;
import ai.maum.mlt.dictionary.entity.TestResultDetailEntity;
import ai.maum.mlt.dictionary.entity.TestResultEntity;
import ai.maum.mlt.dictionary.service.TestFileService;
import ai.maum.mlt.dictionary.service.TestFileService.DictionaryType;
import ai.maum.mlt.dictionary.service.TestResultService;
import ai.maum.mlt.dictionary.service.TestSentenceService;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import maum.brain.nlp.Nlp.NerDict.Dictionary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("api/dictionary/test")
public class TestDictionaryController {

  static final Logger logger = LoggerFactory.getLogger(TestDictionaryController.class);

  @Autowired
  private TestFileService testFileService;

  @Autowired
  private TestSentenceService testSentenceService;

  @Autowired
  private FileService fileService;

  @Autowired
  private FileUtils fileUtils;

  @Autowired
  private TestResultService testResultService;

  @Value("${dictionary.file.path}")
  private String dictionaryPath;

  @UriRoleDesc(role = "dictionary^C")
  @RequestMapping(value = "/upload-file", method = RequestMethod.POST)
  public ResponseEntity<?> uploadFile(@RequestParam("type") String testType,
      @RequestParam("workspaceId") String workspaceId, @RequestParam("creatorId") String userId,
      @RequestParam("file") MultipartFile file)
      throws IOException {
    logger.info(
        "===== call api  [[/api/dictionary/test/upload-file/{testType}/{workspaceId}/{userId}]] :: testType {}, workspaceId {}, id {}",
        testType, workspaceId, userId);
    HashMap<String, Object> result = new HashMap<>();
    try {
      TestFileService.DictionaryType type = TestFileService.DictionaryType.valueOf(testType.toUpperCase());
      result = testFileService.uploadFile(file, workspaceId, type, userId);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("uploadFile e : " , e);
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/download-file/{workspaceId}/{fileId}", method = RequestMethod.GET)
  public ResponseEntity<?> downloadFile(@PathVariable("workspaceId") String workspaceId,
      @PathVariable("fileId") String fileId) throws IOException {
    logger.info(
        "===== call api  [[/api/dictionary/test/downloadFile/{workspaceId}]] :: workspaceId {}, id {}",
        workspaceId, fileId);
    try {
      String fileName = fileId + SystemCode.FILE_EXTENSION_TEXT;
      String path = fileUtils.getSystemConfPath(dictionaryPath + "/" + workspaceId) + "/" + fileName;
      File file = new File(path);
      InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
      HttpHeaders httpHeaders = new HttpHeaders();
      httpHeaders.add(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + fileName);
      httpHeaders.add(HttpHeaders.CONTENT_TYPE, "application/octet-stream");
      return new ResponseEntity<>(resource, httpHeaders, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("downloadFile e : " , e);
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^D")
  @RequestMapping(value = "/delete-files/{testType}/{workspaceId}", method = RequestMethod.POST)
  public ResponseEntity<?> deleteFiles(@PathVariable("testType") String testType,
      @PathVariable("workspaceId") String workspaceId,
      @RequestBody List<Long> ids) throws IOException {
    logger.info(
        "===== call api  [[/api/dictionary/test/delete-files/{testType}/{workspaceId}]] :: testType {}, workspaceId {}, id {}",
        testType, workspaceId, ids.toString());
    try {
      FileEntity entity = new FileEntity();
      List<String> fileIds = testFileService.getFileIds(ids);
      entity.setIds(fileIds);
      entity.setWorkspaceId(workspaceId);

      TestFileService.DictionaryType type = TestFileService.DictionaryType.valueOf(testType.toUpperCase());
      testFileService.deleteFile(ids, type, entity.getWorkspaceId());
      fileService.deleteDictionaryFile(entity);
      fileUtils.deleteFile(entity.getIds(), dictionaryPath, SystemCode.FILE_EXTENSION_TEXT);
      return new ResponseEntity<>(null, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("deleteFiles e : " , e);
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/get-fileList/{testType}/{workspaceId}", method = RequestMethod.POST)
  public ResponseEntity<?> getFileList(@PathVariable("testType") String testType,
      @PathVariable("workspaceId") String workspaceId) throws IOException {
    logger.info(
        "===== call api  [[/api/dictionary/test/get-fileList/{testType}/{workspaceId}]] :: testType {}, workspaceId {}, id {}",
        testType, workspaceId);
    HashMap<String, Object> result = new HashMap<String, Object>();
    try {
      TestFileService.DictionaryType type = TestFileService.DictionaryType.valueOf(testType.toUpperCase());
      List<TestFileEntity> list = new ArrayList<TestFileEntity>();
      list = testFileService.getFileList(workspaceId, type);
      result.put("list", list);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getFileList e : " , e);
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/get-result/{testType}/{workspaceId}/{dictionaryId}", method = RequestMethod.POST)
  public ResponseEntity<?> getResultList(@PathVariable("testType") String testType,
      @PathVariable("workspaceId") String workspaceId, @PathVariable("dictionaryId") String dictionaryId) throws IOException {
    logger.info(
        "===== call api  [[/api/dictionary/test/get-result/{testType}/{workspaceId}]] :: testType {}, workspaceId {}, dictionaryId {}",
        testType, workspaceId, dictionaryId);
    HashMap<String, Object> result = new HashMap<String, Object>();
    try {
      TestResultService.DictionaryType type = TestResultService.DictionaryType.valueOf(testType.toUpperCase());
      List<TestResultEntity> list = new ArrayList<TestResultEntity>();
      list = testResultService.getTestResult(workspaceId, type, dictionaryId);
      result.put("list", list);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getResultList e : " , e);
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/get-resultDetail/{testType}/{workspaceId}/{testId}", method = RequestMethod.POST)
  public ResponseEntity<?> getResultList(@PathVariable("testType") String testType,
      @PathVariable("workspaceId") String workspaceId, @PathVariable("testId") long testId) throws IOException {
    logger.info(
        "===== call api  [[/api/dictionary/test/get-resultDetail/{testType}/{workspaceId}]] :: testType {}, workspaceId {}, testId {}",
        testType, workspaceId, testId);
    HashMap<String, Object> result = new HashMap<String, Object>();
    try {
      TestResultDetailEntity entity = testResultService.getTestResultDetail(workspaceId, testId);
      result.put("detail", entity);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getResultList e : " , e);
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^D")
  @RequestMapping(value = "/delete-result/{testType}/{workspaceId}", method = RequestMethod.POST)
  public ResponseEntity<?> deleteResultList(@PathVariable("testType") String testType,
      @PathVariable("workspaceId") String workspaceId, @RequestBody List<Integer> ids) throws IOException {
    logger.info(
        "===== call api  [[/api/dictionary/test/delete-result/{testType}/{workspaceId}]] :: testType {}, workspaceId {}, id {}",
        testType, workspaceId, ids.toString());
    try {
      if (!ids.isEmpty()) {
        testResultService.deleteTestResults(workspaceId, ids);
      }
      return new ResponseEntity<>(null, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("deleteResultList e : " , e);
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
