package ai.maum.mlt.dictionary.repository;

import ai.maum.mlt.dictionary.entity.NerDictionaryLineEntity;
import java.util.Date;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface NerDictionaryLineRepository extends JpaRepository<NerDictionaryLineEntity, String> {

  @Query(value = "SELECT d "
      + "         FROM NerDictionaryLineEntity d "
      + "         WHERE d.versionId = :versionId "
      + "         ORDER BY d.lineIdx ")
  List<NerDictionaryLineEntity> findAllByVersionId(
      @Param("versionId") String versionId
  );

  @Query(value = "SELECT d "
      + "         FROM NerDictionaryLineEntity d "
      + "         WHERE d.versionId = :versionId "
      + "         AND d.dicType = :dictType"
      + "         ORDER BY d.lineIdx")
  Page<NerDictionaryLineEntity> findPageAllByVersionIdAndDictType(
      @Param("versionId") String versionId, @Param("dictType") int dictType, Pageable pageable
  );

  @Query(value = "SELECT count(d) "
      + "         FROM NerDictionaryLineEntity d "
      + "         WHERE d.versionId = :versionId "
      + "         AND d.dicType = :dictType "
      + "         ORDER BY d.lineIdx")
  int countAllByVersionIdandAndDicType(
      @Param("versionId") String versionId, @Param("dictType") int dictType
  );

  @Modifying
  @Transactional
  @Query(value = "DELETE "
      + "         FROM NerDictionaryLineEntity d "
      + "         WHERE d.versionId = :versionId "
      + "         AND d.id = :id ")
  void deleteByIdAndVersionId(
      @Param("id") String id,
      @Param("versionId") String versionId
  );

  @Modifying
  @Transactional
  @Query(value = "DELETE "
      + "         FROM NerDictionaryLineEntity d "
      + "         WHERE d.versionId = :versionId ")
  void deleteAllByVersionId(
      @Param("versionId") String versionId
  );

  @Modifying
  @Transactional
  @Query(value = "UPDATE  NerDictionaryLineEntity d "
      + " SET "
      + " d.changeTag = :changeTag ,"
      + " d.createdAt = :createdAt ,"
      + " d.originTag = :originTag ,"
      + " d.pattern = :pattern ,"
      + " d.tag = :tag ,"
      + " d.word = :word "
      + "         WHERE d.dicType = :dicType "
      + "         AND d.versionId = :versionId "
      + "         AND d.id = :id ")
  int updateByTypeAndVersionIdAndId(
      @Param("changeTag") String changeTag,
      @Param("createdAt") Date createdAt,
      @Param("originTag") String originTag,
      @Param("pattern") String pattern,
      @Param("tag") String tag,
      @Param("word") String word,
      @Param("dicType") int dicType,
      @Param("versionId") String versionId,
      @Param("id") String id
  );

  @Modifying
  @Transactional
  @Query(value = "UPDATE  NerDictionaryLineEntity d "
      + " SET "
      + " d.lineIdx = d.lineIdx+1 "
      + "         WHERE d.versionId = :versionId "
      + "         AND d.workspaceId = :workspaceId"
      + "         AND d.dicType = :dicType"
      + "         AND d.lineIdx >= :lineIdx")
  int updateLineIdxByLindIdxAndVersionIdAndWorkspaceIdAndDicType(
      @Param("lineIdx") long lineIdx,
      @Param("versionId") String versionId,
      @Param("workspaceId") String workspaceId,
      @Param("dicType") int dicType
  );

  @Modifying
  @Transactional
  @Query(value = "UPDATE  NerDictionaryLineEntity d "
      + " SET "
      + " d.lineIdx = d.lineIdx-1 "
      + "         WHERE d.versionId = :versionId "
      + "         AND d.workspaceId = :workspaceId"
      + "         AND d.dicType = :dicType"
      + "         AND d.lineIdx > :lineIdx")
  int updateLineIdxMinusByLindIdxAndVersionIdAndWorkspaceIdAndDicType(
      @Param("lineIdx") long lineIdx,
      @Param("versionId") String versionId,
      @Param("workspaceId") String workspaceId,
      @Param("dicType") int dicType
  );

  void deleteById(String Id);

  @Query(value = "SELECT distinct d.dicType "
      + "         FROM NerDictionaryLineEntity d "
      + "         WHERE d.versionId = :versionId ")
  List<Integer> getDistinctTypeByVersionId(@Param("versionId") String versionId);
}
