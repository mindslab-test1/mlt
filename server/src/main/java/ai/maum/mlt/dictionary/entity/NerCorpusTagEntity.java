package ai.maum.mlt.dictionary.entity;

import ai.maum.mlt.common.pagenation.PageParameters;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;

@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "MAI_NER_CORPUS_TAG")
public class NerCorpusTagEntity extends PageParameters implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "NER_CORPUS_TAG_GEN")
  @SequenceGenerator(name = "NER_CORPUS_TAG_GEN", sequenceName = "MAI_NER_CORPUS_TAG_SEQ", allocationSize = 5)
  @Column(name = "ID", length = 40)
  private long id;

  @Column(name = "TAG", length = 50, nullable = false)
  private String tag;

  @Column(name = "WORD", length = 50, nullable = false)
  private String word;

  @CreatedDate
  @Column(name = "CREATED_AT")
  private Date createdAt;
}
