package ai.maum.mlt.dictionary.entity;

import ai.maum.mlt.common.auth.entity.WorkspaceEntity;
import ai.maum.mlt.common.pagenation.PageParameters;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "MAI_NER_DICTIONARY_LINE")
public class NerDictionaryLineEntity extends PageParameters implements Serializable {

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid")
  @Column(name = "ID", length = 40)
  private String id;

  @Column(name = "WORKSPACE_ID", length = 40, nullable = false)
  private String workspaceId;

  @Column(name = "DIC_TYPE", length = 40, nullable = false)
  private int dicType;

  @Column(name = "TAG", length = 1000)
  private String tag;

  @Column(name = "PATTERN", length = 1000)
  private String pattern;

  @Column(name = "WORD", length = 1000)
  private String word;

  @Column(name = "ORIGIN_TAG", length = 1000)
  private String originTag;

  @Column(name = "CHANGE_TAG", length = 1000)
  private String changeTag;

  @Column(name = "DESCRIPTION", length = 1000)
  private String description;

  @Column(name = "VERSION_ID", length = 40, nullable = false)
  private String versionId;

  @Column(name = "LINE_IDX")
  private Long lineIdx;

  @CreatedDate
  @Column(name = "CREATED_AT")
  private Date createdAt;

  @ManyToOne
  @JoinColumn(name = "WORKSPACE_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private WorkspaceEntity workspaceEntity;
}
