package ai.maum.mlt.dictionary.entity;

import ai.maum.mlt.common.auth.entity.UserEntity;
import ai.maum.mlt.common.auth.entity.WorkspaceEntity;
import ai.maum.mlt.common.file.entity.FileEntity;
import ai.maum.mlt.common.pagenation.PageParameters;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;

@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "MAI_TEST_FILES")
public class TestFileEntity extends PageParameters implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_FILES_INDEX_HISTORY_GEN")
  @SequenceGenerator(name = "TEST_FILES_INDEX_HISTORY_GEN", sequenceName = "MAI_TEST_FILES_IDX_HSTY_SEQ", allocationSize = 5)
  @Column(name = "ID", length = 40)
  private long id;

  @Column(name = "WORKSPACE_ID", length = 40, nullable = false)
  private String workspaceId;

  @Column(name = "FILE_ID", length = 40)
  private String fileId;

  @Column(name = "FILE_NAME", length = 60, nullable = false)
  private String fileName;

  @Column(name = "TEST_TYPE", length = 40, nullable = false)
  private int testType;

  @Column(name = "LINE_CNT", length = 50)
  private int lineCnt;

  @Column(name = "DESCRIPTION", length = 40)
  private String description;

  @Column(name = "CREATOR_ID", length = 40)
  private String creatorId;

  @CreatedDate
  @Column(name = "CREATED_AT")
  private Date createdAt;

  @ManyToOne
  @JoinColumn(name = "FILE_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private FileEntity fileEntity;

  @OneToMany(fetch = FetchType.EAGER, mappedBy = "testFileId", cascade = CascadeType.REMOVE, orphanRemoval = true)
  private List<TestSentenceEntity> testSentenceEntities;

  @ManyToOne
  @JoinColumn(name = "CREATOR_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity createUserEntity;

  @ManyToOne
  @JoinColumn(name = "WORKSPACE_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private WorkspaceEntity workspaceEntity;
}
