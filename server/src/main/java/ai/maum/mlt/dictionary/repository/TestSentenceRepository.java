package ai.maum.mlt.dictionary.repository;

import ai.maum.mlt.dictionary.entity.TestSentenceEntity;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface TestSentenceRepository extends JpaRepository<TestSentenceEntity, String> {

  List<TestSentenceEntity> findAllByTestFileIdAndWorkspaceId(long fileId, String workspaceId);

  List<TestSentenceEntity> findAllByTestTypeAndWorkspaceId(int testType, String workspaceId);

  @Modifying
  @Transactional
  int deleteByIdAndWorkspaceId(long id, String workspaceId);

  @Modifying
  @Transactional
  int deleteByTestFileIdAndWorkspaceId(long fileId, String workspaceId);


  //  @Query(value = "SELECT d "
  //      + "         FROM TestSentenceEntity d "
  //      + "         WHERE d.workspaceId = :workspaceId "
  //      + "         AND  d.fileId = :fileId "
  //      + "         ORDER BY d.createdAt DESC")
  //  List<TestSentenceEntity> findAllByFileIdAndWorkspaceId(@Param("fileId") String fileId, @Param("workspaceId") String workspaceId);
  //
  //  @Query(value = "SELECT d "
  //      + "         FROM TestSentenceEntity d "
  //      + "         WHERE  d.workspaceId = :workspaceId "
  //      + "         AND d.testType = :testType "
  //      + "         ORDER BY d.createdAt DESC")
  //  List<TestSentenceEntity> findAllByTestTypeAndWorkspaceId(@Param("testType") int testType, @Param("workspaceId") String workspaceId);

//  @Query(value = "DELETE "
//      + "         FROM TestSentenceEntity d "
//      + "         WHERE d.workspaceId = :workspaceId "
//      + "         AND d.id = :id ")
//  int deleteByIdAndWorkspaceId(@Param("id") String id, @Param("workspaceId") String workspaceId);


//  @Query(value = "DELETE "
//      + "         FROM TestSentenceEntity d "
//      + "         WHERE d.workspaceId = :workspaceId "
//      + "         AND d.fileId = :fileId ")
//  int deleteAllByFileIdAndWorkspaceId(@Param("fileId") Long fileId, @Param("workspaceId") String workspaceId);
}
