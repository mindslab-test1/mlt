package ai.maum.mlt.dictionary.service;

import ai.maum.mlt.dictionary.entity.TestResultDetailEntity;
import ai.maum.mlt.dictionary.entity.TestResultEntity;
import java.util.List;

public interface TestResultService {

  enum DictionaryType {
    NER, MORPH, REPLACEMENT
  }

  List<TestResultEntity> getTestResult(String workspaceId, TestResultService.DictionaryType type, String dictionaryId);

  TestResultDetailEntity getTestResultDetail(String workspaceId, long testId);

  long insertTestResult(TestResultEntity entity);

  void insertTestResultDetail(TestResultDetailEntity entity);

  void deleteTestResult(String workspaceId, long id);

  void deleteTestResults(String workspaceId, List<Integer> id);

}
