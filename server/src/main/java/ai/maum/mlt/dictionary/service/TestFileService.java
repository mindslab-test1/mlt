package ai.maum.mlt.dictionary.service;

import ai.maum.mlt.common.file.entity.FileEntity;
import ai.maum.mlt.dictionary.entity.TestFileEntity;
import java.util.HashMap;
import java.util.List;
import org.springframework.web.multipart.MultipartFile;

public interface TestFileService {

  enum DictionaryType {
    NER, MORPH, REPLACEMENT
  }

  List<TestFileEntity> getFileList(String workspaceId, TestFileService.DictionaryType type);

  List<String> getFileIds(List<Long> list);

  String getFileId(long id);

  void deleteFile(List<Long> ids, DictionaryType type, String workspaceId) throws Exception;

  HashMap<String, Object> uploadFile(MultipartFile file, String workspaceId,
      TestFileService.DictionaryType type, String userId)
      throws Exception;
}
