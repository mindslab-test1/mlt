package ai.maum.mlt.dictionary.repository;

import ai.maum.mlt.dictionary.entity.PosDictionaryLineEntity;
import java.util.Date;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface PosDictionaryLineRepository extends JpaRepository<PosDictionaryLineEntity, String> {

  @Query(value = "SELECT d "
      + "         FROM PosDictionaryLineEntity d "
      + "         WHERE d.versionId = :versionId "
      + "         ORDER BY d.lineIdx ")
  List<PosDictionaryLineEntity> findAllByVersionId(
      @Param("versionId") String versionId
  );

  @Modifying
  @Transactional
  @Query(value = "DELETE "
      + "         FROM PosDictionaryLineEntity d "
      + "         WHERE d.versionId = :versionId "
      + "         AND d.id = :id ")
  void deleteByIdAndVersionId(
      @Param("id") String id,
      @Param("versionId") String versionId
  );

  @Modifying
  @Transactional
  @Query(value = "DELETE "
      + "         FROM PosDictionaryLineEntity d "
      + "         WHERE d.versionId = :versionId ")
  void deleteAllByVersionId(
      @Param("versionId") String versionId
  );

  @Modifying
  @Transactional
  @Query(value = "UPDATE  PosDictionaryLineEntity d "
      + " SET "
      + " d.word = :word ,"
      + " d.createdAt = :createdAt ,"
      + " d.pattern = :pattern ,"
      + " d.compoundType = :compoundType ,"
      + " d.srcPos = :srcPos ,"
      + " d.destPos = :destPos,"
      + " d.description = :description"
      + "         WHERE d.dicType = :dicType "
      + "         AND d.versionId = :versionId "
      + "         AND d.id = :id ")
  int updateByTypeAndVersionIdAndId(
      @Param("word") String word,
      @Param("createdAt") Date createdAt,
      @Param("pattern") String pattern,
      @Param("srcPos") String srcPos,
      @Param("destPos") String destPos,
      @Param("compoundType") String compoundType,
      @Param("description") String description,
      @Param("dicType") int dicType,
      @Param("versionId") String versionId,
      @Param("id") String id
  );

  @Query(value = "SELECT d "
      + "         FROM PosDictionaryLineEntity d "
      + "         WHERE d.versionId = :versionId "
      + "         AND d.dicType = :dictType "
      + "         ORDER BY d.lineIdx ")
  Page<PosDictionaryLineEntity> findPageAllByVersionIdAndDictType(
      @Param("versionId") String versionId, @Param("dictType") int dictType, Pageable pageable
  );

  @Query(value = "SELECT count(d) "
      + "         FROM PosDictionaryLineEntity d "
      + "         WHERE d.versionId = :versionId "
      + "         AND d.dicType = :dictType "
      + "         ORDER BY d.lineIdx ")
  int countAllByVersionIdandAndDicType(
      @Param("versionId") String versionId, @Param("dictType") int dictType
  );

  @Modifying
  @Transactional
  @Query(value = "UPDATE  PosDictionaryLineEntity d "
      + " SET "
      + " d.lineIdx = d.lineIdx+1 "
      + "         WHERE d.versionId = :versionId "
      + "         AND d.workspaceId = :workspaceId "
      + "         AND d.dicType = :dicType "
      + "         AND d.lineIdx >= :lineIdx ")
  int updateLineIdxByLindIdxAndVersionIdAndWorkspaceIdAndDicType(
      @Param("lineIdx") long lineIdx,
      @Param("versionId") String versionId,
      @Param("workspaceId") String workspaceId,
      @Param("dicType") int dicType
  );

  @Modifying
  @Transactional
  @Query(value = "UPDATE  PosDictionaryLineEntity d "
      + " SET "
      + " d.lineIdx = d.lineIdx-1 "
      + "         WHERE d.versionId = :versionId "
      + "         AND d.workspaceId = :workspaceId "
      + "         AND d.dicType = :dicType "
      + "         AND d.lineIdx > :lineIdx ")
  int updateLineIdxMinusByLindIdxAndVersionIdAndWorkspaceIdAndDicType(
      @Param("lineIdx") long lineIdx,
      @Param("versionId") String versionId,
      @Param("workspaceId") String workspaceId,
      @Param("dicType") int dicType
  );

  void deleteById(String Id);

  @Query(value = "SELECT distinct d.dicType "
      + "         FROM PosDictionaryLineEntity d "
      + "         WHERE d.versionId = :versionId ")
  List<Integer> getDistinctTypeByVersionId(@Param("versionId") String versionId);

}
