package ai.maum.mlt.dictionary.service;

import ai.maum.mlt.common.file.entity.FileEntity;
import ai.maum.mlt.common.file.repository.FileRepository;
import ai.maum.mlt.common.file.service.FileService;
import ai.maum.mlt.common.system.SystemCode;
import ai.maum.mlt.common.system.SystemErrMsg;
import ai.maum.mlt.common.util.FileUtils;
import ai.maum.mlt.dictionary.entity.TestFileEntity;
import ai.maum.mlt.dictionary.entity.TestSentenceEntity;
import ai.maum.mlt.dictionary.repository.TestSentenceRepository;
import ai.maum.mlt.dictionary.repository.TestFileRepository;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class TestFileServiceImpl implements TestFileService {

  @Value("${dictionary.file.path}")
  private String dictionaryPath;


  @Autowired
  private FileUtils fileUtils;

  @Autowired
  private FileService fileService;

  @Autowired
  private FileRepository fileRepository;

  @Autowired
  private TestFileRepository testFileRepository;

  @Autowired
  private TestSentenceRepository testSentenceRepository;

  @Override
  public List<TestFileEntity> getFileList(String workspaceId, DictionaryType type) {
    List<TestFileEntity> list = new ArrayList<>();
    list = testFileRepository.findAllByWorkspaceIdAndTestType(workspaceId, type.ordinal());
    return list;
  }

  @Override
  public List<String> getFileIds(List<Long> list) {
    return testFileRepository.findFileIdsById(list);
  }

  @Override
  public String getFileId(long id) {
    return testFileRepository.findFileIdById(id);
  }

  @Override
  @Transactional
  public void deleteFile(List<Long> ids, DictionaryType type, String workspaceId) throws Exception {
    try {
      for (long id : ids) {
        testSentenceRepository.deleteByTestFileIdAndWorkspaceId(id, workspaceId);
        testFileRepository.deleteByIdAndWorkspaceIdAndTestType(id ,workspaceId, type.ordinal());
      }
    } catch (Exception e) {
      throw e;
    }
  }

  /**
   * 딕셔너리 테스트 파일 업로드
   * @param file 업로드할 파일
   * @param workspaceId
   * @param type 테스트 타입
   * @return resultMap 성공여부
   * @throws Exception
   */
  @Override
  @Transactional
  public HashMap<String, Object> uploadFile(MultipartFile file, String workspaceId,
      DictionaryType type, String userId) throws Exception {
    try {
      HashMap<String, Object> resultMap = fileService
          .insertFileWithoutChecksum(file, workspaceId, dictionaryPath,
              SystemCode.FILE_GRP_PPOS_DICTIONARY_TEST, SystemCode.FILE_EXTENSION_TEXT, userId);
      if ((boolean) resultMap.get("isSuccess")) {
        insertFileContentToTranscript(workspaceId, resultMap.get("id").toString(), type, userId);
      }
      return resultMap;
    } catch (Exception e) {
      throw e;
    }
  }

  private void insertFileContentToTranscript(String workspaceId, String fileId, DictionaryType type, String userId)
      throws Exception {
    try {
      FileEntity file = fileRepository.findOne(fileId);

      // txt파일을 내용을 바로 db에 insert
      String fileName = fileId + SystemCode.FILE_EXTENSION_TEXT;
      String path =
          fileUtils.getSystemConfPath(dictionaryPath + "/" + workspaceId) + "/" + fileName;
      File readFile = new File(path);
      List<String> lines = new ArrayList<String>();
      BufferedReader br;

      br = new BufferedReader(new FileReader(readFile));
      String line;
      while ((line = br.readLine()) != null) {
        if (!(line.equals(""))) {
          line = line.trim().replaceAll("([^.])$", "$1.") + '\n';
          lines.add(line);
        }
      }

      // 파일 엔티티 저장
      Date date = new Date();
      TestFileEntity entity = new TestFileEntity();
      entity.setWorkspaceId(workspaceId);
      entity.setLineCnt(lines.size());
      entity.setCreatorId(userId);
      entity.setCreatedAt(file.getCreatedAt());
      entity.setFileName(file.getName());
      entity.setTestType(type.ordinal());
      entity.setFileId(file.getId());
      TestFileEntity result_entity = testFileRepository.save(entity);

      // 문장 엔티티 저장
      for (String sent : lines) {
        TestSentenceEntity sentenceEntity = new TestSentenceEntity();
        sentenceEntity.setWorkspaceId(workspaceId);
        sentenceEntity.setCreatedAt(date);
        sentenceEntity.setTestFileId(result_entity.getId());
        sentenceEntity.setTestType(type.ordinal());
        sentenceEntity.setSentence(sent);
        testSentenceRepository.save(sentenceEntity);
      }
    } catch (Exception e) {
      throw e;
    }
  }
}
