package ai.maum.mlt.dictionary.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.dictionary.entity.NerCorpusTagEntity;
import ai.maum.mlt.dictionary.service.NerCorpusTagService;
import java.util.HashMap;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/dictionary/ner/tag")
public class NerCorpusTagController {

  static final Logger logger = LoggerFactory.getLogger(NerCorpusTagController.class);

  @Autowired
  private NerCorpusTagService nerCorpusTagService;

  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/get-tagList", method = RequestMethod.POST)
  public ResponseEntity<?> getCorpusTagList() {
    logger.info(
        "======= call api  POST [[api/dictionary/ner-corpus-manage/corpus-tag/get-tagList]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {
      List<NerCorpusTagEntity> nerCorpusTagEntity = nerCorpusTagService.getList();
      result.put("tag_list", nerCorpusTagEntity);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getCorpusTagList e : " , e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^C")
  @RequestMapping(value = "/insert-tagList", method = RequestMethod.POST)
  public ResponseEntity<?> insertCorpusTagList(@RequestBody List<NerCorpusTagEntity> list) {
    logger.info(
        "======= call api  POST [[api/dictionary/ner-corpus-manage/corpus-tag/insert-tagList]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {
      nerCorpusTagService.insertTags(list);
      return new ResponseEntity<>(null, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("insertCorpusTagList e : " , e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^D")
  @RequestMapping(value = "/delete-tagList", method = RequestMethod.POST)
  public ResponseEntity<?> deleteCorpusTagList() {
    logger.info(
        "======= call api  POST [[api/dictionary/ner-corpus-manage/corpus-tag/delete-tagList]] =======");
    try {
      nerCorpusTagService.deleteAll();
      return new ResponseEntity<>(null, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("deleteCorpusTagList e : " , e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}

