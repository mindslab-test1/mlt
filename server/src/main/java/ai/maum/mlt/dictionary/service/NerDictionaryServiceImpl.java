package ai.maum.mlt.dictionary.service;

import ai.maum.mlt.dictionary.entity.NerDictionaryEntity;
import ai.maum.mlt.dictionary.repository.NerDictionaryRepository;
import java.util.Date;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NerDictionaryServiceImpl implements NerDictionaryService {

  @Autowired
  NerDictionaryRepository nerDictionaryRepository;

  @Override
  public List<NerDictionaryEntity> getDictionaries() {
    return nerDictionaryRepository.findAll();
  }

  @Override
  public NerDictionaryEntity getDictionary(String name) {
    NerDictionaryEntity res = new NerDictionaryEntity();
    List<NerDictionaryEntity> list = nerDictionaryRepository.findAllByName(name);
    if (!list.isEmpty()) {
      res = list.get(0);
    } else {
      res.setId("");
    }
    return res;
  }

  @Override
  public String getDictionaryId(String name) {
    String res = new String();
    List<String> list = nerDictionaryRepository.findIdByName(name);
    if (!list.isEmpty()) {
      res = list.get(0);
    }
    return res;
  }

  @Override
  public String getDictionaryName(String id) {
    String res = new String();
    String name = nerDictionaryRepository.findNameById(id);
    res = name.isEmpty() ? "" : name;
    return res;
  }

  @Override
  public NerDictionaryEntity insertDictionary(NerDictionaryEntity entity) {
    entity.setCreatedAt(new Date());
    return nerDictionaryRepository.save(entity);
  }

  @Transactional
  @Override
  public int updateDictionary(NerDictionaryEntity entity) {
    return nerDictionaryRepository.updateDescription(entity.getId(), entity.getDescription());
  }


  @Override
  public void deleteDictionaries(List<String> idList) {
    for (String id: idList) {
      nerDictionaryRepository.deleteById(id);
    }
    return;
  }

  @Override
  public void deleteDictionary(String id) {
    nerDictionaryRepository.deleteById(id);
    return;
  }
}
