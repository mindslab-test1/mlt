package ai.maum.mlt.dictionary.service;

import ai.maum.mlt.dictionary.entity.TestResultDetailEntity;
import ai.maum.mlt.dictionary.entity.TestResultEntity;
import ai.maum.mlt.dictionary.repository.TestResultDetailRepository;
import ai.maum.mlt.dictionary.repository.TestResultRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class TestResultServiceImpl implements TestResultService {

  @Value("${dictionary.file.path}")
  private String dictionaryPath;

  @Autowired
  private TestResultRepository testResultRepository;

  @Autowired
  private TestResultDetailRepository testResultDetailRepository;

  @Override
  public List<TestResultEntity> getTestResult(String workspaceId, TestResultService.DictionaryType type, String dictionaryId) {
    List<TestResultEntity> list = testResultRepository
        .findAllByWorkspaceIdAndTestTypeAndDictionaryId(workspaceId, type.ordinal(), dictionaryId);
    return list;
  }

  @Override
  public TestResultDetailEntity getTestResultDetail(String workspaceId, long testId) {
    TestResultDetailEntity entity = testResultDetailRepository.findTopByWorkspaceIdAndTestId(workspaceId, testId);
    return entity;
  }

  @Override
  public void deleteTestResult(String workspaceId, long id) {
    testResultDetailRepository.deleteTestResultDetailEntitiesByTestId(id);
    testResultRepository.deleteTestResultEntityById(id);
  }

  @Override
  public void deleteTestResults(String workspaceId, List<Integer> ids) {
    for (long id : ids) {
      testResultDetailRepository.deleteTestResultDetailEntitiesByTestId(id);
      testResultRepository.deleteTestResultEntityById(id);
    }
  }

  @Override
  public long insertTestResult(TestResultEntity entity) {
    TestResultEntity result = testResultRepository.save(entity);
    long id = result.getId();
    return id;
  }

  @Override
  public void insertTestResultDetail(TestResultDetailEntity entity) {
    testResultDetailRepository.save(entity);
  }
}
