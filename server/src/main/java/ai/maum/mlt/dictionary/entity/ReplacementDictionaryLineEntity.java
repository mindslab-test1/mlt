package ai.maum.mlt.dictionary.entity;

import ai.maum.mlt.common.auth.entity.UserEntity;
import ai.maum.mlt.common.pagenation.PageParameters;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;

@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "MAI_REPLACEMENT_DICTIONARY_LINE")
public class ReplacementDictionaryLineEntity extends PageParameters implements Serializable {

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid")
  @Column(name = "ID", length = 40)
  private String id;

  // nullable = false, unique = true 여야.
  // data 정합정을 코드를 통해 유지할 수 있도록 한다.
  @Column(name = "SRC_STR", length = 1000)
  private String srcStr;

  @Column(name = "DEST_STR", length = 1000)
  private String destStr;

  @Column(name = "TYPE", length = 10, nullable = false)
  private String type;

  @Column(name = "DESCRIPTION", length = 1000)
  private String description;

  @Column(name = "VERSION_ID", length = 40, nullable = false)
  private String versionId;

  @Column(name = "CREATOR_ID", length = 40)
  private String creatorId;

  @Column(name = "LINE_IDX")
  private Long lineIdx;

  @CreatedDate
  @Column(name = "CREATED_AT")
  private Date createdAt;

  @Column(name = "USE_YN", length = 1, nullable = false)
  private String useYn = "Y";

  @ManyToOne
  @JoinColumn(name = "TYPE", referencedColumnName = "NAME", insertable = false, updatable = false)
  private ReplacementDictionaryTypeEntity typeEntity;

  @ManyToOne
  @JoinColumn(name = "CREATOR_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity createUserEntity;
}
