package ai.maum.mlt.dictionary.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.common.auth.entity.WorkspaceEntity;
import ai.maum.mlt.common.auth.service.WorkspaceService;
import ai.maum.mlt.common.git.GitManager;
import ai.maum.mlt.common.system.SystemCode;
import ai.maum.mlt.common.version.entity.CommitHistoryEntity;
import ai.maum.mlt.common.version.service.CommitHistoryService;
import ai.maum.mlt.dictionary.entity.NerCorpusEntity;
import ai.maum.mlt.dictionary.entity.NerCorpusTagEntity;
import ai.maum.mlt.dictionary.entity.NerDictionaryEntity;
import ai.maum.mlt.dictionary.entity.NerDictionaryLineEntity;
import ai.maum.mlt.dictionary.entity.TestResultDetailEntity;
import ai.maum.mlt.dictionary.entity.TestResultEntity;
import ai.maum.mlt.dictionary.service.NerCorpusService;
import ai.maum.mlt.dictionary.service.NerCorpusTagService;
import ai.maum.mlt.dictionary.service.NerDictionaryLineService;
import ai.maum.mlt.dictionary.service.NerDictionaryService;
import ai.maum.mlt.dictionary.service.TestResultService;
import ai.maum.mlt.dictionary.service.TestResultService.DictionaryType;
import ai.maum.mlt.itfc.TaGrpcInterfaceManager;
import com.google.protobuf.util.JsonFormat;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.OutputStream;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import maum.brain.nlp.Nlp.Document;
import maum.brain.nlp.Nlp.InputText;
import maum.brain.nlp.Nlp.NamedEntity;
import maum.brain.nlp.Nlp.Sentence;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import maum.brain.nlp.Nlp3Custom.NamedEntityDictList;
import maum.brain.nlp.Nlp3Custom.NamedEntityDictList.NamedEntityDict;
import maum.brain.nlp.Nlp3Custom.NamedEntityDictList.NamedEntityDictEntry;
import maum.brain.nlp.Nlp3Custom.NamedEntityDictList.NamedEntityDictType;
import maum.brain.nlp.Nlp3Custom.NamedEntityDictList.NeChangeDict;
import maum.brain.nlp.Nlp3Custom.NamedEntityDictList.NeChangeDictList;
import maum.brain.nlp.Nlp3Custom.NamedEntityDictList.NeEtcDict;
import maum.brain.nlp.Nlp3Custom.NamedEntityDictList.NeEtcDictList;
import maum.brain.nlp.Nlp3Custom.NamedEntityDictList.NeFilterDict;
import maum.brain.nlp.Nlp3Custom.NamedEntityDictList.NeFilterDictList;
import maum.brain.nlp.Nlp3Custom.NamedEntityDictList.NePatternDict;
import maum.brain.nlp.Nlp3Custom.NamedEntityDictList.NePatternDictList;
import maum.common.LangOuterClass.LangCode;
import org.eclipse.jgit.lib.ObjectId;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ai.maum.mlt.common.util.FileUtils;

class NerResult {

  private String sentence;
  private List<String> ners;

  public NerResult() {
    sentence = "";
    ners = new ArrayList<String>();
  }

  public NerResult(String sent) {
    sentence = sent;
    ners = new ArrayList<String>();
  }

  public void setSentence(String sentence) {
    this.sentence = sentence;
  }

  public boolean hasNerList() {
    return !ners.isEmpty();
  }

  public void put(String tag) {
    this.ners.add(tag);
  }

  public String toString() {
    JSONObject jsonObj = new JSONObject();
    jsonObj.put("sentence", sentence);
    jsonObj.put("ners", ners.toString());
    return jsonObj.toString();
  }
}

@RestController
@RequestMapping("api/dictionary/ner")
public class NerDictionaryController {

  static final Logger logger = LoggerFactory.getLogger(NerDictionaryController.class);

  enum NerDicType {
    NE_PRE_PATTERN, // 전처리 패턴 사전
    NE_POST_PATTERN, // 후처리 패전 사전
    NE_POST_CHANGE, // 후처리 태그 변환 사전
    NE_FILTER, // 예외 처리 사전
    NE_NEW_TAG, // 태그 추가 사전
    NE_ADD_PRE_DIC, // 전처리 추가 사전
    NE_ADD_POST_DIC, // 추처리 추가 사전
    NE_ETC_DIC // 기타 사전
  }

  @Autowired
  private NerDictionaryLineService nerDictionaryLineService;

  @Autowired
  private NerDictionaryService nerDictionaryService;

  @Autowired
  private WorkspaceService workspaceService;

  @Autowired
  private NerCorpusService nerCorpusService;

  @Autowired
  private NerCorpusTagService nerCorpusTagService;

  @Autowired
  private TestResultService testResultService;

  @Autowired
  private CommitHistoryService commitHistoryService;

  @Value("${git.file.path}")
  private String gitPath;

  @Value("${brain-ta.nlp3kor.ip}")
  private String brainTaNlp3korIp;

  @Value("${brain-ta.nlp3kor.port}")
  private int brainTaNlp3korPort;

  @Value("${temp.ner.dict.file.path}")
  private String nerDictPath;

  @Value("${temp.use.dict}")
  private String useInit;

  @Autowired
  private FileUtils fileUtils;

  // dictionary 관련.
  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/get-dictionaryList", method = RequestMethod.POST)
  public ResponseEntity<?> getDictionaryList() {
    logger.info("======= call api  POST [[api/dictionary/ner/get-dictionaryList]] =======");
    List<HashMap<String, Object>> result = new ArrayList<HashMap<String, Object>>();
    try {
      List<NerDictionaryEntity> nerDictionaryEntities = nerDictionaryService.getDictionaries();
      if (!nerDictionaryEntities.isEmpty()) {
        for (NerDictionaryEntity entity : nerDictionaryEntities) {
          int cnt = 0;
          CommitHistoryEntity cs = new CommitHistoryEntity();
          cs.setWorkspaceId(entity.getWorkspaceId());
          cs.setType("NER");
          cs.setFileId(entity.getId());
          cnt = commitHistoryService.getCountCommit(cs);
          HashMap<String, Object> elem = new HashMap<String, Object>();
          elem.put("entity", entity);
          elem.put("cnt", cnt);
          result.add(elem);
        }
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getDictionaryList e : " , e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/get-dictionaryName", method = RequestMethod.POST)
  public ResponseEntity<?> getDictionaryName(@RequestBody String id) {
    logger.info("======= call api  POST [[api/dictionary/ner/get-dictionaryName]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {
      String name = "";
      name = nerDictionaryService.getDictionaryName(id);
      result.put("dictionary_name", name);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getDictionaryName e : " , e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/get-dictionary", method = RequestMethod.POST)
  public ResponseEntity<?> getDictionary(@RequestBody String name) {
    logger.info("======= call api  POST [[api/dictionary/ner/get-dictionary]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {
      String res = nerDictionaryService.getDictionaryId(name);
      result.put("dictionary_id", res);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getDictionary e : " , e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^C")
  @RequestMapping(value = "/insert-dictionary", method = RequestMethod.POST)
  public ResponseEntity<?> insertDictionary(@RequestBody NerDictionaryEntity entity) {
    logger.info("======= call api  POST [[api/dictionary/ner/insert-dictionary]] =======", entity);
    HashMap<String, Object> result = new HashMap<>();
    try {
      NerDictionaryEntity dic = nerDictionaryService.getDictionary(entity.getName());
      if (dic.getId().isEmpty()) {
        result.put("message", "INSERT_SUCCESS");
        result.put("dictionary", nerDictionaryService.insertDictionary(entity));
      } else {
        result.put("message", "INSERT_DUPLICATED");
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("insertDictionary e : " , e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^U")
  @RequestMapping(value = "/update-dictionary", method = RequestMethod.POST)
  public ResponseEntity<?> updateDictionary(@RequestBody NerDictionaryEntity entity) {
    logger.info("======= call api  POST [[api/dictionary/ner/update-dictionary]] =======", entity);
    HashMap<String, Object> result = new HashMap<>();
    try {
      NerDictionaryEntity dic = nerDictionaryService.getDictionary(entity.getName());
      if (!dic.getId().isEmpty()) {
        nerDictionaryService.updateDictionary(entity);
      }
      return new ResponseEntity<>(null, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("updateDictionary e : " , e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^D")
  @RequestMapping(value = "/delete-dictionaries", method = RequestMethod.POST)
  public ResponseEntity<?> deleteDictionaries(@RequestBody List<NerDictionaryEntity> entities) {
    logger.info("======= call api  POST [[api/dictionary/ner/delete-dictionaries]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {
      for (NerDictionaryEntity entity : entities) {
        nerDictionaryService.deleteDictionary(entity.getId());

        CommitHistoryEntity cs = new CommitHistoryEntity();
        cs.setFileId(entity.getId());
        cs.setWorkspaceId(entity.getWorkspaceId());
        cs.setType("NER");
        commitHistoryService.deleteCommit(cs);
      }
      result.put("resultCode", "SUCCESS");
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("deleteDictionaries e : " , e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * ner dictionary 데이터 가져오기
   *
   * @param hashMap workspaceId, id(ner dictionary id)
   */
  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/get-dictionaryContents", method = RequestMethod.POST)
  public ResponseEntity<?> getDictionaryContents(@RequestBody HashMap<String, String> hashMap) {
    logger.info("======= call api  POST [[api/dictionary/ner/get-dictionaryContents]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {
      // ner 사전 내용 가져오기.
      List<NerDictionaryLineEntity> nerDictionaryEntities = getContentsFromDbOrGit(
          hashMap.get("id"), hashMap.get("workspaceId"));
      if (!nerDictionaryEntities.isEmpty()) {
        result.put("dictionary_contents", nerDictionaryEntities);
      }
      if (useInit.replaceAll("\"","").equals("true")) {
        result.put("use_init", true);
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getDictionaryContents e : " , e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/get-dictionaryContentsWithPage", method = RequestMethod.POST)
  public ResponseEntity<?> getDictionaryContentsWithPage(@RequestBody NerDictionaryLineEntity param) {
    logger.info("======= call api  POST [[api/dictionary/morph/get-dictionaryContents]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {
      int cnt = 0;
      cnt = nerDictionaryLineService.getCntDicLines(param.getVersionId(), param.getDicType());
      Page<NerDictionaryLineEntity> nerDictionaryLineEntities = getContentsFromDbOrGitWithPage(param);
      if (nerDictionaryLineEntities.getNumberOfElements() > 0) {
        result.put("dictionary_contents", nerDictionaryLineEntities);
        if (cnt == 0) {
          cnt = nerDictionaryLineEntities.getNumberOfElements();
        }
        result.put("dictionary_contents_size", cnt);
      }
      if (useInit.replaceAll("\"","").equals("true")) {
        result.put("use_init", true);
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("dictionaryContentsWithPage e : ", e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^D")
  @RequestMapping(value = "/delete-dictionaryContents", method = RequestMethod.POST)
  public ResponseEntity<?> deleteDictionaryContents(
      @RequestBody List<NerDictionaryLineEntity> list) {
    logger.info("======= call api  POST [[api/dictionary/ner/delete-dictionaryContents]] =======");
    try {
      nerDictionaryLineService.deleteDicLines(list);
      return new ResponseEntity<>(null, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("delete-dictionaryContents e : " , e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }


  @UriRoleDesc(role = "dictionary^D")
  @RequestMapping(value = "/delete-dictionaryContentsWithIdx", method = RequestMethod.POST)
  public ResponseEntity<?> deleteDictionaryContentsWithIdx(
      @RequestBody List<NerDictionaryLineEntity> list) {
    logger
        .info("======= call api  POST [[api/dictionary/morph/delete-dictionaryContentsWithIdx]] =======");
    try {
      nerDictionaryLineService.deleteDicLinesWithIdx(list);
      return new ResponseEntity<>(null, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("delete-dictionaryContentsWithIdx e : ", e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^D")
  @RequestMapping(value = "/delete-dictionaryContentsAll", method = RequestMethod.POST)
  public ResponseEntity<?> deleteDictionaryContentsAll(@RequestBody String versionId) {
    logger
        .info("======= call api  POST [[api/dictionary/ner/delete-dictionaryContentsAll]] =======");
    try {
      nerDictionaryLineService.deleteDicLinesAllByVersionId(versionId);
      return new ResponseEntity<>(null, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("deleteDictionaryContentsAll e : " , e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^C")
  @RequestMapping(value = "/insert-dictionaryContents", method = RequestMethod.POST)
  public ResponseEntity<?> insertDictionaryList(@RequestBody List<NerDictionaryLineEntity> list) {
    logger.info("======= call api  POST [[api/dictionary/ner/insert-dictionaryContents]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {
      nerDictionaryLineService.insertLines(list);
      result.put("dict_list", list);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("insertDictionaryList e : " , e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^C")
  @RequestMapping(value = "/insert-dictionaryLine", method = RequestMethod.POST)
  public ResponseEntity<?> insertDictionaryLine(@RequestBody NerDictionaryLineEntity entity) {
    logger.info("======= call api  POST [[api/dictionary/ner/insert-dictionaryLine]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {
      NerDictionaryLineEntity nerDictionaryLineEntity = nerDictionaryLineService.insertLine(entity);
      result.put("dictionary_line", nerDictionaryLineEntity);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("insertDictionaryLine e : " , e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^C")
  @RequestMapping(value = "/insert-dictionaryLineWithIdx", method = RequestMethod.POST)
  public ResponseEntity<?> insertDictionaryLineWithIdx(@RequestBody NerDictionaryLineEntity entity) {
    logger.info("======= call api  POST [[api/dictionary/morph/insert-dictionaryLineWithIdx]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {
      nerDictionaryLineService.updateIdx(entity);
      NerDictionaryLineEntity nerDictionaryLineEntity = nerDictionaryLineService.insertLine(entity);
      result.put("dictionary_line", nerDictionaryLineEntity);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("insert dictionaryLineWithIdx e : ", e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^U")
  @RequestMapping(value = "/update-dictionaryLine", method = RequestMethod.POST)
  public ResponseEntity<?> updateDictionaryLine(@RequestBody NerDictionaryLineEntity entity) {
    logger.info("======= call api  POST [[api/dictionary/ner/update-dictionaryLine]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {
      int cnt = nerDictionaryLineService.updateLine(entity);
      if (cnt > 0) {
        result.put("success", true);
      } else {
        result.put("success", false);
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("updateDictionaryLine e : " , e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  // commit
  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/getFileByCommit", method = RequestMethod.POST)
  public ResponseEntity<?> getFileByCommit(@RequestBody HashMap<String, String> param) {
    logger.info("======= call api  POST [[api/dictionary/ner/getFileByCommit]] =======", param);
    logger.debug("start : {}", param);

    try {
      GitManager gs = new GitManager();
      gs.setWorkingDirectory(gitPath + "/ner");

      String result = gs.readFileFromCommit(param.get("workspaceId"), param.get("id"),
          ObjectId.fromString(param.get("source")));

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getFileByCommit e : " , e.getMessage());
      return new ResponseEntity<>(HttpStatus.FAILED_DEPENDENCY);
    }
  }

  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/getDiff", method = RequestMethod.POST)
  public ResponseEntity<?> getDiff(@RequestBody HashMap<String, String> param) {
    logger.info("======= call api  POST [[api/dictionary/ner/getDiff]] =======", param);
    logger.debug("start : {}", param);
    try {
      GitManager gs = new GitManager();
      gs.setWorkingDirectory(gitPath + "/ner");

      String result = gs.diffFile(param.get("workspaceId"), param.get("id"), param.get("source"),
          param.get("target"));
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getDiff e : " , e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/getCommitList", method = RequestMethod.POST)
  public ResponseEntity<?> getCommitList(@RequestBody NerDictionaryEntity param) {
    logger.info("======= call api  POST [[api/dictionary/ner/getCommitList]] =======", param);
    logger.debug("start : {}", param);

    try {
      GitManager gs = new GitManager();
      gs.setWorkingDirectory(gitPath + "/ner");

      List<HashMap<String, Object>> result = gs
          .getCommitList(param.getWorkspaceId(), param.getId());
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getCommitList e : " , e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/get-dictionary-from-git", method = RequestMethod.POST)
  public ResponseEntity<?> getDictionaryByCommit(@RequestBody NerDictionaryEntity param) {
    logger.info("======= call api  POST [[api/dictionary/ner/get-dictionary-from-git]] =======",
        param);
    logger.debug("start : {}", param);
    HashMap<String, Object> result = new HashMap<>();
    try {
      GitManager gs = new GitManager();
      gs.setWorkingDirectory(gitPath + "/ner");
      List<NerDictionaryLineEntity> nerDictionaryEntities;
      List<HashMap<String, Object>> list = gs.getCommitList(param.getWorkspaceId(), param.getId());

      Date last_time = null;
      HashMap<String, Object> last_item = new HashMap<String, Object>();
      boolean first = true;
      for (HashMap<String, Object> item : list) {
        if (first) {
          last_time = (Date) item.get("commitTime");
          last_item = item;
          first = false;
        }
        if (last_time.compareTo((Date) item.get("commitTime")) < 0) {
          last_time = (Date) item.get("commitTime");
          last_item = item;
        }
      }
      String contents = null;
      if (last_item.size() > 0) {
        contents = gs.readFileFromCommit(param.getWorkspaceId(), param.getId(),
            ObjectId.fromString(last_item.get("hashKey").toString()));
      }
      nerDictionaryEntities = jsonStringToEntity(contents);
      if (nerDictionaryEntities.size() > 0) {
        result.put("dictionary_contents", nerDictionaryEntities);
      }
      if (useInit.replaceAll("\"","").equals("true")) {
        result.put("use_init", true);
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getDictionaryByCommit e : " , e.getMessage());
      return new ResponseEntity<>(HttpStatus.FAILED_DEPENDENCY);
    }
  }

  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/download-dictionary", method = RequestMethod.POST)
  public ResponseEntity<?> downloadDictionary(@RequestBody List<NerDictionaryLineEntity> param) {
    logger.info("======= call api  POST [[api/dictionary/ner/download-dictionary]] =======", param);
    logger.debug("start : {}", param);
    HashMap<String, Object> result = new HashMap<>();
    try {
      result.put("dictionary_contents",
          JsonFormat.printer().includingDefaultValueFields().print(entityToProto(param, false)));
      if (useInit.replaceAll("\"","").equals("true")) {
        result.put("use_init", true);
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("downloadDictionary e : " , e.getMessage());
      return new ResponseEntity<>(HttpStatus.FAILED_DEPENDENCY);
    }
  }

  @UriRoleDesc(role = "dictionary^C")
  @RequestMapping(value = "/upload-dictionary", method = RequestMethod.POST)
  public ResponseEntity<?> uploadDictionary(@RequestBody ArrayList<String> param) {
    logger.info("======= call api  POST [[api/dictionary/ner/upload-dictionary]] =======", param);
    logger.debug("start : {}", param);
    HashMap<String, Object> result = new HashMap<>();
    try {
      List<NerDictionaryLineEntity> list = jsonStringToEntity(param.get(0));
      for (NerDictionaryLineEntity elem : list) {
        elem.setVersionId(param.get(1));
        elem.setWorkspaceId(param.get(2));
      }
      nerDictionaryLineService.insertLines(list);
      List<NerDictionaryLineEntity> result_list = nerDictionaryLineService
          .getDicLines(param.get(1));
      result.put("dictionary_contents", result_list);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("uploadDictionary e : " , e.getMessage());
      return new ResponseEntity<>(HttpStatus.FAILED_DEPENDENCY);
    }
  }

  @UriRoleDesc(role = "dictionary^U")
  @RequestMapping(value = "/commit-dictionary", method = RequestMethod.POST)
  public ResponseEntity<?> commitDictionary(@RequestBody NerDictionaryEntity entity) {
    logger.info("======= call api  POST [[api/dictionary/ner/commit-dictionary]] =======", entity);
    logger.debug("start : {}", entity);
    HashMap<String, Object> result = new HashMap<>();
    try {
      GitManager gs = new GitManager();
      gs.setWorkingDirectory(gitPath + "/ner");
      String content = "";
      List<NerDictionaryLineEntity> entities = nerDictionaryLineService.getDicLines(entity.getId());
      content = JsonFormat.printer().includingDefaultValueFields()
          .print(entityToProto(entities, false));
      HashMap<String, String> value = new HashMap<String, String>();
      value.put("gitPath", gitPath + "/ner");
      value.put("workspaceId", entity.getWorkspaceId());
      value.put("name", entity.getCreatorId());
      value.put("email", "mlt@ai");
      value.put("fileName", entity.getId());
      value.put("content", content);
      value.put("messsage", "");
      gs.updateFile(value);

      // 커밋히스토리에 적재
      CommitHistoryEntity ch = new CommitHistoryEntity();
      ch.setFileId(entity.getId());
      ch.setWorkspaceId(entity.getWorkspaceId());
      ch.setType("NER");
      commitHistoryService.insertCommit(ch);

      result.put("entities", entities);

      // commit 후 사전데이터 삭제
//      nerDictionaryLineService.deleteDicLinesAllByVersionId(entity.getId());
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("commitDictionary e : " , e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  public List<NerDictionaryLineEntity> jsonStringToEntity(String content) {
    NamedEntityDictList.Builder result = NamedEntityDictList.newBuilder();
    List<NerDictionaryLineEntity> entities = new ArrayList<NerDictionaryLineEntity>();
    int [] cnt_arr  = new int[10];
    try {
      JsonFormat.parser().merge(content, result);
      List<NamedEntityDict> list = result.getNeDictsList();
      for (NamedEntityDict dict : list) {
        switch (dict.getTypeValue()) {
          case NamedEntityDictType.NE_PRE_PATTERN_VALUE:
          case NamedEntityDictType.NE_POST_PATTERN_VALUE:
            if (dict.getDict().getPatternDict().isInitialized()) {
              List<NePatternDict> pattern_list = dict.getDict().getPatternDict().getEntriesList();
              for (NePatternDict pattern_item : pattern_list) {
                cnt_arr[dict.getTypeValue()]++;
                NerDictionaryLineEntity entity = new NerDictionaryLineEntity();
                entity.setDicType(dict.getTypeValue());
                entity.setTag((pattern_item.getNeTag() == null) ? "" : pattern_item.getNeTag());
                entity.setPattern(
                    (pattern_item.getPattern() == null) ? "" : pattern_item.getPattern());
                entity.setLineIdx(new Long(cnt_arr[dict.getTypeValue()]));
                entities.add(entity);
              }
            }
            break;
          case NamedEntityDictType.NE_POST_CHANGE_VALUE:
            if (dict.getDict().getChangeDict().isInitialized()) {
              List<NeChangeDict> change_list = dict.getDict().getChangeDict().getEntriesList();
              for (NeChangeDict change_item : change_list) {
                cnt_arr[dict.getTypeValue()]++;
                NerDictionaryLineEntity entity = new NerDictionaryLineEntity();
                entity.setDicType(dict.getTypeValue());
                entity.setChangeTag(
                    (change_item.getDestNeTag() == null) ? "" : change_item.getDestNeTag());
                entity.setOriginTag(
                    (change_item.getSrcNeTag() == null) ? "" : change_item.getSrcNeTag());
                entity.setWord((change_item.getWord() == null) ? "" : change_item.getWord());
                entity.setLineIdx(new Long(cnt_arr[dict.getTypeValue()]));
                entities.add(entity);
              }
            }
            break;
          case NamedEntityDictType.NE_FILTER_VALUE:
            if (dict.getDict().getFilterDict().isInitialized()) {
              List<NeFilterDict> change_list = dict.getDict().getFilterDict().getEntriesList();
              for (NeFilterDict ect_item : change_list) {
                cnt_arr[dict.getTypeValue()]++;
                NerDictionaryLineEntity entity = new NerDictionaryLineEntity();
                entity.setDicType(dict.getTypeValue());
                entity.setWord((ect_item.getWord() == null) ? "" : ect_item.getWord());
                entity.setLineIdx(new Long(cnt_arr[dict.getTypeValue()]));
                entities.add(entity);
              }
            }
            break;
          case NamedEntityDictType.NE_ADD_PRE_DIC_VALUE:
          case NamedEntityDictType.NE_ADD_POST_DIC_VALUE:
            if (dict.getDict().getEtcDict().isInitialized()) {
              List<NeEtcDict> ect_list = dict.getDict().getEtcDict().getEntriesList();
              for (NeEtcDict ect_item : ect_list) {
                cnt_arr[dict.getTypeValue()]++;
                NerDictionaryLineEntity entity = new NerDictionaryLineEntity();
                entity.setDicType(dict.getTypeValue());
                entity.setTag((ect_item.getNeTag() == null) ? "" : ect_item.getNeTag());
                entity.setWord((ect_item.getWord() == null) ? "" : ect_item.getWord());
                entity.setLineIdx(new Long(cnt_arr[dict.getTypeValue()]));
                entities.add(entity);
              }
            }
            break;
          default:
            break;
        }
      }
      return entities;
    } catch (Exception e) {
      logger.error("jsonStringToEntity e : " , e.getMessage());
      return entities;
    }
  }


  public NamedEntityDictList entityToProto(List<NerDictionaryLineEntity> entities,
      boolean isClear) {
    NamedEntityDictList.Builder list = NamedEntityDictList.newBuilder();
    try {
      NamedEntityDict.Builder namedEntityDict = NamedEntityDict.newBuilder();

      NePatternDictList.Builder post_pattern_list = NePatternDictList.newBuilder();
      NePatternDictList.Builder pre_pattern_list = NePatternDictList.newBuilder();
      NeEtcDictList.Builder post_add_list = NeEtcDictList.newBuilder();
      NeEtcDictList.Builder pre_add_list = NeEtcDictList.newBuilder();
      NeChangeDictList.Builder change_list = NeChangeDictList.newBuilder();
      NeFilterDictList.Builder filter_list = NeFilterDictList.newBuilder();

      NePatternDict.Builder post_pattern_dic = NePatternDict.newBuilder();
      NePatternDict.Builder pre_pattern_dic = NePatternDict.newBuilder();
      NeEtcDict.Builder post_add_dict = NeEtcDict.newBuilder();
      NeEtcDict.Builder pre_add_dict = NeEtcDict.newBuilder();
      NeChangeDict.Builder change_dict = NeChangeDict.newBuilder();
      NeFilterDict.Builder filter_dict = NeFilterDict.newBuilder();

//    todo  brain-ta ner 사전 reset 처리 반영후에 clear 처리 로직 주석해제.
      if (isClear) {
        List<Integer> type_list = new ArrayList<Integer>();
        for (NerDictionaryLineEntity item : entities) {
          int type = item.getDicType();
          if (type_list.contains(type)) {
            continue;
          }
          type_list.add(type);
          switch (type) {
            case NamedEntityDictType.NE_PRE_PATTERN_VALUE:
              pre_pattern_list.addEntries(pre_pattern_dic);
              break;
            case NamedEntityDictType.NE_POST_PATTERN_VALUE:
              post_pattern_list.addEntries(post_pattern_dic);
              break;
            case NamedEntityDictType.NE_POST_CHANGE_VALUE:
              change_list.addEntries(change_dict);
              break;
            case NamedEntityDictType.NE_FILTER_VALUE:
              filter_list.addEntries(filter_dict);
              break;
            case NamedEntityDictType.NE_ADD_PRE_DIC_VALUE:
              pre_add_list.addEntries(pre_add_dict);
              break;
            case NamedEntityDictType.NE_ADD_POST_DIC_VALUE:
              post_add_list.addEntries(post_add_dict);
              break;
            default:
              break;
          }
        }
        if (pre_pattern_list.getEntriesCount() > 0) {
          namedEntityDict.setType(NamedEntityDictType.NE_PRE_PATTERN)
              .setDict(NamedEntityDictEntry.newBuilder().clearPatternDict().build());
          list.addNeDicts(namedEntityDict);
        }
        if (post_pattern_list.getEntriesCount() > 0) {
          namedEntityDict.setType(NamedEntityDictType.NE_POST_PATTERN)
              .setDict(NamedEntityDictEntry.newBuilder().clearPatternDict().build());
          list.addNeDicts(namedEntityDict);
        }
        if (change_list.getEntriesCount() > 0) {
          namedEntityDict.setType(NamedEntityDictType.NE_POST_CHANGE)
              .setDict(NamedEntityDictEntry.newBuilder().clearChangeDict().build());
          list.addNeDicts(namedEntityDict);
        }
        if (filter_list.getEntriesCount() > 0) {
          namedEntityDict.setType(NamedEntityDictType.NE_FILTER)
              .setDict(NamedEntityDictEntry.newBuilder().clearFilterDict().build());
          list.addNeDicts(namedEntityDict);
        }
        if (pre_add_list.getEntriesCount() > 0) {
          namedEntityDict.setType(NamedEntityDictType.NE_ADD_PRE_DIC)
              .setDict(NamedEntityDictEntry.newBuilder().clearEtcDict().build());
          list.addNeDicts(namedEntityDict);
        }
        if (post_add_list.getEntriesCount() > 0) {
          namedEntityDict.setType(NamedEntityDictType.NE_ADD_POST_DIC)
              .setDict(NamedEntityDictEntry.newBuilder().clearEtcDict().build());
          list.addNeDicts(namedEntityDict);
        }
      } else {
        for (NerDictionaryLineEntity item : entities) {
          int type = item.getDicType();
          switch (type) {
            case NamedEntityDictType.NE_PRE_PATTERN_VALUE:
              if (item.getTag() == null || item.getPattern() == null) {
                continue;
              }
              pre_pattern_dic.setNeTag(item.getTag());
              pre_pattern_dic.setPattern(item.getPattern());
              pre_pattern_list.addEntries(pre_pattern_dic);
              break;
            case NamedEntityDictType.NE_POST_PATTERN_VALUE:
              if (item.getTag() == null || item.getPattern() == null) {
                continue;
              }
              post_pattern_dic.setNeTag(item.getTag());
              post_pattern_dic.setPattern(item.getPattern());
              post_pattern_list.addEntries(post_pattern_dic);
              break;
            case NamedEntityDictType.NE_POST_CHANGE_VALUE:
              if (item.getWord() == null || item.getOriginTag() == null
                  || item.getChangeTag() == null) {
                continue;
              }
              change_dict.setWord(item.getWord());
              change_dict.setSrcNeTag(item.getOriginTag());
              change_dict.setDestNeTag(item.getChangeTag());
              change_list.addEntries(change_dict);
              break;
            case NamedEntityDictType.NE_FILTER_VALUE:
              if (item.getWord() == null) {
                continue;
              }
              filter_dict.setWord(item.getWord());
              filter_list.addEntries(filter_dict);
              break;
            case NamedEntityDictType.NE_ADD_PRE_DIC_VALUE:
              if (item.getTag() == null || item.getWord() == null) {
                continue;
              }
              pre_add_dict.setNeTag(item.getTag());
              pre_add_dict.setWord(item.getWord());
              pre_add_list.addEntries(pre_add_dict);
              break;
            case NamedEntityDictType.NE_ADD_POST_DIC_VALUE:
              if (item.getTag() == null || item.getWord() == null) {
                continue;
              }
              post_add_dict.setNeTag(item.getTag());
              post_add_dict.setWord(item.getWord());
              post_add_list.addEntries(post_add_dict);
              break;
          }
        }

        if (pre_pattern_list.getEntriesCount() > 0) {
          namedEntityDict.setType(NamedEntityDictType.NE_PRE_PATTERN)
              .setDict(NamedEntityDictEntry.newBuilder().setPatternDict(pre_pattern_list).build());
          list.addNeDicts(namedEntityDict);
        }
        if (post_pattern_list.getEntriesCount() > 0) {
          namedEntityDict.setType(NamedEntityDictType.NE_POST_PATTERN)
              .setDict(NamedEntityDictEntry.newBuilder().setPatternDict(post_pattern_list).build());
          list.addNeDicts(namedEntityDict);
        }
        if (change_list.getEntriesCount() > 0) {
          namedEntityDict.setType(NamedEntityDictType.NE_POST_CHANGE)
              .setDict(NamedEntityDictEntry.newBuilder().setChangeDict(change_list).build());
          list.addNeDicts(namedEntityDict);
        }
        if (filter_list.getEntriesCount() > 0) {
          namedEntityDict.setType(NamedEntityDictType.NE_FILTER)
              .setDict(NamedEntityDictEntry.newBuilder().setFilterDict(filter_list).build());
          list.addNeDicts(namedEntityDict);
        }
        if (pre_add_list.getEntriesCount() > 0) {
          namedEntityDict.setType(NamedEntityDictType.NE_ADD_PRE_DIC)
              .setDict(NamedEntityDictEntry.newBuilder().setEtcDict(pre_add_list).build());
          list.addNeDicts(namedEntityDict);
        }
        if (post_add_list.getEntriesCount() > 0) {
          namedEntityDict.setType(NamedEntityDictType.NE_ADD_POST_DIC)
              .setDict(NamedEntityDictEntry.newBuilder().setEtcDict(post_add_list).build());
          list.addNeDicts(namedEntityDict);
        }
      }
      return list.build();
    } catch (Exception e) {
      logger.error("entityToProto e : " , e.getMessage());
      return list.build();
    }
  }

  // 보류
  @UriRoleDesc(role = "dictionary^D")
  @RequestMapping(value = "/delete-dictionary", method = RequestMethod.POST)
  public ResponseEntity<?> deleteDictionary(@RequestBody String id) {
    logger.info("======= call api  POST [[api/dictionary/ner/delete-dictionary]] =======");
    try {
      nerDictionaryService.deleteDictionary(id);
      return new ResponseEntity<>(null, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("deleteDictionary e : " , e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * 특정 ner 사전 적용 기능
   *
   * @param param checkedList 적용할 사전 종류 리스트, isReset 사전 클리어 여부
   */
  @UriRoleDesc(role = "dictionary^X")
  @RequestMapping(value = "/apply-and-test/{workspaceId}/{dictionaryId}/{testerId}", method = RequestMethod.POST)
  public ResponseEntity<?> testSentenceList(@PathVariable("dictionaryId") String dictionaryId,
      @PathVariable("workspaceId") String workspaceId, @PathVariable("testerId") String testerId,
      @RequestBody HashMap<String, Object> param) {
    logger.info("======= call api  POST [[api/dictionary/ner/apply-and-test]] =======");
    HashMap<String, Object> result = new HashMap<>();
    List<String> resultList = new ArrayList<String>();
    try {
      List<Integer> checkedList = (List<Integer>) param.get("checkedList");

      boolean isClear = (boolean) param.get("isReset");

      if (isClear) { // 사전 초기화
        // 사전 초기화 적용
        applyDicitonary(workspaceId, dictionaryId, checkedList, true);
      } else { // 사전 적용과 테스트
        // 사전 데이터 적용
        applyDicitonary(workspaceId, dictionaryId, checkedList, false);

        // 코퍼스문장 가져오기
        List<String> list = replaceCorpus();

        // 사전적용 후 Test 내용 저장
        TestResultEntity testResultEntity = new TestResultEntity();
        testResultEntity.setApplyCheck(checkedList.toString());
        testResultEntity.setFileId("");
        testResultEntity.setCreatedAt(new Date());
        testResultEntity.setDictionaryId(dictionaryId);
        testResultEntity.setTesterId(testerId);
        testResultEntity.setWorkspaceId(workspaceId);
        testResultEntity.setTestType(DictionaryType.NER.ordinal());

        // 테스트
        resultList = testDictionary(workspaceId, dictionaryId, list, testerId, testResultEntity);
        result.put("result", resultList);
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("testSentenceList e : " , e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * 특정 ner 사전 테스트 기능
   *
   * @param hashMap workspaceId, versionId(pos dictionary version id)
   */
  @UriRoleDesc(role = "dictionary^X")
  @RequestMapping(value = "/test", method = RequestMethod.POST)
  public ResponseEntity<?> testCorpusList(@RequestBody HashMap<String, String> hashMap) {
    logger.info("======= call api  POST [[api/dictionary/ner/corpus/test]] =======");
    HashMap<String, Object> result = new HashMap<>();
    List<String> resultList = new ArrayList<String>();
    try {
      List<String> list = replaceCorpus();
      TaGrpcInterfaceManager client = new TaGrpcInterfaceManager(brainTaNlp3korIp,
          brainTaNlp3korPort);
      WorkspaceEntity workspaceEntity = workspaceService
          .getWorkspace(hashMap.get("workspaceId").toString());

      // ner 사전 내용 가져오기.
      List<NerDictionaryLineEntity> nerDictionaryEntities = getContentsFromDbOrGit(
          hashMap.get("versionId"), hashMap.get("workspaceId"));

      for (String corpus : list) {
        InputText.Builder inputText = InputText.newBuilder();
        inputText.setText(corpus);
        // Lang은 일단 항상 한국어로만 사용하도록 코딩
        // inputText.setLang(LangCode.valueOf(workspaceEntity.getLangCode()));
        inputText.setLang(LangCode.kor);
        Document document = client.analyze(inputText.build());
        if (document != null && document.isInitialized()) {
          List<Sentence> sentences = document.getSentencesList();
          if (!sentences.isEmpty()) {
            for (Sentence sentence : sentences) {
              NluResult ne = new NluResult();
              ne.setSentence(sentence.getText());

              List<NamedEntity> ne_list = new ArrayList<NamedEntity>();

              for (NerDictionaryLineEntity entity : nerDictionaryEntities) {
                switch (entity.getDicType()) {
                  case NamedEntityDictType.NE_POST_PATTERN_VALUE:
                  case NamedEntityDictType.NE_PRE_PATTERN_VALUE:
                  case NamedEntityDictType.NE_ADD_POST_DIC_VALUE:
                  case NamedEntityDictType.NE_ADD_PRE_DIC_VALUE: {
                    List<NamedEntity> nes = sentence.getNesList().stream()
                        .filter(n -> n.getType() == entity.getTag()).collect(Collectors.toList());
                    for (NamedEntity en : nes) {
                      ne_list.add(en);
                    }
                    break;
                  }
                  case NamedEntityDictType.NE_POST_CHANGE_VALUE: {
                    List<NamedEntity> nes = sentence.getNesList().stream()
                        .filter(n -> n.getType() == entity.getChangeTag())
                        .collect(Collectors.toList());
                    for (NamedEntity en : nes) {
                      ne_list.add(en);
                    }
                    break;
                  }
                  default:
                    break;
                }
              }
              ne.puts(ne_list);
              resultList.add(ne.toString());
            }
          }
        }
      }
      result.put("result", resultList);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("testCorpusList e : " , e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/get-dictionaryContents-type/{workspaceId}/{dictionaryId}", method = RequestMethod.POST)
  public ResponseEntity<?> getDictionaryTypeList(@PathVariable("dictionaryId") String dictionaryId,
      @PathVariable("workspaceId") String workspaceId) {
    logger.info(
        "======= call api  POST [[api/dictionary/morph/get-dictionaryContents-type]] =======");
    HashMap<String, Object> result = new HashMap<>();
    List<Integer> types = new ArrayList<Integer>();
    try {
      List<NerDictionaryLineEntity> nerDictionaryEntities = nerDictionaryLineService
          .getDicLines(dictionaryId);
      if (nerDictionaryEntities.isEmpty()) { // DB에 값이 없을 경우 Commit에서 가져오기
        nerDictionaryEntities = getNerContentsFromGit(workspaceId, dictionaryId);
        for (NerDictionaryLineEntity elem : nerDictionaryEntities) {
          elem.setVersionId(dictionaryId);
          elem.setWorkspaceId(workspaceId);
        }
        nerDictionaryLineService.insertLines(nerDictionaryEntities);
      }
      types = nerDictionaryLineService.getDicTypes(dictionaryId);
      result.put("list", types);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getDictionaryTypeList e : " , e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  public List<NerDictionaryLineEntity> getNerContentsFromGit(String wid, String id) {
    try {
      GitManager gs = new GitManager();
      gs.setWorkingDirectory(gitPath + "/ner");
      List<HashMap<String, Object>> list = gs.getCommitList(wid, id);

      Date last_time = null;
      HashMap<String, Object> last_item = new HashMap<String, Object>();
      boolean first = true;
      for (HashMap<String, Object> item : list) {
        if (first) {
          last_time = (Date) item.get("commitTime");
          last_item = item;
          first = false;
        }
        if (last_time.compareTo((Date) item.get("commitTime")) < 0) {
          last_time = (Date) item.get("commitTime");
          last_item = item;
        }
      }
      String contents = null;
      if (last_item.size() > 0) {
        contents = gs
            .readFileFromCommit(wid, id, ObjectId.fromString(last_item.get("hashKey").toString()));
      }
      return jsonStringToEntity(contents);
    } catch (Exception e) {
      logger.error("getNerContentsFromGit e : " , e.getMessage());
      return new ArrayList<NerDictionaryLineEntity>();
    }
  }

  private List<NerDictionaryLineEntity> getContentsFromDbOrGit(String id, String workspace) {
    List<NerDictionaryLineEntity> nerDictionaryEntities = new ArrayList<NerDictionaryLineEntity>();
    try {
      nerDictionaryEntities = nerDictionaryLineService.getDicLines(id);
      if (nerDictionaryEntities.isEmpty()) { // DB에 값이 없을 경우 Commit에서 가져오기
        nerDictionaryEntities = getNerContentsFromGit(workspace, id);
      }
      return nerDictionaryEntities;
    } catch (Exception e) {
      logger.error("getContentsFromDbOrGit e : " , e.getMessage());
      return nerDictionaryEntities;
    }
  }

  private Page<NerDictionaryLineEntity> getContentsFromDbOrGitWithPage(NerDictionaryLineEntity entity) {
    List<NerDictionaryLineEntity> tempEntities = new ArrayList<>();
    try {
      Page<NerDictionaryLineEntity> nerDictionaryEntities = nerDictionaryLineService
          .getDicLinesWithPage(entity.getVersionId(), entity.getDicType(), entity.getPageRequest());

      if (nerDictionaryEntities.getNumberOfElements() < 1) { // DB에 값이 없을 경우 Commit에서 가져오기
        tempEntities = getNerContentsFromGit(entity.getWorkspaceId(), entity.getId());
        for (NerDictionaryLineEntity elem : tempEntities) {
          elem.setVersionId(entity.getVersionId());
          elem.setWorkspaceId(entity.getWorkspaceId());
        }
//        nerDictionaryLineService.insertLines(tempEntities);
        List<NerDictionaryLineEntity> typeEntities = tempEntities.stream()
            .filter(x -> x.getDicType() == entity.getDicType()).collect(
            Collectors.toList());
        nerDictionaryEntities = new PageImpl<NerDictionaryLineEntity>(typeEntities, entity.getPageRequest(), typeEntities.size());
      }
      return nerDictionaryEntities;
    } catch (Exception e) {
      logger.error("getContentsFromDbOrGitWithPage e : ", e.getMessage());
      tempEntities = new ArrayList<>();
      Page<NerDictionaryLineEntity> res = new PageImpl<NerDictionaryLineEntity>(
          tempEntities, entity.getPageRequest(), tempEntities.size());
      return res;
    }
  }

  public NamedEntityDictList applyDicitonary(String workspaceId, String dictionaryId,
      List<Integer> checkedList, boolean isClear) {
    try {
      TaGrpcInterfaceManager client = new TaGrpcInterfaceManager(brainTaNlp3korIp,
          brainTaNlp3korPort);

      // dictionaryId 와 일치하는 morpheme 사전 데이터 가져오기
      List<NerDictionaryLineEntity> nerDictionaryEntities = getContentsFromDbOrGit(dictionaryId,
          workspaceId);

      // pos dictionary 데이터 중 checkedTypeList에 해당하는 타입만 가져온다.
      nerDictionaryEntities = nerDictionaryEntities.stream()
          .filter(entity -> checkedList.stream().anyMatch(type -> type == entity.getDicType()))
          .collect(Collectors.toList());
      NamedEntityDictList list = entityToProto(nerDictionaryEntities, isClear);

      client.updateNerDict(list);
      return list;
    } catch (Exception e) {
      NamedEntityDictList list = NamedEntityDictList.newBuilder().build();
      logger.error("applyDicitonary e : " , e.getMessage());
      return list;
    }
  }

  public List<String> testDictionary(String workspaceId, String dictionaryId,
      List<String> sentenceList, String testerId, TestResultEntity testResultEntity) {
    List<String> resultList = new ArrayList<String>();
    try {
      long start = System.currentTimeMillis();
      TaGrpcInterfaceManager client = new TaGrpcInterfaceManager(brainTaNlp3korIp,
          brainTaNlp3korPort);

      // ner 사전 컨텐츠 가져오기.
      List<NerDictionaryLineEntity> nerDictionaryEntities = getContentsFromDbOrGit(dictionaryId,
          workspaceId);

      WorkspaceEntity workspaceEntity = workspaceService.getWorkspace(workspaceId);

      List<String> list = new ArrayList<String>();
      for (NerDictionaryLineEntity entity : nerDictionaryEntities) {
        switch (entity.getDicType()) {
          case NamedEntityDictType.NE_POST_PATTERN_VALUE:
          case NamedEntityDictType.NE_PRE_PATTERN_VALUE:
          case NamedEntityDictType.NE_ADD_POST_DIC_VALUE:
          case NamedEntityDictType.NE_ADD_PRE_DIC_VALUE: {
            if (!list.contains(entity.getTag())) {
              list.add(entity.getTag());
            }
          }
          break;
          case NamedEntityDictType.NE_POST_CHANGE_VALUE: {
            if (!list.contains(entity.getTag())) {
              list.add(entity.getTag());
            }
          }
          break;
        }
      }

      // 코퍼스 테스트
      for (String corpus : sentenceList) {
        corpus = corpus.trim();
        InputText.Builder inputText = InputText.newBuilder();
        // Lang은 일단 항상 한국어로만 사용하도록 코딩
        // inputText.setLang(LangCode.valueOf(workspaceEntity.getLangCode()));
        inputText.setLang(LangCode.kor);
        inputText.setSplitSentence(true);
        inputText.setText(corpus);
        Document document = client.analyze(inputText.build());

        if (!list.isEmpty() && !document.getSentencesList().isEmpty()) {
          for (Sentence sentence : document.getSentencesList()) {
            NluResult neResult = new NluResult(sentence.getText());
            List<NamedEntity> ne_list = sentence.getNesList();
            if (!ne_list.isEmpty()) {
              for (NamedEntity en : ne_list) {
                neResult.put(en.getText(), en.getType());
              }
              resultList.add(neResult.toString());
            }
          }
        } else {
          NluResult neResult = new NluResult(corpus);
          resultList.add(neResult.toString());
        }
      }

      // 테스트 결과 저장
      long testId = testResultService.insertTestResult(testResultEntity);
      // 테스트 세부 결과 저장
      if (testId > -1 && testerId != null) {
        TestResultDetailEntity entity = new TestResultDetailEntity();
        entity.setCreatedAt(new Date());
        entity.setTestId(testId);
        entity.setDictionaryId(dictionaryId);
        entity.setTesterId(testerId);
        entity.setWorkspaceId(workspaceId);
        entity.setTestResult(resultList.toString());
        testResultService.insertTestResultDetail(entity);
      }
      logger.trace("morpheme dictionary test, sentence:[" + resultList.size() + "]/sentence:["
          + nerDictionaryEntities.size() + "]/ time:[" + (System.currentTimeMillis() - start)
          + "]");
      return resultList;
    } catch (Exception e)

    {
      logger.error("testDictionary e : " , e.getMessage());
      throw new RuntimeException();
    }

  }

  public List<String> replaceCorpus() {
    List<String> result_sentence = new ArrayList<String>();
    try {
      List<NerCorpusEntity> nerCorpusEntities = nerCorpusService.getList();
      List<NerCorpusTagEntity> nerCorpusTagEntities = nerCorpusTagService.getList();
      Pattern pattern = Pattern.compile(SystemCode.NER_DICTIONARY_CORPUS_TAG_PATTERN);
      if (!nerCorpusEntities.isEmpty()) {
        for (NerCorpusEntity entity : nerCorpusEntities) {
          boolean b_match = false;
          for (NerCorpusTagEntity tagSet : nerCorpusTagEntities) {
            String sentence = entity.getSentence();
            Matcher matcher = pattern.matcher(sentence);
            if (matcher.find()) {
              String group = matcher.group();
              if (group.equals("<ne>" + tagSet.getTag() + "</ne>")) {
                sentence = sentence.replaceAll(matcher.group(), tagSet.getWord());
                sentence = sentence.replaceAll("<", "&lt;");
                sentence = sentence.replaceAll(">", "&gt;");
                result_sentence.add(sentence);
                b_match = true;
              }
            }
          }
          if (!b_match) {
            String sentence = entity.getSentence();
            sentence = sentence.replaceAll("<", "&lt;");
            sentence = sentence.replaceAll(">", "&gt;");
            result_sentence.add(sentence);
          }
        }
      }
      return result_sentence;
    } catch (Exception e) {
      logger.error("replaceCorpus e : " , e.getMessage());
      throw new RuntimeException();
    }
  }
  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/get-nlp-resource", method = RequestMethod.POST)
  public ResponseEntity<?> getNlpResource(@RequestBody ArrayList<String> param) {
    logger.info("======= call api  POST [[api/dictionary/ner/get-nlp-resource]] =======");
//    HashMap<String, List<NerDictionaryLineEntity>> result = new HashMap<>();
    HashMap<String, Object> result = new HashMap<>();
    List<NerDictionaryLineEntity> nerDictionaryEntities = new ArrayList<>();
    try {
      //  파일읽어오기

      String dir = fileUtils.getSystemConfPath(nerDictPath);
      String pre_pattern = "at_pre_pattern.txt";
      String post_pattern = "at_post_pattern.txt";
      String post_change = "at_post_change_dic.txt";
      String pre_dic = "at_pre_add_dic.txt";
      String post_dic = "at_post_add_dic.txt";
      String id = param.get(0);
      String workspaceId = param.get(1);
      if (dir != "") {
        File file = new File(dir + pre_pattern);
        if (file.exists()) {
          BufferedReader inFile = new BufferedReader(new FileReader(file));
          String line = null;
          long idx = 1;
          while ((line = inFile.readLine()) != null) {
            if (line.contains("=1=")) {
              NerDictionaryLineEntity entity = new NerDictionaryLineEntity();
              String [] data = line.split("=1=");
              entity.setDicType(NerDicType.NE_PRE_PATTERN.ordinal());
              entity.setCreatedAt(new Date());
              entity.setWorkspaceId(workspaceId);
              entity.setVersionId(id);
              entity.setTag(data[0]);
              entity.setPattern(data[1]);
              entity.setLineIdx(idx);
              nerDictionaryEntities.add(entity);
              idx++;
            }
          }
        }

        File file2 = new File(dir + post_pattern);
        if (file2.exists()) {
          BufferedReader inFile = new BufferedReader(new FileReader(file2));
          String line = null;
          long idx = 1;
          while ((line = inFile.readLine()) != null) {
            if (line.contains("=1=")) {
              NerDictionaryLineEntity entity = new NerDictionaryLineEntity();
              String [] data = line.split("=1=");
              entity.setDicType(NerDicType.NE_POST_PATTERN.ordinal());
              entity.setCreatedAt(new Date());
              entity.setWorkspaceId(workspaceId);
              entity.setVersionId(id);
              entity.setTag(data[0]);
              entity.setPattern(data[1]);
              entity.setLineIdx(idx);
              nerDictionaryEntities.add(entity);
              idx++;
            }
          }
        }

        File file3 = new File(dir + post_change);
        if (file3.exists()) {
          BufferedReader inFile = new BufferedReader(new FileReader(file3));
          String line = null;
          long idx = 1;
          while ((line = inFile.readLine()) != null) {
            if (line.contains("\t->\t")) {
              NerDictionaryLineEntity entity = new NerDictionaryLineEntity();
              String [] data = line.split("\\t->\\t");
              entity.setDicType(NerDicType.NE_POST_CHANGE.ordinal());

              entity.setCreatedAt(new Date());
              entity.setWorkspaceId(workspaceId);
              entity.setVersionId(id);
              if (data[0].contains("^^")) {
                String [] ori  = data[0].split("\\^\\^");
                entity.setWord(ori[0]);
                entity.setOriginTag(ori[1]);
              }
              if (data[1].contains("^^")) {
                String [] chg  = data[1].split("\\^\\^");
                entity.setChangeTag(chg[1]);
              }
              entity.setLineIdx(idx);
              nerDictionaryEntities.add(entity);
              idx++;
            }
          }
        }

        File file4 = new File(dir + post_dic);
        if (file4.exists()) {
          BufferedReader inFile = new BufferedReader(new FileReader(file4));
          String line = null;
          long idx = 1;
          while ((line = inFile.readLine()) != null) {
            if (line.contains("=")) {
              NerDictionaryLineEntity entity = new NerDictionaryLineEntity();
              String [] data = line.split("=");
              entity.setDicType(NerDicType.NE_ADD_PRE_DIC.ordinal());
              entity.setCreatedAt(new Date());
              entity.setWorkspaceId(workspaceId);
              entity.setVersionId(id);
              entity.setWord(data[0]);
              entity.setTag(data[1]);
              entity.setLineIdx(idx);
              nerDictionaryEntities.add(entity);
              idx++;
            }
          }
        }


        File file5 = new File(dir + pre_dic);
        if (file5.exists()) {
          BufferedReader inFile = new BufferedReader(new FileReader(file5));
          String line = null;
          long idx = 1;
          while ((line = inFile.readLine()) != null) {
            if (line.contains("=")) {
              NerDictionaryLineEntity entity = new NerDictionaryLineEntity();
              String [] data = line.split("=");
              entity.setDicType(NerDicType.NE_ADD_POST_DIC.ordinal());
              entity.setCreatedAt(new Date());
              entity.setWorkspaceId(workspaceId);
              entity.setVersionId(id);
              entity.setWord(data[0]);
              entity.setTag(data[1]);
              entity.setLineIdx(idx);
              nerDictionaryEntities.add(entity);
              idx++;
            }
          }
        }
      }

      if (!nerDictionaryEntities.isEmpty()) {
        nerDictionaryLineService.deleteDicLinesAllByVersionId(id);
        result.put("dictionary_contents", nerDictionaryEntities);
        nerDictionaryLineService.insertLinesWithDate(nerDictionaryEntities);
      }
      if (useInit.replaceAll("\"","").equals("true")) {
        result.put("use_init", true);
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("get Nlp ner dictionary data e : " , e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
