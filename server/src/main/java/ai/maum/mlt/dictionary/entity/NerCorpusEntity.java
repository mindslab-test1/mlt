package ai.maum.mlt.dictionary.entity;

import ai.maum.mlt.common.pagenation.PageParameters;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;

@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "MAI_NER_CORPUS")
public class NerCorpusEntity extends PageParameters implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "NER_CORPUS_INDEX_HISTORY_GEN")
  @SequenceGenerator(name = "NER_CORPUS_INDEX_HISTORY_GEN", sequenceName = "MAI_NER_CORPUS_IDX_HSTY_SEQ", allocationSize = 5)
  @Column(name = "ID", length = 40)
  private long id;

  @Column(name = "SENTENCE", nullable = false)
  @Size(min = 1, max = 200)
  private String sentence;

  @CreatedDate
  @Column(name = "CREATED_AT")
  private Date createdAt;

}
