package ai.maum.mlt.dictionary.service;

import ai.maum.mlt.dictionary.entity.NerCorpusTagEntity;
import ai.maum.mlt.dictionary.repository.NerCorpusTagRepository;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NerCorpusTagServiceImpl implements NerCorpusTagService {

  @Autowired
  NerCorpusTagRepository nerCorpusTagRepository;

  @Override
  public List<NerCorpusTagEntity> getList() {
    return nerCorpusTagRepository.findAll();
  }

  @Override
  public void insertTags(List<NerCorpusTagEntity> nerCorpusTagEntities) {
    for (NerCorpusTagEntity entity : nerCorpusTagEntities) {
      entity.setCreatedAt(new Date());
      nerCorpusTagRepository.save(entity);
    }
    return;
  }

  @Override
  public void deleteAll() {
    nerCorpusTagRepository.deleteAll();
    return;
  }
}
