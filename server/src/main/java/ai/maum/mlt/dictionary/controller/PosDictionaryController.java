package ai.maum.mlt.dictionary.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.common.auth.entity.WorkspaceEntity;
import ai.maum.mlt.common.auth.service.WorkspaceService;
import ai.maum.mlt.common.git.GitManager;
import ai.maum.mlt.common.util.FileUtils;
import ai.maum.mlt.common.version.entity.CommitHistoryEntity;
import ai.maum.mlt.common.version.service.CommitHistoryService;
import ai.maum.mlt.dictionary.entity.PosDictionaryEntity;
import ai.maum.mlt.dictionary.entity.PosDictionaryLineEntity;
import ai.maum.mlt.dictionary.entity.TestResultDetailEntity;
import ai.maum.mlt.dictionary.entity.TestResultEntity;
import ai.maum.mlt.dictionary.entity.TestSentenceEntity;
import ai.maum.mlt.dictionary.service.PosDictionaryLineService;
import ai.maum.mlt.dictionary.service.PosDictionaryService;
import ai.maum.mlt.dictionary.service.TestResultService;
import ai.maum.mlt.dictionary.service.TestResultService.DictionaryType;
import ai.maum.mlt.dictionary.service.TestSentenceService;
import ai.maum.mlt.itfc.TaGrpcInterfaceManager;
import com.google.protobuf.util.JsonFormat;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Date;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import maum.brain.nlp.Nlp.Document;
import maum.brain.nlp.Nlp.InputText;
import maum.brain.nlp.Nlp.MorphemeEval;
import maum.brain.nlp.Nlp.Sentence;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import maum.brain.nlp.Nlp3Custom.MorpDictList;
import maum.brain.nlp.Nlp3Custom.MorpDictList.MorpCompoundNounDict;
import maum.brain.nlp.Nlp3Custom.MorpDictList.MorpCompoundNounDictList;
import maum.brain.nlp.Nlp3Custom.MorpDictList.MorpCustomDict;
import maum.brain.nlp.Nlp3Custom.MorpDictList.MorpCustomDictList;
import maum.brain.nlp.Nlp3Custom.MorpDictList.MorpDict;
import maum.brain.nlp.Nlp3Custom.MorpDictList.MorpDictEntry;
import maum.brain.nlp.Nlp3Custom.MorpDictList.MorpDictType;
import maum.brain.nlp.Nlp3Custom.MorpDictList.MorpPatternDict;
import maum.brain.nlp.Nlp3Custom.MorpDictList.MorpPatternDictList;
import maum.common.LangOuterClass.LangCode;
import org.eclipse.jgit.lib.ObjectId;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.Transactional;

class MorphResult {

  private String sentence;
  private List<String> morphs;

  public MorphResult() {
    sentence = "";
    morphs = new ArrayList<String>();
  }

  public MorphResult(String sent) {
    sentence = sent;
    morphs = new ArrayList<String>();
  }

  public void setSentence(String sentence) {
    this.sentence = sentence;
  }

  public boolean hasMorphList() {
    return !morphs.isEmpty();
  }

  public void put(String tag) {
    this.morphs.add(tag);
  }

  public String toString() {
    JSONObject jsonObj = new JSONObject();
    jsonObj.put("sentence", sentence);
    jsonObj.put("morphs", morphs.toString());
    return jsonObj.toString();
  }
}

@RestController
@RequestMapping("api/dictionary/morph")
public class PosDictionaryController {

  static final Logger logger = LoggerFactory.getLogger(PosDictionaryController.class);

  @Autowired
  private PosDictionaryLineService posDictionaryLineService;

  @Autowired
  private PosDictionaryService posDictionaryService;

  @Autowired
  private TestSentenceService testSentenceService;

  @Autowired
  private TestResultService testResultService;

  @Autowired
  private WorkspaceService workspaceService;

  @Autowired
  private CommitHistoryService commitHistoryService;

  @Value("${ta.file.path}")
  private String taPath;

  @Value("${git.file.path}")
  private String gitPath;

  @Value("${brain-ta.nlp3kor.ip}")
  private String brainTaNlp3korIp;

  @Value("${brain-ta.nlp3kor.port}")
  private int brainTaNlp3korPort;

  @Value("${temp.morp.dict.file.path}")
  private String morpDictPath;

  @Value("${temp.use.dict}")
  private String useInit;

  @Autowired
  private FileUtils fileUtils;

  // dictionary 관련.
  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/get-dictionaryList", method = RequestMethod.POST)
  public ResponseEntity<?> getDictionaryList(@RequestBody String wid) {
    logger.info("======= call api  POST [[api/dictionary/morph/get-dictionaryList]] =======");
    List<HashMap<String, Object>> result = new ArrayList<HashMap<String, Object>>();
    try {
      List<PosDictionaryEntity> posDictionaryEntities = posDictionaryService.getDictionaries(wid);
      if (!posDictionaryEntities.isEmpty()) {
        for (PosDictionaryEntity entity : posDictionaryEntities) {
          int cnt = 0;
          CommitHistoryEntity cs = new CommitHistoryEntity();
          cs.setWorkspaceId(entity.getWorkspaceId());
          cs.setType("MORPH");
          cs.setFileId(entity.getId());
          cnt = commitHistoryService.getCountCommit(cs);
          HashMap<String, Object> elem = new HashMap<String, Object>();
          elem.put("entity", entity);
          elem.put("cnt", cnt);
          result.add(elem);
        }
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getDictionaryList e : ", e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/get-dictionaryName", method = RequestMethod.POST)
  public ResponseEntity<?> getDictionaryName(@RequestBody String id) {
    logger.info("======= call api  POST [[api/dictionary/morph/get-dictionaryName]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {
      String name = "";
      name = posDictionaryService.getDictionaryName(id);
      result.put("dictionary_name", name);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getDictionaryName e : ", e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/get-dictionary", method = RequestMethod.POST)
  public ResponseEntity<?> getDictionary(@RequestBody String name) {
    logger.info("======= call api  POST [[api/dictionary/morph/get-dictionary]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {
      String res = posDictionaryService.getDictionaryId(name);
      result.put("dictionary_id", res);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getDictionary e : ", e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^C")
  @RequestMapping(value = "/insert-dictionary", method = RequestMethod.POST)
  public ResponseEntity<?> insertDictionary(@RequestBody PosDictionaryEntity entity) {
    logger
        .info("======= call api  POST [[api/dictionary/morph/insert-dictionary]] =======", entity);
    HashMap<String, Object> result = new HashMap<>();
    try {
      PosDictionaryEntity dic = posDictionaryService.getDictionary(entity.getName());
      if (dic.getId().isEmpty()) {
        result.put("message", "INSERT_SUCCESS");
        result.put("dictionary", posDictionaryService.insertDictionary(entity));
      } else {
        result.put("message", "INSERT_DUPLICATED");
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("insertDictionary e : ", e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^U")
  @RequestMapping(value = "/update-dictionary", method = RequestMethod.POST)
  public ResponseEntity<?> updateDictionary(@RequestBody PosDictionaryEntity entity) {
    logger
        .info("======= call api  POST [[api/dictionary/morph/update-dictionary]] =======", entity);
    HashMap<String, Object> result = new HashMap<>();
    try {
      PosDictionaryEntity dic = posDictionaryService.getDictionary(entity.getName());
      if (!dic.getId().isEmpty()) {
        posDictionaryService.updateDictionary(entity);
      }
      return new ResponseEntity<>(null, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("updateDictionary e : ", e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^D")
  @RequestMapping(value = "/delete-dictionaries", method = RequestMethod.POST)
  public ResponseEntity<?> deleteDictionaries(@RequestBody List<PosDictionaryEntity> entities) {
    logger.info("======= call api  POST [[api/dictionary/morph/delete-dictionaries]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {
      for (PosDictionaryEntity entity : entities) {
        posDictionaryService.deleteDictionary(entity.getId());

        CommitHistoryEntity cs = new CommitHistoryEntity();
        cs.setFileId(entity.getId());
        cs.setWorkspaceId(entity.getWorkspaceId());
        cs.setType("MORPH");
        commitHistoryService.deleteCommit(cs);
      }
      result.put("resultCode", "SUCCESS");
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("deleteDictionaries e : ", e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * pos dictionary 데이터 가져오기
   *
   * @param param workspaceId, id(pos dictionary id)
   */
  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/get-dictionaryContents", method = RequestMethod.POST)
  public ResponseEntity<?> getDictionaryContents(@RequestBody HashMap<String, String> param) {
    logger.info("======= call api  POST [[api/dictionary/morph/get-dictionaryContents]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {
      List<PosDictionaryLineEntity> posDictionaryEntities = getContentsFromDbOrGit(param.get("id"),
          param.get("workspaceId"));
      if (!posDictionaryEntities.isEmpty()) {
        result.put("dictionary_contents", posDictionaryEntities);
      }
      if (useInit.replaceAll("\"","").equals("true")) {
        result.put("use_init", true);
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getDictionaryContents e : ", e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/get-dictionaryContentsWithPage", method = RequestMethod.POST)
  public ResponseEntity<?> getDictionaryContentsWithPage(@RequestBody PosDictionaryLineEntity param) {
    logger.info("======= call api  POST [[api/dictionary/morph/get-dictionaryContents]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {
      int cnt = 0;
      cnt = posDictionaryLineService.getCntDicLines(param.getVersionId(), param.getDicType());
      Page<PosDictionaryLineEntity> posDictionaryEntities = getContentsFromDbOrGitWithPage(param);
      if (posDictionaryEntities.getNumberOfElements() > 0) {
        result.put("dictionary_contents", posDictionaryEntities);
        if (cnt == 0) {
          cnt = posDictionaryEntities.getNumberOfElements();
        }
      }
      result.put("dictionary_contents_size", cnt);
      if (useInit.replaceAll("\"","").equals("true")) {
        result.put("use_init", true);
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("dictionaryContentsWithPage e : ", e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^D")
  @RequestMapping(value = "/delete-dictionaryContents", method = RequestMethod.POST)
  public ResponseEntity<?> deleteDictionaryContents(
      @RequestBody List<PosDictionaryLineEntity> list) {
    logger
        .info("======= call api  POST [[api/dictionary/morph/delete-dictionaryContents]] =======");
    try {
      posDictionaryLineService.deleteDicLines(list);
      return new ResponseEntity<>(null, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("deleteDictionaryContents e : ", e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^D")
  @RequestMapping(value = "/delete-dictionaryContentsWithIdx", method = RequestMethod.POST)
  public ResponseEntity<?> deleteDictionaryContentsWithIdx(
      @RequestBody List<PosDictionaryLineEntity> list) {
    logger
        .info("======= call api  POST [[api/dictionary/morph/delete-dictionaryContentsWithIdx]] =======");
    try {
      posDictionaryLineService.deleteDicLinesWithIdx(list);
      return new ResponseEntity<>(null, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("delete-dictionaryContentsWithIdx e : ", e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^D")
  @RequestMapping(value = "/delete-dictionaryContentsAll", method = RequestMethod.POST)
  public ResponseEntity<?> deleteDictionaryContentsAll(@RequestBody String versionId) {
    logger.info(
        "======= call api  POST [[api/dictionary/morph/delete-dictionaryContentsAll]] =======");
    try {
      posDictionaryLineService.deleteDicLinesAllByVersionId(versionId);
      return new ResponseEntity<>(null, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("deleteDictionaryContentsAll e : ", e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^C")
  @RequestMapping(value = "/insert-dictionaryContents", method = RequestMethod.POST)
  public ResponseEntity<?> insertDictionaryList(@RequestBody List<PosDictionaryLineEntity> list) {
    logger
        .info("======= call api  POST [[api/dictionary/morph/insert-dictionaryContents]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {
      if (!list.isEmpty()) {
        posDictionaryLineService.insertLines(list);
        result.put("dict_list", list);
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("insertDictionaryList e : ", e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^C")
  @RequestMapping(value = "/insert-dictionaryLine", method = RequestMethod.POST)
  public ResponseEntity<?> insertDictionaryLine(@RequestBody PosDictionaryLineEntity entity) {
    logger.info("======= call api  POST [[api/dictionary/morph/insert-dictionaryLine]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {
      PosDictionaryLineEntity PosDictionaryLineEntity = posDictionaryLineService.insertLine(entity);
      result.put("dictionary_line", PosDictionaryLineEntity);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("insertDictionaryLine e : ", e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^C")
  @RequestMapping(value = "/insert-dictionaryLineWithIdx", method = RequestMethod.POST)
  public ResponseEntity<?> insertDictionaryLineWithIdx(@RequestBody PosDictionaryLineEntity entity) {
    logger.info("======= call api  POST [[api/dictionary/morph/insert-dictionaryLineWithIdx]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {
      posDictionaryLineService.updateIdx(entity);
      PosDictionaryLineEntity posDictionaryLineEntity = posDictionaryLineService.insertLine(entity);
      result.put("dictionary_line", posDictionaryLineEntity);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("dictionaryLineWithIdx e : ", e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^U")
  @RequestMapping(value = "/update-dictionaryLine", method = RequestMethod.POST)
  public ResponseEntity<?> updateDictionaryLine(@RequestBody PosDictionaryLineEntity entity) {
    logger.info("======= call api  POST [[api/dictionary/morph/update-dictionaryLine]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {
      int cnt = posDictionaryLineService.updateLine(entity);
      if (cnt > 0) {
        result.put("success", true);
      } else {
        result.put("success", false);
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("updateDictionaryLine e : ", e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  // commit
  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/getFileByCommit", method = RequestMethod.POST)
  public ResponseEntity<?> getFileByCommit(@RequestBody HashMap<String, String> param) {
    logger.info("======= call api  POST [[api/dictionary/morph/getFileByCommit]] =======", param);
    logger.debug("start : {}", param);

    try {
      GitManager gs = new GitManager();
      gs.setWorkingDirectory(gitPath + "/morph");

      String result = gs.readFileFromCommit(param.get("workspaceId"), param.get("id"),
          ObjectId.fromString(param.get("source")));

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getFileByCommit e : ", e.getMessage());
      return new ResponseEntity<>(HttpStatus.FAILED_DEPENDENCY);
    }
  }

  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/getDiff", method = RequestMethod.POST)
  public ResponseEntity<?> getDiff(@RequestBody HashMap<String, String> param) {
    logger.info("======= call api  POST [[api/dictionary/morph/getDiff]] =======", param);
    logger.debug("start : {}", param);
    try {
      GitManager gs = new GitManager();
      gs.setWorkingDirectory(gitPath + "/morph");

      String result = gs.diffFile(param.get("workspaceId"), param.get("id"), param.get("source"),
          param.get("target"));
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getDiff e : ", e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/getCommitList", method = RequestMethod.POST)
  public ResponseEntity<?> getCommitList(@RequestBody PosDictionaryEntity param) {
    logger.info("======= call api  POST [[api/dictionary/morph/getCommitList]] =======", param);
    logger.debug("start : {}", param);

    try {
      GitManager gs = new GitManager();
      gs.setWorkingDirectory(gitPath + "/morph");

      List<HashMap<String, Object>> result = gs
          .getCommitList(param.getWorkspaceId(), param.getId());
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getCommitList e : ", e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/get-dictionary-from-git", method = RequestMethod.POST)
  public ResponseEntity<?> getDictionaryByCommit(@RequestBody PosDictionaryEntity param) {
    logger.info("======= call api  POST [[api/dictionary/morph/get-dictionary-from-git]] =======",
        param);
    logger.debug("start : {}", param);
    HashMap<String, Object> result = new HashMap<>();
    try {
      List<PosDictionaryLineEntity> posDictionaryEntities = getPosContentsFromGit(
          param.getWorkspaceId(), param.getId());
      if (posDictionaryEntities.size() > 0) {
        result.put("dictionary_contents", posDictionaryEntities);
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getDictionaryByCommit e : ", e.getMessage());
      return new ResponseEntity<>(HttpStatus.FAILED_DEPENDENCY);
    }
  }

  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/download-dictionary", method = RequestMethod.POST)
  public ResponseEntity<?> downloadDictionary(@RequestBody List<PosDictionaryLineEntity> param) {
    logger
        .info("======= call api  POST [[api/dictionary/morph/download-dictionary]] =======", param);
    logger.debug("start : {}", param);
    HashMap<String, Object> result = new HashMap<>();
    try {
      result.put("dictionary_contents",
          JsonFormat.printer().includingDefaultValueFields().print(entityToProto(param, false)));
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("downloadDictionary e : ", e.getMessage());
      return new ResponseEntity<>(HttpStatus.FAILED_DEPENDENCY);
    }
  }

  @UriRoleDesc(role = "dictionary^C")
  @RequestMapping(value = "/upload-dictionary", method = RequestMethod.POST)
  public ResponseEntity<?> uploadDictionary(@RequestBody ArrayList<String> param) {
    logger.info("======= call api  POST [[api/dictionary/morph/upload-dictionary]] =======", param);
    logger.debug("start : {}", param);
    HashMap<String, Object> result = new HashMap<>();
    try {
      List<PosDictionaryLineEntity> list = jsonStringToEntity(param.get(0));
      if (list.isEmpty()) {
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
      }
      for (PosDictionaryLineEntity elem : list) {
        elem.setVersionId(param.get(1));
        elem.setWorkspaceId(param.get(2));
      }
      posDictionaryLineService.insertLines(list);
      List<PosDictionaryLineEntity> result_list = posDictionaryLineService
          .getDicLines(param.get(1));
      result.put("dictionary_contents", result_list);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("uploadDictionary e : ", e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Transactional
  @Modifying
  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/commit-dictionary", method = RequestMethod.POST)
  public ResponseEntity<?> commitDictionary(@RequestBody PosDictionaryEntity entity) {
    logger
        .info("======= call api  POST [[api/dictionary/morph/commit-dictionary]] =======", entity);
    logger.debug("start : {}", entity);
    HashMap<String, Object> result = new HashMap<>();
    try {
      GitManager gs = new GitManager();
      gs.setWorkingDirectory(gitPath + "/morph");

      String content = "";
      List<PosDictionaryLineEntity> entities = posDictionaryLineService.getDicLines(entity.getId());
//      content = JsonFormat.printer().includingDefaultValueFields()
//          .print(entityToProto(entities, false));
      MorpDictList proto_entity = entityToProto(entities, false);
      content = JsonFormat.printer().omittingInsignificantWhitespace().print(proto_entity);
      HashMap<String, String> value = new HashMap<String, String>();
      value.put("gitPath", gitPath + "/morph");
      value.put("workspaceId", entity.getWorkspaceId());
      value.put("name", entity.getCreatorId());
      value.put("email", "mlt@ai");
      value.put("fileName", entity.getId());
      value.put("content", content);
      value.put("messsage", "");
      gs.updateFile(value);

      // 커밋히스토리에 적재
      CommitHistoryEntity ch = new CommitHistoryEntity();
      ch.setFileId(entity.getId());
      ch.setWorkspaceId(entity.getWorkspaceId());
      ch.setType("MORPH");
      commitHistoryService.insertCommit(ch);

      result.put("entities", "");

      // commit 후 사전데이터 삭제
//      posDictionaryLineService.deleteDicLinesAllByVersionId(entity.getId());
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("commitDictionary e : ", e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  public List<PosDictionaryLineEntity> jsonStringToEntity(String content) {
    MorpDictList.Builder result = MorpDictList.newBuilder();
    List<PosDictionaryLineEntity> entities = new ArrayList<PosDictionaryLineEntity>();
    try {
      if (content.equals("")) {
        return entities;
      }
      JsonFormat.parser().merge(content, result);
      List<MorpDict> list = result.getMorpDictsList();
      int [] cnt_arr  = new int[10];
      for (MorpDict dict : list) {
        switch (dict.getTypeValue()) {
          case MorpDictType.MORP_CUSTOM_VALUE: // 전처리사전
            if (dict.getEntries().getCustomDict().isInitialized()) {
              List<MorpCustomDict> custom_list = dict.getEntries().getCustomDict().getEntriesList();
              for (MorpCustomDict custom_item : custom_list) {
                cnt_arr[dict.getTypeValue()]++;
                PosDictionaryLineEntity entity = new PosDictionaryLineEntity();
                entity.setDicType(dict.getTypeValue());
                entity.setWord((custom_item.getWord() == null) ? "" : custom_item.getWord());
                entity.setDescription(
                    (custom_item.getDescription() == null) ? "" : custom_item.getDescription());
                entity
                    .setPattern((custom_item.getPattern() == null) ? "" : custom_item.getPattern());
                entity.setLineIdx(new Long(cnt_arr[dict.getTypeValue()]));
                entities.add(entity);
              }
            }
            break;
          case MorpDictType.MORP_POST_PATTERN_STR_VALUE: // 후처리사전
//          case MorpDictType.MORP_POST_PATTERN_MORP_VALUE: // 후처리사전
            if (dict.getEntries().getPatternDict().isInitialized()) {
              List<MorpPatternDict> custom_list = dict.getEntries().getPatternDict()
                  .getEntriesList();
              for (MorpPatternDict post_item : custom_list) {
                cnt_arr[dict.getTypeValue()]++;
                PosDictionaryLineEntity entity = new PosDictionaryLineEntity();
                entity.setDicType(dict.getTypeValue());
                entity.setSrcPos((post_item.getSrcPos() == null) ? "" : post_item.getSrcPos());
                entity.setDestPos((post_item.getDestPos() == null) ? "" : post_item.getDestPos());
                entity.setPattern((post_item.getPattern() == null) ? "" : post_item.getPattern());
                entity.setLineIdx(new Long(cnt_arr[dict.getTypeValue()]));
                entities.add(entity);
              }
            }
            break;
          case MorpDictType.MORP_COMPOUND_WORD_VALUE: // 복합사전
            if (dict.getEntries().getCompoundNounDict().isInitialized()) {
              List<MorpCompoundNounDict> custom_list = dict.getEntries().getCompoundNounDict()
                  .getEntriesList();
              for (MorpCompoundNounDict compound_item : custom_list) {
                cnt_arr[dict.getTypeValue()]++;
                PosDictionaryLineEntity entity = new PosDictionaryLineEntity();
                entity.setDicType(dict.getTypeValue());
                entity.setSrcPos(
                    (compound_item.getSrcPos() == null) ? "" : compound_item.getSrcPos());
                entity.setDestPos(
                    (compound_item.getDestPos() == null) ? "" : compound_item.getDestPos());
                entity.setCompoundType(
                    (compound_item.getType() == null) ? "SCAN only" : compound_item.getType());
                entity.setLineIdx(new Long(cnt_arr[dict.getTypeValue()]));
                entities.add(entity);
              }
            }
            break;
          default:
            break;
        }
      }
      return entities;
    } catch (Exception e) {
      logger.error("jsonStringToEntity e : ", e.getMessage());
      return entities;
    }
  }


  public MorpDictList entityToProto(List<PosDictionaryLineEntity> entities, boolean isClear) {
    MorpDictList.Builder list = MorpDictList.newBuilder();
    try {
      MorpDict.Builder morpDict = MorpDict.newBuilder();

      MorpCustomDictList.Builder pre_list = MorpCustomDictList.newBuilder();
      MorpPatternDictList.Builder post_str_list = MorpPatternDictList.newBuilder();
      MorpPatternDictList.Builder post_morp_list = MorpPatternDictList.newBuilder();
      MorpCompoundNounDictList.Builder compound_list = MorpCompoundNounDictList.newBuilder();

      MorpCustomDict.Builder pre_dic = MorpCustomDict.newBuilder();
      MorpPatternDict.Builder post_str_dic = MorpPatternDict.newBuilder();
      MorpPatternDict.Builder post_morp_dic = MorpPatternDict.newBuilder();
      MorpCompoundNounDict.Builder compound_dic = MorpCompoundNounDict.newBuilder();

      if (isClear) { // 사전 초기화용 데이터
        for (PosDictionaryLineEntity item : entities) {
          int type = item.getDicType();
          switch (type) {
            case MorpDictType.MORP_CUSTOM_VALUE: // 전처리사전
              pre_list.addEntries(pre_dic);
              break;
            case MorpDictType.MORP_POST_PATTERN_MORP_VALUE: // 후처리사전
              post_morp_list.addEntries(post_morp_dic);
              break;
            case MorpDictType.MORP_POST_PATTERN_STR_VALUE: // 후처리사전
              post_str_list.addEntries(post_str_dic);
              break;
            case MorpDictType.MORP_COMPOUND_WORD_VALUE: // 복합사전
              compound_list.addEntries(compound_dic);
              break;
            default:
              break;
          }
        }
      } else { // 사전 적용 데이터
        for (PosDictionaryLineEntity item : entities) {
          int type = item.getDicType();
          switch (type) {
            case MorpDictType.MORP_CUSTOM_VALUE: // 전처리사전
              if (item.getWord() == null || item.getPattern() == null) {
                continue;
              }
              pre_dic.setWord(item.getWord());
              pre_dic.setPattern(item.getPattern());
              pre_dic.setDescription(item.getDescription() == null ? "" : item.getDescription());
              pre_list.addEntries(pre_dic);
              break;
            case MorpDictType.MORP_POST_PATTERN_MORP_VALUE: // 후처리사전
              if (item.getSrcPos() == null || item.getDestPos() == null
                  || item.getPattern() == null) {
                continue;
              }
              post_morp_dic.setSrcPos(item.getSrcPos());
              post_morp_dic.setDestPos(item.getDestPos());
              post_morp_dic.setPattern(item.getPattern());
              post_morp_list.addEntries(post_morp_dic);
              break;
            case MorpDictType.MORP_POST_PATTERN_STR_VALUE: // 후처리사전
              if (item.getSrcPos() == null || item.getDestPos() == null
                  || item.getPattern() == null) {
                continue;
              }
              post_str_dic.setSrcPos(item.getSrcPos());
              post_str_dic.setDestPos(item.getDestPos());
              post_str_dic.setPattern(item.getPattern());
              post_str_list.addEntries(post_str_dic);
              break;
            case MorpDictType.MORP_COMPOUND_WORD_VALUE: // 복합사전
              if (item.getSrcPos() == null || item.getDestPos() == null) {
                continue;
              }
              compound_dic.setSrcPos(item.getSrcPos());
              compound_dic.setDestPos(item.getDestPos());
              compound_list.addEntries(compound_dic);
              break;
            default:
              break;
          }
        }
      }

      if (pre_list.getEntriesCount() > 0) {
        morpDict.setType(MorpDictType.MORP_CUSTOM)
            .setEntries(MorpDictEntry.newBuilder().setCustomDict(pre_list).build());
        list.addMorpDicts(morpDict);
      }
      if (post_str_list.getEntriesCount() > 0) {
        morpDict.setType(MorpDictType.MORP_POST_PATTERN_STR)
            .setEntries(MorpDictEntry.newBuilder().setPatternDict(post_str_list).build());
        list.addMorpDicts(morpDict);
      }
      if (post_morp_list.getEntriesCount() > 0) {
        morpDict.setType(MorpDictType.MORP_POST_PATTERN_MORP)
            .setEntries(MorpDictEntry.newBuilder().setPatternDict(post_morp_list).build());
        list.addMorpDicts(morpDict);
      }
      if (compound_list.getEntriesCount() > 0) {
        morpDict.setType(MorpDictType.MORP_COMPOUND_WORD)
            .setEntries(MorpDictEntry.newBuilder().setCompoundNounDict(compound_list).build());
        list.addMorpDicts(morpDict);
      }
      return list.build();
    } catch (Exception e) {
      logger.error("entityToProto e : ", e.getMessage());
      return list.build();
    }
  }

  // 보류
  @UriRoleDesc(role = "dictionary^D")
  @RequestMapping(value = "/delete-dictionary", method = RequestMethod.POST)
  public ResponseEntity<?> deleteDictionary(@RequestBody String id) {
    logger.info("======= call api  POST [[api/dictionary/morph/delete-dictionary]] =======");
    try {
      posDictionaryService.deleteDictionary(id);
      return new ResponseEntity<>(null, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("deleteDictionary e : ", e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * 특정 pos 사전 적용과 테스트 기능
   *
   * @param param checkedList 적용할 사전 종류 리스트, fileList Test 할 File 리스트
   */
  @UriRoleDesc(role = "dictionary^X")
  @RequestMapping(value = "/apply-and-test/{workspaceId}/{dictionaryId}/{testerId}", method = RequestMethod.POST)
  public ResponseEntity<?> testSentenceList(@PathVariable("dictionaryId") String dictionaryId,
      @PathVariable("workspaceId") String workspaceId, @PathVariable("testerId") String testerId,
      @RequestBody HashMap<String, Object> param) {
    logger.info("======= call api  POST [[api/dictionary/morph/apply-and-test]] =======");
    HashMap<String, Object> result = new HashMap<>();
    List<String> resultList = new ArrayList<String>();
    try {
      List<Integer> checkedList = (List<Integer>) param.get("checkedList");
      List<Integer> fileList = (List<Integer>) param.get("fileList");

      if (fileList.size() > 0) { // 문장 test
        // 사전 적용
        applyDicitonary(workspaceId, dictionaryId, checkedList, false);

        // 선택한 파일의 문장 가져오기
        List<String> sentenceList = new ArrayList<String>();
        for (int i = 0; i < fileList.size(); i++) {
          long fileId = fileList.get(i).longValue();
          List<TestSentenceEntity> sentences = testSentenceService
              .getSentencesByFileId(workspaceId, fileId);
          for (TestSentenceEntity entity : sentences) {
            sentenceList.add(entity.getSentence());
          }
        }

        // 사전적용 후 Test 내용 저장
        TestResultEntity testResultEntity = new TestResultEntity();
        testResultEntity.setApplyCheck(checkedList.toString());
        testResultEntity.setFileId(fileList.toString());
        testResultEntity.setCreatedAt(new Date());
        testResultEntity.setDictionaryId(dictionaryId);
        testResultEntity.setTesterId(testerId);
        testResultEntity.setWorkspaceId(workspaceId);
        testResultEntity.setTestType(DictionaryType.MORPH.ordinal());

        // 테스트
        resultList = testDictionary(workspaceId, dictionaryId, sentenceList, testerId,
            testResultEntity);
        result.put("result", resultList);
      } else { // 문장 테스트 안함
        // 사전 초기화
        applyDicitonary(workspaceId, dictionaryId, checkedList, true);
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("testSentenceList e : ", e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/get-dictionaryContents-type/{workspaceId}/{dictionaryId}", method = RequestMethod.POST)
  public ResponseEntity<?> getDictionaryTypeList(@PathVariable("dictionaryId") String dictionaryId,
      @PathVariable("workspaceId") String workspaceId) {
    logger.info(
        "======= call api  POST [[api/dictionary/morph/get-dictionaryContents-type]] =======");
    HashMap<String, Object> result = new HashMap<>();
    List<Integer> types = new ArrayList<Integer>();
    try {
      List<PosDictionaryLineEntity> posDictionaryEntities = posDictionaryLineService
          .getDicLines(dictionaryId);
      if (posDictionaryEntities.isEmpty()) { // DB에 값이 없을 경우 Commit에서 가져오기
        posDictionaryEntities = getPosContentsFromGit(workspaceId, dictionaryId);
        for (PosDictionaryLineEntity elem : posDictionaryEntities) {
          elem.setVersionId(dictionaryId);
          elem.setWorkspaceId(workspaceId);
        }
        posDictionaryLineService.insertLines(posDictionaryEntities);
      }
      types = posDictionaryLineService.getDicTypes(dictionaryId);
      result.put("list", types);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getDictionaryTypeList e : ", e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  public MorpDictList applyDicitonary(String workspaceId, String dictionaryId,
      List<Integer> checkedList, boolean isClear) {
    try {
      TaGrpcInterfaceManager client = new TaGrpcInterfaceManager(brainTaNlp3korIp,
          brainTaNlp3korPort);
      // dictionaryId 와 일치하는 morpheme 사전 데이터 가져오기
      List<PosDictionaryLineEntity> posDictionaryEntities = getContentsFromDbOrGit(dictionaryId,
          workspaceId);

      // pos dictionary 데이터 중 checkedTypeList에 해당하는 타입만 가져온다.
      posDictionaryEntities = posDictionaryEntities.stream()
          .filter(entity -> checkedList.stream().anyMatch(type -> type == entity.getDicType()))
          .collect(Collectors.toList());
      MorpDictList list = entityToProto(posDictionaryEntities, isClear);
      client.updatePosDict(list);
      return list;
    } catch (Exception e) {
      MorpDictList list = MorpDictList.newBuilder().build();
      logger.error("applyDicitonary e : ", e.getMessage());
      return list;
    }
  }

  public List<String> testDictionary(String workspaceId, String dictionaryId,
      List<String> sentenceList, String testerId, TestResultEntity testResultEntity) {
    List<String> resultList = new ArrayList<String>();
    try {
      long start = System.currentTimeMillis();
      TaGrpcInterfaceManager client = new TaGrpcInterfaceManager(brainTaNlp3korIp,
          brainTaNlp3korPort);

      // pos 사전 컨텐츠 가져오기
      List<PosDictionaryLineEntity> posDictionaryEntities = getContentsFromDbOrGit(dictionaryId,
          workspaceId);

      WorkspaceEntity workspaceEntity = workspaceService.getWorkspace(workspaceId);

      // 문장 별 테스트
      for (String line : sentenceList) {
        line = line.trim();
        InputText.Builder inputText = InputText.newBuilder();
        // Lang은 일단 항상 한국어로만 사용하도록 코딩
        // inputText.setLang(LangCode.valueOf(workspaceEntity.getLangCode()));
        inputText.setLang(LangCode.kor);
        inputText.setSplitSentence(true);
        inputText.setText(line);
        Document document = client.analyze(inputText.build());
        if (!document.getSentencesList().isEmpty()) {
          for (Sentence sentence : document.getSentencesList()) {
            MorphResult morphResult = new MorphResult(sentence.getText());
            String taggedStr = "";
            String noramlStr = "";
            for (MorphemeEval morph : sentence.getMorphEvalsList()) {
              if (noramlStr != "") {
                noramlStr += ' ';
                taggedStr += '+';
              }
              noramlStr += morph.getResult();
              taggedStr += morph.getResult();
            }
            for (PosDictionaryLineEntity pos : posDictionaryEntities) {
              if (pos.getDicType() == MorpDictType.MORP_CUSTOM_VALUE) {
                if (pos.getDescription() == null || pos.getDescription() == "") {
                  continue;
                }
                if (taggedStr.contains(pos.getDescription())) {
                  //매칭되는 모든 결과를 보여주기위해 주석처리.
                  //noramlStr = noramlStr.replaceAll(pos.getDescription(), "");
                  //taggedStr = taggedStr.replaceAll(pos.getDescription(), "");
                  morphResult.put(pos.getDescription());
                }
              } else {
                if (noramlStr.contains(pos.getDestPos())) {
                  //매칭되는 모든 결과를 보여주기위해 주석처리.
                  //taggedStr = taggedStr.replaceAll(pos.getDestPos(), "");
                  //noramlStr = noramlStr.replaceAll(pos.getDestPos().replaceAll("\\+", " "), "");
                  morphResult.put(pos.getDestPos());
                }
              }
              if (taggedStr == "" || taggedStr == " ") {
                continue;
              }
            }
            resultList.add(morphResult.toString());
          }
        }
      }

      // 테스트 결과 저장
      long testId = testResultService.insertTestResult(testResultEntity);
      // 테스트 세부 결과 저장
      if (testId > -1 && testerId != null) {
        TestResultDetailEntity entity = new TestResultDetailEntity();
        entity.setCreatedAt(new Date());
        entity.setTestId(testId);
        entity.setDictionaryId(dictionaryId);
        entity.setTesterId(testerId);
        entity.setWorkspaceId(workspaceId);
        entity.setTestResult(resultList.toString());
        testResultService.insertTestResultDetail(entity);
      }
      logger.trace("morpheme dictionary test, sentence:[" + resultList.size() + "]/sentence:["
          + posDictionaryEntities.size() + "]/ time:[" + (System.currentTimeMillis() - start)
          + "]");
      return resultList;
    } catch (Exception e) {
      logger.error("testDictionary e : ", e.getMessage());
      throw new RuntimeException();
    }
  }

  public List<PosDictionaryLineEntity> getPosContentsFromGit(String wid, String id) {
    try {
      GitManager gs = new GitManager();
      gs.setWorkingDirectory(gitPath + "/morph");
      List<HashMap<String, Object>> list = gs.getCommitList(wid, id);

      Date last_time = null;
      HashMap<String, Object> last_item = new HashMap<String, Object>();
      boolean first = true;
      for (HashMap<String, Object> item : list) {
        if (first) {
          last_time = (Date) item.get("commitTime");
          last_item = item;
          first = false;
        }
        if (last_time.compareTo((Date) item.get("commitTime")) < 0) {
          last_time = (Date) item.get("commitTime");
          last_item = item;
        }
      }
      String contents = null;
      if (last_item.size() > 0) {
        contents = gs
            .readFileFromCommit(wid, id, ObjectId.fromString(last_item.get("hashKey").toString()));
      }
      return jsonStringToEntity(contents);
    } catch (Exception e) {
      logger.error("getPosContentsFromGit e : ", e.getMessage());
      return new ArrayList<PosDictionaryLineEntity>();
    }
  }

  private List<PosDictionaryLineEntity> getContentsFromDbOrGit(String id, String workspace) {
    List<PosDictionaryLineEntity> posDictionaryEntities = new ArrayList<PosDictionaryLineEntity>();
    try {
      posDictionaryEntities = posDictionaryLineService.getDicLines(id);
      if (posDictionaryEntities.isEmpty()) { // DB에 값이 없을 경우 Commit에서 가져오기
        posDictionaryEntities = getPosContentsFromGit(workspace, id);
      }
      return posDictionaryEntities;
    } catch (Exception e) {
      logger.error("getContentsFromDbOrGit e : ", e.getMessage());
      return posDictionaryEntities;
    }
  }

  private Page<PosDictionaryLineEntity> getContentsFromDbOrGitWithPage(PosDictionaryLineEntity entity) {
    List<PosDictionaryLineEntity> tempEntities = new ArrayList<>();
    try {
      Page<PosDictionaryLineEntity> posDictionaryEntities = posDictionaryLineService
          .getDicLinesWithPage(entity.getVersionId(), entity.getDicType(), entity.getPageRequest());

      if (posDictionaryEntities.getNumberOfElements() < 1) { // DB에 값이 없을 경우 Commit에서 가져오기
        tempEntities = getPosContentsFromGit(entity.getWorkspaceId(),
            entity.getId());
        List<PosDictionaryLineEntity> typeEntities = tempEntities.stream().filter(x -> x.getDicType() == entity.getDicType()).collect(
            Collectors.toList());
        posDictionaryEntities = new PageImpl<PosDictionaryLineEntity>(typeEntities, entity.getPageRequest(), typeEntities.size());
      }
      return posDictionaryEntities;
    } catch (Exception e) {
      logger.error("getContentsFromDbOrGitWithPage e : ", e.getMessage());
      tempEntities = new ArrayList<>();
      Page<PosDictionaryLineEntity> res = new PageImpl<PosDictionaryLineEntity>(
          tempEntities, entity.getPageRequest(), tempEntities.size());
      return res;
    }
  }

  @UriRoleDesc(role = "dictionary^R")
  @RequestMapping(value = "/get-nlp-resource", method = RequestMethod.POST)
  public ResponseEntity<?> getNlpResource(@RequestBody ArrayList<String> param) {
    logger.info("======= call api  POST [[api/dictionary/morph/get-nlp-resource]] =======");
    HashMap<String, List<PosDictionaryLineEntity>> result = new HashMap<>();
    List<PosDictionaryLineEntity> posDictionaryEntities = new ArrayList<>();
    String id = param.get(0);
    String workspaceId = param.get(1);

    try {
      //  파일읽어오기
      String dir = fileUtils.getSystemConfPath(morpDictPath);
      String morph_dic = "morph_dic_custom.txt";
      String post_reg_str = "post_pattern.regex.string.txt";
      String post_reg_mor = "post_pattern.regex.morp.txt";
      String compnoun = "compnoun_dic.txt";
      Pattern pattern = Pattern.compile("'(.*?)\"");
      if (dir != "") {
        File file = new File(dir + morph_dic);
        if (file.exists()) {
          BufferedReader inFile = new BufferedReader(new FileReader(file));
          String line = null;
          long idx = 1;
          while ((line = inFile.readLine()) != null) {
            if (line.contains("\t")) {
              PosDictionaryLineEntity entity = new PosDictionaryLineEntity();
              String[] data = line.split("\t");
              entity.setDicType(MorpDictType.MORP_CUSTOM.ordinal());
              entity.setCreatedAt(new Date());
              entity.setVersionId(id);
              entity.setWorkspaceId(workspaceId);
              entity.setWord(data[0]);
              entity.setPattern(data[1]);
              if (data.length > 2) {
                String desc = data[2];
                String[] descs1 = desc.split("\\[");
                String[] descs2 = descs1[1].split("\\]");
                entity.setDescription(descs2[0]);
              } else {
                entity.setDescription("");
              }
              entity.setLineIdx(idx);
              posDictionaryEntities.add(entity);
              idx++;
            }
          }
          inFile.close();
        }

        File file2 = new File(dir + post_reg_str);
        if (file2.exists()) {
          BufferedReader inFile = new BufferedReader(new FileReader(file2));
          String line = null;
          long idx = 1;
          while ((line = inFile.readLine()) != null) {
            if (line.contains("\t")) {
              PosDictionaryLineEntity entity = new PosDictionaryLineEntity();

//              ArrayList<String> data = new ArrayList<>();
              String [] data = line.split("\t");
              entity.setDicType(MorpDictType.MORP_POST_PATTERN_STR.ordinal());
              entity.setCreatedAt(new Date());
              entity.setVersionId(id);
              entity.setWorkspaceId(workspaceId);
              entity.setSrcPos(data[1]);
              entity.setDestPos(data[2]);
              String tmp_pattern = "";
              // pattern은 탭으로 이어짐.
              // (curr)(하지|하지\?)$	(next)NIL
              tmp_pattern = data[3];
              for (int i = 4; i < data.length; i++) {
                tmp_pattern += ("\t" + data[i]);
              }
              entity.setPattern(tmp_pattern);
              entity.setLineIdx(idx);
              posDictionaryEntities.add(entity);
              idx++;
            }
          }
          inFile.close();
        }
        File file3 = new File(dir + post_reg_mor);
        if (file3.exists()) {
          BufferedReader inFile = new BufferedReader(new FileReader(file3));
          String line = null;
          long idx = 1;
          while ((line = inFile.readLine()) != null) {
            if (line.contains("\t")) {
              PosDictionaryLineEntity entity = new PosDictionaryLineEntity();
              String[] data = line.split("\t");
              entity.setDicType(MorpDictType.MORP_POST_PATTERN_MORP.ordinal());
              entity.setCreatedAt(new Date());
              entity.setVersionId(id);
              entity.setWorkspaceId(workspaceId);
              entity.setSrcPos(data[1]);
              entity.setDestPos(data[2]);
              entity.setLineIdx(idx);
              posDictionaryEntities.add(entity);
              idx++;
            }
          }
        }

        File file4 = new File(dir + compnoun);
        if (file4.exists()) {
          BufferedReader inFile = new BufferedReader(new FileReader(file4));
          String line = null;
          long idx = 1;
          while ((line = inFile.readLine()) != null) {
            if (line.contains("\t")) {
              PosDictionaryLineEntity entity = new PosDictionaryLineEntity();
              String[] data = line.split("\t");
              entity.setDicType(MorpDictType.MORP_COMPOUND_WORD.ordinal());
              entity.setCreatedAt(new Date());
              entity.setVersionId(id);
              entity.setWorkspaceId(workspaceId);
              entity.setSrcPos(data[0]);
              entity.setDestPos(data[1]);
              entity.setCompoundType(data[2]);
              entity.setLineIdx(idx);
              posDictionaryEntities.add(entity);
              idx++;
            }
          }
          inFile.close();
        }
      }

      if (!posDictionaryEntities.isEmpty()) {
        logger.info("end compnoun dictionary !");
        posDictionaryLineService.deleteDicLinesAllByVersionId(id);
        result.put("dictionary_contents", posDictionaryEntities);
        posDictionaryLineService.insertLinesWithDate(posDictionaryEntities);
      }
      logger.info("end put dictionary !");
      HashMap<String, Object> obj = new HashMap<>();
      obj.put("dictionary_contents","success");
      return new ResponseEntity<>(obj, HttpStatus.OK);

    } catch (Exception e) {
      logger.error("get Nlp pos dictionary data e : ", e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
