package ai.maum.mlt.dictionary.entity;

import ai.maum.mlt.common.pagenation.PageParameters;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "MAI_REPLACEMENT_DICTIONARY_TYPE")
public class ReplacementDictionaryTypeEntity extends PageParameters implements Serializable {

  @Id
  @Column(name = "NAME", length = 10, nullable = false)
  private String name;

  @Column(name = "USE_YN", length = 1, nullable = false)
  private String useYn;
}
