package ai.maum.mlt.dictionary.entity;

import ai.maum.mlt.common.auth.entity.WorkspaceEntity;
import ai.maum.mlt.common.pagenation.PageParameters;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "MAI_POS_DICTIONARY_LINE")
public class PosDictionaryLineEntity extends PageParameters implements Serializable {

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid")
  @Column(name = "ID", length = 40)
  private String id;

  @Column(name = "WORKSPACE_ID", length = 40, nullable = false)
  private String workspaceId;

  @Column(name = "DIC_TYPE", length = 40, nullable = false)
  private int dicType;

  @Column(name = "WORD", length = 1000)
  private String word;

  @Column(name = "PATTERN", length = 1000)
  private String pattern;

  @Column(name = "SRC_POS", length = 1000)
  private String srcPos;

  @Column(name = "DEST_POS", length = 1000)
  private String destPos;

  @Column(name = "COMPOUND_TYPE", length = 100)
  private String compoundType;

  @Column(name = "DESCRIPTION", length = 1000)
  private String description;

  @Column(name = "VERSION_ID", length = 40, nullable = false)
  private String versionId;

  @Column(name = "LINE_IDX")
  private Long lineIdx;

  @Column(name = "CREATED_AT")
  private Date createdAt;

  @ManyToOne
  @JoinColumn(name = "WORKSPACE_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private WorkspaceEntity workspaceEntity;
}
