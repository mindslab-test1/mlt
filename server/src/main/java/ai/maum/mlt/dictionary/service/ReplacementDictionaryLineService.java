package ai.maum.mlt.dictionary.service;

import ai.maum.mlt.dictionary.entity.ReplacementDictionaryLineEntity;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ReplacementDictionaryLineService {
    List<ReplacementDictionaryLineEntity> getDicLines(String versionId);
    void deleteDicLines(List<ReplacementDictionaryLineEntity> replacementDictionaryEntities);
    void insertLines(List<ReplacementDictionaryLineEntity> replacementDictionaryEntities);
    void insertLinesWithDate(List<ReplacementDictionaryLineEntity> replacementDictionaryEntities);
    int updateLine(ReplacementDictionaryLineEntity entity);
    ReplacementDictionaryLineEntity insertLine(ReplacementDictionaryLineEntity entity);
    List<ReplacementDictionaryLineEntity> getList();
    void deleteDicLinesAllByVersionId(String versionId);
    List<Integer> getDicTypes(String dictionaryId);
    ReplacementDictionaryLineEntity getDicLineEntity(String srcStr);
    int getCntDicLines(String versionId);
    Page<ReplacementDictionaryLineEntity> getDicLinesWithPage(String versionId, Pageable pageable);
    void deleteDicLinesWithIdx(List<ReplacementDictionaryLineEntity> posDictionaryEntities);
    int updateIdx(ReplacementDictionaryLineEntity entity);
    int updateIdxMinus(ReplacementDictionaryLineEntity entity);
    List<String> getDictionaryTypeList();
}
