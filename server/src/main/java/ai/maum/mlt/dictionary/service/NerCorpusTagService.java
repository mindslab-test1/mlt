package ai.maum.mlt.dictionary.service;

import ai.maum.mlt.dictionary.entity.NerCorpusTagEntity;
import java.util.List;

public interface NerCorpusTagService {
    List<NerCorpusTagEntity> getList();
    void insertTags(List<NerCorpusTagEntity> nerCorpusTagEntities);
    void deleteAll();
}
