package ai.maum.mlt.dictionary.service;

import ai.maum.mlt.dictionary.entity.TestSentenceEntity;
import java.util.List;

public interface TestSentenceService {

  enum DictionaryType {
    NER, MORPH
  }

  List<TestSentenceEntity> getSentences(String workspaceId, TestSentenceService.DictionaryType type);

  List<TestSentenceEntity> getSentencesByFileId(String workspaceId, long fileId);

  void deleteSentence(String workspaceId, long id);

  void deleteSentences(String workspaceId, List<Integer> id);
}
