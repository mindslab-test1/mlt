package ai.maum.mlt.dictionary.service;

import ai.maum.mlt.dictionary.entity.ReplacementDictionaryLineEntity;
import ai.maum.mlt.dictionary.repository.ReplacementDictionaryLineRepository;
import ai.maum.mlt.dictionary.repository.ReplacementDictionaryTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Service
public class ReplacementDictionaryLineServiceImpl implements ReplacementDictionaryLineService {

  @Autowired
  ReplacementDictionaryLineRepository replacementDictionaryLineRepository;

  @Autowired
  ReplacementDictionaryTypeRepository replacementDictionaryTypeRepository;

  @Override
  public List<ReplacementDictionaryLineEntity> getDicLines(String versionId) {
    return replacementDictionaryLineRepository.findAllByVersionId(versionId);
  }

  @Transactional
  @Override
  public void insertLines(List<ReplacementDictionaryLineEntity> replacementDictionaryEntities) {
    for (ReplacementDictionaryLineEntity entity : replacementDictionaryEntities) {
      if (entity.getCreatedAt() == null) {
        entity.setCreatedAt(new Date());
      }
      replacementDictionaryLineRepository.save(entity);
    }
    return;
  }
  @Transactional
  @Override
  public void insertLinesWithDate(List<ReplacementDictionaryLineEntity> replacementDictionaryEntities) {
    replacementDictionaryLineRepository.save(replacementDictionaryEntities);
    return;
  }
  @Transactional
  @Override
  public ReplacementDictionaryLineEntity insertLine(ReplacementDictionaryLineEntity entity) {
    entity.setCreatedAt(new Date());
    return replacementDictionaryLineRepository.save(entity);
  }

  @Transactional
  @Override
  public int updateLine(ReplacementDictionaryLineEntity entity) {
    entity.setCreatedAt(new Date());
    return replacementDictionaryLineRepository.updateById(entity.getSrcStr(), entity.getDestStr(),
        entity.getType(), entity.getDescription(), entity.getCreatedAt(), entity.getId());
  }

  @Transactional
  @Override
  public void deleteDicLines(List<ReplacementDictionaryLineEntity> nerDictionaryEntities) {
    for (ReplacementDictionaryLineEntity entity : nerDictionaryEntities) {
      replacementDictionaryLineRepository.deleteByIdAndVersionId(entity.getId(), entity.getVersionId());
    }
    return;
  }

  @Override
  public List<ReplacementDictionaryLineEntity> getList() {
    return replacementDictionaryLineRepository.findAll();
  }

  @Transactional
  @Modifying
  @Override
  public void deleteDicLinesAllByVersionId(String versionId) {
    replacementDictionaryLineRepository.deleteAllByVersionId(versionId);
  }

  @Override
  public List<Integer> getDicTypes(String versionId) {
    List<Integer> list = replacementDictionaryLineRepository.getDistinctTypeByVersionId(versionId);
    return list;
  }

  @Override
  public ReplacementDictionaryLineEntity getDicLineEntity(String srcStr) {
    ReplacementDictionaryLineEntity res = replacementDictionaryLineRepository.findBysrcStr(srcStr);
    return res;
  }

  @Override
  public List<String> getDictionaryTypeList() {
    List<String> list = replacementDictionaryTypeRepository.findAllByUseYn();
    return list;
  }

  @Override
  public int getCntDicLines(String versionId) {
    return replacementDictionaryLineRepository.countAllByVersionId(versionId);
  }

  @Override
  public Page<ReplacementDictionaryLineEntity> getDicLinesWithPage(String versionId, Pageable pageable) {
    return replacementDictionaryLineRepository.findPageAllByVersionId(versionId, pageable);
  }

  @Transactional
  @Override
  public void deleteDicLinesWithIdx(List<ReplacementDictionaryLineEntity> nerDictionaryLineEntities) {
    for (ReplacementDictionaryLineEntity entity : nerDictionaryLineEntities) {
      replacementDictionaryLineRepository.deleteByIdAndVersionId(entity.getId(), entity.getVersionId());
      replacementDictionaryLineRepository
          .updateLineIdxMinusByLindIdxAndVersionId(entity.getLineIdx(), entity.getVersionId());
    }
    return;
  }

  @Transactional
  @Override
  public int updateIdx(ReplacementDictionaryLineEntity entity) {
    int res = 0;
    res = replacementDictionaryLineRepository
        .updateLineIdxByLindIdxAndVersionId(entity.getLineIdx(), entity.getVersionId());
    return res;
  }

  @Transactional
  @Override
  public int updateIdxMinus(ReplacementDictionaryLineEntity entity) {
    int res = 0;
    res = replacementDictionaryLineRepository
        .updateLineIdxMinusByLindIdxAndVersionId(entity.getLineIdx(), entity.getVersionId());
    return res;
  }
}
