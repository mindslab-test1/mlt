package ai.maum.mlt.dictionary.service;

import ai.maum.mlt.dictionary.entity.PosDictionaryEntity;
import java.util.List;

public interface PosDictionaryService {
    List<PosDictionaryEntity> getDictionaries();
    List<PosDictionaryEntity> getDictionaries(String wid);
    String getDictionaryName(String id);
    PosDictionaryEntity getDictionary(String name);
    String getDictionaryId(String name);
    PosDictionaryEntity insertDictionary(PosDictionaryEntity entity);
    int updateDictionary(PosDictionaryEntity entity);
    void deleteDictionaries(List<String> idList);
    void deleteDictionary(String id);
}
