package ai.maum.mlt.dictionary.repository;

import ai.maum.mlt.dictionary.entity.NerDictionaryLineEntity;
import ai.maum.mlt.dictionary.entity.TestFileEntity;
import java.util.Date;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface TestFileRepository extends JpaRepository<TestFileEntity, String> {

//  @Query(value = "SELECT d "
//      + "         FROM TestFileEntity d "
//      + "         WHERE d.workspaceId = :workspaceId "
//      + "         AND d.testType = :testType "
//      + "         ORDER BY d.createdAt DESC")
//  List<TestFileEntity> findAllByTestTypeAndWorkspaceId(@Param("workspaceId") String workspaceId, @Param("testType") int testType);

//  @Modifying
//  @Transactional
//  @Query(value = "DELETE "
//      + "         FROM TestFileEntity d "
//      + "         WHERE d.workspaceId = :workspaceId "
//      + "         AND d.testType = :testType "
//      + "         AND d.id = :id ")
//  int deleteByIdAndWorkspaceIdAndTestType(
//      @Param("workspaceId") String workspaceId, @Param("testType") int testType,
//      @Param("id") long id);


  @Query(value = "SELECT d.fileId "
      + "         FROM TestFileEntity d "
      + "         WHERE d.id in :ids "
      + "         ORDER BY d.createdAt DESC")
  List<String> findFileIdsById(@Param("ids") List<Long> ids);

  @Query(value = "SELECT d.fileId "
      + "         FROM TestFileEntity d "
      + "         WHERE d.id = :ids "
      + "         ORDER BY d.createdAt DESC")
  String findFileIdById(@Param("ids") long id);


  List<TestFileEntity> findAllByWorkspaceIdAndTestType(String workspaceId, int testType);

  @Modifying
  @Transactional
  int deleteByIdAndWorkspaceIdAndTestType(long id, String workspaceId, int testType);
}
