package ai.maum.mlt.dictionary.repository;

import ai.maum.mlt.dictionary.entity.PosDictionaryEntity;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface PosDictionaryRepository extends JpaRepository<PosDictionaryEntity, String> {

  @Query(value = "SELECT d "
      + "         FROM PosDictionaryEntity d"
      + "         WHERE d.name= :name "
      + "         ORDER BY d.createdAt DESC")
  List<PosDictionaryEntity> findAllByName(@Param("name") String name);

  @Query(value = "SELECT d.id "
      + "         FROM PosDictionaryEntity d"
      + "         WHERE d.name= :name "
      + "         ORDER BY d.createdAt DESC")
  List<String> findIdByName(@Param("name") String name);

  @Query(value = "SELECT d.name "
      + "         FROM PosDictionaryEntity d"
      + "         WHERE d.id= :id "
      + "         ORDER BY d.createdAt DESC")
  String findNameById(@Param("id") String id);

  @Query(value = "SELECT d.id as id, d.workspaceId as workspaceId, d.createdAt as createdAt, d.description as description,"
      + " d.name as name, d.creatorId  as creatorId"
      + "         FROM PosDictionaryEntity d"
      + "         WHERE d.workspaceId= :wid "
      + "         ORDER BY d.createdAt DESC")
  List<Map> findDictionaryWorkSpaceId(@Param("wid") String wid);

  @Modifying
  @Transactional
  @Query(value = "UPDATE  PosDictionaryEntity d "
      + " SET "
      + " d.description = :description"
      + " WHERE d.id = :id ")
  int updateDescription( @Param("id") String id,
      @Param("description") String description);

  void deleteById(String Id);
}
