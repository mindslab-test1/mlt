package ai.maum.mlt.dictionary.repository;

import ai.maum.mlt.dictionary.entity.TestFileEntity;
import ai.maum.mlt.dictionary.entity.TestResultDetailEntity;
import ai.maum.mlt.dictionary.entity.TestResultEntity;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface TestResultDetailRepository extends JpaRepository<TestResultDetailEntity, String> {

  TestResultDetailEntity findTopByWorkspaceIdAndTestId(String workspaceId, long testId);

  @Modifying
  @Transactional
  int deleteTestResultDetailEntitiesById(long id);

  @Modifying
  @Transactional
  int deleteTestResultDetailEntitiesByTestId(long testId);
}
