package ai.maum.mlt.dictionary.repository;

import ai.maum.mlt.dictionary.entity.ReplacementDictionaryTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface ReplacementDictionaryTypeRepository extends JpaRepository<ReplacementDictionaryTypeEntity, String> {

  @Query(value = "SELECT d.name "
      + "         FROM ReplacementDictionaryTypeEntity d "
      + "         WHERE d.useYn like 'Y' "
      + "         ORDER BY d.name DESC")
  List<String> findAllByUseYn();

}
