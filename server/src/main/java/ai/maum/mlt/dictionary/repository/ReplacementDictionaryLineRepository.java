package ai.maum.mlt.dictionary.repository;

import ai.maum.mlt.dictionary.entity.ReplacementDictionaryLineEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface ReplacementDictionaryLineRepository extends JpaRepository<ReplacementDictionaryLineEntity, String> {

  ReplacementDictionaryLineEntity findBysrcStr(String srcStr);

  @Query(value = "SELECT d "
      + "         FROM ReplacementDictionaryLineEntity d "
      + "         WHERE d.versionId = :versionId "
      + "         ORDER BY d.lineIdx ")
  List<ReplacementDictionaryLineEntity> findAllByVersionId(
          @Param("versionId") String versionId
  );

  @Modifying
  @Transactional
  @Query(value = "DELETE "
      + "         FROM ReplacementDictionaryLineEntity d "
      + "         WHERE d.versionId = :versionId "
      + "         AND d.id = :id ")
  void deleteByIdAndVersionId(
          @Param("id") String id,
          @Param("versionId") String versionId
  );

  @Modifying
  @Transactional
  @Query(value = "DELETE "
      + "         FROM ReplacementDictionaryLineEntity d "
      + "         WHERE d.versionId = :versionId ")
  void deleteAllByVersionId(
          @Param("versionId") String versionId
  );

  @Modifying
  @Transactional
  @Query(value = "UPDATE ReplacementDictionaryLineEntity d "
          + " SET "
          + " d.srcStr = :srcStr ,"
          + " d.destStr = :destStr,"
          + " d.type = :type,"
          + " d.description = :description,"
          + " d.createdAt = :createdAt"
          + "         WHERE d.id = :id ")
  int updateById(
          @Param("srcStr") String srcStr,
          @Param("destStr") String destStr,
          @Param("type") String type,
          @Param("description") String description,
          @Param("createdAt") Date createdAt,
          @Param("id") String id
  );

  void deleteById(String Id);

  @Query(value = "SELECT distinct d.type "
      + "         FROM ReplacementDictionaryLineEntity d "
      + "         WHERE d.versionId = :versionId "
      + "         ORDER BY d.lineIdx ")
  List<Integer> getDistinctTypeByVersionId(@Param("versionId") String versionId);

  @Query(value = "SELECT d "
      + "         FROM ReplacementDictionaryLineEntity d "
      + "         WHERE d.versionId = :versionId "
      + "         ORDER BY d.lineIdx ")
  Page<ReplacementDictionaryLineEntity> findPageAllByVersionId(
      @Param("versionId") String versionId, Pageable pageable
  );

  @Query(value = "SELECT count(d) "
      + "         FROM ReplacementDictionaryLineEntity d "
      + "         WHERE d.versionId = :versionId "
      + "         ORDER BY d.lineIdx ")
  int countAllByVersionId(
      @Param("versionId") String versionId);

  @Modifying
  @Transactional
  @Query(value = "UPDATE  ReplacementDictionaryLineEntity d "
      + " SET "
      + " d.lineIdx = d.lineIdx+1 "
      + "         WHERE d.versionId = :versionId "
      + "         AND d.lineIdx >= :lineIdx ")
  int updateLineIdxByLindIdxAndVersionId(
      @Param("lineIdx") long lineIdx,
      @Param("versionId") String versionId
  );

  @Modifying
  @Transactional
  @Query(value = "UPDATE  ReplacementDictionaryLineEntity d "
      + " SET "
      + " d.lineIdx = d.lineIdx-1 "
      + "         WHERE d.versionId = :versionId "
      + "         AND d.lineIdx > :lineIdx ")
  int updateLineIdxMinusByLindIdxAndVersionId(
      @Param("lineIdx") long lineIdx,
      @Param("versionId") String versionId
  );
}
