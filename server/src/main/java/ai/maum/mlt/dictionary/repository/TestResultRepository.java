package ai.maum.mlt.dictionary.repository;

import ai.maum.mlt.dictionary.entity.TestFileEntity;
import ai.maum.mlt.dictionary.entity.TestResultEntity;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface TestResultRepository extends JpaRepository<TestResultEntity, String> {

  List<TestResultEntity> findAllByWorkspaceIdAndDictionaryId(String workspaceId, String dictionaryId);

  List<TestResultEntity> findAllByWorkspaceIdAndTestTypeAndDictionaryId(String workspaceId, int type, String dictionaryId);

  List<TestResultEntity> findAllByWorkspaceIdAndTestType(String workspaceId, int testType);

  @Modifying
  @Transactional
  int deleteTestResultEntityById(long id);
}
