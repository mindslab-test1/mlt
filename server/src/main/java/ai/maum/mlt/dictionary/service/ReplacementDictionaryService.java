package ai.maum.mlt.dictionary.service;

import ai.maum.mlt.dictionary.entity.ReplacementDictionaryEntity;

import java.util.List;

public interface ReplacementDictionaryService {
    List<ReplacementDictionaryEntity> getDictionaries();
    String getDictionaryName(String id);
    ReplacementDictionaryEntity getDictionary(String name);
    String getDictionaryId(String name);
    ReplacementDictionaryEntity insertDictionary(ReplacementDictionaryEntity entity);
    int updateDictionary(ReplacementDictionaryEntity entity);
    void deleteDictionaries(List<String> idList);
    void deleteDictionary(String id);
}
