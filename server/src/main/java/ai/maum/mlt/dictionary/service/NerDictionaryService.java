package ai.maum.mlt.dictionary.service;

import ai.maum.mlt.dictionary.entity.NerDictionaryEntity;
import java.util.List;

public interface NerDictionaryService {
    List<NerDictionaryEntity> getDictionaries();
    String getDictionaryName(String id);
    NerDictionaryEntity getDictionary(String name);
    String getDictionaryId(String name);
    NerDictionaryEntity insertDictionary(NerDictionaryEntity entity);
    int updateDictionary(NerDictionaryEntity entity);
    void deleteDictionaries(List<String> idList);
    void deleteDictionary(String id);
}
