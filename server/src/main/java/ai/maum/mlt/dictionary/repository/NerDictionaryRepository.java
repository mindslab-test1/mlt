package ai.maum.mlt.dictionary.repository;

import ai.maum.mlt.dictionary.entity.NerDictionaryEntity;
import java.util.Date;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface NerDictionaryRepository extends JpaRepository<NerDictionaryEntity, String> {

  @Query(value = "SELECT d "
      + "         FROM NerDictionaryEntity d"
      + "         WHERE d.name= :name "
      + "         ORDER BY d.createdAt DESC")
  List<NerDictionaryEntity> findAllByName(@Param("name") String name);

  @Query(value = "SELECT d.id "
      + "         FROM NerDictionaryEntity d"
      + "         WHERE d.name= :name "
      + "         ORDER BY d.createdAt DESC")
  List<String> findIdByName(@Param("name") String name);

  @Query(value = "SELECT d.name "
      + "         FROM NerDictionaryEntity d"
      + "         WHERE d.id= :id "
      + "         ORDER BY d.createdAt DESC")
  String findNameById(@Param("id") String id);

  @Modifying
  @Transactional
  @Query(value = "UPDATE  NerDictionaryEntity d "
      + " SET "
      + " d.description = :description"
      + " WHERE d.id = :id ")
  int updateDescription( @Param("id") String id,
      @Param("description") String description);

  void deleteById(String Id);
}
