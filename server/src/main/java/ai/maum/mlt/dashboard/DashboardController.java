package ai.maum.mlt.dashboard;

import ai.maum.mlt.basicqa.entity.BasicQAIndexingHistoryEntity;
import ai.maum.mlt.basicqa.service.BasicQAIndexingHistoryService;
import ai.maum.mlt.basicqa.service.BasicQASkillService;
import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.common.file.entity.FileEntity;
import ai.maum.mlt.common.file.entity.FileGroupEntity;
import ai.maum.mlt.common.file.service.FileGroupService;
import ai.maum.mlt.common.file.service.FileService;
import ai.maum.mlt.common.system.SystemCode;
import ai.maum.mlt.itfc.BasicqaGrpcInterfaceManager;
import ai.maum.mlt.itfc.SttGrpcInterfaceManager;
import ai.maum.mlt.itfc.TaGrpcInterfaceManager;
import ai.maum.mlt.management.entity.HistoryEntity;
import ai.maum.mlt.management.service.HistoryService;
import ai.maum.mlt.stt.entity.STTModelEntity;
import ai.maum.mlt.stt.service.STTModelService;
import ai.maum.mlt.stt.service.STTTranscriptService;
import ai.maum.mlt.ta.dnn.entity.DNNDicEntity;
import ai.maum.mlt.ta.dnn.entity.DNNTrainingEntity;
import ai.maum.mlt.ta.dnn.service.DNNDicService;
import ai.maum.mlt.ta.dnn.service.DNNTrainingService;
import ai.maum.mlt.ta.hmd.entity.HMDDicEntity;
import ai.maum.mlt.ta.hmd.service.HMDDicService;
import com.google.protobuf.Empty;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import maum.brain.cl.train.Cltrainer.ClTrainStatus;
import maum.brain.cl.train.Cltrainer.ClTrainStatusList;
import maum.brain.stt.train.S3Train.TrainKey;
import maum.brain.stt.train.S3Train.TrainResult;
import maum.brain.stt.train.S3Train.TrainStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/dashboard2")
public class DashboardController {

  static final Logger logger = LoggerFactory.getLogger(DashboardController.class);

  @Value("${ta.train.server.ip}")
  private String taTrainIp;
  @Value("${ta.train.server.port}")
  private int taTrainPort;

  @Value("${stt.train.server.ip}")
  private String sttTrainIp;
  @Value("${stt.train.server.port}")
  private int sttTrainPort;

  @Value("${bsqa.server.ip}")
  private String bsqaIp;
  @Value("${bsqa.server.port}")
  private int bsqaPort;


  @Value("${mrc.train.server.ip}")
  private String mrcTrainIp;
  @Value("${mrc.train.server.port}")
  private int mrcTrainPort;


  @Autowired
  private FileGroupService fileGroupService;

  @Autowired
  private STTTranscriptService sttTranscriptService;

  @Autowired
  private HMDDicService hmdDicService;

  @Autowired
  private DNNDicService dnnDicService;

  @Autowired
  private DNNTrainingService dnnTrainingService;

  @Autowired
  private FileService fileService;

  @Autowired
  private HistoryService historyService;

  @Autowired
  private STTModelService sttModelService;

  @Autowired
  private BasicQASkillService basicQASkillService;

  @Autowired
  private BasicQAIndexingHistoryService basicQAIndexingHistoryService;
/*
  @Autowired
  private DataGroupService mrcDataGroupService;

  @Autowired
  private TrainingService mrcTrainingService;
*/
  @UriRoleDesc(role = "dashboard2^R")
  @RequestMapping(
      value = "/getSttFileGroups",
      method = RequestMethod.POST)
  public ResponseEntity<?> getFileGroups(@RequestBody HashMap<String, String> param) {
    logger.info("======= call api  POST [[/api/dashboard/getSttFileGroups]] =======");

    try {
      List<FileGroupEntity> fileGroupEntityList = fileGroupService
          .getFileGroupList(SystemCode.FILE_GRP_PPOS_STT, (String) param.get("workspaceId"));

      for (FileGroupEntity fileGroupEntity : fileGroupEntityList) {
        Integer fileDuration = 0;
        Integer fileCount = 0;
        FileEntity fileEntity = new FileEntity();
        fileEntity.setFileGroupId(fileGroupEntity.getId());
        fileEntity.setPurpose(SystemCode.FILE_GRP_PPOS_STT);
        fileEntity.setWorkspaceId(param.get("workspaceId"));
        fileEntity.setPageIndex(0);
        fileEntity.setPageSize(1000);
        List<FileEntity> test = fileService.getGroupFileList(fileEntity).getContent();
        for (FileEntity fileEntity1 : test) {
          fileDuration += fileEntity1.getDuration();
          fileCount++;
        }
        fileGroupEntity.setFileTotalDuration(fileDuration);
        fileGroupEntity.setFileTotalCount(fileCount);
      }

      return new ResponseEntity<>(fileGroupEntityList, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getFileGroups e : ", e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
/*
  @RequestMapping(
      value = "/get-SttFiles",
      method = RequestMethod.POST)
  public ResponseEntity<?> getSttFileList(@RequestBody FileEntity param) {
    logger.info("======= call api  POST [[/api/dashboard/getSttTrainedModels]] =======");

    try {
      //List<FileEntity> fileEntityList = new ArrayList<>();
      //for(FileEntity fileEntity : param) {
      param.setPurpose(SystemCode.FILE_GRP_PPOS_STT);
        Page<FileEntity> pageResult = fileService.getGroupFileList(param);
        //fileEntityList.add((FileEntity)pageResult);
      //}
      return new ResponseEntity<>(pageResult.getContent(), HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getSttFileList e : " , e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }*/


  @UriRoleDesc(role = "dashboard2^R")
  @RequestMapping(
      value = "/getHmdModels",
      method = RequestMethod.POST)
  public ResponseEntity<?> getHmdModels(@RequestBody HashMap<String, String> param) {
    logger.info("======= call api  POST [[/api/dashboard/getHmdModels]] =======");

    try {

      HMDDicEntity hmdDicEntity = new HMDDicEntity();
      hmdDicEntity.setWorkspaceId((String) param.get("workspaceId"));
      hmdDicEntity.setPageIndex(0);
      hmdDicEntity.setPageSize(5);
      hmdDicEntity.setOrderDirection("desc");
      hmdDicEntity.setOrderProperty("updatedAt");

      Page<HMDDicEntity> hmdDicEntityList =
          hmdDicService.getHMDDicPageList(hmdDicEntity);

      HashMap<String, Object> resultMap = new HashMap();
      resultMap.put("total", hmdDicEntityList.getTotalElements());
      resultMap.put("content", hmdDicEntityList.getContent());

      return new ResponseEntity<>(resultMap, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getHmdModels e : ", e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dashboard2^R")
  @RequestMapping(
      value = "/getSttFileCount",
      method = RequestMethod.POST)
  public ResponseEntity<?> getSttFileCount(@RequestBody HashMap<String, String> param) {
    logger.info("======= call api  POST [[/api/dashboard/getSttFileCount]] =======");

    try {

      Integer totalCount = fileService
          .getFileCount(SystemCode.FILE_GRP_PPOS_STT, (String) param.get("workspaceId"));

      return new ResponseEntity<>(totalCount, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getSttFileCount e : ", e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dashboard2^R")
  @RequestMapping(
      value = "/getTranscriptCompleteCount",
      method = RequestMethod.POST)
  public ResponseEntity<?> getTranscriptCompleteCount(@RequestBody HashMap<String, String> param) {
    logger.info("======= call api  POST [[/api/dashboard/getTranscriptCompleteCount]] =======");

    try {

      Integer count = sttTranscriptService
          .selectTranscriptCompleteCount(SystemCode.FILE_GRP_PPOS_STT,
              (String) param.get("workspaceId"));

      return new ResponseEntity<>(count, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getTranscriptCompleteCount e : ", e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dashboard2^R")
  @RequestMapping(
      value = "/getQualityAssuranceCompleteCount",
      method = RequestMethod.POST)
  public ResponseEntity<?> getQualityAssuranceCompleteCount(
      @RequestBody HashMap<String, String> param) {
    logger
        .info("======= call api  POST [[/api/dashboard/getQualityAssuranceCompleteCount]] =======");

    try {

      Integer count = sttTranscriptService
          .selectQualityAssuranceCompleteCount(SystemCode.FILE_GRP_PPOS_STT,
              (String) param.get("workspaceId"));

      return new ResponseEntity<>(count, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getQualityAssuranceCompleteCount e : ", e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dashboard2^R")
  @RequestMapping(
      value = "/getDnnModels",
      method = RequestMethod.POST)
  public ResponseEntity<?> getDnnModels(@RequestBody DNNDicEntity dnnDicEntity) {
    logger.info("======= call api  POST [[/api/dashboard/getDnnModels]] =======");

    try {
      dnnDicEntity.setPageIndex(0);
      dnnDicEntity.setPageSize(5);
      dnnDicEntity.setOrderDirection("desc");
      dnnDicEntity.setOrderProperty("updatedAt");

      Page<DNNDicEntity> dnnDicEntityList =
          dnnDicService.getDnnDicAllList(dnnDicEntity);

      HashMap<String, Object> resultMap = new HashMap();
      resultMap.put("total", dnnDicEntityList.getTotalElements());
      resultMap.put("content", dnnDicEntityList.getContent());

      return new ResponseEntity<>(resultMap, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getDnnModels e : ", e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dashboard2^R")
  @RequestMapping(
      value = "/getDnnTrainings",
      method = RequestMethod.POST)
  public ResponseEntity<?> getDnnTrainings(@RequestBody DNNTrainingEntity dnnTrainingEntity) {
    logger.info("======= call api  POST [[/api/dashboard/getDnnTrainings]] =======");
    try {

      List<DNNTrainingEntity> dnnTrainingEntities = new ArrayList<>();
      TaGrpcInterfaceManager client = new TaGrpcInterfaceManager(taTrainIp, taTrainPort);

      Empty.Builder req = Empty.newBuilder();
      ClTrainStatusList trainStatusList = client.getAllProgress(req.build());

      if (trainStatusList != null) {
        for (ClTrainStatus trainStatus : trainStatusList.getClTrainsList()) {
          DNNTrainingEntity resultEntity = dnnTrainingService
              .getTraining(trainStatus.getKey());

          if (resultEntity != null) {

            resultEntity.setDnnKey(trainStatus.getKey());
            resultEntity.setName(trainStatus.getModel());
            resultEntity.setNode(trainStatus.getNodeCount());
            resultEntity.setRepeat(trainStatus.getRunCount());
            resultEntity.setRunCur(trainStatus.getRunCur());
            resultEntity.setStatus(trainStatus.getResult().toString());
            resultEntity.setProgress(trainStatus.getValue() * 100 / trainStatus.getMaximum());
            resultEntity.setWorkspaceId(dnnTrainingEntity.getWorkspaceId());

            if (resultEntity.getWorkspaceId().equals(dnnTrainingEntity.getWorkspaceId())) {
              dnnTrainingEntities.add(resultEntity);
            }
          }
        }
      }

      return new ResponseEntity<>(dnnTrainingEntities, HttpStatus.OK);
    } catch (Exception e) {
      logger.debug("getAllProgress e : " + e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }


  @UriRoleDesc(role = "dashboard2^R")
  @RequestMapping(
      value = "/getHistories",
      method = RequestMethod.POST)
  public ResponseEntity<?> getHistories(@RequestBody HistoryEntity historyEntity) {
    logger.info("======= call api  POST [[/api/dashboard/getHistories]] =======");
    historyEntity.setPageIndex(0);
    historyEntity.setPageSize(8);
    historyEntity.setOrderDirection("desc");
    historyEntity.setOrderProperty("updatedAt");
    try {
      Page<HistoryEntity> historyEntities = historyService
          .findAllByWorkspaceId(historyEntity.getWorkspaceId(),
              historyEntity.getCode(), historyEntity.getPageRequest());

      return new ResponseEntity<>(historyEntities, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getHistories e : ", e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dashboard2^R")
  @RequestMapping(
      value = "/get-SttTrainingModels",
      method = RequestMethod.POST)
  public ResponseEntity<?> getSttTrainingModels(@RequestBody HashMap<String, String> param) {
    logger.info("======= call api  POST [[/api/dashboard/SttTrainingModels]] =======");
    SttGrpcInterfaceManager client = new SttGrpcInterfaceManager(sttTrainIp, sttTrainPort);
    try {

      List<STTModelEntity> getTrainingModelList = sttModelService
          .selectTrainedModelList(param.get("workspaceId"), TrainResult.training.toString());
      List<STTModelEntity> setTrainingModelList = new ArrayList<>();

      for (STTModelEntity sttModelEntity : getTrainingModelList) {
        TrainKey.Builder req = TrainKey.newBuilder();
        req.setTrainId(sttModelEntity.getSttKey());
        TrainStatus trainStatus = client.getProgress(req.build());

        STTModelEntity sttModelEntity1 = sttModelService
            .selectTrainingModel(sttModelEntity.getSttKey());
        sttModelEntity1.setProgress(trainStatus.getValue() * 100 / trainStatus.getMaximum());
        sttModelEntity1.setName(sttModelEntity.getName());
        setTrainingModelList.add(sttModelEntity1);
      }
      return new ResponseEntity<>(setTrainingModelList, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getSttTrainingModels e : ", e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dashboard2^R")
  @RequestMapping(
      value = "/get-SttTrainedModels",
      method = RequestMethod.POST)
  public ResponseEntity<?> getSttTrainedModels(@RequestBody HashMap<String, String> param) {
    logger.info("======= call api  POST [[/api/dashboard/getSttTrainedModels]] =======");

    try {
      List<STTModelEntity> resultList = sttModelService
          .selectTrainedModelList(param.get("workspaceId"), TrainResult.success.toString());
      return new ResponseEntity<>(resultList, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getSttTrainedModels e : ", e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dashboard2^R")
  @RequestMapping(
      value = "/getBsqaSkills",
      method = RequestMethod.GET)
  public ResponseEntity<?> getBsqaSkills() {
    logger.info("======= call api  POST [[/api/dashboard/getBsqaSkills]] =======");

    try {
      return new ResponseEntity<>(basicQASkillService.selectList(), HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getBsqaSkills e : ", e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dashboard2^R")
  @RequestMapping(
      value = "/getBsqaIndexingStatus",
      method = RequestMethod.GET)
  public ResponseEntity<?> getBsqaIndexingStatus() {
    logger.info("======= call api  POST [[/api/dashboard/getBsqaIndexingStatus]] =======");

    try {
      BasicqaGrpcInterfaceManager client = new BasicqaGrpcInterfaceManager(bsqaIp, bsqaPort);
      com.google.protobuf.Empty.Builder empty = com.google.protobuf.Empty.newBuilder();

      maum.brain.qa.Basicqa.SearchStatus searchStatus = client.getIndexingStatus(empty.build());
      HashMap<String, Object> result = new HashMap<>();
      result.put("status", searchStatus.getStatus());
      if (searchStatus.getStatus()) {
        float fetched = searchStatus.getFetched();
        float total = searchStatus.getTotal();
        float progress = (fetched / total) * 100;
        result.put("progress", progress);
      } else {
        BasicQAIndexingHistoryEntity basicQAIndexingHistoryEntity = basicQAIndexingHistoryService
            .selectLastIndexingHistory(bsqaIp + ":" + bsqaPort);
        if (basicQAIndexingHistoryEntity != null) {
          float fetched = basicQAIndexingHistoryEntity.getFetched();
          float total = basicQAIndexingHistoryEntity.getTotal();
          if (basicQAIndexingHistoryEntity.getStopYn()) {
            float progress = (fetched / total) * 100;
            result.put("progress", progress);
          } else {
            result.put("progress", 100);
          }
          result.put("latestIndexingHistory", basicQAIndexingHistoryEntity);
        } else {
          result.put("progress", 0);
          result.put("latestIndexingHistory", null);
        }
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getBsqaIndexingStatus e : ", e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

/*
  @UriRoleDesc(role = "dashboard2^R")
  @RequestMapping(
      value = "/getMrcGroups",
      method = RequestMethod.POST)
  public ResponseEntity<?> getMrcGroups(@RequestBody DataGroupEntity dataGroupEntity) {
    logger.info("======= call api  POST [[/api/dashboard/getMrcGroups]] =======");

    try {
      List<DataGroupEntity> resultList = mrcDataGroupService
          .selectDataGroupListByWorkspaceId(dataGroupEntity.getWorkspaceId());
      return new ResponseEntity<>(resultList, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "dashboard2^R")
  @RequestMapping(
      value = "/getMrcTrainings",
      method = RequestMethod.POST
  )
  public ResponseEntity<?> getMrcTrainings(
      @RequestBody MrcTrainingModelEntity mrcTrainingModelEntity) {
    logger.info("======= call api  POST [[/api/dashboard/getMrcTrainings]] =======");

    HashMap<String, Object> result = new HashMap<String, Object>();
    try {

      List<MrcTrainingModelEntity> mtmList = new ArrayList<>();
      MrcGrpcInterfaceManager client = new MrcGrpcInterfaceManager(mrcTrainIp, mrcTrainPort);
      logger.info("======= ip:port =======");
      logger.info(mrcTrainIp + ":" + mrcTrainPort);

      Empty.Builder req = Empty.newBuilder();
      MrcTrainStatusList mrcTrainStatus = client.getAllProgress(req.build());
      logger.debug("mrcTrainStatus : " + mrcTrainStatus);
      if (mrcTrainStatus == null) {
        throw new Exception("NOT_STARTED");
      }

      // train_id db save
      for (MrcTrainStatus mts : mrcTrainStatus.getMrcTrainsList()) {
        MrcTrainingModelEntity mtm = mrcTrainingService.getMrcTrainingModel(mts.getKey());
        if (mtm != null) {
          mtm.setKey(mts.getKey());
          mtm.setStatus(mts.getResult().toString());
          mtm.setMrcProgress(mts.getValue() * 100 / mts.getMaximum());
          logger.debug("mrcProcress : " + mts.getValue() * 100 / mts.getMaximum());
          // mtm.setName(mts.getKey());
          mtm.setUpdatedAt(new Date());
          mrcTrainingService.updateMrcTrainingModel(mtm);
          if (mts.getResult() == MrcTrainResult.cancelled
              || mts.getResult() == MrcTrainResult.success) {
            MrcTrainKey.Builder mtk = MrcTrainKey.newBuilder();
            mtk.setTrainId(mts.getKey());
            MrcTrainStatus mrcTrainStatus2 = client.close(mtk.build());

            logger.debug("mrcTrainStatus : " + mrcTrainStatus2);
          } else {
            if (mtm.getWorkspaceId() == mrcTrainingModelEntity.getWorkspaceId()) {
              mtmList.add(mtm);
            }
          }
        }
      }

      logger.debug("mtmList : " + mtmList);
      result.put("mtmList", mtmList);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.debug("getAllProgress e : " + e);
      if (e.getMessage().equals("NOT_STARTED")) {
        result.put("message", e.getMessage());
      }
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
*/
}
