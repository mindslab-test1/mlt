package ai.maum.mlt.sds.model.repository;

import ai.maum.mlt.sds.model.entity.StartEventUtter;
import ai.maum.mlt.sds.model.entity.StartEventUtterData;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface StartEventUtterDataRepository extends JpaRepository<StartEventUtterData, String> {

  StartEventUtterData findStartEventUtterDataById(String startEventUtterDataId);

  List<StartEventUtterData> findAll();

  List<StartEventUtterData> findStartEventUtterDataByStartEventUtter(
      StartEventUtter startEventUtter);

  void deleteStartEventUtterDataById(String startEventUtterDataId);
}
