package ai.maum.mlt.sds.model.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.sds.model.entity.StartEvent;
import ai.maum.mlt.sds.model.entity.Task;
import ai.maum.mlt.sds.model.service.StartEventService;
import ai.maum.mlt.sds.model.service.TaskService;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/sds/model")
public class StartEventController {

  static final Logger logger = LoggerFactory.getLogger(StartEventController.class);

  @Autowired
  private TaskService taskService;

  @Autowired
  private StartEventService startEventService;

  @UriRoleDesc(role = "sds/model^C")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}/startevent", method = RequestMethod.POST)
  public ResponseEntity<?> insertStartEvent(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String taskId,
      @RequestBody JSONObject bodyParam) {
    logger.info("POST /api/workspace/{}/entity/{}/task/{}/startevent", workspaceId, modelId, taskId,
        bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      String startEventId = null;
      String startEventCondition = "";
      String startEventAction = "";
      String userId = null;

      Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

      if (bodyParam.get("id") == null || bodyParam.get("id").toString().trim().equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "id is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        startEventId = bodyParam.get("id").toString().trim();

        StartEvent findStartEvent = null;
        try {
          findStartEvent = startEventService.findStartEventById(startEventId);
        } catch (Exception ex) {
          findStartEvent = startEventService.findStartEventById(startEventId);
        }
        if (findStartEvent != null) {
          result.put("statusCode", 400);
          result.put("statusMessage", "id is already exist.");

          return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }
      }

      if (bodyParam.get("startEventCondition") != null) {
        startEventCondition = bodyParam.get("startEventCondition").toString().trim()
            .replace("\"", "'");
      }

      if (bodyParam.get("startEventAction") != null) {
        startEventAction = bodyParam.get("startEventAction").toString().trim().replace("\"", "'");
      }

      if (bodyParam.get("userId") == null || bodyParam.get("userId").toString().trim().equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "userId is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        userId = bodyParam.get("userId").toString().trim();
      }

      Task findTask = null;
      try {
        findTask = taskService.findTaskById(taskId);
      } catch (Exception ex) {
        findTask = taskService.findTaskById(taskId);
      }
      if (findTask == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "task doesn't exist.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      StartEvent startEvent = new StartEvent();
      startEvent.setId(startEventId);
      startEvent.setStartEventCondition(startEventCondition);
      startEvent.setStartEventAction(startEventAction);
      startEvent.setTask(findTask);

      startEvent.setCreatorId(userId);
      startEvent.setCreatedAt(currentTimestamp);
      startEvent.setUpdaterId(userId);
      startEvent.setUpdatedAt(currentTimestamp);

      try {
        startEventService.insertStartEvent(startEvent);
      } catch (Exception ex) {
        startEventService.insertStartEvent(startEvent);
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("POST /api/workspace/{}/entity/{}/task/{}/startevent", workspaceId, modelId,
          taskId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^R")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}/startevent/list", method = RequestMethod.GET)
  public ResponseEntity<?> getStartEventListByTaskId(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String taskId) {
    logger.info("GET /api/workspace/{}/entity/{}/task/{}/startevent/list", workspaceId, modelId,
        taskId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      Task findTask = null;
      try {
        findTask = taskService.findTaskById(taskId);
      } catch (Exception ex) {
        findTask = taskService.findTaskById(taskId);
      }
      if (findTask == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "task 데이터가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      List<StartEvent> startEventList = new ArrayList<>();
      try {
        startEventList = startEventService.findStartEventsByTask(findTask);
      } catch (Exception ex) {
        startEventList = startEventService.findStartEventsByTask(findTask);
      }

      JSONObject wrappedStartEventInfo = new JSONObject();
      JSONArray startEventInfoList = new JSONArray();

      for (int i = 0; i < startEventList.size(); i++) {
        JSONObject startEventInfo = new JSONObject();

        startEventInfo.put("id", startEventList.get(i).getId());
        startEventInfo.put("startEventCondition", startEventList.get(i).getStartEventCondition());
        startEventInfo.put("startEventAction", startEventList.get(i).getStartEventAction());
        startEventInfo.put("taskId", startEventList.get(i).getTask().getId());

        startEventInfo.put("creatorId", startEventList.get(i).getCreatorId());
        startEventInfo.put("createdAt", startEventList.get(i).getCreatedAt());
        startEventInfo.put("updaterId", startEventList.get(i).getUpdaterId());
        startEventInfo.put("updatedAt", startEventList.get(i).getUpdatedAt());

        startEventInfoList.add(startEventInfo);
      }

      wrappedStartEventInfo.put("startEventInfoList", startEventInfoList);

      result.put("data", wrappedStartEventInfo);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("GET /api/workspace/{}/entity/{}/task/{}/startevent/list", workspaceId, modelId,
          taskId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^R")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}/startevent/{startEventId}", method = RequestMethod.GET)
  public ResponseEntity<?> getStartEventById(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String taskId,
      @PathVariable String startEventId) {
    logger
        .info("GET /api/workspace/{}/entity/{}/task/{}/startevent/{}", workspaceId, modelId, taskId,
            startEventId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      StartEvent startEvent = null;
      try {
        startEvent = startEventService.findStartEventById(startEventId);
      } catch (Exception ex) {
        startEvent = startEventService.findStartEventById(startEventId);
      }

      JSONObject wrappedStartEventInfo = new JSONObject();
      JSONObject startEventInfo = new JSONObject();

      startEventInfo.put("id", startEvent.getId());
      startEventInfo.put("startEventCondition", startEvent.getStartEventCondition());
      startEventInfo.put("startEventAction", startEvent.getStartEventAction());
      startEventInfo.put("taskId", startEvent.getTask().getId());

      startEventInfo.put("creatorId", startEvent.getCreatorId());
      startEventInfo.put("createdAt", startEvent.getCreatedAt());
      startEventInfo.put("updaterId", startEvent.getUpdaterId());
      startEventInfo.put("updatedAt", startEvent.getUpdatedAt());

      wrappedStartEventInfo.put("startEventInfo", startEventInfo);

      result.put("data", wrappedStartEventInfo);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("GET /api/workspace/{}/entity/{}/task/{}/startevent/{}", workspaceId, modelId,
          taskId, startEventId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^U")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}/startevent/{startEventId}", method = RequestMethod.PUT)
  public ResponseEntity<?> updateStartEvent(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String taskId, @PathVariable String startEventId,
      @RequestBody JSONObject bodyParam) {
    logger
        .info("PUT /api/workspace/{}/entity/{}/task/{}/startevent/{}", workspaceId, modelId, taskId,
            startEventId, bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

      String startEventCondition = null;
      String startEventAction = null;
      String userId = null;

      if (bodyParam.get("startEventCondition") != null) {
        startEventCondition = bodyParam.get("startEventCondition").toString().trim()
            .replace("\"", "'");
      }

      if (bodyParam.get("startEventAction") != null) {
        startEventAction = bodyParam.get("startEventAction").toString().trim().replace("\"", "'");
      }

      if (bodyParam.get("userId") == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "userId 가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        userId = bodyParam.get("userId").toString().trim();
      }

      StartEvent findStartEvent = null;
      try {
        findStartEvent = startEventService.findStartEventById(startEventId);
      } catch (Exception ex) {
        findStartEvent = startEventService.findStartEventById(startEventId);
      }
      if (findStartEvent == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "startEventId 와 일치하는 startEvent 가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        if (startEventCondition != null) {
          findStartEvent.setStartEventCondition(startEventCondition);
        }

        if (startEventAction != null) {
          findStartEvent.setStartEventAction(startEventAction);
        }

        findStartEvent.setUpdaterId(userId);
        findStartEvent.setUpdatedAt(currentTimestamp);
      }

      try {
        startEventService.updateStartEvent(findStartEvent);
      } catch (Exception ex) {
        startEventService.updateStartEvent(findStartEvent);
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("PUT /api/workspace/{}/entity/{}/task/{}/startevent/{}", workspaceId, modelId,
          taskId, startEventId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^D")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}/startevent/{startEventId}", method = RequestMethod.DELETE)
  public ResponseEntity<?> deleteStartEventById(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String taskId,
      @PathVariable String startEventId) {
    logger.info("DELETE /api/workspace/{}/entity/{}/task/{}/startevent/{}", workspaceId, modelId,
        taskId, startEventId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      StartEvent findStartEvent = null;
      try {
        findStartEvent = startEventService.findStartEventById(startEventId);
      } catch (Exception ex) {
        findStartEvent = startEventService.findStartEventById(startEventId);
      }
      if (findStartEvent == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "startEventId 와 일치하는 startEvent 가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        try {
          startEventService.deleteStartEventById(startEventId);
        } catch (Exception ex) {
          startEventService.deleteStartEventById(startEventId);
        }
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("DELETE /api/workspace/{}/entity/{}/task/{}/startevent/{}", workspaceId, modelId,
          taskId, startEventId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
