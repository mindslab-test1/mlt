package ai.maum.mlt.sds.model.service;

import ai.maum.mlt.sds.model.entity.StartEvent;
import ai.maum.mlt.sds.model.entity.Task;
import ai.maum.mlt.sds.model.repository.StartEventRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StartEventServiceImpl implements StartEventService {

  @Autowired
  private StartEventRepository startEventRepository;

  @Override
  public StartEvent insertStartEvent(StartEvent startEvent) {
    return startEventRepository.save(startEvent);
  }

  @Override
  public StartEvent findStartEventById(String startEventId) {
    return startEventRepository.findStartEventById(startEventId);
  }

  @Override
  public List<StartEvent> findAll() {
    return startEventRepository.findAll();
  }

  @Override
  public List<StartEvent> findStartEventsByTask(Task task) {
    return startEventRepository.findStartEventsByTask(task);
  }

  @Override
  public StartEvent updateStartEvent(StartEvent startEvent) {
    return startEventRepository.save(startEvent);
  }

  @Override
  public void deleteStartEventById(String startEventId) {
    startEventRepository.deleteStartEventById(startEventId);
  }
}
