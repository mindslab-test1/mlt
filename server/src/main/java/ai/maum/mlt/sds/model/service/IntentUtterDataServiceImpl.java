package ai.maum.mlt.sds.model.service;

import ai.maum.mlt.sds.model.entity.IntentUtter;
import ai.maum.mlt.sds.model.entity.IntentUtterData;
import ai.maum.mlt.sds.model.repository.IntentUtterDataRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IntentUtterDataServiceImpl implements IntentUtterDataService {

  @Autowired
  private IntentUtterDataRepository intentUtterDataRepository;

  @Override
  public IntentUtterData insertIntentUtterData(IntentUtterData intentUtterData) {
    return intentUtterDataRepository.save(intentUtterData);
  }

  @Override
  public IntentUtterData findIntentUtterDataById(String intentUtterDataId) {
    return intentUtterDataRepository.findIntentUtterDataById(intentUtterDataId);
  }

  @Override
  public List<IntentUtterData> findAll() {
    return intentUtterDataRepository.findAll();
  }

  @Override
  public List<IntentUtterData> findIntentUtterDataByIntentUtter(IntentUtter intentUtter) {
    return intentUtterDataRepository.findIntentUtterDataByIntentUtter(intentUtter);
  }

  @Override
  public IntentUtterData updateIntentUtterData(IntentUtterData intentUtterData) {
    return intentUtterDataRepository.save(intentUtterData);
  }

  @Override
  public void deleteIntentUtterDataById(String intentUtterDataId) {
    intentUtterDataRepository.deleteIntentUtterDataById(intentUtterDataId);

    return;
  }
}
