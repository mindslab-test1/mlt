package ai.maum.mlt.sds.model.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.sds.model.entity.Entity;
import ai.maum.mlt.sds.model.entity.EntityData;
import ai.maum.mlt.sds.model.service.EntityDataService;
import ai.maum.mlt.sds.model.service.EntityService;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/sds/model")
public class EntityDataController {

  static final Logger logger = LoggerFactory.getLogger(EntityDataController.class);

  @Autowired
  private EntityService entityService;

  @Autowired
  private EntityDataService entityDataService;

  @UriRoleDesc(role = "sds/model^C")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/entity/{entityId}/entitydata", method = RequestMethod.POST)
  public ResponseEntity<?> insertEntityData(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String entityId,
      @RequestBody JSONObject bodyParam) {
    logger.info("POST /api/workspace/{}/entity/{}/entity/{}/entitydata", workspaceId, modelId,
        entityId, bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      String entityDataId = null;
      String entityDataContent = "";
      String userId = null;

      Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

      if (bodyParam.get("id") == null || bodyParam.get("id").toString().trim().equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "id is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        entityDataId = bodyParam.get("id").toString().trim();

        EntityData findEntityData = null;
        try {
          findEntityData = entityDataService.findEntityDataById(entityDataId);
        } catch (Exception ex) {
          findEntityData = entityDataService.findEntityDataById(entityDataId);
        }
        if (findEntityData != null) {
          result.put("statusCode", 400);
          result.put("statusMessage", "id is already exist.");

          return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }
      }

      if (bodyParam.get("entityData") != null && !bodyParam.get("entityData").toString().trim()
          .equals("")) {
        entityDataContent = bodyParam.get("entityData").toString().trim();
      }

      if (bodyParam.get("userId") == null || bodyParam.get("userId").toString().trim().equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "userId is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        userId = bodyParam.get("userId").toString().trim();
      }

      Entity findEntity = null;
      try {
        findEntity = entityService.findEntityByWorkspaceIdAndId(workspaceId, entityId);
      } catch (Exception ex) {
        findEntity = entityService.findEntityByWorkspaceIdAndId(workspaceId, entityId);
      }
      if (findEntity == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "Entity doesn't exist.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      EntityData entityData = new EntityData();
      entityData.setId(entityDataId);
      entityData.setEntityData(entityDataContent);
      entityData.setEntity(findEntity);

      entityData.setCreatorId(userId);
      entityData.setCreatedAt(currentTimestamp);
      entityData.setUpdaterId(userId);
      entityData.setUpdatedAt(currentTimestamp);

      try {
        entityDataService.insertEntityData(entityData);
      } catch (Exception ex) {
        entityDataService.insertEntityData(entityData);
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("POST /api/workspace/{}/entity/{}/entity/{}/entitydata", workspaceId, modelId,
          entityId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^R")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/entity/{entityId}/entitydata/list", method = RequestMethod.GET)
  public ResponseEntity<?> getEntityDataListByEntityId(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String entityId) {
    logger.info("GET /api/workspace/{}/entity/{}/entity/{}/list", workspaceId, modelId, entityId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      Entity entity = null;
      try {
        entity = entityService.findEntityByWorkspaceIdAndId(workspaceId, entityId);
      } catch (Exception ex) {
        entity = entityService.findEntityByWorkspaceIdAndId(workspaceId, entityId);
      }
      if (entity == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "entity 데이터가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      List<EntityData> entityDataList = new ArrayList<>();
      try {
        entityDataList = entityDataService.findEntityDataByEntity(entity);
      } catch (Exception ex) {
        entityDataList = entityDataService.findEntityDataByEntity(entity);
      }

      JSONObject wrappedEntityDataInfo = new JSONObject();
      JSONArray entityDataInfoList = new JSONArray();

      for (int i = 0; i < entityDataList.size(); i++) {
        JSONObject entityDataInfo = new JSONObject();

        entityDataInfo.put("id", entityDataList.get(i).getId());
        entityDataInfo.put("entityData", entityDataList.get(i).getEntityData());
        entityDataInfo.put("entityId", entityDataList.get(i).getEntity().getId());

        entityDataInfo.put("creatorId", entityDataList.get(i).getCreatorId());
        entityDataInfo.put("createdAt", entityDataList.get(i).getCreatedAt());
        entityDataInfo.put("updaterId", entityDataList.get(i).getUpdaterId());
        entityDataInfo.put("updatedAt", entityDataList.get(i).getUpdatedAt());

        entityDataInfoList.add(entityDataInfo);
      }

      wrappedEntityDataInfo.put("entityDataInfoList", entityDataInfoList);

      result.put("data", wrappedEntityDataInfo);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("GET /api/workspace/{}/entity/{}/entity/{}/list", workspaceId, modelId, entityId,
          e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^R")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/entity/{entityId}/entitydata/{entityDataId}", method = RequestMethod.GET)
  public ResponseEntity<?> getEntityDataByEntityDataId(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String entityId,
      @PathVariable String entityDataId) {
    logger.info("GET /api/workspace/{}/entity/{}/entity/{}/entitydata/{}", workspaceId, modelId,
        entityId, entityDataId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      Entity entity = null;
      try {
        entity = entityService.findEntityByWorkspaceIdAndId(workspaceId, entityId);
      } catch (Exception ex) {
        entity = entityService.findEntityByWorkspaceIdAndId(workspaceId, entityId);
      }
      if (entity == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "entity 데이터가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      EntityData entityData = null;
      try {
        entityData = entityDataService.findEntityDataById(entityDataId);
      } catch (Exception ex) {
        entityData = entityDataService.findEntityDataById(entityDataId);
      }

      JSONObject wrappedEntityDataInfo = new JSONObject();
      JSONObject entityDataInfo = new JSONObject();

      entityDataInfo.put("id", entityData.getId());
      entityDataInfo.put("entityData", entityData.getEntityData());
      entityDataInfo.put("entityId", entityData.getEntity().getId());

      entityDataInfo.put("creatorId", entityData.getCreatorId());
      entityDataInfo.put("createdAt", entityData.getCreatedAt());
      entityDataInfo.put("updaterId", entityData.getUpdaterId());
      entityDataInfo.put("updatedAt", entityData.getUpdatedAt());

      wrappedEntityDataInfo.put("entityDataInfo", entityDataInfo);

      result.put("data", wrappedEntityDataInfo);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("GET /api/workspace/{}/entity/{}/entity/{}/entitydata/{}", workspaceId, modelId,
          entityId, entityDataId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^U")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/entity/{entityId}/entitydata/{entityDataId}", method = RequestMethod.PUT)
  public ResponseEntity<?> updateEntityData(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String entityId,
      @PathVariable String entityDataId, @RequestBody JSONObject bodyParam) {
    logger.info("PUT /api/workspace/{}/entity/{}/entity/{}/entitydata/{}", workspaceId, modelId,
        entityId, entityDataId, bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

      String entityData = "";
      String userId = null;

      if (bodyParam.get("entityData") != null) {
        entityData = bodyParam.get("entityData").toString().trim();
      }

      if (bodyParam.get("userId") == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "userId 가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        userId = bodyParam.get("userId").toString().trim();
      }

      EntityData findEntityData = null;
      try {
        findEntityData = entityDataService.findEntityDataById(entityDataId);
      } catch (Exception ex) {
        findEntityData = entityDataService.findEntityDataById(entityDataId);
      }
      if (findEntityData == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "entityDataId 와 일치하는 entityData 가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        // 입력한 entityData 값이 저장된 값과 동일하면 return.
        if (findEntityData.getEntityData().equals(entityData)) {
          return new ResponseEntity<>(result, HttpStatus.OK);
        } else {
          findEntityData.setEntityData(entityData);
        }

        findEntityData.setUpdaterId(userId);
        findEntityData.setUpdatedAt(currentTimestamp);
      }

      try {
        entityDataService.updateEntityData(findEntityData);
      } catch (Exception ex) {
        entityDataService.updateEntityData(findEntityData);
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("PUT /api/workspace/{}/entity/{}/entity/{}/entitydata/{}", workspaceId, modelId,
          entityId, entityDataId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^D")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/entity/{entityId}/entitydata/{entityDataId}", method = RequestMethod.DELETE)
  public ResponseEntity<?> deleteEntityDataById(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String entityId,
      @PathVariable String entityDataId) {
    logger.info("DELETE /api/workspace/{}/entity/{}/entity/{}/entitydata/{}", workspaceId, modelId,
        entityId, entityDataId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      EntityData findEntityData = null;
      try {
        findEntityData = entityDataService.findEntityDataById(entityDataId);
      } catch (Exception ex) {
        findEntityData = entityDataService.findEntityDataById(entityDataId);
      }
      if (findEntityData == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "entityDataId 와 일치하는 entityData 가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        try {
          entityDataService.deleteEntityDataById(entityDataId);
        } catch (Exception ex) {
          entityDataService.deleteEntityDataById(entityDataId);
        }
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger
          .error("DELETE /api/workspace/{}/entity/{}/entity/{}/entitydata/{}", workspaceId, modelId,
              entityId, entityDataId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
