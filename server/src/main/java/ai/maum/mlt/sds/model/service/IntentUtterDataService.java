package ai.maum.mlt.sds.model.service;

import ai.maum.mlt.sds.model.entity.IntentUtter;
import ai.maum.mlt.sds.model.entity.IntentUtterData;
import java.util.List;
import javax.transaction.Transactional;

@Transactional
public interface IntentUtterDataService {

  IntentUtterData insertIntentUtterData(IntentUtterData intentUtterData);

  IntentUtterData findIntentUtterDataById(String intentUtterDataId);

  List<IntentUtterData> findAll();

  List<IntentUtterData> findIntentUtterDataByIntentUtter(IntentUtter intentUtter);

  IntentUtterData updateIntentUtterData(IntentUtterData intentUtterData);

  void deleteIntentUtterDataById(String intentUtterDataId);
}
