package ai.maum.mlt.sds.model.service;

import ai.maum.mlt.sds.model.entity.Model;
import ai.maum.mlt.sds.model.repository.ModelRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ModelServiceImpl implements ModelService {

  @Autowired
  private ModelRepository modelRepository;

  @Override
  public Model insertModel(Model model) {
    return modelRepository.save(model);
  }

  @Override
  public List<Model> findModelsByWorkspaceIdOrderByModelName(String workspaceId) {
    return modelRepository.findModelsByWorkspaceIdOrderByModelName(workspaceId);
  }

  @Override
  public Model findModelByWorkspaceIdAndCreatorIdAndModelName(String workspaceId, String creatorId,
      String modelName) {
    return modelRepository
        .findModelByWorkspaceIdAndCreatorIdAndModelName(workspaceId, creatorId, modelName);
  }

  @Override
  public Model findModelByWorkspaceIdAndId(String workspaceId, String modelId) {
    return modelRepository.findModelByWorkspaceIdAndId(workspaceId, modelId);
  }

  @Override
  public Model findModelById(String modelId) {
    return modelRepository.findModelById(modelId);
  }

  @Override
  public Model updateModel(Model model) {
    return modelRepository.save(model);
  }

  @Override
  public void deleteModelByWorkspaceIdAndId(String workspaceId, String modelId) {
    modelRepository.deleteModelByWorkspaceIdAndId(workspaceId, modelId);

    return;
  }
}
