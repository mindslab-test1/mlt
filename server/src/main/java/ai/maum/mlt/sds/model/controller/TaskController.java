package ai.maum.mlt.sds.model.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.sds.model.entity.Event;
import ai.maum.mlt.sds.model.entity.EventCondition;
import ai.maum.mlt.sds.model.entity.EventUtter;
import ai.maum.mlt.sds.model.entity.EventUtterData;
import ai.maum.mlt.sds.model.entity.Model;
import ai.maum.mlt.sds.model.entity.NextTask;
import ai.maum.mlt.sds.model.entity.StartEvent;
import ai.maum.mlt.sds.model.entity.StartEventUtter;
import ai.maum.mlt.sds.model.entity.StartEventUtterData;
import ai.maum.mlt.sds.model.entity.Task;
import ai.maum.mlt.sds.model.service.ModelService;
import ai.maum.mlt.sds.model.service.NextTaskService;
import ai.maum.mlt.sds.model.service.TaskService;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/sds/model")
public class TaskController {

  static final Logger logger = LoggerFactory.getLogger(TaskController.class);

  @Autowired
  private ModelService modelService;

  @Autowired
  private TaskService taskService;

  @Autowired
  private NextTaskService nextTaskService;

  @UriRoleDesc(role = "sds/model^C")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task", method = RequestMethod.POST)
  public ResponseEntity<?> insertTask(@PathVariable String workspaceId,
      @PathVariable String modelId, @RequestBody JSONObject bodyParam) {
    logger.info("POST /api/workspace/{}/entity/{}/task", workspaceId, modelId, bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      String taskId = null;
      String taskName = null;
      String taskGoal = "";
      String taskType = "essential";
      String resetSlot = "N";
      String userId = null;
      String taskLocation = "";

      Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

      if (bodyParam.get("id") == null || "".equals(bodyParam.get("id").toString().trim())) {
        result.put("statusCode", 400);
        result.put("statusMessage", "id is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        taskId = bodyParam.get("id").toString().trim();

        Task findTask = null;
        try {
          findTask = taskService.findTaskById(taskId);
        } catch (Exception ex) {
          findTask = taskService.findTaskById(taskId);
        }
        if (findTask != null) {
          result.put("statusCode", 400);
          result.put("statusMessage", "id is already exist.");

          return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }
      }

      if (bodyParam.get("taskName") == null || ""
          .equals(bodyParam.get("taskName").toString().trim())) {
        result.put("statusCode", 400);
        result.put("statusMessage", "taskName is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        taskName = bodyParam.get("taskName").toString().trim();
      }

      if (bodyParam.get("taskGoal") != null && !""
          .equals(bodyParam.get("taskGoal").toString().trim())) {
        taskGoal = bodyParam.get("taskGoal").toString().trim().replace("\"", "'");
      }

      if (bodyParam.get("taskLocation") != null && !""
          .equals(bodyParam.get("taskLocation").toString().trim())) {
        logger.debug(bodyParam.get("taskLocation").toString());
        taskLocation = bodyParam.get("taskLocation").toString().trim();
      }

      if (bodyParam.get("userId") == null || "".equals(bodyParam.get("userId").toString().trim())) {
        result.put("statusCode", 400);
        result.put("statusMessage", "userId is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        userId = bodyParam.get("userId").toString().trim();
      }

      Model findModel = null;
      try {
        findModel = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
      } catch (Exception ex) {
        findModel = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
      }
      if (findModel == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "Model doesn't exist.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      List<Task> taskList = new ArrayList<>();
      try {
        taskList = taskService.findTasksByModel(findModel);
      } catch (Exception ex) {
        taskList = taskService.findTasksByModel(findModel);
      }

      if (!taskName.equals("")) {
        for (int i = 0; i < taskList.size(); i++) {
          if (taskList.get(i).getTaskName().equals(taskName)) {
            result.put("statusCode", 400);
            result.put("statusMessage", "taskName duplicated.");

            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
          }
        }
      }

      Task task = new Task();
      task.setId(taskId);
      task.setTaskName(taskName);
      task.setTaskGoal(taskGoal);
      task.setTaskType(taskType);
      task.setResetSlot(resetSlot);
      task.setModel(findModel);
      task.setTaskLocation(taskLocation);

      task.setWorkspaceId(workspaceId);
      task.setCreatorId(userId);
      task.setCreatedAt(currentTimestamp);
      task.setUpdaterId(userId);
      task.setUpdatedAt(currentTimestamp);

      try {
        taskService.insertTask(task);
      } catch (Exception ex) {
        taskService.insertTask(task);
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("POST /api/workspace/{}/entity/{}/task", workspaceId, modelId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^R")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/allinfolist", method = RequestMethod.GET)
  public ResponseEntity<?> getTaskAllInfoListByModelId(@PathVariable String workspaceId,
      @PathVariable String modelId) {
    logger.info("GET /api/workspace/{}/entity/{}/task/allinfolist", workspaceId, modelId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      Model findModel = null;
      try {
        findModel = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
      } catch (Exception ex) {
        findModel = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
      }
      if (findModel == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "entity 데이터가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      List<Task> taskList = new ArrayList<>();
      try {
        taskList = taskService.findTasksByModel(findModel);
      } catch (Exception ex) {
        taskList = taskService.findTasksByModel(findModel);
      }

      JSONObject wrappedTaskInfo = new JSONObject();
      JSONArray taskInfoList = new JSONArray();

      for (Task task : taskList) {
        JSONObject taskInfo = new JSONObject();

        taskInfo.put("id", task.getId());
        taskInfo.put("taskName", task.getTaskName());
        taskInfo.put("taskGoal", task.getTaskGoal());
        taskInfo.put("modelId", task.getModel().getId());
        taskInfo.put("taskLocation", task.getTaskLocation());

        // next task
        JSONArray nextTaskInfoList = new JSONArray();
        for (NextTask nextTask : task.getNextTaskList()) {
          JSONObject nextTaskInfo = new JSONObject();

          nextTaskInfo.put("id", nextTask.getId());
          nextTaskInfo.put("nextTaskId", nextTask.getNextTaskId());
          nextTaskInfo.put("nextTaskCondition", nextTask.getNextTaskCondition());
          nextTaskInfo.put("taskId", nextTask.getTask().getId());

          nextTaskInfo.put("creatorId", nextTask.getCreatorId());
          nextTaskInfo.put("createdAt", nextTask.getCreatedAt());
          nextTaskInfo.put("updaterId", nextTask.getUpdaterId());
          nextTaskInfo.put("updatedAt", nextTask.getUpdatedAt());

          nextTaskInfoList.add(nextTaskInfo);
        }
        taskInfo.put("nextTask", nextTaskInfoList);

        // start event
        JSONArray startEventInfoList = new JSONArray();
        for (StartEvent startEvent : task.getStartEventList()) {
          JSONObject startEventInfo = new JSONObject();

          startEventInfo.put("id", startEvent.getId());
          startEventInfo.put("startEventCondition", startEvent.getStartEventCondition());
          startEventInfo.put("startEventAction", startEvent.getStartEventAction());
          startEventInfo.put("taskId", startEvent.getTask().getId());

          // start event utter
          JSONArray startEventUtterInfoList = new JSONArray();
          for (StartEventUtter startEventUtter : startEvent.getStartEventUtterList()) {
            JSONObject startEventUtterInfo = new JSONObject();

            startEventUtterInfo.put("id", startEventUtter.getId());
            startEventUtterInfo.put("intentId", startEventUtter.getIntentId());
            startEventUtterInfo.put("extraData", startEventUtter.getExtraData());
            startEventUtterInfo.put("startEventId", startEventUtter.getStartEvent().getId());

            // start event utter data
            JSONArray startEventUtterDataInfoList = new JSONArray();
            for (StartEventUtterData startEventUtterData : startEventUtter
                .getStartEventUtterDataList()) {
              JSONObject startEventUtterDataInfo = new JSONObject();

              startEventUtterDataInfo.put("id", startEventUtterData.getId());
              startEventUtterDataInfo
                  .put("startEventUtterData", startEventUtterData.getStartEventUtterData());
              startEventUtterDataInfo
                  .put("startEventUtterId", startEventUtterData.getStartEventUtter().getId());

              startEventUtterDataInfo.put("creatorId", startEventUtterData.getCreatorId());
              startEventUtterDataInfo.put("createdAt", startEventUtterData.getCreatedAt());
              startEventUtterDataInfo.put("updaterId", startEventUtterData.getUpdaterId());
              startEventUtterDataInfo.put("updatedAt", startEventUtterData.getUpdatedAt());

              startEventUtterDataInfoList.add(startEventUtterDataInfo);
            }
            startEventUtterInfo.put("startEventUtterData", startEventUtterDataInfoList);

            startEventUtterInfo.put("creatorId", startEventUtter.getCreatorId());
            startEventUtterInfo.put("createdAt", startEventUtter.getCreatedAt());
            startEventUtterInfo.put("updaterId", startEventUtter.getUpdaterId());
            startEventUtterInfo.put("updatedAt", startEventUtter.getUpdatedAt());

            startEventUtterInfoList.add(startEventUtterInfo);
          }
          startEventInfo.put("startEventUtter", startEventUtterInfoList);

          startEventInfo.put("creatorId", startEvent.getCreatorId());
          startEventInfo.put("createdAt", startEvent.getCreatedAt());
          startEventInfo.put("updaterId", startEvent.getUpdaterId());
          startEventInfo.put("updatedAt", startEvent.getUpdatedAt());

          startEventInfoList.add(startEventInfo);
        }
        taskInfo.put("startEvent", startEventInfoList);

        // event
        JSONArray eventInfoList = new JSONArray();
        for (Event event : task.getEventList()) {
          JSONObject eventInfo = new JSONObject();

          eventInfo.put("id", event.getId());
          eventInfo.put("intentId", event.getIntentId());
          eventInfo.put("taskId", event.getTask().getId());

          // event condition
          JSONArray eventConditionInfoList = new JSONArray();
          for (EventCondition eventCondition : event.getEventConditionList()) {
            JSONObject eventConditionInfo = new JSONObject();

            eventConditionInfo.put("id", eventCondition.getId());
            eventConditionInfo.put("eventCondition", eventCondition.getEventCondition());
            eventConditionInfo.put("eventAction", eventCondition.getEventAction());
            eventConditionInfo.put("extraData", eventCondition.getExtraData());
            eventConditionInfo.put("eventId", eventCondition.getEvent().getId());

            // event utter
            JSONArray eventUtterInfoList = new JSONArray();
            for (EventUtter eventUtter : eventCondition.getEventUtterList()) {
              JSONObject eventUtterInfo = new JSONObject();

              eventUtterInfo.put("id", eventUtter.getId());
              eventUtterInfo.put("intentId", eventUtter.getIntentId());
              eventUtterInfo.put("extraData", eventUtter.getExtraData());
              eventUtterInfo.put("eventConditionId", eventUtter.getEventCondition().getId());

              // event utter data
              JSONArray eventUtterDataInfoList = new JSONArray();
              for (EventUtterData eventUtterData : eventUtter.getEventUtterDataList()) {
                JSONObject eventUtterDataInfo = new JSONObject();

                eventUtterDataInfo.put("id", eventUtterData.getId());
                eventUtterDataInfo.put("eventUtterData", eventUtterData.getEventUtterData());
                eventUtterDataInfo.put("eventUtterId", eventUtterData.getEventUtter().getId());

                eventUtterDataInfo.put("creatorId", eventUtterData.getCreatorId());
                eventUtterDataInfo.put("createdAt", eventUtterData.getCreatedAt());
                eventUtterDataInfo.put("updaterId", eventUtterData.getUpdaterId());
                eventUtterDataInfo.put("updatedAt", eventUtterData.getUpdatedAt());

                eventUtterDataInfoList.add(eventUtterDataInfo);
              }
              eventUtterInfo.put("eventUtterData", eventUtterDataInfoList);

              eventUtterInfo.put("creatorId", eventUtter.getCreatorId());
              eventUtterInfo.put("createdAt", eventUtter.getCreatedAt());
              eventUtterInfo.put("updaterId", eventUtter.getUpdaterId());
              eventUtterInfo.put("updatedAt", eventUtter.getUpdatedAt());

              eventUtterInfoList.add(eventUtterInfo);
            }
            eventConditionInfo.put("eventUtter", eventUtterInfoList);

            eventConditionInfo.put("creatorId", eventCondition.getCreatorId());
            eventConditionInfo.put("createdAt", eventCondition.getCreatedAt());
            eventConditionInfo.put("updaterId", eventCondition.getUpdaterId());
            eventConditionInfo.put("updatedAt", eventCondition.getUpdatedAt());

            eventConditionInfoList.add(eventConditionInfo);
          }
          eventInfo.put("eventCondition", eventConditionInfoList);

          eventInfo.put("creatorId", event.getCreatorId());
          eventInfo.put("createdAt", event.getCreatedAt());
          eventInfo.put("updaterId", event.getUpdaterId());
          eventInfo.put("updatedAt", event.getUpdatedAt());

          eventInfoList.add(eventInfo);
        }
        taskInfo.put("event", eventInfoList);

        taskInfo.put("workspaceId", task.getWorkspaceId());
        taskInfo.put("creatorId", task.getCreatorId());
        taskInfo.put("createdAt", task.getCreatedAt());
        taskInfo.put("updaterId", task.getUpdaterId());
        taskInfo.put("updatedAt", task.getUpdatedAt());

        taskInfoList.add(taskInfo);
      }

      wrappedTaskInfo.put("task", taskInfoList);

      result.put("data", wrappedTaskInfo);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("GET /api/workspace/{}/entity/{}/task/allinfolist", workspaceId, modelId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^R")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/list", method = RequestMethod.GET)
  public ResponseEntity<?> getTaskListByModelId(@PathVariable String workspaceId,
      @PathVariable String modelId) {
    logger.info("GET /api/workspace/{}/entity/{}/task/list", workspaceId, modelId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      Model findModel = null;
      try {
        findModel = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
      } catch (Exception ex) {
        findModel = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
      }
      if (findModel == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "entity 데이터가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      List<Task> taskList = new ArrayList<>();
      try {
        taskList = taskService.findTasksByModel(findModel);
      } catch (Exception ex) {
        taskList = taskService.findTasksByModel(findModel);
      }

      JSONObject wrappedTaskInfo = new JSONObject();
      JSONArray taskInfoList = new JSONArray();

      for (int i = 0; i < taskList.size(); i++) {
        JSONObject taskInfo = new JSONObject();

        taskInfo.put("id", taskList.get(i).getId());
        taskInfo.put("taskName", taskList.get(i).getTaskName());
        taskInfo.put("taskGoal", taskList.get(i).getTaskGoal());
        taskInfo.put("modelId", taskList.get(i).getModel().getId());
        taskInfo.put("taskLocation", taskList.get(i).getTaskLocation());

        taskInfo.put("workspaceId", taskList.get(i).getWorkspaceId());
        taskInfo.put("creatorId", taskList.get(i).getCreatorId());
        taskInfo.put("createdAt", taskList.get(i).getCreatedAt());
        taskInfo.put("updaterId", taskList.get(i).getUpdaterId());
        taskInfo.put("updatedAt", taskList.get(i).getUpdatedAt());

        taskInfoList.add(taskInfo);
      }

      wrappedTaskInfo.put("taskInfoList", taskInfoList);

      result.put("data", wrappedTaskInfo);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("GET /api/workspace/{}/entity/{}/task/list", workspaceId, modelId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^R")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}", method = RequestMethod.GET)
  public ResponseEntity<?> getTaskByTaskId(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String taskId) {
    logger.info("GET /api/workspace/{}/entity/{}/task/{}", workspaceId, modelId, taskId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      Model model = null;
      try {
        model = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
      } catch (Exception ex) {
        model = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
      }
      if (model == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "entity 데이터가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      Task task = null;
      try {
        task = taskService.findTaskById(taskId);
      } catch (Exception ex) {
        task = taskService.findTaskById(taskId);
      }

      JSONObject wrappedTaskInfo = new JSONObject();
      JSONObject taskInfo = new JSONObject();

      taskInfo.put("id", task.getId());
      taskInfo.put("taskName", task.getTaskName());
      taskInfo.put("taskGoal", task.getTaskGoal());
      taskInfo.put("modelId", task.getModel().getId());
      taskInfo.put("taskLocation", task.getTaskLocation());

      taskInfo.put("workspaceId", task.getWorkspaceId());
      taskInfo.put("creatorId", task.getCreatorId());
      taskInfo.put("createdAt", task.getCreatedAt());
      taskInfo.put("updaterId", task.getUpdaterId());
      taskInfo.put("updatedAt", task.getUpdatedAt());

      wrappedTaskInfo.put("taskInfo", taskInfo);

      result.put("data", wrappedTaskInfo);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("GET /api/workspace/{}/entity/{}/task/{}", workspaceId, modelId, taskId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^U")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}", method = RequestMethod.PUT)
  public ResponseEntity<?> updateTask(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String taskId,
      @RequestBody JSONObject bodyParam) {
    logger.info("PUT /api/workspace/{}/entity/{}/task/{}", workspaceId, modelId, taskId, bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

      String taskName = null;
      String taskGoal = null;
      String userId = null;
      String taskLocation = null;

      if (bodyParam.get("taskName") != null) {
        taskName = bodyParam.get("taskName").toString().trim();
      }

      if (bodyParam.get("taskGoal") != null) {
        taskGoal = bodyParam.get("taskGoal").toString().trim().replace("\"", "'");
      }

      if (bodyParam.get("taskLocation") != null) {
        taskLocation = bodyParam.get("taskLocation").toString().trim();
      }

      if (bodyParam.get("userId") == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "userId 가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        userId = bodyParam.get("userId").toString().trim();
      }

      Task findTask = null;
      try {
        findTask = taskService.findTaskById(taskId);
      } catch (Exception ex) {
        findTask = taskService.findTaskById(taskId);
      }
      if (findTask == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "taskId 와 일치하는 task 가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        if (taskName.equals("")) {
          result.put("statusCode", 400);
          result.put("statusMessage", "taskName 이 없습니다.");

          return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        } else {
          Model findModel = null;
          try {
            findModel = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
          } catch (Exception ex) {
            findModel = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
          }
          if (findModel == null) {
            result.put("statusCode", 400);
            result.put("statusMessage", "Model doesn't exist.");

            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
          }

          List<Task> taskList = new ArrayList<>();
          try {
            taskList = taskService.findTasksByModel(findModel);
          } catch (Exception ex) {
            taskList = taskService.findTasksByModel(findModel);
          }

          if (!taskName.equals(findTask.getTaskName())) {
            for (int i = 0; i < taskList.size(); i++) {
              if (taskList.get(i).getTaskName().equals(taskName)) {
                result.put("statusCode", 400);
                result.put("statusMessage", "taskName duplicated.");

                return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
              }
            }
          }

          findTask.setTaskName(taskName);
        }

        if (taskGoal != null) {
          findTask.setTaskGoal(taskGoal);
        }

        if (taskLocation != null) {
          findTask.setTaskLocation(taskLocation);
        }

        findTask.setUpdaterId(userId);
        findTask.setUpdatedAt(currentTimestamp);
      }

      try {
        taskService.updateTask(findTask);
      } catch (Exception ex) {
        taskService.updateTask(findTask);
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("PUT /api/workspace/{}/entity/{}/task/{}", workspaceId, modelId, taskId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^D")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}", method = RequestMethod.DELETE)
  public ResponseEntity<?> deleteTaskById(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String taskId) {
    logger.info("DELETE /api/workspace/{}/entity/{}/task/{}", workspaceId, modelId, taskId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      Task findTask = null;
      try {
        findTask = taskService.findTaskById(taskId);
      } catch (Exception ex) {
        findTask = taskService.findTaskById(taskId);
      }
      if (findTask == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "taskId 와 일치하는 task 가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        try {
          Model findModel = modelService.findModelById(modelId);
          if (findModel == null) {
            result.put("statusCode", 400);
            result.put("statusMessage", "modelId 와 일치하는 entity 이 없습니다.");

            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
          }

          List<Task> taskList = taskService.findTasksByModel(findModel);
          for (Task tempTask : taskList) {
            for (NextTask tempNextTask : tempTask.getNextTaskList()) {
              if (tempNextTask.getNextTaskId().equals(taskId)) {
                nextTaskService.deleteNextTaskById(tempNextTask.getId());
                break;
              }
            }
          }

          taskService.deleteTaskById(taskId);
        } catch (Exception ex) {
          taskService.deleteTaskById(taskId);
        }
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("DELETE /api/workspace/{}/entity/{}/task/{}", workspaceId, modelId, taskId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
