package ai.maum.mlt.sds.model.repository;

import ai.maum.mlt.sds.model.entity.Event;
import ai.maum.mlt.sds.model.entity.EventCondition;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface EventConditionRepository extends JpaRepository<EventCondition, String> {

  EventCondition findEventConditionById(String eventConditionId);

  List<EventCondition> findAll();

  List<EventCondition> findEventConditionsByEvent(Event event);

  void deleteEventConditionById(String eventConditionId);
}
