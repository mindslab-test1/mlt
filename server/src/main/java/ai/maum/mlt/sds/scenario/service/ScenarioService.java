package ai.maum.mlt.sds.scenario.service;

import ai.maum.mlt.sds.scenario.entity.ScenarioEntity;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Collection;
import java.util.List;

@Transactional
public interface ScenarioService {

  ScenarioEntity updateSkill(String workspaceId, ScenarioEntity scenarioEntity);

  ScenarioEntity getSkill(String id);

  Page<ScenarioEntity> getAllSkills(ScenarioEntity scenarioEntity);

  Page<ScenarioEntity> getSubSkills(ScenarioEntity scenarioEntity, List<String> skillList);

  ScenarioEntity getImportSkills(String workspaceId, String id);

  void deleteCheckedSkill(String id);
}