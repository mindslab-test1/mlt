package ai.maum.mlt.sds.model.controller;


import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.sds.model.entity.Entity;
import ai.maum.mlt.sds.model.entity.EntityData;
import ai.maum.mlt.sds.model.entity.EntityDataSynonym;
import ai.maum.mlt.sds.model.entity.Event;
import ai.maum.mlt.sds.model.entity.EventCondition;
import ai.maum.mlt.sds.model.entity.EventUtter;
import ai.maum.mlt.sds.model.entity.EventUtterData;
import ai.maum.mlt.sds.model.entity.Intent;
import ai.maum.mlt.sds.model.entity.IntentUtter;
import ai.maum.mlt.sds.model.entity.IntentUtterData;
import ai.maum.mlt.sds.model.entity.Model;
import ai.maum.mlt.sds.model.entity.NextTask;
import ai.maum.mlt.sds.model.entity.StartEvent;
import ai.maum.mlt.sds.model.entity.StartEventUtter;
import ai.maum.mlt.sds.model.entity.StartEventUtterData;
import ai.maum.mlt.sds.model.entity.Task;
import ai.maum.mlt.sds.model.service.EntityDataService;
import ai.maum.mlt.sds.model.service.EntityDataSynonymService;
import ai.maum.mlt.sds.model.service.EntityService;
import ai.maum.mlt.sds.model.service.EventConditionService;
import ai.maum.mlt.sds.model.service.EventService;
import ai.maum.mlt.sds.model.service.EventUtterDataService;
import ai.maum.mlt.sds.model.service.EventUtterService;
import ai.maum.mlt.sds.model.service.IntentService;
import ai.maum.mlt.sds.model.service.IntentUtterDataService;
import ai.maum.mlt.sds.model.service.IntentUtterService;
import ai.maum.mlt.sds.model.service.ModelService;
import ai.maum.mlt.sds.model.service.NextTaskService;
import ai.maum.mlt.sds.model.service.StartEventService;
import ai.maum.mlt.sds.model.service.StartEventUtterDataService;
import ai.maum.mlt.sds.model.service.StartEventUtterService;
import ai.maum.mlt.sds.model.service.TaskService;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/sds/model")
public class ModelController {

  static final Logger logger = LoggerFactory.getLogger(ModelController.class);

  @Autowired
  private ModelService modelService;

  @Autowired
  private TaskService taskService;

  @Autowired
  private NextTaskService nextTaskService;

  @Autowired
  private StartEventService startEventService;

  @Autowired
  private StartEventUtterService startEventUtterService;

  @Autowired
  private StartEventUtterDataService startEventUtterDataService;

  @Autowired
  private EventService eventService;

  @Autowired
  private EventConditionService eventConditionService;

  @Autowired
  private EventUtterService eventUtterService;

  @Autowired
  private EventUtterDataService eventUtterDataService;

  @Autowired
  private IntentService intentService;

  @Autowired
  private IntentUtterService intentUtterService;

  @Autowired
  private IntentUtterDataService intentUtterDataService;

  @Autowired
  private EntityService entityService;

  @Autowired
  private EntityDataService entityDataService;

  @Autowired
  private EntityDataSynonymService entityDataSynonymService;

  @UriRoleDesc(role = "sds/model^C")
  @RequestMapping(value = "/workspace/{workspaceId}/model", method = RequestMethod.POST)
  public ResponseEntity<?> insertModel(@PathVariable String workspaceId,
      @RequestBody JSONObject bodyParam) {
    logger.info("POST /api/workspace/{}/entity", workspaceId, bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      String modelId = null;
      String modelName = null;
      String description = null;
      String status = "edit";
      String userId = null;
      String lastTrainKey = "";

      Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

      if (bodyParam.get("id") == null || bodyParam.get("id").toString().trim().equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "id is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        modelId = bodyParam.get("id").toString().trim();

        Model findModel = null;
        try {
          findModel = modelService.findModelById(modelId);
        } catch (Exception ex) {
          findModel = modelService.findModelById(modelId);
        }

        if (findModel != null) {
          result.put("statusCode", 400);
          result.put("statusMessage", "id is already exist.");

          return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }
      }

      if (bodyParam.get("modelName") == null || bodyParam.get("modelName").toString().trim()
          .equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "modelName is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        modelName = bodyParam.get("modelName").toString().trim();
      }

      if (bodyParam.get("description") == null || bodyParam.get("description").toString().trim()
          .equals("")) {
        description = "";
      } else {
        description = bodyParam.get("description").toString().trim();
      }

      if (bodyParam.get("userId") == null || bodyParam.get("userId").toString().trim().equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "userId is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        userId = bodyParam.get("userId").toString().trim();
      }

      if (bodyParam.get("lastTrainKey") == null || bodyParam.get("lastTrainKey").toString().trim()
          .equals("")) {
        lastTrainKey = "";
      } else {
        lastTrainKey = bodyParam.get("lastTrainKey").toString().trim();
      }

      Model findModel = null;
      try {
        findModel = modelService
            .findModelByWorkspaceIdAndCreatorIdAndModelName(workspaceId, userId, modelName);
      } catch (Exception ex) {
        findModel = modelService
            .findModelByWorkspaceIdAndCreatorIdAndModelName(workspaceId, userId, modelName);
      }

      if (findModel != null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "동일한 modelName 이 있습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      Model model = new Model();
      model.setId(modelId);
      model.setModelName(modelName);
      model.setDescription(description);
      model.setStatus(status);
      model.setLastTrainKey(lastTrainKey);

      model.setWorkspaceId(workspaceId);
      model.setCreatorId(userId);
      model.setCreatedAt(currentTimestamp);
      model.setUpdaterId(userId);
      model.setUpdatedAt(currentTimestamp);

      Task globalTask = new Task();
      globalTask.setId(UUID.randomUUID().toString());
      globalTask.setTaskName("GLOBAL_TASK");
      globalTask.setTaskGoal("");
      globalTask.setTaskType("essential");
      globalTask.setResetSlot("N");
      globalTask.setModel(model);

      globalTask.setWorkspaceId(workspaceId);
      globalTask.setCreatorId(userId);
      globalTask.setCreatedAt(currentTimestamp);
      globalTask.setUpdaterId(userId);
      globalTask.setUpdatedAt(currentTimestamp);

      Task greetTask = new Task();
      greetTask.setId(UUID.randomUUID().toString());
      greetTask.setTaskName("Greet");
      greetTask.setTaskGoal("");
      greetTask.setTaskType("essential");
      greetTask.setResetSlot("N");
      greetTask.setModel(model);

      greetTask.setWorkspaceId(workspaceId);
      greetTask.setCreatorId(userId);
      greetTask.setCreatedAt(currentTimestamp);
      greetTask.setUpdaterId(userId);
      greetTask.setUpdatedAt(currentTimestamp);

      List<String> defaultIntentName = new ArrayList<>();
      defaultIntentName.add("inform");
      defaultIntentName.add("request");
      defaultIntentName.add("confirm");
      defaultIntentName.add("affirm");
      defaultIntentName.add("negate");
      defaultIntentName.add("ack");
      defaultIntentName.add("thankyou");
      defaultIntentName.add("welcome");
      defaultIntentName.add("hello");
      defaultIntentName.add("bye");

      List<Intent> defaultIntentList = new ArrayList<>();
      for (int i = 0; i < defaultIntentName.size(); i++) {
        Intent intent = new Intent();
        intent.setId(UUID.randomUUID().toString());
        intent.setIntentName(defaultIntentName.get(i));
        intent.setSharedYn("N");

        intent.setWorkspaceId(workspaceId);
        intent.setCreatorId(userId);
        intent.setCreatedAt(currentTimestamp);
        intent.setUpdaterId(userId);
        intent.setUpdatedAt(currentTimestamp);

//                entity.addIntent(intent);
        defaultIntentList.add(intent);
//                intent.addModel(entity);
      }

      try {
        modelService.insertModel(model);

        //taskService.insertTask(globalTask); 글로벌 타스크 사용안함(정확한 정의후 넣을지 고민)
        taskService.insertTask(greetTask);
        for (int i = 0; i < defaultIntentList.size(); i++) {
          defaultIntentList.get(i).addModel(model);
          intentService.insertIntent(defaultIntentList.get(i));

          model.addIntent(defaultIntentList.get(i));
        }

        modelService.updateModel(model);
      } catch (Exception ex) {
        modelService.insertModel(model);

        //taskService.insertTask(globalTask); 글로벌 타스크 사용안함(정확한 정의후 넣을지 고민)
        taskService.insertTask(greetTask);
        for (int i = 0; i < defaultIntentList.size(); i++) {
          defaultIntentList.get(i).addModel(model);
          intentService.insertIntent(defaultIntentList.get(i));

          model.addIntent(defaultIntentList.get(i));
        }

        modelService.updateModel(model);
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("POST /api/workspace/{}/entity", workspaceId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^C")
  @RequestMapping(value = "/workspace/{workspaceId}/copymodel", method = RequestMethod.POST)
  public ResponseEntity<?> insertCopyModel(@PathVariable String workspaceId,
      @RequestBody JSONObject bodyParam) {
    logger.info("POST /api/workspace/{}/copymodel", workspaceId, bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      String sourceModelId = null;
      String modelName = null;
      String description = null;
      String status = "edit";
      String userId = null;

      Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

      if (bodyParam.get("sourceModelId") == null || bodyParam.get("sourceModelId").toString().trim()
          .equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "sourceModelId is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        sourceModelId = bodyParam.get("sourceModelId").toString().trim();
      }

      if (bodyParam.get("modelName") == null || bodyParam.get("modelName").toString().trim()
          .equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "modelName is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        modelName = bodyParam.get("modelName").toString().trim();
      }

      if (bodyParam.get("description") == null || bodyParam.get("description").toString().trim()
          .equals("")) {
        description = "";
      } else {
        description = bodyParam.get("description").toString().trim();
      }

      if (bodyParam.get("userId") == null || bodyParam.get("userId").toString().trim().equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "userId is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        userId = bodyParam.get("userId").toString().trim();
      }

      Model sourceModel = null;
      try {
        sourceModel = modelService.findModelById(sourceModelId);
      } catch (Exception ex) {
        sourceModel = modelService.findModelById(sourceModelId);
      }

      if (sourceModel == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "복사하려는 모델의 정보가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      Model findModel = null;
      try {
        findModel = modelService
            .findModelByWorkspaceIdAndCreatorIdAndModelName(workspaceId, userId, modelName);
      } catch (Exception ex) {
        findModel = modelService
            .findModelByWorkspaceIdAndCreatorIdAndModelName(workspaceId, userId, modelName);
      }

      if (findModel != null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "동일한 modelName 이 있습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      JSONObject jsonModel = new JSONObject();

      jsonModel = (JSONObject) getModelAllInfoToJson(sourceModel).get("modelInfo");

      SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

      HashMap<String, String> keyChangeMap = new HashMap<>();
      keyChangeMap.put(jsonModel.get("id").toString(), UUID.randomUUID().toString());

      Model model = new Model();
      model.setId(keyChangeMap.get(jsonModel.get("id").toString()));
      model.setModelName(modelName);
      model.setDescription(description);
      model.setStatus(jsonModel.get("status").toString());
      model.setLastTrainKey("");

      model.setWorkspaceId(workspaceId);
      model.setCreatorId(userId);
      model.setCreatedAt(currentTimestamp);
      model.setUpdaterId(userId);
      model.setUpdatedAt(currentTimestamp);

      modelService.insertModel(model);

      // Entity List
      JSONArray jsonEntityList = (JSONArray) jsonModel.get("entity");

      List<Entity> entityList = new ArrayList<>();
      for (Object jsonEntity : jsonEntityList) {
        Entity entity = new Entity();

        if (((JSONObject) jsonEntity).get("sharedYn").equals("Y")) {
          keyChangeMap.put(((JSONObject) jsonEntity).get("id").toString(),
              ((JSONObject) jsonEntity).get("id").toString());
        } else {
          keyChangeMap
              .put(((JSONObject) jsonEntity).get("id").toString(), UUID.randomUUID().toString());
        }
        entity.setId(keyChangeMap.get(((JSONObject) jsonEntity).get("id").toString()));
        entity.setClassName(((JSONObject) jsonEntity).get("className").toString());
        entity.setClassSource("SYSTEM");
        entity.setEntityName(((JSONObject) jsonEntity).get("entityName").toString());
        entity.setEntitySource("KB");
        entity.setEntityType(((JSONObject) jsonEntity).get("entityType").toString());
        entity.setSharedYn(((JSONObject) jsonEntity).get("sharedYn").toString());

        if (((JSONObject) jsonEntity).get("sharedYn").equals("Y")) {
          entity.setCreatorId(((JSONObject) jsonEntity).get("creatorId").toString());
          entity.setCreatedAt(new Timestamp(
              dateFormat.parse(((JSONObject) jsonEntity).get("createdAt").toString()).getTime()));
          entity.setUpdaterId(((JSONObject) jsonEntity).get("updaterId").toString());
          entity.setUpdatedAt(new Timestamp(
              dateFormat.parse(((JSONObject) jsonEntity).get("updatedAt").toString()).getTime()));
          entity.setWorkspaceId(((JSONObject) jsonEntity).get("workspaceId").toString());
        } else {
          entity.setCreatorId(userId);
          entity.setCreatedAt(currentTimestamp);
          entity.setUpdaterId(userId);
          entity.setUpdatedAt(currentTimestamp);
          entity.setWorkspaceId(workspaceId);
        }

        if (((JSONObject) jsonEntity).get("sharedYn").equals("Y") == false) {
          entityService.insertEntity(entity);
        }

        // Entity Data
        JSONArray jsonEntityDataList = (JSONArray) ((JSONObject) jsonEntity).get("entityData");
        List<EntityData> entityDataList = new ArrayList<>();
        for (Object jsonEntityData : jsonEntityDataList) {
          EntityData entityData = new EntityData();

          if (((JSONObject) jsonEntity).get("sharedYn").equals("Y")) {
            keyChangeMap.put(((JSONObject) jsonEntityData).get("id").toString(),
                ((JSONObject) jsonEntityData).get("id").toString());
          } else {
            keyChangeMap.put(((JSONObject) jsonEntityData).get("id").toString(),
                UUID.randomUUID().toString());
          }

          entityData.setId(keyChangeMap.get(((JSONObject) jsonEntityData).get("id").toString()));
          entityData.setEntityData(((JSONObject) jsonEntityData).get("entityData").toString());

          if (((JSONObject) jsonEntity).get("sharedYn").equals("Y")) {
            entityData.setCreatorId(((JSONObject) jsonEntityData).get("creatorId").toString());
            entityData.setCreatedAt(new Timestamp(
                dateFormat.parse(((JSONObject) jsonEntityData).get("createdAt").toString())
                    .getTime()));
            entityData.setUpdaterId(((JSONObject) jsonEntityData).get("updaterId").toString());
            entityData.setUpdatedAt(new Timestamp(
                dateFormat.parse(((JSONObject) jsonEntityData).get("updatedAt").toString())
                    .getTime()));
          } else {
            entityData.setCreatorId(userId);
            entityData.setCreatedAt(currentTimestamp);
            entityData.setUpdaterId(userId);
            entityData.setUpdatedAt(currentTimestamp);
          }

          entityData.setEntity(entity);

          if (((JSONObject) jsonEntity).get("sharedYn").equals("Y") == false) {
            entityDataService.insertEntityData(entityData);
          }

          // Entity Data Synonym
          JSONArray jsonEntityDataSynonymList = (JSONArray) ((JSONObject) jsonEntityData)
              .get("entityDataSynonym");
          List<EntityDataSynonym> entityDataSynonymList = new ArrayList<>();
          for (Object jsonEntityDataSynonym : jsonEntityDataSynonymList) {
            EntityDataSynonym entityDataSynonym = new EntityDataSynonym();

            if (((JSONObject) jsonEntity).get("sharedYn").equals("Y")) {
              keyChangeMap.put(((JSONObject) jsonEntityDataSynonym).get("id").toString(),
                  ((JSONObject) jsonEntityDataSynonym).get("id").toString());
            } else {
              keyChangeMap.put(((JSONObject) jsonEntityDataSynonym).get("id").toString(),
                  UUID.randomUUID().toString());
            }

            entityDataSynonym
                .setId(keyChangeMap.get(((JSONObject) jsonEntityDataSynonym).get("id").toString()));
            entityDataSynonym.setEntityDataSynonym(
                ((JSONObject) jsonEntityDataSynonym).get("entityDataSynonym").toString());

            if (((JSONObject) jsonEntity).get("sharedYn").equals("Y")) {
              entityDataSynonym
                  .setCreatorId(((JSONObject) jsonEntityDataSynonym).get("creatorId").toString());
              entityDataSynonym.setCreatedAt(new Timestamp(
                  dateFormat.parse(((JSONObject) jsonEntityDataSynonym).get("createdAt").toString())
                      .getTime()));
              entityDataSynonym
                  .setUpdaterId(((JSONObject) jsonEntityDataSynonym).get("updaterId").toString());
              entityDataSynonym.setUpdatedAt(new Timestamp(
                  dateFormat.parse(((JSONObject) jsonEntityDataSynonym).get("updatedAt").toString())
                      .getTime()));
            } else {
              entityDataSynonym.setCreatorId(userId);
              entityDataSynonym.setCreatedAt(currentTimestamp);
              entityDataSynonym.setUpdaterId(userId);
              entityDataSynonym.setUpdatedAt(currentTimestamp);
            }

            entityDataSynonym.setEntityData(entityData);

            entityDataSynonymList.add(entityDataSynonym);

            if (((JSONObject) jsonEntity).get("sharedYn").equals("Y") == false) {
              entityDataSynonymService.insertEntityDataSynonym(entityDataSynonym);
            }
          }

          entityData.setEntityDataSynonymList(entityDataSynonymList);

          entityDataList.add(entityData);

          if (((JSONObject) jsonEntity).get("sharedYn").equals("Y") == false) {
            entityDataService.updateEntityData(entityData);
          }
        }

        entity.setEntityDataList(entityDataList);

        entityList.add(entity);

        if (((JSONObject) jsonEntity).get("sharedYn").equals("Y") == false) {
          entityService.updateEntity(entity);
        }
      }
      model.setEntityList(entityList);

      // Intent List
      JSONArray jsonIntentList = (JSONArray) jsonModel.get("intent");

      List<Intent> intentList = new ArrayList<>();
      for (Object jsonIntent : jsonIntentList) {
        Intent intent = new Intent();

        if (((JSONObject) jsonIntent).get("sharedYn").equals("Y")) {
          keyChangeMap.put(((JSONObject) jsonIntent).get("id").toString(),
              ((JSONObject) jsonIntent).get("id").toString());
        } else {
          keyChangeMap
              .put(((JSONObject) jsonIntent).get("id").toString(), UUID.randomUUID().toString());
        }

        intent.setId(keyChangeMap.get(((JSONObject) jsonIntent).get("id").toString()));
        intent.setIntentName(((JSONObject) jsonIntent).get("intentName").toString());
        intent.setSharedYn(((JSONObject) jsonIntent).get("sharedYn").toString());

        if (((JSONObject) jsonIntent).get("sharedYn").equals("Y")) {
          intent.setCreatorId(((JSONObject) jsonIntent).get("creatorId").toString());
          intent.setCreatedAt(new Timestamp(
              dateFormat.parse(((JSONObject) jsonIntent).get("createdAt").toString()).getTime()));
          intent.setUpdaterId(((JSONObject) jsonIntent).get("updaterId").toString());
          intent.setUpdatedAt(new Timestamp(
              dateFormat.parse(((JSONObject) jsonIntent).get("updatedAt").toString()).getTime()));
          intent.setWorkspaceId(((JSONObject) jsonIntent).get("workspaceId").toString());
        } else {
          intent.setCreatorId(userId);
          intent.setCreatedAt(currentTimestamp);
          intent.setUpdaterId(userId);
          intent.setUpdatedAt(currentTimestamp);
          intent.setWorkspaceId(workspaceId);
        }

        if (((JSONObject) jsonIntent).get("sharedYn").equals("Y") == false) {
          intentService.insertIntent(intent);
        }

        // Intent Utter
        JSONArray jsonIntentUtterList = (JSONArray) ((JSONObject) jsonIntent).get("intentUtter");
        List<IntentUtter> intentUtterList = new ArrayList<>();
        for (Object jsonIntentUtter : jsonIntentUtterList) {
          IntentUtter intentUtter = new IntentUtter();

          if (((JSONObject) jsonIntent).get("sharedYn").equals("Y")) {
            keyChangeMap.put(((JSONObject) jsonIntentUtter).get("id").toString(),
                ((JSONObject) jsonIntentUtter).get("id").toString());
          } else {
            keyChangeMap.put(((JSONObject) jsonIntentUtter).get("id").toString(),
                UUID.randomUUID().toString());
          }

          intentUtter.setId(keyChangeMap.get(((JSONObject) jsonIntentUtter).get("id").toString()));
          intentUtter.setUserSay(((JSONObject) jsonIntentUtter).get("userSay").toString());

          String mappedUserSay = ((JSONObject) jsonIntentUtter).get("mappedUserSay").toString();
          for (String key : keyChangeMap.keySet()) {
            mappedUserSay = mappedUserSay.replace(key, keyChangeMap.get(key));
          }
          intentUtter.setMappedUserSay(mappedUserSay);

          if (((JSONObject) jsonIntent).get("sharedYn").equals("Y")) {
            intentUtter.setCreatorId(((JSONObject) jsonIntentUtter).get("creatorId").toString());
            intentUtter.setCreatedAt(new Timestamp(
                dateFormat.parse(((JSONObject) jsonIntentUtter).get("createdAt").toString())
                    .getTime()));
            intentUtter.setUpdaterId(((JSONObject) jsonIntentUtter).get("updaterId").toString());
            intentUtter.setUpdatedAt(new Timestamp(
                dateFormat.parse(((JSONObject) jsonIntentUtter).get("updatedAt").toString())
                    .getTime()));
          } else {
            intentUtter.setCreatorId(userId);
            intentUtter.setCreatedAt(currentTimestamp);
            intentUtter.setUpdaterId(userId);
            intentUtter.setUpdatedAt(currentTimestamp);
          }

          intentUtter.setIntent(intent);

          if (((JSONObject) jsonIntent).get("sharedYn").equals("Y") == false) {
            intentUtterService.insertIntentUtter(intentUtter);
          }

          // Intent Utter Data
          JSONArray jsonIntentUtterDataList = (JSONArray) ((JSONObject) jsonIntentUtter)
              .get("intentUtterData");
          List<IntentUtterData> intentUtterDataList = new ArrayList<>();
          for (Object jsonIntentUtterData : jsonIntentUtterDataList) {
            IntentUtterData intentUtterData = new IntentUtterData();

            if (((JSONObject) jsonIntent).get("sharedYn").equals("Y")) {
              keyChangeMap.put(((JSONObject) jsonIntentUtterData).get("id").toString(),
                  ((JSONObject) jsonIntentUtterData).get("id").toString());
            } else {
              keyChangeMap.put(((JSONObject) jsonIntentUtterData).get("id").toString(),
                  UUID.randomUUID().toString());
            }
            intentUtterData
                .setId(keyChangeMap.get(((JSONObject) jsonIntentUtterData).get("id").toString()));
            intentUtterData.setEntityId(
                keyChangeMap.get(((JSONObject) jsonIntentUtterData).get("entityId").toString()));
            intentUtterData
                .setMappedValue(((JSONObject) jsonIntentUtterData).get("mappedValue").toString());

            if (((JSONObject) jsonIntent).get("sharedYn").equals("Y")) {
              intentUtterData
                  .setCreatorId(((JSONObject) jsonIntentUtterData).get("creatorId").toString());
              intentUtterData.setCreatedAt(new Timestamp(
                  dateFormat.parse(((JSONObject) jsonIntentUtterData).get("createdAt").toString())
                      .getTime()));
              intentUtterData
                  .setUpdaterId(((JSONObject) jsonIntentUtterData).get("updaterId").toString());
              intentUtterData.setUpdatedAt(new Timestamp(
                  dateFormat.parse(((JSONObject) jsonIntentUtterData).get("updatedAt").toString())
                      .getTime()));
            } else {
              intentUtterData.setCreatorId(userId);
              intentUtterData.setCreatedAt(currentTimestamp);
              intentUtterData.setUpdaterId(userId);
              intentUtterData.setUpdatedAt(currentTimestamp);
            }

            intentUtterData.setIntentUtter(intentUtter);

            intentUtterDataList.add(intentUtterData);

            if (((JSONObject) jsonIntent).get("sharedYn").equals("Y") == false) {
              intentUtterDataService.insertIntentUtterData(intentUtterData);
            }
          }

          intentUtter.setIntentUtterDataList(intentUtterDataList);

          intentUtterList.add(intentUtter);

          if (((JSONObject) jsonIntent).get("sharedYn").equals("Y") == false) {
            intentUtterService.updateIntentUtter(intentUtter);
          }
        }

        intent.setIntentUtterList(intentUtterList);

        intentList.add(intent);

        if (((JSONObject) jsonIntent).get("sharedYn").equals("Y") == false) {
          intentService.updateIntent(intent);
        }
      }
      model.setIntentList(intentList);

      // Task List
      JSONArray jsonTaskList = (JSONArray) jsonModel.get("task");
      List<Task> taskList = new ArrayList<>();
      for (Object jsonTask : jsonTaskList) {
        Task task = new Task();

        keyChangeMap
            .put(((JSONObject) jsonTask).get("id").toString(), UUID.randomUUID().toString());

        task.setId(keyChangeMap.get(((JSONObject) jsonTask).get("id").toString()));
        task.setTaskName(((JSONObject) jsonTask).get("taskName").toString());
        task.setTaskGoal(((JSONObject) jsonTask).get("taskGoal").toString());
        task.setTaskType("essential");
        task.setResetSlot("N");
        task.setModel(model);

        task.setCreatorId(userId);
        task.setCreatedAt(currentTimestamp);
        task.setUpdaterId(userId);
        task.setUpdatedAt(currentTimestamp);
        task.setWorkspaceId(workspaceId);

        taskList.add(task);

        taskService.insertTask(task);
      }

      for (Object jsonTask : jsonTaskList) {
        // Next Task
        JSONArray jsonNextTaskList = (JSONArray) ((JSONObject) jsonTask).get("nextTask");
        List<NextTask> nextTaskList = new ArrayList<>();
        for (Object jsonNextTask : jsonNextTaskList) {
          NextTask nextTask = new NextTask();

          keyChangeMap
              .put(((JSONObject) jsonNextTask).get("id").toString(), UUID.randomUUID().toString());

          nextTask.setId(keyChangeMap.get(((JSONObject) jsonNextTask).get("id").toString()));
          nextTask.setNextTaskId(
              keyChangeMap.get(((JSONObject) jsonNextTask).get("nextTaskId").toString()));
          nextTask.setNextTaskCondition(
              ((JSONObject) jsonNextTask).get("nextTaskCondition").toString());
          nextTask.setController("system");

          nextTask.setCreatorId(userId);
          nextTask.setCreatedAt(currentTimestamp);
          nextTask.setUpdaterId(userId);
          nextTask.setUpdatedAt(currentTimestamp);

          for (Task task : taskList) {
            if (task.getId()
                .equals(keyChangeMap.get(((JSONObject) jsonTask).get("id").toString()))) {
              nextTask.setTask(task);
              break;
            }
          }

          nextTaskService.insertNextTask(nextTask);
        }

        // Start Event
        JSONArray jsonStartEventList = (JSONArray) ((JSONObject) jsonTask).get("startEvent");
        List<StartEvent> startEventList = new ArrayList<>();
        for (Object jsonStartEvent : jsonStartEventList) {
          StartEvent startEvent = new StartEvent();

          keyChangeMap.put(((JSONObject) jsonStartEvent).get("id").toString(),
              UUID.randomUUID().toString());

          startEvent.setId(keyChangeMap.get(((JSONObject) jsonStartEvent).get("id").toString()));
          startEvent.setStartEventCondition(
              ((JSONObject) jsonStartEvent).get("startEventCondition").toString());
          startEvent.setStartEventAction(
              ((JSONObject) jsonStartEvent).get("startEventAction").toString());

          startEvent.setCreatorId(userId);
          startEvent.setCreatedAt(currentTimestamp);
          startEvent.setUpdaterId(userId);
          startEvent.setUpdatedAt(currentTimestamp);

          for (Task task : taskList) {
            if (task.getId()
                .equals(keyChangeMap.get(((JSONObject) jsonTask).get("id").toString()))) {
              startEvent.setTask(task);
              break;
            }
          }

          startEventService.insertStartEvent(startEvent);

          // Start Event Utter
          JSONArray jsonStartEventUtterList = (JSONArray) ((JSONObject) jsonStartEvent)
              .get("startEventUtter");
          List<StartEventUtter> startEventUtterList = new ArrayList<>();
          for (Object jsonStartEventUtter : jsonStartEventUtterList) {
            StartEventUtter startEventUtter = new StartEventUtter();

            keyChangeMap.put(((JSONObject) jsonStartEventUtter).get("id").toString(),
                UUID.randomUUID().toString());

            startEventUtter
                .setId(keyChangeMap.get(((JSONObject) jsonStartEventUtter).get("id").toString()));
            startEventUtter.setIntentId(
                keyChangeMap.get(((JSONObject) jsonStartEventUtter).get("intentId").toString()));
            startEventUtter
                .setExtraData(((JSONObject) jsonStartEventUtter).get("extraData").toString());

            startEventUtter.setCreatorId(userId);
            startEventUtter.setCreatedAt(currentTimestamp);
            startEventUtter.setUpdaterId(userId);
            startEventUtter.setUpdatedAt(currentTimestamp);

            startEventUtter.setStartEvent(startEvent);

            startEventUtterService.insertStartEventUtter(startEventUtter);

            // Start Event Utter Data
            JSONArray jsonStartEventUtterDataList = (JSONArray) ((JSONObject) jsonStartEventUtter)
                .get("startEventUtterData");
            List<StartEventUtterData> startEventUtterDataList = new ArrayList<>();
            for (Object jsonStartEventUtterData : jsonStartEventUtterDataList) {
              StartEventUtterData startEventUtterData = new StartEventUtterData();

              keyChangeMap.put(((JSONObject) jsonStartEventUtterData).get("id").toString(),
                  UUID.randomUUID().toString());

              startEventUtterData.setId(
                  keyChangeMap.get(((JSONObject) jsonStartEventUtterData).get("id").toString()));
              startEventUtterData.setStartEventUtterData(
                  ((JSONObject) jsonStartEventUtterData).get("startEventUtterData").toString());

              startEventUtterData.setCreatorId(userId);
              startEventUtterData.setCreatedAt(currentTimestamp);
              startEventUtterData.setUpdaterId(userId);
              startEventUtterData.setUpdatedAt(currentTimestamp);

              startEventUtterData.setStartEventUtter(startEventUtter);

              startEventUtterDataService.insertStartEventUtterData(startEventUtterData);
            }
          }
        }

        // Event
        JSONArray jsonEventList = (JSONArray) ((JSONObject) jsonTask).get("event");
        List<Event> eventList = new ArrayList<>();
        for (Object jsonEvent : jsonEventList) {
          Event event = new Event();

          keyChangeMap
              .put(((JSONObject) jsonEvent).get("id").toString(), UUID.randomUUID().toString());

          event.setId(keyChangeMap.get(((JSONObject) jsonEvent).get("id").toString()));
          event.setIntentId(keyChangeMap.get(((JSONObject) jsonEvent).get("intentId").toString()));

          event.setCreatorId(userId);
          event.setCreatedAt(currentTimestamp);
          event.setUpdaterId(userId);
          event.setUpdatedAt(currentTimestamp);

          for (Task task : taskList) {
            if (task.getId()
                .equals(keyChangeMap.get(((JSONObject) jsonTask).get("id").toString()))) {
              event.setTask(task);
              break;
            }
          }

          eventService.insertEvent(event);

          // Event Condition
          JSONArray jsonEventConditionList = (JSONArray) ((JSONObject) jsonEvent)
              .get("eventCondition");
          List<EventCondition> eventConditionList = new ArrayList<>();
          for (Object jsonEventCondition : jsonEventConditionList) {
            EventCondition eventCondition = new EventCondition();

            keyChangeMap.put(((JSONObject) jsonEventCondition).get("id").toString(),
                UUID.randomUUID().toString());

            eventCondition
                .setId(keyChangeMap.get(((JSONObject) jsonEventCondition).get("id").toString()));
            eventCondition.setEventCondition(
                ((JSONObject) jsonEventCondition).get("eventCondition").toString());
            eventCondition
                .setEventAction(((JSONObject) jsonEventCondition).get("eventAction").toString());
            eventCondition
                .setExtraData(((JSONObject) jsonEventCondition).get("extraData").toString());

            eventCondition.setCreatorId(userId);
            eventCondition.setCreatedAt(currentTimestamp);
            eventCondition.setUpdaterId(userId);
            eventCondition.setUpdatedAt(currentTimestamp);

            eventCondition.setEvent(event);

            eventConditionService.insertEventCondition(eventCondition);

            // Event Utter
            JSONArray jsonEventUtterList = (JSONArray) ((JSONObject) jsonEventCondition)
                .get("eventUtter");
            List<EventUtter> eventUtterList = new ArrayList<>();
            for (Object jsonEventUtter : jsonEventUtterList) {
              EventUtter eventUtter = new EventUtter();

              keyChangeMap.put(((JSONObject) jsonEventUtter).get("id").toString(),
                  UUID.randomUUID().toString());

              eventUtter
                  .setId(keyChangeMap.get(((JSONObject) jsonEventUtter).get("id").toString()));
              eventUtter.setIntentId(
                  keyChangeMap.get(((JSONObject) jsonEventUtter).get("intentId").toString()));
              eventUtter.setExtraData(((JSONObject) jsonEventUtter).get("extraData").toString());

              eventUtter.setCreatorId(userId);
              eventUtter.setCreatedAt(currentTimestamp);
              eventUtter.setUpdaterId(userId);
              eventUtter.setUpdatedAt(currentTimestamp);

              eventUtter.setEventCondition(eventCondition);

              eventUtterService.insertEventUtter(eventUtter);

              // Event Utter Data
              JSONArray jsonEventUtterDataList = (JSONArray) ((JSONObject) jsonEventUtter)
                  .get("eventUtterData");
              List<EventUtterData> eventUtterDataList = new ArrayList<>();
              for (Object jsonEventUtterData : jsonEventUtterDataList) {
                EventUtterData eventUtterData = new EventUtterData();

                keyChangeMap.put(((JSONObject) jsonEventUtterData).get("id").toString(),
                    UUID.randomUUID().toString());

                eventUtterData.setId(
                    keyChangeMap.get(((JSONObject) jsonEventUtterData).get("id").toString()));
                eventUtterData.setEventUtterData(
                    ((JSONObject) jsonEventUtterData).get("eventUtterData").toString());

                eventUtterData.setCreatorId(userId);
                eventUtterData.setCreatedAt(currentTimestamp);
                eventUtterData.setUpdaterId(userId);
                eventUtterData.setUpdatedAt(currentTimestamp);

                eventUtterData.setEventUtter(eventUtter);

                eventUtterDataService.insertEventUtterData(eventUtterData);
              }
            }
          }
        }
      }
      model.setTaskList(taskList);

      modelService.updateModel(model);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("POST /api/workspace/{}/copymodel", workspaceId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^C")
  @RequestMapping(value = "/workspace/{workspaceId}/uploadmodel", method = RequestMethod.POST)
  public ResponseEntity<?> insertUploadModel(@PathVariable String workspaceId,
      @RequestBody JSONObject bodyParam) {
    logger.info("POST /api/workspace/{}/uploadmodel", workspaceId, bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      String modelName = null;
      String description = null;
      String status = "edit";
      String userId = null;

      Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

      if (bodyParam.get("modelName") == null || bodyParam.get("modelName").toString().trim()
          .equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "modelName is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        modelName = bodyParam.get("modelName").toString().trim();
      }

      if (bodyParam.get("description") == null || bodyParam.get("description").toString().trim()
          .equals("")) {
        description = "";
      } else {
        description = bodyParam.get("description").toString().trim();
      }

      if (bodyParam.get("userId") == null || bodyParam.get("userId").toString().trim().equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "userId is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        userId = bodyParam.get("userId").toString().trim();
      }

      Model findModel = null;
      try {
        findModel = modelService
            .findModelByWorkspaceIdAndCreatorIdAndModelName(workspaceId, userId, modelName);
      } catch (Exception ex) {
        findModel = modelService
            .findModelByWorkspaceIdAndCreatorIdAndModelName(workspaceId, userId, modelName);
      }

      if (findModel != null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "동일한 modelName 이 있습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      JSONObject jsonModel = new JSONObject();

      if (bodyParam.get("modelInfo") == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "Model 정보가 없습니다..");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        JSONParser jsonParser = new JSONParser();
        jsonModel = (JSONObject) ((JSONObject) jsonParser.parse(bodyParam.toJSONString()))
            .get("modelInfo");
      }

      SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

      HashMap<String, String> keyChangeMap = new HashMap<>();
      keyChangeMap.put(jsonModel.get("id").toString(), UUID.randomUUID().toString());

      Model model = new Model();
      model.setId(keyChangeMap.get(jsonModel.get("id").toString()));
      model.setModelName(modelName);
      model.setDescription(description);
      model.setStatus(jsonModel.get("status").toString());
      model.setLastTrainKey("");

      model.setWorkspaceId(workspaceId);
      model.setCreatorId(userId);
      model.setCreatedAt(currentTimestamp);
      model.setUpdaterId(userId);
      model.setUpdatedAt(currentTimestamp);

      modelService.insertModel(model);

      // Entity List
      JSONArray jsonEntityList = (JSONArray) jsonModel.get("entity");

      List<Entity> entityList = new ArrayList<>();
      for (Object jsonEntity : jsonEntityList) {
        Entity entity = new Entity();

        if (((JSONObject) jsonEntity).get("sharedYn").equals("Y")) {
          keyChangeMap.put(((JSONObject) jsonEntity).get("id").toString(),
              ((JSONObject) jsonEntity).get("id").toString());
        } else {
          keyChangeMap
              .put(((JSONObject) jsonEntity).get("id").toString(), UUID.randomUUID().toString());
        }
        entity.setId(keyChangeMap.get(((JSONObject) jsonEntity).get("id").toString()));
        entity.setClassName(((JSONObject) jsonEntity).get("className").toString());
        entity.setClassSource("SYSTEM");
        entity.setEntityName(((JSONObject) jsonEntity).get("entityName").toString());
        entity.setEntitySource("KB");
        entity.setEntityType(((JSONObject) jsonEntity).get("entityType").toString());
        entity.setSharedYn(((JSONObject) jsonEntity).get("sharedYn").toString());

        if (((JSONObject) jsonEntity).get("sharedYn").equals("Y")) {
          entity.setCreatorId(((JSONObject) jsonEntity).get("creatorId").toString());
          entity.setCreatedAt(new Timestamp(
              dateFormat.parse(((JSONObject) jsonEntity).get("createdAt").toString()).getTime()));
          entity.setUpdaterId(((JSONObject) jsonEntity).get("updaterId").toString());
          entity.setUpdatedAt(new Timestamp(
              dateFormat.parse(((JSONObject) jsonEntity).get("updatedAt").toString()).getTime()));
          entity.setWorkspaceId(((JSONObject) jsonEntity).get("workspaceId").toString());
        } else {
          entity.setCreatorId(userId);
          entity.setCreatedAt(currentTimestamp);
          entity.setUpdaterId(userId);
          entity.setUpdatedAt(currentTimestamp);
          entity.setWorkspaceId(workspaceId);
        }

        entity.addModel(model);

        if (((JSONObject) jsonEntity).get("sharedYn").equals("Y") == false) {
          entityService.insertEntity(entity);
        } else {
          entityService.updateEntity(entity);
        }

        // Entity Data
        JSONArray jsonEntityDataList = (JSONArray) ((JSONObject) jsonEntity).get("entityData");
        List<EntityData> entityDataList = new ArrayList<>();
        for (Object jsonEntityData : jsonEntityDataList) {
          EntityData entityData = new EntityData();

          if (((JSONObject) jsonEntity).get("sharedYn").equals("Y")) {
            keyChangeMap.put(((JSONObject) jsonEntityData).get("id").toString(),
                ((JSONObject) jsonEntityData).get("id").toString());
          } else {
            keyChangeMap.put(((JSONObject) jsonEntityData).get("id").toString(),
                UUID.randomUUID().toString());
          }

          entityData.setId(keyChangeMap.get(((JSONObject) jsonEntityData).get("id").toString()));
          entityData.setEntityData(((JSONObject) jsonEntityData).get("entityData").toString());

          if (((JSONObject) jsonEntity).get("sharedYn").equals("Y")) {
            entityData.setCreatorId(((JSONObject) jsonEntityData).get("creatorId").toString());
            entityData.setCreatedAt(new Timestamp(
                dateFormat.parse(((JSONObject) jsonEntityData).get("createdAt").toString())
                    .getTime()));
            entityData.setUpdaterId(((JSONObject) jsonEntityData).get("updaterId").toString());
            entityData.setUpdatedAt(new Timestamp(
                dateFormat.parse(((JSONObject) jsonEntityData).get("updatedAt").toString())
                    .getTime()));
          } else {
            entityData.setCreatorId(userId);
            entityData.setCreatedAt(currentTimestamp);
            entityData.setUpdaterId(userId);
            entityData.setUpdatedAt(currentTimestamp);
          }

          entityData.setEntity(entity);

          if (((JSONObject) jsonEntity).get("sharedYn").equals("Y") == false) {
            entityDataService.insertEntityData(entityData);
          }

          // Entity Data Synonym
          JSONArray jsonEntityDataSynonymList = (JSONArray) ((JSONObject) jsonEntityData)
              .get("entityDataSynonym");
          List<EntityDataSynonym> entityDataSynonymList = new ArrayList<>();
          for (Object jsonEntityDataSynonym : jsonEntityDataSynonymList) {
            EntityDataSynonym entityDataSynonym = new EntityDataSynonym();

            if (((JSONObject) jsonEntity).get("sharedYn").equals("Y")) {
              keyChangeMap.put(((JSONObject) jsonEntityDataSynonym).get("id").toString(),
                  ((JSONObject) jsonEntityDataSynonym).get("id").toString());
            } else {
              keyChangeMap.put(((JSONObject) jsonEntityDataSynonym).get("id").toString(),
                  UUID.randomUUID().toString());
            }

            entityDataSynonym
                .setId(keyChangeMap.get(((JSONObject) jsonEntityDataSynonym).get("id").toString()));
            entityDataSynonym.setEntityDataSynonym(
                ((JSONObject) jsonEntityDataSynonym).get("entityDataSynonym").toString());

            if (((JSONObject) jsonEntity).get("sharedYn").equals("Y")) {
              entityDataSynonym
                  .setCreatorId(((JSONObject) jsonEntityDataSynonym).get("creatorId").toString());
              entityDataSynonym.setCreatedAt(new Timestamp(
                  dateFormat.parse(((JSONObject) jsonEntityDataSynonym).get("createdAt").toString())
                      .getTime()));
              entityDataSynonym
                  .setUpdaterId(((JSONObject) jsonEntityDataSynonym).get("updaterId").toString());
              entityDataSynonym.setUpdatedAt(new Timestamp(
                  dateFormat.parse(((JSONObject) jsonEntityDataSynonym).get("updatedAt").toString())
                      .getTime()));
            } else {
              entityDataSynonym.setCreatorId(userId);
              entityDataSynonym.setCreatedAt(currentTimestamp);
              entityDataSynonym.setUpdaterId(userId);
              entityDataSynonym.setUpdatedAt(currentTimestamp);
            }

            entityDataSynonym.setEntityData(entityData);

            entityDataSynonymList.add(entityDataSynonym);

            if (((JSONObject) jsonEntity).get("sharedYn").equals("Y") == false) {
              entityDataSynonymService.insertEntityDataSynonym(entityDataSynonym);
            }
          }

          entityData.setEntityDataSynonymList(entityDataSynonymList);

          entityDataList.add(entityData);

          if (((JSONObject) jsonEntity).get("sharedYn").equals("Y") == false) {
            entityDataService.updateEntityData(entityData);
          }
        }

        entity.setEntityDataList(entityDataList);

        entityList.add(entity);

        if (((JSONObject) jsonEntity).get("sharedYn").equals("Y") == false) {
          entityService.updateEntity(entity);
        }
      }
      model.setEntityList(entityList);

      // Intent List
      JSONArray jsonIntentList = (JSONArray) jsonModel.get("intent");

      List<Intent> intentList = new ArrayList<>();
      for (Object jsonIntent : jsonIntentList) {
        Intent intent = new Intent();

        if (((JSONObject) jsonIntent).get("sharedYn").equals("Y")) {
          keyChangeMap.put(((JSONObject) jsonIntent).get("id").toString(),
              ((JSONObject) jsonIntent).get("id").toString());
        } else {
          keyChangeMap
              .put(((JSONObject) jsonIntent).get("id").toString(), UUID.randomUUID().toString());
        }

        intent.setId(keyChangeMap.get(((JSONObject) jsonIntent).get("id").toString()));
        intent.setIntentName(((JSONObject) jsonIntent).get("intentName").toString());
        intent.setSharedYn(((JSONObject) jsonIntent).get("sharedYn").toString());

        if (((JSONObject) jsonIntent).get("sharedYn").equals("Y")) {
          intent.setCreatorId(((JSONObject) jsonIntent).get("creatorId").toString());
          intent.setCreatedAt(new Timestamp(
              dateFormat.parse(((JSONObject) jsonIntent).get("createdAt").toString()).getTime()));
          intent.setUpdaterId(((JSONObject) jsonIntent).get("updaterId").toString());
          intent.setUpdatedAt(new Timestamp(
              dateFormat.parse(((JSONObject) jsonIntent).get("updatedAt").toString()).getTime()));
          intent.setWorkspaceId(((JSONObject) jsonIntent).get("workspaceId").toString());
        } else {
          intent.setCreatorId(userId);
          intent.setCreatedAt(currentTimestamp);
          intent.setUpdaterId(userId);
          intent.setUpdatedAt(currentTimestamp);
          intent.setWorkspaceId(workspaceId);
        }

        intent.addModel(model);

        if (((JSONObject) jsonIntent).get("sharedYn").equals("Y") == false) {
          intentService.insertIntent(intent);
        } else {
          intentService.updateIntent(intent);
        }

        // Intent Utter
        JSONArray jsonIntentUtterList = (JSONArray) ((JSONObject) jsonIntent).get("intentUtter");
        List<IntentUtter> intentUtterList = new ArrayList<>();
        for (Object jsonIntentUtter : jsonIntentUtterList) {
          IntentUtter intentUtter = new IntentUtter();

          if (((JSONObject) jsonIntent).get("sharedYn").equals("Y")) {
            keyChangeMap.put(((JSONObject) jsonIntentUtter).get("id").toString(),
                ((JSONObject) jsonIntentUtter).get("id").toString());
          } else {
            keyChangeMap.put(((JSONObject) jsonIntentUtter).get("id").toString(),
                UUID.randomUUID().toString());
          }

          intentUtter.setId(keyChangeMap.get(((JSONObject) jsonIntentUtter).get("id").toString()));
          intentUtter.setUserSay(((JSONObject) jsonIntentUtter).get("userSay").toString());

          String mappedUserSay = ((JSONObject) jsonIntentUtter).get("mappedUserSay").toString();
          for (String key : keyChangeMap.keySet()) {
            mappedUserSay = mappedUserSay.replace(key, keyChangeMap.get(key));
          }
          intentUtter.setMappedUserSay(mappedUserSay);

          if (((JSONObject) jsonIntent).get("sharedYn").equals("Y")) {
            intentUtter.setCreatorId(((JSONObject) jsonIntentUtter).get("creatorId").toString());
            intentUtter.setCreatedAt(new Timestamp(
                dateFormat.parse(((JSONObject) jsonIntentUtter).get("createdAt").toString())
                    .getTime()));
            intentUtter.setUpdaterId(((JSONObject) jsonIntentUtter).get("updaterId").toString());
            intentUtter.setUpdatedAt(new Timestamp(
                dateFormat.parse(((JSONObject) jsonIntentUtter).get("updatedAt").toString())
                    .getTime()));
          } else {
            intentUtter.setCreatorId(userId);
            intentUtter.setCreatedAt(currentTimestamp);
            intentUtter.setUpdaterId(userId);
            intentUtter.setUpdatedAt(currentTimestamp);
          }

          intentUtter.setIntent(intent);

          if (((JSONObject) jsonIntent).get("sharedYn").equals("Y") == false) {
            intentUtterService.insertIntentUtter(intentUtter);
          }

          // Intent Utter Data
          JSONArray jsonIntentUtterDataList = (JSONArray) ((JSONObject) jsonIntentUtter)
              .get("intentUtterData");
          List<IntentUtterData> intentUtterDataList = new ArrayList<>();
          for (Object jsonIntentUtterData : jsonIntentUtterDataList) {
            IntentUtterData intentUtterData = new IntentUtterData();

            if (((JSONObject) jsonIntent).get("sharedYn").equals("Y")) {
              keyChangeMap.put(((JSONObject) jsonIntentUtterData).get("id").toString(),
                  ((JSONObject) jsonIntentUtterData).get("id").toString());
            } else {
              keyChangeMap.put(((JSONObject) jsonIntentUtterData).get("id").toString(),
                  UUID.randomUUID().toString());
            }
            intentUtterData
                .setId(keyChangeMap.get(((JSONObject) jsonIntentUtterData).get("id").toString()));
            intentUtterData.setEntityId(
                keyChangeMap.get(((JSONObject) jsonIntentUtterData).get("entityId").toString()));
            intentUtterData
                .setMappedValue(((JSONObject) jsonIntentUtterData).get("mappedValue").toString());

            if (((JSONObject) jsonIntent).get("sharedYn").equals("Y")) {
              intentUtterData
                  .setCreatorId(((JSONObject) jsonIntentUtterData).get("creatorId").toString());
              intentUtterData.setCreatedAt(new Timestamp(
                  dateFormat.parse(((JSONObject) jsonIntentUtterData).get("createdAt").toString())
                      .getTime()));
              intentUtterData
                  .setUpdaterId(((JSONObject) jsonIntentUtterData).get("updaterId").toString());
              intentUtterData.setUpdatedAt(new Timestamp(
                  dateFormat.parse(((JSONObject) jsonIntentUtterData).get("updatedAt").toString())
                      .getTime()));
            } else {
              intentUtterData.setCreatorId(userId);
              intentUtterData.setCreatedAt(currentTimestamp);
              intentUtterData.setUpdaterId(userId);
              intentUtterData.setUpdatedAt(currentTimestamp);
            }

            intentUtterData.setIntentUtter(intentUtter);

            intentUtterDataList.add(intentUtterData);

            if (((JSONObject) jsonIntent).get("sharedYn").equals("Y") == false) {
              intentUtterDataService.insertIntentUtterData(intentUtterData);
            }
          }

          intentUtter.setIntentUtterDataList(intentUtterDataList);

          intentUtterList.add(intentUtter);

          if (((JSONObject) jsonIntent).get("sharedYn").equals("Y") == false) {
            intentUtterService.updateIntentUtter(intentUtter);
          }
        }

        intent.setIntentUtterList(intentUtterList);

        intentList.add(intent);

        if (((JSONObject) jsonIntent).get("sharedYn").equals("Y") == false) {
          intentService.updateIntent(intent);
        }
      }
      model.setIntentList(intentList);

      // Task List
      JSONArray jsonTaskList = (JSONArray) jsonModel.get("task");
      List<Task> taskList = new ArrayList<>();
      for (Object jsonTask : jsonTaskList) {
        Task task = new Task();

        keyChangeMap
            .put(((JSONObject) jsonTask).get("id").toString(), UUID.randomUUID().toString());

        task.setId(keyChangeMap.get(((JSONObject) jsonTask).get("id").toString()));
        task.setTaskName(((JSONObject) jsonTask).get("taskName").toString());
        task.setTaskGoal(((JSONObject) jsonTask).get("taskGoal").toString());
        task.setTaskType("essential");
        task.setResetSlot("N");
        task.setModel(model);

        task.setCreatorId(userId);
        task.setCreatedAt(currentTimestamp);
        task.setUpdaterId(userId);
        task.setUpdatedAt(currentTimestamp);
        task.setWorkspaceId(workspaceId);

        taskList.add(task);

        taskService.insertTask(task);
      }

      for (Object jsonTask : jsonTaskList) {
        // Next Task
        JSONArray jsonNextTaskList = (JSONArray) ((JSONObject) jsonTask).get("nextTask");
        List<NextTask> nextTaskList = new ArrayList<>();
        for (Object jsonNextTask : jsonNextTaskList) {
          NextTask nextTask = new NextTask();

          keyChangeMap
              .put(((JSONObject) jsonNextTask).get("id").toString(), UUID.randomUUID().toString());

          nextTask.setId(keyChangeMap.get(((JSONObject) jsonNextTask).get("id").toString()));
          nextTask.setNextTaskId(
              keyChangeMap.get(((JSONObject) jsonNextTask).get("nextTaskId").toString()));
          nextTask.setNextTaskCondition(
              ((JSONObject) jsonNextTask).get("nextTaskCondition").toString());
          nextTask.setController("system");

          nextTask.setCreatorId(userId);
          nextTask.setCreatedAt(currentTimestamp);
          nextTask.setUpdaterId(userId);
          nextTask.setUpdatedAt(currentTimestamp);

          for (Task task : taskList) {
            if (task.getId()
                .equals(keyChangeMap.get(((JSONObject) jsonNextTask).get("taskId").toString()))) {
              nextTask.setTask(task);
              break;
            }
          }

          nextTaskService.insertNextTask(nextTask);
        }

        // Start Event
        JSONArray jsonStartEventList = (JSONArray) ((JSONObject) jsonTask).get("startEvent");
        List<StartEvent> startEventList = new ArrayList<>();
        for (Object jsonStartEvent : jsonStartEventList) {
          StartEvent startEvent = new StartEvent();

          keyChangeMap.put(((JSONObject) jsonStartEvent).get("id").toString(),
              UUID.randomUUID().toString());

          startEvent.setId(keyChangeMap.get(((JSONObject) jsonStartEvent).get("id").toString()));
          startEvent.setStartEventCondition(
              ((JSONObject) jsonStartEvent).get("startEventCondition").toString());
          startEvent.setStartEventAction(
              ((JSONObject) jsonStartEvent).get("startEventAction").toString());

          startEvent.setCreatorId(userId);
          startEvent.setCreatedAt(currentTimestamp);
          startEvent.setUpdaterId(userId);
          startEvent.setUpdatedAt(currentTimestamp);

          for (Task task : taskList) {
            if (task.getId()
                .equals(keyChangeMap.get(((JSONObject) jsonStartEvent).get("taskId").toString()))) {
              startEvent.setTask(task);
              break;
            }
          }

          startEventService.insertStartEvent(startEvent);

          // Start Event Utter
          JSONArray jsonStartEventUtterList = (JSONArray) ((JSONObject) jsonStartEvent)
              .get("startEventUtter");
          List<StartEventUtter> startEventUtterList = new ArrayList<>();
          for (Object jsonStartEventUtter : jsonStartEventUtterList) {
            StartEventUtter startEventUtter = new StartEventUtter();

            keyChangeMap.put(((JSONObject) jsonStartEventUtter).get("id").toString(),
                UUID.randomUUID().toString());

            startEventUtter
                .setId(keyChangeMap.get(((JSONObject) jsonStartEventUtter).get("id").toString()));
            startEventUtter.setIntentId(
                keyChangeMap.get(((JSONObject) jsonStartEventUtter).get("intentId").toString()));
            startEventUtter
                .setExtraData(((JSONObject) jsonStartEventUtter).get("extraData").toString());

            startEventUtter.setCreatorId(userId);
            startEventUtter.setCreatedAt(currentTimestamp);
            startEventUtter.setUpdaterId(userId);
            startEventUtter.setUpdatedAt(currentTimestamp);

            startEventUtter.setStartEvent(startEvent);

            startEventUtterService.insertStartEventUtter(startEventUtter);

            // Start Event Utter Data
            JSONArray jsonStartEventUtterDataList = (JSONArray) ((JSONObject) jsonStartEventUtter)
                .get("startEventUtterData");
            List<StartEventUtterData> startEventUtterDataList = new ArrayList<>();
            for (Object jsonStartEventUtterData : jsonStartEventUtterDataList) {
              StartEventUtterData startEventUtterData = new StartEventUtterData();

              keyChangeMap.put(((JSONObject) jsonStartEventUtterData).get("id").toString(),
                  UUID.randomUUID().toString());

              startEventUtterData.setId(
                  keyChangeMap.get(((JSONObject) jsonStartEventUtterData).get("id").toString()));
              startEventUtterData.setStartEventUtterData(
                  ((JSONObject) jsonStartEventUtterData).get("startEventUtterData").toString());

              startEventUtterData.setCreatorId(userId);
              startEventUtterData.setCreatedAt(currentTimestamp);
              startEventUtterData.setUpdaterId(userId);
              startEventUtterData.setUpdatedAt(currentTimestamp);

              startEventUtterData.setStartEventUtter(startEventUtter);

              startEventUtterDataService.insertStartEventUtterData(startEventUtterData);
            }
          }
        }

        // Event
        JSONArray jsonEventList = (JSONArray) ((JSONObject) jsonTask).get("event");
        List<Event> eventList = new ArrayList<>();
        for (Object jsonEvent : jsonEventList) {
          Event event = new Event();

          keyChangeMap
              .put(((JSONObject) jsonEvent).get("id").toString(), UUID.randomUUID().toString());

          event.setId(keyChangeMap.get(((JSONObject) jsonEvent).get("id").toString()));
          event.setIntentId(keyChangeMap.get(((JSONObject) jsonEvent).get("intentId").toString()));

          event.setCreatorId(userId);
          event.setCreatedAt(currentTimestamp);
          event.setUpdaterId(userId);
          event.setUpdatedAt(currentTimestamp);

          for (Task task : taskList) {
            if (task.getId()
                .equals(keyChangeMap.get(((JSONObject) jsonEvent).get("taskId").toString()))) {
              event.setTask(task);
              break;
            }
          }

          eventService.insertEvent(event);

          // Event Condition
          JSONArray jsonEventConditionList = (JSONArray) ((JSONObject) jsonEvent)
              .get("eventCondition");
          List<EventCondition> eventConditionList = new ArrayList<>();
          for (Object jsonEventCondition : jsonEventConditionList) {
            EventCondition eventCondition = new EventCondition();

            keyChangeMap.put(((JSONObject) jsonEventCondition).get("id").toString(),
                UUID.randomUUID().toString());

            eventCondition
                .setId(keyChangeMap.get(((JSONObject) jsonEventCondition).get("id").toString()));
            eventCondition.setEventCondition(
                ((JSONObject) jsonEventCondition).get("eventCondition").toString());
            eventCondition
                .setEventAction(((JSONObject) jsonEventCondition).get("eventAction").toString());
            eventCondition
                .setExtraData(((JSONObject) jsonEventCondition).get("extraData").toString());

            eventCondition.setCreatorId(userId);
            eventCondition.setCreatedAt(currentTimestamp);
            eventCondition.setUpdaterId(userId);
            eventCondition.setUpdatedAt(currentTimestamp);

            eventCondition.setEvent(event);

            eventConditionService.insertEventCondition(eventCondition);

            // Event Utter
            JSONArray jsonEventUtterList = (JSONArray) ((JSONObject) jsonEventCondition)
                .get("eventUtter");
            List<EventUtter> eventUtterList = new ArrayList<>();
            for (Object jsonEventUtter : jsonEventUtterList) {
              EventUtter eventUtter = new EventUtter();

              keyChangeMap.put(((JSONObject) jsonEventUtter).get("id").toString(),
                  UUID.randomUUID().toString());

              eventUtter
                  .setId(keyChangeMap.get(((JSONObject) jsonEventUtter).get("id").toString()));
              eventUtter.setIntentId(
                  keyChangeMap.get(((JSONObject) jsonEventUtter).get("intentId").toString()));
              eventUtter.setExtraData(((JSONObject) jsonEventUtter).get("extraData").toString());

              eventUtter.setCreatorId(userId);
              eventUtter.setCreatedAt(currentTimestamp);
              eventUtter.setUpdaterId(userId);
              eventUtter.setUpdatedAt(currentTimestamp);

              eventUtter.setEventCondition(eventCondition);

              eventUtterService.insertEventUtter(eventUtter);

              // Event Utter Data
              JSONArray jsonEventUtterDataList = (JSONArray) ((JSONObject) jsonEventUtter)
                  .get("eventUtterData");
              List<EventUtterData> eventUtterDataList = new ArrayList<>();
              for (Object jsonEventUtterData : jsonEventUtterDataList) {
                EventUtterData eventUtterData = new EventUtterData();

                keyChangeMap.put(((JSONObject) jsonEventUtterData).get("id").toString(),
                    UUID.randomUUID().toString());

                eventUtterData.setId(
                    keyChangeMap.get(((JSONObject) jsonEventUtterData).get("id").toString()));
                eventUtterData.setEventUtterData(
                    ((JSONObject) jsonEventUtterData).get("eventUtterData").toString());

                eventUtterData.setCreatorId(userId);
                eventUtterData.setCreatedAt(currentTimestamp);
                eventUtterData.setUpdaterId(userId);
                eventUtterData.setUpdatedAt(currentTimestamp);

                eventUtterData.setEventUtter(eventUtter);

                eventUtterDataService.insertEventUtterData(eventUtterData);
              }
            }
          }
        }
      }
      model.setTaskList(taskList);

      modelService.updateModel(model);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("POST /api/workspace/{}/uploadmodel", workspaceId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^R")
  @RequestMapping(value = "/workspace/{workspaceId}/model/list", method = RequestMethod.GET)
  public ResponseEntity<?> getModelListByWorkspaceId(@PathVariable String workspaceId) {
    logger.info("GET /api/workspace/{}/entity/list", workspaceId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      List<Model> modelList = new ArrayList<>();
      try {
        modelList = modelService.findModelsByWorkspaceIdOrderByModelName(workspaceId);
      } catch (Exception ex) {
        modelList = modelService.findModelsByWorkspaceIdOrderByModelName(workspaceId);
      }

      JSONObject wrappedModelInfo = new JSONObject();
      JSONArray modelInfoList = new JSONArray();

      for (int index = 0; index < modelList.size(); index++) {
        JSONObject modelInfo = new JSONObject();

        modelInfo.put("id", modelList.get(index).getId());
        modelInfo.put("modelName", modelList.get(index).getModelName());
        modelInfo.put("description", modelList.get(index).getDescription());
        modelInfo.put("status", modelList.get(index).getStatus());
        modelInfo.put("lastTrainKey", modelList.get(index).getLastTrainKey());
        modelInfo.put("creatorId", modelList.get(index).getCreatorId());
        modelInfo.put("createdAt", modelList.get(index).getCreatedAt());
        modelInfo.put("updaterId", modelList.get(index).getUpdaterId());
        modelInfo.put("updatedAt", modelList.get(index).getUpdatedAt());
        modelInfo.put("workspaceId", modelList.get(index).getWorkspaceId());

        modelInfoList.add(modelInfo);
      }

      wrappedModelInfo.put("modelInfoList", modelInfoList);

      result.put("data", wrappedModelInfo);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("GET /api/workspace/{}/entity/list", workspaceId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^R")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/allinfo", method = RequestMethod.GET)
  public ResponseEntity<?> getModelAllInfoByWorkspaceIdAndId(@PathVariable String workspaceId,
      @PathVariable String modelId) {
    logger.info("GET /api/workspace/{}/entity/{}/allinfo", workspaceId, modelId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      Model model = null;
      try {
        model = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
      } catch (Exception ex) {
        model = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
      }

      JSONObject wrappedModelInfo = new JSONObject();

      if (model == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "Model doesn't exist.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        wrappedModelInfo = getModelAllInfoToJson(model);
      }

      result.put("data", wrappedModelInfo);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("GET /api/workspace/{}/entity/{}/allinfo", workspaceId, modelId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^R")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}", method = RequestMethod.GET)
  public ResponseEntity<?> getModelByWorkspaceIdAndId(@PathVariable String workspaceId,
      @PathVariable String modelId) {
    logger.info("GET /api/workspace/{}/entity/{}", workspaceId, modelId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      Model model = null;
      try {
        model = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
      } catch (Exception ex) {
        model = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
      }

      JSONObject wrappedModelInfo = new JSONObject();
      JSONObject modelInfo = new JSONObject();

      if (model == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "Model doesn't exist.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        modelInfo.put("id", model.getId());
        modelInfo.put("modelName", model.getModelName());
        modelInfo.put("description", model.getDescription());
        modelInfo.put("status", model.getStatus());
        modelInfo.put("lastTrainKey", model.getLastTrainKey());
        modelInfo.put("creatorId", model.getCreatorId());
        modelInfo.put("createdAt", model.getCreatedAt());
        modelInfo.put("updaterId", model.getUpdaterId());
        modelInfo.put("updatedAt", model.getUpdatedAt());
        modelInfo.put("workspaceId", model.getWorkspaceId());
      }

      wrappedModelInfo.put("modelInfo", modelInfo);

      result.put("data", wrappedModelInfo);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("GET /api/workspace/{}/entity/{}", workspaceId, modelId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^U")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}", method = RequestMethod.PUT)
  public ResponseEntity<?> updateModel(@PathVariable String workspaceId,
      @PathVariable String modelId, @RequestBody JSONObject bodyParam) {
    logger.info("PUT /api/workspace/{}/entity/{}", workspaceId, modelId, bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      String modelName = null;
      String description = null;
      String lastTrainKey = null;
      String userId = null;

      Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

      if (bodyParam.get("modelName") != null && !bodyParam.get("modelName").toString().trim()
          .equals("")) {
        modelName = bodyParam.get("modelName").toString().trim();
      }

      if (bodyParam.get("description") != null && !bodyParam.get("description").toString().trim()
          .equals("")) {
        description = bodyParam.get("description").toString().trim();
      }

      if (bodyParam.get("lastTrainKey") != null && !bodyParam.get("lastTrainKey").toString().trim()
          .equals("")) {
        lastTrainKey = bodyParam.get("lastTrainKey").toString().trim();
      }

      if (bodyParam.get("userId") == null || bodyParam.get("userId").toString().trim().equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "userId is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        userId = bodyParam.get("userId").toString().trim();
      }

      Model findModel = null;
      try {
        findModel = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
      } catch (Exception ex) {
        findModel = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
      }
      if (findModel == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "entity 이 존재하지 않습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      if (!findModel.getCreatorId().equals(userId)) {
        result.put("statusCode", 400);
        result.put("statusMessage", "다른 사용자가 작성한 모델은 수정할 수 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

//            if (modelName == null && description == null) {
//                result.put("statusCode", 400);
//                result.put("statusMessage", "변경할 데이터가 없습니다.");
//
//                return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
//            }

      if (modelName != null && !modelName.equals(findModel.getModelName())) {
        Model duplicatedModel = null;
        try {
          duplicatedModel = modelService
              .findModelByWorkspaceIdAndCreatorIdAndModelName(workspaceId, userId, modelName);
        } catch (Exception ex) {
          duplicatedModel = modelService
              .findModelByWorkspaceIdAndCreatorIdAndModelName(workspaceId, userId, modelName);
        }
        if (duplicatedModel != null) {
          result.put("statusCode", 400);
          result.put("statusMessage", "동일한 modelName 이 있습니다. 다른 modelName 으로 변경하세요.");

          return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        findModel.setModelName(modelName);
      }

      if (description != null) {
        findModel.setDescription(description);
      }

      if (lastTrainKey != null) {
        findModel.setLastTrainKey(lastTrainKey);
      }

      findModel.setUpdaterId(userId);
      findModel.setUpdatedAt(currentTimestamp);

      try {
        modelService.updateModel(findModel);
      } catch (Exception ex) {
        modelService.updateModel(findModel);
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("PUT /api/workspace/{}/entity/{}", workspaceId, modelId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^D")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}", method = RequestMethod.DELETE)
  public ResponseEntity<?> deleteModelByWorkspaceIdAndId(@PathVariable String workspaceId,
      @PathVariable String modelId) {
    logger.info("DELETE /api/workspace/{}/entity/{}", workspaceId, modelId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      Model findModel = null;
      try {
        findModel = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
      } catch (Exception ex) {
        findModel = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
      }

      if (findModel == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "entity doesn't exist.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      List<Entity> entityList = findModel.getEntityList();
      for (int i = 0; i < entityList.size(); i++) {
        if (entityList.get(i).getSharedYn().equals("Y")) {
          entityList.remove(i);
        }
      }

      findModel.setEntityList(entityList);
      try {
        modelService.updateModel(findModel);
      } catch (Exception ex) {
        modelService.updateModel(findModel);
      }

      try {
        modelService.deleteModelByWorkspaceIdAndId(workspaceId, modelId);
      } catch (Exception ex) {
        modelService.deleteModelByWorkspaceIdAndId(workspaceId, modelId);
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("DELETE /api/workspace/{}/entity/{}", workspaceId, modelId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  public JSONObject getModelAllInfoToJson(Model model) {
    JSONObject wrappedModelInfo = new JSONObject();
    JSONObject modelInfo = new JSONObject();

    modelInfo.put("id", model.getId());
    modelInfo.put("modelName", model.getModelName());
    modelInfo.put("description", model.getDescription());
    modelInfo.put("status", model.getStatus());
    modelInfo.put("lastTrainKey", model.getLastTrainKey());
    modelInfo.put("creatorId", model.getCreatorId());
    modelInfo.put("createdAt", model.getCreatedAt());
    modelInfo.put("updaterId", model.getUpdaterId());
    modelInfo.put("updatedAt", model.getUpdatedAt());
    modelInfo.put("workspaceId", model.getWorkspaceId());

    // entity
    JSONArray entityInfoList = new JSONArray();
    for (Entity entity : model.getEntityList()) {
      JSONObject entityInfo = new JSONObject();

      entityInfo.put("id", entity.getId());
      entityInfo.put("className", entity.getClassName());
      entityInfo.put("entityName", entity.getEntityName());
      entityInfo.put("entityType", entity.getEntityType());
      entityInfo.put("sharedYn", entity.getSharedYn());

      JSONArray entityDataInfoList = new JSONArray();
      for (EntityData entityData : entity.getEntityDataList()) {
        JSONObject entityDataInfo = new JSONObject();

        entityDataInfo.put("id", entityData.getId());
        entityDataInfo.put("entityData", entityData.getEntityData());
        entityDataInfo.put("entityId", entityData.getEntity().getId());

        JSONArray entityDataSynonymInfoList = new JSONArray();
        for (EntityDataSynonym entityDataSynonym : entityData.getEntityDataSynonymList()) {
          JSONObject entityDataSynonymInfo = new JSONObject();

          entityDataSynonymInfo.put("id", entityDataSynonym.getId());
          entityDataSynonymInfo.put("entityDataSynonym", entityDataSynonym.getEntityDataSynonym());
          entityDataSynonymInfo.put("entityDataId", entityDataSynonym.getEntityData().getId());

          entityDataSynonymInfo.put("creatorId", entityDataSynonym.getCreatorId());
          entityDataSynonymInfo.put("createdAt", entityDataSynonym.getCreatedAt());
          entityDataSynonymInfo.put("updaterId", entityDataSynonym.getUpdaterId());
          entityDataSynonymInfo.put("updatedAt", entityDataSynonym.getUpdatedAt());

          entityDataSynonymInfoList.add(entityDataSynonymInfo);
        }
        entityDataInfo.put("entityDataSynonym", entityDataSynonymInfoList);

        entityDataInfo.put("creatorId", entityData.getCreatorId());
        entityDataInfo.put("createdAt", entityData.getCreatedAt());
        entityDataInfo.put("updaterId", entityData.getUpdaterId());
        entityDataInfo.put("updatedAt", entityData.getUpdatedAt());

        entityDataInfoList.add(entityDataInfo);
      }
      entityInfo.put("entityData", entityDataInfoList);

      entityInfo.put("creatorId", entity.getCreatorId());
      entityInfo.put("createdAt", entity.getCreatedAt());
      entityInfo.put("updaterId", entity.getUpdaterId());
      entityInfo.put("updatedAt", entity.getUpdatedAt());
      entityInfo.put("workspaceId", entity.getWorkspaceId());

      entityInfoList.add(entityInfo);
    }
    modelInfo.put("entity", entityInfoList);

    // intent
    JSONArray intentInfoList = new JSONArray();
    for (Intent intent : model.getIntentList()) {
      JSONObject intentInfo = new JSONObject();

      intentInfo.put("id", intent.getId());
      intentInfo.put("intentName", intent.getIntentName());
      intentInfo.put("sharedYn", intent.getSharedYn());

      JSONArray intentUtterInfoList = new JSONArray();
      for (IntentUtter intentUtter : intent.getIntentUtterList()) {
        JSONObject intentUtterInfo = new JSONObject();

        intentUtterInfo.put("id", intentUtter.getId());
        intentUtterInfo.put("userSay", intentUtter.getUserSay());
        intentUtterInfo.put("mappedUserSay", intentUtter.getMappedUserSay());
        intentUtterInfo.put("intentId", intentUtter.getIntent().getId());

        JSONArray intentUtterDataInfoList = new JSONArray();
        for (IntentUtterData intentUtterData : intentUtter.getIntentUtterDataList()) {
          JSONObject intentUtterDataInfo = new JSONObject();

          intentUtterDataInfo.put("id", intentUtterData.getId());
          intentUtterDataInfo.put("entityId", intentUtterData.getEntityId());
          intentUtterDataInfo.put("mappedValue", intentUtterData.getMappedValue());
          intentUtterDataInfo.put("intentUtterId", intentUtterData.getIntentUtter().getId());

          intentUtterDataInfo.put("creatorId", intentUtterData.getCreatorId());
          intentUtterDataInfo.put("createdAt", intentUtterData.getCreatedAt());
          intentUtterDataInfo.put("updaterId", intentUtterData.getUpdaterId());
          intentUtterDataInfo.put("updatedAt", intentUtterData.getUpdatedAt());

          intentUtterDataInfoList.add(intentUtterDataInfo);
        }
        intentUtterInfo.put("intentUtterData", intentUtterDataInfoList);

        intentUtterInfo.put("creatorId", intentUtter.getCreatorId());
        intentUtterInfo.put("createdAt", intentUtter.getCreatedAt());
        intentUtterInfo.put("updaterId", intentUtter.getUpdaterId());
        intentUtterInfo.put("updatedAt", intentUtter.getUpdatedAt());

        intentUtterInfoList.add(intentUtterInfo);
      }
      intentInfo.put("intentUtter", intentUtterInfoList);

      intentInfo.put("creatorId", intent.getCreatorId());
      intentInfo.put("createdAt", intent.getCreatedAt());
      intentInfo.put("updaterId", intent.getUpdaterId());
      intentInfo.put("updatedAt", intent.getUpdatedAt());
      intentInfo.put("workspaceId", intent.getWorkspaceId());

      intentInfoList.add(intentInfo);
    }
    modelInfo.put("intent", intentInfoList);

    // task
    JSONArray taskInfoList = new JSONArray();

    for (Task task : model.getTaskList()) {
      JSONObject taskInfo = new JSONObject();

      taskInfo.put("id", task.getId());
      taskInfo.put("taskName", task.getTaskName());
      taskInfo.put("taskGoal", task.getTaskGoal());
      taskInfo.put("modelId", task.getModel().getId());
      taskInfo.put("taskLocation", task.getTaskLocation());

      // next task
      JSONArray nextTaskInfoList = new JSONArray();
      for (NextTask nextTask : task.getNextTaskList()) {
        JSONObject nextTaskInfo = new JSONObject();

        nextTaskInfo.put("id", nextTask.getId());
        nextTaskInfo.put("nextTaskId", nextTask.getNextTaskId());
        nextTaskInfo.put("nextTaskCondition", nextTask.getNextTaskCondition());
        nextTaskInfo.put("taskId", nextTask.getTask().getId());

        nextTaskInfo.put("creatorId", nextTask.getCreatorId());
        nextTaskInfo.put("createdAt", nextTask.getCreatedAt());
        nextTaskInfo.put("updaterId", nextTask.getUpdaterId());
        nextTaskInfo.put("updatedAt", nextTask.getUpdatedAt());

        nextTaskInfoList.add(nextTaskInfo);
      }
      taskInfo.put("nextTask", nextTaskInfoList);

      // start event
      JSONArray startEventInfoList = new JSONArray();
      for (StartEvent startEvent : task.getStartEventList()) {
        JSONObject startEventInfo = new JSONObject();

        startEventInfo.put("id", startEvent.getId());
        startEventInfo.put("startEventCondition", startEvent.getStartEventCondition());
        startEventInfo.put("startEventAction", startEvent.getStartEventAction());
        startEventInfo.put("taskId", startEvent.getTask().getId());

        // start event utter
        JSONArray startEventUtterInfoList = new JSONArray();
        for (StartEventUtter startEventUtter : startEvent.getStartEventUtterList()) {
          JSONObject startEventUtterInfo = new JSONObject();

          startEventUtterInfo.put("id", startEventUtter.getId());
          startEventUtterInfo.put("intentId", startEventUtter.getIntentId());
          startEventUtterInfo.put("extraData", startEventUtter.getExtraData());
          startEventUtterInfo.put("startEventId", startEventUtter.getStartEvent().getId());

          // start event utter data
          JSONArray startEventUtterDataInfoList = new JSONArray();
          for (StartEventUtterData startEventUtterData : startEventUtter
              .getStartEventUtterDataList()) {
            JSONObject startEventUtterDataInfo = new JSONObject();

            startEventUtterDataInfo.put("id", startEventUtterData.getId());
            startEventUtterDataInfo
                .put("startEventUtterData", startEventUtterData.getStartEventUtterData());
            startEventUtterDataInfo
                .put("startEventUtterId", startEventUtterData.getStartEventUtter().getId());

            startEventUtterDataInfo.put("creatorId", startEventUtterData.getCreatorId());
            startEventUtterDataInfo.put("createdAt", startEventUtterData.getCreatedAt());
            startEventUtterDataInfo.put("updaterId", startEventUtterData.getUpdaterId());
            startEventUtterDataInfo.put("updatedAt", startEventUtterData.getUpdatedAt());

            startEventUtterDataInfoList.add(startEventUtterDataInfo);
          }
          startEventUtterInfo.put("startEventUtterData", startEventUtterDataInfoList);

          startEventUtterInfo.put("creatorId", startEventUtter.getCreatorId());
          startEventUtterInfo.put("createdAt", startEventUtter.getCreatedAt());
          startEventUtterInfo.put("updaterId", startEventUtter.getUpdaterId());
          startEventUtterInfo.put("updatedAt", startEventUtter.getUpdatedAt());

          startEventUtterInfoList.add(startEventUtterInfo);
        }
        startEventInfo.put("startEventUtter", startEventUtterInfoList);

        startEventInfo.put("creatorId", startEvent.getCreatorId());
        startEventInfo.put("createdAt", startEvent.getCreatedAt());
        startEventInfo.put("updaterId", startEvent.getUpdaterId());
        startEventInfo.put("updatedAt", startEvent.getUpdatedAt());

        startEventInfoList.add(startEventInfo);
      }
      taskInfo.put("startEvent", startEventInfoList);

      // event
      JSONArray eventInfoList = new JSONArray();
      for (Event event : task.getEventList()) {
        JSONObject eventInfo = new JSONObject();

        eventInfo.put("id", event.getId());
        eventInfo.put("intentId", event.getIntentId());
        eventInfo.put("taskId", event.getTask().getId());

        // event condition
        JSONArray eventConditionInfoList = new JSONArray();
        for (EventCondition eventCondition : event.getEventConditionList()) {
          JSONObject eventConditionInfo = new JSONObject();

          eventConditionInfo.put("id", eventCondition.getId());
          eventConditionInfo.put("eventCondition", eventCondition.getEventCondition());
          eventConditionInfo.put("eventAction", eventCondition.getEventAction());
          eventConditionInfo.put("extraData", eventCondition.getExtraData());
          eventConditionInfo.put("eventId", eventCondition.getEvent().getId());

          // event utter
          JSONArray eventUtterInfoList = new JSONArray();
          for (EventUtter eventUtter : eventCondition.getEventUtterList()) {
            JSONObject eventUtterInfo = new JSONObject();

            eventUtterInfo.put("id", eventUtter.getId());
            eventUtterInfo.put("intentId", eventUtter.getIntentId());
            eventUtterInfo.put("extraData", eventUtter.getExtraData());
            eventUtterInfo.put("eventConditionId", eventUtter.getEventCondition().getId());

            // event utter data
            JSONArray eventUtterDataInfoList = new JSONArray();
            for (EventUtterData eventUtterData : eventUtter.getEventUtterDataList()) {
              JSONObject eventUtterDataInfo = new JSONObject();

              eventUtterDataInfo.put("id", eventUtterData.getId());
              eventUtterDataInfo.put("eventUtterData", eventUtterData.getEventUtterData());
              eventUtterDataInfo.put("eventUtterId", eventUtterData.getEventUtter().getId());

              eventUtterDataInfo.put("creatorId", eventUtterData.getCreatorId());
              eventUtterDataInfo.put("createdAt", eventUtterData.getCreatedAt());
              eventUtterDataInfo.put("updaterId", eventUtterData.getUpdaterId());
              eventUtterDataInfo.put("updatedAt", eventUtterData.getUpdatedAt());

              eventUtterDataInfoList.add(eventUtterDataInfo);
            }
            eventUtterInfo.put("eventUtterData", eventUtterDataInfoList);

            eventUtterInfo.put("creatorId", eventUtter.getCreatorId());
            eventUtterInfo.put("createdAt", eventUtter.getCreatedAt());
            eventUtterInfo.put("updaterId", eventUtter.getUpdaterId());
            eventUtterInfo.put("updatedAt", eventUtter.getUpdatedAt());

            eventUtterInfoList.add(eventUtterInfo);
          }
          eventConditionInfo.put("eventUtter", eventUtterInfoList);

          eventConditionInfo.put("creatorId", eventCondition.getCreatorId());
          eventConditionInfo.put("createdAt", eventCondition.getCreatedAt());
          eventConditionInfo.put("updaterId", eventCondition.getUpdaterId());
          eventConditionInfo.put("updatedAt", eventCondition.getUpdatedAt());

          eventConditionInfoList.add(eventConditionInfo);
        }
        eventInfo.put("eventCondition", eventConditionInfoList);

        eventInfo.put("creatorId", event.getCreatorId());
        eventInfo.put("createdAt", event.getCreatedAt());
        eventInfo.put("updaterId", event.getUpdaterId());
        eventInfo.put("updatedAt", event.getUpdatedAt());

        eventInfoList.add(eventInfo);
      }
      taskInfo.put("event", eventInfoList);

      taskInfo.put("workspaceId", task.getWorkspaceId());
      taskInfo.put("creatorId", task.getCreatorId());
      taskInfo.put("createdAt", task.getCreatedAt());
      taskInfo.put("updaterId", task.getUpdaterId());
      taskInfo.put("updatedAt", task.getUpdatedAt());

      taskInfoList.add(taskInfo);
    }
    modelInfo.put("task", taskInfoList);

    wrappedModelInfo.put("modelInfo", modelInfo);

    return wrappedModelInfo;
  }
}
