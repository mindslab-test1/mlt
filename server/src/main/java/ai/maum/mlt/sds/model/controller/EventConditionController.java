package ai.maum.mlt.sds.model.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.sds.model.entity.Event;
import ai.maum.mlt.sds.model.entity.EventCondition;
import ai.maum.mlt.sds.model.service.EventConditionService;
import ai.maum.mlt.sds.model.service.EventService;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/sds/model")
public class EventConditionController {

  static final Logger logger = LoggerFactory.getLogger(EventConditionController.class);

  @Autowired
  private EventService eventService;

  @Autowired
  private EventConditionService eventConditionService;

  @UriRoleDesc(role = "sds/model^C")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}/event/{eventId}/eventcondition", method = RequestMethod.POST)
  public ResponseEntity<?> insertEventCondition(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String taskId, @PathVariable String eventId,
      @RequestBody JSONObject bodyParam) {
    logger.info("POST /api/workspace/{}/entity/{}/task/{}/event/{}/eventcondition", workspaceId,
        modelId, taskId, eventId, bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      String eventConditionId = null;
      String eventConditionContent = "";
      String eventAction = "";
      String extraData = "";
      String userId = null;

      Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

      if (bodyParam.get("id") == null || bodyParam.get("id").toString().trim().equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "id is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        eventConditionId = bodyParam.get("id").toString().trim();

        EventCondition findEventCondition = null;
        try {
          findEventCondition = eventConditionService.findEventConditionById(eventConditionId);
        } catch (Exception ex) {
          findEventCondition = eventConditionService.findEventConditionById(eventConditionId);
        }
        if (findEventCondition != null) {
          result.put("statusCode", 400);
          result.put("statusMessage", "id is already exist.");

          return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }
      }

      if (bodyParam.get("eventCondition") != null) {
        eventConditionContent = bodyParam.get("eventCondition").toString().trim()
            .replace("\"", "'");
      }

      if (bodyParam.get("eventAction") != null) {
        eventAction = bodyParam.get("eventAction").toString().trim().replace("\"", "'");
      }

      if (bodyParam.get("extraData") != null) {
        extraData = bodyParam.get("extraData").toString().trim();
      }

      if (bodyParam.get("userId") == null || bodyParam.get("userId").toString().trim().equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "userId is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        userId = bodyParam.get("userId").toString().trim();
      }

      Event findEvent = null;
      try {
        findEvent = eventService.findEventById(eventId);
      } catch (Exception ex) {
        findEvent = eventService.findEventById(eventId);
      }
      if (findEvent == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "event doesn't exist.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      EventCondition eventCondition = new EventCondition();
      eventCondition.setId(eventConditionId);
      eventCondition.setEventCondition(eventConditionContent);
      eventCondition.setEventAction(eventAction);
      eventCondition.setExtraData(extraData);
      eventCondition.setEvent(findEvent);

      eventCondition.setCreatorId(userId);
      eventCondition.setCreatedAt(currentTimestamp);
      eventCondition.setUpdaterId(userId);
      eventCondition.setUpdatedAt(currentTimestamp);

      try {
        eventConditionService.insertEventCondition(eventCondition);
      } catch (Exception ex) {
        eventConditionService.insertEventCondition(eventCondition);
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("POST /api/workspace/{}/entity/{}/task/{}/event/{}/eventcondition", workspaceId,
          modelId, taskId, eventId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^R")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}/event/{eventId}/eventcondition/list", method = RequestMethod.GET)
  public ResponseEntity<?> getEventConditionListByEventId(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String taskId, @PathVariable String eventId) {
    logger.info("GET /api/workspace/{}/entity/{}/task/{}/event/{}/eventcondition/list", workspaceId,
        modelId, taskId, eventId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      Event findEvent = null;
      try {
        findEvent = eventService.findEventById(eventId);
      } catch (Exception ex) {
        findEvent = eventService.findEventById(eventId);
      }
      if (findEvent == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "event 데이터가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      List<EventCondition> eventConditionList = new ArrayList<>();
      try {
        eventConditionList = eventConditionService.findEventConditionsByEvent(findEvent);
      } catch (Exception ex) {
        eventConditionList = eventConditionService.findEventConditionsByEvent(findEvent);
      }

      JSONObject wrappedEventConditionInfo = new JSONObject();
      JSONArray eventConditionInfoList = new JSONArray();

      for (int i = 0; i < eventConditionList.size(); i++) {
        JSONObject eventConditionInfo = new JSONObject();

        eventConditionInfo.put("id", eventConditionList.get(i).getId());
        eventConditionInfo.put("eventCondition", eventConditionList.get(i).getEventCondition());
        eventConditionInfo.put("eventAction", eventConditionList.get(i).getEventAction());
        eventConditionInfo.put("extraData", eventConditionList.get(i).getExtraData());
        eventConditionInfo.put("eventId", eventConditionList.get(i).getEvent().getId());

        eventConditionInfo.put("creatorId", eventConditionList.get(i).getCreatorId());
        eventConditionInfo.put("createdAt", eventConditionList.get(i).getCreatedAt());
        eventConditionInfo.put("updaterId", eventConditionList.get(i).getUpdaterId());
        eventConditionInfo.put("updatedAt", eventConditionList.get(i).getUpdatedAt());

        eventConditionInfoList.add(eventConditionInfo);
      }

      wrappedEventConditionInfo.put("eventConditionInfoList", eventConditionInfoList);

      result.put("data", wrappedEventConditionInfo);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("GET /api/workspace/{}/entity/{}/task/{}/event/{}/eventcondition/list",
          workspaceId, modelId, taskId, eventId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^R")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}/event/{eventId}/eventcondition/{eventConditionId}", method = RequestMethod.GET)
  public ResponseEntity<?> getEventConditionById(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String taskId, @PathVariable String eventId,
      @PathVariable String eventConditionId) {
    logger.info("GET /api/workspace/{}/entity/{}/task/{}/event/{}/eventcondition/{}", workspaceId,
        modelId, taskId, eventId, eventConditionId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      EventCondition eventCondition = null;
      try {
        eventCondition = eventConditionService.findEventConditionById(eventConditionId);
      } catch (Exception ex) {
        eventCondition = eventConditionService.findEventConditionById(eventConditionId);
      }

      JSONObject wrappedEventConditionInfo = new JSONObject();
      JSONObject eventConditionInfo = new JSONObject();

      eventConditionInfo.put("id", eventCondition.getId());
      eventConditionInfo.put("eventCondition", eventCondition.getEventCondition());
      eventConditionInfo.put("eventAction", eventCondition.getEventAction());
      eventConditionInfo.put("extraData", eventCondition.getExtraData());
      eventConditionInfo.put("eventId", eventCondition.getEvent().getId());

      eventConditionInfo.put("creatorId", eventCondition.getCreatorId());
      eventConditionInfo.put("createdAt", eventCondition.getCreatedAt());
      eventConditionInfo.put("updaterId", eventCondition.getUpdaterId());
      eventConditionInfo.put("updatedAt", eventCondition.getUpdatedAt());

      wrappedEventConditionInfo.put("eventConditionInfo", eventConditionInfo);

      result.put("data", wrappedEventConditionInfo);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger
          .error("GET /api/workspace/{}/entity/{}/task/{}/event/{}/eventcondition/{}", workspaceId,
              modelId, taskId, eventId, eventConditionId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^U")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}/event/{eventId}/eventcondition/{eventConditionId}", method = RequestMethod.PUT)
  public ResponseEntity<?> updateEventCondition(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String taskId, @PathVariable String eventId,
      @PathVariable String eventConditionId, @RequestBody JSONObject bodyParam) {
    logger.info("PUT /api/workspace/{}/entity/{}/task/{}/event/{}/eventcondition/{}", workspaceId,
        modelId, taskId, eventId, eventConditionId, bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

      String eventCondition = null;
      String eventAction = null;
      String extraData = null;
      String userId = null;

      if (bodyParam.get("eventCondition") != null) {
        eventCondition = bodyParam.get("eventCondition").toString().trim().replace("\"", "'");
      }

      if (bodyParam.get("eventAction") != null) {
        eventAction = bodyParam.get("eventAction").toString().trim().replace("\"", "'");
      }

      if (bodyParam.get("extraData") != null) {
        extraData = bodyParam.get("extraData").toString().trim();
      }

      if (bodyParam.get("userId") == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "userId 가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        userId = bodyParam.get("userId").toString().trim();
      }

      EventCondition findEventCondition = null;
      try {
        findEventCondition = eventConditionService.findEventConditionById(eventConditionId);
      } catch (Exception ex) {
        findEventCondition = eventConditionService.findEventConditionById(eventConditionId);
      }
      if (findEventCondition == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "eventConditionId 와 일치하는 eventCondition 이 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        if (eventCondition != null) {
          findEventCondition.setEventCondition(eventCondition);
        }

        if (eventAction != null) {
          findEventCondition.setEventAction(eventAction);
        }

        if (extraData != null) {
          findEventCondition.setExtraData(extraData);
        }

        findEventCondition.setUpdaterId(userId);
        findEventCondition.setUpdatedAt(currentTimestamp);
      }

      try {
        eventConditionService.updateEventCondition(findEventCondition);
      } catch (Exception ex) {
        eventConditionService.updateEventCondition(findEventCondition);
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger
          .error("PUT /api/workspace/{}/entity/{}/task/{}/event/{}/eventcondition/{}", workspaceId,
              modelId, taskId, eventId, eventConditionId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^D")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}/event/{eventId}/eventcondition/{eventConditionId}", method = RequestMethod.DELETE)
  public ResponseEntity<?> deleteEventConditionById(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String taskId, @PathVariable String eventId,
      @PathVariable String eventConditionId) {
    logger
        .info("DELETE /api/workspace/{}/entity/{}/task/{}/event/{}/eventcondition/{}", workspaceId,
            modelId, taskId, eventId, eventConditionId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      EventCondition findEventCondition = null;
      try {
        findEventCondition = eventConditionService.findEventConditionById(eventConditionId);
      } catch (Exception ex) {
        findEventCondition = eventConditionService.findEventConditionById(eventConditionId);
      }
      if (findEventCondition == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "eventConditionId 와 일치하는 eventCondition 이 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        try {
          eventConditionService.deleteEventConditionById(eventConditionId);
        } catch (Exception ex) {
          eventConditionService.deleteEventConditionById(eventConditionId);
        }
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("DELETE /api/workspace/{}/entity/{}/task/{}/event/{}/eventcondition/{}",
          workspaceId, modelId, taskId, eventId, eventConditionId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
