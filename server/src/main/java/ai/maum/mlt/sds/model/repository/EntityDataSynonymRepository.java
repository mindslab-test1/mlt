package ai.maum.mlt.sds.model.repository;

import ai.maum.mlt.sds.model.entity.EntityData;
import ai.maum.mlt.sds.model.entity.EntityDataSynonym;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface EntityDataSynonymRepository extends JpaRepository<EntityDataSynonym, String> {

  EntityDataSynonym findEntityDataSynonymById(String entityDataSynonymId);

  List<EntityDataSynonym> findAll();

  List<EntityDataSynonym> findEntityDataSynonymsByEntityData(EntityData entityData);

  void deleteEntityDataSynonymById(String entityDataSynonymId);
}
