package ai.maum.mlt.sds.model.service;

import ai.maum.mlt.sds.model.entity.EntityData;
import ai.maum.mlt.sds.model.entity.EntityDataSynonym;
import ai.maum.mlt.sds.model.repository.EntityDataSynonymRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EntityDataSynonymServiceImpl implements EntityDataSynonymService {

  @Autowired
  private EntityDataSynonymRepository entityDataSynonymRepository;


  @Override
  public EntityDataSynonym insertEntityDataSynonym(EntityDataSynonym entityDataSynonym) {
    return entityDataSynonymRepository.save(entityDataSynonym);
  }

  @Override
  public EntityDataSynonym findEntityDataSynonymById(String entityDataSynonymId) {
    return entityDataSynonymRepository.findEntityDataSynonymById(entityDataSynonymId);
  }

  @Override
  public List<EntityDataSynonym> findAll() {
    return entityDataSynonymRepository.findAll();
  }

  @Override
  public List<EntityDataSynonym> findEntityDataSynonymsByEntityData(EntityData entityData) {
    return entityDataSynonymRepository.findEntityDataSynonymsByEntityData(entityData);
  }

  @Override
  public EntityDataSynonym updateEntityDataSynonym(EntityDataSynonym entityDataSynonym) {
    return entityDataSynonymRepository.save(entityDataSynonym);
  }

  @Override
  public void deleteEntityDataSynonymById(String entityDataSynonymId) {
    entityDataSynonymRepository.deleteEntityDataSynonymById(entityDataSynonymId);

    return;
  }
}
