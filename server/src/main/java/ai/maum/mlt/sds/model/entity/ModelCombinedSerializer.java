package ai.maum.mlt.sds.model.entity;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.jackson.JsonComponent;

@JsonComponent
public class ModelCombinedSerializer {

  public static class ModelJsonSerializer extends JsonSerializer<Model> {

    static final Logger logger = LoggerFactory
        .getLogger(ModelCombinedSerializer.ModelJsonSerializer.class);

    @Override
    public void serialize(Model model, JsonGenerator gen, SerializerProvider serializers)
        throws IOException, JsonProcessingException {
      gen.writeStartObject();

      gen.writeObjectField("data", model);
      gen.writeStringField("id", model.getId());
      gen.writeStringField("modelName", model.getModelName());
//            gen.writeStringField("modelDescription", dialogModel.getModelDescription());
//            gen.writeStringField("modelStatus", dialogModel.getModelStatus());
//            gen.writeStringField("tasks", dialogModel.getTasks());
//            gen.writeStringField("userIntents", dialogModel.getUserIntents());
//            gen.writeStringField("systemIntents", dialogModel.getSystemIntents());
//
////            if (dialogModel.getEntities() == null) {
////                gen.writeObjectField("entities", new JSONArray());
////            } else {
////                try {
////                    JSONParser jsonParser = new JSONParser();
////                    Object entities = jsonParser.parse(dialogModel.getEntities());
////                    gen.writeObjectField("entities", entities);
////                } catch (Exception ex) {
////                    logger.error(ex.toString());
////                }
////            }
//
//            gen.writeNumberField("userId", dialogModel.getUserId());
//            gen.writeStringField("userEmail", dialogModel.getUserEmail());
//            gen.writeStringField("workspaceId", dialogModel.getWorkspaceId());
//            gen.writeStringField("workspaceName", dialogModel.getWorkspaceName());
      gen.writeStringField("createdAt", model.getCreatedAt());
      gen.writeStringField("updatedAt", model.getUpdatedAt());

      gen.writeEndObject();
    }
  }
}
