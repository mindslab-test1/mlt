package ai.maum.mlt.sds.model.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.sds.model.entity.Entity;
import ai.maum.mlt.sds.model.entity.EntityData;
import ai.maum.mlt.sds.model.entity.EntityDataSynonym;
import ai.maum.mlt.sds.model.entity.Event;
import ai.maum.mlt.sds.model.entity.EventCondition;
import ai.maum.mlt.sds.model.entity.EventUtter;
import ai.maum.mlt.sds.model.entity.Intent;
import ai.maum.mlt.sds.model.entity.IntentUtter;
import ai.maum.mlt.sds.model.entity.IntentUtterData;
import ai.maum.mlt.sds.model.entity.Model;
import ai.maum.mlt.sds.model.entity.StartEvent;
import ai.maum.mlt.sds.model.entity.StartEventUtter;
import ai.maum.mlt.sds.model.entity.Task;
import ai.maum.mlt.sds.model.service.EntityDataService;
import ai.maum.mlt.sds.model.service.EntityDataSynonymService;
import ai.maum.mlt.sds.model.service.EntityService;
import ai.maum.mlt.sds.model.service.IntentService;
import ai.maum.mlt.sds.model.service.IntentUtterDataService;
import ai.maum.mlt.sds.model.service.IntentUtterService;
import ai.maum.mlt.sds.model.service.ModelService;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/sds/model")
public class IntentController {

  static final Logger logger = LoggerFactory.getLogger(IntentController.class);

  @Autowired
  private ModelService modelService;

  @Autowired
  private IntentService intentService;

  @Autowired
  private IntentUtterService intentUtterService;

  @Autowired
  private IntentUtterDataService intentUtterDataService;

  @Autowired
  private EntityService entityService;

  @Autowired
  private EntityDataService entityDataService;

  @Autowired
  private EntityDataSynonymService entityDataSynonymService;

  @UriRoleDesc(role = "sds/model^C")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/intent", method = RequestMethod.POST)
  public ResponseEntity<?> insertIntent(@PathVariable String workspaceId,
      @PathVariable String modelId, @RequestBody JSONObject bodyParam) {
    logger.info("POST /api/workspace/{}/entity/{}/intent", workspaceId, modelId, bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      String intentId = null;
      String intentName = null;
      String sharedYn = null;
      String userId = null;

      Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

      if (bodyParam.get("id") == null || bodyParam.get("id").toString().trim().equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "id is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        intentId = bodyParam.get("id").toString().trim();

        Intent findIntent = null;
        try {
          findIntent = intentService.findIntentByWorkspaceIdAndId(workspaceId, intentId);
        } catch (Exception ex) {
          findIntent = intentService.findIntentByWorkspaceIdAndId(workspaceId, intentId);
        }
        if (findIntent != null) {
          result.put("statusCode", 400);
          result.put("statusMessage", "id is already exist.");

          return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }
      }

      if (bodyParam.get("intentName") != null) {
        intentName = bodyParam.get("intentName").toString().trim();
      }

      if (bodyParam.get("sharedYn") == null || bodyParam.get("sharedYn").toString().trim()
          .equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "sharedYn is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        sharedYn = bodyParam.get("sharedYn").toString().trim();
      }

      if (bodyParam.get("userId") == null || bodyParam.get("userId").toString().trim().equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "userId is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        userId = bodyParam.get("userId").toString().trim();
      }

      Model findModel = null;
      try {
        findModel = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
      } catch (Exception ex) {
        findModel = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
      }
      if (findModel == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "Model doesn't exist.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      List<Intent> intentList = new ArrayList<>();
      try {
        intentList = intentService.findIntentsByWorkspaceId(workspaceId);
      } catch (Exception ex) {
        intentList = intentService.findIntentsByWorkspaceId(workspaceId);
      }

      List<Intent> filteredIntentList = new ArrayList<>();
      for (int i = 0; i < intentList.size(); i++) {
        List<Model> modelList = intentList.get(i).getModelList();
        List<Model> filteredModelList = modelList.stream()
            .filter(model -> model.getId().equals(modelId)).collect(Collectors.toList());
        if (filteredModelList.size() == 0) {
          continue;
        }

        filteredIntentList.add(intentList.get(i));
      }

      if (!intentName.equals("")) {
        for (int i = 0; i < filteredIntentList.size(); i++) {
          if (filteredIntentList.get(i).getIntentName().equals(intentName)) {
            result.put("statusCode", 400);
            result.put("statusMessage", "intentName duplicated.");

            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
          }
        }
      }

      Intent intent = new Intent();
      intent.setId(intentId);
      intent.setIntentName(intentName);
      intent.setSharedYn(sharedYn);

      intent.setWorkspaceId(workspaceId);
      intent.setCreatorId(userId);
      intent.setCreatedAt(currentTimestamp);
      intent.setUpdaterId(userId);
      intent.setUpdatedAt(currentTimestamp);

      findModel.addIntent(intent);
      intent.addModel(findModel);

      try {
        intentService.insertIntent(intent);
      } catch (Exception ex) {
        intentService.insertIntent(intent);
      }

      try {
        modelService.updateModel(findModel);
      } catch (Exception ex) {
        modelService.updateModel(findModel);
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("POST /api/workspace/{}/entity/{}/intent", workspaceId, modelId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^R")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/intent/allinfolist", method = RequestMethod.GET)
  public ResponseEntity<?> getIntentAllInfoListByWorkspaceIdAndModelId(
      @PathVariable String workspaceId, @PathVariable String modelId) {
    logger.info("GET /api/workspace/{}/entity/{}/intent/allinfolist", workspaceId, modelId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      List<Intent> intentList = new ArrayList<>();
      try {
        intentList = intentService.findIntentsByWorkspaceId(workspaceId);
      } catch (Exception ex) {
        intentList = intentService.findIntentsByWorkspaceId(workspaceId);
      }

      JSONObject wrappedIntentInfo = new JSONObject();
      JSONArray intentInfoList = new JSONArray();

      List<Model> modelListInWorkspace = new ArrayList<>();

      if (intentList.size() > 0) {
        modelListInWorkspace = modelService.findModelsByWorkspaceIdOrderByModelName(workspaceId);
      }

      for (int i = 0; i < intentList.size(); i++) {
        String strRelatedTask = "";

        List<Model> modelList = intentList.get(i).getModelList();
        List<Model> filteredModelList = modelList.stream()
            .filter(model -> model.getId().equals(modelId)).collect(Collectors.toList());

        if (filteredModelList.size() == 0) {
          continue;
        }

        for (Model model : filteredModelList) {
          for (Task task : model.getTaskList()) {
            boolean findRelatedTask = false;

            for (StartEvent startEvent : task.getStartEventList()) {
              for (StartEventUtter startEventUtter : startEvent.getStartEventUtterList()) {
                if (startEventUtter.getIntentId() != null && startEventUtter.getIntentId()
                    .equals(intentList.get(i).getId())) {
                  findRelatedTask = true;

                  if (strRelatedTask.contains(task.getTaskName()) == false) {
                    if (strRelatedTask.equals("")) {
                      strRelatedTask = task.getTaskName();
                    } else {
                      strRelatedTask = strRelatedTask + ", " + task.getTaskName();
                    }
                  }
                }

                if (findRelatedTask == true) {
                  break;
                }
              }

              if (findRelatedTask == true) {
                break;
              }
            }

            for (Event event : task.getEventList()) {
              if (event.getIntentId() != null && event.getIntentId()
                  .equals(intentList.get(i).getId())) {
                findRelatedTask = true;

                if (strRelatedTask.contains(task.getTaskName()) == false) {
                  if (strRelatedTask.equals("")) {
                    strRelatedTask = task.getTaskName();
                  } else {
                    strRelatedTask = strRelatedTask + ", " + task.getTaskName();
                  }
                }
              }

              if (findRelatedTask == true) {
                break;
              }

              for (EventCondition eventCondition : event.getEventConditionList()) {
                for (EventUtter eventUtter : eventCondition.getEventUtterList()) {
                  if (eventUtter.getIntentId() != null && eventUtter.getIntentId()
                      .equals(intentList.get(i).getId())) {
                    findRelatedTask = true;

                    if (strRelatedTask.contains(task.getTaskName()) == false) {
                      if (strRelatedTask.equals("")) {
                        strRelatedTask = task.getTaskName();
                      } else {
                        strRelatedTask = strRelatedTask + ", " + task.getTaskName();
                      }
                    }
                  }

                  if (findRelatedTask == true) {
                    break;
                  }
                }

                if (findRelatedTask == true) {
                  break;
                }
              }

              if (findRelatedTask == true) {
                break;
              }
            }
          }
        }

        JSONObject intentInfo = new JSONObject();

        intentInfo.put("id", intentList.get(i).getId());
        intentInfo.put("intentName", intentList.get(i).getIntentName());
        intentInfo.put("relatedTask", strRelatedTask);
        intentInfo.put("sharedYn", intentList.get(i).getSharedYn());

        JSONArray intentUtterInfoList = new JSONArray();
        for (IntentUtter intentUtter : intentList.get(i).getIntentUtterList()) {
          JSONObject intentUtterInfo = new JSONObject();

          intentUtterInfo.put("id", intentUtter.getId());
          intentUtterInfo.put("userSay", intentUtter.getUserSay());
          intentUtterInfo.put("mappedUserSay", intentUtter.getMappedUserSay());
          intentUtterInfo.put("intentId", intentUtter.getIntent().getId());

          JSONArray intentUtterDataInfoList = new JSONArray();
          for (IntentUtterData intentUtterData : intentUtter.getIntentUtterDataList()) {
            JSONObject intentUtterDataInfo = new JSONObject();

            intentUtterDataInfo.put("id", intentUtterData.getId());
            intentUtterDataInfo.put("entityId", intentUtterData.getEntityId());
            intentUtterDataInfo.put("mappedValue", intentUtterData.getMappedValue());
            intentUtterDataInfo.put("intentUtterId", intentUtterData.getIntentUtter().getId());

            intentUtterDataInfo.put("creatorId", intentUtterData.getCreatorId());
            intentUtterDataInfo.put("createdAt", intentUtterData.getCreatedAt());
            intentUtterDataInfo.put("updaterId", intentUtterData.getUpdaterId());
            intentUtterDataInfo.put("updatedAt", intentUtterData.getUpdatedAt());

            intentUtterDataInfoList.add(intentUtterDataInfo);
          }
          intentUtterInfo.put("intentUtterData", intentUtterDataInfoList);

          intentUtterInfo.put("creatorId", intentUtter.getCreatorId());
          intentUtterInfo.put("createdAt", intentUtter.getCreatedAt());
          intentUtterInfo.put("updaterId", intentUtter.getUpdaterId());
          intentUtterInfo.put("updatedAt", intentUtter.getUpdatedAt());

          intentUtterInfoList.add(intentUtterInfo);
        }
        intentInfo.put("intentUtter", intentUtterInfoList);

        intentInfo.put("creatorId", intentList.get(i).getCreatorId());
        intentInfo.put("createdAt", intentList.get(i).getCreatedAt());
        intentInfo.put("updaterId", intentList.get(i).getUpdaterId());
        intentInfo.put("updatedAt", intentList.get(i).getUpdatedAt());
        intentInfo.put("workspaceId", intentList.get(i).getWorkspaceId());

        intentInfoList.add(intentInfo);
      }

      wrappedIntentInfo.put("intent", intentInfoList);

      result.put("data", wrappedIntentInfo);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("GET /api/workspace/{}/entity/{}/intent/allinfolist", workspaceId, modelId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^R")
  @RequestMapping(value = "/workspace/{workspaceId}/sharedintent/list", method = RequestMethod.GET)
  public ResponseEntity<?> getSharedIntentListByWorkspaceId(@PathVariable String workspaceId) {
    logger.info("GET /api/workspace/{}/sharedintent/list", workspaceId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      List<Intent> intentList = new ArrayList<>();
      try {
        intentList = intentService.findIntentsByWorkspaceId(workspaceId);
      } catch (Exception ex) {
        intentList = intentService.findIntentsByWorkspaceId(workspaceId);
      }

      JSONObject wrappedIntentInfo = new JSONObject();
      JSONArray intentInfoList = new JSONArray();

      for (int i = 0; i < intentList.size(); i++) {
        if (intentList.get(i).getSharedYn().equals("Y") == false) {
          continue;
        }

        String strRelatedTask = "";

        List<Model> filteredModelList = intentList.get(i).getModelList();
        for (Model model : filteredModelList) {
          for (Task task : model.getTaskList()) {
            boolean findRelatedTask = false;

            for (StartEvent startEvent : task.getStartEventList()) {
              for (StartEventUtter startEventUtter : startEvent.getStartEventUtterList()) {
                if (startEventUtter.getIntentId() != null && startEventUtter.getIntentId()
                    .equals(intentList.get(i).getId())) {
                  findRelatedTask = true;

                  if (strRelatedTask.contains(task.getTaskName()) == false) {
                    if (strRelatedTask.equals("")) {
                      strRelatedTask = task.getTaskName();
                    } else {
                      strRelatedTask = strRelatedTask + ", " + task.getTaskName();
                    }
                  }
                }

                if (findRelatedTask == true) {
                  break;
                }
              }

              if (findRelatedTask == true) {
                break;
              }
            }

            for (Event event : task.getEventList()) {
              if (event.getIntentId() != null & event.getIntentId()
                  .equals(intentList.get(i).getId())) {
                findRelatedTask = true;

                if (strRelatedTask.contains(task.getTaskName()) == false) {
                  if (strRelatedTask.equals("")) {
                    strRelatedTask = task.getTaskName();
                  } else {
                    strRelatedTask = strRelatedTask + ", " + task.getTaskName();
                  }
                }
              }

              if (findRelatedTask == true) {
                break;
              }

              for (EventCondition eventCondition : event.getEventConditionList()) {
                for (EventUtter eventUtter : eventCondition.getEventUtterList()) {
                  if (eventUtter.getIntentId() != null && eventUtter.getIntentId()
                      .equals(intentList.get(i).getId())) {
                    findRelatedTask = true;

                    if (strRelatedTask.contains(task.getTaskName()) == false) {
                      if (strRelatedTask.equals("")) {
                        strRelatedTask = task.getTaskName();
                      } else {
                        strRelatedTask = strRelatedTask + ", " + task.getTaskName();
                      }
                    }
                  }

                  if (findRelatedTask == true) {
                    break;
                  }
                }

                if (findRelatedTask == true) {
                  break;
                }
              }

              if (findRelatedTask == true) {
                break;
              }
            }
          }
        }

        JSONObject intentInfo = new JSONObject();

        intentInfo.put("id", intentList.get(i).getId());
        intentInfo.put("intentName", intentList.get(i).getIntentName());
        intentInfo.put("relatedTask", strRelatedTask);
        intentInfo.put("sharedYn", intentList.get(i).getSharedYn());

        JSONArray intentUtterInfoList = new JSONArray();
        for (IntentUtter intentUtter : intentList.get(i).getIntentUtterList()) {
          JSONObject intentUtterInfo = new JSONObject();

          intentUtterInfo.put("id", intentUtter.getId());
          intentUtterInfo.put("userSay", intentUtter.getUserSay());
          intentUtterInfo.put("mappedUserSay", intentUtter.getMappedUserSay());
          intentUtterInfo.put("intentId", intentUtter.getIntent().getId());

          JSONArray intentUtterDataInfoList = new JSONArray();
          for (IntentUtterData intentUtterData : intentUtter.getIntentUtterDataList()) {
            JSONObject intentUtterDataInfo = new JSONObject();

            intentUtterDataInfo.put("id", intentUtterData.getId());
            intentUtterDataInfo.put("entityId", intentUtterData.getEntityId());
            intentUtterDataInfo.put("mappedValue", intentUtterData.getMappedValue());
            intentUtterDataInfo.put("intentUtterId", intentUtterData.getIntentUtter().getId());

            intentUtterDataInfo.put("creatorId", intentUtterData.getCreatorId());
            intentUtterDataInfo.put("createdAt", intentUtterData.getCreatedAt());
            intentUtterDataInfo.put("updaterId", intentUtterData.getUpdaterId());
            intentUtterDataInfo.put("updatedAt", intentUtterData.getUpdatedAt());

            intentUtterDataInfoList.add(intentUtterDataInfo);
          }
          intentUtterInfo.put("intentUtterData", intentUtterDataInfoList);

          intentUtterInfo.put("creatorId", intentUtter.getCreatorId());
          intentUtterInfo.put("createdAt", intentUtter.getCreatedAt());
          intentUtterInfo.put("updaterId", intentUtter.getUpdaterId());
          intentUtterInfo.put("updatedAt", intentUtter.getUpdatedAt());

          intentUtterInfoList.add(intentUtterInfo);
        }
        intentInfo.put("intentUtter", intentUtterInfoList);

        intentInfo.put("creatorId", intentList.get(i).getCreatorId());
        intentInfo.put("createdAt", intentList.get(i).getCreatedAt());
        intentInfo.put("updaterId", intentList.get(i).getUpdaterId());
        intentInfo.put("updatedAt", intentList.get(i).getUpdatedAt());
        intentInfo.put("workspaceId", intentList.get(i).getWorkspaceId());

        intentInfoList.add(intentInfo);
      }

      wrappedIntentInfo.put("intent", intentInfoList);

      result.put("data", wrappedIntentInfo);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("GET /api/workspace/{}/sharedintent/list", workspaceId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^R")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/intent/list", method = RequestMethod.GET)
  public ResponseEntity<?> getIntentListByWorkspaceIdAndModelId(@PathVariable String workspaceId,
      @PathVariable String modelId) {
    logger.info("GET /api/workspace/{}/entity/{}/intent/list", workspaceId, modelId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      List<Intent> intentList = new ArrayList<>();
      try {
        intentList = intentService.findIntentsByWorkspaceId(workspaceId);
      } catch (Exception ex) {
        intentList = intentService.findIntentsByWorkspaceId(workspaceId);
      }

      JSONObject wrappedIntentInfo = new JSONObject();
      JSONArray intentInfoList = new JSONArray();

      List<Model> modelListInWorkspace = new ArrayList<>();

      if (intentList.size() > 0) {
        modelListInWorkspace = modelService.findModelsByWorkspaceIdOrderByModelName(workspaceId);
      }

      for (int i = 0; i < intentList.size(); i++) {
        String strRelatedTask = "";

        List<Model> modelList = intentList.get(i).getModelList();
        List<Model> filteredModelList = modelList.stream()
            .filter(model -> model.getId().equals(modelId)).collect(Collectors.toList());

        if (filteredModelList.size() == 0) {
          continue;
        }

        for (Model model : filteredModelList) {
          for (Task task : model.getTaskList()) {
            boolean findRelatedTask = false;

            for (StartEvent startEvent : task.getStartEventList()) {
              for (StartEventUtter startEventUtter : startEvent.getStartEventUtterList()) {
                if (startEventUtter.getIntentId() != null && startEventUtter.getIntentId()
                    .equals(intentList.get(i).getId())) {
                  findRelatedTask = true;

                  if (strRelatedTask.contains(task.getTaskName()) == false) {
                    if (strRelatedTask.equals("")) {
                      strRelatedTask = task.getTaskName();
                    } else {
                      strRelatedTask = strRelatedTask + ", " + task.getTaskName();
                    }
                  }
                }

                if (findRelatedTask == true) {
                  break;
                }
              }

              if (findRelatedTask == true) {
                break;
              }
            }

            for (Event event : task.getEventList()) {
              if (event.getIntentId() != null && event.getIntentId()
                  .equals(intentList.get(i).getId())) {
                findRelatedTask = true;

                if (strRelatedTask.contains(task.getTaskName()) == false) {
                  if (strRelatedTask.equals("")) {
                    strRelatedTask = task.getTaskName();
                  } else {
                    strRelatedTask = strRelatedTask + ", " + task.getTaskName();
                  }
                }
              }

              if (findRelatedTask == true) {
                break;
              }

              for (EventCondition eventCondition : event.getEventConditionList()) {
                for (EventUtter eventUtter : eventCondition.getEventUtterList()) {
                  if (eventUtter.getIntentId() != null && eventUtter.getIntentId()
                      .equals(intentList.get(i).getId())) {
                    findRelatedTask = true;

                    if (strRelatedTask.contains(task.getTaskName()) == false) {
                      if (strRelatedTask.equals("")) {
                        strRelatedTask = task.getTaskName();
                      } else {
                        strRelatedTask = strRelatedTask + ", " + task.getTaskName();
                      }
                    }
                  }

                  if (findRelatedTask == true) {
                    break;
                  }
                }

                if (findRelatedTask == true) {
                  break;
                }
              }

              if (findRelatedTask == true) {
                break;
              }
            }
          }
        }

        JSONObject intentInfo = new JSONObject();

        intentInfo.put("id", intentList.get(i).getId());
        intentInfo.put("intentName", intentList.get(i).getIntentName());
        intentInfo.put("relatedTask", strRelatedTask);
        intentInfo.put("sharedYn", intentList.get(i).getSharedYn());

        intentInfo.put("creatorId", intentList.get(i).getCreatorId());
        intentInfo.put("createdAt", intentList.get(i).getCreatedAt());
        intentInfo.put("updaterId", intentList.get(i).getUpdaterId());
        intentInfo.put("updatedAt", intentList.get(i).getUpdatedAt());
        intentInfo.put("workspaceId", intentList.get(i).getWorkspaceId());

        intentInfoList.add(intentInfo);
      }

      wrappedIntentInfo.put("intentInfoList", intentInfoList);

      result.put("data", wrappedIntentInfo);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("GET /api/workspace/{}/entity/{}/intent/list", workspaceId, modelId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^R")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/intent/{intentId}", method = RequestMethod.GET)
  public ResponseEntity<?> getIntentByWorkspaceIdAndModelId(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String intentId) {
    logger.info("GET /api/workspace/{}/entity/{}/intent/{}", workspaceId, modelId, intentId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      Intent intent = null;
      try {
        intent = intentService.findIntentByWorkspaceIdAndId(workspaceId, intentId);
      } catch (Exception ex) {
        intent = intentService.findIntentByWorkspaceIdAndId(workspaceId, intentId);
      }

      JSONObject wrappedIntentInfo = new JSONObject();
      JSONObject intentInfo = new JSONObject();

      intentInfo.put("id", intent.getId());
      intentInfo.put("entityName", intent.getIntentName());
      intentInfo.put("sharedYn", intent.getSharedYn());

      intentInfo.put("creatorId", intent.getCreatorId());
      intentInfo.put("createdAt", intent.getCreatedAt());
      intentInfo.put("updaterId", intent.getUpdaterId());
      intentInfo.put("updatedAt", intent.getUpdatedAt());
      intentInfo.put("workspaceId", intent.getWorkspaceId());

      wrappedIntentInfo.put("intentInfo", intentInfo);

      result.put("data", wrappedIntentInfo);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("GET /api/workspace/{}/entity/{}/intent/{}", workspaceId, modelId, intentId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^U")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/intent/{intentId}", method = RequestMethod.PUT)
  public ResponseEntity<?> updateIntent(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String intentId,
      @RequestBody JSONObject bodyParam) {
    logger.info("PUT /api/workspace/{}/entity/{}/intent/{}", workspaceId, modelId, intentId,
        bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      String intentName = null;
      String sharedYn = null;
      String userId = null;

      Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

      Intent findIntent = null;
      try {
        findIntent = intentService.findIntentByWorkspaceIdAndId(workspaceId, intentId);
      } catch (Exception ex) {
        findIntent = intentService.findIntentByWorkspaceIdAndId(workspaceId, intentId);
      }
      if (findIntent == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "Intent doesn't exist.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      if (bodyParam.get("intentName") != null) {
        intentName = bodyParam.get("intentName").toString().trim();
      }

      if (bodyParam.get("sharedYn") != null && !bodyParam.get("sharedYn").toString().trim()
          .equals("")) {
        sharedYn = bodyParam.get("sharedYn").toString().trim();
      }

      if (bodyParam.get("userId") == null || bodyParam.get("userId").toString().trim().equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "userId is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        userId = bodyParam.get("userId").toString().trim();
      }

      if (!findIntent.getCreatorId().equals(userId)) {
        result.put("statusCode", 400);
        result.put("statusMessage", "다른 사용자가 작성한 모델은 수정할 수 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      Model findModel = null;
      try {
        findModel = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
      } catch (Exception ex) {
        findModel = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
      }
      if (findModel == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "Model doesn't exist.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      List<Intent> intentList = new ArrayList<>();
      try {
        intentList = intentService.findIntentsByWorkspaceId(workspaceId);
      } catch (Exception ex) {
        intentList = intentService.findIntentsByWorkspaceId(workspaceId);
      }

      List<Intent> filteredIntentList = new ArrayList<>();
      for (int i = 0; i < intentList.size(); i++) {
        List<Model> modelList = intentList.get(i).getModelList();
        List<Model> filteredModelList = modelList.stream()
            .filter(model -> model.getId().equals(modelId)).collect(Collectors.toList());
        if (filteredModelList.size() == 0) {
          continue;
        }

        filteredIntentList.add(intentList.get(i));
      }

      if (intentName != null && !intentName.equals("")) {
        for (int i = 0; i < filteredIntentList.size(); i++) {
          if (intentName != null) {
            if (filteredIntentList.get(i).getIntentName().equals(intentName)) {
              result.put("statusCode", 400);
              result.put("statusMessage", "intentName duplicated.");

              return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
            }
          }
        }
      }

      if (intentName != null) {
        findIntent.setIntentName(intentName);
      }

      if (sharedYn != null) {
        findIntent.setSharedYn(sharedYn);
      }

      findIntent.setUpdaterId(userId);
      findIntent.setUpdatedAt(currentTimestamp);

      try {
        intentService.updateIntent(findIntent);
      } catch (Exception ex) {
        intentService.updateIntent(findIntent);
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("PUT /api/workspace/{}/entity/{}/intent/{}", workspaceId, modelId, intentId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^D")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/intent/{intentId}", method = RequestMethod.POST)
  public ResponseEntity<?> deleteIntentByWorkspaceIdAndId(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String intentId,
      @RequestBody JSONObject bodyParam) {
    logger.info("POST /api/workspace/{}/entity/{}/intent/{}", workspaceId, modelId, intentId,
        bodyParam);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      boolean isLinkDelete = false;

      if (bodyParam.get("isLinkDelete") != null) {
        isLinkDelete = (boolean) bodyParam.get("isLinkDelete");
      }

      Intent intent = null;
      try {
        intent = intentService.findIntentByWorkspaceIdAndId(workspaceId, intentId);
      } catch (Exception ex) {
        intent = intentService.findIntentByWorkspaceIdAndId(workspaceId, intentId);
      }

      if (intent == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "intentId 와 일치하는 Intent 정보가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      List<String> entityIdList = new ArrayList<>();
      for (IntentUtter intentUtter : intent.getIntentUtterList()) {
        for (IntentUtterData intentUtterData : intentUtter.getIntentUtterDataList()) {
          entityIdList.add(intentUtterData.getEntityId());
        }
      }

      if (isLinkDelete == true) {
        Model findModel = null;
        try {
          findModel = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
        } catch (Exception ex) {
          findModel = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
        }

        for (String entityId : entityIdList) {
          for (int i = 0; i < findModel.getEntityList().size(); i++) {
            if (findModel.getEntityList().get(i).getId().equals(entityId)) {
              findModel.getEntityList().remove(i);
              break;
            }
          }
        }

        for (int i = 0; i < findModel.getIntentList().size(); i++) {
          if (findModel.getIntentList().get(i).getId().equals(intentId)) {
            findModel.getIntentList().remove(i);
            break;
          }
        }

        try {
          modelService.updateModel(findModel);
        } catch (Exception ex) {
          modelService.updateModel(findModel);
        }

        for (String entityId : entityIdList) {
          Entity entity = entityService.findEntityByWorkspaceIdAndId(workspaceId, entityId);
          for (int i = 0; i < entity.getModelList().size(); i++) {
            if (entity.getModelList().get(i).getId().equals(findModel.getId())) {
              entity.getModelList().remove(i);
              break;
            }
          }

          try {
            entityService.updateEntity(entity);
          } catch (Exception ex) {
            entityService.updateEntity(entity);
          }
        }

        for (int i = 0; i < intent.getModelList().size(); i++) {
          if (intent.getModelList().get(i).getId().equals(findModel.getId())) {
            intent.getModelList().remove(i);
            break;
          }
        }

        try {
          intentService.updateIntent(intent);
        } catch (Exception ex) {
          intentService.updateIntent(intent);
        }
      } else {
        List<Model> modelList = modelService.findModelsByWorkspaceIdOrderByModelName(workspaceId);
        for (Model model : modelList) {
          for (String entityId : entityIdList) {
            for (int i = 0; i < model.getEntityList().size(); i++) {
              if (model.getEntityList().get(i).getId().equals(entityId)) {
                model.getEntityList().remove(i);

                try {
                  modelService.updateModel(model);
                } catch (Exception ex) {
                  modelService.updateModel(model);
                }

                break;
              }
            }
          }

          for (int i = 0; i < model.getIntentList().size(); i++) {
            if (model.getIntentList().get(i).getId().equals(intentId)) {
              model.getIntentList().remove(i);

              try {
                modelService.updateModel(model);
              } catch (Exception ex) {
                modelService.updateModel(model);
              }

              break;
            }
          }
        }

        for (String entityId : entityIdList) {
          Entity entity = entityService.findEntityByWorkspaceIdAndId(workspaceId, entityId);
          entity.setModelList(new ArrayList<>());

          try {
            entityService.updateEntity(entity);
            entityService.deleteEntityByWorkspaceIdAndId(workspaceId, entityId);
          } catch (Exception ex) {
            entityService.updateEntity(entity);
            entityService.deleteEntityByWorkspaceIdAndId(workspaceId, entityId);
          }
        }

        intent.setModelList(new ArrayList<>());

        try {
          intentService.deleteIntentByWorkspaceIdAndId(workspaceId, intentId);
        } catch (Exception ex) {
          intentService.deleteIntentByWorkspaceIdAndId(workspaceId, intentId);
        }
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("POST /api/workspace/{}/entity/{}/intent/{}", workspaceId, modelId, intentId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^U")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/intent/{intentId}/share", method = RequestMethod.PUT)
  public ResponseEntity<?> updateIntentShare(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String intentId,
      @RequestBody JSONObject bodyParam) {
    logger.info("PUT /api/workspace/{}/entity/{}/intent/{}/share", workspaceId, modelId, intentId,
        bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      String userId = null;

      Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

      Intent findIntent = null;
      try {
        findIntent = intentService.findIntentByWorkspaceIdAndId(workspaceId, intentId);
      } catch (Exception ex) {
        findIntent = intentService.findIntentByWorkspaceIdAndId(workspaceId, intentId);
      }
      if (findIntent == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "Intent doesn't exist.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      if (bodyParam.get("userId") == null || bodyParam.get("userId").toString().trim().equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "userId is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        userId = bodyParam.get("userId").toString().trim();
      }

      if (!findIntent.getCreatorId().equals(userId)) {
        result.put("statusCode", 400);
        result.put("statusMessage", "다른 사용자가 작성한 모델은 수정할 수 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      List<Intent> intentList = new ArrayList<>();
      try {
        intentList = intentService.findIntentsByWorkspaceId(workspaceId);
      } catch (Exception ex) {
        intentList = intentService.findIntentsByWorkspaceId(workspaceId);
      }

      for (Intent intent : intentList) {
        if (intent.getSharedYn().equals("Y") && intent.getIntentName()
            .equals(findIntent.getIntentName())) {
          result.put("statusCode", 400);
          result.put("statusMessage", "Share Intent 목록에 동일한 이름의 Intent 가 있습니다.");

          return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }
      }

      Model findModel = null;
      try {
        findModel = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
      } catch (Exception ex) {
        findModel = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
      }
      if (findModel == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "Model doesn't exist.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      boolean isSharedOk = true;
      List<Entity> entityListInWorksapce = null;
      try {
        entityListInWorksapce = entityService.findEntitiesByWorkspaceId(workspaceId);
      } catch (Exception ex) {
        entityListInWorksapce = entityService.findEntitiesByWorkspaceId(workspaceId);
      }

      if (entityListInWorksapce != null) {
        entityListInWorksapce = entityListInWorksapce.stream()
            .filter(entity -> entity.getSharedYn().equals("Y")).collect(Collectors.toList());
      }

      for (Entity entity : findModel.getEntityList()) {
        if (entity.getSharedYn().equals("Y")) {
          continue;
        }

        for (Entity compareEntity : entityListInWorksapce) {
          if (compareEntity.getId().equals(entity.getId())) {
            continue;
          }

          if (compareEntity.getClassName().equals(entity.getClassName()) && compareEntity
              .getEntityName().equals(entity.getEntityName())) {
            isSharedOk = false;
            break;
          }
        }
      }

      if (isSharedOk == false) {
        result.put("statusCode", 400);
        result.put("statusMessage",
            "Intent 에서 사용하는 Entity 중 동일한 이름으로 Share 된 Entity 가 포함되어 있어 Share 할 수 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      for (Entity entity : findModel.getEntityList()) {
        if (entity.getSharedYn().equals("Y")) {
          continue;
        }

        entity.setSharedYn("Y");
        entity.setUpdaterId(userId);
        entity.setUpdatedAt(currentTimestamp);
        try {
          entityService.updateEntity(entity);
        } catch (Exception ex) {
          entityService.updateEntity(entity);
        }
      }

      findIntent.setSharedYn("Y");
      findIntent.setUpdaterId(userId);
      findIntent.setUpdatedAt(currentTimestamp);

      try {
        intentService.updateIntent(findIntent);
      } catch (Exception ex) {
        intentService.updateIntent(findIntent);
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger
          .error("PUT /api/workspace/{}/entity/{}/intent/{}/share", workspaceId, modelId, intentId,
              e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^C")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/intent/{intentId}/import", method = RequestMethod.PUT)
  public ResponseEntity<?> importSharedIntent(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String intentId,
      @RequestBody JSONObject bodyParam) {
    logger.info("PUT /api/workspace/{}/entity/{}/intent/{}/import", workspaceId, modelId, intentId,
        bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      String userId = null;

      Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

      Intent findIntent = null;
      try {
        findIntent = intentService.findIntentByWorkspaceIdAndId(workspaceId, intentId);
      } catch (Exception ex) {
        findIntent = intentService.findIntentByWorkspaceIdAndId(workspaceId, intentId);
      }
      if (findIntent == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "Intent doesn't exist.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      if (bodyParam.get("userId") == null || bodyParam.get("userId").toString().trim().equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "userId is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        userId = bodyParam.get("userId").toString().trim();
      }

      if (findIntent.getSharedYn().equals("Y") == false) {
        result.put("statusCode", 400);
        result.put("statusMessage", "Shared Intent 가 아닙니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      Model findModel = null;
      try {
        findModel = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
      } catch (Exception ex) {
        findModel = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
      }
      if (findModel == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "Model doesn't exist.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      List<Entity> entityListInWorkspace = entityService.findEntitiesByWorkspaceId(workspaceId);
      for (Intent intent : findModel.getIntentList()) {
//                if (intent.getId().equals(findIntent.getId())) {
//                    result.put("statusCode", 400);
//                    result.put("statusMessage", "이미 import 한 Intent 입니다.");
//
//                    return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
//                }

        if (intent.getIntentName().equals(findIntent.getIntentName())) {
          result.put("statusCode", 400);
          result.put("statusMessage", "동일한 이름의 Intent 가 있습니다.");

          return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        for (IntentUtter intentUtter : intent.getIntentUtterList()) {
          for (IntentUtterData intentUtterData : intentUtter.getIntentUtterDataList()) {
            for (Entity entityInModel : findModel.getEntityList()) {
              if (entityInModel.getId().equals(intentUtterData.getEntityId())) {
                continue;
              }

              List<Entity> filteredEntityList = entityListInWorkspace.stream()
                  .filter(entity -> entity.getId().equals(intentUtterData.getEntityId()))
                  .collect(Collectors.toList());
              if (filteredEntityList.size() == 0) {
                result.put("statusCode", 400);
                result.put("statusMessage",
                    "Intent 에서 사용하는 Entity 중 DB 에 존재하지 않는 Entity 가 포함되어 있습니다.");

                return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
              } else {
                for (Entity tempEntity : filteredEntityList) {
                  if (entityInModel.getClassName().equals(tempEntity.getClassName())
                      && entityInModel.getEntityName().equals(tempEntity.getEntityName())) {
                    result.put("statusCode", 400);
                    result.put("statusMessage",
                        "Intent 에서 사용하는 Entity 중 동일한 이름" + "(" + tempEntity.getClassName() + " - "
                            + tempEntity.getEntityName() + ") 의 Entity 가 포함되어 있습니다.");

                    return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
                  }
                }
              }
            }
          }
        }
      }

      HashMap<String, String> keyMap = new HashMap<>();

      List<String> insertEntityIdList = new ArrayList<>();
      List<Entity> insertEntityList = new ArrayList<>();
      List<EntityData> insertEntityDataList = new ArrayList<>();
      List<EntityDataSynonym> insertEntityDataSynonymList = new ArrayList<>();

      for (IntentUtter findIntentUtter : findIntent.getIntentUtterList()) {
        for (IntentUtterData findIntentUtterData : findIntentUtter.getIntentUtterDataList()) {
          keyMap.put(findIntentUtterData.getEntityId(), UUID.randomUUID().toString());
          insertEntityIdList.add(findIntentUtterData.getEntityId());
        }
      }

      for (String entityId : insertEntityIdList) {
        List<Entity> filteredEntityList = entityListInWorkspace.stream()
            .filter(entity -> entity.getId().equals(entityId)).collect(Collectors.toList());
        if (filteredEntityList.size() == 0) {
          result.put("statusCode", 400);
          result.put("statusMessage", "Intent 에서 사용하는 Entity 중 DB 에 존재하지 않는 Entity 가 포함되어 있습니다.");

          return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        } else {
          Entity insertEntity = new Entity();
          insertEntity.setId(keyMap.get(filteredEntityList.get(0).getId()));
          insertEntity.setClassName(filteredEntityList.get(0).getClassName());
          insertEntity.setClassSource(filteredEntityList.get(0).getClassSource());
          insertEntity.setEntityName(filteredEntityList.get(0).getEntityName());
          insertEntity.setEntitySource(filteredEntityList.get(0).getEntitySource());
          insertEntity.setEntityType(filteredEntityList.get(0).getEntityType());
          insertEntity.setSharedYn("N");

          insertEntity.setCreatorId(userId);
          insertEntity.setCreatedAt(currentTimestamp);
          insertEntity.setUpdaterId(userId);
          insertEntity.setUpdatedAt(currentTimestamp);
          insertEntity.setWorkspaceId(workspaceId);

          insertEntityList.add(insertEntity);

          for (EntityData findEntityData : filteredEntityList.get(0).getEntityDataList()) {
            keyMap.put(findEntityData.getId(), UUID.randomUUID().toString());

            EntityData insertEntityData = new EntityData();
            insertEntityData.setId(keyMap.get(findEntityData.getId()));
            insertEntityData.setEntityData(findEntityData.getEntityData());

            insertEntityData.setCreatorId(userId);
            insertEntityData.setCreatedAt(currentTimestamp);
            insertEntityData.setUpdaterId(userId);
            insertEntityData.setUpdatedAt(currentTimestamp);
            insertEntityData.setEntity(insertEntity);

            for (EntityDataSynonym findEntityDataSynonym : findEntityData
                .getEntityDataSynonymList()) {
              keyMap.put(findEntityDataSynonym.getId(), UUID.randomUUID().toString());

              EntityDataSynonym insertEntityDataSynonym = new EntityDataSynonym();
              insertEntityDataSynonym.setId(keyMap.get(findEntityDataSynonym.getId()));
              insertEntityDataSynonym
                  .setEntityDataSynonym(findEntityDataSynonym.getEntityDataSynonym());

              insertEntityDataSynonym.setCreatorId(userId);
              insertEntityDataSynonym.setCreatedAt(currentTimestamp);
              insertEntityDataSynonym.setUpdaterId(userId);
              insertEntityDataSynonym.setUpdatedAt(currentTimestamp);
              insertEntityDataSynonym.setEntityData(insertEntityData);

              insertEntityDataSynonymList.add(insertEntityDataSynonym);
            }

            insertEntityDataList.add(insertEntityData);
          }
        }
      }

      Intent insertIntent = new Intent();
      List<IntentUtter> insertIntentUtterList = new ArrayList<>();
      List<IntentUtterData> insertIntentUtterDataList = new ArrayList<>();

      // intent
      keyMap.put(findIntent.getId(), UUID.randomUUID().toString());
      insertIntent.setId(keyMap.get(findIntent.getId()));
      insertIntent.setIntentName(findIntent.getIntentName());
      insertIntent.setSharedYn("N");

      insertIntent.setCreatorId(userId);
      insertIntent.setCreatedAt(currentTimestamp);
      insertIntent.setUpdaterId(userId);
      insertIntent.setUpdatedAt(currentTimestamp);
      insertIntent.setWorkspaceId(workspaceId);

      for (IntentUtter findIntentUtter : findIntent.getIntentUtterList()) {
        keyMap.put(findIntentUtter.getId(), UUID.randomUUID().toString());
        IntentUtter insertIntentUtter = new IntentUtter();
        insertIntentUtter.setId(keyMap.get(findIntentUtter.getId()));
        insertIntentUtter.setUserSay(findIntentUtter.getUserSay());
        String mappedUserSay = findIntentUtter.getMappedUserSay();
        for (String key : keyMap.keySet()) {
          mappedUserSay = mappedUserSay.replace(key, keyMap.get(key));
        }
        insertIntentUtter.setMappedUserSay(mappedUserSay);

        insertIntentUtter.setCreatorId(userId);
        insertIntentUtter.setCreatedAt(currentTimestamp);
        insertIntentUtter.setUpdaterId(userId);
        insertIntentUtter.setUpdatedAt(currentTimestamp);
        insertIntentUtter.setIntent(insertIntent);

        for (IntentUtterData findIntentUtterData : findIntentUtter.getIntentUtterDataList()) {
          keyMap.put(findIntentUtterData.getId(), UUID.randomUUID().toString());
          IntentUtterData insertIntentUtterData = new IntentUtterData();
          insertIntentUtterData.setId(keyMap.get(findIntentUtterData.getId()));
          insertIntentUtterData.setEntityId(keyMap.get(findIntentUtterData.getEntityId()));
          insertIntentUtterData.setMappedValue(findIntentUtterData.getMappedValue());

          insertIntentUtterData.setCreatorId(userId);
          insertIntentUtterData.setCreatedAt(currentTimestamp);
          insertIntentUtterData.setUpdaterId(userId);
          insertIntentUtterData.setUpdatedAt(currentTimestamp);
          insertIntentUtterData.setIntentUtter(insertIntentUtter);

          insertIntentUtterDataList.add(insertIntentUtterData);
        }

        insertIntentUtterList.add(insertIntentUtter);
      }

//            for (Entity entity : insertEntityList) {
//                List<Entity> entityListInModel = findModel.getEntityList();
//                entityListInModel.add(entity);
//                findModel.setEntityList(entityListInModel);
//            }
//
//            List<Intent> intentListInModel = findModel.getIntentList();
//            intentListInModel.add(insertIntent);
//            findModel.setIntentList(intentListInModel);

//            try {
//                modelService.updateModel(findModel);
//            } catch (Exception ex) {
//                modelService.updateModel(findModel);
//            }

      for (Entity entity : insertEntityList) {
        List<Model> modelList = entity.getModelList();
        modelList.add(findModel);
        entity.setModelList(modelList);

        entityService.insertEntity(entity);

        List<Entity> entityListInModel = findModel.getEntityList();
        entityListInModel.add(entity);
        findModel.setEntityList(entityListInModel);
      }

      for (EntityData entityData : insertEntityDataList) {
        entityDataService.insertEntityData(entityData);
      }

      for (EntityDataSynonym entityDataSynonym : insertEntityDataSynonymList) {
        entityDataSynonymService.insertEntityDataSynonym(entityDataSynonym);
      }

      intentService.insertIntent(insertIntent);

      List<Intent> intentListInModel = findModel.getIntentList();
      intentListInModel.add(insertIntent);
      findModel.setIntentList(intentListInModel);

      for (IntentUtter intentUtter : insertIntentUtterList) {
        intentUtterService.insertIntentUtter(intentUtter);
      }

      for (IntentUtterData intentUtterData : insertIntentUtterDataList) {
        intentUtterDataService.insertIntentUtterData(intentUtterData);
      }

      try {
        modelService.updateModel(findModel);
      } catch (Exception ex) {
        modelService.updateModel(findModel);
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("PUT /api/workspace/{}/entity/{}/intent/{}", workspaceId, modelId, intentId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^X")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/intent/{intentId}/link", method = RequestMethod.PUT)
  public ResponseEntity<?> linkSharedIntent(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String intentId,
      @RequestBody JSONObject bodyParam) {
    logger.info("PUT /api/workspace/{}/entity/{}/intent/{}/link", workspaceId, modelId, intentId,
        bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      String userId = null;

      Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

      Intent findIntent = null;
      try {
        findIntent = intentService.findIntentByWorkspaceIdAndId(workspaceId, intentId);
      } catch (Exception ex) {
        findIntent = intentService.findIntentByWorkspaceIdAndId(workspaceId, intentId);
      }
      if (findIntent == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "Intent doesn't exist.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      if (bodyParam.get("userId") == null || bodyParam.get("userId").toString().trim().equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "userId is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        userId = bodyParam.get("userId").toString().trim();
      }

      if (findIntent.getSharedYn().equals("Y") == false) {
        result.put("statusCode", 400);
        result.put("statusMessage", "Shared Intent 가 아닙니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      Model findModel = null;
      try {
        findModel = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
      } catch (Exception ex) {
        findModel = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
      }
      if (findModel == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "Model doesn't exist.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      List<Entity> entityListInWorkspace = entityService.findEntitiesByWorkspaceId(workspaceId);
      for (Intent intent : findModel.getIntentList()) {
        if (intent.getId().equals(findIntent.getId())) {
          result.put("statusCode", 400);
          result.put("statusMessage", "이미 Link 된 Intent 입니다.");

          return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        if (intent.getIntentName().equals(findIntent.getIntentName())) {
          result.put("statusCode", 400);
          result.put("statusMessage", "동일한 이름의 Intent 가 있습니다.");

          return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        for (IntentUtter intentUtter : intent.getIntentUtterList()) {
          for (IntentUtterData intentUtterData : intentUtter.getIntentUtterDataList()) {
            for (Entity entityInModel : findModel.getEntityList()) {
              if (entityInModel.getId().equals(intentUtterData.getEntityId())) {
                continue;
              }

              List<Entity> filteredEntityList = entityListInWorkspace.stream()
                  .filter(entity -> entity.getId().equals(intentUtterData.getEntityId()))
                  .collect(Collectors.toList());
              if (filteredEntityList.size() == 0) {
                result.put("statusCode", 400);
                result.put("statusMessage",
                    "Intent 에서 사용하는 Entity 중 DB 에 존재하지 않는 Entity 가 포함되어 있습니다.");

                return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
              } else {
                for (Entity tempEntity : filteredEntityList) {
                  if (entityInModel.getClassName().equals(tempEntity.getClassName())
                      && entityInModel.getEntityName().equals(tempEntity.getEntityName())) {
                    result.put("statusCode", 400);
                    result.put("statusMessage",
                        "Intent 에서 사용하는 Entity 중 동일한 이름" + "(" + tempEntity.getClassName() + " - "
                            + tempEntity.getEntityName() + ") 의 Entity 가 포함되어 있습니다.");

                    return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
                  }
                }
              }
            }
          }
        }
      }

      List<Entity> insertEntityList = new ArrayList<>();
      for (IntentUtter intentUtter : findIntent.getIntentUtterList()) {
        for (IntentUtterData intentUtterData : intentUtter.getIntentUtterDataList()) {
          if (findModel.getEntityList().size() == 0) {
            List<Entity> filteredEntityList = entityListInWorkspace.stream()
                .filter(entity -> entity.getId().equals(intentUtterData.getEntityId()))
                .collect(Collectors.toList());
            if (filteredEntityList.size() == 0) {
              result.put("statusCode", 400);
              result
                  .put("statusMessage", "Intent 에서 사용하는 Entity 중 DB 에 존재하지 않는 Entity 가 포함되어 있습니다.");

              return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
            } else {
              for (Entity tempEntity : filteredEntityList) {
                List<Entity> existEntityList = insertEntityList.stream()
                    .filter(entity -> entity.getId().equals(tempEntity.getId()))
                    .collect(Collectors.toList());
                if (existEntityList.size() == 0) {
                  insertEntityList.add(tempEntity);
                }
              }
            }
          } else {
            for (Entity entityInModel : findModel.getEntityList()) {
              if (entityInModel.getId().equals(intentUtterData.getEntityId())) {
                continue;
              }

              List<Entity> filteredEntityList = entityListInWorkspace.stream()
                  .filter(entity -> entity.getId().equals(intentUtterData.getEntityId()))
                  .collect(Collectors.toList());
              if (filteredEntityList.size() == 0) {
                result.put("statusCode", 400);
                result.put("statusMessage",
                    "Intent 에서 사용하는 Entity 중 DB 에 존재하지 않는 Entity 가 포함되어 있습니다.");

                return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
              } else {
                for (Entity tempEntity : filteredEntityList) {
                  if (entityInModel.getClassName().equals(tempEntity.getClassName())
                      && entityInModel.getEntityName().equals(tempEntity.getEntityName())) {
                    result.put("statusCode", 400);
                    result.put("statusMessage",
                        "Intent 에서 사용하는 Entity 중 동일한 이름" + "(" + tempEntity.getClassName() + " - "
                            + tempEntity.getEntityName() + ") 의 Entity 가 포함되어 있습니다.");

                    return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
                  } else {
                    List<Entity> existEntityList = insertEntityList.stream()
                        .filter(entity -> entity.getId().equals(tempEntity.getId()))
                        .collect(Collectors.toList());
                    if (existEntityList.size() == 0) {
                      insertEntityList.add(tempEntity);
                    }
                  }
                }
              }
            }
          }
        }
      }

      for (Entity entity : insertEntityList) {
        List<Entity> entityListInModel = findModel.getEntityList();
        entityListInModel.add(entity);
        findModel.setEntityList(entityListInModel);
      }

      List<Intent> intentListInModel = findModel.getIntentList();
      intentListInModel.add(findIntent);
      findModel.setIntentList(intentListInModel);

      try {
        modelService.updateModel(findModel);
      } catch (Exception ex) {
        modelService.updateModel(findModel);
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("PUT /api/workspace/{}/entity/{}/intent/{}/link", workspaceId, modelId, intentId,
          e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^R")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/intentdownload", method = RequestMethod.GET)
  public ResponseEntity<?> downloadIntentListByWorkspaceIdAndModelId(
      @PathVariable String workspaceId, @PathVariable String modelId) {
    logger.info("GET /api/workspace/{}/entity/{}/intentdownload", workspaceId, modelId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      Model findModel = null;
      try {
        findModel = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
      } catch (Exception ex) {
        findModel = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
      }

      if (findModel == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "Model doesn't exist.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      List<Intent> intentList = findModel.getIntentList();
      List<Entity> entityList = findModel.getEntityList();

      JSONObject wrappedIntentInfo = new JSONObject();

      StringBuilder strResult = new StringBuilder();
      strResult.append("intentName");
      strResult.append("\t");
      strResult.append("userSay");
      strResult.append("\t");
      strResult.append("mappedUserSay");
      strResult.append("\t");
      strResult.append("mappedValue");
      strResult.append("\n");

      for (int i = 0; i < intentList.size(); i++) {
        // intentName, userSay, mappedUserSay, mappedValue

        StringBuilder strIntentInfo = new StringBuilder();

        strIntentInfo.append(intentList.get(i).getIntentName());

        if (intentList.get(i).getIntentUtterList().size() == 0) {
          strResult.append(strIntentInfo);
          strResult.append("\n");
        } else {
          for (IntentUtter intentUtter : intentList.get(i).getIntentUtterList()) {
            StringBuilder strIntentUtterInfo = new StringBuilder();

            strIntentUtterInfo.append(intentUtter.getUserSay());

            String mappedUserSay = intentUtter.getMappedUserSay();
            for (Entity entity : entityList) {
              mappedUserSay = mappedUserSay.replace(entity.getId(), entity.getEntityName());
            }
            strIntentUtterInfo.append("\t");
            strIntentUtterInfo.append(mappedUserSay);

            if (intentUtter.getIntentUtterDataList().size() == 0) {
              strResult.append(strIntentInfo);
              strResult.append("\t");
              strResult.append(strIntentUtterInfo);
              strResult.append("\n");
            } else {
              List<String> intentUtterDataList = new ArrayList<>();
              for (IntentUtterData intentUtterData : intentUtter.getIntentUtterDataList()) {
                intentUtterDataList.add(intentUtterData.getMappedValue());
              }

              String strIntentUtterDataInfo = String.join("|", intentUtterDataList);

              strResult.append(strIntentInfo);
              strResult.append("\t");
              strResult.append(strIntentUtterInfo);
              strResult.append("\t");
              strResult.append(strIntentUtterDataInfo);
              strResult.append("\n");

//                            for (IntentUtterData intentUtterData : intentUtter.getIntentUtterDataList()) {
//                                StringBuilder strIntentUtterDataInfo = new StringBuilder();
//
//                                strIntentUtterDataInfo.append(intentUtterData.getMappedValue());
//
//                                strResult.append(strIntentInfo);
//                                strResult.append("\t");
//                                strResult.append(strIntentUtterInfo);
//                                strResult.append("\t");
//                                strResult.append(strIntentUtterDataInfo);
//                                strResult.append("\n");
//                            }
            }
          }
        }
      }

      HttpHeaders header = new HttpHeaders();
      header.add("Content-Type", "text/tsv; charset=UTF-8");
      header.add("Content-Disposition", "attachment; filename=\"" + "intent.tsv" + "\"");

      return new ResponseEntity<>(strResult, header, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("GET /api/workspace/{}/entity/{}/intentdownload", workspaceId, modelId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^C")
  @RequestMapping(value = "/user/{userId}/workspace/{workspaceId}/model/{modelId}/intentupload", method = RequestMethod.POST)
  public ResponseEntity<?> uploadIntentListByWorkspaceIdAndModelId(@PathVariable String userId,
      @PathVariable String workspaceId, @PathVariable String modelId,
      @RequestPart MultipartFile sourceFile) {
    logger.info("POST /api/userid/{}/workspace/{}/entity/{}/intentupload", userId, workspaceId,
        modelId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

    BufferedReader br = null;
    String line;
    String tsvSplitBy = "\t";

    try {
      Model findModel = null;
      try {
        findModel = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
      } catch (Exception ex) {
        findModel = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
      }

      if (findModel == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "Model doesn't exist.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

//            HashMap<String, String> entityNameAndEntityIdMap = new HashMap<>();
      HashMap<String, String> valueAndEntityIdMap = new HashMap<>();

      List<Entity> entityList = findModel.getEntityList();
      List<Intent> intentList = findModel.getIntentList();

      long rowCount = 0;
      long successCount = 0;
      long failCount = 0;
      List<String> failDataList = new ArrayList<>();

      br = new BufferedReader(new InputStreamReader(sourceFile.getInputStream(), "UTF-8"));
      while ((line = br.readLine()) != null) {
        rowCount++;

        if (rowCount == 1) {
          if (line.startsWith("\uFEFF")) {
            line = line.substring(1);
          }

          String[] field = line.split(tsvSplitBy);

          if (field.length != 4) {
            result.put("statusCode", 500);
            result.put("statusMessage", "Intent Data 형식의 파일이 아닙니다.");

            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
          }

          if (field[0].equals("intentName") && field[1].equals("userSay") && field[2]
              .equals("mappedUserSay") && field[3].equals("mappedValue")) {
            continue;
          } else {
            result.put("statusCode", 500);
            result.put("statusMessage", "Intent Data 형식의 파일이 아닙니다.");

            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
          }
        } else {
          String[] field = line.split(tsvSplitBy);

          List<Intent> filteredIntentList = intentList.stream()
              .filter(intent -> intent.getIntentName().equals(field[0]))
              .collect(Collectors.toList());
          if (filteredIntentList.size() == 0) {
            // Intent
            if (field.length >= 1 && !field[0].trim().equals("")) {
              Intent intent = new Intent();
              intent.setId(UUID.randomUUID().toString());
              intent.setIntentName(field[0]);
              intent.setSharedYn("N");

              intent.setWorkspaceId(workspaceId);
              intent.setCreatorId(userId);
              intent.setCreatedAt(currentTimestamp);
              intent.setUpdaterId(userId);
              intent.setUpdatedAt(currentTimestamp);

              findModel.addIntent(intent);
              intent.addModel(findModel);

              try {
                intentService.insertIntent(intent);
              } catch (Exception ex) {
                intentService.insertIntent(intent);
              }

              try {
                modelService.updateModel(findModel);
              } catch (Exception ex) {
                modelService.updateModel(findModel);
              }

              // Intent Utter
              if (field.length >= 3 && !field[1].trim().equals("")) {
                IntentUtter intentUtter = new IntentUtter();
                intentUtter.setId(UUID.randomUUID().toString());
                intentUtter.setUserSay(field[1]);

                String mappedUserSay = field[2];
                Pattern pattern = Pattern.compile("<(.*?)>");
                Matcher matcher = pattern.matcher(mappedUserSay);

                int notFouncEntityCount = 0;
                while (matcher.find()) {
                  String[] mappedData = matcher.group(1).split("=");
                  String entityName = mappedData[0];
                  String value = mappedData[1];

                  boolean isFindEntity = false;
                  for (Entity entity : entityList) {
                    if (entity.getEntityName().equals(entityName)) {
//                                            entityNameAndEntityIdMap.put(entityName, entity.getId());
                      mappedUserSay = mappedUserSay.replace(entityName + "=", entity.getId() + "=");
                      valueAndEntityIdMap.put(value, entity.getId());
                      isFindEntity = true;

                      break;
                    }
                  }

                  if (isFindEntity == false) {
                    notFouncEntityCount++;
                  }
                }

                if (notFouncEntityCount > 0) {
                  failCount++;
                  failDataList.add(line);
                  continue;
                }

                intentUtter.setMappedUserSay(mappedUserSay);
                intentUtter.setIntent(intent);

                intentUtter.setCreatorId(userId);
                intentUtter.setCreatedAt(currentTimestamp);
                intentUtter.setUpdaterId(userId);
                intentUtter.setUpdatedAt(currentTimestamp);

                try {
                  intentUtterService.insertIntentUtter(intentUtter);

                  for (Intent findIntent : intentList) {
                    if (findIntent.getId().equals(intent.getId())) {
                      findIntent.getIntentUtterList().add(intentUtter);
                      break;
                    }
                  }
                } catch (Exception ex) {
                  for (Intent findIntent : intentList) {
                    if (findIntent.getId().equals(intent.getId())) {
                      findIntent.getIntentUtterList().add(intentUtter);
                      break;
                    }
                  }
                }

                // Intent Utter Data
                if (field.length >= 4 && !field[3].trim().equals("")) {
                  String[] intentUtterDataList = field[3].split("\\|");
                  for (String strIntentUtterData : intentUtterDataList) {
                    List<IntentUtterData> filteredIntentUtterDataList = intentUtter
                        .getIntentUtterDataList().stream()
                        .filter(intentUtterData -> intentUtterData.getMappedValue()
                            .equals(strIntentUtterData))
                        .collect(Collectors.toList());

                    if (filteredIntentUtterDataList.size() == 0) {
                      IntentUtterData intentUtterData = new IntentUtterData();
                      intentUtterData.setId(UUID.randomUUID().toString());
                      intentUtterData.setEntityId(valueAndEntityIdMap.get(strIntentUtterData));
                      intentUtterData.setMappedValue(strIntentUtterData);
                      intentUtterData.setIntentUtter(intentUtter);

                      intentUtterData.setCreatorId(userId);
                      intentUtterData.setCreatedAt(currentTimestamp);
                      intentUtterData.setUpdaterId(userId);
                      intentUtterData.setUpdatedAt(currentTimestamp);

                      try {
                        intentUtterDataService.insertIntentUtterData(intentUtterData);

                        for (Intent findIntent : intentList) {
                          if (findIntent.getId().equals(intent.getId())) {
                            for (IntentUtter findIntentUtter : findIntent.getIntentUtterList()) {
                              if (findIntentUtter.getId().equals(intentUtter.getId())) {
                                findIntentUtter.getIntentUtterDataList().add(intentUtterData);
                                break;
                              }
                            }
                          }
                        }
                      } catch (Exception ex) {
                        intentUtterDataService.insertIntentUtterData(intentUtterData);

                        for (Intent findIntent : intentList) {
                          if (findIntent.getId().equals(intent.getId())) {
                            for (IntentUtter findIntentUtter : findIntent.getIntentUtterList()) {
                              if (findIntentUtter.getId().equals(intentUtter.getId())) {
                                findIntentUtter.getIntentUtterDataList().add(intentUtterData);
                                break;
                              }
                            }
                          }
                        }
                      }
                    }
                  }

                  successCount++;
                } else {
                  successCount++;
                  continue;
                }
              } else {
                successCount++;
                continue;
              }
            } else {
              failCount++;
              failDataList.add(line);
              continue;
            }
          } else {
            // Intent
            if (field.length >= 1 && !field[0].trim().equals("")) {
              Intent intent = filteredIntentList.get(0);

              // Intent Utter
              if (field.length >= 3 && !field[1].trim().equals("")) {
                List<IntentUtter> filteredIntentUtterList = intent.getIntentUtterList().stream()
                    .filter(intentUtter -> intentUtter.getUserSay().equals(field[1]))
                    .collect(Collectors.toList());

                IntentUtter intentUtter = null;
                if (filteredIntentUtterList.size() == 0) {
                  intentUtter = new IntentUtter();
                  intentUtter.setId(UUID.randomUUID().toString());
                  intentUtter.setUserSay(field[1]);

                  String mappedUserSay = field[2];
                  Pattern pattern = Pattern.compile("<(.*?)>");
                  Matcher matcher = pattern.matcher(mappedUserSay);

                  int notFouncEntityCount = 0;
                  while (matcher.find()) {
                    String[] mappedData = matcher.group(1).split("=");
                    String entityName = mappedData[0];
                    String value = mappedData[1];

                    boolean isFindEntity = false;
                    for (Entity entity : entityList) {
                      if (entity.getEntityName().equals(entityName)) {
//                                            entityNameAndEntityIdMap.put(entityName, entity.getId());
                        mappedUserSay = mappedUserSay
                            .replace(entityName + "=", entity.getId() + "=");
                        valueAndEntityIdMap.put(value, entity.getId());
                        isFindEntity = true;

                        break;
                      }
                    }

                    if (isFindEntity == false) {
                      notFouncEntityCount++;
                    }
                  }

                  if (notFouncEntityCount > 0) {
                    failCount++;
                    failDataList.add(line);
                    continue;
                  }

                  intentUtter.setMappedUserSay(mappedUserSay);
                  intentUtter.setIntent(intent);

                  intentUtter.setCreatorId(userId);
                  intentUtter.setCreatedAt(currentTimestamp);
                  intentUtter.setUpdaterId(userId);
                  intentUtter.setUpdatedAt(currentTimestamp);

                  try {
                    intentUtterService.insertIntentUtter(intentUtter);

                    for (Intent findIntent : intentList) {
                      if (findIntent.getId().equals(intent.getId())) {
                        findIntent.getIntentUtterList().add(intentUtter);
                        break;
                      }
                    }
                  } catch (Exception ex) {
                    for (Intent findIntent : intentList) {
                      if (findIntent.getId().equals(intent.getId())) {
                        findIntent.getIntentUtterList().add(intentUtter);
                        break;
                      }
                    }
                  }
                } else {
                  intentUtter = filteredIntentUtterList.get(0);
                }

                // Intent Utter Data
                if (field.length >= 4 && !field[3].trim().equals("")) {
                  String[] intentUtterDataList = field[3].split("\\|");
                  for (String strIntentUtterData : intentUtterDataList) {
                    List<IntentUtterData> filteredIntentUtterDataList = intentUtter
                        .getIntentUtterDataList().stream()
                        .filter(intentUtterData -> intentUtterData.getMappedValue()
                            .equals(strIntentUtterData))
                        .collect(Collectors.toList());

                    if (filteredIntentUtterDataList.size() == 0) {
                      IntentUtterData intentUtterData = new IntentUtterData();
                      intentUtterData.setId(UUID.randomUUID().toString());
                      intentUtterData.setEntityId(valueAndEntityIdMap.get(strIntentUtterData));
                      intentUtterData.setMappedValue(strIntentUtterData);
                      intentUtterData.setIntentUtter(intentUtter);

                      intentUtterData.setCreatorId(userId);
                      intentUtterData.setCreatedAt(currentTimestamp);
                      intentUtterData.setUpdaterId(userId);
                      intentUtterData.setUpdatedAt(currentTimestamp);

                      try {
                        intentUtterDataService.insertIntentUtterData(intentUtterData);

                        for (Intent findIntent : intentList) {
                          if (findIntent.getId().equals(intent.getId())) {
                            for (IntentUtter findIntentUtter : findIntent.getIntentUtterList()) {
                              if (findIntentUtter.getId().equals(intentUtter.getId())) {
                                findIntentUtter.getIntentUtterDataList().add(intentUtterData);
                                break;
                              }
                            }
                          }
                        }
                      } catch (Exception ex) {
                        intentUtterDataService.insertIntentUtterData(intentUtterData);

                        for (Intent findIntent : intentList) {
                          if (findIntent.getId().equals(intent.getId())) {
                            for (IntentUtter findIntentUtter : findIntent.getIntentUtterList()) {
                              if (findIntentUtter.getId().equals(intentUtter.getId())) {
                                findIntentUtter.getIntentUtterDataList().add(intentUtterData);
                                break;
                              }
                            }
                          }
                        }
                      }
                    }
                  }

                  successCount++;
                } else {
                  successCount++;
                  continue;
                }
              } else {
                successCount++;
                continue;
              }
            } else {
              failCount++;
              failDataList.add(line);
              continue;
            }
          }
        }
      }

      JSONObject data = new JSONObject();
      data.put("totalRowCount", rowCount - 1);
      data.put("successCount", successCount);
      data.put("failCount", failCount);
      data.put("failDataList", failDataList);

      result.put("data", data);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("POST /api/userid/{}/workspace/{}/entity/{}/intentupload", userId, workspaceId,
          modelId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    } finally {
      if (br != null) {
        try {
          br.close();
        } catch (IOException e) {
          logger
              .error("POST /api/userid/{}/workspace/{}/entity/{}/intentupload", userId, workspaceId,
                  modelId, e);
        }
      }
    }
  }
}
