package ai.maum.mlt.sds.model.service;

import ai.maum.mlt.sds.model.entity.Entity;
import ai.maum.mlt.sds.model.entity.EntityData;
import ai.maum.mlt.sds.model.entity.EntityDataSynonym;
import ai.maum.mlt.sds.model.entity.Event;
import ai.maum.mlt.sds.model.entity.EventCondition;
import ai.maum.mlt.sds.model.entity.EventUtter;
import ai.maum.mlt.sds.model.entity.EventUtterData;
import ai.maum.mlt.sds.model.entity.Intent;
import ai.maum.mlt.sds.model.entity.IntentUtter;
import ai.maum.mlt.sds.model.entity.IntentUtterData;
import ai.maum.mlt.sds.model.entity.Model;
import ai.maum.mlt.sds.model.entity.NextTask;
import ai.maum.mlt.sds.model.entity.StartEvent;
import ai.maum.mlt.sds.model.entity.StartEventUtter;
import ai.maum.mlt.sds.model.entity.StartEventUtterData;
import ai.maum.mlt.sds.model.entity.Task;
import com.google.protobuf.ByteString;
import com.google.protobuf.Empty;
import com.google.protobuf.util.JsonFormat;
import io.grpc.Channel;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import maum.brain.sds.train.SdsTrainerGrpc;
import maum.brain.sds.train.Sdstrainer;
import net.devh.springboot.autoconfigure.grpc.client.GrpcClient;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.CannotCreateTransactionException;

@Service
public class TrainerGrpcService {

  static final Logger logger = LoggerFactory.getLogger(TrainerGrpcService.class);

  @GrpcClient("sdstrain")
  private Channel trainServerChannel;

  @Autowired
  private ModelService modelService;

  public JSONObject trainerOpen(String modelId) {
    boolean succeeded = false;
    int tries = 0;

    try {
      Model model = null;

      tries = 0;
      while (!succeeded && tries < 3) {
        try {
          model = modelService.findModelById(modelId);
          succeeded = true;
        } catch (CannotCreateTransactionException ex) {
          logger.error(ex.toString());
        } finally {
          tries++;
        }
      }

      if (!succeeded) {
        JSONObject error = new JSONObject();
        error.put("error", "DB Connection Error.");

        return error;
      }

      if (model == null) {
        JSONObject error = new JSONObject();
        error.put("error", "Model is null.");

        return error;
      }

//            JSONParser jsonParser = new JSONParser();

      Sdstrainer.SdsModel.Builder sdsModel = Sdstrainer.SdsModel.newBuilder();

      sdsModel.setId(model.getId());
      sdsModel.setName(model.getModelName());

      // Entity
      List<Entity> entityList = model.getEntityList();
      for (Entity tempEntity : entityList) {
        Sdstrainer.Entity.Builder entity = Sdstrainer.Entity.newBuilder();

        entity.setId(tempEntity.getId());
        entity.setName(tempEntity.getEntityName());
        entity.setSource(Sdstrainer.Source.valueOf(tempEntity.getEntitySource().toUpperCase()));
        entity.setType(Sdstrainer.EntityType.valueOf(tempEntity.getEntityType().toUpperCase()));
        entity.setClassName(tempEntity.getClassName());
        entity.setClassSource(Sdstrainer.Source.valueOf(tempEntity.getClassSource()));

        for (EntityData tempEntityData : tempEntity.getEntityDataList()) {
          Sdstrainer.EntityData.Builder entityData = Sdstrainer.EntityData.newBuilder();

          entityData.setId(tempEntityData.getId());
          entityData.setEntityData(tempEntityData.getEntityData());

          for (EntityDataSynonym tempEntityDataSynonym : tempEntityData
              .getEntityDataSynonymList()) {
            Sdstrainer.SynonymData.Builder synonymData = Sdstrainer.SynonymData.newBuilder();

            synonymData.setId(tempEntityDataSynonym.getId());
            synonymData.setSynonym(tempEntityDataSynonym.getEntityDataSynonym());

            entityData.addSynonymDatas(synonymData);
          }

          entity.addEntityDatas(entityData);
        }

        sdsModel.addEntities(entity);
      }

      // Intent
      List<Intent> intentList = model.getIntentList();
      for (Intent tempIntent : intentList) {
        Sdstrainer.Intent.Builder intent = Sdstrainer.Intent.newBuilder();

        intent.setId(tempIntent.getId());
        intent.setName(tempIntent.getIntentName());
        intent.setIntentClass("default");

        for (IntentUtter tempIntentUtter : tempIntent.getIntentUtterList()) {
          Sdstrainer.IntentUtter.Builder intentUtter = Sdstrainer.IntentUtter.newBuilder();

          intentUtter.setId(tempIntentUtter.getId());
          intentUtter.setUserSay(tempIntentUtter.getUserSay());

          // mappedUserSay에 있는 id 값을 entityName으로 치환하여 set
          String mappedUserSay = tempIntentUtter.getMappedUserSay();
          for (Entity tempEntity : entityList) {
            mappedUserSay = mappedUserSay.replace(tempEntity.getId(), tempEntity.getEntityName());
          }
          intentUtter.setMappedUserSay(mappedUserSay);

          for (IntentUtterData tempIntentUtterData : tempIntentUtter.getIntentUtterDataList()) {
            Sdstrainer.IntentUtterData.Builder intentUtterData = Sdstrainer.IntentUtterData
                .newBuilder();

            intentUtterData.setId(tempIntentUtterData.getId());

            for (Sdstrainer.Entity findEntity : sdsModel.getEntitiesList()) {
              if (findEntity.getId().equals(tempIntentUtterData.getEntityId())) {
                intentUtterData.setMappedEntity(findEntity);
                break;
              }
            }

            intentUtterData.setMappedValue(tempIntentUtterData.getMappedValue());

            intentUtter.addIntentUtterDatas(intentUtterData);
          }

          intent.addIntentUtters(intentUtter);
        }

        sdsModel.addIntents(intent);
      }

      // Task
      List<Task> taskList = model.getTaskList();
      for (Task tempTask : taskList) {
        Sdstrainer.Task.Builder task = Sdstrainer.Task.newBuilder();

        task.setId(tempTask.getId());
        task.setName(tempTask.getTaskName());
        task.setGoal(tempTask.getTaskGoal().replace("'", "\""));
        task.setType(Sdstrainer.TaskType.valueOf(tempTask.getTaskType().toLowerCase()));
        if (tempTask.getResetSlot().equals("Y")) {
          task.setResetSlot(true);
        } else {
          task.setResetSlot(false);
        }

        // startEvent
        for (StartEvent tempStartEvent : tempTask.getStartEventList()) {
          Sdstrainer.StartEvent.Builder startEvent = Sdstrainer.StartEvent.newBuilder();

          startEvent.setId(tempStartEvent.getId());
          startEvent.setCondition(tempStartEvent.getStartEventCondition().replace("'", "\""));
          startEvent.setAction(tempStartEvent.getStartEventAction().replace("'", "\""));

          for (StartEventUtter tempStartEventUtter : tempStartEvent.getStartEventUtterList()) {
            Sdstrainer.Utter.Builder utter = Sdstrainer.Utter.newBuilder();

            utter.setId(tempStartEventUtter.getId());

            for (Sdstrainer.Intent findIntent : sdsModel.getIntentsList()) {
              if (findIntent.getId().equals(tempStartEventUtter.getIntentId())) {
                utter.setSystemIntent(findIntent);
                break;
              }
            }
            utter.setExtraData(tempStartEventUtter.getExtraData());

            for (StartEventUtterData tempStartEventUtterData : tempStartEventUtter
                .getStartEventUtterDataList()) {
              Sdstrainer.UtterData.Builder utterData = Sdstrainer.UtterData.newBuilder();

              utterData.setId(tempStartEventUtterData.getId());
              utterData.setUtter(tempStartEventUtterData.getStartEventUtterData());

              utter.addUtters(utterData);
            }

            startEvent.addUtters(utter);
          }

          task.addStartEvents(startEvent);
        }

        // event
        for (Event tempEvent : tempTask.getEventList()) {
          Sdstrainer.Event.Builder event = Sdstrainer.Event.newBuilder();

          event.setId(tempEvent.getId());

          for (Sdstrainer.Intent findIntent : sdsModel.getIntentsList()) {
            if (findIntent.getId().equals(tempEvent.getIntentId())) {
              event.setUserIntent(findIntent);
              break;
            }
          }

          for (EventCondition tempEventCondition : tempEvent.getEventConditionList()) {
            Sdstrainer.EventCondition.Builder eventCondition = Sdstrainer.EventCondition
                .newBuilder();

            eventCondition.setId(tempEventCondition.getId());
            eventCondition.setCondition(tempEventCondition.getEventCondition().replace("'", "\""));
            eventCondition.setAction(tempEventCondition.getEventAction().replace("'", "\""));

            for (EventUtter tempEventUtter : tempEventCondition.getEventUtterList()) {
              Sdstrainer.Utter.Builder utter = Sdstrainer.Utter.newBuilder();

              utter.setId(tempEventUtter.getId());

              for (Sdstrainer.Intent findIntent : sdsModel.getIntentsList()) {
                if (findIntent.getId().equals(tempEventUtter.getIntentId())) {
                  utter.setSystemIntent(findIntent);
                  break;
                }
              }
              utter.setExtraData(tempEventUtter.getExtraData());

              for (EventUtterData tempEventUtterData : tempEventUtter.getEventUtterDataList()) {
                Sdstrainer.UtterData.Builder utterData = Sdstrainer.UtterData.newBuilder();

                utterData.setId(tempEventUtterData.getId());
                utterData.setUtter(tempEventUtterData.getEventUtterData());

                utter.addUtters(utterData);
              }

              eventCondition.addUtters(utter);
            }

            event.addEventConditions(eventCondition);
          }

          task.addEvents(event);
        }

        // next task
        for (NextTask tempNextTask : tempTask.getNextTaskList()) {
          Sdstrainer.NextTask.Builder nextTask = Sdstrainer.NextTask.newBuilder();

          nextTask.setId(tempNextTask.getId());

          for (Task findTask : taskList) {
            if (findTask.getId().equals(tempNextTask.getNextTaskId())) {
              nextTask.setName(findTask.getTaskName());
              break;
            }
          }

          nextTask.setCondition(tempNextTask.getNextTaskCondition().replace("'", "\""));
          nextTask
              .setController(Sdstrainer.Source.valueOf(tempNextTask.getController().toUpperCase()));

          task.addNextTasks(nextTask);
        }

        sdsModel.addTasks(task);
      }

      SdsTrainerGrpc.SdsTrainerBlockingStub stub = SdsTrainerGrpc
          .newBlockingStub(trainServerChannel);
      Sdstrainer.SdsModel buildModel = sdsModel.build();
      Sdstrainer.SdsTrainKey sdsTrainKey = stub.open(buildModel);

      String jsonString = JsonFormat.printer().print(sdsTrainKey);
      JSONParser parser = new JSONParser();
      Object obj = parser.parse(jsonString);

      JSONObject result = new JSONObject();
      result.put("resultInfo", obj);

//            String jsonString = JsonFormat.printer().print(sdsModel);
//
//            JSONParser parser = new JSONParser();
//            Object obj = parser.parse(jsonString);
//
//            JSONObject trainKey = new JSONObject();
//            trainKey.put("train_id", obj);

      return result;
    } catch (Exception ex) {
      JSONObject error = new JSONObject();
      error.put("error", ex.toString());

      return error;
    }
  }

  public JSONObject trainerGetProgress(String strTrainKey) {
    try {
      Sdstrainer.SdsTrainKey.Builder trainKey = Sdstrainer.SdsTrainKey.newBuilder();
      trainKey.setTrainId(strTrainKey);

      SdsTrainerGrpc.SdsTrainerBlockingStub stub = SdsTrainerGrpc
          .newBlockingStub(trainServerChannel);
      Sdstrainer.SdsTrainKey buildTrainKey = trainKey.build();
      Sdstrainer.SdsTrainStatus sdsTrainStatus = stub.getProgress(buildTrainKey);

      String jsonString = JsonFormat.printer().print(sdsTrainStatus);
      JSONParser parser = new JSONParser();
      Object obj = parser.parse(jsonString);

      JSONObject result = new JSONObject();
      result.put("resultInfo", obj);

      return result;
    } catch (Exception ex) {
      JSONObject error = new JSONObject();
      error.put("error", ex.toString());

      return error;
    }
  }

  public JSONObject trainerGetBinary(String modelName, String strTrainKey) {
    FileOutputStream outputStream = null;

    try {
      Sdstrainer.SdsTrainKey.Builder trainKey = Sdstrainer.SdsTrainKey.newBuilder();
      trainKey.setTrainId(strTrainKey);

      SdsTrainerGrpc.SdsTrainerBlockingStub stub = SdsTrainerGrpc
          .newBlockingStub(trainServerChannel);
      Sdstrainer.SdsTrainKey buildTrainKey = trainKey.build();

      String fileName = "/home/minds/maum/trained/sds-entity/ki/" + modelName + ".tar.gz";
//            String fileName = "./" + modelName + ".tar.gz";
      outputStream = new FileOutputStream(fileName);
      Iterator<Sdstrainer.SdsTrainBinary> sdsTrainBinaryIterator = stub.getBinary(buildTrainKey);

      while (sdsTrainBinaryIterator.hasNext()) {
        ByteString.ByteIterator iterator = sdsTrainBinaryIterator.next().getTar().iterator();
        while (iterator.hasNext()) {
          outputStream.write(iterator.nextByte());
        }
      }

      JSONObject result = new JSONObject();
      result.put("resultInfo", "Success");

      return result;
    } catch (Exception ex) {
      JSONObject error = new JSONObject();
      error.put("error", ex.toString());

      return error;
    } finally {
      try {
        outputStream.close();
      } catch (IOException io) {

      }
    }
  }

  public JSONObject trainerClose(String strTrainKey) {
    try {
      Sdstrainer.SdsTrainKey.Builder trainKey = Sdstrainer.SdsTrainKey.newBuilder();
      trainKey.setTrainId(strTrainKey);

      SdsTrainerGrpc.SdsTrainerBlockingStub stub = SdsTrainerGrpc
          .newBlockingStub(trainServerChannel);
      Sdstrainer.SdsTrainKey buildTrainKey = trainKey.build();
      Sdstrainer.SdsTrainStatus sdsTrainStatus = stub.close(buildTrainKey);

      String jsonString = JsonFormat.printer().print(sdsTrainStatus);
      JSONParser parser = new JSONParser();
      Object obj = parser.parse(jsonString);

      JSONObject result = new JSONObject();
      result.put("resultInfo", obj);

      return result;
    } catch (Exception ex) {
      JSONObject error = new JSONObject();
      error.put("error", ex.toString());

      return error;
    }
  }

  public JSONObject trainerStop(String strTrainKey) {
    try {
      Sdstrainer.SdsTrainKey.Builder trainKey = Sdstrainer.SdsTrainKey.newBuilder();
      trainKey.setTrainId(strTrainKey);

      SdsTrainerGrpc.SdsTrainerBlockingStub stub = SdsTrainerGrpc
          .newBlockingStub(trainServerChannel);
      Sdstrainer.SdsTrainKey buildTrainKey = trainKey.build();
      Sdstrainer.SdsTrainStatus sdsTrainStatus = stub.stop(buildTrainKey);

      String jsonString = JsonFormat.printer().print(sdsTrainStatus);
      JSONParser parser = new JSONParser();
      Object obj = parser.parse(jsonString);

      JSONObject result = new JSONObject();
      result.put("resultInfo", obj);

      return result;
    } catch (Exception ex) {
      JSONObject error = new JSONObject();
      error.put("error", ex.toString());

      return error;
    }
  }

  public JSONObject trainerGetAllProgress() {
    try {
      SdsTrainerGrpc.SdsTrainerBlockingStub stub = SdsTrainerGrpc
          .newBlockingStub(trainServerChannel);
      Sdstrainer.SdsTrainStatusList sdsTrainStatusList = stub
          .getAllProgress(Empty.getDefaultInstance());

      String jsonString = JsonFormat.printer().print(sdsTrainStatusList);
      JSONParser parser = new JSONParser();
      Object obj = parser.parse(jsonString);

      JSONObject result = new JSONObject();

      if (jsonString.equals("{\n}")) {
        JSONArray emptyArray = new JSONArray();
        result.put("resultInfo", emptyArray);
      } else {
        result.put("resultInfo", obj);
      }

      return result;
    } catch (Exception ex) {
      JSONObject error = new JSONObject();
      error.put("error", ex.toString());

      return error;
    }
  }

  public JSONObject trainerRemoveBinary(String strTrainKey) {
    try {
      Sdstrainer.SdsTrainKey.Builder trainKey = Sdstrainer.SdsTrainKey.newBuilder();
      trainKey.setTrainId(strTrainKey);

      SdsTrainerGrpc.SdsTrainerBlockingStub stub = SdsTrainerGrpc
          .newBlockingStub(trainServerChannel);
      Sdstrainer.SdsTrainKey buildTrainKey = trainKey.build();
      Empty sdsTrainStatus = stub.removeBinary(buildTrainKey);

      String jsonString = JsonFormat.printer().print(sdsTrainStatus);
      JSONParser parser = new JSONParser();
      Object obj = parser.parse(jsonString);

      JSONObject result = new JSONObject();
      result.put("resultInfo", obj);

      return result;
    } catch (Exception ex) {
      JSONObject error = new JSONObject();
      error.put("error", ex.toString());

      return error;
    }
  }
}
