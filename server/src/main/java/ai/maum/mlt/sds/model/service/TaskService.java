package ai.maum.mlt.sds.model.service;

import ai.maum.mlt.sds.model.entity.Model;
import ai.maum.mlt.sds.model.entity.Task;
import java.util.List;
import javax.transaction.Transactional;

@Transactional
public interface TaskService {

  Task insertTask(Task task);

  Task findTaskById(String taskId);

  List<Task> findAll();

  List<Task> findTasksByModel(Model model);

  Task updateTask(Task task);

  void deleteTaskById(String taskId);
}
