package ai.maum.mlt.sds.model.service;

import ai.maum.mlt.sds.model.entity.Intent;
import ai.maum.mlt.sds.model.repository.IntentRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IntentServiceImpl implements IntentService {

  @Autowired
  private IntentRepository intentRepository;

  @Override
  public Intent insertIntent(Intent intent) {
    return intentRepository.save(intent);
  }

  @Override
  public List<Intent> findIntentsByWorkspaceId(String workspaceId) {
    return intentRepository.findIntentsByWorkspaceIdOrderByIntentName(workspaceId);
  }

  @Override
  public Intent findIntentByWorkspaceIdAndId(String workspaceId, String intentId) {
    return intentRepository.findIntentByWorkspaceIdAndId(workspaceId, intentId);
  }

  @Override
  public Intent updateIntent(Intent intent) {
    return intentRepository.save(intent);
  }

  @Override
  public void deleteIntentByWorkspaceIdAndId(String workspaceId, String intentId) {
    intentRepository.deleteIntentByWorkspaceIdAndId(workspaceId, intentId);

    return;
  }
}
