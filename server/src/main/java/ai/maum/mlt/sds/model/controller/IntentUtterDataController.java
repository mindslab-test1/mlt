package ai.maum.mlt.sds.model.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.sds.model.entity.IntentUtter;
import ai.maum.mlt.sds.model.entity.IntentUtterData;
import ai.maum.mlt.sds.model.service.IntentUtterDataService;
import ai.maum.mlt.sds.model.service.IntentUtterService;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/sds/model")
public class IntentUtterDataController {

  static final Logger logger = LoggerFactory.getLogger(IntentUtterDataController.class);

  @Autowired
  private IntentUtterService intentUtterService;

  @Autowired
  private IntentUtterDataService intentUtterDataService;

  @UriRoleDesc(role = "sds/model^C")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/intent/{intentId}/intentutter/{intentUtterId}/intentutterdata", method = RequestMethod.POST)
  public ResponseEntity<?> insertIntentUtterData(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String intentId,
      @PathVariable String intentUtterId, @RequestBody JSONObject bodyParam) {
    logger.info("POST /api/workspace/{}/entity/{}/intent/{}/intentutter/{}/intentutterdata",
        workspaceId, modelId, intentId, intentUtterId, bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

      String intentUtterDataId = null;
      String entityId = "";
      String mappedValue = "";
      String userId = null;

      if (bodyParam.get("id") == null || bodyParam.get("id").toString().trim().equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "id is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        intentUtterDataId = bodyParam.get("id").toString().trim();

        IntentUtterData findIntentUtterData = null;
        try {
          findIntentUtterData = intentUtterDataService.findIntentUtterDataById(intentUtterDataId);
        } catch (Exception ex) {
          findIntentUtterData = intentUtterDataService.findIntentUtterDataById(intentUtterDataId);
        }
        if (findIntentUtterData != null) {
          result.put("statusCode", 400);
          result.put("statusMessage", "id is already exist.");

          return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }
      }

      if (bodyParam.get("entityId") != null) {
        entityId = bodyParam.get("entityId").toString().trim();
      }

      if (bodyParam.get("mappedValue") != null) {
        mappedValue = bodyParam.get("mappedValue").toString().trim();
      }

      if (bodyParam.get("userId") == null || bodyParam.get("userId").toString().trim().equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "userId is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        userId = bodyParam.get("userId").toString().trim();
      }

      IntentUtter findIntentUtter = null;
      try {
        findIntentUtter = intentUtterService.findIntentUtterById(intentUtterId);
      } catch (Exception ex) {
        findIntentUtter = intentUtterService.findIntentUtterById(intentUtterId);
      }
      if (findIntentUtter == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "intentUtter doesn't exist.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      IntentUtterData intentUtterData = new IntentUtterData();
      intentUtterData.setId(intentUtterDataId);
      intentUtterData.setEntityId(entityId);
      intentUtterData.setMappedValue(mappedValue);
      intentUtterData.setIntentUtter(findIntentUtter);

      intentUtterData.setCreatorId(userId);
      intentUtterData.setUpdaterId(userId);
      intentUtterData.setCreatedAt(currentTimestamp);
      intentUtterData.setUpdatedAt(currentTimestamp);

      try {
        intentUtterDataService.insertIntentUtterData(intentUtterData);
      } catch (Exception ex) {
        intentUtterDataService.insertIntentUtterData(intentUtterData);
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("POST /api/workspace/{}/entity/{}/intent/{}/intentutter/{}/intentutterdata",
          workspaceId, modelId, intentId, intentUtterId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^R")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/intent/{intentId}/intentutter/{intentUtterId}/intentutterdata/list", method = RequestMethod.GET)
  public ResponseEntity<?> getIntentUtterDataListByIntentUtterId(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String intentId,
      @PathVariable String intentUtterId) {
    logger.info("GET /api/workspace/{}/entity/{}/intent/{}/intentutter/{}/intentutterdata/list",
        workspaceId, modelId, intentId, intentUtterId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      IntentUtter findIntentUtter = null;
      try {
        findIntentUtter = intentUtterService.findIntentUtterById(intentUtterId);
      } catch (Exception ex) {
        findIntentUtter = intentUtterService.findIntentUtterById(intentUtterId);
      }

      List<IntentUtterData> intentUtterDataList = new ArrayList<>();
      try {
        intentUtterDataList = intentUtterDataService
            .findIntentUtterDataByIntentUtter(findIntentUtter);
      } catch (Exception ex) {
        intentUtterDataList = intentUtterDataService
            .findIntentUtterDataByIntentUtter(findIntentUtter);
      }

      JSONObject wrappedIntentUtterDataInfo = new JSONObject();
      JSONArray intentUtterDataInfoList = new JSONArray();

      for (int i = 0; i < intentUtterDataList.size(); i++) {
        JSONObject intentUtterDataInfo = new JSONObject();

        intentUtterDataInfo.put("id", intentUtterDataList.get(i).getId());
        intentUtterDataInfo.put("entityId", intentUtterDataList.get(i).getEntityId());
        intentUtterDataInfo.put("mappedValue", intentUtterDataList.get(i).getMappedValue());
        intentUtterDataInfo
            .put("intentUtterId", intentUtterDataList.get(i).getIntentUtter().getId());

        intentUtterDataInfo.put("creatorId", intentUtterDataList.get(i).getCreatorId());
        intentUtterDataInfo.put("createdAt", intentUtterDataList.get(i).getCreatedAt());
        intentUtterDataInfo.put("updaterId", intentUtterDataList.get(i).getUpdaterId());
        intentUtterDataInfo.put("updatedAt", intentUtterDataList.get(i).getUpdatedAt());

        intentUtterDataInfoList.add(intentUtterDataInfo);
      }

      wrappedIntentUtterDataInfo.put("entityDataSynonymInfoList", intentUtterDataInfoList);

      result.put("data", wrappedIntentUtterDataInfo);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("GET /api/workspace/{}/entity/{}/intent/{}/intentutter/{}/intentutterdata/list",
          workspaceId, modelId, intentId, intentUtterId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^R")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/intent/{intentId}/intentutter/{intentUtterId}/intentutterdata/{intentUtterDataId}", method = RequestMethod.GET)
  public ResponseEntity<?> getIntentUtterDataByIntentUtterDataId(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String intentId,
      @PathVariable String intentUtterId, @PathVariable String intentUtterDataId) {
    logger.info("GET /api/workspace/{}/entity/{}/intent/{}/intentutter/{}/intentutterdata/{}",
        workspaceId, modelId, intentId, intentUtterId, intentUtterDataId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      IntentUtter intentUtter = null;
      try {
        intentUtter = intentUtterService.findIntentUtterById(intentUtterId);
      } catch (Exception ex) {
        intentUtter = intentUtterService.findIntentUtterById(intentUtterId);
      }
      if (intentUtter == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "intentUtter 가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      IntentUtterData intentUtterData = null;
      try {
        intentUtterData = intentUtterDataService.findIntentUtterDataById(intentUtterDataId);
      } catch (Exception ex) {
        intentUtterData = intentUtterDataService.findIntentUtterDataById(intentUtterDataId);
      }

      JSONObject wrappedIntentUtterDataInfo = new JSONObject();
      JSONObject intentUtterDataInfo = new JSONObject();

      intentUtterDataInfo.put("id", intentUtterData.getId());
      intentUtterDataInfo.put("entityId", intentUtterData.getEntityId());
      intentUtterDataInfo.put("mappedValue", intentUtterData.getMappedValue());
      intentUtterDataInfo.put("intentUtterId", intentUtterData.getIntentUtter().getId());

      intentUtterDataInfo.put("creatorId", intentUtterData.getCreatorId());
      intentUtterDataInfo.put("createdAt", intentUtterData.getCreatedAt());
      intentUtterDataInfo.put("updaterId", intentUtterData.getUpdaterId());
      intentUtterDataInfo.put("updatedAt", intentUtterData.getUpdatedAt());

      wrappedIntentUtterDataInfo.put("entityDataSynonymInfo", intentUtterDataInfo);

      result.put("data", wrappedIntentUtterDataInfo);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("GET /api/workspace/{}/entity/{}/intent/{}/intentutter/{}/intentutterdata/{}",
          workspaceId, modelId, intentId, intentUtterId, intentUtterDataId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^U")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/intent/{intentId}/intentutter/{intentUtterId}/intentutterdata/{intentUtterDataId}", method = RequestMethod.PUT)
  public ResponseEntity<?> updateIntentUtterData(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String intentId,
      @PathVariable String intentUtterId, @PathVariable String intentUtterDataId,
      @RequestBody JSONObject bodyParam) {
    logger.info("PUT /api/workspace/{}/entity/{}/intent/{}/intentutter/{}/intentutterdata/{}",
        workspaceId, modelId, intentId, intentUtterId, intentUtterDataId, bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

      String entityId = null;
      String mappedValue = null;
      String userId = null;

      if (bodyParam.get("entityId") != null) {
        entityId = bodyParam.get("entityId").toString().trim();
      }

      if (bodyParam.get("mappedValue") != null) {
        mappedValue = bodyParam.get("mappedValue").toString().trim();
      }

      if (bodyParam.get("userId") == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "userId 가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        userId = bodyParam.get("userId").toString().trim();
      }

      IntentUtterData findIntentUtterData = null;
      try {
        findIntentUtterData = intentUtterDataService.findIntentUtterDataById(intentUtterDataId);
      } catch (Exception ex) {
        findIntentUtterData = intentUtterDataService.findIntentUtterDataById(intentUtterDataId);
      }
      if (findIntentUtterData == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "intentUtterDataId 와 일치하는 intentUtterData 가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        if (entityId == null & mappedValue == null) {
          return new ResponseEntity<>(result, HttpStatus.OK);
        } else {
          if (entityId != null) {
            findIntentUtterData.setEntityId(entityId);
          }

          if (mappedValue != null) {
            findIntentUtterData.setMappedValue(mappedValue);
          }
        }

        findIntentUtterData.setUpdaterId(userId);
        findIntentUtterData.setUpdatedAt(currentTimestamp);
      }

      try {
        intentUtterDataService.updateIntentUtterData(findIntentUtterData);
      } catch (Exception ex) {
        intentUtterDataService.updateIntentUtterData(findIntentUtterData);
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("PUT /api/workspace/{}/entity/{}/intent/{}/intentutter/{}/intentutterdata/{}",
          workspaceId, modelId, intentId, intentUtterId, intentUtterDataId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^D")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/intent/{intentId}/intentutter/{intentUtterId}/intentutterdata/{intentUtterDataId}", method = RequestMethod.DELETE)
  public ResponseEntity<?> deleteIntentUtterDataById(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String intentId,
      @PathVariable String intentUtterId, @PathVariable String intentUtterDataId) {
    logger.info("DELETE /api/workspace/{}/entity/{}/intent/{}/intentutter/{}/intentutterdata/{}",
        workspaceId, modelId, intentId, intentUtterId, intentUtterDataId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      IntentUtterData findIntentUtterData = null;
      try {
        findIntentUtterData = intentUtterDataService.findIntentUtterDataById(intentUtterDataId);
      } catch (Exception ex) {
        findIntentUtterData = intentUtterDataService.findIntentUtterDataById(intentUtterDataId);
      }
      if (findIntentUtterData == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "intentUtterDataId 와 일치하는 intentUtterData 가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        try {
          intentUtterDataService.deleteIntentUtterDataById(intentUtterDataId);
        } catch (Exception ex) {
          intentUtterDataService.deleteIntentUtterDataById(intentUtterDataId);
        }
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("DELETE /api/workspace/{}/entity/{}/intent/{}/intentutter/{}/intentutterdata/{}",
          workspaceId, modelId, intentId, intentUtterId, intentUtterDataId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
