package ai.maum.mlt.sds.model.repository;

import ai.maum.mlt.sds.model.entity.Entity;
import ai.maum.mlt.sds.model.entity.EntityData;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface EntityDataRepository extends JpaRepository<EntityData, String> {

  EntityData findEntityDataById(String entityDataId);

  List<EntityData> findAll();

  List<EntityData> findEntityDataByEntity(Entity entity);

  void deleteEntityDataById(String entityDataId);
}
