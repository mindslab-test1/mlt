package ai.maum.mlt.sds.model.service;

import ai.maum.mlt.sds.model.entity.Entity;
import ai.maum.mlt.sds.model.entity.EntityData;
import java.util.List;
import javax.transaction.Transactional;

@Transactional
public interface EntityDataService {

  EntityData insertEntityData(EntityData entityData);

  EntityData findEntityDataById(String entityDataId);

  List<EntityData> findAll();

  List<EntityData> findEntityDataByEntity(Entity entity);

  EntityData updateEntityData(EntityData entityData);

  void deleteEntityDataById(String entityDataId);
}
