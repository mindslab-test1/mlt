package ai.maum.mlt.sds.scenario.service;

import ai.maum.mlt.sds.scenario.entity.ScenarioEntity;
import ai.maum.mlt.sds.scenario.repository.ScenarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class ScenarioServiceImpl implements ScenarioService {

  @Autowired
  private ScenarioRepository scenarioRepository;

  @Override
  public ScenarioEntity updateSkill(String workspaceId, ScenarioEntity scenarioEntity) {
    return scenarioRepository.save(scenarioEntity);
  }

  @Override
  public ScenarioEntity getSkill(String id) { return this.scenarioRepository.findById(id).get(); }

  @Override
  public Page<ScenarioEntity> getAllSkills(ScenarioEntity scenarioEntity) {
    return this.scenarioRepository.findAllByWorkspaceId(scenarioEntity.getWorkspaceId(),
        scenarioEntity.getSkillId(), scenarioEntity.getSkillName(), scenarioEntity.getGroup1(),
        scenarioEntity.getGroup2(), scenarioEntity.getGroup3(), scenarioEntity.getGroup4(),
        scenarioEntity.getStatus(), scenarioEntity.getUpdaterId(),
        scenarioEntity.getSearchOption(),  scenarioEntity.getPageRequest());
  }

  @Override
  public Page<ScenarioEntity> getSubSkills(ScenarioEntity scenarioEntity, List<String> skillList) {
    return this.scenarioRepository.findAllByWorkspaceId(scenarioEntity.getWorkspaceId(),
            skillList, scenarioEntity.getPageRequest());
  }

  @Override
  public ScenarioEntity getImportSkills(String workspaceId, String id) {
    return this.scenarioRepository.findAllByWorkspaceIdAndId(workspaceId, id);
  }

  @Override
  public void deleteCheckedSkill(String id) {
    this.scenarioRepository.deleteById(id);
  }
}