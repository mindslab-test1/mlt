package ai.maum.mlt.sds.model.repository;

import ai.maum.mlt.sds.model.entity.NextTask;
import ai.maum.mlt.sds.model.entity.Task;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface NextTaskRepository extends JpaRepository<NextTask, String> {

  NextTask findNextTaskById(String id);

  List<NextTask> findAll();

  List<NextTask> findNextTasksByTask(Task task);

  void deleteNextTaskById(String id);
}
