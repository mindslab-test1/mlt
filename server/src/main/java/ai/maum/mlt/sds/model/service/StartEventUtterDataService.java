package ai.maum.mlt.sds.model.service;

import ai.maum.mlt.sds.model.entity.StartEventUtter;
import ai.maum.mlt.sds.model.entity.StartEventUtterData;
import java.util.List;
import javax.transaction.Transactional;

@Transactional
public interface StartEventUtterDataService {

  StartEventUtterData insertStartEventUtterData(StartEventUtterData startEventUtterData);

  StartEventUtterData findStartEventUtterDataById(String startEventUtterDataId);

  List<StartEventUtterData> findAll();

  List<StartEventUtterData> findStartEventUtterDataByStartEventUtter(
      StartEventUtter startEventUtter);

  StartEventUtterData updateStartEventUtterData(StartEventUtterData startEventUtterData);

  void deleteStartEventUtterDataById(String startEventUtterDataId);
}
