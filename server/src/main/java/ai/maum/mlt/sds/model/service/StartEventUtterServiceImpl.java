package ai.maum.mlt.sds.model.service;

import ai.maum.mlt.sds.model.entity.StartEvent;
import ai.maum.mlt.sds.model.entity.StartEventUtter;
import ai.maum.mlt.sds.model.repository.StartEventUtterRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StartEventUtterServiceImpl implements StartEventUtterService {

  @Autowired
  private StartEventUtterRepository startEventUtterRepository;

  @Override
  public StartEventUtter insertStartEventUtter(StartEventUtter startEventUtter) {
    return startEventUtterRepository.save(startEventUtter);
  }

  @Override
  public StartEventUtter findStartEventUtterById(String startEventUtterId) {
    return startEventUtterRepository.findStartEventUtterById(startEventUtterId);
  }

  @Override
  public List<StartEventUtter> findAll() {
    return startEventUtterRepository.findAll();
  }

  @Override
  public List<StartEventUtter> findStartEventUttersByStartEvent(StartEvent startEvent) {
    return startEventUtterRepository.findStartEventUttersByStartEvent(startEvent);
  }

  @Override
  public StartEventUtter updateStartEventUtter(StartEventUtter startEventUtter) {
    return startEventUtterRepository.save(startEventUtter);
  }

  @Override
  public void deleteStartEventUtterById(String startEventUtterId) {
    startEventUtterRepository.deleteStartEventUtterById(startEventUtterId);

    return;
  }
}
