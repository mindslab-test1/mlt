package ai.maum.mlt.sds.model.repository;

import ai.maum.mlt.sds.model.entity.Model;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface ModelRepository extends JpaRepository<Model, String> {

  List<Model> findModelsByWorkspaceIdOrderByModelName(String workspaceId);

  Model findModelByWorkspaceIdAndCreatorIdAndModelName(String workspaceId, String creatorId,
      String modelName);

  Model findModelByWorkspaceIdAndId(String workspaceId, String modelId);

  Model findModelById(String modelId);

  void deleteModelByWorkspaceIdAndId(String workspaceId, String modelId);
}
