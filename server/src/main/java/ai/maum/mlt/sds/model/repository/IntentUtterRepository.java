package ai.maum.mlt.sds.model.repository;

import ai.maum.mlt.sds.model.entity.Intent;
import ai.maum.mlt.sds.model.entity.IntentUtter;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface IntentUtterRepository extends JpaRepository<IntentUtter, String> {

  IntentUtter findIntentUtterById(String intentUtterId);

  List<IntentUtter> findAll();

  List<IntentUtter> findIntentUttersByIntent(Intent intent);

  void deleteIntentUtterById(String intentUtterId);
}
