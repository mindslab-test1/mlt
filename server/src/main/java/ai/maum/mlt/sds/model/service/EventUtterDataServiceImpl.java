package ai.maum.mlt.sds.model.service;

import ai.maum.mlt.sds.model.entity.EventUtter;
import ai.maum.mlt.sds.model.entity.EventUtterData;
import ai.maum.mlt.sds.model.repository.EventUtterDataRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EventUtterDataServiceImpl implements EventUtterDataService {

  @Autowired
  private EventUtterDataRepository eventUtterDataRepository;

  @Override
  public EventUtterData insertEventUtterData(EventUtterData eventUtterData) {
    return eventUtterDataRepository.save(eventUtterData);
  }

  @Override
  public EventUtterData findEventUtterDataById(String eventUtterDataId) {
    return eventUtterDataRepository.findEventUtterDataById(eventUtterDataId);
  }

  @Override
  public List<EventUtterData> findAll() {
    return eventUtterDataRepository.findAll();
  }

  @Override
  public List<EventUtterData> findEventUtterDataByEventUtter(EventUtter eventUtter) {
    return eventUtterDataRepository.findEventUtterDataByEventUtter(eventUtter);
  }

  @Override
  public EventUtterData updateEventUtterData(EventUtterData eventUtterData) {
    return eventUtterDataRepository.save(eventUtterData);
  }

  @Override
  public void deleteEventUtterDataById(String eventUtterDataId) {
    eventUtterDataRepository.deleteEventUtterDataById(eventUtterDataId);

    return;
  }
}
