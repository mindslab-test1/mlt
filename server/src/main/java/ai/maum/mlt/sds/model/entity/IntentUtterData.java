package ai.maum.mlt.sds.model.entity;

import ai.maum.mlt.common.auth.entity.UserEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import java.io.Serializable;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

@javax.persistence.Entity
@Table(name = "MAI_SDS_INTENT_UTTER_DATA")
@EqualsAndHashCode(of = "id")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class IntentUtterData implements Serializable {

  @Id
  @Column(name = "ID", length = 40, nullable = false)
  private String id;

  @Column(name = "ENTITY_ID", length = 40, nullable = true)
  private String entityId;

  @Column(name = "MAPPED_VALUE", length = 255, nullable = true)
  private String mappedValue;

  @JsonBackReference
  @ManyToOne(targetEntity = IntentUtter.class, fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "INTENT_UTTER_ID", nullable = false)
  private IntentUtter intentUtter;

  @Column(name = "CREATOR_ID", length = 40, nullable = false)
  private String creatorId;

  @CreatedDate
  @Column(name = "CREATED_AT", nullable = false)
  private Timestamp createdAt;

  @Column(name = "UPDATER_ID", length = 40, nullable = false)
  private String updaterId;

  @LastModifiedDate
  @Column(name = "UPDATED_AT", nullable = false)
  private Timestamp updatedAt;

  @ManyToOne
  @JoinColumn(name = "CREATOR_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity createUserEntity;

  @ManyToOne
  @JoinColumn(name = "UPDATER_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity updateUserEntity;

  public String getCreatedAt() {
    if (createdAt != null) {
      Date date = new Date(createdAt.getTime());
      DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

      return dateFormat.format(date);
    } else {
      return null;
    }
  }

  public String getUpdatedAt() {
    if (updatedAt != null) {
      Date date = new Date(updatedAt.getTime());
      DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

      return dateFormat.format(date);
    } else {
      return null;
    }
  }
}
