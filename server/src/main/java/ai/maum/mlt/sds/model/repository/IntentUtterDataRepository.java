package ai.maum.mlt.sds.model.repository;

import ai.maum.mlt.sds.model.entity.IntentUtter;
import ai.maum.mlt.sds.model.entity.IntentUtterData;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface IntentUtterDataRepository extends JpaRepository<IntentUtterData, String> {

  IntentUtterData findIntentUtterDataById(String intentUtterDataId);

  List<IntentUtterData> findAll();

  List<IntentUtterData> findIntentUtterDataByIntentUtter(IntentUtter intentUtter);

  void deleteIntentUtterDataById(String intentUtterDataId);
}
