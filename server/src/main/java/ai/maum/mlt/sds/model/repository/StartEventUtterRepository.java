package ai.maum.mlt.sds.model.repository;

import ai.maum.mlt.sds.model.entity.StartEvent;
import ai.maum.mlt.sds.model.entity.StartEventUtter;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface StartEventUtterRepository extends JpaRepository<StartEventUtter, String> {

  StartEventUtter findStartEventUtterById(String startEventUtterId);

  List<StartEventUtter> findAll();

  List<StartEventUtter> findStartEventUttersByStartEvent(StartEvent startEvent);

  void deleteStartEventUtterById(String startEventUtterId);
}
