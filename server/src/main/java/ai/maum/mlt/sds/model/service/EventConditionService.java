package ai.maum.mlt.sds.model.service;

import ai.maum.mlt.sds.model.entity.Event;
import ai.maum.mlt.sds.model.entity.EventCondition;
import java.util.List;
import javax.transaction.Transactional;

@Transactional
public interface EventConditionService {

  EventCondition insertEventCondition(EventCondition eventCondition);

  EventCondition findEventConditionById(String eventConditionId);

  List<EventCondition> findAll();

  List<EventCondition> findEventConditionsByEvent(Event event);

  EventCondition updateEventCondition(EventCondition eventCondition);

  void deleteEventConditionById(String eventConditionId);
}
