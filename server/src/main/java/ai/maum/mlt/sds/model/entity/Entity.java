package ai.maum.mlt.sds.model.entity;

import ai.maum.mlt.common.auth.entity.UserEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.io.Serializable;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

@javax.persistence.Entity
@Table(name = "MAI_SDS_ENTITY")
@EqualsAndHashCode(of = "id")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Entity implements Serializable {

  @Id
  @Column(name = "ID", length = 40, nullable = false)
  private String id;

  @Column(name = "CLASS_NAME", length = 255, nullable = false)
  private String className;

  @Column(name = "CLASS_SOURCE", length = 40, nullable = false)
  private String classSource;

  @Column(name = "ENTITY_NAME", length = 255, nullable = false)
  private String entityName;

  @Column(name = "ENTITY_SOURCE", length = 40, nullable = false)
  private String entitySource;

  @Column(name = "ENTITY_TYPE", length = 40, nullable = false)
  private String entityType;

  @Column(name = "SHARED_YN", length = 1, nullable = false)
  private String sharedYn;

  @Column(name = "CREATOR_ID", length = 40, nullable = false)
  private String creatorId;

  @CreatedDate
  @Column(name = "CREATED_AT", nullable = false)
  private Timestamp createdAt;

  @Column(name = "UPDATER_ID", length = 40, nullable = false)
  private String updaterId;

  @LastModifiedDate
  @Column(name = "UPDATED_AT", nullable = false)
  private Timestamp updatedAt;

  @ManyToOne
  @JoinColumn(name = "CREATOR_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity createUserEntity;

  @ManyToOne
  @JoinColumn(name = "UPDATER_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity updateUserEntity;

  @JsonIgnore
  @JsonManagedReference
  @OneToMany(mappedBy = "entity", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, orphanRemoval = true)
  private List<EntityData> entityDataList = new ArrayList<>();

  public String getCreatedAt() {
    if (createdAt != null) {
      Date date = new Date(createdAt.getTime());
      DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

      return dateFormat.format(date);
    } else {
      return null;
    }
  }

  public String getUpdatedAt() {
    if (updatedAt != null) {
      Date date = new Date(updatedAt.getTime());
      DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

      return dateFormat.format(date);
    } else {
      return null;
    }
  }

  @Column(name = "workspaceId", length = 40, nullable = false)
  private String workspaceId;

  @ManyToMany(cascade = CascadeType.REMOVE, mappedBy = "entityList")
  private List<Model> modelList = new ArrayList<>();

  public List<Model> getModelList() {
    return modelList;
  }

  public void setModelList(List<Model> modelList) {
    this.modelList = modelList;
  }

  public void addModel(Model model) {
    this.modelList.add(model);
  }
}
