package ai.maum.mlt.sds.model.service;

import ai.maum.mlt.sds.model.entity.EventCondition;
import ai.maum.mlt.sds.model.entity.EventUtter;
import ai.maum.mlt.sds.model.repository.EventUtterRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EventUtterServiceImpl implements EventUtterService {

  @Autowired
  private EventUtterRepository eventUtterRepository;

  @Override
  public EventUtter insertEventUtter(EventUtter eventUtter) {
    return eventUtterRepository.save(eventUtter);
  }

  @Override
  public EventUtter findEventUtterById(String eventUtterId) {
    return eventUtterRepository.findEventUtterById(eventUtterId);
  }

  @Override
  public List<EventUtter> findAll() {
    return eventUtterRepository.findAll();
  }

  @Override
  public List<EventUtter> findEventUttersByEventCondition(EventCondition eventCondition) {
    return eventUtterRepository.findEventUttersByEventCondition(eventCondition);
  }

  @Override
  public EventUtter updateEventUtter(EventUtter eventUtter) {
    return eventUtterRepository.save(eventUtter);
  }

  @Override
  public void deleteEventUtterById(String eventUtterId) {
    eventUtterRepository.deleteEventUtterById(eventUtterId);
  }
}
