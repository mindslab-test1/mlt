package ai.maum.mlt.sds.model.service;

import ai.maum.mlt.sds.model.entity.Entity;
import ai.maum.mlt.sds.model.repository.EntityRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EntityServiceImpl implements EntityService {

  @Autowired
  private EntityRepository entityRepository;

  @Override
  public Entity insertEntity(Entity entity) {
    return entityRepository.save(entity);
  }

  @Override
  public List<Entity> findEntitiesByWorkspaceId(String workspaceId) {
    return entityRepository.findEntitiesByWorkspaceId(workspaceId);
  }

  @Override
  public Entity findEntityByWorkspaceIdAndId(String workspaceId, String entityId) {
    return entityRepository.findEntityByWorkspaceIdAndId(workspaceId, entityId);
  }

  @Override
  public Entity updateEntity(Entity entity) {
    return entityRepository.save(entity);
  }

  @Override
  public void deleteEntityByWorkspaceIdAndId(String workspaceId, String entityId) {
    entityRepository.deleteEntityByWorkspaceIdAndId(workspaceId, entityId);

    return;
  }

}
