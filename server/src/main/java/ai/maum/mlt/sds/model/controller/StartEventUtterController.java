package ai.maum.mlt.sds.model.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.sds.model.entity.StartEvent;
import ai.maum.mlt.sds.model.entity.StartEventUtter;
import ai.maum.mlt.sds.model.service.StartEventService;
import ai.maum.mlt.sds.model.service.StartEventUtterService;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/sds/model")
public class StartEventUtterController {

  static final Logger logger = LoggerFactory.getLogger(StartEventUtterController.class);

  @Autowired
  private StartEventService startEventService;

  @Autowired
  private StartEventUtterService startEventUtterService;

  @UriRoleDesc(role = "sds/model^C")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}/startevent/{startEventId}/starteventutter", method = RequestMethod.POST)
  public ResponseEntity<?> insertStartEventUtter(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String taskId, @PathVariable String startEventId,
      @RequestBody JSONObject bodyParam) {
    logger
        .info("POST /api/workspace/{}/entity/{}/task/{}/startevent/{}/starteventutter", workspaceId,
            modelId, taskId, startEventId, bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      String startEventUtterId = null;
      String intentId = "";
      String extraData = "";
      String userId = null;

      Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

      if (bodyParam.get("id") == null || bodyParam.get("id").toString().trim().equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "id is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        startEventUtterId = bodyParam.get("id").toString().trim();

        StartEventUtter findStartEventUtter = null;
        try {
          findStartEventUtter = startEventUtterService.findStartEventUtterById(startEventUtterId);
        } catch (Exception ex) {
          findStartEventUtter = startEventUtterService.findStartEventUtterById(startEventUtterId);
        }
        if (findStartEventUtter != null) {
          result.put("statusCode", 400);
          result.put("statusMessage", "id is already exist.");

          return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }
      }

      if (bodyParam.get("intentId") != null) {
        intentId = bodyParam.get("intentId").toString().trim();
      }

      if (bodyParam.get("extraData") != null) {
        extraData = bodyParam.get("extraData").toString().trim();
      }

      if (bodyParam.get("userId") == null || bodyParam.get("userId").toString().trim().equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "userId is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        userId = bodyParam.get("userId").toString().trim();
      }

      StartEvent findStartEvent = null;
      try {
        findStartEvent = startEventService.findStartEventById(startEventId);
      } catch (Exception ex) {
        findStartEvent = startEventService.findStartEventById(startEventId);
      }
      if (findStartEvent == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "startEvent doesn't exist.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      StartEventUtter startEventUtter = new StartEventUtter();
      startEventUtter.setId(startEventUtterId);
      startEventUtter.setIntentId(intentId);
      startEventUtter.setExtraData(extraData);
      startEventUtter.setStartEvent(findStartEvent);

      startEventUtter.setCreatorId(userId);
      startEventUtter.setCreatedAt(currentTimestamp);
      startEventUtter.setUpdaterId(userId);
      startEventUtter.setUpdatedAt(currentTimestamp);

      try {
        startEventUtterService.insertStartEventUtter(startEventUtter);
      } catch (Exception ex) {
        startEventUtterService.insertStartEventUtter(startEventUtter);
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("POST /api/workspace/{}/entity/{}/task/{}/startevent/{}/starteventutter",
          workspaceId, modelId, taskId, startEventId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^R")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}/startevent/{startEventId}/starteventutter/list", method = RequestMethod.GET)
  public ResponseEntity<?> getStartEventUtterListByStartEventId(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String taskId,
      @PathVariable String startEventId) {
    logger.info("GET /api/workspace/{}/entity/{}/task/{}/startevent/{}/starteventutter/list",
        workspaceId, modelId, taskId, startEventId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      StartEvent findStartEvent = null;
      try {
        findStartEvent = startEventService.findStartEventById(startEventId);
      } catch (Exception ex) {
        findStartEvent = startEventService.findStartEventById(startEventId);
      }
      if (findStartEvent == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "startEvent 데이터가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      List<StartEventUtter> startEventUtterList = new ArrayList<>();
      try {
        startEventUtterList = startEventUtterService
            .findStartEventUttersByStartEvent(findStartEvent);
      } catch (Exception ex) {
        startEventUtterList = startEventUtterService
            .findStartEventUttersByStartEvent(findStartEvent);
      }

      JSONObject wrappedStartEventUtterInfo = new JSONObject();
      JSONArray startEventUtterInfoList = new JSONArray();

      for (int i = 0; i < startEventUtterList.size(); i++) {
        JSONObject startEventUtterInfo = new JSONObject();

        startEventUtterInfo.put("id", startEventUtterList.get(i).getId());
        startEventUtterInfo.put("intentId", startEventUtterList.get(i).getIntentId());
        startEventUtterInfo.put("extraData", startEventUtterList.get(i).getExtraData());
        startEventUtterInfo.put("startEventId", startEventUtterList.get(i).getStartEvent().getId());

        startEventUtterInfo.put("creatorId", startEventUtterList.get(i).getCreatorId());
        startEventUtterInfo.put("createdAt", startEventUtterList.get(i).getCreatedAt());
        startEventUtterInfo.put("updaterId", startEventUtterList.get(i).getUpdaterId());
        startEventUtterInfo.put("updatedAt", startEventUtterList.get(i).getUpdatedAt());

        startEventUtterInfoList.add(startEventUtterInfo);
      }

      wrappedStartEventUtterInfo.put("startEventUtterInfoList", startEventUtterInfoList);

      result.put("data", wrappedStartEventUtterInfo);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("GET /api/workspace/{}/entity/{}/task/{}/startevent/{}/starteventutter/list",
          workspaceId, modelId, taskId, startEventId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^R")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}/startevent/{startEventId}/starteventutter/{startEventUtterId}", method = RequestMethod.GET)
  public ResponseEntity<?> getStartEventUtterById(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String taskId, @PathVariable String startEventId,
      @PathVariable String startEventUtterId) {
    logger.info("GET /api/workspace/{}/entity/{}/task/{}/startevent/{}/starteventutter/{}",
        workspaceId, modelId, taskId, startEventId, startEventUtterId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      StartEventUtter startEventUtter = null;
      try {
        startEventUtter = startEventUtterService.findStartEventUtterById(startEventUtterId);
      } catch (Exception ex) {
        startEventUtter = startEventUtterService.findStartEventUtterById(startEventUtterId);
      }

      JSONObject wrappedStartEventUtterInfo = new JSONObject();
      JSONObject startEventUtterInfo = new JSONObject();

      startEventUtterInfo.put("id", startEventUtter.getId());
      startEventUtterInfo.put("intentId", startEventUtter.getIntentId());
      startEventUtterInfo.put("extraData", startEventUtter.getExtraData());
      startEventUtterInfo.put("startEventId", startEventUtter.getStartEvent().getId());

      startEventUtterInfo.put("creatorId", startEventUtter.getCreatorId());
      startEventUtterInfo.put("createdAt", startEventUtter.getCreatedAt());
      startEventUtterInfo.put("updaterId", startEventUtter.getUpdaterId());
      startEventUtterInfo.put("updatedAt", startEventUtter.getUpdatedAt());

      wrappedStartEventUtterInfo.put("startEventUtterInfo", startEventUtterInfo);

      result.put("data", wrappedStartEventUtterInfo);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("GET /api/workspace/{}/entity/{}/task/{}/startevent/{}/starteventutter/{}",
          workspaceId, modelId, taskId, startEventId, startEventUtterId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^U")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}/startevent/{startEventId}/starteventutter/{startEventUtterId}", method = RequestMethod.PUT)
  public ResponseEntity<?> updateStartEventUtter(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String taskId, @PathVariable String startEventId,
      @PathVariable String startEventUtterId, @RequestBody JSONObject bodyParam) {
    logger.info("PUT /api/workspace/{}/entity/{}/task/{}/startevent/{}/starteventutter/{}",
        workspaceId, modelId, taskId, startEventId, startEventUtterId, bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

      String intentId = null;
      String extraData = null;
      String userId = null;

      if (bodyParam.get("intentId") != null) {
        intentId = bodyParam.get("intentId").toString().trim();
      }

      if (bodyParam.get("extraData") != null) {
        extraData = bodyParam.get("extraData").toString().trim();
      }

      if (bodyParam.get("userId") == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "userId 가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        userId = bodyParam.get("userId").toString().trim();
      }

      StartEventUtter findStartEventUtter = null;
      try {
        findStartEventUtter = startEventUtterService.findStartEventUtterById(startEventUtterId);
      } catch (Exception ex) {
        findStartEventUtter = startEventUtterService.findStartEventUtterById(startEventUtterId);
      }
      if (findStartEventUtter == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "startEventUtterId 와 일치하는 startEventUtter 가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        if (intentId != null) {
          findStartEventUtter.setIntentId(intentId);
        }

        if (extraData != null) {
          findStartEventUtter.setExtraData(extraData);
        }

        findStartEventUtter.setUpdaterId(userId);
        findStartEventUtter.setUpdatedAt(currentTimestamp);
      }

      try {
        startEventUtterService.updateStartEventUtter(findStartEventUtter);
      } catch (Exception ex) {
        startEventUtterService.updateStartEventUtter(findStartEventUtter);
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("PUT /api/workspace/{}/entity/{}/task/{}/startevent/{}/starteventutter/{}",
          workspaceId, modelId, taskId, startEventId, startEventUtterId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^D")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}/startevent/{startEventId}/starteventutter/{startEventUtterId}", method = RequestMethod.DELETE)
  public ResponseEntity<?> deleteStartEventUtterById(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String taskId, @PathVariable String startEventId,
      @PathVariable String startEventUtterId) {
    logger.info("DELETE /api/workspace/{}/entity/{}/task/{}/startevent/{}/starteventutter/{}",
        workspaceId, modelId, taskId, startEventId, startEventUtterId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      StartEventUtter findStartEventUtter = null;
      try {
        findStartEventUtter = startEventUtterService.findStartEventUtterById(startEventUtterId);
      } catch (Exception ex) {
        findStartEventUtter = startEventUtterService.findStartEventUtterById(startEventUtterId);
      }
      if (findStartEventUtter == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "startEventUtterId 와 일치하는 startEventUtter 가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        try {
          startEventUtterService.deleteStartEventUtterById(startEventUtterId);
        } catch (Exception ex) {
          startEventUtterService.deleteStartEventUtterById(startEventUtterId);
        }
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("DELETE /api/workspace/{}/entity/{}/task/{}/startevent/{}/starteventutter/{}",
          workspaceId, modelId, taskId, startEventId, startEventUtterId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
