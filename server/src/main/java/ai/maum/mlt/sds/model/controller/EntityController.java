package ai.maum.mlt.sds.model.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.sds.model.entity.Entity;
import ai.maum.mlt.sds.model.entity.EntityData;
import ai.maum.mlt.sds.model.entity.EntityDataSynonym;
import ai.maum.mlt.sds.model.entity.Model;
import ai.maum.mlt.sds.model.service.EntityDataService;
import ai.maum.mlt.sds.model.service.EntityDataSynonymService;
import ai.maum.mlt.sds.model.service.EntityService;
import ai.maum.mlt.sds.model.service.ModelService;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/sds/model")
public class EntityController {

  static final Logger logger = LoggerFactory.getLogger(EntityController.class);

  @Autowired
  private ModelService modelService;

  @Autowired
  private EntityService entityService;

  @Autowired
  private EntityDataService entityDataService;

  @Autowired
  private EntityDataSynonymService entityDataSynonymService;

  @UriRoleDesc(role = "sds/model^C")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/entity", method = RequestMethod.POST)
  public ResponseEntity<?> insertEntity(@PathVariable String workspaceId,
      @PathVariable String modelId, @RequestBody JSONObject bodyParam) {
    logger.info("POST /api/workspace/{}/entity/{}/entity", workspaceId, modelId, bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      String entityId = null;
      String className = null;
      String classSource = "SYSTEM";
      String entityName = null;
      String entitySource = "KB";
      String entityType = null;
      String sharedYn = null;
      String userId = null;

      Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

      if (bodyParam.get("id") == null || bodyParam.get("id").toString().trim().equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "id is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        entityId = bodyParam.get("id").toString().trim();

        Entity findEntity = null;
        try {
          findEntity = entityService.findEntityByWorkspaceIdAndId(workspaceId, entityId);
        } catch (Exception ex) {
          findEntity = entityService.findEntityByWorkspaceIdAndId(workspaceId, entityId);
        }
        if (findEntity != null) {
          result.put("statusCode", 400);
          result.put("statusMessage", "id is already exist.");

          return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }
      }

      if (bodyParam.get("className") == null || bodyParam.get("className").toString().trim()
          .equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "className is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        className = bodyParam.get("className").toString().trim();
      }

      if (bodyParam.get("entityName") == null || bodyParam.get("entityName").toString().trim()
          .equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "entityName is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        entityName = bodyParam.get("entityName").toString().trim();
      }

      if (bodyParam.get("entityType") == null || bodyParam.get("entityType").toString().trim()
          .equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "entityType is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        entityType = bodyParam.get("entityType").toString().trim();
      }

      if (bodyParam.get("sharedYn") == null || bodyParam.get("sharedYn").toString().trim()
          .equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "sharedYn is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        sharedYn = bodyParam.get("sharedYn").toString().trim();
      }

      if (bodyParam.get("userId") == null || bodyParam.get("userId").toString().trim().equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "userId is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        userId = bodyParam.get("userId").toString().trim();
      }

      Model findModel = null;
      try {
        findModel = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
      } catch (Exception ex) {
        findModel = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
      }
      if (findModel == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "Model doesn't exist.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      List<Entity> entityList = new ArrayList<>();
      try {
        entityList = entityService.findEntitiesByWorkspaceId(workspaceId);
      } catch (Exception ex) {
        entityList = entityService.findEntitiesByWorkspaceId(workspaceId);
      }

      List<Entity> filteredEntityList = new ArrayList<>();
      for (int i = 0; i < entityList.size(); i++) {
        List<Model> modelList = entityList.get(i).getModelList();
        List<Model> filteredModelList = modelList.stream()
            .filter(model -> model.getId().equals(modelId)).collect(Collectors.toList());
        if (filteredModelList.size() == 0) {
          continue;
        }

        filteredEntityList.add(entityList.get(i));
      }

      for (int i = 0; i < filteredEntityList.size(); i++) {
        if (filteredEntityList.get(i).getEntityName().equals(entityName)) {
          result.put("statusCode", 400);
          result.put("statusMessage", "entityName duplicated.");

          return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }
      }

      Entity entity = new Entity();
      entity.setId(entityId);
      entity.setClassName(className);
      entity.setClassSource(classSource);
      entity.setEntityName(entityName);
      entity.setEntitySource(entitySource);
      entity.setEntityType(entityType);
      entity.setSharedYn(sharedYn);

      entity.setWorkspaceId(workspaceId);
      entity.setCreatorId(userId);
      entity.setCreatedAt(currentTimestamp);
      entity.setUpdaterId(userId);
      entity.setUpdatedAt(currentTimestamp);

      findModel.addEntity(entity);
      entity.addModel(findModel);

      try {
        entityService.insertEntity(entity);
      } catch (Exception ex) {
        entityService.insertEntity(entity);
      }

      try {
        modelService.updateModel(findModel);
      } catch (Exception ex) {
        modelService.updateModel(findModel);
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("POST /api/workspace/{}/entity/{}/entity", workspaceId, modelId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^R")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/entity/allinfolist", method = RequestMethod.GET)
  public ResponseEntity<?> getEntityAllInfoListByWorkspaceIdAndModelId(
      @PathVariable String workspaceId, @PathVariable String modelId) {
    logger.info("GET /api/workspace/{}/entity/{}/entity/allinfolist", workspaceId, modelId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      List<Entity> entityList = new ArrayList<>();
      try {
        entityList = entityService.findEntitiesByWorkspaceId(workspaceId);
      } catch (Exception ex) {
        entityList = entityService.findEntitiesByWorkspaceId(workspaceId);
      }

      JSONObject wrappedEntityInfo = new JSONObject();
      JSONArray entityInfoList = new JSONArray();

      for (int i = 0; i < entityList.size(); i++) {
        List<Model> modelList = entityList.get(i).getModelList();
        List<Model> filteredModelList = modelList.stream()
            .filter(model -> model.getId().equals(modelId)).collect(Collectors.toList());
        if (filteredModelList.size() == 0) {
          continue;
        }

        JSONObject entityInfo = new JSONObject();

        entityInfo.put("id", entityList.get(i).getId());
        entityInfo.put("className", entityList.get(i).getClassName());
        entityInfo.put("entityName", entityList.get(i).getEntityName());
        entityInfo.put("entityType", entityList.get(i).getEntityType());
        entityInfo.put("sharedYn", entityList.get(i).getSharedYn());

        JSONArray entityDataInfoList = new JSONArray();
        for (EntityData entityData : entityList.get(i).getEntityDataList()) {
          JSONObject entityDataInfo = new JSONObject();

          entityDataInfo.put("id", entityData.getId());
          entityDataInfo.put("entityData", entityData.getEntityData());
          entityDataInfo.put("entityId", entityData.getEntity().getId());

          JSONArray entityDataSynonymInfoList = new JSONArray();
          for (EntityDataSynonym entityDataSynonym : entityData.getEntityDataSynonymList()) {
            JSONObject entityDataSynonymInfo = new JSONObject();

            entityDataSynonymInfo.put("id", entityDataSynonym.getId());
            entityDataSynonymInfo
                .put("entityDataSynonym", entityDataSynonym.getEntityDataSynonym());
            entityDataSynonymInfo.put("entityDataId", entityDataSynonym.getEntityData().getId());

            entityDataSynonymInfo.put("creatorId", entityDataSynonym.getCreatorId());
            entityDataSynonymInfo.put("createdAt", entityDataSynonym.getCreatedAt());
            entityDataSynonymInfo.put("updaterId", entityDataSynonym.getUpdaterId());
            entityDataSynonymInfo.put("updatedAt", entityDataSynonym.getUpdatedAt());

            entityDataSynonymInfoList.add(entityDataSynonymInfo);
          }
          entityDataInfo.put("entityDataSynonym", entityDataSynonymInfoList);

          entityDataInfo.put("creatorId", entityData.getCreatorId());
          entityDataInfo.put("createdAt", entityData.getCreatedAt());
          entityDataInfo.put("updaterId", entityData.getUpdaterId());
          entityDataInfo.put("updatedAt", entityData.getUpdatedAt());

          entityDataInfoList.add(entityDataInfo);
        }
        entityInfo.put("entityData", entityDataInfoList);

        entityInfo.put("creatorId", entityList.get(i).getCreatorId());
        entityInfo.put("createdAt", entityList.get(i).getCreatedAt());
        entityInfo.put("updaterId", entityList.get(i).getUpdaterId());
        entityInfo.put("updatedAt", entityList.get(i).getUpdatedAt());
        entityInfo.put("workspaceId", entityList.get(i).getWorkspaceId());

        entityInfoList.add(entityInfo);
      }

      wrappedEntityInfo.put("entity", entityInfoList);

      result.put("data", wrappedEntityInfo);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("GET /api/workspace/{}/entity/{}/entity/allinfolist", workspaceId, modelId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^R")
  @RequestMapping(value = "/workspace/{workspaceId}/sharedentity/list", method = RequestMethod.GET)
  public ResponseEntity<?> getSharedEntityListByWorkspaceId(@PathVariable String workspaceId) {
    logger.info("GET /api/workspace/{}/sharedentity/list", workspaceId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      List<Entity> entityList = new ArrayList<>();
      try {
        entityList = entityService.findEntitiesByWorkspaceId(workspaceId);
      } catch (Exception ex) {
        entityList = entityService.findEntitiesByWorkspaceId(workspaceId);
      }

      JSONObject wrappedEntityInfo = new JSONObject();
      JSONArray entityInfoList = new JSONArray();

      for (int i = 0; i < entityList.size(); i++) {
        if (entityList.get(i).getSharedYn().equals("Y") == false) {
          continue;
        }

        JSONObject entityInfo = new JSONObject();

        entityInfo.put("id", entityList.get(i).getId());
        entityInfo.put("className", entityList.get(i).getClassName());
        entityInfo.put("entityName", entityList.get(i).getEntityName());
        entityInfo.put("entityType", entityList.get(i).getEntityType());
        entityInfo.put("sharedYn", entityList.get(i).getSharedYn());

        JSONArray entityDataInfoList = new JSONArray();
        for (EntityData entityData : entityList.get(i).getEntityDataList()) {
          JSONObject entityDataInfo = new JSONObject();

          entityDataInfo.put("id", entityData.getId());
          entityDataInfo.put("entityData", entityData.getEntityData());
          entityDataInfo.put("entityId", entityData.getEntity().getId());

          JSONArray entityDataSynonymInfoList = new JSONArray();
          for (EntityDataSynonym entityDataSynonym : entityData.getEntityDataSynonymList()) {
            JSONObject entityDataSynonymInfo = new JSONObject();

            entityDataSynonymInfo.put("id", entityDataSynonym.getId());
            entityDataSynonymInfo
                .put("entityDataSynonym", entityDataSynonym.getEntityDataSynonym());
            entityDataSynonymInfo.put("entityDataId", entityDataSynonym.getEntityData().getId());

            entityDataSynonymInfo.put("creatorId", entityDataSynonym.getCreatorId());
            entityDataSynonymInfo.put("createdAt", entityDataSynonym.getCreatedAt());
            entityDataSynonymInfo.put("updaterId", entityDataSynonym.getUpdaterId());
            entityDataSynonymInfo.put("updatedAt", entityDataSynonym.getUpdatedAt());

            entityDataSynonymInfoList.add(entityDataSynonymInfo);
          }
          entityDataInfo.put("entityDataSynonym", entityDataSynonymInfoList);

          entityDataInfo.put("creatorId", entityData.getCreatorId());
          entityDataInfo.put("createdAt", entityData.getCreatedAt());
          entityDataInfo.put("updaterId", entityData.getUpdaterId());
          entityDataInfo.put("updatedAt", entityData.getUpdatedAt());

          entityDataInfoList.add(entityDataInfo);
        }
        entityInfo.put("entityData", entityDataInfoList);

        entityInfo.put("creatorId", entityList.get(i).getCreatorId());
        entityInfo.put("createdAt", entityList.get(i).getCreatedAt());
        entityInfo.put("updaterId", entityList.get(i).getUpdaterId());
        entityInfo.put("updatedAt", entityList.get(i).getUpdatedAt());
        entityInfo.put("workspaceId", entityList.get(i).getWorkspaceId());

        entityInfoList.add(entityInfo);
      }

      wrappedEntityInfo.put("entity", entityInfoList);

      result.put("data", wrappedEntityInfo);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("GET /api/workspace/{}/sharedentity/list", workspaceId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^R")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/entity/list", method = RequestMethod.GET)
  public ResponseEntity<?> getEntityListByWorkspaceIdAndModelId(@PathVariable String workspaceId,
      @PathVariable String modelId) {
    logger.info("GET /api/workspace/{}/entity/{}/entity/list", workspaceId, modelId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      List<Entity> entityList = new ArrayList<>();
      try {
        entityList = entityService.findEntitiesByWorkspaceId(workspaceId);
      } catch (Exception ex) {
        entityList = entityService.findEntitiesByWorkspaceId(workspaceId);
      }

      JSONObject wrappedEntityInfo = new JSONObject();
      JSONArray entityInfoList = new JSONArray();

      for (int i = 0; i < entityList.size(); i++) {
        List<Model> modelList = entityList.get(i).getModelList();
        List<Model> filteredModelList = modelList.stream()
            .filter(model -> model.getId().equals(modelId)).collect(Collectors.toList());
        if (filteredModelList.size() == 0) {
          continue;
        }

        JSONObject entityInfo = new JSONObject();

        entityInfo.put("id", entityList.get(i).getId());
        entityInfo.put("className", entityList.get(i).getClassName());
        entityInfo.put("entityName", entityList.get(i).getEntityName());
        entityInfo.put("entityType", entityList.get(i).getEntityType());
        entityInfo.put("sharedYn", entityList.get(i).getSharedYn());

        entityInfo.put("creatorId", entityList.get(i).getCreatorId());
        entityInfo.put("createdAt", entityList.get(i).getCreatedAt());
        entityInfo.put("updaterId", entityList.get(i).getUpdaterId());
        entityInfo.put("updatedAt", entityList.get(i).getUpdatedAt());
        entityInfo.put("workspaceId", entityList.get(i).getWorkspaceId());

        entityInfoList.add(entityInfo);
      }

      wrappedEntityInfo.put("entityInfoList", entityInfoList);

      result.put("data", wrappedEntityInfo);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("GET /api/workspace/{}/entity/{}/entity/list", workspaceId, modelId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^R")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/entity/{entityId}", method = RequestMethod.GET)
  public ResponseEntity<?> getEntityByWorkspaceIdAndModelId(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String entityId) {
    logger.info("GET /api/workspace/{}/entity/{}/entity/{}", workspaceId, modelId, entityId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      Entity entity = null;
      try {
        entity = entityService.findEntityByWorkspaceIdAndId(workspaceId, entityId);
      } catch (Exception ex) {
        entity = entityService.findEntityByWorkspaceIdAndId(workspaceId, entityId);
      }

      JSONObject wrappedEntityInfo = new JSONObject();
      JSONObject entityInfo = new JSONObject();

      entityInfo.put("id", entity.getId());
      entityInfo.put("className", entity.getClassName());
      entityInfo.put("entityName", entity.getEntityName());
      entityInfo.put("entityType", entity.getEntityType());
      entityInfo.put("sharedYn", entity.getSharedYn());

      entityInfo.put("creatorId", entity.getCreatorId());
      entityInfo.put("createdAt", entity.getCreatedAt());
      entityInfo.put("updaterId", entity.getUpdaterId());
      entityInfo.put("updatedAt", entity.getUpdatedAt());
      entityInfo.put("workspaceId", entity.getWorkspaceId());

      wrappedEntityInfo.put("entityInfo", entityInfo);

      result.put("data", wrappedEntityInfo);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("GET /api/workspace/{}/entity/{}/entity/{}", workspaceId, modelId, entityId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^U")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/entity/{entityId}", method = RequestMethod.PUT)
  public ResponseEntity<?> updateEntity(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String entityId,
      @RequestBody JSONObject bodyParam) {
    logger.info("PUT /api/workspace/{}/entity/{}/entity/{}", workspaceId, modelId, entityId,
        bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      String className = null;
      String entityName = null;
      String entityType = null;
      String sharedYn = null;
      String userId = null;

      Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

      Entity findEntity = null;
      try {
        findEntity = entityService.findEntityByWorkspaceIdAndId(workspaceId, entityId);
      } catch (Exception ex) {
        findEntity = entityService.findEntityByWorkspaceIdAndId(workspaceId, entityId);
      }
      if (findEntity == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "Entity doesn't exist.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      if (bodyParam.get("className") != null && !bodyParam.get("className").toString().trim()
          .equals("")) {
        className = bodyParam.get("className").toString().trim();
      }

      if (bodyParam.get("entityName") != null && !bodyParam.get("entityName").toString().trim()
          .equals("")) {
        entityName = bodyParam.get("entityName").toString().trim();
      }

      if (bodyParam.get("entityType") != null && !bodyParam.get("entityType").toString().trim()
          .equals("")) {
        entityType = bodyParam.get("entityType").toString().trim();
      }

      if (bodyParam.get("sharedYn") != null && !bodyParam.get("sharedYn").toString().trim()
          .equals("")) {
        sharedYn = bodyParam.get("sharedYn").toString().trim();
      }

      if (bodyParam.get("userId") == null || bodyParam.get("userId").toString().trim().equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "userId is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        userId = bodyParam.get("userId").toString().trim();
      }

      if (!sharedYn.equals("Y") && !findEntity.getCreatorId().equals(userId)) {
        result.put("statusCode", 400);
        result.put("statusMessage", "다른 사용자가 작성한 모델은 수정할 수 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      Model findModel = null;
      try {
        findModel = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
      } catch (Exception ex) {
        findModel = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
      }
      if (findModel == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "Model doesn't exist.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      List<Entity> entityList = new ArrayList<>();
      try {
        entityList = entityService.findEntitiesByWorkspaceId(workspaceId);
      } catch (Exception ex) {
        entityList = entityService.findEntitiesByWorkspaceId(workspaceId);
      }

      List<Entity> filteredEntityList = new ArrayList<>();
      for (int i = 0; i < entityList.size(); i++) {
        if (sharedYn.equals("Y")) {
          if (entityList.get(i).getSharedYn().equals("Y") == false) {
            continue;
          }
        } else {
          List<Model> modelList = entityList.get(i).getModelList();
          List<Model> filteredModelList = modelList.stream()
              .filter(model -> model.getId().equals(modelId)).collect(Collectors.toList());
          if (filteredModelList.size() == 0) {
            continue;
          }
        }

        filteredEntityList.add(entityList.get(i));
      }

      for (int i = 0; i < filteredEntityList.size(); i++) {
        if (entityName != null) {
          if (filteredEntityList.get(i).getEntityName().equals(entityName)) {
            result.put("statusCode", 400);
            result.put("statusMessage", "entityName duplicated.");

            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
          }
        }
      }

      if (className != null) {
        String oldClassName = findEntity.getClassName();

        for (int i = 0; i < filteredEntityList.size(); i++) {
          if (filteredEntityList.get(i).getClassName().equals(oldClassName)) {
            filteredEntityList.get(i).setClassName(className);
            filteredEntityList.get(i).setUpdaterId(userId);
            filteredEntityList.get(i).setUpdatedAt(currentTimestamp);

            try {
              entityService.updateEntity(filteredEntityList.get(i));
            } catch (Exception ex) {
              entityService.updateEntity(filteredEntityList.get(i));
            }
          }
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
      }

      if (entityName != null) {
        findEntity.setEntityName(entityName);
      }

      if (entityType != null) {
        findEntity.setEntityType(entityType);
      }

      if (sharedYn != null) {
        findEntity.setSharedYn(sharedYn);
      }

      findEntity.setUpdaterId(userId);
      findEntity.setUpdatedAt(currentTimestamp);

      try {
        entityService.updateEntity(findEntity);
      } catch (Exception ex) {
        entityService.updateEntity(findEntity);
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("PUT /api/workspace/{}/entity/{}/entity/{}", workspaceId, modelId, entityId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^D")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/entity/{entityId}", method = RequestMethod.POST)
  public ResponseEntity<?> deleteEntityByWorkspaceIdAndId(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String entityId,
      @RequestBody JSONObject bodyParam) {
    logger.info("POST /api/workspace/{}/entity/{}/entity/{}", workspaceId, modelId, entityId,
        bodyParam);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      boolean isLinkDelete = false;

      if (bodyParam.get("isLinkDelete") != null) {
        isLinkDelete = (boolean) bodyParam.get("isLinkDelete");
      }

      Entity entity = null;
      try {
        entity = entityService.findEntityByWorkspaceIdAndId(workspaceId, entityId);
      } catch (Exception ex) {
        entity = entityService.findEntityByWorkspaceIdAndId(workspaceId, entityId);
      }

      if (entity == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "entityId 와 일치하는 entity 정보가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.OK);
      }

      if (isLinkDelete == true) {
        Model findModel = null;
        try {
          findModel = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
        } catch (Exception ex) {
          findModel = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
        }

        for (int i = 0; i < findModel.getEntityList().size(); i++) {
          if (findModel.getEntityList().get(i).getId().equals(entityId)) {
            findModel.getEntityList().remove(i);
            break;
          }
        }

        try {
          modelService.updateModel(findModel);
        } catch (Exception ex) {
          modelService.updateModel(findModel);
        }

        for (int i = 0; i < entity.getModelList().size(); i++) {
          if (entity.getModelList().get(i).getId().equals(findModel.getId())) {
            entity.getModelList().remove(i);
            break;
          }
        }

        try {
          entityService.updateEntity(entity);
        } catch (Exception ex) {
          entityService.updateEntity(entity);
        }
      } else {
        List<Model> modelList = modelService.findModelsByWorkspaceIdOrderByModelName(workspaceId);
        for (Model model : modelList) {
          for (int i = 0; i < model.getEntityList().size(); i++) {
            if (model.getEntityList().get(i).getId().equals(entityId)) {
              model.getEntityList().remove(i);

              try {
                modelService.updateModel(model);
              } catch (Exception ex) {
                modelService.updateModel(model);
              }

              break;
            }
          }
        }

        entity.setModelList(new ArrayList<>());

        try {
          entityService.updateEntity(entity);
          entityService.deleteEntityByWorkspaceIdAndId(workspaceId, entityId);
        } catch (Exception ex) {
          entityService.updateEntity(entity);
          entityService.deleteEntityByWorkspaceIdAndId(workspaceId, entityId);
        }
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("POST /api/workspace/{}/entity/{}/entity/{}", workspaceId, modelId, entityId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^D")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/entity/list", method = RequestMethod.POST)
  public ResponseEntity<?> deleteEntityClassByWorkspaceIdAndId(@PathVariable String workspaceId,
      @PathVariable String modelId, @RequestBody JSONObject bodyParam) {
    logger.info("POST /api/workspace/{}/entity/{}/entity/list", workspaceId, modelId, bodyParam);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      ArrayList<String> isLinkDelete = new ArrayList<>();
      ArrayList<String> entityIdList = new ArrayList<>();

      if (bodyParam.get("isLinkDelete") == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "isLinkDelete 가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        isLinkDelete = (ArrayList) bodyParam.get("isLinkDelete");
      }

      if (bodyParam.get("entityIdList") == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "entityIdList 가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        entityIdList = (ArrayList) bodyParam.get("entityIdList");
      }

      for (int idx = 0; idx < entityIdList.size(); idx++) {
        Entity entity = null;
        try {
          entity = entityService.findEntityByWorkspaceIdAndId(workspaceId, entityIdList.get(idx));
        } catch (Exception ex) {
          entity = entityService.findEntityByWorkspaceIdAndId(workspaceId, entityIdList.get(idx));
        }

        if (entity == null) {
          result.put("statusCode", 400);
          result.put("statusMessage", "entityId 와 일치하는 entity 정보가 없습니다.");

          return new ResponseEntity<>(result, HttpStatus.OK);
        }

        if (isLinkDelete.get(idx).equals("true")) {
          Model findModel = null;
          try {
            findModel = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
          } catch (Exception ex) {
            findModel = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
          }

          for (int i = 0; i < findModel.getEntityList().size(); i++) {
            if (findModel.getEntityList().get(i).getId().equals(entityIdList.get(idx))) {
              findModel.getEntityList().remove(i);
              break;
            }
          }

          try {
            modelService.updateModel(findModel);
          } catch (Exception ex) {
            modelService.updateModel(findModel);
          }

          for (int i = 0; i < entity.getModelList().size(); i++) {
            if (entity.getModelList().get(i).getId().equals(findModel.getId())) {
              entity.getModelList().remove(i);
              break;
            }
          }

          try {
            entityService.updateEntity(entity);
          } catch (Exception ex) {
            entityService.updateEntity(entity);
          }
        } else {
          List<Model> modelList = modelService.findModelsByWorkspaceIdOrderByModelName(workspaceId);
          for (Model model : modelList) {
            for (int i = 0; i < model.getEntityList().size(); i++) {
              if (model.getEntityList().get(i).getId().equals(entityIdList.get(idx))) {
                model.getEntityList().remove(i);

                try {
                  modelService.updateModel(model);
                } catch (Exception ex) {
                  modelService.updateModel(model);
                }

                break;
              }
            }
          }

          entity.setModelList(new ArrayList<>());

          try {
            entityService.updateEntity(entity);
            entityService.deleteEntityByWorkspaceIdAndId(workspaceId, entityIdList.get(idx));
          } catch (Exception ex) {
            entityService.updateEntity(entity);
            entityService.deleteEntityByWorkspaceIdAndId(workspaceId, entityIdList.get(idx));
          }
        }
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("POST /api/workspace/{}/entity/{}/entity/list", workspaceId, modelId, bodyParam,
          e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^R")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/entitydownload", method = RequestMethod.GET)
  public ResponseEntity<?> downloadEntityListByWorkspaceIdAndModelId(
      @PathVariable String workspaceId, @PathVariable String modelId) {
    logger.info("GET /api/workspace/{}/entity/{}/entitydownload", workspaceId, modelId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      Model findModel = null;
      try {
        findModel = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
      } catch (Exception ex) {
        findModel = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
      }

      if (findModel == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "Model doesn't exist.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      List<Entity> entityList = findModel.getEntityList();

      JSONObject wrappedEntityInfo = new JSONObject();

      StringBuilder strResult = new StringBuilder();
      strResult.append("className");
      strResult.append("\t");
      strResult.append("entityName");
      strResult.append("\t");
      strResult.append("entityType");
      strResult.append("\t");
      strResult.append("entityData");
      strResult.append("\t");
      strResult.append("entityDataSynonymList");
      strResult.append("\n");

      for (int i = 0; i < entityList.size(); i++) {
        // className, entityName, entityType, entityData, entityDataSynonymList

        StringBuilder strEntityInfo = new StringBuilder();

        strEntityInfo.append(entityList.get(i).getClassName());

        strEntityInfo.append("\t");
        strEntityInfo.append(entityList.get(i).getEntityName());

        strEntityInfo.append("\t");
        strEntityInfo.append(entityList.get(i).getEntityType());

        if (entityList.get(i).getEntityDataList().size() == 0) {
          strResult.append(strEntityInfo);
          strResult.append("\n");
        } else {
          for (EntityData entityData : entityList.get(i).getEntityDataList()) {
            StringBuilder strEntityDataInfo = new StringBuilder();

            strEntityDataInfo.append(entityData.getEntityData());

            if (entityData.getEntityDataSynonymList().size() == 0) {
              strResult.append(strEntityInfo);
              strResult.append("\t");
              strResult.append(strEntityDataInfo);
              strResult.append("\n");
            } else {
              List<String> entityDataSynonymList = new ArrayList<>();
              for (EntityDataSynonym entityDataSynonym : entityData.getEntityDataSynonymList()) {
                entityDataSynonymList.add(entityDataSynonym.getEntityDataSynonym());
              }

              String strEntityDataSynonymInfo = String.join("|", entityDataSynonymList);

              strResult.append(strEntityInfo);
              strResult.append("\t");
              strResult.append(strEntityDataInfo);
              strResult.append("\t");
              strResult.append(strEntityDataSynonymInfo);
              strResult.append("\n");

//                            for (EntityDataSynonym entityDataSynonym : entityData.getEntityDataSynonymList()) {
//                                StringBuilder strEntityDataSynonymInfo = new StringBuilder();
//
//                                strEntityDataSynonymInfo.append(entityDataSynonym.getEntityDataSynonym());
//
//                                strResult.append(strEntityInfo);
//                                strResult.append("\t");
//                                strResult.append(strEntityDataInfo);
//                                strResult.append("\t");
//                                strResult.append(strEntityDataSynonymInfo);
//                                strResult.append("\n");
//                            }
            }
          }
        }
      }

      HttpHeaders header = new HttpHeaders();
      header.add("Content-Type", "text/tsv; charset=UTF-8");
      header.add("Content-Disposition", "attachment; filename=\"" + "entity.tsv" + "\"");

      return new ResponseEntity<>(strResult, header, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("GET /api/workspace/{}/entity/{}/entitydownload", workspaceId, modelId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^C")
  @RequestMapping(value = "/user/{userId}/workspace/{workspaceId}/model/{modelId}/entityupload", method = RequestMethod.POST)
  public ResponseEntity<?> uploadEntityListByWorkspaceIdAndModelId(@PathVariable String userId,
      @PathVariable String workspaceId, @PathVariable String modelId,
      @RequestPart MultipartFile sourceFile) {
    logger.info("POST /api/userid/{}/workspace/{}/entity/{}/entityupload", userId, workspaceId,
        modelId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

    BufferedReader br = null;
    String line;
    String tsvSplitBy = "\t";

    try {
      Model findModel = null;
      try {
        findModel = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
      } catch (Exception ex) {
        findModel = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
      }

      if (findModel == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "Model doesn't exist.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      List<Entity> entityList = findModel.getEntityList();

      long rowCount = 0;
      long successCount = 0;
      long failCount = 0;
      List<String> failDataList = new ArrayList<>();

      br = new BufferedReader(new InputStreamReader(sourceFile.getInputStream(), "UTF-8"));
      while ((line = br.readLine()) != null) {
        rowCount++;

        if (rowCount == 1) {
          if (line.startsWith("\uFEFF")) {
            line = line.substring(1);
          }

          String[] field = line.split(tsvSplitBy);

          if (field.length != 5) {
            result.put("statusCode", 500);
            result.put("statusMessage", "Entity Data 형식의 파일이 아닙니다.");

            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
          }

          if (field[0].equals("className") && field[1].equals("entityName") && field[2]
              .equals("entityType") && field[3].equals("entityData") && field[4]
              .equals("entityDataSynonymList")) {
            continue;
          } else {
            result.put("statusCode", 500);
            result.put("statusMessage", "Entity Data 형식의 파일이 아닙니다.");

            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
          }
        } else {
          String[] field = line.split(tsvSplitBy);

          List<Entity> filteredEntityList = entityList.stream()
              .filter(entity -> entity.getEntityName().equals(field[1]))
              .collect(Collectors.toList());
          if (filteredEntityList.size() == 0) {
            // Entity
            if (field.length >= 3 && !field[0].trim().equals("") && !field[1].trim().equals("")
                && !field[2].trim().equals("")) {
              if (!field[2].equals("INT") && !field[2].equals("FLOAT") && !field[2].equals("STRING")
                  && !field[2].equals("TIME") && !field[2].equals("DATE") && !field[2]
                  .equals("EVENT")) {
                failCount++;
                failDataList.add(line);
                continue;
              }

              Entity entity = new Entity();
              entity.setId(UUID.randomUUID().toString());
              entity.setClassName(field[0]);
              entity.setClassSource("SYSTEM");
              entity.setEntityName(field[1]);
              entity.setEntitySource("KB");
              entity.setEntityType(field[2]);
              entity.setSharedYn("N");

              entity.setWorkspaceId(workspaceId);
              entity.setCreatorId(userId);
              entity.setCreatedAt(currentTimestamp);
              entity.setUpdaterId(userId);
              entity.setUpdatedAt(currentTimestamp);

              findModel.addEntity(entity);
              entity.addModel(findModel);

              try {
                entityService.insertEntity(entity);
              } catch (Exception ex) {
                entityService.insertEntity(entity);
              }

              try {
                modelService.updateModel(findModel);
              } catch (Exception ex) {
                modelService.updateModel(findModel);
              }

              // Entity Data
              if (field.length >= 4 && !field[3].trim().equals("")) {
                EntityData entityData = new EntityData();
                entityData.setId(UUID.randomUUID().toString());
                entityData.setEntityData(field[3]);
                entityData.setEntity(entity);

                entityData.setCreatorId(userId);
                entityData.setCreatedAt(currentTimestamp);
                entityData.setUpdaterId(userId);
                entityData.setUpdatedAt(currentTimestamp);

                try {
                  entityDataService.insertEntityData(entityData);

                  for (Entity findEntity : entityList) {
                    if (findEntity.getId().equals(entity.getId())) {
                      findEntity.getEntityDataList().add(entityData);
                      break;
                    }
                  }
                } catch (Exception ex) {
                  entityDataService.insertEntityData(entityData);

                  for (Entity findEntity : entityList) {
                    if (findEntity.getId().equals(entity.getId())) {
                      findEntity.getEntityDataList().add(entityData);
                      break;
                    }
                  }
                }

                // Entity Data Synonym
                if (field.length >= 5 && !field[4].trim().equals("")) {
                  String[] entityDataSynonymList = field[4].split("\\|");
                  for (String strEntityDataSynonym : entityDataSynonymList) {
                    EntityDataSynonym entityDataSynonym = new EntityDataSynonym();
                    entityDataSynonym.setId(UUID.randomUUID().toString());
                    entityDataSynonym.setEntityDataSynonym(strEntityDataSynonym);
                    entityDataSynonym.setEntityData(entityData);

                    entityDataSynonym.setCreatorId(userId);
                    entityDataSynonym.setCreatedAt(currentTimestamp);
                    entityDataSynonym.setUpdaterId(userId);
                    entityDataSynonym.setUpdatedAt(currentTimestamp);

                    try {
                      entityDataSynonymService.insertEntityDataSynonym(entityDataSynonym);

                      for (Entity findEntity : entityList) {
                        if (findEntity.getId().equals(entity.getId())) {
                          for (EntityData findEntityData : findEntity.getEntityDataList()) {
                            if (findEntityData.getId().equals(entityData.getId())) {
                              findEntityData.getEntityDataSynonymList().add(entityDataSynonym);
                              break;
                            }
                          }
                        }
                      }
                    } catch (Exception ex) {
                      entityDataSynonymService.insertEntityDataSynonym(entityDataSynonym);

                      for (Entity findEntity : entityList) {
                        if (findEntity.getId().equals(entity.getId())) {
                          for (EntityData findEntityData : findEntity.getEntityDataList()) {
                            if (findEntityData.getId().equals(entityData.getId())) {
                              findEntityData.getEntityDataSynonymList().add(entityDataSynonym);
                              break;
                            }
                          }
                        }
                      }
                    }
                  }

                  successCount++;
                } else {
                  successCount++;
                  continue;
                }
              } else {
                successCount++;
                continue;
              }
            } else {
              failCount++;
              failDataList.add(line);
              continue;
            }
          } else {
            // Entity
            if (field.length >= 3 && !field[0].trim().equals("") && !field[1].trim().equals("")
                && !field[2].trim().equals("")) {
              Entity entity = filteredEntityList.get(0);

              // Entity Data
              if (field.length >= 4 && !field[3].trim().equals("")) {
                List<EntityData> filteredEntityDataList = entity.getEntityDataList().stream()
                    .filter(entityData -> entityData.getEntityData().equals(field[3]))
                    .collect(Collectors.toList());

                EntityData entityData = null;
                if (filteredEntityDataList.size() == 0) {
                  entityData = new EntityData();
                  entityData.setId(UUID.randomUUID().toString());
                  entityData.setEntityData(field[3]);
                  entityData.setEntity(entity);

                  entityData.setCreatorId(userId);
                  entityData.setCreatedAt(currentTimestamp);
                  entityData.setUpdaterId(userId);
                  entityData.setUpdatedAt(currentTimestamp);

                  try {
                    entityDataService.insertEntityData(entityData);

                    for (Entity findEntity : entityList) {
                      if (findEntity.getId().equals(entity.getId())) {
                        findEntity.getEntityDataList().add(entityData);
                        break;
                      }
                    }
                  } catch (Exception ex) {
                    entityDataService.insertEntityData(entityData);

                    for (Entity findEntity : entityList) {
                      if (findEntity.getId().equals(entity.getId())) {
                        findEntity.getEntityDataList().add(entityData);
                        break;
                      }
                    }
                  }
                } else {
                  entityData = filteredEntityDataList.get(0);
                }

                // Entity Data Synonym
                if (field.length >= 5 && !field[4].trim().equals("")) {
                  String[] entityDataSynonymList = field[4].split("\\|");
                  for (String strEntityDataSynonym : entityDataSynonymList) {
                    EntityDataSynonym entityDataSynonym = new EntityDataSynonym();
                    entityDataSynonym.setId(UUID.randomUUID().toString());
                    entityDataSynonym.setEntityDataSynonym(strEntityDataSynonym);
                    entityDataSynonym.setEntityData(entityData);

                    entityDataSynonym.setCreatorId(userId);
                    entityDataSynonym.setCreatedAt(currentTimestamp);
                    entityDataSynonym.setUpdaterId(userId);
                    entityDataSynonym.setUpdatedAt(currentTimestamp);

                    try {
                      entityDataSynonymService.insertEntityDataSynonym(entityDataSynonym);

                      for (Entity findEntity : entityList) {
                        if (findEntity.getId().equals(entity.getId())) {
                          for (EntityData findEntityData : findEntity.getEntityDataList()) {
                            if (findEntityData.getId().equals(entityData.getId())) {
                              findEntityData.getEntityDataSynonymList().add(entityDataSynonym);
                              break;
                            }
                          }
                        }
                      }
                    } catch (Exception ex) {
                      entityDataSynonymService.insertEntityDataSynonym(entityDataSynonym);

                      for (Entity findEntity : entityList) {
                        if (findEntity.getId().equals(entity.getId())) {
                          for (EntityData findEntityData : findEntity.getEntityDataList()) {
                            if (findEntityData.getId().equals(entityData.getId())) {
                              findEntityData.getEntityDataSynonymList().add(entityDataSynonym);
                              break;
                            }
                          }
                        }
                      }
                    }
                  }

                  successCount++;
                } else {
                  successCount++;
                  continue;
                }
              } else {
                successCount++;
                continue;
              }
            } else {
              failCount++;
              failDataList.add(line);
              continue;
            }
          }
        }
      }

      JSONObject data = new JSONObject();
      data.put("totalRowCount", rowCount - 1);
      data.put("successCount", successCount);
      data.put("failCount", failCount);
      data.put("failDataList", failDataList);

      result.put("data", data);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("POST /api/userid/{}/workspace/{}/entity/{}/entityupload", userId, workspaceId,
          modelId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    } finally {
      if (br != null) {
        try {
          br.close();
        } catch (IOException e) {
          logger
              .error("POST /api/userid/{}/workspace/{}/entity/{}/entityupload", userId, workspaceId,
                  modelId, e);
        }
      }
    }
  }
}
