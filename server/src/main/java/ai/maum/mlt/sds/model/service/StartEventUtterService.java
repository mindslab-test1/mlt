package ai.maum.mlt.sds.model.service;

import ai.maum.mlt.sds.model.entity.StartEvent;
import ai.maum.mlt.sds.model.entity.StartEventUtter;
import java.util.List;
import javax.transaction.Transactional;

@Transactional
public interface StartEventUtterService {

  StartEventUtter insertStartEventUtter(StartEventUtter startEventUtter);

  StartEventUtter findStartEventUtterById(String startEventUtterId);

  List<StartEventUtter> findAll();

  List<StartEventUtter> findStartEventUttersByStartEvent(StartEvent startEvent);

  StartEventUtter updateStartEventUtter(StartEventUtter startEventUtter);

  void deleteStartEventUtterById(String startEventUtterId);
}
