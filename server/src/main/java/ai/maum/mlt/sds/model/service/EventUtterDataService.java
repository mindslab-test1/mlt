package ai.maum.mlt.sds.model.service;

import ai.maum.mlt.sds.model.entity.EventUtter;
import ai.maum.mlt.sds.model.entity.EventUtterData;
import java.util.List;
import javax.transaction.Transactional;

@Transactional
public interface EventUtterDataService {

  EventUtterData insertEventUtterData(EventUtterData eventUtterData);

  EventUtterData findEventUtterDataById(String eventUtterDataId);

  List<EventUtterData> findAll();

  List<EventUtterData> findEventUtterDataByEventUtter(EventUtter eventUtter);

  EventUtterData updateEventUtterData(EventUtterData eventUtterData);

  void deleteEventUtterDataById(String eventUtterDataId);
}
