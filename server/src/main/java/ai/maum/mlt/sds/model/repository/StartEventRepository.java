package ai.maum.mlt.sds.model.repository;

import ai.maum.mlt.sds.model.entity.StartEvent;
import ai.maum.mlt.sds.model.entity.Task;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface StartEventRepository extends JpaRepository<StartEvent, String> {

  StartEvent findStartEventById(String startEventId);

  List<StartEvent> findAll();

  List<StartEvent> findStartEventsByTask(Task task);

  void deleteStartEventById(String startEventId);
}
