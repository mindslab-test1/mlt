package ai.maum.mlt.sds.model.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.sds.model.entity.EventUtter;
import ai.maum.mlt.sds.model.entity.EventUtterData;
import ai.maum.mlt.sds.model.service.EventUtterDataService;
import ai.maum.mlt.sds.model.service.EventUtterService;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/sds/model")
public class EventUtterDataController {

  static final Logger logger = LoggerFactory.getLogger(EventUtterDataController.class);

  @Autowired
  private EventUtterService eventUtterService;

  @Autowired
  private EventUtterDataService eventUtterDataService;

  @UriRoleDesc(role = "sds/model^C")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}/event/{eventId}/eventcondition/{eventConditionId}/eventutter/{eventUtterId}/eventutterdata", method = RequestMethod.POST)
  public ResponseEntity<?> insertEventUtterData(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String taskId, @PathVariable String eventId,
      @PathVariable String eventConditionId, @PathVariable String eventUtterId,
      @RequestBody JSONObject bodyParam) {
    logger.info(
        "POST /api/workspace/{}/entity/{}/task/{}/event/{}/eventcondition/{}/eventutter/{}/eventutterdata",
        workspaceId, modelId, taskId, eventId, eventConditionId, eventUtterId, bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      String eventUtterDataId = null;
      String eventUtterDataContent = "";

      String userId = null;

      Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

      if (bodyParam.get("id") == null || bodyParam.get("id").toString().trim().equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "id is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        eventUtterDataId = bodyParam.get("id").toString().trim();

        EventUtterData findEventUtterData = null;
        try {
          findEventUtterData = eventUtterDataService.findEventUtterDataById(eventUtterDataId);
        } catch (Exception ex) {
          findEventUtterData = eventUtterDataService.findEventUtterDataById(eventUtterDataId);
        }
        if (findEventUtterData != null) {
          result.put("statusCode", 400);
          result.put("statusMessage", "id is already exist.");

          return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }
      }

      if (bodyParam.get("eventUtterData") != null) {
        eventUtterDataContent = bodyParam.get("eventUtterData").toString().trim();
      }

      if (bodyParam.get("userId") == null || bodyParam.get("userId").toString().trim().equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "userId is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        userId = bodyParam.get("userId").toString().trim();
      }

      EventUtter findEventUtter = null;
      try {
        findEventUtter = eventUtterService.findEventUtterById(eventUtterId);
      } catch (Exception ex) {
        findEventUtter = eventUtterService.findEventUtterById(eventUtterId);
      }
      if (findEventUtter == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "eventUtter doesn't exist.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      EventUtterData eventUtterData = new EventUtterData();
      eventUtterData.setId(eventUtterDataId);
      eventUtterData.setEventUtterData(eventUtterDataContent);
      eventUtterData.setEventUtter(findEventUtter);

      eventUtterData.setCreatorId(userId);
      eventUtterData.setCreatedAt(currentTimestamp);
      eventUtterData.setUpdaterId(userId);
      eventUtterData.setUpdatedAt(currentTimestamp);

      try {
        eventUtterDataService.insertEventUtterData(eventUtterData);
      } catch (Exception ex) {
        eventUtterDataService.insertEventUtterData(eventUtterData);
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error(
          "POST /api/workspace/{}/entity/{}/task/{}/event/{}/eventcondition/{}/eventutter/{}/eventutterdata",
          workspaceId, modelId, taskId, eventId, eventConditionId, eventUtterId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^R")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}/event/{eventId}/eventcondition/{eventConditionId}/eventutter/{eventUtterId}/eventutterdata/list", method = RequestMethod.GET)
  public ResponseEntity<?> getEventUtterDataListByEventUtterId(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String taskId, @PathVariable String eventId,
      @PathVariable String eventConditionId, @PathVariable String eventUtterId) {
    logger.info(
        "GET /api/workspace/{}/entity/{}/task/{}/event/{}/eventcondition/{}/eventutter/{}/eventutterdata/list",
        workspaceId, modelId, taskId, eventId, eventConditionId, eventUtterId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      EventUtter findEventUtter = null;
      try {
        findEventUtter = eventUtterService.findEventUtterById(eventUtterId);
      } catch (Exception ex) {
        findEventUtter = eventUtterService.findEventUtterById(eventUtterId);
      }
      if (findEventUtter == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "eventUtter 데이터가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      List<EventUtterData> eventUtterDataList = new ArrayList<>();
      try {
        eventUtterDataList = eventUtterDataService.findEventUtterDataByEventUtter(findEventUtter);
      } catch (Exception ex) {
        eventUtterDataList = eventUtterDataService.findEventUtterDataByEventUtter(findEventUtter);
      }

      JSONObject wrappedEventUtterDataInfo = new JSONObject();
      JSONArray eventUtterDataInfoList = new JSONArray();

      for (int i = 0; i < eventUtterDataList.size(); i++) {
        JSONObject eventUtterDataInfo = new JSONObject();

        eventUtterDataInfo.put("id", eventUtterDataList.get(i).getId());
        eventUtterDataInfo.put("eventUtterData", eventUtterDataList.get(i).getEventUtterData());
        eventUtterDataInfo.put("eventUtterId", eventUtterDataList.get(i).getEventUtter().getId());

        eventUtterDataInfo.put("creatorId", eventUtterDataList.get(i).getCreatorId());
        eventUtterDataInfo.put("createdAt", eventUtterDataList.get(i).getCreatedAt());
        eventUtterDataInfo.put("updaterId", eventUtterDataList.get(i).getUpdaterId());
        eventUtterDataInfo.put("updatedAt", eventUtterDataList.get(i).getUpdatedAt());

        eventUtterDataInfoList.add(eventUtterDataInfo);
      }

      wrappedEventUtterDataInfo.put("eventUtterDataInfoList", eventUtterDataInfoList);

      result.put("data", wrappedEventUtterDataInfo);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error(
          "GET /api/workspace/{}/entity/{}/task/{}/event/{}/eventcondition/{}/eventutter/{}/eventutterdata/list",
          workspaceId, modelId, taskId, eventId, eventConditionId, eventUtterId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^R")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}/event/{eventId}/eventcondition/{eventConditionId}/eventutter/{eventUtterId}/eventutterdata/{eventUtterDataId}", method = RequestMethod.GET)
  public ResponseEntity<?> getEventUtterDataById(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String taskId, @PathVariable String eventId,
      @PathVariable String eventConditionId, @PathVariable String eventUtterId,
      @PathVariable String eventUtterDataId) {
    logger.info(
        "GET /api/workspace/{}/entity/{}/task/{}/event/{}/eventcondition/{}/eventutter/{}/eventutterdata/{}",
        workspaceId, modelId, taskId, eventId, eventConditionId, eventUtterId, eventUtterDataId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      EventUtterData eventUtterData = null;
      try {
        eventUtterData = eventUtterDataService.findEventUtterDataById(eventUtterDataId);
      } catch (Exception ex) {
        eventUtterData = eventUtterDataService.findEventUtterDataById(eventUtterDataId);
      }

      JSONObject wrappedEventUtterDataInfo = new JSONObject();
      JSONObject eventUtterDataInfo = new JSONObject();

      eventUtterDataInfo.put("id", eventUtterData.getId());
      eventUtterDataInfo.put("eventUtterData", eventUtterData.getEventUtterData());
      eventUtterDataInfo.put("eventUtterId", eventUtterData.getEventUtter().getId());

      eventUtterDataInfo.put("creatorId", eventUtterData.getCreatorId());
      eventUtterDataInfo.put("createdAt", eventUtterData.getCreatedAt());
      eventUtterDataInfo.put("updaterId", eventUtterData.getUpdaterId());
      eventUtterDataInfo.put("updatedAt", eventUtterData.getUpdatedAt());

      wrappedEventUtterDataInfo.put("eventUtterDataInfo", eventUtterDataInfo);

      result.put("data", wrappedEventUtterDataInfo);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error(
          "GET /api/workspace/{}/entity/{}/task/{}/event/{}/eventcondition/{}/eventutter/{}/eventutterdata/{}",
          workspaceId, modelId, taskId, eventId, eventConditionId, eventUtterId, eventUtterDataId,
          e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^U")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}/event/{eventId}/eventcondition/{eventConditionId}/eventutter/{eventUtterId}/eventutterdata/{eventUtterDataId}", method = RequestMethod.PUT)
  public ResponseEntity<?> updateEventUtterData(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String taskId, @PathVariable String eventId,
      @PathVariable String eventConditionId, @PathVariable String eventUtterId,
      @PathVariable String eventUtterDataId, @RequestBody JSONObject bodyParam) {
    logger.info(
        "PUT /api/workspace/{}/entity/{}/task/{}/event/{}/eventcondition/{}/eventutter/{}/eventutterdata/{}",
        workspaceId, modelId, taskId, eventId, eventConditionId, eventUtterId, eventUtterDataId,
        bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

      String eventUtterDataContent = null;
      String userId = null;

      if (bodyParam.get("eventUtterData") != null) {
        eventUtterDataContent = bodyParam.get("eventUtterData").toString().trim();
      }

      if (bodyParam.get("userId") == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "userId 가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        userId = bodyParam.get("userId").toString().trim();
      }

      EventUtterData findEventUtterData = null;
      try {
        findEventUtterData = eventUtterDataService.findEventUtterDataById(eventUtterDataId);
      } catch (Exception ex) {
        findEventUtterData = eventUtterDataService.findEventUtterDataById(eventUtterDataId);
      }
      if (findEventUtterData == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "eventUtterDataId 와 일치하는 eventUtterData 가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        if (eventUtterDataContent != null) {
          findEventUtterData.setEventUtterData(eventUtterDataContent);
        }

        findEventUtterData.setUpdaterId(userId);
        findEventUtterData.setUpdatedAt(currentTimestamp);
      }

      try {
        eventUtterDataService.updateEventUtterData(findEventUtterData);
      } catch (Exception ex) {
        eventUtterDataService.updateEventUtterData(findEventUtterData);
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error(
          "PUT /api/workspace/{}/entity/{}/task/{}/event/{}/eventcondition/{}/eventutter/{}/eventutterdata/{}",
          workspaceId, modelId, taskId, eventId, eventConditionId, eventUtterId, eventUtterDataId,
          e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^D")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}/event/{eventId}/eventcondition/{eventConditionId}/eventutter/{eventUtterId}/eventutterdata/{eventUtterDataId}", method = RequestMethod.DELETE)
  public ResponseEntity<?> deleteEventUtterDataById(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String taskId, @PathVariable String eventId,
      @PathVariable String eventConditionId, @PathVariable String eventUtterId,
      @PathVariable String eventUtterDataId) {
    logger.info(
        "DELETE /api/workspace/{}/entity/{}/task/{}/event/{}/eventcondition/{}/eventutter/{}/eventutterdata/{}",
        workspaceId, modelId, taskId, eventId, eventConditionId, eventUtterId, eventUtterDataId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      EventUtterData findEventUtterData = null;
      try {
        findEventUtterData = eventUtterDataService.findEventUtterDataById(eventUtterDataId);
      } catch (Exception ex) {
        findEventUtterData = eventUtterDataService.findEventUtterDataById(eventUtterDataId);
      }
      if (findEventUtterData == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "eventUtterDataId 와 일치하는 eventUtterData 가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        try {
          eventUtterDataService.deleteEventUtterDataById(eventUtterDataId);
        } catch (Exception ex) {
          eventUtterDataService.deleteEventUtterDataById(eventUtterDataId);
        }
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error(
          "DELETE /api/workspace/{}/entity/{}/task/{}/event/{}/eventcondition/{}/eventutter/{}/eventutterdata/{}",
          workspaceId, modelId, taskId, eventId, eventConditionId, eventUtterId, eventUtterDataId,
          e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
