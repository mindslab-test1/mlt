package ai.maum.mlt.sds.model.repository;

import ai.maum.mlt.sds.model.entity.Intent;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface IntentRepository extends JpaRepository<Intent, String> {

  List<Intent> findIntentsByWorkspaceIdOrderByIntentName(String workspaceId);

  Intent findIntentByWorkspaceIdAndId(String workspaceId, String intentId);

  void deleteIntentByWorkspaceIdAndId(String workspaceId, String intentId);
}
