package ai.maum.mlt.sds.model.service;

import ai.maum.mlt.sds.model.entity.StartEventUtter;
import ai.maum.mlt.sds.model.entity.StartEventUtterData;
import ai.maum.mlt.sds.model.repository.StartEventUtterDataRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StartEventUtterDataServiceImpl implements StartEventUtterDataService {

  @Autowired
  private StartEventUtterDataRepository startEventUtterDataRepository;

  @Override
  public StartEventUtterData insertStartEventUtterData(StartEventUtterData startEventUtterData) {
    return startEventUtterDataRepository.save(startEventUtterData);
  }

  @Override
  public StartEventUtterData findStartEventUtterDataById(String startEventUtterDataId) {
    return startEventUtterDataRepository.findStartEventUtterDataById(startEventUtterDataId);
  }

  @Override
  public List<StartEventUtterData> findAll() {
    return startEventUtterDataRepository.findAll();
  }

  @Override
  public List<StartEventUtterData> findStartEventUtterDataByStartEventUtter(
      StartEventUtter startEventUtter) {
    return startEventUtterDataRepository.findStartEventUtterDataByStartEventUtter(startEventUtter);
  }

  @Override
  public StartEventUtterData updateStartEventUtterData(StartEventUtterData startEventUtterData) {
    return startEventUtterDataRepository.save(startEventUtterData);
  }

  @Override
  public void deleteStartEventUtterDataById(String startEventUtterDataId) {
    startEventUtterDataRepository.deleteStartEventUtterDataById(startEventUtterDataId);
  }
}
