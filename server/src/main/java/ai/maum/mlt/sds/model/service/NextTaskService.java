package ai.maum.mlt.sds.model.service;

import ai.maum.mlt.sds.model.entity.NextTask;
import ai.maum.mlt.sds.model.entity.Task;
import java.util.List;
import javax.transaction.Transactional;

@Transactional
public interface NextTaskService {

  NextTask insertNextTask(NextTask nextTask);

  NextTask findNextTaskById(String id);

  List<NextTask> findAll();

  List<NextTask> findNextTasksByTask(Task task);

  NextTask updateNextTask(NextTask nextTask);

  void deleteNextTaskById(String id);
}
