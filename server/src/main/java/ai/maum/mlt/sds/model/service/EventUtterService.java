package ai.maum.mlt.sds.model.service;

import ai.maum.mlt.sds.model.entity.EventCondition;
import ai.maum.mlt.sds.model.entity.EventUtter;
import java.util.List;
import javax.transaction.Transactional;

@Transactional
public interface EventUtterService {

  EventUtter insertEventUtter(EventUtter eventUtter);

  EventUtter findEventUtterById(String eventUtterId);

  List<EventUtter> findAll();

  List<EventUtter> findEventUttersByEventCondition(EventCondition eventCondition);

  EventUtter updateEventUtter(EventUtter eventUtter);

  void deleteEventUtterById(String eventUtterId);
}
