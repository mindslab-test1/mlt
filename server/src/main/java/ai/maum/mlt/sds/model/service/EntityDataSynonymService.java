package ai.maum.mlt.sds.model.service;

import ai.maum.mlt.sds.model.entity.EntityData;
import ai.maum.mlt.sds.model.entity.EntityDataSynonym;
import java.util.List;
import javax.transaction.Transactional;

@Transactional
public interface EntityDataSynonymService {

  EntityDataSynonym insertEntityDataSynonym(EntityDataSynonym entityDataSynonym);

  EntityDataSynonym findEntityDataSynonymById(String entityDataSynonymId);

  List<EntityDataSynonym> findAll();

  List<EntityDataSynonym> findEntityDataSynonymsByEntityData(EntityData entityData);

  EntityDataSynonym updateEntityDataSynonym(EntityDataSynonym entityDataSynonym);

  void deleteEntityDataSynonymById(String entityDataSynonymId);
}
