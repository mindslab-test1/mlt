package ai.maum.mlt.sds.model.service;

import ai.maum.mlt.sds.model.entity.Event;
import ai.maum.mlt.sds.model.entity.Task;
import ai.maum.mlt.sds.model.repository.EventRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EventServiceImpl implements EventService {

  @Autowired
  private EventRepository eventRepository;

  @Override
  public Event insertEvent(Event event) {
    return eventRepository.save(event);
  }

  @Override
  public Event findEventById(String eventId) {
    return eventRepository.findEventById(eventId);
  }

  @Override
  public List<Event> findAll() {
    return eventRepository.findAll();
  }

  @Override
  public List<Event> findEventsByTask(Task task) {
    return eventRepository.findEventsByTask(task);
  }

  @Override
  public Event updateEvent(Event event) {
    return eventRepository.save(event);
  }

  @Override
  public void deleteEventById(String eventId) {
    eventRepository.deleteEventById(eventId);

    return;
  }
}
