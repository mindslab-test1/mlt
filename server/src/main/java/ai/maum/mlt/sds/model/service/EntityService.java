package ai.maum.mlt.sds.model.service;

import ai.maum.mlt.sds.model.entity.Entity;
import java.util.List;
import javax.transaction.Transactional;

@Transactional
public interface EntityService {

  Entity insertEntity(Entity entity);

  List<Entity> findEntitiesByWorkspaceId(String workspaceId);

  Entity findEntityByWorkspaceIdAndId(String workspaceId, String entityId);

  Entity updateEntity(Entity entity);

  void deleteEntityByWorkspaceIdAndId(String workspaceId, String entityId);
}
