package ai.maum.mlt.sds.model.service;

import ai.maum.mlt.sds.model.entity.Entity;
import ai.maum.mlt.sds.model.entity.EntityData;
import ai.maum.mlt.sds.model.repository.EntityDataRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EntityDataServiceImpl implements EntityDataService {

  @Autowired
  private EntityDataRepository entityDataRepository;

  @Override
  public EntityData insertEntityData(EntityData entityData) {
    return entityDataRepository.save(entityData);
  }

  @Override
  public EntityData findEntityDataById(String entityDataId) {
    return entityDataRepository.findEntityDataById(entityDataId);
  }

  @Override
  public List<EntityData> findAll() {
    return entityDataRepository.findAll();
  }

  @Override
  public List<EntityData> findEntityDataByEntity(Entity entity) {
    return entityDataRepository.findEntityDataByEntity(entity);
  }

  @Override
  public EntityData updateEntityData(EntityData entityData) {
    return entityDataRepository.save(entityData);
  }

  @Override
  public void deleteEntityDataById(String entityDataId) {
    entityDataRepository.deleteEntityDataById(entityDataId);

    return;
  }
}
