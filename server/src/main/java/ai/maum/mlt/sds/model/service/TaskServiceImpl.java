package ai.maum.mlt.sds.model.service;

import ai.maum.mlt.sds.model.entity.Model;
import ai.maum.mlt.sds.model.entity.Task;
import ai.maum.mlt.sds.model.repository.TaskRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TaskServiceImpl implements TaskService {

  @Autowired
  private TaskRepository taskRepository;

  @Override
  public Task insertTask(Task task) {
    return taskRepository.save(task);
  }

  @Override
  public Task findTaskById(String taskId) {
    return taskRepository.findTaskById(taskId);
  }

  @Override
  public List<Task> findAll() {
    return taskRepository.findAll();
  }

  @Override
  public List<Task> findTasksByModel(Model model) {
    return taskRepository.findTasksByModel(model);
  }

  @Override
  public Task updateTask(Task task) {
    return taskRepository.save(task);
  }

  @Override
  public void deleteTaskById(String taskId) {
    taskRepository.deleteTaskById(taskId);

    return;
  }
}
