package ai.maum.mlt.sds.model.service;

import com.google.protobuf.ByteString;
import com.google.protobuf.Empty;
import com.google.protobuf.util.JsonFormat;
import io.grpc.Channel;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Metadata;
import io.grpc.stub.MetadataUtils;
import io.grpc.stub.StreamObserver;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import maum.brain.sds.*;
import maum.common.LangOuterClass;
import maum.common.Types;
import net.devh.springboot.autoconfigure.grpc.client.GrpcClient;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class TalkGrpcService {

  static final Logger logger = LoggerFactory.getLogger(TalkGrpcService.class);

  @GrpcClient("sdstalk")
  private Channel talkServerChannel;

  public JSONObject resolverPing(String modelName) {
    try {
      Resolver.ModelGroup.Builder modelGroup = Resolver.ModelGroup.newBuilder();
      modelGroup.setName(modelName);
      modelGroup.setLang(LangOuterClass.LangCode.kor);
      modelGroup.setIsExternal(false);
      modelGroup.setIsEducation(false);

      SdsServiceResolverGrpc.SdsServiceResolverBlockingStub stub = SdsServiceResolverGrpc
          .newBlockingStub(talkServerChannel);
      Resolver.ModelGroup buildModelGroup = modelGroup.build();
      Resolver.ServerStatus serverStatus = stub.ping(buildModelGroup);

      Metadata fixedHeaders = new Metadata();

      Metadata.Key<String> keyLang = Metadata.Key.of("in.lang", Metadata.ASCII_STRING_MARSHALLER);
      Metadata.Key<String> keyIsExternal = Metadata.Key
          .of("in.is_external", Metadata.ASCII_STRING_MARSHALLER);
      Metadata.Key<String> keyName = Metadata.Key.of("in.name", Metadata.ASCII_STRING_MARSHALLER);

      fixedHeaders.put(keyLang, "kor");
      fixedHeaders.put(keyIsExternal, "false");
      fixedHeaders.put(keyName, "testmodel_1111");

      stub = MetadataUtils.attachHeaders(stub, fixedHeaders);

      String jsonString = JsonFormat.printer().print(serverStatus);
      JSONParser parser = new JSONParser();
      Object obj = parser.parse(jsonString);

      JSONObject result = new JSONObject();
      result.put("resultInfo", obj);

      return result;
    } catch (Exception ex) {
      JSONObject error = new JSONObject();
      error.put("error", ex.toString());

      return error;
    }
  }

  public JSONObject resolverUpdateModel(String modelId, String modelName) {
    try {
      Resolver.ModelGroup.Builder modelGroup = Resolver.ModelGroup.newBuilder();
      modelGroup.setName(modelName);
      modelGroup.setLang(LangOuterClass.LangCode.kor);
      modelGroup.setIsExternal(false);
      modelGroup.setIsEducation(false);

      SdsServiceResolverGrpc.SdsServiceResolverStub stub = SdsServiceResolverGrpc
          .newStub(talkServerChannel);

      Metadata fixedHeaders = new Metadata();
      Metadata.Key<String> keyLang = Metadata.Key.of("in.lang", Metadata.ASCII_STRING_MARSHALLER);
      Metadata.Key<String> keyIsExternal = Metadata.Key
          .of("in.is_external", Metadata.ASCII_STRING_MARSHALLER);
      Metadata.Key<String> keyName = Metadata.Key.of("in.name", Metadata.ASCII_STRING_MARSHALLER);

      fixedHeaders.put(keyLang, "kor");
      fixedHeaders.put(keyIsExternal, "false");
      fixedHeaders.put(keyName, modelName + "_" + modelId);

      stub = MetadataUtils.attachHeaders(stub, fixedHeaders);

      StreamObserver<Empty> response = new StreamObserver<Empty>() {
        @Override
        public void onNext(Empty value) {
          System.out.println("onNext");
        }

        @Override
        public void onError(Throwable t) {
          System.out.println("onError");
        }

        @Override
        public void onCompleted() {
          System.out.println("onCompleted");
        }
      };

      StreamObserver<Types.FilePart> request = stub.updateModel(response);

      try {
        String fileName = "/home/minds/maum/trained/sds-entity/ki/" + modelName + ".tar.gz";
//                String fileName = "./" + modelName + ".tar.gz";
        File file = new File(fileName);
        if (file.exists() == false) {
          JSONObject error = new JSONObject();
          error.put("error", "File does not exist");

          return error;
        }

        try {
          BufferedInputStream bufferedInputStream = new BufferedInputStream(
              new FileInputStream(file));
          byte[] buffer = new byte[1024 * 1024];

          int tmp = 0;
          int size = 0;
          while ((tmp = bufferedInputStream.read(buffer)) > 0) {
            size += tmp;
            ByteString byteString = ByteString.copyFrom(buffer);
            Types.FilePart filePart = Types.FilePart.newBuilder().setPart(byteString).build();
            request.onNext(filePart);
          }
        } catch (FileNotFoundException e) {
          logger.error("resolverUpdateModel e : " , e);
        } catch (IOException e) {
          logger.error("resolverUpdateModel e : " , e);
        }
      } catch (RuntimeException e) {
        request.onError(e);
        throw e;
      }

      request.onCompleted();

      JSONObject result = new JSONObject();
      result.put("resultInfo", "Success");

      return result;
    } catch (Exception ex) {
      JSONObject error = new JSONObject();
      error.put("error", ex.toString());

      return error;
    }
  }

  public JSONObject resolverLinkModel(String modelId, String modelName) {
    try {
      Resolver.ModelParam.Builder modelParam = Resolver.ModelParam.newBuilder();
      modelParam.setGroupName("sds_train");
      modelParam.setModelName(modelName + "_" + modelId);
      modelParam.setLang(LangOuterClass.LangCode.valueOf("kor"));
      modelParam.setIsExternal(false);
      modelParam.setIsEducation(false);

      SdsServiceResolverGrpc.SdsServiceResolverBlockingStub stub = SdsServiceResolverGrpc
          .newBlockingStub(talkServerChannel);
      Resolver.ModelParam buildModelParam = modelParam.build();

      stub.linkModel(buildModelParam);

      JSONObject result = new JSONObject();
      result.put("resultInfo", "Success");

      return result;
    } catch (Exception ex) {
      JSONObject error = new JSONObject();
      error.put("error", ex.toString());

      return error;
    }
  }

  //    public JSONObject resolverFind(String modelId, String modelName) {
  public JSONObject resolverFind(String groupName) {
    try {
      Resolver.ModelGroup.Builder modelGroup = Resolver.ModelGroup.newBuilder();
//            modelGroup.setName(modelName + "_" + modelId);
      modelGroup.setName(groupName);
      modelGroup.setLang(LangOuterClass.LangCode.valueOf("kor"));
      modelGroup.setIsExternal(false);
      modelGroup.setIsEducation(false);

      SdsServiceResolverGrpc.SdsServiceResolverBlockingStub stub = SdsServiceResolverGrpc
          .newBlockingStub(talkServerChannel);
      Resolver.ModelGroup buildModelGroup = modelGroup.build();

      Resolver.ServerStatus serverStatus = stub.find(buildModelGroup);

      String jsonString = JsonFormat.printer().print(serverStatus);
      JSONParser parser = new JSONParser();
      Object obj = parser.parse(jsonString);

      JSONObject result = new JSONObject();
      result.put("resultInfo", obj);

      return result;
    } catch (Exception ex) {
      JSONObject error = new JSONObject();
      error.put("error", ex.toString());

      return error;
    }
  }

  public JSONObject sdsOpen(String host, int port, String modelId, String modelName,
      long sessionKey) {
    try {
      Sds.DialogueParam.Builder dialogueParam = Sds.DialogueParam.newBuilder();
//            dialogueParam.setModel(modelName + "_" + modelId);
      dialogueParam.setModel(modelName);
      dialogueParam.setSessionKey(sessionKey);
      dialogueParam.setUserInitiative(false);

      ManagedChannel channel = ManagedChannelBuilder.forAddress(host, port).usePlaintext(true)
          .build();

      SpokenDialogServiceInternalGrpc.SpokenDialogServiceInternalBlockingStub stub = SpokenDialogServiceInternalGrpc
          .newBlockingStub(channel);
      Sds.DialogueParam buildDialogueParam = dialogueParam.build();

      Sds.OpenResult openResult = stub.open(buildDialogueParam);

      String jsonString = JsonFormat.printer().print(openResult);
      JSONParser parser = new JSONParser();
      Object obj = parser.parse(jsonString);

      JSONObject result = new JSONObject();
      result.put("resultInfo", obj);

      return result;
    } catch (Exception ex) {
      JSONObject error = new JSONObject();
      error.put("error", ex.toString());

      return error;
    }
  }

  public JSONObject sdsDialog(String host, int port, String modelId, String modelName,
      long sessionKey, String utter) {
    try {
      Sds.SdsQuery.Builder sdsQuery = Sds.SdsQuery.newBuilder();
//            sdsQuery.setModel(modelName + "_" + modelId);
      sdsQuery.setModel(modelName);
      sdsQuery.setSessionKey(sessionKey);
      sdsQuery.setUtter(utter);

      ManagedChannel channel = ManagedChannelBuilder.forAddress(host, port).usePlaintext(true)
          .build();

      SpokenDialogServiceInternalGrpc.SpokenDialogServiceInternalBlockingStub stub = SpokenDialogServiceInternalGrpc
          .newBlockingStub(channel);
      Sds.SdsQuery buildSdsQuery = sdsQuery.build();

      Sds.SdsResponse dialogResult = stub.dialog(buildSdsQuery);

      String jsonString = JsonFormat.printer().print(dialogResult);
      JSONParser parser = new JSONParser();
      Object obj = parser.parse(jsonString);

      JSONObject result = new JSONObject();
      result.put("resultInfo", obj);

      return result;
    } catch (Exception ex) {
      JSONObject error = new JSONObject();
      error.put("error", ex.toString());

      return error;
    }
  }

  public JSONObject sdsClose(String host, int port, String modelId, String modelName,
      long sessionKey) {
    try {
      Sds.DialogueParam.Builder dialogueParam = Sds.DialogueParam.newBuilder();
//            dialogueParam.setModel(modelName + "_" + modelId);
      dialogueParam.setModel(modelName);
      dialogueParam.setSessionKey(sessionKey);
      dialogueParam.setUserInitiative(false);

      ManagedChannel channel = ManagedChannelBuilder.forAddress(host, port).usePlaintext(true)
          .build();

      SpokenDialogServiceInternalGrpc.SpokenDialogServiceInternalBlockingStub stub = SpokenDialogServiceInternalGrpc
          .newBlockingStub(channel);
      Sds.DialogueParam buildDialogueParam = dialogueParam.build();

      stub.close(buildDialogueParam);

      JSONObject result = new JSONObject();
      result.put("resultInfo", "Success");

      return result;
    } catch (Exception ex) {
      JSONObject error = new JSONObject();
      error.put("error", ex.toString());

      return error;
    }
  }
}
