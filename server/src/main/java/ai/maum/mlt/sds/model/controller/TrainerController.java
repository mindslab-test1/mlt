package ai.maum.mlt.sds.model.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.sds.model.service.TrainerGrpcService;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/sds/model")
public class TrainerController {

  static final Logger logger = LoggerFactory.getLogger(TrainerController.class);

  @Autowired
  private TrainerGrpcService trainerGrpcService;

  @UriRoleDesc(role = "sds/model^X")
  @RequestMapping(value = "/trainer/open/{modelId}", method = RequestMethod.GET)
  public ResponseEntity<?> callTrainerOpen(@PathVariable String modelId) {
    logger.info("GET /trainer/open/{}", modelId);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      JSONObject data = trainerGrpcService.trainerOpen(modelId);
      result.put("data", data);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception ex) {
      logger.error("POST /api/trainer/open/{}", modelId, ex);

      result.put("statusCode", 500);
      result.put("statusMessage", ex.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^X")
  @RequestMapping(value = "/trainer/progress/{trainKey}", method = RequestMethod.GET)
  public ResponseEntity<?> callTrainerGetProgress(@PathVariable String trainKey) {
    logger.info("GET /trainer/progress/{}", trainKey);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      JSONObject data = trainerGrpcService.trainerGetProgress(trainKey);
      result.put("data", data);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception ex) {
      logger.error("POST /api/trainer/progress/{}", trainKey, ex);

      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^X")
  @RequestMapping(value = "/trainer/getbinary/{modelName}/{trainKey}", method = RequestMethod.GET)
  public ResponseEntity<?> callTrainerGetBinary(@PathVariable String modelName,
      @PathVariable String trainKey) {
    logger.info("GET /trainer/getbinary/{}/{}", modelName, trainKey);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      JSONObject data = trainerGrpcService.trainerGetBinary(modelName, trainKey);
      result.put("data", data);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception ex) {
      logger.error("GET /trainer/getbinary/{}/{}", modelName, trainKey, ex);

      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^X")
  @RequestMapping(value = "/trainer/close/{trainKey}", method = RequestMethod.GET)
  public ResponseEntity<?> callTrainerClose(@PathVariable String trainKey) {
    logger.info("GET /trainer/close/{}", trainKey);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      JSONObject data = trainerGrpcService.trainerClose(trainKey);
      result.put("data", data);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception ex) {
      logger.error("GET /trainer/close/{}", trainKey, ex);

      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^X")
  @RequestMapping(value = "/trainer/stop/{trainKey}", method = RequestMethod.GET)
  public ResponseEntity<?> callTrainerStop(@PathVariable String trainKey) {
    logger.info("GET /trainer/stop/{}", trainKey);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      JSONObject data = trainerGrpcService.trainerStop(trainKey);
      result.put("data", data);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception ex) {
      logger.error("GET /trainer/stop/{}", trainKey, ex);

      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^X")
  @RequestMapping(value = "/trainer/allprogress", method = RequestMethod.GET)
  public ResponseEntity<?> callTrainerClose() {
    logger.info("GET /trainer/allprogress");

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      JSONObject data = trainerGrpcService.trainerGetAllProgress();
      result.put("data", data);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception ex) {
      logger.error("POST /api/trainer/allprogress", ex);

      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^X")
  @RequestMapping(value = "/trainer/removebinary/{trainKey}", method = RequestMethod.GET)
  public ResponseEntity<?> callTrainerRemoveBinary(@PathVariable String trainKey) {
    logger.info("GET /trainer/removebinary/{}", trainKey);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      JSONObject data = trainerGrpcService.trainerRemoveBinary(trainKey);
      result.put("data", data);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception ex) {
      logger.error("GET /trainer/removebinary/{}", trainKey, ex);

      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
