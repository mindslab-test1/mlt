package ai.maum.mlt.sds.scenario.repository;

import ai.maum.mlt.sds.scenario.entity.ScenarioEntity;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ScenarioRepository extends JpaRepository<ScenarioEntity, String> {

  void deleteById(String id);

  Optional<ScenarioEntity> findById(String id);

  Optional<ScenarioEntity> findBySkillName(String skillName);

  ScenarioEntity findAllByWorkspaceIdAndId(String workspaceId, String id);

  @Query(value = "SELECT s "
      + "         FROM ScenarioEntity s "
      + "         WHERE s.workspaceId =:workspaceId "
      + "         AND (s.skillId like CONCAT('%',:skillId,'%') OR :skillId IS NULL)"
      + "         AND (UPPER(s.skillName) like CONCAT('%',:skillName,'%') OR :skillName IS NULL)"
      + "         AND (UPPER(s.group1) like CONCAT('%',:group1,'%') OR :group1 IS NULL)"
      + "         AND (UPPER(s.group2) like CONCAT('%',:group2,'%') OR :group2 IS NULL)"
      + "         AND (UPPER(s.group3) like CONCAT('%',:group3,'%') OR :group3 IS NULL)"
      + "         AND (UPPER(s.group4) like CONCAT('%',:group4,'%') OR :group4 IS NULL)"
      + "         AND (UPPER(s.status) in (SELECT c.id FROM CodeEntity c WHERE c.groupCode = 'SCENARIO_STATUS'"
      + "              AND UPPER(c.name) LIKE CONCAT('%', :status ,'%')) OR :status IS NULL)"
      + "         AND (UPPER(s.updaterId) in (SELECT u.id FROM UserEntity u WHERE"
      + "              UPPER(u.username) LIKE CONCAT('%', :updaterId ,'%')) OR :updaterId IS NULL)"
      + "         AND (UPPER(s.searchOption)like CONCAT('%',:searchOption,'%') OR :searchOption IS NULL)"
  )
  Page<ScenarioEntity> findAllByWorkspaceId(
      @Param("workspaceId") String workspaceId,
      @Param("skillId") String skillId,
      @Param("skillName") String skillName,
      @Param("group1") String group1,
      @Param("group2") String group2,
      @Param("group3") String group3,
      @Param("group4") String group4,
      @Param("status") String status,
      @Param("updaterId") String updaterId,
      @Param("searchOption") String searchOption,
      Pageable pageable
  );

  @Query(value = "SELECT s "
          + "         FROM ScenarioEntity s "
          + "         WHERE s.workspaceId =:workspaceId "
          + "         AND (s.skillId IN (:skillList))"
  )
  Page<ScenarioEntity> findAllByWorkspaceId(
          @Param("workspaceId") String workspaceId,
          @Param("skillList") List<String> skillList,
          Pageable pageable
  );
//  + "         AND (:skillList like CONCAT('%|',s.skillId,'|%') OR :skillList IS NULL)"
}
