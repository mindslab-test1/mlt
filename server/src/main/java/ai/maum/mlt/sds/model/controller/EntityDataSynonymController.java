package ai.maum.mlt.sds.model.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.sds.model.entity.EntityData;
import ai.maum.mlt.sds.model.entity.EntityDataSynonym;
import ai.maum.mlt.sds.model.service.EntityDataService;
import ai.maum.mlt.sds.model.service.EntityDataSynonymService;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/sds/model")
public class EntityDataSynonymController {

  static final Logger logger = LoggerFactory.getLogger(EntityDataSynonymController.class);

  @Autowired
  private EntityDataService entityDataService;

  @Autowired
  private EntityDataSynonymService entityDataSynonymService;

  @UriRoleDesc(role = "sds/model^C")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/entity/{entityId}/entitydata/{entityDataId}/entitydatasynonym", method = RequestMethod.POST)
  public ResponseEntity<?> insertEntityDataSynonym(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String entityId,
      @PathVariable String entityDataId, @RequestBody JSONObject bodyParam) {
    logger.info("POST /api/workspace/{}/entity/{}/entity/{}/entityData/{}/entitydatasynonym",
        workspaceId, modelId, entityId, entityDataId, bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

      String entityDataSynonymId = null;
      String synonym = "";
      String userId = null;

      if (bodyParam.get("id") == null || bodyParam.get("id").toString().trim().equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "id is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        entityDataSynonymId = bodyParam.get("id").toString().trim();

        EntityDataSynonym findEntityDataSynonym = null;
        try {
          findEntityDataSynonym = entityDataSynonymService
              .findEntityDataSynonymById(entityDataSynonymId);
        } catch (Exception ex) {
          findEntityDataSynonym = entityDataSynonymService
              .findEntityDataSynonymById(entityDataSynonymId);
        }
        if (findEntityDataSynonym != null) {
          result.put("statusCode", 400);
          result.put("statusMessage", "id is already exist.");

          return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }
      }

      if (bodyParam.get("entityDataSynonym") != null) {
        synonym = bodyParam.get("entityDataSynonym").toString().trim();
      }

      if (bodyParam.get("userId") == null || bodyParam.get("userId").toString().trim().equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "userId is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        userId = bodyParam.get("userId").toString().trim();
      }

      EntityData findEntityData = null;
      try {
        findEntityData = entityDataService.findEntityDataById(entityDataId);
      } catch (Exception ex) {
        findEntityData = entityDataService.findEntityDataById(entityDataId);
      }
      if (findEntityData == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "EntityData doesn't exist.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      EntityDataSynonym entityDataSynonym = new EntityDataSynonym();
      entityDataSynonym.setId(entityDataSynonymId);
      entityDataSynonym.setEntityDataSynonym(synonym);
      entityDataSynonym.setEntityData(findEntityData);
      entityDataSynonym.setCreatorId(userId);
      entityDataSynonym.setUpdaterId(userId);
      entityDataSynonym.setCreatedAt(currentTimestamp);
      entityDataSynonym.setUpdatedAt(currentTimestamp);

      try {
        entityDataSynonymService.insertEntityDataSynonym(entityDataSynonym);
      } catch (Exception ex) {
        entityDataSynonymService.insertEntityDataSynonym(entityDataSynonym);
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("POST /api/workspace/{}/entity/{}/entity/{}/entityData/{}/entitydatasynonym",
          workspaceId, modelId, entityId, entityDataId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^R")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/entity/{entityId}/entitydata/{entityDataId}/entitydatasynonym/list", method = RequestMethod.GET)
  public ResponseEntity<?> getEntityDataSynonymListByEntityDataId(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String entityId,
      @PathVariable String entityDataId) {
    logger.info("GET /api/workspace/{}/entity/{}/entity/{}/entityData/{}/entitydatasynonym/list",
        workspaceId, modelId, entityId, entityDataId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      EntityData findEntityData = null;
      try {
        findEntityData = entityDataService.findEntityDataById(entityDataId);
      } catch (Exception ex) {
        findEntityData = entityDataService.findEntityDataById(entityDataId);
      }

      List<EntityDataSynonym> entityDataSynonymList = new ArrayList<>();
      try {
        entityDataSynonymList = entityDataSynonymService
            .findEntityDataSynonymsByEntityData(findEntityData);
      } catch (Exception ex) {
        entityDataSynonymList = entityDataSynonymService
            .findEntityDataSynonymsByEntityData(findEntityData);
      }

      JSONObject wrappedEntityDataSynonymInfo = new JSONObject();
      JSONArray entityDataSynonymInfoList = new JSONArray();

      for (int i = 0; i < entityDataSynonymList.size(); i++) {
        JSONObject entityDataSynonymInfo = new JSONObject();

        entityDataSynonymInfo.put("id", entityDataSynonymList.get(i).getId());
        entityDataSynonymInfo
            .put("entityDataSynonym", entityDataSynonymList.get(i).getEntityDataSynonym());
        entityDataSynonymInfo
            .put("entityDataId", entityDataSynonymList.get(i).getEntityData().getId());

        entityDataSynonymInfo.put("creatorId", entityDataSynonymList.get(i).getCreatorId());
        entityDataSynonymInfo.put("createdAt", entityDataSynonymList.get(i).getCreatedAt());
        entityDataSynonymInfo.put("updaterId", entityDataSynonymList.get(i).getUpdaterId());
        entityDataSynonymInfo.put("updatedAt", entityDataSynonymList.get(i).getUpdatedAt());

        entityDataSynonymInfoList.add(entityDataSynonymInfo);
      }

      wrappedEntityDataSynonymInfo.put("entityDataSynonymInfoList", entityDataSynonymInfoList);

      result.put("data", wrappedEntityDataSynonymInfo);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("GET /api/workspace/{}/entity/{}/entity/{}/entityData/{}/entitydatasynonym/list",
          workspaceId, modelId, entityId, entityDataId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^R")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/entity/{entityId}/entitydata/{entityDataId}/entitydatasynonym/{entityDataSynonymId}", method = RequestMethod.GET)
  public ResponseEntity<?> getEntityDataSynonymByEntityDataId(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String entityId,
      @PathVariable String entityDataId, @PathVariable String entityDataSynonymId) {
    logger.info("GET /api/workspace/{}/entity/{}/entity/{}/entityData/{}/entitydatasynonym/{}",
        workspaceId, modelId, entityId, entityDataId, entityDataSynonymId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      EntityData entityData = null;
      try {
        entityData = entityDataService.findEntityDataById(entityDataId);
      } catch (Exception ex) {
        entityData = entityDataService.findEntityDataById(entityDataId);
      }
      if (entityData == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "entityData 가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      EntityDataSynonym entityDataSynonym = null;
      try {
        entityDataSynonym = entityDataSynonymService.findEntityDataSynonymById(entityDataSynonymId);
      } catch (Exception ex) {
        entityDataSynonym = entityDataSynonymService.findEntityDataSynonymById(entityDataSynonymId);
      }

      JSONObject wrappedEntityDataSynonymInfo = new JSONObject();
      JSONObject entityDataSynonymInfo = new JSONObject();

      entityDataSynonymInfo.put("id", entityDataSynonym.getId());
      entityDataSynonymInfo.put("entityDataSynonym", entityDataSynonym.getEntityDataSynonym());
      entityDataSynonymInfo.put("entityDataId", entityDataSynonym.getEntityData().getId());

      entityDataSynonymInfo.put("creatorId", entityDataSynonym.getCreatorId());
      entityDataSynonymInfo.put("createdAt", entityDataSynonym.getCreatedAt());
      entityDataSynonymInfo.put("updaterId", entityDataSynonym.getUpdaterId());
      entityDataSynonymInfo.put("updatedAt", entityDataSynonym.getUpdatedAt());

      wrappedEntityDataSynonymInfo.put("entityDataSynonymInfo", entityDataSynonymInfo);

      result.put("data", wrappedEntityDataSynonymInfo);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("GET /api/workspace/{}/entity/{}/entity/{}/entityData/{}/entitydatasynonym/{}",
          workspaceId, modelId, entityId, entityDataId, entityDataSynonymId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^U")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/entity/{entityId}/entitydata/{entityDataId}/entitydatasynonym/{entityDataSynonymId}", method = RequestMethod.PUT)
  public ResponseEntity<?> updateEntityDataSynonym(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String entityId,
      @PathVariable String entityDataId, @PathVariable String entityDataSynonymId,
      @RequestBody JSONObject bodyParam) {
    logger.info("PUT /api/workspace/{}/entity/{}/entity/{}/entityData/{}/entitydatasynonym/{}",
        workspaceId, modelId, entityId, entityDataId, entityDataSynonymId, bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

      String entityDataSynonym = "";
      String userId = null;

      if (bodyParam.get("entityDataSynonym") != null) {
        entityDataSynonym = bodyParam.get("entityDataSynonym").toString().trim();
      }

      if (bodyParam.get("userId") == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "userId 가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        userId = bodyParam.get("userId").toString().trim();
      }

      EntityDataSynonym findEntityDataSynonym = null;
      try {
        findEntityDataSynonym = entityDataSynonymService
            .findEntityDataSynonymById(entityDataSynonymId);
      } catch (Exception ex) {
        findEntityDataSynonym = entityDataSynonymService
            .findEntityDataSynonymById(entityDataSynonymId);
      }
      if (findEntityDataSynonym == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "entityDataSynonymId 와 일치하는 entityDataSynonym 이 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        // 입력한 entityDataSynonym 값이 저장된 값과 동일하면 return.
        if (findEntityDataSynonym.getEntityDataSynonym().equals(entityDataSynonym)) {
          return new ResponseEntity<>(result, HttpStatus.OK);
        } else {
          findEntityDataSynonym.setEntityDataSynonym(entityDataSynonym);
        }

        findEntityDataSynonym.setUpdaterId(userId);
        findEntityDataSynonym.setUpdatedAt(currentTimestamp);
      }

      try {
        entityDataSynonymService.updateEntityDataSynonym(findEntityDataSynonym);
      } catch (Exception ex) {
        entityDataSynonymService.updateEntityDataSynonym(findEntityDataSynonym);
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("PUT /api/workspace/{}/entity/{}/entity/{}/entityData/{}/entitydatasynonym/{}",
          workspaceId, modelId, entityId, entityDataId, entityDataSynonymId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^D")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/entity/{entityId}/entitydata/{entityDataId}/entitydatasynonym/{entityDataSynonymId}", method = RequestMethod.DELETE)
  public ResponseEntity<?> deleteEntityDataSynonymById(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String entityId,
      @PathVariable String entityDataId, @PathVariable String entityDataSynonymId) {
    logger.info("DELETE /api/workspace/{}/entity/{}/entity/{}/entitydata/{}/entitydatasynonym/{}",
        workspaceId, modelId, entityId, entityDataId, entityDataSynonymId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      EntityDataSynonym findEntityDataSynonym = null;
      try {
        findEntityDataSynonym = entityDataSynonymService
            .findEntityDataSynonymById(entityDataSynonymId);
      } catch (Exception ex) {
        findEntityDataSynonym = entityDataSynonymService
            .findEntityDataSynonymById(entityDataSynonymId);
      }
      if (findEntityDataSynonym == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "entityDataSynonymId 와 일치하는 entityDataSynonym 이 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        try {
          entityDataSynonymService.deleteEntityDataSynonymById(entityDataSynonymId);
        } catch (Exception ex) {
          entityDataSynonymService.deleteEntityDataSynonymById(entityDataSynonymId);
        }
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger
          .error("DELETE /api/workspace/{}/entity/{}/entity/{}/entitydata/{}/entitydatasynonym/{}",
              workspaceId, modelId, entityId, entityDataId, entityDataSynonymId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
