package ai.maum.mlt.sds.model.repository;

import ai.maum.mlt.sds.model.entity.EventUtter;
import ai.maum.mlt.sds.model.entity.EventUtterData;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface EventUtterDataRepository extends JpaRepository<EventUtterData, String> {

  EventUtterData findEventUtterDataById(String eventUtterDataId);

  List<EventUtterData> findAll();

  List<EventUtterData> findEventUtterDataByEventUtter(EventUtter eventUtter);

  void deleteEventUtterDataById(String eventUtterDataId);
}
