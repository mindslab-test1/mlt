package ai.maum.mlt.sds.model.service;

import ai.maum.mlt.sds.model.entity.Intent;
import ai.maum.mlt.sds.model.entity.IntentUtter;
import java.util.List;
import javax.transaction.Transactional;

@Transactional
public interface IntentUtterService {

  IntentUtter insertIntentUtter(IntentUtter intentUtter);

  IntentUtter findIntentUtterById(String intentUtterId);

  List<IntentUtter> findAll();

  List<IntentUtter> findIntentUttersByIntent(Intent intent);

  IntentUtter updateIntentUtter(IntentUtter intentUtter);

  void deleteIntentUtterById(String intentUtterId);
}
