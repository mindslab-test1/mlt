package ai.maum.mlt.sds.model.service;

import ai.maum.mlt.sds.model.entity.NextTask;
import ai.maum.mlt.sds.model.entity.Task;
import ai.maum.mlt.sds.model.repository.NextTaskRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NextTaskServiceImpl implements NextTaskService {

  @Autowired
  private NextTaskRepository nextTaskRepository;

  @Override
  public NextTask insertNextTask(NextTask nextTask) {
    return nextTaskRepository.save(nextTask);
  }

  @Override
  public NextTask findNextTaskById(String id) {
    return nextTaskRepository.findNextTaskById(id);
  }

  @Override
  public List<NextTask> findAll() {
    return nextTaskRepository.findAll();
  }

  @Override
  public List<NextTask> findNextTasksByTask(Task task) {
    return nextTaskRepository.findNextTasksByTask(task);
  }

  @Override
  public NextTask updateNextTask(NextTask nextTask) {
    return nextTaskRepository.save(nextTask);
  }

  @Override
  public void deleteNextTaskById(String id) {
    nextTaskRepository.deleteNextTaskById(id);

    return;
  }
}
