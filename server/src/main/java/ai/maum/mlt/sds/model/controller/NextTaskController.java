package ai.maum.mlt.sds.model.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.sds.model.entity.Model;
import ai.maum.mlt.sds.model.entity.NextTask;
import ai.maum.mlt.sds.model.entity.Task;
import ai.maum.mlt.sds.model.service.ModelService;
import ai.maum.mlt.sds.model.service.NextTaskService;
import ai.maum.mlt.sds.model.service.TaskService;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/sds/model")
public class NextTaskController {

  static final Logger logger = LoggerFactory.getLogger(NextTaskController.class);

  @Autowired
  private ModelService modelService;

  @Autowired
  private TaskService taskService;

  @Autowired
  private NextTaskService nextTaskService;

  @UriRoleDesc(role = "sds/model^R")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}/nexttask", method = RequestMethod.POST)
  public ResponseEntity<?> insertNextTask(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String taskId,
      @RequestBody JSONObject bodyParam) {
    logger.info("POST /api/workspace/{}/entity/{}/task/{}/nexttask", workspaceId, modelId, taskId,
        bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      String id = null;
      String nextTaskId = null;
      String nextTaskCondition = "";
      String controller = "system";
      String userId = null;

      Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

      if (bodyParam.get("id") == null || bodyParam.get("id").toString().trim().equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "id is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        id = bodyParam.get("id").toString().trim();

        NextTask findNextTask = null;
        try {
          findNextTask = nextTaskService.findNextTaskById(id);
        } catch (Exception ex) {
          findNextTask = nextTaskService.findNextTaskById(id);
        }
        if (findNextTask != null) {
          result.put("statusCode", 400);
          result.put("statusMessage", "id is already exist.");

          return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }
      }

      if (bodyParam.get("nextTaskId") == null || bodyParam.get("nextTaskId").toString().trim()
          .equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "nextTaskId is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        nextTaskId = bodyParam.get("nextTaskId").toString().trim();
      }

      if (nextTaskId != null) {
        Model findModel = null;
        try {
          findModel = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
        } catch (Exception ex) {
          findModel = modelService.findModelByWorkspaceIdAndId(workspaceId, modelId);
        }
        if (findModel == null) {
          result.put("statusCode", 400);
          result.put("statusMessage", "Model doesn't exist.");

          return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        boolean isTaskExist = false;
        for (int i = 0; i < findModel.getTaskList().size(); i++) {
          if (findModel.getTaskList().get(i).getId().equals(nextTaskId)) {
            isTaskExist = true;
          }
        }

        if (isTaskExist == false) {
          result.put("statusCode", 400);
          result.put("statusMessage", "nextTaskId 와 일치하는 task 가 없습니다.");

          return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }
      }

      if (bodyParam.get("nextTaskCondition") != null) {
        nextTaskCondition = bodyParam.get("nextTaskCondition").toString().trim().replace("\"", "'");
      }

      if (bodyParam.get("userId") == null || bodyParam.get("userId").toString().trim().equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "userId is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        userId = bodyParam.get("userId").toString().trim();
      }

      Task findTask = null;
      try {
        findTask = taskService.findTaskById(taskId);
      } catch (Exception ex) {
        findTask = taskService.findTaskById(taskId);
      }
      if (findTask == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "task doesn't exist.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      if (nextTaskId != null) {
        for (int i = 0; i < findTask.getNextTaskList().size(); i++) {
          if (findTask.getNextTaskList().get(i).getId().equals(nextTaskId)) {
            result.put("statusCode", 400);
            result.put("statusMessage", "nextTaskId duplicated.");

            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
          }
        }
      }

      NextTask nextTask = new NextTask();
      nextTask.setId(id);
      nextTask.setNextTaskId(nextTaskId);
      nextTask.setNextTaskCondition(nextTaskCondition);
      nextTask.setController(controller);
      nextTask.setTask(findTask);

      nextTask.setCreatorId(userId);
      nextTask.setCreatedAt(currentTimestamp);
      nextTask.setUpdaterId(userId);
      nextTask.setUpdatedAt(currentTimestamp);

      try {
        nextTaskService.insertNextTask(nextTask);
      } catch (Exception ex) {
        nextTaskService.insertNextTask(nextTask);
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger
          .error("POST /api/workspace/{}/entity/{}/task/{}/nexttask", workspaceId, modelId, taskId,
              e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^R")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}/nexttask/list", method = RequestMethod.GET)
  public ResponseEntity<?> getNextTaskListByTaskId(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String taskId) {
    logger.info("GET /api/workspace/{}/entity/{}/task/{}/nexttask/list", workspaceId, modelId,
        taskId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      Task findTask = null;
      try {
        findTask = taskService.findTaskById(taskId);
      } catch (Exception ex) {
        findTask = taskService.findTaskById(taskId);
      }
      if (findTask == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "task 데이터가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      List<NextTask> nextTaskList = new ArrayList<>();
      try {
        nextTaskList = nextTaskService.findNextTasksByTask(findTask);
      } catch (Exception ex) {
        nextTaskList = nextTaskService.findNextTasksByTask(findTask);
      }

      JSONObject wrappedNextTaskInfo = new JSONObject();
      JSONArray nextTaskInfoList = new JSONArray();

      for (int i = 0; i < nextTaskList.size(); i++) {
        JSONObject nextTaskInfo = new JSONObject();

        nextTaskInfo.put("id", nextTaskList.get(i).getId());
        nextTaskInfo.put("nextTaskId", nextTaskList.get(i).getNextTaskId());
        nextTaskInfo.put("nextTaskCondition", nextTaskList.get(i).getNextTaskCondition());
        nextTaskInfo.put("taskId", nextTaskList.get(i).getTask().getId());

        nextTaskInfo.put("creatorId", nextTaskList.get(i).getCreatorId());
        nextTaskInfo.put("createdAt", nextTaskList.get(i).getCreatedAt());
        nextTaskInfo.put("updaterId", nextTaskList.get(i).getUpdaterId());
        nextTaskInfo.put("updatedAt", nextTaskList.get(i).getUpdatedAt());

        nextTaskInfoList.add(nextTaskInfo);
      }

      wrappedNextTaskInfo.put("nextTaskInfoList", nextTaskInfoList);

      result.put("data", wrappedNextTaskInfo);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("GET /api/workspace/{}/entity/{}/task/{}/nexttask/list", workspaceId, modelId,
          taskId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^R")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}/nexttask/{id}", method = RequestMethod.GET)
  public ResponseEntity<?> getNextTaskById(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String taskId, @PathVariable String id) {
    logger.info("GET /api/workspace/{}/entity/{}/task/{}/nexttask/{}", workspaceId, modelId, taskId,
        id);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      NextTask nextTask = null;
      try {
        nextTask = nextTaskService.findNextTaskById(id);
      } catch (Exception ex) {
        nextTask = nextTaskService.findNextTaskById(id);
      }

      JSONObject wrappedNextTaskInfo = new JSONObject();
      JSONObject nextTaskInfo = new JSONObject();

      nextTaskInfo.put("id", nextTask.getId());
      nextTaskInfo.put("nextTaskId", nextTask.getNextTaskId());
      nextTaskInfo.put("nextTaskCondition", nextTask.getNextTaskCondition());
      nextTaskInfo.put("taskId", nextTask.getTask().getId());

      nextTaskInfo.put("creatorId", nextTask.getCreatorId());
      nextTaskInfo.put("createdAt", nextTask.getCreatedAt());
      nextTaskInfo.put("updaterId", nextTask.getUpdaterId());
      nextTaskInfo.put("updatedAt", nextTask.getUpdatedAt());

      wrappedNextTaskInfo.put("nextTaskInfo", nextTaskInfo);

      result.put("data", wrappedNextTaskInfo);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("GET /api/workspace/{}/entity/{}/task/{}/nexttask/{}", workspaceId, modelId,
          taskId, id, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^U")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}/nexttask/{id}", method = RequestMethod.PUT)
  public ResponseEntity<?> updateNextTask(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String taskId, @PathVariable String id,
      @RequestBody JSONObject bodyParam) {
    logger.info("PUT /api/workspace/{}/entity/{}/task/{}/nexttask/{}", workspaceId, modelId, taskId,
        id, bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

      String nextTaskId = "";
      String nextTaskCondition = "";
      String userId = null;

      if (bodyParam.get("nextTaskId") != null) {
        nextTaskId = bodyParam.get("nextTaskId").toString().trim();
      }

      if (bodyParam.get("nextTaskCondition") != null) {
        nextTaskCondition = bodyParam.get("nextTaskCondition").toString().trim().replace("\"", "'");
      }

      if (bodyParam.get("userId") == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "userId 가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        userId = bodyParam.get("userId").toString().trim();
      }

      NextTask findNextTask = null;
      try {
        findNextTask = nextTaskService.findNextTaskById(id);
      } catch (Exception ex) {
        findNextTask = nextTaskService.findNextTaskById(id);
      }
      if (findNextTask == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "id 와 일치하는 nextTask 가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        if (!nextTaskId.equals("")) {
          findNextTask.setNextTaskId(nextTaskId);
        }

        if (nextTaskCondition != null) {
          findNextTask.setNextTaskCondition(nextTaskCondition);
        }

        findNextTask.setUpdaterId(userId);
        findNextTask.setUpdatedAt(currentTimestamp);
      }

      try {
        nextTaskService.updateNextTask(findNextTask);
      } catch (Exception ex) {
        nextTaskService.updateNextTask(findNextTask);
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("PUT /api/workspace/{}/entity/{}/task/{}/nexttask/{}", workspaceId, modelId,
          taskId, id, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^D")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}/nexttask/{id}", method = RequestMethod.DELETE)
  public ResponseEntity<?> deleteNextTaskById(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String taskId, @PathVariable String id) {
    logger.info("DELETE /api/workspace/{}/entity/{}/task/{}/nexttask/{}", workspaceId, modelId,
        taskId, id);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      NextTask findNextTask = null;
      try {
        findNextTask = nextTaskService.findNextTaskById(id);
      } catch (Exception ex) {
        findNextTask = nextTaskService.findNextTaskById(id);
      }
      if (findNextTask == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "id 와 일치하는 nextTask 가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        try {
          nextTaskService.deleteNextTaskById(id);
        } catch (Exception ex) {
          nextTaskService.deleteNextTaskById(id);
        }
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("DELETE /api/workspace/{}/entity/{}/task/{}/nexttask/{}", workspaceId, modelId,
          taskId, id, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
