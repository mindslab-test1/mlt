package ai.maum.mlt.sds.model.repository;

import ai.maum.mlt.sds.model.entity.EventCondition;
import ai.maum.mlt.sds.model.entity.EventUtter;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface EventUtterRepository extends JpaRepository<EventUtter, String> {

  EventUtter findEventUtterById(String eventUtterId);

  List<EventUtter> findAll();

  List<EventUtter> findEventUttersByEventCondition(EventCondition eventCondition);

  void deleteEventUtterById(String eventUtterId);
}
