package ai.maum.mlt.sds.model.service;

import ai.maum.mlt.sds.model.entity.StartEvent;
import ai.maum.mlt.sds.model.entity.Task;
import java.util.List;
import javax.transaction.Transactional;

@Transactional
public interface StartEventService {

  StartEvent insertStartEvent(StartEvent startEvent);

  StartEvent findStartEventById(String startEventId);

  List<StartEvent> findAll();

  List<StartEvent> findStartEventsByTask(Task task);

  StartEvent updateStartEvent(StartEvent startEvent);

  void deleteStartEventById(String startEventId);
}
