package ai.maum.mlt.sds.model.repository;

import ai.maum.mlt.sds.model.entity.Model;
import ai.maum.mlt.sds.model.entity.Task;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface TaskRepository extends JpaRepository<Task, String> {

  Task findTaskById(String taskId);

  List<Task> findAll();

  List<Task> findTasksByModel(Model model);

  void deleteTaskById(String taskId);
}
