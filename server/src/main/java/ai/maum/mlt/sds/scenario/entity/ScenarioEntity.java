package ai.maum.mlt.sds.scenario.entity;

import ai.maum.mlt.common.auth.entity.UserEntity;
import ai.maum.mlt.common.auth.entity.WorkspaceEntity;
import ai.maum.mlt.common.pagenation.PageParameters;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import javax.persistence.Transient;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "MAI_SCENARIO_MODEL")
public class ScenarioEntity extends PageParameters implements Serializable {

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid")
  @Column(name = "ID", length = 40, nullable = false)
  private String id;

  @Column(name = "WORKSPACE_ID", length = 40, nullable = false)
  private String workspaceId;

  @Column(name = "SKILL_ID", length = 40)
  private String skillId;

  @Column(name = "SKILL_NAME", length = 255)
  private String skillName;

  @Column(name = "TOPIC_SENTENCE", length = 255)
  private String topicSentence;

  @Column(name = "STATUS", length = 255)
  private String status;

  @Column(name = "META_YN", length = 1)
  private String metaYn;

  @Column(name = "SUGGEST_SKILLS", length = 255)
  private String suggestSkills;

  @Column(name = "NEXT_SKILLS", length = 255)
  private String nextSkills;

  @Column(name = "IMPORT_SKILLS", length = 255)
  private String importSkills;

  @Column(name = "LINK_YN", length = 1)
  private String linkYn;

  @Lob
  @Column(name = "JSON_DATA")
  private String jsonData;

  @Column(name = "GROUP1", length = 255)
  private String group1;

  @Column(name = "GROUP2", length = 255)
  private String group2;

  @Column(name = "GROUP3", length = 255)
  private String group3;

  @Column(name = "GROUP4", length = 255)
  private String group4;

  @Column(name = "DESCRIPTION", length = 512)
  private String description;

  @Column(name = "SEARCH_OPTION", length = 255)
  private String searchOption;

  @Column(name = "CREATOR_ID", length = 40, nullable = false)
  private String creatorId;

  @CreatedDate
  @Column(name = "CREATED_AT", nullable = false)
  private Date createdAt;

  @Column(name = "UPDATER_ID", length = 40, nullable = false)
  private String updaterId;

  @LastModifiedDate
  @Column(name = "UPDATED_AT", nullable = false)
  private Date updatedAt;

  @ManyToOne
  @JoinColumn(name = "CREATOR_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity createUserEntity;

  @ManyToOne
  @JoinColumn(name = "UPDATER_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity updateUserEntity;

  @ManyToOne
  @JoinColumn(name = "WORKSPACE_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private WorkspaceEntity workspaceEntity;

  @Transient
  private List<String> ids;

  @Transient
  private String subSkillType;
}