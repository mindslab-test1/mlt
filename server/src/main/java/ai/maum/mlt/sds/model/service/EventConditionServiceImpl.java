package ai.maum.mlt.sds.model.service;

import ai.maum.mlt.sds.model.entity.Event;
import ai.maum.mlt.sds.model.entity.EventCondition;
import ai.maum.mlt.sds.model.repository.EventConditionRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EventConditionServiceImpl implements EventConditionService {

  @Autowired
  private EventConditionRepository eventConditionRepository;

  @Override
  public EventCondition insertEventCondition(EventCondition eventCondition) {
    return eventConditionRepository.save(eventCondition);
  }

  @Override
  public EventCondition findEventConditionById(String eventConditionId) {
    return eventConditionRepository.findEventConditionById(eventConditionId);
  }

  @Override
  public List<EventCondition> findAll() {
    return eventConditionRepository.findAll();
  }

  @Override
  public List<EventCondition> findEventConditionsByEvent(Event event) {
    return eventConditionRepository.findEventConditionsByEvent(event);
  }

  @Override
  public EventCondition updateEventCondition(EventCondition eventCondition) {
    return eventConditionRepository.save(eventCondition);
  }

  @Override
  public void deleteEventConditionById(String eventConditionId) {
    eventConditionRepository.deleteEventConditionById(eventConditionId);

    return;
  }
}
