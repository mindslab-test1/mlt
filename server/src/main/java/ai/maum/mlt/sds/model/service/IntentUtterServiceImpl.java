package ai.maum.mlt.sds.model.service;

import ai.maum.mlt.sds.model.entity.Intent;
import ai.maum.mlt.sds.model.entity.IntentUtter;
import ai.maum.mlt.sds.model.repository.IntentUtterRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IntentUtterServiceImpl implements IntentUtterService {

  @Autowired
  private IntentUtterRepository intentUtterRepository;

  @Override
  public IntentUtter insertIntentUtter(IntentUtter intentUtter) {
    return intentUtterRepository.save(intentUtter);
  }

  @Override
  public IntentUtter findIntentUtterById(String intentUtterId) {
    return intentUtterRepository.findIntentUtterById(intentUtterId);
  }

  @Override
  public List<IntentUtter> findAll() {
    return intentUtterRepository.findAll();
  }

  @Override
  public List<IntentUtter> findIntentUttersByIntent(Intent intent) {
    return intentUtterRepository.findIntentUttersByIntent(intent);
  }

  @Override
  public IntentUtter updateIntentUtter(IntentUtter intentUtter) {
    return intentUtterRepository.save(intentUtter);
  }

  @Override
  public void deleteIntentUtterById(String intentUtterId) {
    intentUtterRepository.deleteIntentUtterById(intentUtterId);

    return;
  }
}
