package ai.maum.mlt.sds.model.entity;

import ai.maum.mlt.common.auth.entity.UserEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.io.Serializable;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

@javax.persistence.Entity
@Table(name = "MAI_SDS_START_EVENT")
@EqualsAndHashCode(of = "id")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StartEvent implements Serializable {

  @Id
  @Column(name = "ID", length = 40, nullable = false)
  private String id;

  @Lob
  @Column(name = "START_EVENT_CONDITION", nullable = true)
  private String startEventCondition;

  @Lob
  @Column(name = "START_EVENT_ACTION", nullable = true)
  private String startEventAction;

  @JsonBackReference
  @ManyToOne(targetEntity = Task.class, fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "TASK_ID", nullable = false)
  private Task task;

  @Column(name = "CREATOR_ID", length = 40, nullable = false)
  private String creatorId;

  @Column(name = "UPDATER_ID", length = 40, nullable = false)
  private String updaterId;

  @CreatedDate
  @Column(name = "CREATED_AT", nullable = false)
  private Timestamp createdAt;

  @LastModifiedDate
  @Column(name = "UPDATED_AT", nullable = false)
  private Timestamp updatedAt;

  @ManyToOne
  @JoinColumn(name = "CREATOR_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity createUserEntity;

  @ManyToOne
  @JoinColumn(name = "UPDATER_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity updateUserEntity;

  @JsonIgnore
  @JsonManagedReference
  @OneToMany(mappedBy = "startEvent", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, orphanRemoval = true)
  private List<StartEventUtter> startEventUtterList = new ArrayList<>();

  public String getCreatedAt() {
    if (createdAt != null) {
      Date date = new Date(createdAt.getTime());
      DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

      return dateFormat.format(date);
    } else {
      return null;
    }
  }

  public String getUpdatedAt() {
    if (updatedAt != null) {
      Date date = new Date(updatedAt.getTime());
      DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

      return dateFormat.format(date);
    } else {
      return null;
    }
  }
}
