package ai.maum.mlt.sds.model.entity;

import ai.maum.mlt.common.auth.entity.UserEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.io.Serializable;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

@javax.persistence.Entity
@Table(name = "MAI_SDS_INTENT_UTTER")
@EqualsAndHashCode(of = "id")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class IntentUtter implements Serializable {

  @Id
  @Column(name = "ID", length = 40, nullable = false)
  private String id;

  @Column(name = "USER_SAY", length = 255, nullable = true)
  private String userSay;

  @Column(name = "MAPPED_USER_SAY", length = 255, nullable = true)
  private String mappedUserSay;

  @JsonBackReference
  @ManyToOne(targetEntity = Intent.class, fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "INTENT_ID", nullable = false)
  private Intent intent;

  @Column(name = "CREATOR_ID", length = 40, nullable = false)
  private String creatorId;

  @CreatedDate
  @Column(name = "CREATED_AT", nullable = false)
  private Timestamp createdAt;

  @Column(name = "UPDATER_ID", length = 40, nullable = false)
  private String updaterId;

  @LastModifiedDate
  @Column(name = "UPDATED_AT", nullable = false)
  private Timestamp updatedAt;

  @ManyToOne
  @JoinColumn(name = "CREATOR_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity createUserEntity;

  @ManyToOne
  @JoinColumn(name = "UPDATER_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity updateUserEntity;

  @JsonIgnore
  @JsonManagedReference
  @OneToMany(mappedBy = "intentUtter", cascade = CascadeType.REMOVE, orphanRemoval = true, fetch = FetchType.LAZY)
  private List<IntentUtterData> intentUtterDataList = new ArrayList<>();

  public String getCreatedAt() {
    if (createdAt != null) {
      Date date = new Date(createdAt.getTime());
      DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

      return dateFormat.format(date);
    } else {
      return null;
    }
  }

  public String getUpdatedAt() {
    if (updatedAt != null) {
      Date date = new Date(updatedAt.getTime());
      DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

      return dateFormat.format(date);
    } else {
      return null;
    }
  }
}
