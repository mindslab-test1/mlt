package ai.maum.mlt.sds.model.entity;

import ai.maum.mlt.common.auth.entity.UserEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.io.Serializable;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

@javax.persistence.Entity
@Table(name = "MAI_SDS_MODEL")
@EqualsAndHashCode(of = "id")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
//@JsonSerialize(using = ModelCombinedSerializer.ModelJsonSerializer.class)
public class Model implements Serializable {

  @Id
  @Column(name = "ID", length = 40, nullable = false)
  private String id;

  @Column(name = "WORKSPACE_ID", length = 40, nullable = false)
  private String workspaceId;

  @Column(name = "MODEL_NAME", length = 255, nullable = false)
  private String modelName;

  @Column(name = "DESCRIPTION", length = 255, nullable = true)
  private String description;

  @Column(name = "STATUS", length = 10, nullable = false)
  private String status;

  @Column(name = "LAST_TRAIN_KEY", length = 40, nullable = true)
  private String lastTrainKey;

  @Column(name = "CREATOR_ID", length = 40, nullable = false)
  private String creatorId;

  @CreatedDate
  @Column(name = "CREATED_AT", nullable = false)
  private Timestamp createdAt;

  @Column(name = "UPDATER_ID", length = 40, nullable = false)
  private String updaterId;

  @LastModifiedDate
  @Column(name = "UPDATED_AT", nullable = false)
  private Timestamp updatedAt;

  @ManyToOne
  @JoinColumn(name = "CREATOR_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity createUserEntity;

  @ManyToOne
  @JoinColumn(name = "UPDATER_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity updateUserEntity;

  @JsonIgnore
  @JsonManagedReference
  @OneToMany(mappedBy = "model", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, orphanRemoval = true)
  private List<Task> taskList;

  @ManyToMany(cascade = CascadeType.REMOVE)
  @JoinTable(name = "MAI_SDS_MODEL_ENTITY", joinColumns = @JoinColumn(name = "MODEL_ID", referencedColumnName = "ID"), inverseJoinColumns = @JoinColumn(name = "ENTITY_ID", referencedColumnName = "ID"))
  private List<Entity> entityList = new ArrayList<>();

  @ManyToMany(cascade = CascadeType.REMOVE)
  @JoinTable(name = "MAI_SDS_MODEL_INTENT", joinColumns = @JoinColumn(name = "MODEL_ID", referencedColumnName = "ID"), inverseJoinColumns = @JoinColumn(name = "INTENT_ID", referencedColumnName = "ID"))
  private List<Intent> intentList = new ArrayList<>();


  public String getCreatedAt() {
    if (createdAt != null) {
      Date date = new Date(createdAt.getTime());
      DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

      return dateFormat.format(date);
    } else {
      return null;
    }
  }

  public String getUpdatedAt() {
    if (updatedAt != null) {
      Date date = new Date(updatedAt.getTime());
      DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

      return dateFormat.format(date);
    } else {
      return null;
    }
  }


  public List<Entity> getEntityList() {
    return entityList;
  }

  public void setEntityList(List<Entity> entityList) {
    this.entityList = entityList;
  }

  public void addEntity(Entity entity) {
    this.entityList.add(entity);
  }

  public List<Intent> getIntentList() {
    return intentList;
  }

  public void setIntentList(List<Intent> intentList) {
    this.intentList = intentList;
  }

  public void addIntent(Intent intent) {
    this.intentList.add(intent);
  }
}
