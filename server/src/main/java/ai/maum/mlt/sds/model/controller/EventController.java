package ai.maum.mlt.sds.model.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.sds.model.entity.Event;
import ai.maum.mlt.sds.model.entity.Task;
import ai.maum.mlt.sds.model.service.EventService;
import ai.maum.mlt.sds.model.service.TaskService;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/sds/model")
public class EventController {

  static final Logger logger = LoggerFactory.getLogger(EventController.class);

  @Autowired
  private TaskService taskService;

  @Autowired
  private EventService eventService;

  @UriRoleDesc(role = "sds/model^C")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}/event", method = RequestMethod.POST)
  public ResponseEntity<?> insertEvent(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String taskId,
      @RequestBody JSONObject bodyParam) {
    logger.info("POST /api/workspace/{}/entity/{}/task/{}/event", workspaceId, modelId, taskId,
        bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      String eventId = null;
      String intentId = "";
      String userId = null;

      Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

      if (bodyParam.get("id") == null || bodyParam.get("id").toString().trim().equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "id is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        eventId = bodyParam.get("id").toString().trim();

        Event findEvent = null;
        try {
          findEvent = eventService.findEventById(eventId);
        } catch (Exception ex) {
          findEvent = eventService.findEventById(eventId);
        }
        if (findEvent != null) {
          result.put("statusCode", 400);
          result.put("statusMessage", "id is already exist.");

          return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }
      }

      if (bodyParam.get("intentId") != null) {
        intentId = bodyParam.get("intentId").toString().trim();
      }

      if (bodyParam.get("userId") == null || bodyParam.get("userId").toString().trim().equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "userId is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        userId = bodyParam.get("userId").toString().trim();
      }

      Task findTask = null;
      try {
        findTask = taskService.findTaskById(taskId);
      } catch (Exception ex) {
        findTask = taskService.findTaskById(taskId);
      }
      if (findTask == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "task doesn't exist.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      Event event = new Event();
      event.setId(eventId);
      event.setIntentId(intentId);
      event.setTask(findTask);

      event.setCreatorId(userId);
      event.setCreatedAt(currentTimestamp);
      event.setUpdaterId(userId);
      event.setUpdatedAt(currentTimestamp);

      try {
        eventService.insertEvent(event);
      } catch (Exception ex) {
        eventService.insertEvent(event);
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger
          .error("POST /api/workspace/{}/entity/{}/task/{}/event", workspaceId, modelId, taskId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^R")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}/event/list", method = RequestMethod.GET)
  public ResponseEntity<?> getEventListByTaskId(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String taskId) {
    logger.info("GET /api/workspace/{}/entity/{}/task/{}/event/list", workspaceId, modelId, taskId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      Task findTask = null;
      try {
        findTask = taskService.findTaskById(taskId);
      } catch (Exception ex) {
        findTask = taskService.findTaskById(taskId);
      }
      if (findTask == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "task 데이터가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      List<Event> eventList = new ArrayList<>();
      try {
        eventList = eventService.findEventsByTask(findTask);
      } catch (Exception ex) {
        eventList = eventService.findEventsByTask(findTask);
      }

      JSONObject wrappedEventInfo = new JSONObject();
      JSONArray eventInfoList = new JSONArray();

      for (int i = 0; i < eventList.size(); i++) {
        JSONObject eventInfo = new JSONObject();

        eventInfo.put("id", eventList.get(i).getId());
        eventInfo.put("intentId", eventList.get(i).getIntentId());
        eventInfo.put("taskId", eventList.get(i).getTask().getId());

        eventInfo.put("creatorId", eventList.get(i).getCreatorId());
        eventInfo.put("createdAt", eventList.get(i).getCreatedAt());
        eventInfo.put("updaterId", eventList.get(i).getUpdaterId());
        eventInfo.put("updatedAt", eventList.get(i).getUpdatedAt());

        eventInfoList.add(eventInfo);
      }

      wrappedEventInfo.put("eventInfoList", eventInfoList);

      result.put("data", wrappedEventInfo);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger
          .error("GET /api/workspace/{}/entity/{}/task/{}/event/list", workspaceId, modelId, taskId,
              e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^R")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}/event/{eventId}", method = RequestMethod.GET)
  public ResponseEntity<?> getEventById(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String taskId, @PathVariable String eventId) {
    logger.info("GET /api/workspace/{}/entity/{}/task/{}/event/{}", workspaceId, modelId, taskId,
        eventId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      Event event = null;
      try {
        event = eventService.findEventById(eventId);
      } catch (Exception ex) {
        event = eventService.findEventById(eventId);
      }

      JSONObject wrappedEventInfo = new JSONObject();
      JSONObject eventInfo = new JSONObject();

      eventInfo.put("id", event.getId());
      eventInfo.put("intentId", event.getIntentId());
      eventInfo.put("taskId", event.getTask().getId());

      eventInfo.put("creatorId", event.getCreatorId());
      eventInfo.put("createdAt", event.getCreatedAt());
      eventInfo.put("updaterId", event.getUpdaterId());
      eventInfo.put("updatedAt", event.getUpdatedAt());

      wrappedEventInfo.put("eventInfo", eventInfo);

      result.put("data", wrappedEventInfo);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("GET /api/workspace/{}/entity/{}/task/{}/event/{}", workspaceId, modelId, taskId,
          eventId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^U")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}/event/{eventId}", method = RequestMethod.PUT)
  public ResponseEntity<?> updateEvent(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String taskId, @PathVariable String eventId,
      @RequestBody JSONObject bodyParam) {
    logger.info("PUT /api/workspace/{}/entity/{}/task/{}/event/{}", workspaceId, modelId, taskId,
        eventId, bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

      String intentId = null;
      String userId = null;

      if (bodyParam.get("intentId") != null) {
        intentId = bodyParam.get("intentId").toString().trim();
      }

      if (bodyParam.get("userId") == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "userId 가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        userId = bodyParam.get("userId").toString().trim();
      }

      Event findEvent = null;
      try {
        findEvent = eventService.findEventById(eventId);
      } catch (Exception ex) {
        findEvent = eventService.findEventById(eventId);
      }
      if (findEvent == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "eventId 와 일치하는 event 가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        if (intentId != null) {
          findEvent.setIntentId(intentId);
        }

        findEvent.setUpdaterId(userId);
        findEvent.setUpdatedAt(currentTimestamp);
      }

      try {
        eventService.updateEvent(findEvent);
      } catch (Exception ex) {
        eventService.updateEvent(findEvent);
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("PUT /api/workspace/{}/entity/{}/task/{}/event/{}", workspaceId, modelId, taskId,
          eventId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^D")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}/event/{eventId}", method = RequestMethod.DELETE)
  public ResponseEntity<?> deleteEventById(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String taskId, @PathVariable String eventId) {
    logger.info("DELETE /api/workspace/{}/entity/{}/task/{}/event/{}", workspaceId, modelId, taskId,
        eventId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      Event findEvent = null;
      try {
        findEvent = eventService.findEventById(eventId);
      } catch (Exception ex) {
        findEvent = eventService.findEventById(eventId);
      }
      if (findEvent == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "eventId 와 일치하는 event 가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        try {
          eventService.deleteEventById(eventId);
        } catch (Exception ex) {
          eventService.deleteEventById(eventId);
        }
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("DELETE /api/workspace/{}/entity/{}/task/{}/event/{}", workspaceId, modelId,
          taskId, eventId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
