package ai.maum.mlt.sds.model.service;

import ai.maum.mlt.sds.model.entity.Model;
import java.util.List;
import javax.transaction.Transactional;

@Transactional
public interface ModelService {

  Model insertModel(Model model);

  List<Model> findModelsByWorkspaceIdOrderByModelName(String workspaceId);

  Model findModelByWorkspaceIdAndCreatorIdAndModelName(String workspaceId, String creatorId,
      String modelName);

  Model findModelByWorkspaceIdAndId(String workspaceId, String modelId);

  Model findModelById(String modelId);

  Model updateModel(Model model);

  void deleteModelByWorkspaceIdAndId(String workspaceId, String modelId);
}
