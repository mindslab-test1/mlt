package ai.maum.mlt.sds.model.repository;

import ai.maum.mlt.sds.model.entity.Entity;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface EntityRepository extends JpaRepository<Entity, String> {

  @Query("select e from Entity e where e.workspaceId = :workspaceId order by e.className asc, e.entityName asc")
  List<Entity> findEntitiesByWorkspaceId(@Param("workspaceId") String workspaceId);

  Entity findEntityByWorkspaceIdAndId(String workspaceId, String entityId);

  void deleteEntityByWorkspaceIdAndId(String workspaceId, String entityId);
}
