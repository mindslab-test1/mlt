package ai.maum.mlt.sds.model.service;

import ai.maum.mlt.sds.model.entity.Intent;
import java.util.List;
import javax.transaction.Transactional;

@Transactional
public interface IntentService {

  Intent insertIntent(Intent intent);

  List<Intent> findIntentsByWorkspaceId(String workspaceId);

  Intent findIntentByWorkspaceIdAndId(String workspaceId, String intentId);

  Intent updateIntent(Intent intent);

  void deleteIntentByWorkspaceIdAndId(String workspaceId, String intentId);
}
