package ai.maum.mlt.sds.model.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.sds.model.entity.EventCondition;
import ai.maum.mlt.sds.model.entity.EventUtter;
import ai.maum.mlt.sds.model.service.EventConditionService;
import ai.maum.mlt.sds.model.service.EventUtterService;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/sds/model")
public class EventUtterController {

  static final Logger logger = LoggerFactory.getLogger(EventUtterController.class);

  @Autowired
  private EventConditionService eventConditionService;

  @Autowired
  private EventUtterService eventUtterService;

  @UriRoleDesc(role = "sds/model^C")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}/event/{eventId}/eventcondition/{eventConditionId}/eventutter", method = RequestMethod.POST)
  public ResponseEntity<?> insertEventUtter(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String taskId, @PathVariable String eventId,
      @PathVariable String eventConditionId, @RequestBody JSONObject bodyParam) {
    logger.info("POST /api/workspace/{}/entity/{}/task/{}/event/{}/eventcondition/{}/eventutter",
        workspaceId, modelId, taskId, eventId, eventConditionId, bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      String eventUtterId = null;
      String intentId = "";
      String extraData = "";
      String userId = null;

      Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

      if (bodyParam.get("id") == null || bodyParam.get("id").toString().trim().equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "id is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        eventUtterId = bodyParam.get("id").toString().trim();

        EventUtter findEventUtter = null;
        try {
          findEventUtter = eventUtterService.findEventUtterById(eventUtterId);
        } catch (Exception ex) {
          findEventUtter = eventUtterService.findEventUtterById(eventUtterId);
        }
        if (findEventUtter != null) {
          result.put("statusCode", 400);
          result.put("statusMessage", "id is already exist.");

          return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }
      }

      if (bodyParam.get("intentId") != null) {
        intentId = bodyParam.get("intentId").toString().trim();
      }

      if (bodyParam.get("extraData") != null) {
        extraData = bodyParam.get("extraData").toString().trim();
      }

      if (bodyParam.get("userId") == null || bodyParam.get("userId").toString().trim().equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "userId is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        userId = bodyParam.get("userId").toString().trim();
      }

      EventCondition findEventCondition = null;
      try {
        findEventCondition = eventConditionService.findEventConditionById(eventConditionId);
      } catch (Exception ex) {
        findEventCondition = eventConditionService.findEventConditionById(eventConditionId);
      }
      if (findEventCondition == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "eventCondition doesn't exist.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      EventUtter eventUtter = new EventUtter();
      eventUtter.setId(eventUtterId);
      eventUtter.setIntentId(intentId);
      eventUtter.setExtraData(extraData);
      eventUtter.setEventCondition(findEventCondition);

      eventUtter.setCreatorId(userId);
      eventUtter.setCreatedAt(currentTimestamp);
      eventUtter.setUpdaterId(userId);
      eventUtter.setUpdatedAt(currentTimestamp);

      try {
        eventUtterService.insertEventUtter(eventUtter);
      } catch (Exception ex) {
        eventUtterService.insertEventUtter(eventUtter);
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("POST /api/workspace/{}/entity/{}/task/{}/event/{}/eventcondition/{}/eventutter",
          workspaceId, modelId, taskId, eventId, eventConditionId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^R")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}/event/{eventId}/eventcondition/{eventConditionId}/eventutter/list", method = RequestMethod.GET)
  public ResponseEntity<?> getEventUtterListByEventConditionId(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String taskId, @PathVariable String eventId,
      @PathVariable String eventConditionId) {
    logger
        .info("GET /api/workspace/{}/entity/{}/task/{}/event/{}/eventcondition/{}/eventutter/list",
            workspaceId, modelId, taskId, eventId, eventConditionId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      EventCondition findEventCondition = null;
      try {
        findEventCondition = eventConditionService.findEventConditionById(eventConditionId);
      } catch (Exception ex) {
        findEventCondition = eventConditionService.findEventConditionById(eventConditionId);
      }
      if (findEventCondition == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "eventCondition 데이터가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      List<EventUtter> eventUtterList = new ArrayList<>();
      try {
        eventUtterList = eventUtterService.findEventUttersByEventCondition(findEventCondition);
      } catch (Exception ex) {
        eventUtterList = eventUtterService.findEventUttersByEventCondition(findEventCondition);
      }

      JSONObject wrappedEventUtterInfo = new JSONObject();
      JSONArray eventUtterInfoList = new JSONArray();

      for (int i = 0; i < eventUtterList.size(); i++) {
        JSONObject eventUtterInfo = new JSONObject();

        eventUtterInfo.put("id", eventUtterList.get(i).getId());
        eventUtterInfo.put("intentId", eventUtterList.get(i).getIntentId());
        eventUtterInfo.put("extraData", eventUtterList.get(i).getExtraData());
        eventUtterInfo.put("eventConditionId", eventUtterList.get(i).getEventCondition().getId());

        eventUtterInfo.put("creatorId", eventUtterList.get(i).getCreatorId());
        eventUtterInfo.put("createdAt", eventUtterList.get(i).getCreatedAt());
        eventUtterInfo.put("updaterId", eventUtterList.get(i).getUpdaterId());
        eventUtterInfo.put("updatedAt", eventUtterList.get(i).getUpdatedAt());

        eventUtterInfoList.add(eventUtterInfo);
      }

      wrappedEventUtterInfo.put("eventUtterInfoList", eventUtterInfoList);

      result.put("data", wrappedEventUtterInfo);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error(
          "GET /api/workspace/{}/entity/{}/task/{}/event/{}/eventcondition/{}/eventutter/list",
          workspaceId, modelId, taskId, eventId, eventConditionId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^R")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}/event/{eventId}/eventcondition/{eventConditionId}/eventutter/{eventUtterId}", method = RequestMethod.GET)
  public ResponseEntity<?> getEventUtterById(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String taskId, @PathVariable String eventId,
      @PathVariable String eventConditionId, @PathVariable String eventUtterId) {
    logger.info("GET /api/workspace/{}/entity/{}/task/{}/event/{}/eventcondition/{}/eventutter/{}",
        workspaceId, modelId, taskId, eventId, eventConditionId, eventUtterId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      EventUtter eventUtter = null;
      try {
        eventUtter = eventUtterService.findEventUtterById(eventUtterId);
      } catch (Exception ex) {
        eventUtter = eventUtterService.findEventUtterById(eventUtterId);
      }

      JSONObject wrappedEventUtterInfo = new JSONObject();
      JSONObject eventUtterInfo = new JSONObject();

      eventUtterInfo.put("id", eventUtter.getId());
      eventUtterInfo.put("intentId", eventUtter.getIntentId());
      eventUtterInfo.put("extraData", eventUtter.getExtraData());
      eventUtterInfo.put("eventConditionId", eventUtter.getEventCondition().getId());

      eventUtterInfo.put("creatorId", eventUtter.getCreatorId());
      eventUtterInfo.put("createdAt", eventUtter.getCreatedAt());
      eventUtterInfo.put("updaterId", eventUtter.getUpdaterId());
      eventUtterInfo.put("updatedAt", eventUtter.getUpdatedAt());

      wrappedEventUtterInfo.put("eventUtterInfo", eventUtterInfo);

      result.put("data", wrappedEventUtterInfo);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger
          .error("GET /api/workspace/{}/entity/{}/task/{}/event/{}/eventcondition/{}/eventutter/{}",
              workspaceId, modelId, taskId, eventId, eventConditionId, eventUtterId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^U")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}/event/{eventId}/eventcondition/{eventConditionId}/eventutter/{eventUtterId}", method = RequestMethod.PUT)
  public ResponseEntity<?> updateEventUtter(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String taskId, @PathVariable String eventId,
      @PathVariable String eventConditionId, @PathVariable String eventUtterId,
      @RequestBody JSONObject bodyParam) {
    logger.info("PUT /api/workspace/{}/entity/{}/task/{}/event/{}/eventcondition/{}/eventutter/{}",
        workspaceId, modelId, taskId, eventId, eventConditionId, eventUtterId, bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

      String intentId = null;
      String extraData = null;
      String userId = null;

      if (bodyParam.get("intentId") != null) {
        intentId = bodyParam.get("intentId").toString().trim();
      }

      if (bodyParam.get("extraData") != null) {
        extraData = bodyParam.get("extraData").toString().trim();
      }

      if (bodyParam.get("userId") == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "userId 가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        userId = bodyParam.get("userId").toString().trim();
      }

      EventUtter findEventUtter = null;
      try {
        findEventUtter = eventUtterService.findEventUtterById(eventUtterId);
      } catch (Exception ex) {
        findEventUtter = eventUtterService.findEventUtterById(eventUtterId);
      }
      if (findEventUtter == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "eventUtterId 와 일치하는 eventUtter 가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        if (intentId != null) {
          findEventUtter.setIntentId(intentId);
        }

        if (extraData != null) {
          findEventUtter.setExtraData(extraData);
        }

        findEventUtter.setUpdaterId(userId);
        findEventUtter.setUpdatedAt(currentTimestamp);
      }

      try {
        eventUtterService.updateEventUtter(findEventUtter);
      } catch (Exception ex) {
        eventUtterService.updateEventUtter(findEventUtter);
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger
          .error("PUT /api/workspace/{}/entity/{}/task/{}/event/{}/eventcondition/{}/eventutter/{}",
              workspaceId, modelId, taskId, eventId, eventConditionId, eventUtterId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^D")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}/event/{eventId}/eventcondition/{eventConditionId}/eventutter/{eventUtterId}", method = RequestMethod.DELETE)
  public ResponseEntity<?> deleteEventUtterById(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String taskId, @PathVariable String eventId,
      @PathVariable String eventConditionId, @PathVariable String eventUtterId) {
    logger
        .info("DELETE /api/workspace/{}/entity/{}/task/{}/event/{}/eventcondition/{}/eventutter/{}",
            workspaceId, modelId, taskId, eventId, eventConditionId, eventUtterId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      EventUtter findEventUtter = null;
      try {
        findEventUtter = eventUtterService.findEventUtterById(eventUtterId);
      } catch (Exception ex) {
        findEventUtter = eventUtterService.findEventUtterById(eventUtterId);
      }
      if (findEventUtter == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "eventUtterId 와 일치하는 eventUtter 가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        try {
          eventUtterService.deleteEventUtterById(eventUtterId);
        } catch (Exception ex) {
          eventUtterService.deleteEventUtterById(eventUtterId);
        }
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error(
          "DELETE /api/workspace/{}/entity/{}/task/{}/event/{}/eventcondition/{}/eventutter/{}",
          workspaceId, modelId, taskId, eventId, eventConditionId, eventUtterId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
