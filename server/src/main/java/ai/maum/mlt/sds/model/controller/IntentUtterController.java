package ai.maum.mlt.sds.model.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.sds.model.entity.Intent;
import ai.maum.mlt.sds.model.entity.IntentUtter;
import ai.maum.mlt.sds.model.service.IntentService;
import ai.maum.mlt.sds.model.service.IntentUtterService;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/sds/model")
public class IntentUtterController {

  static final Logger logger = LoggerFactory.getLogger(IntentUtterController.class);

  @Autowired
  private IntentService intentService;

  @Autowired
  private IntentUtterService intentUtterService;

  @UriRoleDesc(role = "sds/model^C")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/intent/{intentId}/intentutter", method = RequestMethod.POST)
  public ResponseEntity<?> insertIntentUtter(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String intentId,
      @RequestBody JSONObject bodyParam) {
    logger.info("POST /api/workspace/{}/entity/{}/intent/{}/intentutter", workspaceId, modelId,
        intentId, bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      String intentUtterId = null;
      String userSay = "";
      String mappedUserSay = "";
      String userId = null;

      Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

      if (bodyParam.get("id") == null || bodyParam.get("id").toString().trim().equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "id is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        intentUtterId = bodyParam.get("id").toString().trim();

        IntentUtter findIntentUtter = null;
        try {
          findIntentUtter = intentUtterService.findIntentUtterById(intentUtterId);
        } catch (Exception ex) {
          findIntentUtter = intentUtterService.findIntentUtterById(intentUtterId);
        }
        if (findIntentUtter != null) {
          result.put("statusCode", 400);
          result.put("statusMessage", "id is already exist.");

          return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }
      }

      if (bodyParam.get("userSay") != null && !bodyParam.get("userSay").toString().trim()
          .equals("")) {
        userSay = bodyParam.get("userSay").toString().trim();
      }

      if (bodyParam.get("mappedUserSay") != null && !bodyParam.get("mappedUserSay").toString()
          .trim().equals("")) {
        mappedUserSay = bodyParam.get("mappedUserSay").toString().trim();
      }

      if (bodyParam.get("userId") == null || bodyParam.get("userId").toString().trim().equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "userId is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        userId = bodyParam.get("userId").toString().trim();
      }

      Intent findIntent = null;
      try {
        findIntent = intentService.findIntentByWorkspaceIdAndId(workspaceId, intentId);
      } catch (Exception ex) {
        findIntent = intentService.findIntentByWorkspaceIdAndId(workspaceId, intentId);
      }
      if (findIntent == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "Intent doesn't exist.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      IntentUtter intentUtter = new IntentUtter();
      intentUtter.setId(intentUtterId);
      intentUtter.setUserSay(userSay);
      intentUtter.setMappedUserSay(mappedUserSay);
      intentUtter.setIntent(findIntent);

      intentUtter.setCreatorId(userId);
      intentUtter.setCreatedAt(currentTimestamp);
      intentUtter.setUpdaterId(userId);
      intentUtter.setUpdatedAt(currentTimestamp);

      try {
        intentUtterService.insertIntentUtter(intentUtter);
      } catch (Exception ex) {
        intentUtterService.insertIntentUtter(intentUtter);
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("POST /api/workspace/{}/entity/{}/intent/{}/intentutter", workspaceId, modelId,
          intentId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^R")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/intent/{intentId}/intentutter/list", method = RequestMethod.GET)
  public ResponseEntity<?> getIntentUtterListByIntentId(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String intentId) {
    logger.info("GET /api/workspace/{}/entity/{}/intent/{}/list", workspaceId, modelId, intentId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      Intent intent = null;
      try {
        intent = intentService.findIntentByWorkspaceIdAndId(workspaceId, intentId);
      } catch (Exception ex) {
        intent = intentService.findIntentByWorkspaceIdAndId(workspaceId, intentId);
      }
      if (intent == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "intent 데이터가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      List<IntentUtter> intentUtterList = new ArrayList<>();
      try {
        intentUtterList = intentUtterService.findIntentUttersByIntent(intent);
      } catch (Exception ex) {
        intentUtterList = intentUtterService.findIntentUttersByIntent(intent);
      }

      JSONObject wrappedIntentUtterInfo = new JSONObject();
      JSONArray intentUtterInfoList = new JSONArray();

      for (int i = 0; i < intentUtterList.size(); i++) {
        JSONObject intentUtterInfo = new JSONObject();

        intentUtterInfo.put("id", intentUtterList.get(i).getId());
        intentUtterInfo.put("userSay", intentUtterList.get(i).getUserSay());
        intentUtterInfo.put("mappedUserSay", intentUtterList.get(i).getMappedUserSay());
        intentUtterInfo.put("intentId", intentUtterList.get(i).getIntent().getId());

        intentUtterInfo.put("creatorId", intentUtterList.get(i).getCreatorId());
        intentUtterInfo.put("createdAt", intentUtterList.get(i).getCreatedAt());
        intentUtterInfo.put("updaterId", intentUtterList.get(i).getUpdaterId());
        intentUtterInfo.put("updatedAt", intentUtterList.get(i).getUpdatedAt());

        intentUtterInfoList.add(intentUtterInfo);
      }

      wrappedIntentUtterInfo.put("intentUtterInfoList", intentUtterInfoList);

      result.put("data", wrappedIntentUtterInfo);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("GET /api/workspace/{}/entity/{}/intent/{}/list", workspaceId, modelId, intentId,
          e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^R")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/intent/{intentId}/intentutter/{intentUtterId}", method = RequestMethod.GET)
  public ResponseEntity<?> getIntentUtterByIntentUtterId(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String intentId,
      @PathVariable String intentUtterId) {
    logger.info("GET /api/workspace/{}/entity/{}/intent/{}/intentUtter/{}", workspaceId, modelId,
        intentId, intentUtterId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      Intent intent = null;
      try {
        intent = intentService.findIntentByWorkspaceIdAndId(workspaceId, intentId);
      } catch (Exception ex) {
        intent = intentService.findIntentByWorkspaceIdAndId(workspaceId, intentId);
      }
      if (intent == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "intent 데이터가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      IntentUtter intentUtter = null;
      try {
        intentUtter = intentUtterService.findIntentUtterById(intentUtterId);
      } catch (Exception ex) {
        intentUtter = intentUtterService.findIntentUtterById(intentUtterId);
      }

      JSONObject wrappedIntentUtterInfo = new JSONObject();
      JSONObject intentUtterInfo = new JSONObject();

      intentUtterInfo.put("id", intentUtter.getId());
      intentUtterInfo.put("userSay", intentUtter.getUserSay());
      intentUtterInfo.put("mappedUserSay", intentUtter.getMappedUserSay());
      intentUtterInfo.put("intentId", intentUtter.getIntent().getId());

      intentUtterInfo.put("creatorId", intentUtter.getCreatorId());
      intentUtterInfo.put("createdAt", intentUtter.getCreatedAt());
      intentUtterInfo.put("updaterId", intentUtter.getUpdaterId());
      intentUtterInfo.put("updatedAt", intentUtter.getUpdatedAt());

      wrappedIntentUtterInfo.put("intentUtterInfo", intentUtterInfo);

      result.put("data", wrappedIntentUtterInfo);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("GET /api/workspace/{}/entity/{}/intent/{}/intentutter/{}", workspaceId, modelId,
          intentId, intentUtterId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^U")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/intent/{intentId}/intentutter/{intentUtterId}", method = RequestMethod.PUT)
  public ResponseEntity<?> updateIntentUtter(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String intentId,
      @PathVariable String intentUtterId, @RequestBody JSONObject bodyParam) {
    logger.info("PUT /api/workspace/{}/entity/{}/intent/{}/intentutter/{}", workspaceId, modelId,
        intentId, intentUtterId, bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

      String userSay = null;
      String mappedUserSay = null;
      String userId = null;

      if (bodyParam.get("userSay") != null) {
        userSay = bodyParam.get("userSay").toString().trim();
      }

      if (bodyParam.get("mappedUserSay") != null) {
        mappedUserSay = bodyParam.get("mappedUserSay").toString().trim();
      }

      if (bodyParam.get("userId") == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "userId 가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        userId = bodyParam.get("userId").toString().trim();
      }

      IntentUtter findIntentUtter = null;
      try {
        findIntentUtter = intentUtterService.findIntentUtterById(intentUtterId);
      } catch (Exception ex) {
        findIntentUtter = intentUtterService.findIntentUtterById(intentUtterId);
      }
      if (findIntentUtter == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "intentUtterId 와 일치하는 intentUtter 가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        if (userSay == null & mappedUserSay == null) {
          return new ResponseEntity<>(result, HttpStatus.OK);
        } else {
          if (userSay != null) {
            findIntentUtter.setUserSay(userSay);
          }

          if (mappedUserSay != null) {
            findIntentUtter.setMappedUserSay(mappedUserSay);
          }
        }

        findIntentUtter.setUpdaterId(userId);
        findIntentUtter.setUpdatedAt(currentTimestamp);
      }

      try {
        intentUtterService.updateIntentUtter(findIntentUtter);
      } catch (Exception ex) {
        intentUtterService.updateIntentUtter(findIntentUtter);
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("PUT /api/workspace/{}/entity/{}/intent/{}/intentutter/{}", workspaceId, modelId,
          intentId, intentUtterId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^D")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/intent/{intentId}/intentutter/{intentUtterId}", method = RequestMethod.DELETE)
  public ResponseEntity<?> deleteIntentUtterById(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String intentId,
      @PathVariable String intentUtterId) {
    logger.info("DELETE /api/workspace/{}/entity/{}/intent/{}/intentutter/{}", workspaceId, modelId,
        intentId, intentUtterId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      IntentUtter findIntentUtter = null;
      try {
        findIntentUtter = intentUtterService.findIntentUtterById(intentUtterId);
      } catch (Exception ex) {
        findIntentUtter = intentUtterService.findIntentUtterById(intentUtterId);
      }
      if (findIntentUtter == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "intentUtterId 와 일치하는 intentUtter 가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        try {
          intentUtterService.deleteIntentUtterById(intentUtterId);
        } catch (Exception ex) {
          intentUtterService.deleteIntentUtterById(intentUtterId);
        }
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("DELETE /api/workspace/{}/entity/{}/intent/{}/intentutter/{}", workspaceId,
          modelId, intentId, intentUtterId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
