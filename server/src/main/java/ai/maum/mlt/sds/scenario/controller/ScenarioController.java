package ai.maum.mlt.sds.scenario.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.common.code.service.CodeService;
import ai.maum.mlt.common.git.GitManager;
import ai.maum.mlt.common.system.SystemCode;
import ai.maum.mlt.common.version.entity.CommitHistoryEntity;
import ai.maum.mlt.common.version.service.CommitHistoryService;
import ai.maum.mlt.sds.scenario.entity.ScenarioEntity;
import ai.maum.mlt.sds.scenario.service.ScenarioService;
import ai.maum.mlt.sds.scenario.repository.ScenarioRepository;
import java.text.SimpleDateFormat;
import java.util.*;

import org.eclipse.jgit.lib.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/sds/scenario")
public class ScenarioController {

  static final Logger logger = LoggerFactory.getLogger(ScenarioController.class);
  SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss", Locale.KOREA);

  @Value("${git.file.path}")
  private String gitPath;

  @Autowired
  private ScenarioService scenarioService;

  @Autowired
  private ScenarioRepository scenarioRepository;

  @Autowired
  private CodeService codeService;

  @Autowired
  private CommitHistoryService commitHistoryService;

  @UriRoleDesc(role = "sds/scenario^U")
  @RequestMapping(
      value = "/updateSkill",
      method = RequestMethod.POST)
  public ResponseEntity<?> updateSkill(@RequestBody ScenarioEntity param) {
    logger.info("===== call api  GET [[/api/sds/dialogDesign/updateSkill]] :: {}", param);
    try {
      boolean findSkillName = scenarioRepository.findBySkillName(param.getSkillName()).isPresent();
      boolean findId = scenarioRepository.findById(param.getId()).isPresent();

      if (findSkillName && !findId) {
        throw new java.lang.IllegalArgumentException("SkillName is duplicated");
      } else {
        String date;
        if (param.getCreatedAt() == null) {
          date = formatter.format (new Date());
          param.setCreatedAt(formatter.parse(date));
        }
        date = formatter.format (new Date());
        param.setUpdatedAt(formatter.parse(date));

        ScenarioEntity scenarioEntities = scenarioService.updateSkill(param.getWorkspaceId(), param);

        return new ResponseEntity<>(scenarioEntities, HttpStatus.OK);
      }
    } catch (Exception e) {
      logger.error("updateSkill e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/scenario^R")
  @RequestMapping(
      value = "/getSkillLists",
      method = RequestMethod.POST)
  public ResponseEntity<?> getSkillLists(@RequestBody ScenarioEntity param) {
    logger.info("===== call api  GET [[/api/sds/dialogDesign/getSkillLists]] :: {}", param);
    try {
      Page<ScenarioEntity> scenarioEntities = scenarioService
          .getAllSkills(param);
      return new ResponseEntity<>(scenarioEntities, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getSkillLists e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/scenario^R")
  @RequestMapping(
      value = "/getSkill",
      method = RequestMethod.POST)
  public ResponseEntity<?> getSkill(@RequestBody ScenarioEntity param) {
    logger.info("===== call api  GET [[/api/sds/dialogDesign/getSkill]] :: {}", param);
    try {
      ScenarioEntity scenarioEntity = scenarioService.getSkill(param.getId());
      return new ResponseEntity<>(scenarioEntity, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getSkill e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/scenario^R")
  @RequestMapping(
      value = "/getSubSkills",
      method = RequestMethod.POST)
  public ResponseEntity<?> getSubSkills(@RequestBody ScenarioEntity param) {
    logger.info("===== call api  GET [[/api/sds/dialogDesign/getSubSkills]] :: {}", param);
    try {
      String skillList = "";
      if (param.getSubSkillType().equals("suggest")) {
        skillList = param.getSuggestSkills();
      } else if (param.getSubSkillType().equals("next")) {
        skillList = param.getNextSkills();
      }

      String[] subList = skillList.split(",");
      for (int i = 0; i < subList.length; i++) {
        if (subList[i].length() > 6) {
          subList[i] = subList[i].trim().substring(0, 6);
        } else {
          continue;
        }
      }
      List<String> list = Arrays.asList(subList);


      Page<ScenarioEntity> scenarioEntities = scenarioService.getSubSkills(param, list);

      return new ResponseEntity<>(scenarioEntities, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getSubSkills e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/scenario^R")
  @RequestMapping(
      value = "/getImportSkills",
      method = RequestMethod.POST)
  public ResponseEntity<?> getImportSkills(@RequestBody ScenarioEntity param) {
    logger.info("===== call api  GET [[/api/sds/dialogDesign/getImportSkills]] :: {}", param);

    StringTokenizer subList = new StringTokenizer(param.getImportSkills(),",");
    try {
      List<ScenarioEntity> scenarioEntities = new ArrayList<ScenarioEntity>();
      while (subList.hasMoreTokens()) {
        ScenarioEntity tmpEntity = scenarioService.getImportSkills(param.getWorkspaceId(), subList.nextToken().trim());
        if (tmpEntity != null) {
          scenarioEntities.add(tmpEntity);
        }
      }

      return new ResponseEntity<>(scenarioEntities, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getImportSkills e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/scenario^D")
  @RequestMapping(
      value = "/deleteCheckedSkill",
      method = RequestMethod.POST)
  public void deleteCheckedSkill(@RequestBody ScenarioEntity param) {
    logger.info("===== call api  GET [[/api/sds/dialogDesign/deleteCheckedSkill]] :: {}", param);
    try {
      List<String> ids = param.getIds();
      for (int i = 0; i < ids.size(); i++) {
        logger.debug("ids,get(i)", ids.get(i));
        scenarioService.deleteCheckedSkill(ids.get(i));
      }
    } catch (Exception e) {
      logger.error("deleteCheckedSkill e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
    }
  }

  @UriRoleDesc(role = "sds/scenario^U")
  @RequestMapping(
          value = "/updateVersion",
          method = RequestMethod.POST)
  public ResponseEntity<?> updateVersion(@RequestBody ScenarioEntity param) {
    logger.info("===== call api  GET [[/api/sds/dialogDesign/updateVersion]] :: {}", param);
    try {
      GitManager gs = new GitManager();
      HashMap<String, String> value = new HashMap<String, String>();

      value.put("gitPath", gitPath + "/sds/scenario");
      value.put("workspaceId", param.getWorkspaceId());
      value.put("name", "admin");
      value.put("email", "mlt@ai");
      value.put("fileName", param.getId());
      value.put("content", param.getJsonData());
      value.put("messsage", "");
      gs.updateFile(value);

      // 커밋히스토리에 적재
      CommitHistoryEntity entity = new CommitHistoryEntity();
      entity.setFileId(param.getId());
      entity.setWorkspaceId(param.getWorkspaceId());
      entity.setType("SCENARIO");
      commitHistoryService.insertCommit(entity);

      return new ResponseEntity<>(HttpStatus.OK);
    } catch (Exception e) {
      logger.error("updateSkill e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/scenario^R")
  @RequestMapping(
          value = "/getCommitList",
          method = RequestMethod.POST)
  public ResponseEntity<?> getCommitList(@RequestBody ScenarioEntity param) {
    logger.debug("start : {}", param);

    try {
      GitManager gs = new GitManager();
      gs.setWorkingDirectory(gitPath + "/sds/scenario");

      List<HashMap<String, Object>> result = gs
              .getCommitList(param.getWorkspaceId(), param.getId());

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      e.printStackTrace();
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/scenario^R")
  @RequestMapping(
          value = "/getCommit",
          method = RequestMethod.POST)
  public ResponseEntity<?> getCommit(@RequestBody HashMap<String, String> param) {
    logger.debug("start : {}", param);

    try {
      GitManager gs = new GitManager();
      gs.setWorkingDirectory(gitPath + "/sds/scenario");

      String result = gs.readFileFromCommit(param.get("workspaceId"), param.get("id"),  ObjectId.fromString(param.get("commitId")));

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      e.printStackTrace();
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/scenario^R")
  @RequestMapping(
      value = "/getSystemCode",
      method = RequestMethod.GET)
  public ResponseEntity<?> getNodeCode() {
    logger.info("===== call api  GET [[/api/sds/dialogDesign/getSystemCode]]");
    try {
      HashMap<String, Object> systemCodeList = new HashMap<String, Object>();

      systemCodeList.put("nodeCode", codeService.getCodeList(SystemCode.COMMON_GROUP_CODE_SCENARIO_NODE));
      systemCodeList.put("edgeCode", codeService.getCodeList(SystemCode.COMMON_GROUP_CODE_SCENARIO_EDGE));
      systemCodeList.put("statusCode", codeService.getCodeList(SystemCode.COMMON_GROUP_CODE_SCENARIO_STATUS));
      systemCodeList.put("group1Code", codeService.getCodeList(SystemCode.COMMON_GROUP_CODE_SCENARIO_GROUP1));

      return new ResponseEntity(systemCodeList, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getNodeCode e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
