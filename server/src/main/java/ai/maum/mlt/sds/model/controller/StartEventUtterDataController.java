package ai.maum.mlt.sds.model.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.sds.model.entity.StartEventUtter;
import ai.maum.mlt.sds.model.entity.StartEventUtterData;
import ai.maum.mlt.sds.model.service.StartEventUtterDataService;
import ai.maum.mlt.sds.model.service.StartEventUtterService;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/sds/model")
public class StartEventUtterDataController {

  static final Logger logger = LoggerFactory.getLogger(StartEventUtterDataController.class);

  @Autowired
  private StartEventUtterService startEventUtterService;

  @Autowired
  private StartEventUtterDataService startEventUtterDataService;

  @UriRoleDesc(role = "sds/model^C")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}/startevent/{startEventId}/starteventutter/{startEventUtterId}/starteventutterdata", method = RequestMethod.POST)
  public ResponseEntity<?> insertStartEventUtterData(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String taskId, @PathVariable String startEventId,
      @PathVariable String startEventUtterId, @RequestBody JSONObject bodyParam) {
    logger.info(
        "POST /api/workspace/{}/entity/{}/task/{}/startevent/{}/starteventutter/{}/starteventutterdata",
        workspaceId, modelId, taskId, startEventId, startEventUtterId, bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      String startEventUtterDataId = null;
      String startEventUtterDataContent = "";
      String userId = null;

      Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

      if (bodyParam.get("id") == null || bodyParam.get("id").toString().trim().equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "id is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        startEventUtterDataId = bodyParam.get("id").toString().trim();

        StartEventUtterData findStartEventUtterData = null;
        try {
          findStartEventUtterData = startEventUtterDataService
              .findStartEventUtterDataById(startEventUtterDataId);
        } catch (Exception ex) {
          findStartEventUtterData = startEventUtterDataService
              .findStartEventUtterDataById(startEventUtterDataId);
        }
        if (findStartEventUtterData != null) {
          result.put("statusCode", 400);
          result.put("statusMessage", "id is already exist.");

          return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }
      }

      if (bodyParam.get("startEventUtterData") != null) {
        startEventUtterDataContent = bodyParam.get("startEventUtterData").toString().trim();
      }

      if (bodyParam.get("userId") == null || bodyParam.get("userId").toString().trim().equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "userId is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        userId = bodyParam.get("userId").toString().trim();
      }

      StartEventUtter findStartEventUtter = null;
      try {
        findStartEventUtter = startEventUtterService.findStartEventUtterById(startEventUtterId);
      } catch (Exception ex) {
        findStartEventUtter = startEventUtterService.findStartEventUtterById(startEventUtterId);
      }
      if (findStartEventUtter == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "startEventUtter doesn't exist.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      StartEventUtterData startEventUtterData = new StartEventUtterData();
      startEventUtterData.setId(startEventUtterDataId);
      startEventUtterData.setStartEventUtterData(startEventUtterDataContent);
      startEventUtterData.setStartEventUtter(findStartEventUtter);

      startEventUtterData.setCreatorId(userId);
      startEventUtterData.setCreatedAt(currentTimestamp);
      startEventUtterData.setUpdaterId(userId);
      startEventUtterData.setUpdatedAt(currentTimestamp);

      try {
        startEventUtterDataService.insertStartEventUtterData(startEventUtterData);
      } catch (Exception ex) {
        startEventUtterDataService.insertStartEventUtterData(startEventUtterData);
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error(
          "POST /api/workspace/{}/entity/{}/task/{}/startevent/{}/starteventutter/{}/starteventutterdata",
          workspaceId, modelId, taskId, startEventId, startEventUtterId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^R")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}/startevent/{startEventId}/starteventutter/{startEventUtterId}/starteventutterdata/list", method = RequestMethod.GET)
  public ResponseEntity<?> getStartEventUtterDataListByStartEventUtterId(
      @PathVariable String workspaceId, @PathVariable String modelId, @PathVariable String taskId,
      @PathVariable String startEventId, @PathVariable String startEventUtterId) {
    logger.info(
        "GET /api/workspace/{}/entity/{}/task/{}/startevent/{}/starteventutter/{}/starteventutterdata/list",
        workspaceId, modelId, taskId, startEventId, startEventUtterId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      StartEventUtter findStartEventUtter = null;
      try {
        findStartEventUtter = startEventUtterService.findStartEventUtterById(startEventUtterId);
      } catch (Exception ex) {
        findStartEventUtter = startEventUtterService.findStartEventUtterById(startEventUtterId);
      }
      if (findStartEventUtter == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "startEventUtter 데이터가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      List<StartEventUtterData> startEventUtterDataList = new ArrayList<>();
      try {
        startEventUtterDataList = startEventUtterDataService
            .findStartEventUtterDataByStartEventUtter(findStartEventUtter);
      } catch (Exception ex) {
        startEventUtterDataList = startEventUtterDataService
            .findStartEventUtterDataByStartEventUtter(findStartEventUtter);
      }

      JSONObject wrappedStartEventUtterDataInfo = new JSONObject();
      JSONArray startEventUtterDataInfoList = new JSONArray();

      for (int i = 0; i < startEventUtterDataList.size(); i++) {
        JSONObject startEventUtterDataInfo = new JSONObject();

        startEventUtterDataInfo.put("id", startEventUtterDataList.get(i).getId());
        startEventUtterDataInfo
            .put("startEventUtterData", startEventUtterDataList.get(i).getStartEventUtterData());
        startEventUtterDataInfo
            .put("startEventUtterId", startEventUtterDataList.get(i).getStartEventUtter().getId());

        startEventUtterDataInfo.put("creatorId", startEventUtterDataList.get(i).getCreatorId());
        startEventUtterDataInfo.put("createdAt", startEventUtterDataList.get(i).getCreatedAt());
        startEventUtterDataInfo.put("updaterId", startEventUtterDataList.get(i).getUpdaterId());
        startEventUtterDataInfo.put("updatedAt", startEventUtterDataList.get(i).getUpdatedAt());

        startEventUtterDataInfoList.add(startEventUtterDataInfo);
      }

      wrappedStartEventUtterDataInfo
          .put("startEventUtterDataInfoList", startEventUtterDataInfoList);

      result.put("data", wrappedStartEventUtterDataInfo);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error(
          "GET /api/workspace/{}/entity/{}/task/{}/startevent/{}/starteventutter/{}/starteventutterdata/list",
          workspaceId, modelId, taskId, startEventId, startEventUtterId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^R")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}/startevent/{startEventId}/starteventutter/{startEventUtterId}/starteventutterdata/{startEventUtterDataId}", method = RequestMethod.GET)
  public ResponseEntity<?> getStartEventUtterById(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String taskId, @PathVariable String startEventId,
      @PathVariable String startEventUtterId, @PathVariable String startEventUtterDataId) {
    logger.info(
        "GET /api/workspace/{}/entity/{}/task/{}/startevent/{}/starteventutter/{}/starteventutterdata/{}",
        workspaceId, modelId, taskId, startEventId, startEventUtterId, startEventUtterDataId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      StartEventUtterData startEventUtterData = null;
      try {
        startEventUtterData = startEventUtterDataService
            .findStartEventUtterDataById(startEventUtterDataId);
      } catch (Exception ex) {
        startEventUtterData = startEventUtterDataService
            .findStartEventUtterDataById(startEventUtterDataId);
      }

      JSONObject wrappedStartEventUtterDataInfo = new JSONObject();
      JSONObject startEventUtterDataInfo = new JSONObject();

      startEventUtterDataInfo.put("id", startEventUtterData.getId());
      startEventUtterDataInfo
          .put("startEventUtterData", startEventUtterData.getStartEventUtterData());
      startEventUtterDataInfo
          .put("startEventUtterId", startEventUtterData.getStartEventUtter().getId());

      startEventUtterDataInfo.put("creatorId", startEventUtterData.getCreatorId());
      startEventUtterDataInfo.put("createdAt", startEventUtterData.getCreatedAt());
      startEventUtterDataInfo.put("updaterId", startEventUtterData.getUpdaterId());
      startEventUtterDataInfo.put("updatedAt", startEventUtterData.getUpdatedAt());

      wrappedStartEventUtterDataInfo.put("startEventUtterDataInfo", startEventUtterDataInfo);

      result.put("data", wrappedStartEventUtterDataInfo);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error(
          "GET /api/workspace/{}/entity/{}/task/{}/startevent/{}/starteventutter/{}/starteventutterdata/{}",
          workspaceId, modelId, taskId, startEventId, startEventUtterId, startEventUtterDataId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^U")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}/startevent/{startEventId}/starteventutter/{startEventUtterId}/starteventutterdata/{startEventUtterDataId}", method = RequestMethod.PUT)
  public ResponseEntity<?> updateStartEventUtterData(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String taskId, @PathVariable String startEventId,
      @PathVariable String startEventUtterId, @PathVariable String startEventUtterDataId,
      @RequestBody JSONObject bodyParam) {
    logger.info(
        "PUT /api/workspace/{}/entity/{}/task/{}/startevent/{}/starteventutter/{}/starteventutterdata/{}",
        workspaceId, modelId, taskId, startEventId, startEventUtterId, startEventUtterDataId,
        bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

      String startEventUtterData = null;
      String userId = null;

      if (bodyParam.get("startEventUtterData") != null) {
        startEventUtterData = bodyParam.get("startEventUtterData").toString().trim();
      }

      if (bodyParam.get("userId") == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "userId 가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        userId = bodyParam.get("userId").toString().trim();
      }

      StartEventUtterData findStartEventUtterData = null;
      try {
        findStartEventUtterData = startEventUtterDataService
            .findStartEventUtterDataById(startEventUtterDataId);
      } catch (Exception ex) {
        findStartEventUtterData = startEventUtterDataService
            .findStartEventUtterDataById(startEventUtterDataId);
      }
      if (findStartEventUtterData == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "startEventUtterDataId 와 일치하는 startEventUtterData 가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        if (startEventUtterData != null) {
          findStartEventUtterData.setStartEventUtterData(startEventUtterData);
        }

        findStartEventUtterData.setUpdaterId(userId);
        findStartEventUtterData.setUpdatedAt(currentTimestamp);
      }

      try {
        startEventUtterDataService.updateStartEventUtterData(findStartEventUtterData);
      } catch (Exception ex) {
        startEventUtterDataService.updateStartEventUtterData(findStartEventUtterData);
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error(
          "PUT /api/workspace/{}/entity/{}/task/{}/startevent/{}/starteventutter/{}/starteventutterdata/{}",
          workspaceId, modelId, taskId, startEventId, startEventUtterId, startEventUtterDataId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "sds/model^D")
  @RequestMapping(value = "/workspace/{workspaceId}/model/{modelId}/task/{taskId}/startevent/{startEventId}/starteventutter/{startEventUtterId}/starteventutterdata/{startEventUtterDataId}", method = RequestMethod.DELETE)
  public ResponseEntity<?> deleteStartEventUtterDataById(@PathVariable String workspaceId,
      @PathVariable String modelId, @PathVariable String taskId, @PathVariable String startEventId,
      @PathVariable String startEventUtterId, @PathVariable String startEventUtterDataId) {
    logger.info(
        "DELETE /api/workspace/{}/entity/{}/task/{}/startevent/{}/starteventutter/{}/starteventutterdata/{}",
        workspaceId, modelId, taskId, startEventId, startEventUtterId, startEventUtterDataId);

    JSONObject result = new JSONObject();
    result.put("data", null);
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      StartEventUtterData findStartEventUtterData = null;
      try {
        findStartEventUtterData = startEventUtterDataService
            .findStartEventUtterDataById(startEventUtterDataId);
      } catch (Exception ex) {
        findStartEventUtterData = startEventUtterDataService
            .findStartEventUtterDataById(startEventUtterDataId);
      }
      if (findStartEventUtterData == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "startEventUtterDataId 와 일치하는 startEventUtterData 가 없습니다.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        try {
          startEventUtterDataService.deleteStartEventUtterDataById(startEventUtterDataId);
        } catch (Exception ex) {
          startEventUtterDataService.deleteStartEventUtterDataById(startEventUtterDataId);
        }
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error(
          "DELETE /api/workspace/{}/entity/{}/task/{}/startevent/{}/starteventutter/{}/starteventutterdata/{}",
          workspaceId, modelId, taskId, startEventId, startEventUtterId, startEventUtterDataId, e);

      result.put("statusCode", 500);
      result.put("statusMessage", e.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
