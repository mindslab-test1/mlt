package ai.maum.mlt.sds.model.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.sds.model.service.TalkGrpcService;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/sds/model")
public class TalkController {

  static final Logger logger = LoggerFactory.getLogger(TalkController.class);

  @Autowired
  private TalkGrpcService talkGrpcService;

  @RequestMapping(value = "/talk/resolver/ping/{modelName}", method = RequestMethod.GET)
  public ResponseEntity<?> callResolverPing(@PathVariable String modelName) {
    logger.info("GET /talk/resolver/ping/{}", modelName);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      JSONObject data = talkGrpcService.resolverPing(modelName);
      result.put("data", data);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception ex) {
      logger.error("GET /talk/resolver/ping/{}", modelName, ex);

      result.put("statusCode", 500);
      result.put("statusMessage", ex.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @RequestMapping(value = "/talk/resolver/updatemodel", method = RequestMethod.POST)
  public ResponseEntity<?> callResolverUpdateModel(@RequestBody JSONObject bodyParam) {
    logger.info("POST /talk/resolver/updatemodel", bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      String modelId = null;
      String modelName = null;

      if (bodyParam.get("modelId") == null || bodyParam.get("modelId").toString().trim()
          .equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "modelId is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        modelId = bodyParam.get("modelId").toString().trim();
      }

      if (bodyParam.get("modelName") == null || bodyParam.get("modelName").toString().trim()
          .equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "modelName is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        modelName = bodyParam.get("modelName").toString().trim();
      }

      JSONObject data = talkGrpcService.resolverUpdateModel(modelId, modelName);
      result.put("result", data);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception ex) {
      logger.error("POST /talk/resolver/updatemodel", ex);

      result.put("statusCode", 500);
      result.put("statusMessage", ex.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @RequestMapping(value = "/talk/resolver/linkmodel", method = RequestMethod.POST)
  public ResponseEntity<?> callResolverLinkModel(@RequestBody JSONObject bodyParam) {
    logger.info("POST /talk/resolver/linkmodel", bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      String modelId = null;
      String modelName = null;

      if (bodyParam.get("modelId") == null || bodyParam.get("modelId").toString().trim()
          .equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "modelId is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        modelId = bodyParam.get("modelId").toString().trim();
      }

      if (bodyParam.get("modelName") == null || bodyParam.get("modelName").toString().trim()
          .equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "modelName is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        modelName = bodyParam.get("modelName").toString().trim();
      }

      JSONObject data = talkGrpcService.resolverLinkModel(modelId, modelName);
      result.put("result", data);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception ex) {
      logger.error("POST /talk/resolver/linkmodel", ex);

      result.put("statusCode", 500);
      result.put("statusMessage", ex.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @RequestMapping(value = "/talk/resolver/find", method = RequestMethod.POST)
  public ResponseEntity<?> callResolverFind(@RequestBody JSONObject bodyParam) {
    logger.info("POST /talk/resolver/find", bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
//            String modelId = null;
//            String modelName = null;
      String groupName = null;

//            if (bodyParam.get("modelId") == null || bodyParam.get("modelId").toString().trim().equals("")) {
//                result.put("statusCode", 400);
//                result.put("statusMessage", "modelId is null or empty.");
//
//                return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
//            } else {
//                modelId = bodyParam.get("modelId").toString().trim();
//            }
//
//            if (bodyParam.get("modelName") == null || bodyParam.get("modelName").toString().trim().equals("")) {
//                result.put("statusCode", 400);
//                result.put("statusMessage", "modelName is null or empty.");
//
//                return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
//            } else {
//                modelName = bodyParam.get("modelName").toString().trim();
//            }

      if (bodyParam.get("groupName") == null || bodyParam.get("groupName").toString().trim()
          .equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "groupName is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        groupName = bodyParam.get("groupName").toString().trim();
      }

//            JSONObject data = talkGrpcService.resolverFind(modelId, modelName);
      JSONObject data = talkGrpcService.resolverFind(groupName);
      result.put("result", data);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception ex) {
      logger.error("POST /talk/resolver/find", ex);

      result.put("statusCode", 500);
      result.put("statusMessage", ex.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @RequestMapping(value = "/talk/sds/open", method = RequestMethod.POST)
  public ResponseEntity<?> callSdsOpen(@RequestBody JSONObject bodyParam) {
    logger.info("POST /talk/sds/open", bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      String host = null;
      int port = -1;
      String modelId = null;
      String modelName = null;
      long sessionKey = -1;

      if (bodyParam.get("host") == null || bodyParam.get("host").toString().trim().equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "host is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        host = bodyParam.get("host").toString().trim();
      }

      if (bodyParam.get("port") == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "port is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        port = Integer.parseInt(bodyParam.get("port").toString());
      }

      if (bodyParam.get("modelId") == null || bodyParam.get("modelId").toString().trim()
          .equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "modelId is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        modelId = bodyParam.get("modelId").toString().trim();
      }

      if (bodyParam.get("modelName") == null || bodyParam.get("modelName").toString().trim()
          .equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "modelName is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        modelName = bodyParam.get("modelName").toString().trim();
      }

      if (bodyParam.get("sessionKey") == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "sessionKey is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        sessionKey = Long.parseLong(bodyParam.get("sessionKey").toString());
      }

      JSONObject data = talkGrpcService.sdsOpen(host, port, modelId, modelName, sessionKey);
      result.put("result", data);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception ex) {
      logger.error("POST /talk/sds/open", ex);

      result.put("statusCode", 500);
      result.put("statusMessage", ex.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @RequestMapping(value = "/talk/sds/dialog", method = RequestMethod.POST)
  public ResponseEntity<?> callSdsDialog(@RequestBody JSONObject bodyParam) {
    logger.info("POST /talk/sds/dialog", bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      String host = null;
      int port = -1;
      String modelId = null;
      String modelName = null;
      long sessionKey = -1;
      String utter = null;

      if (bodyParam.get("host") == null || bodyParam.get("host").toString().trim().equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "host is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        host = bodyParam.get("host").toString().trim();
      }

      if (bodyParam.get("port") == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "port is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        port = Integer.parseInt(bodyParam.get("port").toString());
      }

      if (bodyParam.get("modelId") == null || bodyParam.get("modelId").toString().trim()
          .equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "modelId is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        modelId = bodyParam.get("modelId").toString().trim();
      }

      if (bodyParam.get("modelName") == null || bodyParam.get("modelName").toString().trim()
          .equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "modelName is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        modelName = bodyParam.get("modelName").toString().trim();
      }

      if (bodyParam.get("sessionKey") == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "sessionKey is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        sessionKey = Long.parseLong(bodyParam.get("sessionKey").toString());
      }

      if (bodyParam.get("utter") == null || bodyParam.get("utter").toString().trim().equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "utter is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        utter = bodyParam.get("utter").toString().trim();
      }

      JSONObject data = talkGrpcService
          .sdsDialog(host, port, modelId, modelName, sessionKey, utter);
      result.put("result", data);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception ex) {
      logger.error("POST /talk/sds/dialog", ex);

      result.put("statusCode", 500);
      result.put("statusMessage", ex.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @RequestMapping(value = "/talk/sds/close", method = RequestMethod.POST)
  public ResponseEntity<?> callSdsClose(@RequestBody JSONObject bodyParam) {
    logger.info("POST /talk/sds/close", bodyParam);

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      String host = null;
      int port = -1;
      String modelId = null;
      String modelName = null;
      long sessionKey = -1;

      if (bodyParam.get("host") == null || bodyParam.get("host").toString().trim().equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "host is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        host = bodyParam.get("host").toString().trim();
      }

      if (bodyParam.get("port") == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "port is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        port = Integer.parseInt(bodyParam.get("port").toString());
      }

      if (bodyParam.get("modelId") == null || bodyParam.get("modelId").toString().trim()
          .equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "modelId is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        modelId = bodyParam.get("modelId").toString().trim();
      }

      if (bodyParam.get("modelName") == null || bodyParam.get("modelName").toString().trim()
          .equals("")) {
        result.put("statusCode", 400);
        result.put("statusMessage", "modelName is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        modelName = bodyParam.get("modelName").toString().trim();
      }

      if (bodyParam.get("sessionKey") == null) {
        result.put("statusCode", 400);
        result.put("statusMessage", "sessionKey is null or empty.");

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      } else {
        sessionKey = Long.parseLong(bodyParam.get("sessionKey").toString());
      }

      JSONObject data = talkGrpcService.sdsClose(host, port, modelId, modelName, sessionKey);
      result.put("result", data);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception ex) {
      logger.error("POST /talk/sds/close", ex);

      result.put("statusCode", 500);
      result.put("statusMessage", ex.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @RequestMapping(value = "/talk/errlog", method = RequestMethod.GET)
  public ResponseEntity<?> getErrorLog() {
    logger.info("GET /talk/errlog");

    JSONObject result = new JSONObject();
    result.put("statusCode", 200);
    result.put("statusMessage", "success.");

    try {
      StringBuilder errLog = new StringBuilder();

      try {
        String fileName = "/home/minds/maum/logs/supervisor/brain-sds.err";
//                String fileName = "./test.log";
        File file = new File(fileName);
        if (file.exists() == false) {
          result.put("statusCode", 500);
          result.put("statusMessage", "Error Log 파일이 존재하지 않습니다.");

          return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

        String temp;
        while ((temp = bufferedReader.readLine()) != null) {
          errLog.append(temp);
          errLog.append("\n");
        }
      } catch (IOException ex) {
        logger.error("GET /talk/errlog", ex);

        result.put("statusCode", 500);
        result.put("statusMessage", ex.toString());

        return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
      }

      return new ResponseEntity<>(errLog, HttpStatus.OK);
    } catch (Exception ex) {
      logger.error("GET /talk/errlog", ex);

      result.put("statusCode", 500);
      result.put("statusMessage", ex.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
