package ai.maum.mlt.sds.model.service;

import ai.maum.mlt.sds.model.entity.Event;
import ai.maum.mlt.sds.model.entity.Task;
import java.util.List;
import javax.transaction.Transactional;

@Transactional
public interface EventService {

  Event insertEvent(Event event);

  Event findEventById(String eventId);

  List<Event> findAll();

  List<Event> findEventsByTask(Task task);

  Event updateEvent(Event event);

  void deleteEventById(String eventId);
}
