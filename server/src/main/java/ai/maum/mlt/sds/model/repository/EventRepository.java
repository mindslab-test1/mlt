package ai.maum.mlt.sds.model.repository;

import ai.maum.mlt.sds.model.entity.Event;
import ai.maum.mlt.sds.model.entity.Task;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface EventRepository extends JpaRepository<Event, String> {

  Event findEventById(String eventId);

  List<Event> findAll();

  List<Event> findEventsByTask(Task task);

  void deleteEventById(String eventId);
}
