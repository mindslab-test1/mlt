package ai.maum.mlt.common.auth.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "MAI_ROLE_MAPPING")
public class RoleMappingEntity implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ROLE_MAPPING_GEN")
  @SequenceGenerator(name = "ROLE_MAPPING_GEN", sequenceName = "MAI_ROLE_MAPPING_SEQ", initialValue = 59, allocationSize = 5)
  @Column(name="ID")
  private Integer id;

  // User 일경우 'USER', Group 일경우 'Group'
  @Column(name="PRINCIPAL_TYPE", length = 512)
  private String principalType;

  // User 일경우 userId, Group 일경우 groupId
  @Column(name = "PRINCIPAL_ID", length = 255)
  private String principalId;

  @Column(name = "ROLE_ID", length = 11)
  private Integer roleId;

  @Column(name = "ROLE_MAPPING_ID", length = 255)
  private String roleMappingId;
}
