package ai.maum.mlt.common.code.service;

import ai.maum.mlt.common.code.entity.CodeEntity;
import ai.maum.mlt.common.code.repository.CodeRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CodeServiceImpl implements CodeService {

  @Autowired
  private CodeRepository codeRepository;

  @Override
  public List<CodeEntity> getCodeList(String groupCode) {
    return codeRepository.findAllByGroupCode(groupCode);
  }

  @Override
  public CodeEntity insertCode(CodeEntity codeEntity) {
    return codeRepository.save(codeEntity);
  }

  @Override
  public CodeEntity updateCode(CodeEntity codeEntity) {
    return codeRepository.saveAndFlush(codeEntity);
  }

  @Override
  public void deleteCode(String id) {
    codeRepository.delete(id);
  }
}
