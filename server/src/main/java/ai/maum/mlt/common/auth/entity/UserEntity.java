package ai.maum.mlt.common.auth.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;

@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "MAI_USER")
public class UserEntity implements Serializable {

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid")
  @Column(name="ID", length = 40)
  private String id;

  // 실제 로그인 할때 사용하는 ID
  @Column(name = "USER_NAME", length = 512)
  private String username;

  @Column(name = "PASSWORD", length = 512, nullable = false)
  private String password;

  @Column(name = "EMAIL", length = 512, nullable = false)
  private String email;

  @Column(name = "ACTIVATED")
  private String activated;

  @CreatedDate
  @Column(name = "LAST_UPDATED")
  private Date lastUpdated;

  @Transient
  private HashMap<String, String> roles;
}
