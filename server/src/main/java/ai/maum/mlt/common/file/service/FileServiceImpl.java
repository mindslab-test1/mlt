package ai.maum.mlt.common.file.service;

import ai.maum.mlt.common.file.entity.FileEntity;
import ai.maum.mlt.common.file.entity.FileGroupEntity;
import ai.maum.mlt.common.file.entity.FileGroupRelEntity;
import ai.maum.mlt.common.file.repository.FileGroupRelRepository;
import ai.maum.mlt.common.file.repository.FileGroupRepository;
import ai.maum.mlt.common.file.repository.FileRepository;
import ai.maum.mlt.common.system.SystemCode;
import ai.maum.mlt.common.system.SystemErrMsg;
import ai.maum.mlt.common.util.FileUtils;
import ai.maum.mlt.stt.repository.STTAnalysisResultRepository;
import ai.maum.mlt.stt.repository.STTTranscriptRepository;

import ai.maum.mlt.stt.service.STTLmFileManageServiceImpl;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.transaction.Transactional;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FileServiceImpl implements FileService {
  private static final Logger logger = LoggerFactory.getLogger(STTLmFileManageServiceImpl.class);

  @Autowired
  private FileRepository fileRepository;

  @Autowired
  private FileGroupRepository fileGroupRepository;

  @Autowired
  private FileGroupRelRepository fileGroupRelRepository;

  @Autowired
  private STTAnalysisResultRepository sttAnalysisResultRepository;

  @Autowired
  private STTTranscriptRepository sttTranscriptRepository;

  @Autowired
  private FileUtils fileUtils;

  @Override
  public FileEntity getFileInfo(String id) {
    return fileRepository.getOne(id);
  }

  @Override
  public Page<FileEntity> getFileList(FileEntity fileEntity) {
    fileEntity.setName(fileEntity.getName() == null ? "" : fileEntity.getName());
    fileEntity
        .setFileGroupId(fileEntity.getFileGroupId() == null ? "" : fileEntity.getFileGroupId());
    return fileRepository
        .findAllByNotInGroupId(fileEntity.getWorkspaceId(), fileEntity.getPurpose(),
            fileEntity.getFileGroupId(),
            fileEntity.getName(), fileEntity.getPageRequest(), null);
  }

  @Override
  public List<FileEntity> getFiles(FileEntity fileEntity) {
    List<FileEntity> list = new ArrayList<FileEntity>();
    fileEntity.setName(fileEntity.getName() == null ? "" : fileEntity.getName());
    fileEntity
        .setFileGroupId(fileEntity.getFileGroupId() == null ? "" : fileEntity.getFileGroupId());
    list =  fileRepository
        .findAllByWorkspaceIdAndPurposeAndCreatorIdAndMeta(fileEntity.getWorkspaceId(), fileEntity.getPurpose(),
            fileEntity.getCreatorId(), null);
    return list;
  }

  @Override
  public List<FileEntity> getFilesWithIds(String workspaceId, List<String> list) {
    List<FileEntity> result = new ArrayList<FileEntity>();
    for (String id : list) {
      FileEntity entity = fileRepository.findByWorkspaceIdAndId(workspaceId, id);
      if (!entity.getPurpose().isEmpty()) {
        result.add(entity);
      }
    }
    return result;
  }

  @Override
  public Page<FileEntity> getGroupFileList(FileEntity fileEntity) {
    fileEntity.setName(fileEntity.getName() == null ? "" : fileEntity.getName());
    return fileRepository
        .findAllByInGroupId(fileEntity.getWorkspaceId(), fileEntity.getPurpose(),
            fileEntity.getFileGroupId(),
            fileEntity.getName(), fileEntity.getPageRequest());
  }

  @Override
  public List<FileEntity> selectAllGroupFileList(FileEntity fileEntity) {
    fileEntity.setName(fileEntity.getName() == null ? "" : fileEntity.getName());
    return fileRepository
        .findAllByInGroupId(fileEntity.getPurpose(),
            fileEntity.getFileGroupId(),
            fileEntity.getName());
  }

  @Override
  public Integer getFileCount(String purpose, String wokrspaceId) {
    return fileRepository.countAllByPurposeAndWorkspaceId(purpose, wokrspaceId);
  }

  @Override
  public HashMap<String, Object> insertFile(MultipartFile file, String workspaceId, String path,
      String purpose, String extension) throws Exception {

    String[] fileNameArr = file.getOriginalFilename().split("\\.");
    String fileExtension = "." + fileNameArr[fileNameArr.length - 1].toLowerCase();
    if (!extension.equals(fileExtension)) {
      throw new Exception("Not Support File Extension");
    }

    String hash = fileUtils.getFileChecksum(file);
    HashMap<String, Object> map = new HashMap<>();
    FileEntity checkFile = fileRepository
        .findByWorkspaceIdAndPurposeAndHash(workspaceId, purpose, hash);
    Integer duration = fileUtils.getFileDuration(file);

    if (checkFile != null) {
      map.put("isSuccess", false);
      map.put("name", file.getOriginalFilename());
      map.put("msg", "Same file is already uploaded as " + file.getOriginalFilename());
      throw new Exception("Same file is already uploaded as " + file.getOriginalFilename());
    } else {
      map = fileUtils.uploadFile(file, path + '/' + workspaceId);

      if ((boolean) map.get("isSuccess")) {
        try {
          FileEntity fileEntity = new FileEntity();
          fileEntity
              .setFileInfo(file, (String) map.get("id"), hash, workspaceId, purpose, duration);
          fileRepository.save(fileEntity);
          map.put("msg", "Complete");
        } catch (Exception e) {
          map.put("isSuccess", false);
        }
      }
    }

    return map;
  }

  @Override
  @Transactional
  public HashMap<String, Object> insertFileWithoutChecksum(MultipartFile file, String workspaceId,
      String path, String purpose, String extension, String userId) throws Exception {

    String[] fileNameArr = file.getOriginalFilename().split("\\.");
    String fileExtension = "." + fileNameArr[fileNameArr.length - 1].toLowerCase();
    if (!extension.equals(fileExtension)) {
      throw new Exception("Not Support File Extension");
    }

    HashMap<String, Object> map = new HashMap<>();
    Integer duration = fileUtils.getFileDuration(file);
    String hash = fileUtils.getFileChecksum(file);
    map = fileUtils.uploadFile(file, path + '/' + workspaceId);

    if ((boolean) map.get("isSuccess")) {
      try {
        FileEntity fileEntity = new FileEntity();
        fileEntity.setFileInfo(file, (String) map.get("id"), hash, workspaceId, purpose, duration);
        fileEntity.setCreatorId(userId);
        FileEntity res =fileRepository.save(fileEntity);
        map.put("msg", "Complete");
      } catch (Exception e) {
        map.put("isSuccess", false);
      }
    }

    return map;
  }

  @Override
  @Transactional
  public HashMap<String, Object> insertSttFile(MultipartFile file, String workspaceId,
      String path,
      String purpose, String extension, String fileGroupId) throws Exception {

    String[] fileNameArr = file.getOriginalFilename().split("\\.");
    String fileExtension = "." + fileNameArr[fileNameArr.length - 1].toLowerCase();
    if (!extension.equals(fileExtension)) {
      throw new Exception(SystemErrMsg.NOT_SUPPORTED_FILE_EXTENSION);
    }
    FileGroupEntity fg = fileGroupRepository.findOne(fileGroupId);

    String hash = fileUtils.getFileChecksum(file);
    HashMap<String, Object> map = new HashMap<>();
    FileEntity checkFile = fileRepository
        .findByWorkspaceIdAndPurposeAndHash(workspaceId, purpose, hash);
    Integer duration = fileUtils.getFileDuration(file);
    String rate = "";
    if (fg.getPurpose().equals(SystemCode.FILE_GRP_PPOS_STT_AM)) {
      rate = fileUtils.getWavFileRate(file);
      JSONParser parser = new JSONParser();
      JSONObject fileGroupMeta = (JSONObject) parser.parse(fg.getMeta());
      if (!rate.equals(fileGroupMeta.get("rate"))) {
        throw new Exception(SystemErrMsg.FILE_GROUP_WAV_RATE_NOT_MATCH);
      }
    }

    if (checkFile != null) {
      int checkFileExistInFileGroup = fileGroupRelRepository.
          countAllByFileIdAndFileGroupId(checkFile.getId(), fileGroupId);
      if (checkFileExistInFileGroup == 0) {
        // file group mapping
        FileGroupRelEntity fileGroupRelEntity = new FileGroupRelEntity();
        fileGroupRelEntity.setFileId(checkFile.getId());
        fileGroupRelEntity.setFileGroupId(fileGroupId);
        fileGroupRelRepository.save(fileGroupRelEntity);

        map.put("isSuccess", true);
        map.put("id", checkFile.getId());
        map.put("name", file.getOriginalFilename());
        map.put("msg", "file is uploaded as " + file.getOriginalFilename());
      } else {
        throw new Exception(
            SystemErrMsg.SAME_FILE_EXIST_AT_FILE_GROUP + " as " + file.getOriginalFilename());
      }
    } else {
      map = fileUtils.uploadFile(file, path + '/' + workspaceId);
      if ((boolean) map.get("isSuccess")) {
        try {
          FileEntity fileEntity = new FileEntity();
          if (fg.getPurpose().equals(SystemCode.FILE_GRP_PPOS_STT_AM)) {
            JSONObject meta = new JSONObject();
            meta.put("rate", rate);
            fileEntity.setMeta(meta.toString());
          }
          fileEntity
              .setFileInfo(file, (String) map.get("id"), hash, workspaceId, purpose, duration);
          fileRepository.save(fileEntity);

          // file group mapping
          FileGroupRelEntity fileGroupRelEntity = new FileGroupRelEntity();
          fileGroupRelEntity.setFileId(map.get("id").toString());
          fileGroupRelEntity.setFileGroupId(fileGroupId);
          fileGroupRelRepository.save(fileGroupRelEntity);
          map.put("msg", "Complete");
        } catch (Exception e) {
          map.put("isSuccess", false);
        }
      }
    }

    return map;
  }

  @Override
  @Transactional
  public void deleteFile(FileEntity fileEntity) throws Exception {
    List<FileEntity> fileEntityList = new ArrayList<>();

    for (String id : fileEntity.getIds()) {
      fileGroupRelRepository.deleteByFileGroupIdAndFileId(fileEntity.getFileGroupId(), id);
      sttAnalysisResultRepository.deleteByFileIdAndWorkspaceId(id, fileEntity.getWorkspaceId());
      sttTranscriptRepository.deleteByFileIdAndWorkspaceId(id, fileEntity.getWorkspaceId());
    }

    for (String id : fileEntity.getIds()) {
      FileEntity tempEntity = new FileEntity();
      tempEntity.setId(id);
      fileEntityList.add(tempEntity);
    }
    fileRepository.delete(fileEntityList);
  }

  @Override
  @Transactional
  public HashMap<String, Object> deleteFileWithIds(List<String> list) throws Exception {
    HashMap<String, Object> map = new HashMap<>();
    try {
      int flag = 0;
      for (String id : list) {
        flag = fileRepository.deleteById(id);
      }
      if (flag > 0) {
        map.put("isSuccess", true);
      } else {
        map.put("isSuccess", false);
      }
      return map;
    } catch (Exception e) {
      map.put("isSuccess", false);
      return map;
    }
  }

  @Override
  @Transactional
  public void deleteDictionaryFile(FileEntity fileEntity) throws Exception {
    for (String id : fileEntity.getIds()) {
      fileRepository.delete(id);
    }
  }

  @Override
  public List<FileEntity> getFileLists(List<String> idList) {
    List<FileEntity> list = new ArrayList<>();
    list = fileRepository.findByIdIn(idList);
    return list;
  }
}
