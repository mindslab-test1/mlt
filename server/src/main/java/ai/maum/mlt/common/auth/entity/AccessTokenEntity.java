package ai.maum.mlt.common.auth.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;

@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "MAI_ACCESS_TOKEN")
public class AccessTokenEntity implements Serializable {

  @Id
  @Column(name="ID", length = 100)
  private String id;

  // 토큰 유효시간 ms
  @Column(name = "TTL")
  private Integer ttl;

  @Column(name = "SCOPES")
  private String scopes;

  @Column(name = "USER_ID")
  private String userId;

  @Column(name = "ACCESS_TOKEN_ID")
  private String accessTokenId;

  @CreatedDate
  @Column(name = "CREATED_AT")
  private Date createdAt;
}
