package ai.maum.mlt.common.auth.repository;

import ai.maum.mlt.common.auth.entity.UserEntity;
import ai.maum.mlt.dictionary.entity.PosDictionaryEntity;
import java.util.Date;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Integer> {

  @Query(value = "SELECT d "
      + "         FROM UserEntity d")
  List<UserEntity> findAllUser();

  @Query(value = "SELECT d "
      + "         FROM UserEntity d"
      + "         WHERE d.username= :username ")
  List<UserEntity> findAllByName(@Param("username") String username);

  @Modifying
  @Transactional
  @Query(value = "UPDATE UserEntity d "
      + "         SET"
      + "         d.email = :email        ,"
      + "         d.activated = :activated,"
      + "         d.lastUpdated = :lastUpdated,"
      + "         d.password = :password"
      + "         WHERE d.id = :id")
  void updateUser(
      @Param("id") String id,
      @Param("email") String email,
      @Param("activated") String activated,
      @Param("lastUpdated") Date lastUpdated,
      @Param("password") String password
  );

  void deleteById(String Id);

}
