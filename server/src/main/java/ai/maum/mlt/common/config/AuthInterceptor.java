package ai.maum.mlt.common.config;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

@Slf4j
@Component
public class AuthInterceptor implements HandlerInterceptor {

  @Autowired
  private RequestUriRole requestUriRole;

  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
      throws Exception {
    /*log.debug("=========================================================================");
    log.debug("=========================================================================");
    log.debug("=========================================================================");
    log.debug("AuthInterceptor.preHandle uri : {}", request.getRequestURI());
    log.debug("AuthInterceptor.preHandle uri : {}", requestUriRole.validateUriRole("sds/model^R", request.getRequestURI()));
    log.debug("=========================================================================");
    log.debug("=========================================================================");
    log.debug("=========================================================================");*/

    return true;
  }

  @Override
  public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
      ModelAndView modelAndView) throws Exception {

  }

  @Override
  public void afterCompletion(HttpServletRequest request, HttpServletResponse response,
      Object handler, Exception ex) throws Exception {

  }
}
