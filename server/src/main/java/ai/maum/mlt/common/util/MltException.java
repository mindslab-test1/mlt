package ai.maum.mlt.common.util;

public class MltException extends Exception {

  private final String detail;

  public MltException(String message, String detail) {
    super(message);
    this.detail = detail;
  }

  public String getDetail() {
    return detail;
  }
}
