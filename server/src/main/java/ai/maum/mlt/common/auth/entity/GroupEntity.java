package ai.maum.mlt.common.auth.entity;

import ai.maum.mlt.common.pagenation.PageParameters;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode(callSuper=false)
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "MAI_GROUP")
public class GroupEntity extends PageParameters implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GROUP_GEN")
  @SequenceGenerator(name = "GROUP_GEN", sequenceName = "MAI_GROUP_SEQ", allocationSize = 5)
  @Column(name="ID")
  private Integer id;

  @Column(name = "NAME", length = 512, nullable = false)
  private String name;

  @Column(name = "DESCRIPTION", length = 512)
  private String description;

  // 그룹의 오너 ID [userId]
  @Column(name = "OWNER_ID")
  private String ownerId;
}
