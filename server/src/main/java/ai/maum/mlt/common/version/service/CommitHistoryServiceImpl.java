package ai.maum.mlt.common.version.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import ai.maum.mlt.common.version.entity.CommitHistoryEntity;
import ai.maum.mlt.common.version.repository.CommitHistoryRepository;

import javax.transaction.Transactional;

@Service
public class CommitHistoryServiceImpl implements CommitHistoryService {

  @Autowired
  private CommitHistoryRepository commitHistoryRepository;

  @Transactional
  @Modifying
  @Override
  public CommitHistoryEntity insertCommit(CommitHistoryEntity entity) {
    return commitHistoryRepository.save(entity);
  }

  @Transactional
  @Override
  public void deleteCommit(CommitHistoryEntity entity) {
    commitHistoryRepository
        .deleteAllByWorkspaceIdAndTypeAndFileId(entity.getWorkspaceId(), entity.getType(),
            entity.getFileId());
  }

  @Override
  public int getCountCommit(CommitHistoryEntity entity) {
    return commitHistoryRepository
        .countCommitHistoryEntitiesByWorkspaceIdAndTypeAndFileId(entity.getWorkspaceId(),
            entity.getType(), entity.getFileId());
  }
}
