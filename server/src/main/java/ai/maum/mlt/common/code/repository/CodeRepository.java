package ai.maum.mlt.common.code.repository;

import ai.maum.mlt.common.code.entity.CodeEntity;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface CodeRepository extends JpaRepository<CodeEntity, String> {
  List<CodeEntity> findAllByGroupCode(String groupCode);
}