package ai.maum.mlt.common.model;

import ai.maum.m2u.common.portable.PortableClassId;

public class NameSpace {

  public static final class MAP {

    public static final String DIALOG_AGENT_INSTANCE_RESOURCE = "dialog-agent-instance-resource";
    public static final String DIALOG_AGENT_INSTANCE_RESOURCE_SKILL = "dialog-agent-instance-resource-skill";
    public static final String DIALOG_AGENT_INSTANCE_EXECUTION = PortableClassId.NsDAInstExec;
    public static final String CHATBOT = PortableClassId.NsChatbot;
    public static final String SESSION = "session";
    public static final String TALK = "talk";
    public static final String SESSION_V3 = "session-v3";
    public static final String TALK_V3 = "talk-v3";
    public static final String CLASSIFICATION = PortableClassId.NsClassificationRecord;
    public static final String TALK_NEXT = "talk-next";
    public static final String DEVICES = "devices";
  }

  public static final class Setting {

    public static final String LOGGER_FRONT_ENABLE = "logger.front.enable";
    public static final String LOGGER_FRONT_SESSION = "logger.front.session";
    public static final String LOGGER_FRONT_TALK = "logger.front.talk";
    public static final String LOGGER_ROUTER_ENABLE = "logger.router.enable";
    public static final String LOGGER_ROUTER_SESSION = "logger.router.session";
    public static final String LOGGER_ROUTER_TALK = "logger.router.talk";
    public static final String LOGGER_ROUTER_CLASSIFICATION = "logger.router.classification";

    public static final String LOGGER_STARTSEC = "logger.startsec";
    public static final String LOGGER_PERIODSEC = "logger.periodsec";
    public static final String LOGGER_SESSION_PERIODSEC = "logger.session.periodsec";
    public static final String DEFAULT_SESSION_EXPIRE_TIME = "logger.session.monitor.expiration";
    public static final String NON_CHATBOT_SESSION_EXPIRE_TIME = "logger.zombie.chatbot.session.timeout";
    public static final String LOGGER_SESSION_CHANNELS = "logger.session.channels";

    public static final String LOGGER_LOCK_NAME = "logger.lock.name";
    public static final String SESSION_LOCK_NAME = "session.lock.name";
  }
}
