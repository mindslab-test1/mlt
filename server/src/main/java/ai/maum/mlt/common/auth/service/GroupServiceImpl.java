package ai.maum.mlt.common.auth.service;

import ai.maum.mlt.common.auth.entity.GroupEntity;
import ai.maum.mlt.common.auth.repository.GroupRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
public class GroupServiceImpl implements GroupService{

  @Autowired
  private GroupRepository groupRepository;

  @Override
  public Page<GroupEntity> findGroupList(GroupEntity groupEntity) {
    return groupRepository.findAllByName(groupEntity.getName(), groupEntity.getPageRequest());
  }

  @Override
  public GroupEntity getGroup(Integer groupId) {
    return groupRepository.findOne(groupId);
  }

  @Override
  public void addGroup(GroupEntity groupEntity) {
    groupRepository.save(groupEntity);

  }

  @Override
  public void editGroup(GroupEntity groupEntity) {
    groupRepository.save(groupEntity);
  }

  @Override
  public void deleteGroup(Integer groupId) {
    groupRepository.delete(groupId);

  }

  @Override
  public String findOwnerId(Integer id) {
    return groupRepository.findOwnerId(id);
  }
}
