package ai.maum.mlt.common.file.service;

import ai.maum.mlt.common.file.entity.FileGroupEntity;
import ai.maum.mlt.common.file.entity.FileGroupRelEntity;
import java.util.List;

public interface FileGroupService {

  List<FileGroupEntity> getFileGroupList(String purpose, String workspaceId);
  void insertFileGroup(FileGroupEntity fileGroupEntity, String purpose) throws Exception;
  void updateFileGroup(FileGroupEntity fileGroupEntity);
  void deleteFileGroup(String id);
  void insertFileGroupRel(List<FileGroupRelEntity> fileGroupRelEntities);
  void deleteFileGroupRel(List<FileGroupRelEntity> fileGroupRelEntities);

}
