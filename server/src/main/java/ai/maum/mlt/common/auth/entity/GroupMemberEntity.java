package ai.maum.mlt.common.auth.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "MAI_GROUP_MEMBER")
public class GroupMemberEntity implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PASSWORD_HISTORY_GEN")
  @SequenceGenerator(name = "PASSWORD_HISTORY_GEN", sequenceName = "MAI_GROUP_MEMBER_SEQ", initialValue = 4, allocationSize = 5)
  @Column(name="ID")
  private Integer id;

  @Column(name = "GROUP_ID")
  private Integer groupId;

  // 그룹에 해당된 userId
  @Column(name = "USER_ID")
  private String userId;

  // 그룹에 속해져 있는지 여부
  @Column(name = "STATE", length = 512)
  private String state;

  // 그룹에 대한 user의 role [owner or member]
  @Column(name = "ROLE", length = 512)
  private String role;

  @Column(name = "DESCRIPTION", length = 512)
  private String description;
}
