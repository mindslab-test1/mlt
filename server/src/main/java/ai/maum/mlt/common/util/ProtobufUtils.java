package ai.maum.mlt.common.util;

import com.google.protobuf.Descriptors;
import com.google.protobuf.Message;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;

public class ProtobufUtils {

  @NotNull
  public Map<String, Object> protoToMap(Message proto) {
    final Map<Descriptors.FieldDescriptor, Object> allFields = proto.getAllFields();
    Map<String, Object> map = new LinkedHashMap<>();
    for (Map.Entry<Descriptors.FieldDescriptor, Object> entry : allFields.entrySet()) {
      final Descriptors.FieldDescriptor fieldDescriptor = entry.getKey();
      final Object requestVal = entry.getValue();
      final Object mapVal = convertVal(proto, fieldDescriptor, requestVal);
      if (mapVal != null) {
        final String fieldName = fieldDescriptor.getName();
        map.put(fieldName, mapVal);
      }
    }
    return map;
  }


  @Nullable
    /*package*/ Object convertVal(@NotNull Message proto, @NotNull Descriptors.FieldDescriptor fieldDescriptor, @Nullable Object protoVal) {
    Object result = null;
    if (protoVal != null) {
      if (fieldDescriptor.isRepeated()) {
        if (proto.getRepeatedFieldCount(fieldDescriptor) > 0) {
          final List originals = (List) protoVal;
          final List copies = new ArrayList(originals.size());
          for (Object original : originals) {
            copies.add(convertAtomicVal(fieldDescriptor, original));
          }
          result = copies;
        }
      } else {
        result = convertAtomicVal(fieldDescriptor, protoVal);
      }
    }
    return result;
  }


  @Nullable
    /*package*/ Object convertAtomicVal(@NotNull Descriptors.FieldDescriptor fieldDescriptor, @Nullable Object protoVal) {
    Object result = null;
    if (protoVal != null) {
      switch (fieldDescriptor.getJavaType()) {
        case INT:
        case LONG:
        case FLOAT:
        case DOUBLE:
        case BOOLEAN:
        case STRING:
          result = protoVal;
          break;
        case BYTE_STRING:
        case ENUM:
          result = protoVal.toString();
          break;
        case MESSAGE:
          result = protoToMap((Message) protoVal);
          break;
      }
    }
    return result;
  }
}
