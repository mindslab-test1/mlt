package ai.maum.mlt.common.auth.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.net.InterfaceAddress;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "MAI_ACL")
public class AclEntity implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ACL_GEN")
  @SequenceGenerator(name = "ACL_GEN", sequenceName = "MAI_ACL_SEQ", allocationSize = 5)
  @Column(name="ID")
  private Integer id;

  // 접근 모델 [Grpc or ..?]
  @Column(name = "MODEL", length = 512)
  private String model;

  // 접근 url 이나 method
  @Column(name = "PROPERTY", length = 512)
  private String property;

  // *로 사용하긴 하는데 여러가지 타입이 있는지 확인 필요
  @Column(name = "ACCESS_TYPE", length = 512)
  private String accessType;

  // ALLOW or FORBID
  @Column(name = "PERMISSION", length = 512)
  private String permission;

  @Column(name = "PRINCIPAL_TYPE", length = 512)
  private String principalType;

  @Column(name = "PRINCIPAL_ID", length = 512)
  private String principalId;
}
