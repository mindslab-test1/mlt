package ai.maum.mlt.common.config;

import java.util.HashMap;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
    entityManagerFactoryRef = "basicQaEntityManagerFactory",
    transactionManagerRef = "basicQaTransactionManager",
    basePackages = {"ai.maum.mlt.basicqa.repository", "ai.maum.mlt.nqa.repository"}
)
public class DatabaseConfigBasicQa {

  @Value("${spring.jpa.hibernate.ddl-auto}")
  private String ddl_auto;

  @Value("${spring.jpa.hibernate.dialect}")
  private String dialect;

  @Bean
  @ConfigurationProperties("datasource.basicqa")
  public DataSourceProperties basicQaDataSourceProperties() {
    return new DataSourceProperties();
  }

  @Bean
  @ConfigurationProperties("datasource.basicqa")
  public DataSource basicQaDataSource() {
    return basicQaDataSourceProperties().initializeDataSourceBuilder().build();
  }

  /*
  @Bean
  public LocalContainerEntityManagerFactoryBean basicQaEntityManagerFactory(EntityManagerFactoryBuilder builder) {
    return builder
        .dataSource(basicQaDataSource())
        .packages("ai.maum.mlt.basicqa.entity")
        .persistenceUnit("basicQaPersistenceUnit")
        .build();
  }
  */
  @Bean
  public LocalContainerEntityManagerFactoryBean basicQaEntityManagerFactory() {
    LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
    em.setDataSource(basicQaDataSource());
    em.setPackagesToScan("ai.maum.mlt.basicqa.entity", "ai.maum.mlt.nqa.entity",
        "ai.maum.mlt.common.auth.entity");
    HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
    em.setJpaVendorAdapter(vendorAdapter);
    HashMap<String, Object> properties = new HashMap<>();
    properties.put("hibernate.hbm2ddl.auto", ddl_auto);
    properties.put("hibernate.dialect", dialect);
    em.setJpaPropertyMap(properties);
    return em;
  }

  @Bean
  public PlatformTransactionManager basicQaTransactionManager(
      @Qualifier("basicQaEntityManagerFactory") EntityManagerFactory basicQaEntityManagerFactory) {
    return new JpaTransactionManager(basicQaEntityManagerFactory);
  }
}
