package ai.maum.mlt.common.system;


public class SystemCode {

  public static final String COMMON_GROUP_CODE_SCENARIO_NODE = "SCENARIO_NODE";
  public static final String COMMON_GROUP_CODE_SCENARIO_EDGE = "SCENARIO_EDGE";
  public static final String COMMON_GROUP_CODE_SCENARIO_STATUS = "SCENARIO_STATUS";
  public static final String COMMON_GROUP_CODE_SCENARIO_GROUP1 = "SCENARIO_GROUP1";

  public static final String MRC_DATA_GRP = "MRC_DATA_GRP";
  public static final String BASICQA_DATA_GRP = "BASICQA_DATA_GRP";

  // {table: FileGroup, column: purpose}
  public static final String FILE_GRP_PPOS_STT = "STT";
  public static final String FILE_GRP_PPOS_STT_LM = "STT_LM"; // STT 언어학습 파일 그룹
  public static final String FILE_GRP_PPOS_STT_AM = "STT_AM"; // STT 음향학습 파일 그룹
  public static final String FILE_GRP_PPOS_TA = "TA";
  public static final String FILE_GRP_PPOS_TA_HMD = "HMD";
  public static final String FILE_GRP_PPOS_TA_DNN = "DNN";
  public static final String FILE_GRP_PPOS_TA_XDC = "XDC";
  public static final String FILE_GRP_PPOS_DICTIONARY_TEST = "TEST_DICTIONARY";

  // data file extension
  public static final String FILE_EXTENSION_WAV = ".wav";
  public static final String FILE_EXTENSION_TEXT = ".txt";

  // file contetns type
  public static final String FILE_COTENT_TYPE_AUDIO = "audio/wav";
  public static final String FILE_COTENT_TYPE_TEXT = "text/plain";

  // Separator
  public static final String FILE_SEPARATE_COMMA = ",";

  // similarQueryExpansion
  public static final String PARAPHRASE_STEP_READY = "R"; // 질의확장 준비단계
  public static final String PARAPHRASE_STEP_INVERSION = "I"; // 질의확장 도치단계
  public static final String PARAPHRASE_STEP_WORDEMBEDDING = "W"; // 질의확장 WE단계
  public static final String PARAPHRASE_STEP_SYNONYM = "S";  // 질의확장 동의어단계
  public static final String PARAPHRASE_STEP_ENDING_POSTPOSITION = "E"; // 질의확장 어미/조사단계
  public static final String PARAPHRASE_STEP_COMPLETE = "C"; // 질의확장 완료단계

  // E : 어미 P : 조사
  public static final String PARAPHRASE_ENDING = "E"; // 어미 조사 사전의 조사
  public static final String PARAPHRASE_POST_POSITION = "P"; // 어미 조사 사전의 조사

  public static final String PRE_PROCESS = "PRE_PROCESS";
  public static final String POST_PROCESS = "POST_PROCESS";

  public final static String PASSWORD_ENCRYPT_KEY = "!mlt500#00!00429";

  // stt traintype
  public static final String STT_TRAINING_TYPE_DNN = "DNN";
  public static final String STT_TRAINING_TYPE_LSTM = "LSTM";

  // ner dictionary corpus tag pattern
  public static final String NER_DICTIONARY_CORPUS_TAG_PATTERN = "<ne>(.*?)</ne>";

  // stt baseline model name
  public static final String STT_BASE_LINE_DNN = "baseline_DNN";
  public static final String STT_BASE_LINE_LSTM = "baseline_LSTM";

  // stt baseline model-binary name
  public static final String STT_BASE_LINE_BINARY_DNN = "baseline";
  public static final String STT_BASE_LINE_BINARY_LSTM = "base_bilstm";
}
