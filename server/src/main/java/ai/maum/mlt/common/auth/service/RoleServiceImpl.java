package ai.maum.mlt.common.auth.service;//package ai.maum.mlt.common.auth.service;

import ai.maum.mlt.common.auth.entity.RoleEntity;
import ai.maum.mlt.common.auth.repository.RoleRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {

  @Autowired
  RoleRepository roleRepository;

  @Override
  public Integer getRoleId(String name) {
    Integer res = null;
    res = roleRepository.getRoleId(name);
    return res;
  }

  @Override
  public String getRoleName(Integer id) {
    String res = null;
    res = roleRepository.getRoleName(id);
    return res;
  }

  @Override
  public List<RoleEntity> findAllRoleList() {
    return roleRepository.findAllRoleList();
  }
}
