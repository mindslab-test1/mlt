package ai.maum.mlt.common.file.repository;

import ai.maum.mlt.common.file.entity.FileGroupRelEntity;
import ai.maum.mlt.common.util.StringUtils;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileGroupRelRepository extends JpaRepository<FileGroupRelEntity, String>{

  int countAllByFileIdAndFileGroupId(String fileId, String fileGroupId);

  int countAllByFileId(String fileId);

  void deleteAllByFileGroupId(String fileGroupId);

  void deleteByFileGroupIdAndFileId(String fileGroupId, String fileId);
}
