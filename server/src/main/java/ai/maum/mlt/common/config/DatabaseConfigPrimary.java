package ai.maum.mlt.common.config;

import java.util.HashMap;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
    entityManagerFactoryRef = "primaryEntityManagerFactory",
    transactionManagerRef = "primaryTransactionManager",
    basePackages = {
        "ai.maum.mlt.common.auth.repository", "ai.maum.mlt.common.code.repository",
        "ai.maum.mlt.common.version.repository",
        "ai.maum.mlt.dictionary.repository", "ai.maum.mlt.common.file.repository",
        "ai.maum.mlt.management.repository", "ai.maum.mlt.mrc.repository",
        "ai.maum.mlt.stt.repository", "ai.maum.mlt.ta.common.repository",
        "ai.maum.mlt.ta.dnn.repository", "ai.maum.mlt.ta.hmd.repository",
        "ai.maum.mlt.ta.xdc.repository",
        "ai.maum.mlt.twe.repository", "ai.maum.mlt.paraphrase.repository",
        "ai.maum.mlt.sds.model.repository", "ai.maum.mlt.sds.scenario.repository",
        "ai.maum.mlt.chatmonitoring.repository",
    }
)
public class DatabaseConfigPrimary {

  @Value("${spring.jpa.hibernate.ddl-auto}")
  private String ddl_auto;

  @Value("${spring.jpa.hibernate.dialect}")
  private String dialect;

  @Bean
  @Primary
  @ConfigurationProperties("datasource.primary")
  public DataSourceProperties primaryDataSourceProperties() {
    return new DataSourceProperties();
  }

  @Bean
  @Primary
  @ConfigurationProperties("datasource.primary")
  public DataSource primaryDataSource() {
    return primaryDataSourceProperties().initializeDataSourceBuilder().build();
  }

  /*
  @Bean
  @Primary
  public LocalContainerEntityManagerFactoryBean primaryEntityManagerFactory(EntityManagerFactoryBuilder builder) {
    return builder
        .dataSource(primaryDataSource())
        .packages("ai.maum.mlt.common.auth.entity", "ai.maum.mlt.common.code.entity", "ai.maum.mlt.mrc.entity", "ai.maum.mlt.common.file.entity", "ai.maum.mlt.twe.entity")
        .persistenceUnit("primaryPersistenceUnit")
        .build();
  }
  */
  @Bean
  @Primary
  public LocalContainerEntityManagerFactoryBean primaryEntityManagerFactory() {
    LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
    em.setDataSource(primaryDataSource());
    em.setPackagesToScan(new String[]{
        "ai.maum.mlt.common.auth.entity", "ai.maum.mlt.common.code.entity",
        "ai.maum.mlt.common.version.entity",
        "ai.maum.mlt.dictionary.entity", "ai.maum.mlt.common.file.entity",
        "ai.maum.mlt.management.entity", "ai.maum.mlt.mrc.entity",
        "ai.maum.mlt.stt.entity", "ai.maum.mlt.ta.common.entity",
        "ai.maum.mlt.ta.dnn.entity", "ai.maum.mlt.ta.hmd.entity",
        "ai.maum.mlt.ta.xdc.entity",
        "ai.maum.mlt.twe.entity", "ai.maum.mlt.paraphrase.entity",
        "ai.maum.mlt.sds.model.entity", "ai.maum.mlt.sds.scenario.entity",
        "ai.maum.mlt.chatmonitoring.entity",
    });
    HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
    em.setJpaVendorAdapter(vendorAdapter);
    HashMap<String, Object> properties = new HashMap<>();
    properties.put("hibernate.hbm2ddl.auto", ddl_auto);
    properties.put("hibernate.dialect", dialect);
    em.setJpaPropertyMap(properties);
    return em;
  }

  @Bean
  @Primary
  public PlatformTransactionManager primaryTransactionManager(
      @Qualifier("primaryEntityManagerFactory") EntityManagerFactory primaryEntityManagerFactory) {
    return new JpaTransactionManager(primaryEntityManagerFactory);
  }
}
