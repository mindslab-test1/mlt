package ai.maum.mlt.common.auth.repository;

import ai.maum.mlt.common.auth.entity.GroupEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupRepository extends JpaRepository<GroupEntity, Integer> {

  Page<GroupEntity> findAllByName(@Param("name") String name, Pageable pageable);

  @Query(value = "SELECT d.ownerId "
      + "         FROM GroupEntity d"
      + "         WHERE d.id= :id ")
  String findOwnerId(@Param("id") Integer id);

}
