package ai.maum.mlt.common.auth.repository;

import ai.maum.mlt.common.auth.entity.AclEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AclRepository extends JpaRepository<AclEntity, Integer> {

}
