package ai.maum.mlt.common.auth.service;

import ai.maum.mlt.common.auth.entity.RoleMappingEntity;
import ai.maum.mlt.common.auth.repository.RoleMappingRepository;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleMappingServiceImpl implements RoleMappingService {

  @Autowired
  RoleMappingRepository roleMappingRepository;


  @Override
  @Transactional
  public void addRoleMapping(RoleMappingEntity roleMappingEntity) {
    roleMappingRepository.save(roleMappingEntity);
  }

  @Override
  public List<Integer> getRoleIds(String principalId) {
    List<Integer> list = null;
    list = roleMappingRepository.getRoleIds(principalId);
    return list;
  }

  @Override
  @Transactional
  public void deleteByPrincipalIdAndRoleId(String principalId, Integer roleId) {
    roleMappingRepository.deleteByPrincipalIdAndRoleId(principalId, roleId);
  }

  @Override
  public Integer findId(String principalId, Integer roleId) {
    return roleMappingRepository.findId(principalId, roleId);
  }
}
