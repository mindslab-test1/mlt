package ai.maum.mlt.common.code.service;

import ai.maum.mlt.common.code.entity.CodeEntity;
import java.util.List;

public interface CodeService {
  List<CodeEntity> getCodeList(String groupCode);

  CodeEntity insertCode(CodeEntity codeEntity);
  CodeEntity updateCode(CodeEntity codeEntity);
  void deleteCode(String id);

}