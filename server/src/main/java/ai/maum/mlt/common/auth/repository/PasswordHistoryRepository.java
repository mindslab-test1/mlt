package ai.maum.mlt.common.auth.repository;

import ai.maum.mlt.common.auth.entity.PasswordHistoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PasswordHistoryRepository extends JpaRepository<PasswordHistoryEntity, Integer> {

}