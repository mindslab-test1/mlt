package ai.maum.mlt.common.auth.service;//package ai.maum.mlt.common.auth.service;

import ai.maum.mlt.common.auth.entity.RoleEntity;
import java.util.List;

public interface RoleService {
    Integer getRoleId(String name);
    String getRoleName(Integer id);
    List<RoleEntity> findAllRoleList();
}
