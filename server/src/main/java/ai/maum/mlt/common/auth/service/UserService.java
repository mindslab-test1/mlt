package ai.maum.mlt.common.auth.service;

import ai.maum.mlt.common.auth.entity.GroupMemberEntity;
import ai.maum.mlt.common.auth.entity.RoleMappingEntity;
import ai.maum.mlt.common.auth.entity.UserEntity;
import ai.maum.mlt.dictionary.entity.PosDictionaryEntity;
import java.util.List;

public interface UserService {
    List<UserEntity> findUserList();
    UserEntity getUser(String username);
    void deleteUser(String id);
    UserEntity addUser(UserEntity userEntity, List<RoleMappingEntity> roleMappingEntityList,
        GroupMemberEntity groupMemberEntity);
    void updateUser(UserEntity userEntity, List<RoleMappingEntity> roleMappingEntityAddList, List<RoleMappingEntity> roleMappingEntityDelList);
}
