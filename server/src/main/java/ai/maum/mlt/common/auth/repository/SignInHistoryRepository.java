package ai.maum.mlt.common.auth.repository;

import ai.maum.mlt.common.auth.entity.SignInHistoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SignInHistoryRepository extends JpaRepository<SignInHistoryEntity, Integer> {

}
