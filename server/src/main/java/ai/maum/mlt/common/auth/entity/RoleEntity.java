package ai.maum.mlt.common.auth.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.LastModifiedDate;

@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "MAI_ROLE")
public class RoleEntity implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ROLE_GEN")
  @SequenceGenerator(name = "ROLE_GEN", sequenceName = "MAI_ROLE_SEQ", allocationSize = 5)
  @Column(name="ID")
  private Integer id;

  // role 이름, module과 행위가 정의되어 있음
  @Column(name = "NAME", length = 255, nullable = false)
  private String name;

  @Column(name = "DESCRIPTION", length = 512)
  private String description;

  @CreatedDate
  @Column(name = "CREATED_AT")
  private Date createdAt;

  @LastModifiedDate
  @Column(name = "UPDATED_AT")
  private Date updatedAt;
}
