package ai.maum.mlt.common.config;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Slf4j
@Scope("singleton")
@Component
public class RequestUriRole extends Exception{

  RequestUriRole() {
    this.uriMap = new HashMap<>();
  }

  private HashMap<String, Set<String>> uriMap;

  // uriMap에 Role을 key로 Uri Set 생성
  public void setUri(String role, String uri) throws Exception {
    if ("".equals(role) || role == null || "".equals(uri) || uri == null) {
      throw new Exception("Exception RequestUriRole setUri parameter is null");
    }
    if (!uriMap.containsKey(role)) {
      Set<String> uriSet = new HashSet<>();
      uriSet.add(uri);
      uriMap.put(role, uriSet);
    } else if(uriMap.containsKey(role)){
      uriMap.get(role).add(uri);
    } else {
      throw new Exception();
    }
  }

  // 요청 Uri가 Role에 포함된 Uri인지 검증
  public boolean validateUriRole(String role, String requestUri) throws Exception {
    if ("".equals(role) || role == null || "".equals(requestUri) || requestUri == null) {
      throw new Exception("Exception RequestUriRole validateUriRole parameter is null");
    }

    if (uriMap.containsKey(role)) {
      boolean containUri = false;
      for(String roleUri: this.uriMap.get(role)) {
        log.debug("{}, {}", roleUri, role);
        if (validateUriPath(roleUri.split("/"), requestUri.split("/"))) containUri = true;
      }

      return containUri;
    } else {
      return false;
    }
  }


  private boolean validateUriPath(String[] roleUri, String[] requestUri) {
    if (roleUri.length != requestUri.length) {
      return false;
    }

    boolean validate = true;
    Pattern pattern = Pattern.compile("(\\{.*\\})");

    for (int i = 0 ; i  < roleUri.length ; i++) {
      if (pattern.matcher(roleUri[i]).find()) {
        continue;
      } else {
        if (!roleUri[i].equals(roleUri[i])) {
          validate = false;
          break;
        }
      }
    }

    return validate;
  }
}
