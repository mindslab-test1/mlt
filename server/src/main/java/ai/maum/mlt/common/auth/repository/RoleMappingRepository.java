package ai.maum.mlt.common.auth.repository;

import ai.maum.mlt.common.auth.entity.RoleMappingEntity;
import java.util.Date;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleMappingRepository extends JpaRepository<RoleMappingEntity, Integer> {

  @Query(value = "SELECT d.roleId "
      + "         FROM RoleMappingEntity d"
      + "         WHERE d.principalId= :principalId ")
  List<Integer> getRoleIds(@Param("principalId") String principalId);

  @Transactional
  void deleteByPrincipalIdAndRoleId(String principalId, Integer roleId);

  @Query(value = "SELECT d.id "
      + "         FROM RoleMappingEntity d"
      + "         WHERE d.principalId= :principalId "
      + "         AND d.roleId= :roleId ")
  Integer findId(@Param("principalId") String principalId, @Param("roleId") Integer roleId);

}
