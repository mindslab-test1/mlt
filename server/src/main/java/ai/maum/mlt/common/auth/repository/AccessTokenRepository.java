package ai.maum.mlt.common.auth.repository;

import ai.maum.mlt.common.auth.entity.AccessTokenEntity;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface AccessTokenRepository extends JpaRepository<AccessTokenEntity, String> {

}
