package ai.maum.mlt.common.file.repository;

import ai.maum.mlt.common.file.entity.FileGroupEntity;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface FileGroupRepository extends JpaRepository<FileGroupEntity, String>{

  List<FileGroupEntity> findAllByPurposeAndWorkspaceIdOrderByNameAsc(String purpose, String workspaceId);

  @Modifying
  @Transactional
  @Query(value = "UPDATE FileGroupEntity f SET f.name =:name WHERE f.id =:id")
  void updateFileGroupName(@Param("id") String id, @Param("name") String name);

  Integer countAllByNameAndWorkspaceIdAndPurpose(String name, String workspaceId, String purpose);
}
