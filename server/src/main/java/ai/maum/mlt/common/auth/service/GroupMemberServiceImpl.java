package ai.maum.mlt.common.auth.service;

import ai.maum.mlt.common.auth.entity.GroupMemberEntity;
import ai.maum.mlt.common.auth.repository.GroupMemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GroupMemberServiceImpl implements GroupMemberService{

  @Autowired
  private GroupMemberRepository groupMemberRepository;

  @Override
  public void addGroupMember(GroupMemberEntity groupMemberEntity) {
    groupMemberRepository.save(groupMemberEntity);
  }

  @Override
  public Integer findGroupId(String userId) {
    return groupMemberRepository.findGroupId(userId);

  }
}
