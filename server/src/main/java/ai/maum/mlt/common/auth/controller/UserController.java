package ai.maum.mlt.common.auth.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.common.auth.entity.GroupMemberEntity;
import ai.maum.mlt.common.auth.entity.RoleEntity;
import ai.maum.mlt.common.auth.entity.RoleMappingEntity;
import ai.maum.mlt.common.auth.entity.UserEntity;
import ai.maum.mlt.common.auth.entity.WorkspaceEntity;
import ai.maum.mlt.common.auth.service.GroupMemberService;
import ai.maum.mlt.common.auth.service.RoleMappingService;
import ai.maum.mlt.common.auth.service.RoleService;
import ai.maum.mlt.common.auth.service.UserService;
import ai.maum.mlt.common.auth.service.WorkspaceService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/user")
public class UserController {

  static final Logger logger = LoggerFactory.getLogger(UserController.class);

  @Autowired
  private UserService userService;

  @Autowired
  private RoleService roleService;

  @Autowired
  private RoleMappingService roleMappingService;

  @UriRoleDesc(role = "user^R")
  @RequestMapping(value = "/findUserList", method = RequestMethod.POST)
  public ResponseEntity<?> findUserList() {
    logger.info("======= call api  POST [[api/user/findUserList]] =======");
    try {
      List<UserEntity> roleList = new ArrayList<>();
      roleList = userService.findUserList();
      return new ResponseEntity<>(roleList, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("findUserList e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "user^R")
  @RequestMapping(value = "/findRoleList", method = RequestMethod.POST)
  public ResponseEntity<?> findRoleList() {
    logger.info("======= call api  POST [[api/user/findRoleList]] =======");
    try {
      List<RoleEntity> roleEntityList = new ArrayList<>();
      roleEntityList = roleService.findAllRoleList();
      ArrayList<ArrayList<String>> rstl = new ArrayList<>();
      HashMap<String, ArrayList<String>> tempMap = new HashMap<>();
      for (RoleEntity roleEntity : roleEntityList) {
        String[] module = roleEntity.getName().split("\\^");

        if ("admin".equals(module[0])) {
          continue;
        }

        if (tempMap.containsKey(module[0])) {
          String tempRole = ((ArrayList<String>) tempMap.get(module[0])).get(1);
          tempRole += module[1];
          tempMap.get(module[0]).set(1, new String(tempRole));
        } else {
          ArrayList<String> row = new ArrayList<>();
          row.add(0, module[0]);
          row.add(1, module[1]);
          row.add(2, roleEntity.getDescription());
          row.add(3, module[0]);

          tempMap.put(module[0], row);
          rstl.add(row);
        }
      }

      return new ResponseEntity<>(rstl, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("findRoleList e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "user^R")
  @RequestMapping(value = "/getUserRoles", method = RequestMethod.POST)
  public ResponseEntity<?> findUserList(@RequestBody String id) {
    logger.info("======= call api  POST [[api/user/getUserRoles]] =======");
    try {
      List<Integer> roleIds = new ArrayList<Integer>();
      String roleId = null;
      JSONObject json = new JSONObject();
      roleIds = roleMappingService.getRoleIds(id);
      for (int i = 0; i < roleIds.size(); i++) {
        roleId = roleService.getRoleName(roleIds.get(i));
        json.put(roleId, true);
      }

      return new ResponseEntity<>(json.toString(), HttpStatus.OK);
    } catch (Exception e) {
      logger.error("findUserList e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }


  @UriRoleDesc(role = "user^D")
  @RequestMapping(value = "/deleteUser", method = RequestMethod.POST)
  public ResponseEntity<?> deleteUser(@RequestBody List<HashMap<String, String>> idList) {
    logger.info("======= call api  POST [[api/user/deleteUser]] =======");
    try {

      for (int i = 0; i < idList.size(); i++) {
        userService.deleteUser(idList.get(i).get("id"));
      }

      return new ResponseEntity<>(null, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("deleteUser e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "user^C")
  @RequestMapping(value = "/addUser", method = RequestMethod.POST)
  public ResponseEntity<?> addUser(@RequestBody UserEntity userEntity) {
    logger.info("======= call api  POST [[api/user/addUser]] =======");
    try {

      String hashPwd = BCrypt.hashpw(userEntity.getPassword(), BCrypt.gensalt());
      userEntity.setPassword(hashPwd);
      GroupMemberEntity groupMemberEntity = new GroupMemberEntity();
      HashMap<String, Object> result = new HashMap<>();
      UserEntity usr = userService.getUser(userEntity.getUsername());
      List<RoleMappingEntity> roleMappingEntityList = new ArrayList<>();
      if (usr.getId().isEmpty()) {

        for (String key : userEntity.getRoles().keySet()) {
          RoleMappingEntity roleMappingEntity = new RoleMappingEntity();
          roleMappingEntity.setPrincipalType("USER");
          roleMappingEntity.setRoleId(roleService.getRoleId(key));
          roleMappingEntityList.add(roleMappingEntity);
        }

        groupMemberEntity.setGroupId(1);
        groupMemberEntity.setRole("member");
        groupMemberEntity.setState("accepted");

        userService.addUser(userEntity, roleMappingEntityList, groupMemberEntity);

        result.put("message", "INSERT_SUCCESS");
      } else {
        result.put("message", "INSERT_DUPLICATED");
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("addUser e : " , e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }


  @UriRoleDesc(role = "user^U")
  @RequestMapping(value = "/updateUser", method = RequestMethod.POST)
  public ResponseEntity<?> updateUser(
      @RequestBody UserEntity userEntity) {
    logger.info("======= call api  POST [[api/user/updateUser]] =======");
    try {

      List<RoleMappingEntity> roleMappingEntityAddList = new ArrayList<>();
      List<RoleMappingEntity> roleMappingEntityDelList = new ArrayList<>();
      String hashPwd = BCrypt.hashpw(userEntity.getPassword(), BCrypt.gensalt());
      userEntity.setPassword(hashPwd);

      for (String key : userEntity.getRoles().keySet()) {
        RoleMappingEntity roleMappingEntity = new RoleMappingEntity();
        roleMappingEntity.setPrincipalType("USER");
        roleMappingEntity.setPrincipalId(userEntity.getId());
        roleMappingEntity.setRoleId(roleService.getRoleId(key));

        if (userEntity.getRoles().get(key) == "true") {
          roleMappingEntityAddList.add(roleMappingEntity);
        } else {
          roleMappingEntity
              .setId(roleMappingService.findId(userEntity.getId(), roleService.getRoleId(key)));
          roleMappingEntityDelList.add(roleMappingEntity);
        }
      }
      userService.updateUser(userEntity, roleMappingEntityAddList, roleMappingEntityDelList);

      return new ResponseEntity<>(HttpStatus.OK);
    } catch (Exception e) {
      logger.error("updateUser e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "user^R")
  @RequestMapping(value = "/getUserInfo", method = RequestMethod.POST)
  public ResponseEntity<?> getUserInfo(
      @RequestBody String userName) {
    logger.info("======= call api  POST [[api/user/getUserInfo]] =======");
    try {
      UserEntity userEntity = new UserEntity();
      userEntity = userService.getUser(userName);
      return new ResponseEntity<>(userEntity, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getUserInfo e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}
