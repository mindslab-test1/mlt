package ai.maum.mlt.common.version.service;

import ai.maum.mlt.common.version.entity.CommitHistoryEntity;

public interface CommitHistoryService {
  CommitHistoryEntity insertCommit(CommitHistoryEntity entity);
  void deleteCommit(CommitHistoryEntity entity);
  int getCountCommit(CommitHistoryEntity entity);
}