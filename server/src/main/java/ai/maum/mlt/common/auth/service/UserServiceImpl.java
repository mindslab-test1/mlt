package ai.maum.mlt.common.auth.service;

import ai.maum.mlt.common.auth.entity.GroupMemberEntity;
import ai.maum.mlt.common.auth.entity.RoleMappingEntity;
import ai.maum.mlt.common.auth.entity.UserEntity;
import ai.maum.mlt.common.auth.repository.GroupMemberRepository;
import ai.maum.mlt.common.auth.repository.RoleMappingRepository;
import ai.maum.mlt.common.auth.repository.UserRepository;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

  @Autowired
  UserRepository userRepository;

  @Autowired
  RoleMappingRepository roleMappingRepository;

  @Autowired
  GroupMemberRepository groupMemberRepository;

  @Override
  public List<UserEntity> findUserList() {
    return userRepository.findAllUser();
  }

  @Override
  public UserEntity getUser(String username) {
    UserEntity res = new UserEntity();
    List<UserEntity> list = userRepository.findAllByName(username);
    if (!list.isEmpty()) {
      res = list.get(0);
    } else {
      res.setId("");
    }
    return res;
  }

  @Override
  @Transactional
  public void deleteUser(String id) {
    userRepository.deleteById(id);
  }

  @Override
  @Transactional
  public UserEntity addUser(UserEntity userEntity, List<RoleMappingEntity> roleMappingEntityList,
      GroupMemberEntity groupMemberEntity) {
    userEntity.setLastUpdated(new Date());
    userEntity = userRepository.save(userEntity);

    groupMemberEntity.setUserId(userEntity.getId());
    groupMemberRepository.save(groupMemberEntity);

    for (int i = 0; i < roleMappingEntityList.size(); i++) {
      roleMappingEntityList.get(i).setPrincipalId(userEntity.getId());
    }
    roleMappingRepository.save(roleMappingEntityList);

    return userRepository.save(userEntity);

  }

  @Override
  @Transactional
  public void updateUser(UserEntity userEntity, List<RoleMappingEntity> roleMappingEntityAddList,
      List<RoleMappingEntity> roleMappingEntityDelList) {
    userEntity.setLastUpdated(new Date());
    userRepository.updateUser(userEntity.getId(), userEntity.getEmail(),
        userEntity.getActivated(), userEntity.getLastUpdated(), userEntity.getPassword());

    roleMappingRepository.save(roleMappingEntityAddList);
    roleMappingRepository.delete(roleMappingEntityDelList);

  }

}
