package ai.maum.mlt.common.util;

import javax.persistence.AttributeConverter;
import javax.persistence.Convert;
import org.springframework.beans.factory.annotation.Value;

@Convert
public class BooleanToNumberConverter implements AttributeConverter<Boolean, Integer> {

  @Value("${spring.jpa.database}")
  String database;

  @Override
  public Integer convertToDatabaseColumn(Boolean attribute) {
    return (attribute != null && attribute) ? 1 : 0;
  }

  @Override
  public Boolean convertToEntityAttribute(Integer dbData) {
    return dbData == 1 ? true : false;
  }
}
