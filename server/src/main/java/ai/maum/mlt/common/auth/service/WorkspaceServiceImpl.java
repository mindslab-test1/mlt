package ai.maum.mlt.common.auth.service;

import ai.maum.mlt.common.auth.entity.WorkspaceEntity;
import ai.maum.mlt.common.auth.repository.WorkspaceRepository;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WorkspaceServiceImpl implements WorkspaceService {

  @Autowired
  WorkspaceRepository workspaceRepository;

  @Override
  public List<WorkspaceEntity> getAllWorkspace() {
    return workspaceRepository.findAll();
  }

  @Override
  public WorkspaceEntity getWorkspace(String id) {
    return workspaceRepository.findOne(id);
  }

  @Override
  public void addWorkspace(WorkspaceEntity workspaceEntity) {
    workspaceEntity.setCreatedAt(new Date());
    workspaceEntity.setUpdatedAt(new Date());
    workspaceRepository.save(workspaceEntity);
  }

  @Override
  public List<WorkspaceEntity> findGroupWorkspace(String ownerId) {
    return workspaceRepository.findGroupWorkspace(ownerId);
  }

}
