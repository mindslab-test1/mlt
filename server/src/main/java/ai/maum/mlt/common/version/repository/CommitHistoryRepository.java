package ai.maum.mlt.common.version.repository;

import ai.maum.mlt.common.version.entity.CommitHistoryEntity;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface CommitHistoryRepository extends JpaRepository<CommitHistoryEntity, String> {
  int countCommitHistoryEntitiesByWorkspaceIdAndTypeAndFileId(String workspaceId, String type, String fileId);
  void deleteAllByWorkspaceIdAndTypeAndFileId(String workspaceId, String type, String fileId);
}