package ai.maum.mlt.common.system;

public class SystemErrMsg {

  public static final String FILE_GROUP_NAME_DUPLICATE = "Same Name group is alreay exist";
  public static final String SAME_FILE_EXIST_AT_WORKSPACE = "Same file is alreay uploaded";
  public static final String SAME_FILE_EXIST_AT_FILE_GROUP = "Same file is alreay uploaded";
  public static final String NOT_SUPPORTED_FILE_EXTENSION = "Not Support File Extension";
  public static final String FILE_INCLUDED_IN_OTHER_FILE_GROUP = "File included in other file group";
  public static final String WAV_DOES_NOT_EXIST = "Wav file is not exist";
  public static final String TRANSCRIPT_EXIST = "Transcript already exist";
  public static final String FILE_GROUP_WAV_RATE_NOT_MATCH = "Wave file rate and file group rate not match";
  public static final String NOT_SUPPORTED_WAV_RATE = "Not Support Wave Rate";
}
