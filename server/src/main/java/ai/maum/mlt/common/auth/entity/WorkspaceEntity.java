package ai.maum.mlt.common.auth.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Immutable;
import org.springframework.data.annotation.LastModifiedDate;

@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "MAI_WORKSPACE")
public class WorkspaceEntity implements Serializable{

  @Id
  @GeneratedValue(generator="system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid")
  @Column(name="ID", length = 40)
  private String id;

  // 워크스페이스 오너가 누구인지 [Group, ConsoleUser]
  @Column(name = "OWNER_TYPE", length = 512, nullable = false)
  private String ownerType;

  // 오너의 ID [Group 이면 Group Id, ConsoleUser 이면 UserId]
  @Column(name = "OWNER_ID", length = 512, nullable = false)
  private String ownerId;

  // 임의로 생성되는 Path
  @Column(name = "PATH", length = 512, nullable = false)
  private String path;

  @CreatedDate
  @Column(name = "CREATED_AT")
  private Date createdAt;

  @LastModifiedDate
  @Column(name = "UPDATED_AT")
  private Date updatedAt;
}
