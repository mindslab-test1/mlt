package ai.maum.mlt.common.auth.repository;

import ai.maum.mlt.common.auth.entity.WorkspaceEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface WorkspaceRepository extends JpaRepository<WorkspaceEntity, String> {

  @Query(value = "SELECT d "
      + "         FROM WorkspaceEntity d"
      + "         WHERE d.ownerId= :ownerId")
  List<WorkspaceEntity> findGroupWorkspace(@Param("ownerId") String ownerId);

}
