package ai.maum.mlt.common.config;

import java.util.HashMap;
import java.util.Set;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Scope("singleton")
@Component
public class RequestRole {

  RequestRole() {
    this.roleMap = new HashMap<>();
  }

  private HashMap<String, Set<String>> roleMap;

  public void setModule(String moduleName, String requestUri) {
  }
}
