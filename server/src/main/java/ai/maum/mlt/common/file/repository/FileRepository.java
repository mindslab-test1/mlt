package ai.maum.mlt.common.file.repository;

import ai.maum.mlt.common.file.entity.FileEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface FileRepository extends JpaRepository<FileEntity, String> {

  int countAllByWorkspaceIdAndPurposeAndHash(String workspaceId, String purpose, String hash);

  FileEntity findByWorkspaceIdAndPurposeAndHash(String workspaceId, String purpose, String hash);

  FileEntity findByWorkspaceIdAndId(String workspaceId, String id);

  List<FileEntity> findByIdIn(List<String> ids);

  Integer deleteById(String id);

  @Query(value = "SELECT f "
      + "         FROM FileEntity f "
      + "         WHERE f.workspaceId =:workspaceId "
      + "         AND f.purpose =:purpose "
      + "         AND f.creatorId =:creatorId "
      + "         AND (f.meta LIKE CONCAT('%', :meta ,'%') OR :meta IS NULL)"
  )
  List<FileEntity> findAllByWorkspaceIdAndPurposeAndCreatorIdAndMeta(@Param("workspaceId") String workspaceId, @Param("purpose") String purpose,
      @Param("creatorId") String creatorId, @Param("meta") String meta);

  @Query(value = "SELECT f "
      + "         FROM FileEntity f "
      + "         WHERE f.workspaceId =:workspaceId "
      + "         AND f.purpose =:purpose "
      + "         AND f.id not in(SELECT fgr.fileId "
      + "                         FROM FileGroupRelEntity fgr "
      + "                         WHERE fgr.fileGroupId =:fileGroupId )"
      + "         AND f.name like CONCAT('%',:name,'%')"
      + "         AND (f.meta LIKE CONCAT('%', :meta ,'%') OR :meta IS NULL)"
  )
  Page<FileEntity> findAllByNotInGroupId(@Param("workspaceId") String workspaceId, @Param("purpose") String purpose,
      @Param("fileGroupId") String fileGroupId, @Param("name") String name, Pageable pageable, @Param("meta") String meta);

  @Query(value = "SELECT f "
      + "         FROM FileEntity f "
      + "         WHERE f.workspaceId =:workspaceId "
      + "         AND f.purpose =:purpose "
      + "         AND f.id in(SELECT fgr.fileId "
      + "                     FROM FileGroupRelEntity fgr "
      + "                     WHERE fgr.fileGroupId =:fileGroupId )"
      + "         AND f.name like CONCAT('%',:name,'%')"
  )
  Page<FileEntity> findAllByInGroupId(@Param("workspaceId") String workspaceId, @Param("purpose") String purpose,
      @Param("fileGroupId") String fileGroupId, @Param("name") String name, Pageable pageable);

  @Query(value = "SELECT f "
      + "         FROM FileEntity f "
      + "         WHERE f.purpose =:purpose "
      + "         AND f.id in(SELECT fgr.fileId "
      + "                     FROM FileGroupRelEntity fgr "
      + "                     WHERE fgr.fileGroupId =:fileGroupId )"
      + "         AND (f.name like CONCAT('%',:name,'%') OR :name IS NULL)"
  )
  List<FileEntity> findAllByInGroupId(@Param("purpose") String purpose,
      @Param("fileGroupId") String fileGroupId, @Param("name") String name);


  @Query(value = "SELECT f "
      + "         FROM FileEntity f "
      + "         WHERE f.workspaceId =:workspaceId "
      + "         AND f.purpose =:purpose "
      + "         AND f.id in(SELECT fgr.fileId "
      + "                     FROM FileGroupRelEntity fgr "
      + "                     WHERE fgr.fileGroupId =:fileGroupId )"
      + "         AND f.name = :name"
  )
  FileEntity findByWorkspaceIdAndPurposeAndFileGroupIdAndName(@Param("workspaceId") String workspaceId, @Param("purpose") String purpose,
      @Param("fileGroupId") String fileGroupId, @Param("name") String name);

  Integer countAllByPurposeAndWorkspaceId(String purpose, String workspaceId);
}
