package ai.maum.mlt.common.util;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import maum.brain.nlp.Nlp.MorphemeEval;
import maum.brain.nlp.Nlp.Sentence;
import maum.brain.nlp.Nlp.Word;
import org.apache.commons.text.StringEscapeUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class StringUtils {

  public static final DateFormat fileDateFormat = new SimpleDateFormat("yyyyMMdd");

  public String makeMorphTripleBracket(List<Sentence> sentenceList) throws InterruptedException {

    StringBuffer tripleBracketBuff = new StringBuffer();

    tripleBracketBuff.append("[");
    for (Sentence s : sentenceList) {
      tripleBracketBuff.append("[");
      for (MorphemeEval me : s.getMorphEvalsList()) {
        tripleBracketBuff.append("['" + me.getResult().replace("+", "','").toLowerCase() + "']");
        if (s.getMorphEvalsList().indexOf(me) != (s.getMorphEvalsList().size() - 1)) {
          tripleBracketBuff.append(",");
        }
      }
      tripleBracketBuff.append("]");
      if (sentenceList.indexOf(s) != (sentenceList.size() - 1)) {
        tripleBracketBuff.append(",");
      }
    }
    tripleBracketBuff.append("]");
    String tripleBracket = tripleBracketBuff.toString();
    return tripleBracket;
  }

  public String makeWordTripleBracket(List<Sentence> sentenceList) throws InterruptedException {

    StringBuffer tripleBracketBuff = new StringBuffer();

    tripleBracketBuff.append("[");
    for (Sentence s : sentenceList) {
      tripleBracketBuff.append("[");
      for (Word me : s.getWordsList()) {
        tripleBracketBuff.append("['" + me.getText().replace("+", "','") + "']");
        if (s.getWordsList().indexOf(me) != (s.getWordsList().size() - 1)) {
          tripleBracketBuff.append(",");
        }
      }
      tripleBracketBuff.append("]");
      if (sentenceList.indexOf(s) != (sentenceList.size() - 1)) {
        tripleBracketBuff.append(",");
      }
    }
    tripleBracketBuff.append("]");
    String tripleBracket = tripleBracketBuff.toString();
    return tripleBracket;
  }

  public HashMap<String, Integer> getWordStartAndEndByMarker(String context, String word) {
    HashMap<String, Integer> result = new HashMap<>();
    String[] splitedContext = context.split("[|][|][|][|][|]");
//    String[] splitedContext = context.split("23");
    for (int i = 0; i < splitedContext.length; i++) {
      if (splitedContext[i].equals(word)) {
        int sum = 0;
        for (int j = 0; j < i; j++) {
          sum += splitedContext[j].length();
        }
        result.put("answerStart", sum);
        result.put("answerEnd", sum + splitedContext[i].length());
        break;
      }
    }
    return result;
  }

  public String removeAnswerMarker(String context) {
    String result = context.replace("|||||", "");
    return result;
  }

  public JSONObject makeJsonString(String json) {
    log.trace("MakeJsonString = {}", json);
    JSONParser parser = new JSONParser();
    Object obj = null;
    try {
      obj = parser.parse(json);
    } catch (Exception e) {
      log.error("MakeJsonString e : ", e);
    }
    JSONObject jsonObject = (JSONObject) obj;

    return jsonObject;
  }

  public static String escapeMs949(String text) {
    StringBuilder escaping = new StringBuilder();

    int length = text != null ? text.length() : 0;
    int pos = 0;
    int codePoint;
    while (pos < length) {
      codePoint = text.codePointAt(pos);
      String utf8 = new String(Character.toChars(codePoint));
      String ms949;
      try {
        ms949 = new String(utf8.getBytes("MS949"), "MS949");
      } catch (UnsupportedEncodingException e) {
        ms949 = "";
      }

      if (!utf8.equals(ms949)) {
        escaping.append("&#").append(codePoint).append(';');
      } else {
        escaping.appendCodePoint(codePoint);
      }
      pos = text.offsetByCodePoints(pos, 1);
    }

    return escaping.toString();
  }

  public static String unescapeMs949(String text) {
    return StringEscapeUtils.unescapeHtml4(text);
  }

  public static boolean isUseful(String text) {
    return text != null && text.length() > 0;
  }

  public static boolean isUseless(String text) {
    return text == null || text.isEmpty();
  }

  public static boolean equals(String text1, String text2) {
    return text1 != null ?
        text2 != null ? text1.equals(text2) : text1.isEmpty() :
        isUseless(text2);
  }

}
