package ai.maum.mlt.common.auth.entity;


import ai.maum.mlt.common.util.BooleanToNumberConverter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "MAI_SIGNIN_HISTORY")
public class SignInHistoryEntity implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SIGNIN_HISTORY_GEN")
  @SequenceGenerator(name = "SIGNIN_HISTORY_GEN", sequenceName = "MAI_SIGNIN_HISTORY_SEQ", allocationSize = 5)
  @Column(name="ID")
  private Integer id;

  // 접속 요청한 IP 정보
  // 내부 IP 까지 확인하여 저장해야함(구글 찾아보세요)
  @Column(name="REMOTE", length = 512)
  private String remote;

  // 접속 요청한 브라우저 정보
  @Column(name = "AGENT", length = 512)
  private String agent;

  // 로그인 성공여부
  @Column(name = "SIGNED")
  private String signed;

  // 로그인 성공/실패 메세지
  @Column(name = "MSG", length = 512)
  private String msg;

  @Column(name = "USER_NAME", length = 512)
  private String username;

  @CreatedDate
  @Column(name = "CREATED_AT")
  private Date createdAt;
}
