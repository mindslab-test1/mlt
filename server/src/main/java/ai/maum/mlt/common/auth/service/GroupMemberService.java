package ai.maum.mlt.common.auth.service;

import ai.maum.mlt.common.auth.entity.GroupMemberEntity;

public interface GroupMemberService {

  void addGroupMember(GroupMemberEntity groupMemberEntity);
  Integer findGroupId(String userId);
}
