package ai.maum.mlt.common.version.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "MAI_COMMIT_HISTORY")
public class CommitHistoryEntity implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COMMIT_HISTORY_INDEX_HISTORY_GEN")
  @SequenceGenerator(name = "COMMIT_HISTORY_INDEX_HISTORY_GEN", sequenceName = "MAI_COMMIT_IDX_HSTY_SEQ", allocationSize = 5)
  @Column(name = "ID", length = 40)
  private long id;

  @Column(name = "FILE_ID", length = 40, nullable = false)
  private String fileId;

  @Column(name = "TYPE", length = 40, nullable = false)
  private String type;

  @Column(name = "WORKSPACE_ID", length = 40, nullable = false)
  private String workspaceId;
}