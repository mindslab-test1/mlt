package ai.maum.mlt.common.auth.service;

import ai.maum.mlt.common.auth.entity.WorkspaceEntity;
import java.util.List;

public interface WorkspaceService {

  List<WorkspaceEntity> getAllWorkspace();
  WorkspaceEntity getWorkspace(String id);
  void addWorkspace(WorkspaceEntity workspaceEntity);
  List<WorkspaceEntity> findGroupWorkspace(String ownerId);
}
