package ai.maum.mlt.common.auth.repository;

import ai.maum.mlt.common.auth.entity.RoleEntity;
import ai.maum.mlt.common.auth.entity.UserEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<RoleEntity, Integer> {

  @Query(value = "SELECT d.id "
      + "         FROM RoleEntity d"
      + "         WHERE d.name= :name ")
  Integer getRoleId(@Param("name") String name);

  @Query(value = "SELECT d.name "
      + "         FROM RoleEntity d"
      + "         WHERE d.id= :id ")
  String getRoleName(@Param("id") Integer id);

  @Query(value = "SELECT d "
      + "         FROM RoleEntity d")
  List<RoleEntity> findAllRoleList();


}
