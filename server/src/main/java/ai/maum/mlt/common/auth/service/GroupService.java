package ai.maum.mlt.common.auth.service;

import ai.maum.mlt.common.auth.entity.GroupEntity;
import org.springframework.data.domain.Page;

public interface GroupService {

  Page<GroupEntity> findGroupList(GroupEntity groupEntity);
  GroupEntity getGroup(Integer groupId);
  void addGroup(GroupEntity groupEntity);
  void editGroup(GroupEntity groupEntity);
  void deleteGroup(Integer groupId);
  String findOwnerId(Integer id);
}
