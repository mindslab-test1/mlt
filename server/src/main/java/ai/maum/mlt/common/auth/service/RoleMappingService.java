package ai.maum.mlt.common.auth.service;

import ai.maum.mlt.common.auth.entity.RoleMappingEntity;
import java.util.List;

public interface RoleMappingService {
    void addRoleMapping(RoleMappingEntity roleMappingEntity);
    List<Integer> getRoleIds(String principalId);
    void deleteByPrincipalIdAndRoleId(String principalId, Integer roleId);
    Integer findId(String principalId, Integer roleId);
}
