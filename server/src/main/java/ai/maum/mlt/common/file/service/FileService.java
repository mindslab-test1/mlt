package ai.maum.mlt.common.file.service;

import ai.maum.mlt.common.file.entity.FileEntity;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

public interface FileService {

  FileEntity getFileInfo(String id);
  List<FileEntity> getFiles(FileEntity fileEntity);
  List<FileEntity> getFilesWithIds(String workspaceId, List<String> list);
  Page<FileEntity> getFileList(FileEntity fileEntity);
  Page<FileEntity> getGroupFileList(FileEntity fileEntity);
  List<FileEntity> selectAllGroupFileList(FileEntity fileEntity);
  Integer getFileCount(String purpose, String wokrspaceId);
  HashMap<String, Object> insertFile(MultipartFile file, String workspaceId, String path, String purpose, String extension) throws Exception;
  HashMap<String, Object> insertSttFile(MultipartFile file, String workspaceId, String path, String purpose, String extension, String fileGroupId) throws Exception;
  HashMap<String, Object> insertFileWithoutChecksum(MultipartFile file, String workspaceId,
      String path, String purpose, String extension, String userId) throws Exception;
  void deleteFile(FileEntity fileEntity) throws Exception;
  HashMap<String, Object> deleteFileWithIds(List<String> list) throws Exception;
  void deleteDictionaryFile(FileEntity fileEntity) throws Exception;
  List<FileEntity> getFileLists(List<String> idList) throws Exception;
}
