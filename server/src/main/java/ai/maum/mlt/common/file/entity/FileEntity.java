package ai.maum.mlt.common.file.entity;

import ai.maum.mlt.common.auth.entity.UserEntity;
import ai.maum.mlt.common.auth.entity.WorkspaceEntity;
import ai.maum.mlt.common.pagenation.PageParameters;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.web.multipart.MultipartFile;

@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "MAI_FILE")
public class FileEntity extends PageParameters implements Serializable {

  @Id
  @Column(name = "ID", length = 40)
  private String id;

  @Column(name = "WORKSPACE_ID", length = 40, nullable = false)
  private String workspaceId;

  @Column(name = "HASH", length = 200, nullable = false)
  private String hash;

  @Column(name = "NAME", length = 200, nullable = false)
  private String name;

  @Column(name = "TYPE", length = 200, nullable = false)
  private String type;

  @Column(name = "PURPOSE", length = 200)
  private String purpose;

  @Column(name = "FILE_SIZE", nullable = false)
  private Long size;

  @Column(name = "DURATION")
  private Integer duration;

  @Column(name = "META")
  private String meta;

  @Column(name = "CREATOR_ID", length = 40)
  private String creatorId;

  @CreatedDate
  @Column(name = "CREATED_AT")
  private Date createdAt;

  @Column(name = "UPDATER_ID", length = 40)
  private String updaterId;

  @LastModifiedDate
  @Column(name = "UPDATED_AT")
  private Date updatedAt;

  @ManyToOne
  @JoinColumn(name = "WORKSPACE_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private WorkspaceEntity workspaceEntity;

  @ManyToOne
  @JoinColumn(name = "CREATOR_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity createUserEntity;

  @ManyToOne
  @JoinColumn(name = "UPDATER_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity updateUserEntity;

  /* param values */
  @Transient
  private String fileGroupId;

  @Transient
  private List<String> ids;

 /* @Transient
  private String fileId;*/

  @JsonIgnore
  public void setFileInfo(MultipartFile file, String fileName, String hash, String workspaceId,
      String purpose, Integer duration) throws Exception {
    this.id = fileName;
    this.workspaceId = workspaceId;
    this.hash = hash;
    this.name = file.getOriginalFilename();
    this.type = file.getContentType();
    this.size = file.getSize();
    this.purpose = purpose;
    this.duration = duration;
    this.createdAt = new Date();
    this.updatedAt = new Date();
  }
}
