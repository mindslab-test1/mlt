package ai.maum.mlt.common.git;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.eclipse.jgit.api.AddCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.diff.RawTextComparator;
import org.eclipse.jgit.internal.storage.file.FileRepository;
import org.eclipse.jgit.lib.*;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.treewalk.AbstractTreeIterator;
import org.eclipse.jgit.treewalk.CanonicalTreeParser;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.eclipse.jgit.treewalk.filter.PathFilter;

import java.nio.charset.StandardCharsets;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GitManager {

  static final Logger logger = LoggerFactory.getLogger(GitManager.class);

  private File workingDirectory;

  public void setWorkingDirectory(String path) {
    File t = new File(path);
    if (!t.exists()) {
      t.mkdirs();
    }
    this.workingDirectory = t;
  }

  public void createRepository(String workspaceId) {
    try {
      FileRepositoryBuilder frb = new FileRepositoryBuilder();
      Repository repo = null;

      File gf = new File(this.workingDirectory, workspaceId);

      if (gf.exists() && gf.canWrite()) {
        deleteDirectory(gf);
      }

      try {
        Git git = Git.init().setDirectory(gf).setBare(true).call();
        StoredConfig config = git.getRepository().getConfig();
        config.setString("core", null, "worktree", gf.getCanonicalPath());
        config.save();
      } catch (IllegalStateException ise) {
        System.out.println(ise.getMessage());
      }
    } catch (Exception e) {
      logger.error("createRepository e : " , e);
    }
  }

  public boolean existRepository(String workspaceId) {
    boolean existYn = false;
    try {
      FileRepositoryBuilder frb = new FileRepositoryBuilder();
      Repository repo = null;

      File gf = new File(this.workingDirectory, workspaceId);

      if (gf.exists()) {
        existYn = true;
      } else {
        existYn = false;
      }
    } catch (Exception e) {
      logger.error("existRepository e : " , e);
    }

    return existYn;
  }

  public void createFile(String ri, String div) {
    Repository repo = null;
    File gf = new File(this.workingDirectory, ri);

    try {
      repo = new FileRepository(gf);
      Git git = new Git(repo);
//      repo = git.getRepository();
      System.out.println("== isBare : " + repo.isBare());
      String branch = repo.getFullBranch();
      System.out.println("== branch : " + branch);

      File cf = new File(gf, div);
      if (!cf.exists()) {
        cf.createNewFile();
      }
      writeFileCont(cf, "");
      AddCommand add = git.add();
      add.addFilepattern(cf.getName()).call();

      RevCommit commit = git.commit().setMessage("initial commit").call();
      System.out.println("== create : " + commit.getId().getName());

    } catch (Exception e) {
      logger.error("createFile e : " , e);
    }
  }

  public Git getGit(String repoId) {
    Repository repo = null;
    File gf = new File(this.workingDirectory, repoId);
    Git git = null;
    try {
      repo = new FileRepository(gf);
      git = new Git(repo);
    } catch (Exception e) {
      logger.error("getGit e : " , e);
    }
    return git;
  }

  public List<HashMap<String, Object>> getCommitList(String repoId, String fileId) throws Exception {
    Git git = this.getGit(repoId);
    List<HashMap<String, Object>> commitlist = new ArrayList();
    try {
      if (!existRepository(repoId)) {
        this.createRepository(repoId);
        this.createFile(repoId, fileId);
      } else {
        File gf = new File(this.workingDirectory, repoId);
        File cf = new File(gf, fileId);
        if (!cf.exists()) {
          this.createFile(repoId,fileId);
        }
      }
      
      HashMap<String, Object> map = null;
      Iterable<RevCommit> log = git.log().addPath(fileId).call();
      Iterator<RevCommit> it = log.iterator();
      while (it.hasNext()) {
        map = new HashMap<>();
        RevCommit rc = it.next();
        System.out.println(rc.toString());
        map.put("hashKey", rc.toObjectId().name());
        map.put("commitTime", rc.getCommitterIdent().getWhen());
        map.put("updaterId", rc.getCommitterIdent().getName());
        map.put("updaterEmail", rc.getCommitterIdent().getEmailAddress());

        commitlist.add(map);
      }
      return commitlist;
    } catch (Exception e) {
      throw e;
    }
  }

  public void updateFile(HashMap<String, String> value) {

    String gitPath = value.get("gitPath");
    String workspaceId = value.get("workspaceId");
    String fileName = value.get("fileName");
    String content = value.get("content");
    String messsage = value.get("messsage");
    PersonIdent sourceAuthor = new PersonIdent(value.get("name"), value.get("email"));

    this.setWorkingDirectory(gitPath);
    if (!existRepository(workspaceId)) {
      this.createRepository(workspaceId);
      this.createFile(workspaceId, fileName);
    }

    try {
      File gf = new File(this.workingDirectory, workspaceId);

      Repository repo = new FileRepository(gf);
      Git git = new Git(repo);

      File cf = new File(gf, fileName);
      if (!cf.exists()) {
        cf.createNewFile();
      }
      writeFileCont(cf, content);

      AddCommand add = git.add();
      add.addFilepattern(cf.getName()).call();

      RevCommit commit = git.commit().setAuthor(sourceAuthor).setCommitter(sourceAuthor)
          .setMessage(messsage).call();

      System.out.println("== update : " + commit.toObjectId().name());
    } catch (Exception e) {
      logger.error("updateFile e : " , e);
    }
  }

  public String readFileFromCommit(String ri, String div, ObjectId commitId) {
    Repository repo = null;
    File gf = new File(this.workingDirectory, ri);
    String result = "";
    try {
      repo = new FileRepository(gf);
      RevWalk rw = new RevWalk(repo);
      RevCommit rc = rw.parseCommit(commitId);
      RevTree rt = rc.getTree();
      try (TreeWalk tw = new TreeWalk(repo)) {
        tw.addTree(rt);
        tw.setRecursive(true);
        tw.setFilter(PathFilter.create(div)); // set file name
        if (!tw.next()) {
          throw new IllegalStateException("Did not find expected file 'README.md'");
        }
        ObjectId objectId = tw.getObjectId(0);
        ObjectLoader loader = repo.open(objectId);
        result = new String(loader.getBytes(), StandardCharsets.UTF_8.displayName());
      }
      rw.dispose();

    } catch (Exception e) {
      logger.error("readFileFromCommit e : " , e);
    }
    return result;
  }

  public String diffFile(String ri, String div, String src, String tgt) {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    Repository repo = null;
    File gf = new File(this.workingDirectory, ri);
    try {
      repo = new FileRepository(gf);
      AbstractTreeIterator otp = prepareTreeParser(repo, src);
      AbstractTreeIterator ntp = prepareTreeParser(repo, tgt);

      // then the porcelain diff-command returns a list of diff entries
      try (Git git = new Git(repo)) {
        List<DiffEntry> diff = git.diff().
            setOldTree(otp).
            setNewTree(ntp).
            setPathFilter(PathFilter.create(div)).call();
        for (DiffEntry entry : diff) {
          System.out.println("== Entry : " + entry);
          System.out.println("== from : " + entry.getOldId());
          System.out.println("== to : " + entry.getNewId());

          DiffFormatter df = new DiffFormatter(baos);
          //df.setOldPrefix("old_");
          //df.setNewPrefix("new_");
          df.setDiffComparator(RawTextComparator.DEFAULT);
          df.setRepository(repo);
          df.format(entry);

          //return entry;
          return baos.toString(StandardCharsets.UTF_8.displayName());
        }
      }
    } catch (Exception e) {

    }
    return null;
  }

  private static AbstractTreeIterator prepareTreeParser(Repository repository, String objectId)
      throws IOException {
    try (RevWalk walk = new RevWalk(repository)) {
      RevCommit commit = walk.parseCommit(repository.resolve(objectId));
      RevTree tree = walk.parseTree(commit.getTree().getId());
      CanonicalTreeParser treeParser = new CanonicalTreeParser();
      try (ObjectReader reader = repository.newObjectReader()) {
        treeParser.reset(reader, tree.getId());
      }
      walk.dispose();
      return treeParser;
    }
  }

  private boolean deleteDirectory(File path) {
    if (!path.exists()) {
      return false;
    }
    File[] files = path.listFiles();
    for (File file : files) {
      if (file.isDirectory()) {
        deleteDirectory(file);
      } else {
        file.delete();
      }
    }
    return path.delete();
  }

  private void writeFileCont(File cf, String cont) {
    BufferedWriter bw = null;
    try {
      bw = new BufferedWriter(new FileWriter(cf));
      bw.write(cont);
      bw.flush();
    } catch (IOException e) {
      logger.error("writeFileCont e : " , e);
    } finally {
      if (bw != null) {
        try {
          bw.close();
        } catch (IOException e) {
        }
      }
    }
  }

  private String readFileCont(File cf) {
    BufferedReader br = null;
    StringBuffer sb = new StringBuffer();
    try {
      br = new BufferedReader(new FileReader(cf));
      String line;
      while ((line = br.readLine()) != null) {
        sb.append(line + System.lineSeparator());
      }
    } catch (FileNotFoundException fnfe) {
      logger.error("readFileCont e : " , fnfe);
    } catch (IOException e) {
      logger.error("readFileCont e : " , e);
    } finally {
      if (br != null) {
        try {
          br.close();
        } catch (IOException e) {
        }
      }
    }
    return sb.toString();
  }
}