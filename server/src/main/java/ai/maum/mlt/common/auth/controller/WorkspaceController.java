package ai.maum.mlt.common.auth.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.common.auth.entity.WorkspaceEntity;
import ai.maum.mlt.common.auth.service.GroupMemberService;
import ai.maum.mlt.common.auth.service.GroupService;
import ai.maum.mlt.common.auth.service.WorkspaceService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController()
@RequestMapping("api/workspace")
public class WorkspaceController {

  @Autowired
  private GroupService groupService;

  @Autowired
  private GroupMemberService groupMemberService;

  @Autowired
  private WorkspaceService workspaceService;

  @UriRoleDesc(role = "workspace^R")
  @RequestMapping(value = "/getWorkspaces", method = RequestMethod.POST)
  public ResponseEntity<?> getWorkspaces(@RequestBody String id){
    log.info("======= call api  POST [[api/group/getWorkspaces]] =======");
    log.trace("param : {}", id);

    Integer groupId = 0;
    String ownerID = null;
    List<WorkspaceEntity> workspaces = new ArrayList<>();

    try {
      if (groupMemberService.findGroupId(id) != null) {
        groupId = groupMemberService.findGroupId(id);
        ownerID = groupService.findOwnerId(groupId);
        workspaces = workspaceService.findGroupWorkspace(ownerID);
      }else{
        throw new Exception("Invalid Argument : Group Id");
      }

      return new ResponseEntity<>(workspaces, HttpStatus.OK);

    } catch (Exception e) {
      log.error("getWorkspaces e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
