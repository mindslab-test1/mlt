package ai.maum.mlt.common.auth.repository;

import ai.maum.mlt.common.auth.entity.GroupMemberEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupMemberRepository extends JpaRepository<GroupMemberEntity, Integer> {

  @Query(value = "SELECT d.groupId "
      + "         FROM GroupMemberEntity d"
      + "         WHERE d.userId= :userId ")
  Integer findGroupId(@Param("userId") String userId);

}
