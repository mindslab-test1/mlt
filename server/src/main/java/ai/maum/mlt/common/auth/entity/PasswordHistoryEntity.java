package ai.maum.mlt.common.auth.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "MAI_PASSWORD_HISTORY")
public class PasswordHistoryEntity implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PASSWORD_HISTORY_GEN")
  @SequenceGenerator(name = "PASSWORD_HISTORY_GEN", sequenceName = "MAI_PASSWORD_HISTORY_SEQ", allocationSize = 5)
  @Column(name="ID")
  private Integer id;

  @Column(name="USER_ID")
  private Integer userId;

  @Column(name = "PASSWORD", length = 512)
  private String password;

  @Column(name = "LAST_UPDATED")
  private Date lastUpdated;
}
