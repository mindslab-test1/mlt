package ai.maum.mlt.common.code.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "MAI_CODE")
public class CodeEntity implements Serializable {

  @Id
  @Column(name = "ID", length = 10, nullable = false)
  private String id;

  @Column(name = "NAME", length = 40, nullable = false)
  private String name;

  @Column(name = "GROUP_CODE", length = 40, nullable = false)
  private String groupCode;

  @Column(name = "DESCRIPTION", length = 512)
  private String description;
}
