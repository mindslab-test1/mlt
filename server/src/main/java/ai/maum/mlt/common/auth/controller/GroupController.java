package ai.maum.mlt.common.auth.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.common.auth.entity.GroupEntity;
import ai.maum.mlt.common.auth.service.GroupService;
import java.util.HashMap;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController()
@RequestMapping("api/group")
public class GroupController {

  @Autowired
  private GroupService groupService;

  @UriRoleDesc(role = "group^R")
  @RequestMapping(value = "/findGroupList", method = RequestMethod.POST)
  public ResponseEntity<?> findGroupList(@RequestBody GroupEntity param){
    log.info("======= call api  POST [[api/group/findGroupList]] =======");
    log.trace("param : {}", param);

    try {

      Page<GroupEntity> groupEntityList = groupService.findGroupList(param);
      return new ResponseEntity<>(groupEntityList, HttpStatus.OK);

    } catch (Exception e) {
      log.error("findGroupList e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "group^R")
  @RequestMapping(value = "/getGroup", method = RequestMethod.POST)
  public ResponseEntity<?> getGroup(@RequestBody GroupEntity param){
    log.info("======= call api  POST [[api/group/getGroup]] =======");
    log.trace("param : {}", param);

    try {
      if (param.getId() == null || param.getId() < 1) {
        throw new Exception("Invalid Argument : Group Id");
      }

      GroupEntity groupEntity = groupService.getGroup(param.getId());
      return new ResponseEntity<>(groupEntity, HttpStatus.OK);

    } catch (Exception e) {
      log.error("getGroup e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "group^C")
  @RequestMapping(value = "/addGroup", method = RequestMethod.POST)
  public ResponseEntity<?> addGroup(@RequestBody GroupEntity param){
    log.info("======= call api  POST [[api/group/addGroup]] =======");
    log.trace("param : {}", param);
    log.trace("param : {}", param);

    try {

      if (param.getId() == null || param.getId() < 1) {
        throw new Exception("Invalid Argument : Group Name");
      }

      groupService.addGroup(param);
      return new ResponseEntity<>(HttpStatus.OK);

    } catch (Exception e) {
      log.error("addGroup e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "group^U")
  @RequestMapping(value = "/editGroup", method = RequestMethod.POST)
  public ResponseEntity<?> editGroup(@RequestBody GroupEntity param){
    log.info("======= call api  POST [[api/group/editGroup]] =======");
    log.trace("param : {}", param);

    try {

      if (param.getId() == null || param.getId() < 1) {
        throw new Exception("Invalid Argument : Group Id");
      }

      if (param.getName() == null || "".equals(param.getName())) {
        throw new Exception("Invalid Argument : Group Name");
      }

      groupService.editGroup(param);
      return new ResponseEntity<>(HttpStatus.OK);

    } catch (Exception e) {
      log.error("editGroup e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "group^D")
  @RequestMapping(value = "/deleteGroup", method = RequestMethod.POST)
  public ResponseEntity<?> deleteGroup(@RequestBody GroupEntity param){
    log.info("======= call api  POST [[api/group/deleteGroup]] =======");
    log.trace("param : {}", param);

    try {

      if (param.getId() == null || param.getId() < 1) {
        throw new Exception("Invalid Argument : Group Id");
      }

      groupService.deleteGroup(param.getId());
      return new ResponseEntity<>(HttpStatus.OK);

    } catch (Exception e) {
      log.error("deleteGroup e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
