package ai.maum.mlt.common.file.service;

import ai.maum.mlt.common.file.entity.FileGroupEntity;
import ai.maum.mlt.common.file.entity.FileGroupRelEntity;
import ai.maum.mlt.common.file.repository.FileGroupRelRepository;
import ai.maum.mlt.common.file.repository.FileGroupRepository;
import ai.maum.mlt.common.system.SystemErrMsg;
import java.util.Date;
import java.util.List;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.method.P;
import org.springframework.stereotype.Service;

@Service
public class FileGroupServiceImpl implements FileGroupService{

  static final Logger logger = LoggerFactory.getLogger(FileGroupServiceImpl.class);

  @Autowired
  private FileGroupRepository fileGroupRepository;

  @Autowired
  private FileGroupRelRepository fileGroupRelRepository;

  @Override
  public List<FileGroupEntity> getFileGroupList(String purpose, String workspaceId) {
    return fileGroupRepository.findAllByPurposeAndWorkspaceIdOrderByNameAsc(purpose, workspaceId);
  }

  @Override
  public void insertFileGroup(FileGroupEntity fileGroupEntity, String purpose) throws Exception {
    try {
      if (fileGroupRepository.
          countAllByNameAndWorkspaceIdAndPurpose(fileGroupEntity.getName(), fileGroupEntity.getWorkspaceId(), purpose)
          != 0) {
        throw new Exception(SystemErrMsg.FILE_GROUP_NAME_DUPLICATE);
      }
      fileGroupEntity.setCreatedAt(new Date());
      fileGroupEntity.setPurpose(purpose);
      fileGroupRepository.save(fileGroupEntity);
    } catch (Exception e) {
      logger.error(e.getMessage());
      throw e;
    }
    fileGroupRepository.save(fileGroupEntity);
  }

  @Override
  public void updateFileGroup(FileGroupEntity fileGroupEntity) {
    fileGroupEntity.setUpdatedAt(new Date());

    fileGroupRepository.updateFileGroupName(fileGroupEntity.getId(), fileGroupEntity.getName());
  }

  @Override
  public void deleteFileGroup(String id) {
    fileGroupRelRepository.deleteAllByFileGroupId(id);
    fileGroupRepository.delete(id);
  }

  @Override
  public void insertFileGroupRel(List<FileGroupRelEntity> fileGroupRelEntities) {
    for(FileGroupRelEntity fileGroupRelEntity : fileGroupRelEntities){
      fileGroupRelEntity.setCreatedAt(new Date());
    }

    fileGroupRelRepository.save(fileGroupRelEntities);
  }

  @Override
  @Transactional
  public void deleteFileGroupRel(List<FileGroupRelEntity> fileGroupRelEntities) {
    for(FileGroupRelEntity fileGroupRelEntity: fileGroupRelEntities){
      fileGroupRelRepository.deleteByFileGroupIdAndFileId(fileGroupRelEntity.getFileGroupId(), fileGroupRelEntity.getFileId());
    }
  }
}
