package ai.maum.mlt.common.hzc;

import ai.maum.m2u.common.portable.LoggerPortableFactory;
import ai.maum.m2u.common.portable.PortableClassId;
import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.config.GroupConfig;
import com.hazelcast.core.HazelcastInstance;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class HazelcastConnector {

  public static HazelcastInstance client;

  @Value("${hazelcast.group.name}")
  String name;

  @Value("${hazelcast.group.password}")
  String password;

  @Value("${hazelcast.server.ip.1}")
  String address;

  @Value("${hazelcast.server.port}")
  String port;

  @Value("${hazelcast.invocation.timeout}")
  String timeOut;

  private HazelcastConnector() {
  }

  @PostConstruct
  public void init() {
    if (HazelcastConnector.client == null
        || !HazelcastConnector.client.getLifecycleService().isRunning()) {
      List<String> addrList = new ArrayList<>();

      if (address != null && !"".equals(address) && port != null && !"".equals(port)) {
        addrList.add(address.concat(":").concat(port));
      }

      this.connect(name, password, addrList);
      log.info("###this.connect(name, password, addrList) = {}", addrList.toString());
    }
  }

  public static HazelcastConnector getInstance() {
    return Singleton.instance;
  }

  private void connect(String name, String password, List<String> addrList) {
    ClientConfig clientConfig = new ClientConfig();

    clientConfig.setGroupConfig(new GroupConfig(name, password));
    clientConfig.getNetworkConfig().setAddresses(addrList);

    try {
      int timeout = Integer.parseInt(timeOut);
      if (timeout > 0) {
        clientConfig.setProperty("hazelcast.client.invocation.timeout.seconds", timeOut);
      }
    } catch (Exception e) {
      log.warn("Hazelcast connect {} => ", e.getMessage(), e);
    }

    clientConfig.getSerializationConfig()
        .addPortableFactory(PortableClassId.FACTORY_ID, new LoggerPortableFactory());

    try {
      HazelcastConnector.client = HazelcastClient.newHazelcastClient(clientConfig);
    } catch (Exception e) {
      log.debug("hzc connection failed!!");
    }

    log.info("hzc connection success");
  }

  public void release() {
    log.info("hzc release success");
    HazelcastConnector.client.shutdown();
  }

  private static class Singleton {

    private static final HazelcastConnector instance = new HazelcastConnector();
  }
}
