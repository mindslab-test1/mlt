package ai.maum.mlt.common.config;

import javax.servlet.MultipartConfigElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.support.MultipartFilter;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {

  @Autowired
  private AuthInterceptor authInterceptor;

  @Value("${mlt.cors.allow.option}")
  private String CORS_OPTION;

  @Value("${mlt.cors.allow.mapping}")
  private String[] CORS_MAPPINGS;

  @Value("${multipart.maxFileSize}")
  private String maxFileSize;

  @Value("${multipart.maxRequestSize}")
  private String maxRequestSize;

  private String[] corsOptionArr = {"default", "all"};

  @Override
  public void addInterceptors(InterceptorRegistry registry) {
    registry.addInterceptor(authInterceptor)
        .addPathPatterns("/api/**");
  }

  @Bean
  public WebMvcConfigurer corsConfigurer() {
    if (corsOptionArr[0].equals(CORS_OPTION.toLowerCase())) {
      return new WebMvcConfigurerAdapter() {
        @Override
        public void addCorsMappings(CorsRegistry registry) {
          for (String mapping : CORS_MAPPINGS) {
            String pathPattern = mapping.split("@")[0];
            String origins = mapping.split("@")[1] + "://" + mapping.split("@")[2] + (
                mapping.split("@").length >= 4 ? ":" + mapping.split("@")[3] : "");
            registry.addMapping(pathPattern).allowedOrigins(origins).allowedMethods(
                HttpMethod.GET.name(), HttpMethod.HEAD.name(), HttpMethod.POST.name(),
                HttpMethod.PUT.name(), HttpMethod.DELETE.name(), HttpMethod.OPTIONS.name());
          }
        }
      };
    } else if (corsOptionArr[1].equals(CORS_OPTION.toLowerCase())) {
      return new WebMvcConfigurerAdapter() {
        @Override
        public void addCorsMappings(CorsRegistry registry) {
          registry.addMapping("/api/**");
        }
      };
    } else {
      return new WebMvcConfigurerAdapter() {
        @Override
        public void addCorsMappings(CorsRegistry registry) {
          registry.addMapping("/**");
        }
      };
    }
  }

  @Bean
  public MultipartConfigElement multipartConfigElement() {
    MultipartConfigFactory factory = new MultipartConfigFactory();

    factory.setMaxFileSize(maxFileSize);
    factory.setMaxRequestSize(maxRequestSize);

    return factory.createMultipartConfig();
  }

  @Bean
  public MultipartResolver multipartResolver() {
    return new StandardServletMultipartResolver();
  }

  @Bean
  @Order(0)
  public MultipartFilter multipartFilter() {
    MultipartFilter multipartFilter = new MultipartFilter();
    multipartFilter.setMultipartResolverBeanName("multipartResolver");
    return multipartFilter;
  }

}
