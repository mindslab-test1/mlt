package ai.maum.mlt.common.model;

import java.util.ArrayList;
import java.util.List;

/**
 * SessionLogger에서 sessionStatusLogger에게 넘길 객체입니다
 * SessionStatItem은 chatbot, channel, count로 구성되어 있습니다
 */
public class SessionStat {

  private List<SessionStatItem> sessionStatItems;

  public SessionStat() {
    sessionStatItems = new ArrayList<>();
  }

  /**
   * SessionStatItem 객체 입니다
   * chatbot : 챗봇명
   * channel : 채널명
   * count : 세션수
   */
  public class SessionStatItem {
    public String chatbot;
    public String channel;
    public int count;

    public SessionStatItem(String chatbot, String channel, int count) {
      this.chatbot = chatbot;
      this.channel = channel;
      this.count = count;
    }

    @Override
    public String toString() {
      return "SessionStatItem{" +
          "chatbot='" + chatbot + '\'' +
          ", channel='" + channel + '\'' +
          ", count=" + count +
          '}';
    }
  }

  /**
   * sessionStatItems 리스트에 값을 입력합니다
   *
   * @param chatbot 로그 통계를 위한 챗봇
   * @param channel 로그 통계를 위한 채널
   * @param cnt 챗봇 및 채널 별 개수
   */
  public void put(String chatbot, String channel, int cnt) {
    SessionStatItem sessionStatItem = new SessionStatItem(chatbot, channel, cnt);

    sessionStatItems.add(sessionStatItem);
  }

  /**
   * 세션수를 리턴하는 함수 입니다
   *
   * @return
   */
  public int size() {
   return sessionStatItems.size();
  }

  public SessionStatItem get(int index) {

    return sessionStatItems.get(index);
  }

  @Override
  public String toString() {
    return "SessionStat { sessionStatItems=" + sessionStatItems + '}';
  }
}
