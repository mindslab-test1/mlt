package ai.maum.mlt.basicqa.service;

import ai.maum.mlt.basicqa.entity.BasicQACategoryEntity;
import ai.maum.mlt.basicqa.repository.BasicQACategoryRepository;
import ai.maum.mlt.common.code.entity.CodeEntity;
import ai.maum.mlt.common.code.repository.CodeRepository;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BasicQACategoryServiceImpl implements BasicQACategoryService {
  static final Logger logger = LoggerFactory.getLogger(BasicQACategoryServiceImpl.class);

  @Autowired
  private BasicQACategoryRepository basicQACategoryRepository;

  @Autowired
  private CodeRepository codeRepository;

  @Override
  public BasicQACategoryEntity findByParCatIdAndCatName(String parCatId, String catName) {
    return basicQACategoryRepository.findByParCatIdAndCatName(parCatId, catName);
  }

  @Override
  public List<BasicQACategoryEntity> findCategoryAllList() {
    return basicQACategoryRepository.findAll();
  }

  @Override
  public HashMap<String, List<BasicQACategoryEntity>> findCategoryTree(String rootGrpCd) {

    HashMap<String, List<BasicQACategoryEntity>> resultMap = new HashMap<String, List<BasicQACategoryEntity>>();
    // rootGrpCd로 MLT_CODE 테이블 조회
    List<CodeEntity> codeEntityList = codeRepository.findAllByGroupCode(rootGrpCd);
    // MLT_BSQA_CATEGORY 리스트 전체 조회
    List<BasicQACategoryEntity> categoryList = findCategoryAllList();

    for (CodeEntity codeEntity : codeEntityList) {
      List<BasicQACategoryEntity> rootCategoryEntity = new ArrayList<>();

      for (BasicQACategoryEntity categoryEntity : categoryList) {
        // MLT_CODE에 등록된 root category만 추출하여 resultMap에 저장
        if (codeEntity.getName().equals(categoryEntity.getParCatId())) {
          rootCategoryEntity
              .add(getChildCategory(categoryList, categoryEntity, categoryEntity.getId()));
        }
      }

      logger.debug("BSQA codeEntity.getName() [{}] rootCategoryEntity[{}]", codeEntity.getName(), rootCategoryEntity);

      resultMap.put(codeEntity.getName(), rootCategoryEntity);
    }

    return resultMap;
  }

  private BasicQACategoryEntity getChildCategory(List<BasicQACategoryEntity> categoryList,
      BasicQACategoryEntity currentCategoryEntity, String parentCategoryId) {

    for (BasicQACategoryEntity categoryEntity : categoryList) {
      List<BasicQACategoryEntity> childrenObject = currentCategoryEntity.getChildren();
      if (parentCategoryId.equals(categoryEntity.getParCatId())) {
        childrenObject.add(getChildCategory(categoryList, categoryEntity, categoryEntity.getId()));
      }
    }

    return currentCategoryEntity;
  }

  @Override
  public BasicQACategoryEntity updateCategory(BasicQACategoryEntity category) {
    return basicQACategoryRepository.saveAndFlush(category);
  }

  @Override
  public BasicQACategoryEntity insertCategory(BasicQACategoryEntity category) {
    return basicQACategoryRepository.save(category);
  }

  @Override
  public boolean deleteCategory(String id) {
    try {
      basicQACategoryRepository.delete(id);
      return true;
    } catch (Exception e) {
      return false;
    }
  }
}
