package ai.maum.mlt.basicqa.service;

import ai.maum.mlt.basicqa.entity.BasicQAParaphraseMainWordEntity;
import java.util.List;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

public interface BasicQAParaphraseMainWordService {

  List<BasicQAParaphraseMainWordEntity> selectListByWord(String word);

  Page<BasicQAParaphraseMainWordEntity> selectList(String useYn,
      String searchMainWord, String searchWords, Pageable pageable);

  List<BasicQAParaphraseMainWordEntity> selectListByWordList(List<String> word);

  BasicQAParaphraseMainWordEntity insertWords(
      BasicQAParaphraseMainWordEntity basicQAParaphraseMainWordEntity);

  BasicQAParaphraseMainWordEntity updateWord(
      BasicQAParaphraseMainWordEntity basicQAParaphraseMainWordEntity);

  void deleteWord(List<BasicQAParaphraseMainWordEntity> basicQAParaphraseMainWordEntities);

  void uploadFiles(List<MultipartFile> files) throws Exception;

  InputStreamResource downloadFile() throws Exception;
}
