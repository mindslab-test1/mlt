package ai.maum.mlt.basicqa.service;

import ai.maum.mlt.basicqa.entity.BasicQASkillEntity;
import java.util.List;

public interface BasicQASkillService {

  List<BasicQASkillEntity> selectList();

  BasicQASkillEntity selectOneByName(String name);

  BasicQASkillEntity selectOneById(Integer id);

  BasicQASkillEntity insertSkill(BasicQASkillEntity basicQASkillEntity);

  BasicQASkillEntity updateSkill(BasicQASkillEntity basicQASkillEntity);

  void deleteSkills(List<BasicQASkillEntity> basicQASkillEntities);
}
