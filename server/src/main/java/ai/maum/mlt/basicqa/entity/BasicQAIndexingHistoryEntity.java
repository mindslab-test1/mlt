package ai.maum.mlt.basicqa.entity;

import ai.maum.mlt.common.auth.entity.UserEntity;
import ai.maum.mlt.common.pagenation.PageParameters;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Date;
import javax.persistence.*;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "MAI_BQA_INDEXING_HISTORY")
public class BasicQAIndexingHistoryEntity extends PageParameters {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BQA_INDEXING_HISTORY_GEN")
  @SequenceGenerator(name = "BQA_INDEXING_HISTORY_GEN", sequenceName = "MAI_BQA_IDX_HISTORY_SEQ", allocationSize = 5)
  @Column(name = "ID")
  private Integer id;

  @Column(name = "TYPE", length = 30)
  private String type;

  @Column(name = "STATUS")
  private Boolean status;

  @Column(name = "TOTAL")
  private Integer total;

  @Column(name = "FETCHED")
  private Integer fetched;

  @Column(name = "PROCESSED")
  private Integer processed;

  @Column(name = "MESSAGE", length = 100)
  private String message;

  @Column(name = "STOP_YN")
  private Boolean stopYn;

  @Column(name = "ADDRESS")
  private String address;

  @Column(name = "CREATOR_ID", length = 40)
  private String creatorId;

  @CreatedDate
  @Column(name = "CREATED_AT")
  private Date createdAt;

  @Column(name = "UPDATER_ID", length = 40)
  private String updaterId;

  @LastModifiedDate
  @Column(name = "UPDATED_AT")
  private Date updatedAt;

  @ManyToOne
  @JoinColumn(name = "CREATOR_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity createUserEntity;

  @ManyToOne
  @JoinColumn(name = "UPDATER_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity updateUserEntity;
}
