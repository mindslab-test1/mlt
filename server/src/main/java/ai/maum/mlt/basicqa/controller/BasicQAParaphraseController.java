package ai.maum.mlt.basicqa.controller;

import ai.maum.mlt.basicqa.entity.BasicQAParaphraseMainWordEntity;
import ai.maum.mlt.basicqa.entity.BasicQAParaphraseRelEntity;
import ai.maum.mlt.basicqa.entity.BasicQAParaphraseWordEntity;
import ai.maum.mlt.basicqa.service.BasicQAParaphraseMainWordService;
import ai.maum.mlt.basicqa.service.BasicQAParaphraseRelService;
import ai.maum.mlt.basicqa.service.BasicQAParaphraseWordService;
import ai.maum.mlt.common.annotation.UriRoleDesc;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController()
@RequestMapping("api/basicqa/paraphrase")
public class BasicQAParaphraseController {

  static final Logger logger = LoggerFactory.getLogger(
      BasicQAParaphraseController.class);

  @Value("${excel.bsqa.paraphrase.filename}")
  private String bsqaFileName;

  @Autowired
  private BasicQAParaphraseMainWordService basicQAParaphraseMainWordService;

  @Autowired
  private BasicQAParaphraseWordService basicQAParaphraseWordService;

  @Autowired
  private BasicQAParaphraseRelService basicQAParaphraseRelService;

  @UriRoleDesc(role = "basicqa^R")
  @RequestMapping(
      value = "/checkMainWordExist",
      method = RequestMethod.POST)
  public ResponseEntity<?> checkWordExist(@RequestBody List<String> wordList) {
    logger.info("======= call api  [[/api/basicqa/paraphrase/checkMainWordExist]] =======");
    try {
      List<BasicQAParaphraseMainWordEntity> basicQAParaphraseMainWordEntities = basicQAParaphraseMainWordService
          .selectListByWordList(wordList);
      if (basicQAParaphraseMainWordEntities.size() > 0) {
        return new ResponseEntity<>(null, HttpStatus.CONFLICT);
      } else {
        return new ResponseEntity<>(null, HttpStatus.OK);
      }
    } catch (Exception e) {
      logger.error("checkWordExist e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "basicqa^R")
  @RequestMapping(
      value = "/getMainWords",
      method = RequestMethod.POST)
  public ResponseEntity<?> getBasicQAItemList(
      @RequestBody BasicQAParaphraseMainWordEntity basicQAParaphraseMainWordEntityParam) {
    logger.info("======= call api  [[/api/basicqa/paraphrase/getMainWords]] =======");

    try {
      Page<BasicQAParaphraseMainWordEntity> basicQAParaphraseMainWordEntities
          = basicQAParaphraseMainWordService
          .selectList(
              basicQAParaphraseMainWordEntityParam.getUseYn(),
              basicQAParaphraseMainWordEntityParam.getSearchMainWord(),
              basicQAParaphraseMainWordEntityParam.getSearchWords(),
              basicQAParaphraseMainWordEntityParam.getPageRequest());

      for (BasicQAParaphraseMainWordEntity basicQAParaphraseMainWordEntity : basicQAParaphraseMainWordEntities) {
        basicQAParaphraseMainWordEntity.setBasicQAParaphraseRelEntities(basicQAParaphraseRelService
            .selectListByMainId(basicQAParaphraseMainWordEntity.getId()));
      }

      return new ResponseEntity<>(basicQAParaphraseMainWordEntities, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getBasicQAItemList e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "basicqa^C")
  @RequestMapping(
      value = "/addMainWord",
      method = RequestMethod.POST)
  public ResponseEntity<?> add(
      @RequestBody BasicQAParaphraseMainWordEntity basicQAParaphraseMainWordEntityParam) {
    logger.info("======= call api  [[/api/basicqa/paraphrase/addMainWord]] =======");

    try {
      if (basicQAParaphraseMainWordService
          .selectListByWord(basicQAParaphraseMainWordEntityParam.getWord()).size() != 0) {
        return new ResponseEntity<>(null, HttpStatus.CONFLICT);
      }

      if (basicQAParaphraseWordService
          .selectListByWord(basicQAParaphraseMainWordEntityParam.getWord()).size() != 0) {
        return new ResponseEntity<>(null, HttpStatus.CONFLICT);
      }

      basicQAParaphraseMainWordEntityParam.setBasicQAParaphraseRelEntities(null);
      BasicQAParaphraseMainWordEntity insertedMainWordEntity = basicQAParaphraseMainWordService
          .insertWords(basicQAParaphraseMainWordEntityParam);
      return new ResponseEntity<>(insertedMainWordEntity, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("add e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "basicqa^C")
  @RequestMapping(
      value = "/addWords",
      method = RequestMethod.POST)
  public ResponseEntity<?> addWord(
      @RequestBody List<BasicQAParaphraseWordEntity> basicQAParaphraseWordEntitiesParam) {
    logger.info("======= call api  [[/api/basicqa/paraphrase/addWords]] =======");

    try {
      List<String> wordList = new ArrayList<>();
      for (BasicQAParaphraseWordEntity basicQAParaphraseWordEntity : basicQAParaphraseWordEntitiesParam) {
        wordList.add(basicQAParaphraseWordEntity.getWord());
      }
      if (basicQAParaphraseMainWordService.selectListByWordList(wordList).size() > 0) {
        return new ResponseEntity<>(null, HttpStatus.CONFLICT);
      }
//      if (basicQAParaphraseWordService.selectListByWordList(wordList).size() > 0) {
//        return new ResponseEntity<>(null, HttpStatus.CONFLICT);
//      }
      List<BasicQAParaphraseWordEntity> toBeInsertedWordEntities = new ArrayList<>();
      List<BasicQAParaphraseWordEntity> insertedWordEntities = new ArrayList<>();
      for (BasicQAParaphraseWordEntity basicQAParaphraseWordEntity : basicQAParaphraseWordEntitiesParam) {
        if (basicQAParaphraseWordService.selectListByWord(basicQAParaphraseWordEntity.getWord())
            .size() == 0) {
          toBeInsertedWordEntities.add(basicQAParaphraseWordEntity);
        } else {
          basicQAParaphraseWordEntity.setId(
              basicQAParaphraseWordService.selectOneByWord(basicQAParaphraseWordEntity.getWord())
                  .getId());
          insertedWordEntities.add(basicQAParaphraseWordEntity);
        }
      }

      insertedWordEntities
          .addAll(basicQAParaphraseWordService.insertWordsList(toBeInsertedWordEntities));
      List<BasicQAParaphraseRelEntity> basicQAParaphraseRelEntities = new ArrayList<>();
      for (BasicQAParaphraseWordEntity basicQAParaphraseWordEntity : insertedWordEntities) {
        BasicQAParaphraseRelEntity basicQAParaphraseRelEntity = new BasicQAParaphraseRelEntity();
        basicQAParaphraseRelEntity.setMainId(basicQAParaphraseWordEntity.getMainId());
        basicQAParaphraseRelEntity.setParaphraseId(basicQAParaphraseWordEntity.getId());
        basicQAParaphraseRelEntities.add(basicQAParaphraseRelEntity);
      }

      basicQAParaphraseRelService.insertRels(basicQAParaphraseRelEntities);

      return new ResponseEntity<>(insertedWordEntities, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("addWord e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "basicqa^U")
  @RequestMapping(
      value = "/updateMainWord",
      method = RequestMethod.POST)
  public ResponseEntity<?> update(
      @RequestBody BasicQAParaphraseMainWordEntity basicQAParaphraseMainWordEntityParam) {
    logger.info("======= call api  [[/api/basicqa/paraphrase/updateMainWord]] =======");

    try {
      List<BasicQAParaphraseMainWordEntity> temp = basicQAParaphraseMainWordService
          .selectListByWord(basicQAParaphraseMainWordEntityParam.getWord());
      boolean itsMe = false;
      for (BasicQAParaphraseMainWordEntity basicQAParaphraseMainWordEntity : temp) {
        if (basicQAParaphraseMainWordEntity.getId()
            .equals(basicQAParaphraseMainWordEntityParam.getId())) {
          itsMe = true;
        }
      }
      if ((!itsMe) && (temp.size() != 0)) {
        return new ResponseEntity<>(null, HttpStatus.CONFLICT);
      }

      if (basicQAParaphraseWordService
          .selectListByWord(basicQAParaphraseMainWordEntityParam.getWord()).size() != 0) {
        return new ResponseEntity<>(null, HttpStatus.CONFLICT);
      }
      basicQAParaphraseMainWordEntityParam.setBasicQAParaphraseRelEntities(null);
      BasicQAParaphraseMainWordEntity basicQAParaphraseMainWordEntity = basicQAParaphraseMainWordService
          .updateWord(basicQAParaphraseMainWordEntityParam);
      return new ResponseEntity<>(basicQAParaphraseMainWordEntity, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("update e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "basicqa^D")
  @RequestMapping(
      value = "/deleteMainWords",
      method = RequestMethod.POST)
  public ResponseEntity<?> delete(
      @RequestBody List<BasicQAParaphraseMainWordEntity> basicQAParaphraseMainWordEntitiesParam) {
    logger.info("======= call api  [[/api/basicqa/paraphrase/deleteMainWords]] =======");

    try {

      for (BasicQAParaphraseMainWordEntity basicQAParaphraseMainWordEntity : basicQAParaphraseMainWordEntitiesParam) {
        basicQAParaphraseRelService
            .deleteRels(basicQAParaphraseRelService
                .selectListByMainId(basicQAParaphraseMainWordEntity.getId()));
        basicQAParaphraseMainWordEntity.setBasicQAParaphraseRelEntities(null);
      }
      basicQAParaphraseMainWordService.deleteWord(basicQAParaphraseMainWordEntitiesParam);

      return new ResponseEntity<>(null, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("delete e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "basicqa^D")
  @RequestMapping(
      value = "/deleteRels",
      method = RequestMethod.POST)
  public ResponseEntity<?> deleteWords(
      @RequestBody List<BasicQAParaphraseRelEntity> basicQAParaphraseRelEntitiesParam) {
    logger.info("======= call api  [[/api/basicqa/paraphrase/deleteRels]] =======");

    try {
      basicQAParaphraseRelService.deleteRels(basicQAParaphraseRelEntitiesParam);
      return new ResponseEntity<>(null, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("deleteWords e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

  }

  @UriRoleDesc(role = "basicqa^C")
  @RequestMapping(
      value = "/upload-files",
      method = RequestMethod.POST)
  public ResponseEntity<?> insertFile(@RequestParam("file") List<MultipartFile> files) {
    logger.info("======= call api  [[/api/basicqa/paraphrase/upload-files]] =======");
    try {
      basicQAParaphraseMainWordService.uploadFiles(files);

      HashMap<String, Object> result = new HashMap<>();
      result.put("status", "success");

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("insertFile e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "basicqa^R")
  @RequestMapping(
      value = "/download-file",
      method = RequestMethod.GET)
  public ResponseEntity<?> downloadFile() throws IOException {
    logger.info("======= call api  [[/api/basicqa/paraphrase/download-file]] =======");

    try {
      HttpHeaders httpHeaders = new HttpHeaders();
      httpHeaders
          .add(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + bsqaFileName + ".xlsx");
      httpHeaders.add(HttpHeaders.CONTENT_TYPE, "application/octet-stream");

      return new ResponseEntity<>(basicQAParaphraseMainWordService.downloadFile(), httpHeaders,
          HttpStatus.OK);
    } catch (Exception e) {
      logger.error("downloadFile e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
