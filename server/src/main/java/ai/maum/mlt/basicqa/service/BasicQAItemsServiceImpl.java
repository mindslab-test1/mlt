package ai.maum.mlt.basicqa.service;

import ai.maum.mlt.basicqa.entity.BasicQAItemEntity;
import ai.maum.mlt.basicqa.repository.BasicQAItemsRepository;
import ai.maum.mlt.common.util.FileUtils;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

@Service
public class BasicQAItemsServiceImpl implements BasicQAItemsService {

  @Autowired
  private BasicQAItemsRepository basicQAItemsRepository;

  @Autowired
  private FileUtils fileUtils;

  @Value("#{'${excel.bsqa.item.columns}'.split(',')}")
  private List<String> bsqaColumns;

  @Value("#{'${excel.bsqa.item.headers}'.split(',')}")
  private List<String> bsqaHeaders;

  @Value("${excel.bsqa.item.filename}")
  private String bsqaFileName;

  @Override
  public Page<BasicQAItemEntity> selectList(Integer skillId, String src, String question,
      String answer, String useYn, String mainYn, String channel, String category,
      String subCategory, String subSubCategory, Pageable pageable) {
    return basicQAItemsRepository
        .findItems(skillId, src, question, answer, useYn, mainYn, channel, category,
            subCategory, subSubCategory, pageable);
  }

  @Override
  @Transactional
  public BasicQAItemEntity insertItem(BasicQAItemEntity basicQAItemEntity) {
    Integer maxId = basicQAItemsRepository.selectMaxId();
    if ((maxId == null) || (maxId.equals("undefined"))) {
      maxId = 0;
    }
    basicQAItemEntity.setId(maxId + 1);
    basicQAItemEntity.setCreateDtm(new Date());
    basicQAItemEntity.setUpdateDtm(new Date());
    return basicQAItemsRepository.save(basicQAItemEntity);
  }

  @Transactional
  @Override
  public BasicQAItemEntity updateItem(BasicQAItemEntity basicQAItemEntity) {
    return basicQAItemsRepository.save(basicQAItemEntity);
  }

  @Transactional
  @Override
  public void deleteItem(List<BasicQAItemEntity> basicQAItemEntities) {
    basicQAItemsRepository.delete(basicQAItemEntities);
  }

  @Transactional
  @Override
  public void deleteItemBySkillId(Integer skillId) {
    basicQAItemsRepository.deleteAllBySkillId(skillId);
  }

  @Transactional
  @Override
  public void uploadFiles(List<MultipartFile> files, Integer skillId) throws Exception {
    List<List> fileObjectList = null;
    List<BasicQAItemEntity> basicQAItemEntities = new ArrayList<>();

    for (MultipartFile file : files) {
      fileObjectList = fileUtils.excelFileToObject(file, bsqaColumns);
      for (List<HashMap> fileObjects : fileObjectList) {
        for (HashMap<String, Object> object : fileObjects) {
          if (object.size() > 0) {
            BasicQAItemEntity basicQAItemEntity = new BasicQAItemEntity();
            basicQAItemEntity.setSkillId(skillId);
            basicQAItemEntity.setQuestion((String) object.get("question"));
            basicQAItemEntity.setChannel((String) object.get("channel"));
            basicQAItemEntity.setCategory((String) object.get("category"));
            basicQAItemEntity.setSubCategory((String) object.get("subCategory"));
            basicQAItemEntity.setSubSubCategory((String) object.get("subSubCategory"));
            basicQAItemEntity.setAnswer((String) object.get("answer"));
            basicQAItemEntity.setUseYn((String) object.get("useYn"));
            basicQAItemEntity.setMainYn((String) object.get("mainYn"));
            basicQAItemEntity.setWeight(new BigDecimal((String) object.get("weight")).setScale(1,
                RoundingMode.HALF_DOWN));
            basicQAItemEntity.setSrc((String) object.get("src"));
            if ((object.get("createDtm") == null) || (object.get("createDtm")
                .equals("undefined"))) {
              basicQAItemEntity.setCreateDtm(new Date());
            } else {
              try {
                basicQAItemEntity.setCreateDtm(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                    .parse((String) object.get("createDtm")));
              } catch (Exception e) {
                basicQAItemEntity.setCreateDtm(new Date());
              }
            }
            if ((object.get("updateDtm") == null) || (object.get("updateDtm")
                .equals("undefined"))) {
              basicQAItemEntity.setUpdateDtm(new Date());
            } else {
              try {
                basicQAItemEntity.setUpdateDtm(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                    .parse((String) object.get("updateDtm")));
              } catch (Exception e) {
                basicQAItemEntity.setUpdateDtm(new Date());
              }
            }
            //basicQAItemEntity.setId(++maxId);
            basicQAItemEntities.add(basicQAItemEntity);
          }
        }
      }
    }

    // BasicQAItemEntity 리스트 insert 시작
    for (BasicQAItemEntity bqi : basicQAItemEntities) {
      try {
        // 중간에 오류가 난 경우(Category가 없는 경우) 연속된 ID를 제공하기 위해서
        Integer maxId = basicQAItemsRepository.selectMaxId();
        if ((maxId == null) || (maxId.equals("undefined"))) {
          maxId = 0;
        }
        bqi.setId(++maxId);

        basicQAItemsRepository.insertItems(
            bqi.getId(),
            bqi.getSkillId(),
            bqi.getQuestion(),
            bqi.getChannel(),
            bqi.getCategory(),
            bqi.getSubCategory(),
            bqi.getSubSubCategory(),
            bqi.getAnswer(),
            bqi.getUseYn(),
            bqi.getMainYn(),
            bqi.getWeight(),
            bqi.getSrc(),
            bqi.getCreateDtm(),
            bqi.getUpdateDtm()
        );
      } catch (Exception e) {
        // 해당 list insert에 실패 되더라도 다음 list로 이동한다
        continue;
      }
    }
  }

  @Override
  public InputStreamResource downloadFile(Integer skillId) throws Exception {
    // 해당 skillId로 리스트 조회
    List<BasicQAItemEntity> basicQAItemEntities = basicQAItemsRepository.getAllBySkillId(skillId);

    return fileUtils.ObjectToExcel(basicQAItemEntities, bsqaHeaders, bsqaColumns, bsqaFileName);
  }
}
