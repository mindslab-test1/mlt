package ai.maum.mlt.basicqa.controller;

import ai.maum.mlt.basicqa.entity.BasicQAItemEntity;
import ai.maum.mlt.basicqa.service.BasicQAItemsService;
import ai.maum.mlt.basicqa.service.BasicQASkillService;
import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.common.annotation.UriRoleDesc;
import java.io.IOException;
import java.math.RoundingMode;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController()
@RequestMapping("api/basicqa/item")
public class BasicQAItemController {

  static final Logger logger = LoggerFactory.getLogger(
      BasicQAItemController.class);

  @Autowired
  private BasicQAItemsService basicQAItemsService;

  @Autowired
  private BasicQASkillService basicQASkillService;

  @Value("${excel.bsqa.item.filename}")
  private String bsqaFileName;

  @UriRoleDesc(role = "basicqa^R")
  @RequestMapping(
      value = "/getItems",
      method = RequestMethod.POST)
  public ResponseEntity<?> getBasicQAItemList(
      @RequestBody BasicQAItemEntity basicQAItemEntityParam) {
    logger.info("======= call api  [[/api/basicqa/item/getItems]] =======");

    try {
      Page<BasicQAItemEntity> basicQAItemEntities = basicQAItemsService
          .selectList(basicQAItemEntityParam.getSkillId(), basicQAItemEntityParam.getSrc(),
              basicQAItemEntityParam.getQuestion(), basicQAItemEntityParam.getAnswer(),
              basicQAItemEntityParam.getUseYn(), basicQAItemEntityParam.getMainYn(),
              basicQAItemEntityParam.getChannel(), basicQAItemEntityParam.getCategory(),
              basicQAItemEntityParam.getSubCategory(), basicQAItemEntityParam.getSubSubCategory(),
              basicQAItemEntityParam.getPageRequest());

      return new ResponseEntity<>(basicQAItemEntities, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getBasicQAItemList e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "basicqa^C")
  @RequestMapping(
      value = "/add",
      method = RequestMethod.POST)
  public ResponseEntity<?> add(@RequestBody BasicQAItemEntity basicQAItemEntity) {
    logger.info("======= call api  [[/api/basicqa/item/add]] =======");

    try {

      basicQAItemEntity.setCreateDtm(new Date());
      basicQAItemEntity
          .setWeight(basicQAItemEntity.getWeight().setScale(1, RoundingMode.HALF_DOWN));
      return new ResponseEntity<>(basicQAItemsService.insertItem(basicQAItemEntity), HttpStatus.OK);
    } catch (Exception e) {
      logger.error("add e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "basicqa^U")
  @RequestMapping(
      value = "/update",
      method = RequestMethod.POST)
  public ResponseEntity<?> update(@RequestBody BasicQAItemEntity basicQAItemEntity) {
    logger.info("======= call api  [[/api/basicqa/item/update]] =======");

    try {
      basicQAItemEntity.setUpdateDtm(new Date());
      basicQAItemEntity
          .setWeight(basicQAItemEntity.getWeight().setScale(1, RoundingMode.HALF_DOWN));
      return new ResponseEntity<>(basicQAItemsService.updateItem(basicQAItemEntity), HttpStatus.OK);
    } catch (Exception e) {
      logger.error("update e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "basicqa^D")
  @RequestMapping(
      value = "/delete",
      method = RequestMethod.POST)
  public ResponseEntity<?> delete(@RequestBody List<BasicQAItemEntity> basicQAItemEntities) {
    logger.info("======= call api  [[/api/basicqa/item/delete]] =======");

    try {
      basicQAItemsService.deleteItem(basicQAItemEntities);
      return new ResponseEntity<>(null, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("delete e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "basicqa^D")
  @RequestMapping(
      value = "/delete/all",
      method = RequestMethod.POST)
  public ResponseEntity<?> delete(@RequestBody Integer skillId) {
    logger.info("======= call api  [[/api/basicqa/item/delete/all]] =======");

    try {
      basicQAItemsService.deleteItemBySkillId(skillId);
      return new ResponseEntity<>(null, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("delete e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "basicqa^C")
  @RequestMapping(
      value = "/{skillId}/upload-files",
      method = RequestMethod.POST)
  public ResponseEntity<?> insertFile(@RequestParam("file") List<MultipartFile> files,
      @PathVariable Integer skillId) {
    logger.info("======= call api  [[/api/basicqa/item/{skillId}/upload-files]] =======");
    try {
      basicQAItemsService.uploadFiles(files, skillId);

      HashMap<String, Object> result = new HashMap<>();
      result.put("status", "success");

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("insertFile e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "basicqa^R")
  @RequestMapping(
      value = "/{skillId}/download-file",
      method = RequestMethod.GET)
  public ResponseEntity<?> downloadFile(@PathVariable Integer skillId) throws IOException {
    logger.info("======= call api  [[/api/basicqa/item/{skillId}/download-file]] =======");

    try {
      HttpHeaders httpHeaders = new HttpHeaders();
      httpHeaders
          .add(HttpHeaders.CONTENT_DISPOSITION,
              "attachment;filename=" + bsqaFileName + "-" + basicQASkillService
                  .selectOneById(skillId).getName() + ".xlsx");
      httpHeaders.add(HttpHeaders.CONTENT_TYPE, "application/octet-stream");

      return new ResponseEntity<>(basicQAItemsService.downloadFile(skillId), httpHeaders,
          HttpStatus.OK);
    } catch (Exception e) {
      logger.error("downloadFile e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
