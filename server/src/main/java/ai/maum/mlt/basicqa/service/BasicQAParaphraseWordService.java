package ai.maum.mlt.basicqa.service;

import ai.maum.mlt.basicqa.entity.BasicQAParaphraseWordEntity;
import java.util.List;

public interface BasicQAParaphraseWordService {

  BasicQAParaphraseWordEntity selectOneByWord(String word);

  List<BasicQAParaphraseWordEntity> selectListByWord(String word);

  List<BasicQAParaphraseWordEntity> selectListByWordList(List<String> word);

  List<BasicQAParaphraseWordEntity> insertWordsList(
      List<BasicQAParaphraseWordEntity> basicQAParaphraseWordEntities);

  void deleteWords(
      List<BasicQAParaphraseWordEntity> basicQAParaphraseWordEntities);
}
