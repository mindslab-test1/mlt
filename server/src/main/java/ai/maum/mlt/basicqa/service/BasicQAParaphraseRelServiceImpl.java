package ai.maum.mlt.basicqa.service;

import ai.maum.mlt.basicqa.entity.BasicQAParaphraseRelEntity;
import ai.maum.mlt.basicqa.repository.BasicQAParaphraseRelRepository;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BasicQAParaphraseRelServiceImpl implements BasicQAParaphraseRelService {

  @Autowired
  private BasicQAParaphraseRelRepository basicQAParaphraseRelRepository;

  @Override
  public List<BasicQAParaphraseRelEntity> selectListByMainId(Integer mainId) {
    return basicQAParaphraseRelRepository.findAllByMainId(mainId);
  }

  @Transactional
  @Override
  public List<BasicQAParaphraseRelEntity> insertRels(
      List<BasicQAParaphraseRelEntity> basicQAParaphraseRelEntities) {
    return basicQAParaphraseRelRepository.save(basicQAParaphraseRelEntities);
  }

  @Transactional
  @Override
  public void deleteRels(List<BasicQAParaphraseRelEntity> basicQAParaphraseRelEntities) {
    basicQAParaphraseRelRepository.delete(basicQAParaphraseRelEntities);
  }
}
