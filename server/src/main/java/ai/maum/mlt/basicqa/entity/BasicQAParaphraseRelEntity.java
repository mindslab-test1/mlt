package ai.maum.mlt.basicqa.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@IdClass(BasicQAParaphraseRelId.class)
@Table(name = "BRAIN_BQA_PRPRS_REL")
public class BasicQAParaphraseRelEntity implements Serializable {

  @Id
  @Column(name = "MAIN_ID")
  private Integer mainId;

  @Id
  @Column(name = "PARAPHRASE_ID")
  private Integer paraphraseId;

  @ManyToOne
  @JoinColumn(name = "PARAPHRASE_ID", insertable = false, updatable = false)
  private BasicQAParaphraseWordEntity basicQAParaphraseWordEntity;

}