package ai.maum.mlt.basicqa.service;

import ai.maum.mlt.basicqa.entity.BasicQAIndexingHistoryEntity;

public interface BasicQAIndexingHistoryService {

  BasicQAIndexingHistoryEntity insertIndexingHistory(
      BasicQAIndexingHistoryEntity basicQAIndexingHistoryEntity);

  BasicQAIndexingHistoryEntity updateIndexingHistory(
      BasicQAIndexingHistoryEntity basicQAIndexingHistoryEntity);

  BasicQAIndexingHistoryEntity selectLastIndexingHistory(String address);
}
