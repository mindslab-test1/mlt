package ai.maum.mlt.basicqa.repository;

import ai.maum.mlt.basicqa.entity.BasicQAItemEntity;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface BasicQAItemsRepository extends JpaRepository<BasicQAItemEntity, String> {

  // BasicQAItem 테이블 조회 (검색 조건이 너무 길어져서 findItems로 수정)
  @Query(value = "SELECT BQA"
      + "     FROM BasicQAItemEntity BQA "
      + "     WHERE BQA.skillId = :skillId "
      + "     AND (BQA.src LIKE CONCAT('%',:src,'%') OR :src IS NULL) "
      + "     AND (BQA.question LIKE CONCAT('%',:question,'%') OR :question IS NULL) "
      + "     AND (BQA.answer LIKE CONCAT('%',:answer,'%') OR :answer IS NULL) "
      + "     AND (BQA.useYn = :useYn OR :useYn IS NULL) "
      + "     AND (BQA.mainYn = :mainYn OR :mainYn IS NULL) "
      + "     AND (BQA.channel = :channel OR :channel IS NULL) "
      + "     AND (BQA.category = :category OR :category IS NULL) "
      + "     AND (BQA.subCategory = :subCategory OR :subCategory IS NULL) "
      + "     AND (BQA.subSubCategory = :subSubCategory OR :subSubCategory IS NULL) "
  )
  Page<BasicQAItemEntity> findItems(
      @Param("skillId") Integer skillId,
      @Param("src") String src,
      @Param("question") String question,
      @Param("answer") String answer,
      @Param("useYn") String useYn,
      @Param("mainYn") String mainYn,
      @Param("channel") String channel,
      @Param("category") String category,
      @Param("subCategory") String subCategory,
      @Param("subSubCategory") String subSubCategory,
      Pageable pageable
  );

  @Query(value = "SELECT MAX(BQA.id)"
      + "         FROM BasicQAItemEntity BQA "
  )
  Integer selectMaxId();

  List<BasicQAItemEntity> findAllBySkillId(Integer skillId);

  @Modifying
  @Transactional
  @Query(name = "insertItems", nativeQuery = true)
  Integer insertItems(
      @Param("id") Integer id,
      @Param("skillId") Integer skillId,
      @Param("question") String question,
      @Param("channel") String channel,
      @Param("category") String category,
      @Param("subCategory") String subCategory,
      @Param("subSubCategory") String subSubCategory,
      @Param("answer") String answer,
      @Param("useYn") String useYn,
      @Param("mainYn") String mainYn,
      @Param("weight") BigDecimal weight,
      @Param("src") String src,
      @Param("createDtm") Date createDtm,
      @Param("updateDtm") Date updateDtm
  );

  @Modifying
  @Transactional
  @Query(name = "getAllBySkillId", nativeQuery = true)
  List<BasicQAItemEntity> getAllBySkillId(@Param("skillId") Integer skillId);

  void deleteAllBySkillId(Integer skillId);
}
