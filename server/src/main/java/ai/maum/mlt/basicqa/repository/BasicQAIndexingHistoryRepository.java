package ai.maum.mlt.basicqa.repository;

import ai.maum.mlt.basicqa.entity.BasicQAIndexingHistoryEntity;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface BasicQAIndexingHistoryRepository extends
    JpaRepository<BasicQAIndexingHistoryEntity, String> {

  BasicQAIndexingHistoryEntity findTopByAddressOrderByCreatedAt(String address);
}
