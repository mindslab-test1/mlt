package ai.maum.mlt.basicqa.service;

import ai.maum.mlt.basicqa.entity.BasicQASkillEntity;
import ai.maum.mlt.basicqa.repository.BasicQAItemsRepository;
import ai.maum.mlt.basicqa.repository.BasicQASkillRepository;
import java.util.Date;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BasicQASkillServiceImpl implements BasicQASkillService {

  @Autowired
  private BasicQASkillRepository basicQASkillRepository;

  @Autowired
  private BasicQAItemsRepository basicQAItemsRepository;

  @Override
  public List<BasicQASkillEntity> selectList() {
    return basicQASkillRepository.findAllByOrderByNameAsc();
  }

  @Override
  public BasicQASkillEntity selectOneByName(String name) {
    return basicQASkillRepository.findByName(name);
  }

  @Override
  public BasicQASkillEntity selectOneById(Integer id) {
    return basicQASkillRepository.findById(id);
  }

  @Transactional
  @Override
  public BasicQASkillEntity insertSkill(BasicQASkillEntity basicQASkillEntity) {
    Date now = new Date();
    basicQASkillEntity.setCreatedAt(now);
    basicQASkillEntity.setUpdatedAt(now);
    return basicQASkillRepository.save(basicQASkillEntity);
  }

  @Transactional
  @Override
  public BasicQASkillEntity updateSkill(BasicQASkillEntity basicQASkillEntity) {
    return basicQASkillRepository.save(basicQASkillEntity);
  }

  @Transactional
  @Override
  public void deleteSkills(List<BasicQASkillEntity> basicQASkillEntities) {
    for (BasicQASkillEntity basicQASkillEntity : basicQASkillEntities) {
      basicQAItemsRepository
          .delete(basicQAItemsRepository.findAllBySkillId(basicQASkillEntity.getId()));
    }
    basicQASkillRepository.delete(basicQASkillEntities);
  }
}
