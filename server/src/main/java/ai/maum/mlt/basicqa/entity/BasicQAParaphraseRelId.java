package ai.maum.mlt.basicqa.entity;

import java.io.Serializable;

public class BasicQAParaphraseRelId implements Serializable {

  private Integer mainId;
  private Integer paraphraseId;

  public BasicQAParaphraseRelId() {
  }

  public BasicQAParaphraseRelId(Integer mainId, Integer paraphraseId) {
    this.mainId = mainId;
    this.paraphraseId = paraphraseId;
  }

}
