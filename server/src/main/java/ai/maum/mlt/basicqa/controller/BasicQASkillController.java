package ai.maum.mlt.basicqa.controller;

import ai.maum.mlt.basicqa.entity.BasicQASkillEntity;
import ai.maum.mlt.basicqa.service.BasicQASkillService;
import ai.maum.mlt.common.annotation.UriRoleDesc;
import java.util.HashMap;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@RequestMapping("api/basicqa/skill")
public class BasicQASkillController {

  static final Logger logger = LoggerFactory.getLogger(
      BasicQASkillController.class);

  @Autowired
  private BasicQASkillService basicQASkillService;

  @UriRoleDesc(role = "basicqa^R")
  @RequestMapping(
      value = "/list",
      method = RequestMethod.GET)
  public ResponseEntity<?> getList() {
    logger.info("======= call api  [[/api/basicqa/skill/list]] =======");

    try {
      return new ResponseEntity<>(basicQASkillService.selectList(), HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getList e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "basicqa^R")
  @RequestMapping(
      value = "/name/{name}",
      method = RequestMethod.GET)
  public ResponseEntity<?> getSkillByName(@PathVariable String name) {
    logger.info("======= call api  [[/api/basicqa/skill/name/{name}]] =======");

    try {
      return new ResponseEntity<>(basicQASkillService.selectOneByName(name), HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getSkillByName e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "basicqa^R")
  @RequestMapping(
      value = "/id/{id}",
      method = RequestMethod.GET)
  public ResponseEntity<?> getSkillById(@PathVariable Integer id) {
    logger.info("======= call api  [[/api/basicqa/skill/id/{id}]] =======");

    try {
      return new ResponseEntity<>(basicQASkillService.selectOneById(id), HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getSkillById e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "basicqa^C")
  @RequestMapping(
      value = "/add",
      method = RequestMethod.POST)
  public ResponseEntity<?> add(@RequestBody BasicQASkillEntity basicQASkillEntityParam) {
    logger.info("======= call api  [[/api/basicqa/skill/add]] =======");

    try {
      if (basicQASkillService.selectOneByName(basicQASkillEntityParam.getName()) != null) {
        return new ResponseEntity<>(null, HttpStatus.CONFLICT);
      }

      return new ResponseEntity<>(basicQASkillService.insertSkill(basicQASkillEntityParam),
          HttpStatus.OK);
    } catch (Exception e) {
      logger.error("add e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "basicqa^U")
  @RequestMapping(
      value = "/update",
      method = RequestMethod.POST)
  public ResponseEntity<?> update(@RequestBody BasicQASkillEntity basicQASkillEntityParam) {
    logger.info("======= call api  [[/api/basicqa/skill/update]] =======");

    try {
      if (basicQASkillService.selectOneByName(basicQASkillEntityParam.getName()) != null) {
        return new ResponseEntity<>(null, HttpStatus.CONFLICT);
      }

      return new ResponseEntity<>(basicQASkillService.updateSkill(basicQASkillEntityParam),
          HttpStatus.OK);
    } catch (Exception e) {
      logger.error("update e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "basicqa^D")
  @RequestMapping(
      value = "/delete/list",
      method = RequestMethod.POST)
  public ResponseEntity<?> delete(
      @RequestBody List<BasicQASkillEntity> basicQASkillEntitiesParam) {
    logger.info("======= call api  [[/api/basicqa/skill/delete/list]] =======");

    try {
      basicQASkillService.deleteSkills(basicQASkillEntitiesParam);
      return new ResponseEntity<>(null, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("delete e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
