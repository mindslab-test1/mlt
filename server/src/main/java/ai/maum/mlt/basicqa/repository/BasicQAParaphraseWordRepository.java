package ai.maum.mlt.basicqa.repository;

import ai.maum.mlt.basicqa.entity.BasicQAParaphraseWordEntity;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface BasicQAParaphraseWordRepository extends
    JpaRepository<BasicQAParaphraseWordEntity, String> {

  BasicQAParaphraseWordEntity findOneByWord(String word);

  @Query(value = "SELECT BQA "
      + "         FROM BasicQAParaphraseWordEntity BQA "
      + "         WHERE BQA.word in(:keyword)"
  )
  List<BasicQAParaphraseWordEntity> findMainWordByInKeyword(@Param("keyword") List keyword);

  List<BasicQAParaphraseWordEntity> findAllByWord(String word);

  List<BasicQAParaphraseWordEntity> save(String word);
}
