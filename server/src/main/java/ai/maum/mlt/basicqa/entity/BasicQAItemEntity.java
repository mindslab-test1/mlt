package ai.maum.mlt.basicqa.entity;

import ai.maum.mlt.common.auth.entity.UserEntity;
import ai.maum.mlt.common.pagenation.PageParameters;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.hibernate.annotations.NamedNativeQueries;
import org.hibernate.annotations.NamedNativeQuery;
import org.springframework.data.annotation.LastModifiedDate;


@SqlResultSetMappings({
    @SqlResultSetMapping(
        name = "insertResult",
        columns = {
            @ColumnResult(name = "count", type = Integer.class)}
    ),
    @SqlResultSetMapping(
        name = "getAllBySkillIdResult",
        classes = @ConstructorResult(
            targetClass = BasicQAItemEntity.class,
            columns = {
                // 엑셀로 추출할 column 값이 BasicQAItemEntity 생성자와 정확히 일치해야 한다
                @ColumnResult(name = "ID", type = Integer.class),
                @ColumnResult(name = "QUESTION", type = String.class),
                @ColumnResult(name = "CHANNEL", type = String.class),
                @ColumnResult(name = "CATEGORY", type = String.class),
                @ColumnResult(name = "SUB_CATEGORY", type = String.class),
                @ColumnResult(name = "SUB_SUB_CATEGORY", type = String.class),
                @ColumnResult(name = "ANSWER", type = String.class),
                @ColumnResult(name = "SRC", type = String.class),
                @ColumnResult(name = "USE_YN", type = String.class),
                @ColumnResult(name = "MAIN_YN", type = String.class),
                @ColumnResult(name = "WEIGHT", type = BigDecimal.class),
                @ColumnResult(name = "CREATE_DTM", type = Date.class),
                @ColumnResult(name = "UPDATE_DTM", type = Date.class)
            })
    )
})

@NamedNativeQueries({
    @NamedNativeQuery(
        name = "insertItems",
        query = "insert into BRAIN_BQA_ITEM "
            + "(ID, SKILL_ID, QUESTION, CHANNEL, CATEGORY, SUB_CATEGORY, SUB_SUB_CATEGORY, ANSWER, USE_YN, MAIN_YN, WEIGHT, SRC, CREATE_DTM, UPDATE_DTM) "
            + "values "
            + "(:id, :skillId, :question"
            + ", (select ID from MAI_BSQA_CATEGORY where CATE_PATH =:channel)"
            + ", (select ID from MAI_BSQA_CATEGORY where CATE_PATH = (:channel||'>'||:category) )"
            + ", (select ID from MAI_BSQA_CATEGORY where CATE_PATH = (:channel||'>'||:category||'>'||:subCategory) )"
            + ", (select ID from MAI_BSQA_CATEGORY where CATE_PATH = (:channel||'>'||:category||'>'||:subCategory||'>'||:subSubCategory) )"
            + ", :answer, :useYn, :mainYn, :weight, :src, :createDtm, :updateDtm)",
        resultSetMapping = "insertResult"
    ),
    @NamedNativeQuery(
        name = "getAllBySkillId",
        query = "select "
            + " ID "
            + ", QUESTION "
            + ", (select CATE_NAME from MAI_BSQA_CATEGORY where ID = CHANNEL) as CHANNEL "
            + ", (select CATE_NAME from MAI_BSQA_CATEGORY where ID = CATEGORY) as CATEGORY "
            + ", (select CATE_NAME from MAI_BSQA_CATEGORY where ID = SUB_CATEGORY) as SUB_CATEGORY "
            + ", (select CATE_NAME from MAI_BSQA_CATEGORY where ID = SUB_SUB_CATEGORY) as SUB_SUB_CATEGORY "
            + ", ANSWER"
            + ", SRC "
            + ", USE_YN"
            + ", MAIN_YN"
            + ", WEIGHT"
            + ", CREATED_AT"
            + ", UPDATED_AT "
            + " from BRAIN_BQA_ITEM "
            + " where SKILL_ID = :skillId ",
        resultSetMapping = "getAllBySkillIdResult"
    )
})

@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "BRAIN_BQA_ITEM")
public class BasicQAItemEntity extends PageParameters {

  public BasicQAItemEntity() {
  }

  public BasicQAItemEntity(
      Integer ID
      , String QUESTION
      , String CHANNEL
      , String CATEGORY
      , String SUB_CATEGORY
      , String SUB_SUB_CATEGORY
      , String ANSWER
      , String SRC
      , String USE_YN
      , String MAIN_YN
      , BigDecimal WEIGHT
      , Date CREATE_DTM
      , Date UPDATE_DTM
  ) {
    this.setId(ID);
    this.setQuestion(QUESTION);
    this.setChannel(CHANNEL);
    this.setCategory(CATEGORY);
    this.setSubCategory(SUB_CATEGORY);
    this.setSubSubCategory(SUB_SUB_CATEGORY);
    this.setAnswer(ANSWER);
    this.setSrc(SRC);
    this.setUseYn(USE_YN);
    this.setMainYn(MAIN_YN);
    this.setWeight(WEIGHT);
    this.setCreateDtm(CREATE_DTM);
    this.setUpdateDtm(UPDATE_DTM);
  }

  @Id
  @Column(name = "ID", nullable = false)
  private Integer id;

  @Column(name = "C_ID")
  private Integer cId;

  @Column(name = "QNA_ID", length = 255)
  private String qnId;

  @Lob
  @Column(name = "QUESTION")
  private String question;

  @Lob
  @Column(name = "ANSWER")
  private String answer;

  @Column(name = "USE_YN", length = 1)
  private String useYn;

  @Column(name = "MAIN_YN", length = 1)
  private String mainYn;

  @Column(name = "SRC", length = 20)
  private String src;

  @Column(name = "WEIGHT", precision = 2, scale = 1)
  private BigDecimal weight;

  @Column(name = "CONFIRMER", length = 40)
  private String confirmer;

  @Column(name = "CONFIRM_DTM")
  private Date confirmDtm;

  @Column(name = "SKILL_ID")
  private Integer skillId;

  @Column(name = "CHANNEL", length = 40, nullable = false)
  private String channel;

  @Column(name = "CATEGORY", length = 40)
  private String category;

  @Column(name = "SUB_CATEGORY", length = 40)
  private String subCategory;

  @Column(name = "SUB_SUB_CATEGORY", length = 40)
  private String subSubCategory;

  @Column(name = "CREATOR_ID", length = 40)
  private String creator;

  @CreatedDate
  @Column(name = "CREATED_AT")
  private Date createDtm;

  @Column(name = "UPDATER_ID", length = 40)
  private String updator;

  @LastModifiedDate
  @Column(name = "UPDATED_AT")
  private Date updateDtm;

  @ManyToOne
  @JoinColumn(name = "SKILL_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private BasicQASkillEntity basicQASkillEntity;

  @ManyToOne
  @JoinColumn(name = "CHANNEL", referencedColumnName = "ID", insertable = false, updatable = false)
  private BasicQACategoryEntity basicQACategoryEntity;

  @ManyToOne
  @JoinColumn(name = "CATEGORY", referencedColumnName = "ID", insertable = false, updatable = false)
  private BasicQACategoryEntity basicQACategoryEntity2;

  @ManyToOne
  @JoinColumn(name = "SUB_CATEGORY", referencedColumnName = "ID", insertable = false, updatable = false)
  private BasicQACategoryEntity basicQACategoryEntity3;

  @ManyToOne
  @JoinColumn(name = "SUB_SUB_CATEGORY", referencedColumnName = "ID", insertable = false, updatable = false)
  private BasicQACategoryEntity basicQACategoryEntity4;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "CREATOR_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity createUserEntity;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "UPDATER_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity updateUserEntity;
}
