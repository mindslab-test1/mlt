package ai.maum.mlt.basicqa.entity;

import ai.maum.mlt.common.auth.entity.UserEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Date;
import javax.persistence.*;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "BRAIN_BQA_SKILL")
public class BasicQASkillEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BQA_SKILL_GEN")
  @SequenceGenerator(name = "BQA_SKILL_GEN", sequenceName = "BRAIN_BQA_SKILL_SEQ", allocationSize = 5)
  @Column(name = "ID")
  private Integer id;

  @Column(name = "NAME", length = 40, nullable = false)
  private String name;

  @Column(name = "CREATOR_ID", length = 40)
  private String creatorId;

  @CreatedDate
  @Column(name = "CREATED_AT")
  private Date createdAt;

  @Column(name = "UPDATER_ID", length = 40)
  private String updaterId;

  @LastModifiedDate
  @Column(name = "UPDATED_AT")
  private Date updatedAt;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "CREATOR_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity createUserEntity;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "UPDATER_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity updateUserEntity;
}
