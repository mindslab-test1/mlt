package ai.maum.mlt.basicqa.controller;

import ai.maum.mlt.basicqa.entity.BasicQAIndexingHistoryEntity;
import ai.maum.mlt.basicqa.service.BasicQAIndexingHistoryService;
import ai.maum.mlt.basicqa.service.BasicQASkillService;
import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.itfc.BasicqaGrpcInterfaceManager;
import java.util.Date;
import java.util.HashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@RequestMapping("api/basicqa/schedule")
public class BasicQAScheduleController {

  static final Logger logger = LoggerFactory.getLogger(
      BasicQAScheduleController.class);

  @Autowired
  private BasicQAIndexingHistoryService basicQAIndexingHistoryService;

  @Autowired
  private BasicQASkillService basicQASkillService;

  @Value("${bsqa.server.ip}")
  private String ip;

  @Value("${bsqa.server.port}")
  private int port;

  @UriRoleDesc(role = "basicqa^R")
  @RequestMapping(
      value = "/skill/list",
      method = RequestMethod.GET)
  public ResponseEntity<?> getList() {
    logger.info("======= call api  [[/api/basicqa/schedule/skill/list]] =======");

    try {
      return new ResponseEntity<>(basicQASkillService.selectList(), HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getList e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "basicqa^X")
  @RequestMapping(
      value = "/fullIndexing/{skillId}/{clean}",
      method = RequestMethod.GET)
  public ResponseEntity<?> fullIndexing(@PathVariable Integer skillId,
      @PathVariable boolean clean) {
    logger
        .info("======= call api  [[/api/basicqa/schedule/fullIndexing/{skillId}/{clean}]] =======");
    try {

      BasicqaGrpcInterfaceManager client = new BasicqaGrpcInterfaceManager(ip, port);
      maum.brain.qa.Basicqa.IndexingInput.Builder indexingInput = maum.brain.qa.Basicqa.IndexingInput
          .newBuilder();
      indexingInput.setClean(clean);
      if (skillId > 0) {
        indexingInput.setSkillId(skillId);
      }
      maum.brain.qa.Basicqa.SearchStatus searchStatus = client.fullIndexing(indexingInput.build());
      if (searchStatus == null || searchStatus.equals("")) {
        HashMap<String, Object> result = new HashMap<>();
        result.put("message", "BasicQA Engine Error");
        return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
      }
      BasicQAIndexingHistoryEntity basicQAIndexingHistoryEntity = new BasicQAIndexingHistoryEntity();
      basicQAIndexingHistoryEntity.setType("fullIndexing");
      basicQAIndexingHistoryEntity.setStatus(searchStatus.getStatus());
      basicQAIndexingHistoryEntity.setTotal(searchStatus.getTotal());
      basicQAIndexingHistoryEntity.setFetched(0);
      basicQAIndexingHistoryEntity.setProcessed(searchStatus.getProcessed());
      basicQAIndexingHistoryEntity.setMessage(searchStatus.getMessage());
      basicQAIndexingHistoryEntity.setCreatedAt(new Date());
      basicQAIndexingHistoryEntity.setStopYn(false);
      basicQAIndexingHistoryEntity.setAddress(ip + ":" + port);
      basicQAIndexingHistoryService.insertIndexingHistory(basicQAIndexingHistoryEntity);

      HashMap<String, Object> result = new HashMap<>();
      result.put("status", searchStatus.getStatus());
      if (searchStatus.getStatus()) {
        if (searchStatus.getTotal() == 0) {
          result.put("progress", 0);
        } else {
          result.put("progress", searchStatus.getFetched() / searchStatus.getTotal() * 100);
        }
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("fullIndexing e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "basicqa^X")
  @RequestMapping(
      value = "/incrementalIndexing/{skillId}/{clean}",
      method = RequestMethod.GET)
  public ResponseEntity<?> incrementalIndexing(@PathVariable Integer skillId,
      @PathVariable boolean clean) {
    logger.info(
        "======= call api  [[/api/basicqa/schedule/incrementalIndexing/{skillId}/{clean}]] =======");
    try {
      BasicqaGrpcInterfaceManager client = new BasicqaGrpcInterfaceManager(ip, port);
      maum.brain.qa.Basicqa.IndexingInput.Builder indexingInput = maum.brain.qa.Basicqa.IndexingInput
          .newBuilder();
      indexingInput.setClean(clean);
      if (skillId > 0) {
        indexingInput.setSkillId(skillId);
      }
      maum.brain.qa.Basicqa.SearchStatus searchStatus = client
          .additionalIndexing(indexingInput.build());
      if (searchStatus == null || searchStatus.equals("")) {
        HashMap<String, Object> result = new HashMap<>();
        result.put("message", "BasicQA Engine Error");
        return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
      }

      BasicQAIndexingHistoryEntity basicQAIndexingHistoryEntity = new BasicQAIndexingHistoryEntity();
      basicQAIndexingHistoryEntity.setType("incrementalIndexing");
      basicQAIndexingHistoryEntity.setStatus(searchStatus.getStatus());
      basicQAIndexingHistoryEntity.setTotal(searchStatus.getTotal());
      basicQAIndexingHistoryEntity.setFetched(0);
      basicQAIndexingHistoryEntity.setProcessed(searchStatus.getProcessed());
      basicQAIndexingHistoryEntity.setMessage(searchStatus.getMessage());
      basicQAIndexingHistoryEntity.setCreatedAt(new Date());
      basicQAIndexingHistoryEntity.setStopYn(false);
      basicQAIndexingHistoryEntity.setAddress(ip + ":" + port);
      basicQAIndexingHistoryService.insertIndexingHistory(basicQAIndexingHistoryEntity);

      HashMap<String, Object> result = new HashMap<>();
      result.put("status", searchStatus.getStatus());
      if (searchStatus.getStatus()) {
        if (searchStatus.getTotal() == 0) {
          result.put("progress", 0);
        } else {
          result.put("progress", searchStatus.getFetched() / searchStatus.getTotal() * 100);
        }
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("incrementalIndexing e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "basicqa^X")
  @RequestMapping(
      value = "/getIndexingStatus",
      method = RequestMethod.GET)
  public ResponseEntity<?> getIndexingStatus() {
    logger.info("======= call api  [[/api/basicqa/schedule/getIndexingStatus]] =======");
    try {
      BasicqaGrpcInterfaceManager client = new BasicqaGrpcInterfaceManager(ip, port);
      com.google.protobuf.Empty.Builder empty = com.google.protobuf.Empty.newBuilder();

      maum.brain.qa.Basicqa.SearchStatus searchStatus = client.getIndexingStatus(empty.build());
      if (searchStatus == null || searchStatus.equals("")) {
        HashMap<String, Object> result = new HashMap<>();
        result.put("message", "BasicQA Engine Error");
        return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
      }
      HashMap<String, Object> result = new HashMap<>();
      result.put("status", searchStatus.getStatus());
      if (searchStatus.getStatus()) {
        float fetched = searchStatus.getFetched();
        float total = searchStatus.getTotal();
        float progress = (fetched / total) * 100;
        result.put("progress", progress);
      } else {
        BasicQAIndexingHistoryEntity basicQAIndexingHistoryEntity = basicQAIndexingHistoryService
            .selectLastIndexingHistory(ip + ":" + port);
        if (basicQAIndexingHistoryEntity != null) {
          float fetched = basicQAIndexingHistoryEntity.getFetched();
          float total = basicQAIndexingHistoryEntity.getTotal();
          if (basicQAIndexingHistoryEntity.getStopYn()) {
            float progress = (fetched / total) * 100;
            result.put("progress", progress);
          } else {
            result.put("progress", 100);
          }
          result.put("latestIndexingHistory", basicQAIndexingHistoryEntity);
        } else {
          result.put("progress", 0);
          result.put("latestIndexingHistory", null);
        }
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getIndexingStatus e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "basicqa^X")
  @RequestMapping(
      value = "/abortIndexing",
      method = RequestMethod.GET)
  public ResponseEntity<?> abortIndexing() {
    logger.info("======= call api  [[/api/basicqa/schedule/abortIndexing]] =======");
    try {
      BasicqaGrpcInterfaceManager client = new BasicqaGrpcInterfaceManager(ip, port);
      com.google.protobuf.Empty.Builder empty = com.google.protobuf.Empty.newBuilder();

      maum.brain.qa.Basicqa.SearchStatus searchStatus = client.getIndexingStatus(empty.build());
      if (searchStatus == null || searchStatus.equals("")) {
        HashMap<String, Object> result = new HashMap<>();
        result.put("message", "BasicQA Engine Error");
        return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
      }

      BasicQAIndexingHistoryEntity basicQAIndexingHistoryEntity = basicQAIndexingHistoryService
          .selectLastIndexingHistory(ip + ":" + port);

      basicQAIndexingHistoryEntity.setFetched(searchStatus.getFetched());
      basicQAIndexingHistoryEntity.setProcessed(searchStatus.getProcessed());

      searchStatus = client.abortIndexing(empty.build());
      basicQAIndexingHistoryEntity.setUpdatedAt(new Date());
      basicQAIndexingHistoryEntity.setStopYn(true);
      basicQAIndexingHistoryEntity.setMessage(searchStatus.getMessage());

      basicQAIndexingHistoryService.updateIndexingHistory(basicQAIndexingHistoryEntity);

      HashMap<String, Object> result = new HashMap<>();
      result.put("status", searchStatus.getStatus());
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("abortIndexing e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
