package ai.maum.mlt.basicqa.service;

import ai.maum.mlt.basicqa.entity.BasicQAParaphraseRelEntity;
import java.util.List;

public interface BasicQAParaphraseRelService {

  List<BasicQAParaphraseRelEntity> selectListByMainId(Integer mainId);

  List<BasicQAParaphraseRelEntity> insertRels(
      List<BasicQAParaphraseRelEntity> basicQAParaphraseRelEntities);

  void deleteRels(List<BasicQAParaphraseRelEntity> basicQAParaphraseRelEntities);
}
