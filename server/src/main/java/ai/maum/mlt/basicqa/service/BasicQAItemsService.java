package ai.maum.mlt.basicqa.service;

import ai.maum.mlt.basicqa.entity.BasicQAItemEntity;
import java.util.List;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

public interface BasicQAItemsService {

  Page<BasicQAItemEntity> selectList(Integer skillId, String src, String question, String answer,
      String useYn, String mainYn, String channel, String category, String subCategory,
      String subSubCategory, Pageable pageable);

  BasicQAItemEntity insertItem(BasicQAItemEntity basicQAItemEntity);

  BasicQAItemEntity updateItem(BasicQAItemEntity basicQAItemEntity);

  void deleteItem(List<BasicQAItemEntity> basicQAItemEntities);

  void deleteItemBySkillId(Integer skillId);

  void uploadFiles(List<MultipartFile> files, Integer skillId) throws Exception;

  InputStreamResource downloadFile(Integer skillId) throws Exception;
}
