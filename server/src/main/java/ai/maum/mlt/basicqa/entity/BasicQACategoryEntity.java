package ai.maum.mlt.basicqa.entity;

import ai.maum.mlt.common.auth.entity.UserEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.*;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.LastModifiedDate;

@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "BRAIN_BQA_CATEGORY")
public class BasicQACategoryEntity implements Serializable {
  public BasicQACategoryEntity(){ }

  public BasicQACategoryEntity(String catName){
    this.setCatName(catName);
  }

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid")
  @Column(name = "ID", length = 40)
  private String id;

  @Column(name = "CATE_NAME", length = 40, nullable = false)
  private String catName;

  @Column(name = "CATE_PATH", length = 200, nullable = false)
  private String catPath;

  @Column(name = "PAR_CATE_ID", length = 40)
  private String parCatId;

  @Column(name = "CREATOR_ID", length = 40)
  private String creatorId;

  @CreatedDate
  @Column(name = "CREATED_AT")
  private Date createdAt;

  @Column(name = "UPDATER_ID", length = 40)
  private String updaterId;

  @LastModifiedDate
  @Column(name = "UPDATED_AT")
  private Date updatedAt;

  @ManyToOne
  @JoinColumn(name = "CREATOR_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity createUserEntity;

  @ManyToOne
  @JoinColumn(name = "UPDATER_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity updateUserEntity;

  @Transient
  private List<BasicQACategoryEntity> children = new ArrayList<>();

  @Transient
  private String rootGrpCd;

  @Transient
  private String groupCodeName;

}
