package ai.maum.mlt.basicqa.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.itfc.BasicqaGrpcInterfaceManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import maum.brain.qa.Basicqa.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@RequestMapping("api/basicqa/question")
public class BasicQAQuestionController {

  static final Logger logger = LoggerFactory.getLogger(
      BasicQAQuestionController.class);

  @Value("${bsqa.server.ip}")
  private String ip;

  @Value("${bsqa.server.port}")
  private int port;

  @UriRoleDesc(role = "basicqa^R")
  @RequestMapping(
      value = "/question",
      method = RequestMethod.POST)
  public ResponseEntity<?> question(@RequestBody HashMap<Object, Object> param) {
    logger.info("======= call api  [[/api/basicqa/schedule/question]] =======");
    try {
      BasicqaGrpcInterfaceManager client = new BasicqaGrpcInterfaceManager(ip, port);
      maum.brain.qa.Basicqa.QuestionInput.Builder questionInput = maum.brain.qa.Basicqa.QuestionInput
          .newBuilder();

      ArrayList<Object> resultList = new ArrayList<>();
      String text = param.get("question").toString();
      String[] splitText = text.split("\n");
      int textResultSeq = 1;

      for (String question: splitText) {
        questionInput.setNtop(Integer.parseInt(param.get("nTop").toString()));
        questionInput.setType(Type.ALL);
        questionInput.setSkillId(Integer.parseInt(param.get("skillId").toString()));
        if (param.containsKey("src")) {
          questionInput.setSrc(param.get("src").toString());
        }
        questionInput.setQuestion(question);
        if (param.containsKey("meta")) {
          questionInput.putAllMeta((HashMap) param.get("meta"));
        }

        if (param.containsKey("channel")) {
          questionInput.setChannel(param.get("channel").toString());
        }

        if (param.containsKey("category")) {
          questionInput.setCategory(param.get("category").toString());
        }

        if (param.containsKey("subCategory")) {
          questionInput.setSubCategory(param.get("subCategory").toString());
        }

        if (param.containsKey("subSubCategory")) {
          questionInput.setSubSubCategory(param.get("subSubCategory").toString());
        }

        maum.brain.qa.Basicqa.AnswerOutput answerOutput = client.question(questionInput.build());

        if (answerOutput == null || answerOutput.equals("")) {
          HashMap<String, Object> result = new HashMap<>();
          result.put("message", "BasicQA Engine Error");
          return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        List<HashMap<String, Object>> dbResultList = new ArrayList<>();
        for (maum.brain.qa.Basicqa.DBResult dbResult : answerOutput.getDbResultList()) {
          HashMap<String, Object> dbResultMap = new HashMap<>();
          dbResultMap.put("skillId", dbResult.getSkillId());
          dbResultMap.put("source", dbResult.getSrc());
          dbResultMap.put("answer", dbResult.getAnswer());
          dbResultList.add(dbResultMap);
        }

        List<HashMap<String, Object>> searchResultList = new ArrayList<>();
        for (maum.brain.qa.Basicqa.SearchResult searchResult : answerOutput.getSearchResultList()) {
          HashMap<String, Object> searchResultMap = new HashMap<>();
          searchResultMap.put("skillId", searchResult.getSkillId());
          searchResultMap.put("src", searchResult.getSrcBytes());
          searchResultMap.put("question_morph", searchResult.getQuestionMorph());
          searchResultMap.put("question", searchResult.getQuestion());
          searchResultMap.put("answer", searchResult.getAnswer());
          searchResultMap.put("question_nlg", searchResult.getQuestionNlg());
          searchResultMap.put("score", searchResult.getScore());
          searchResultList.add(searchResultMap);
        }

        HashMap<String, Object> tempResult = new HashMap<>();
        tempResult.put("textResultSeq", textResultSeq);
        tempResult.put("question", answerOutput.getQuestion());
        tempResult.put("dbResult", dbResultList);
        tempResult.put("searchResult", searchResultList);

        resultList.add(tempResult);
        textResultSeq++;
      }

      return new ResponseEntity<>(resultList, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("question e : " , e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
