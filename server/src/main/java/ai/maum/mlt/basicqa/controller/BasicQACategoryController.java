package ai.maum.mlt.basicqa.controller;

import static ai.maum.mlt.common.system.SystemCode.BASICQA_DATA_GRP;

import ai.maum.mlt.basicqa.entity.BasicQACategoryEntity;
import ai.maum.mlt.basicqa.service.BasicQACategoryService;
import ai.maum.mlt.common.annotation.UriRoleDesc;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/basicqa/category")
public class BasicQACategoryController {

  static final Logger logger = LoggerFactory.getLogger(BasicQACategoryController.class);

  @Autowired
  private BasicQACategoryService basicQACategoryService;

  @UriRoleDesc(role = "basicqa^R")
  @RequestMapping(
      value = "/getCategoryAllList",
      method = RequestMethod.GET)
  public ResponseEntity<?> getCategoryAllList() {
    logger.info("======= call api  [[/api/basicqa/category/getCategoryAllList]] =======");
    try {
      // MLT_CODE 에서 groupCode가 BASICQA_DATA_GRP 인 정보 모두 조회
      return new ResponseEntity<>(
          basicQACategoryService.findCategoryTree(BASICQA_DATA_GRP), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "basicqa^C")
  @RequestMapping(
      value = "/insertCategory",
      method = RequestMethod.POST)
  public ResponseEntity<?> insertCategory(@RequestBody BasicQACategoryEntity category) {
    logger.info("======= call api  [[/api/basicqa/category/insertCategory]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {
      // 중복되는 Category가 있는지 확인
      BasicQACategoryEntity duplicateCategory = basicQACategoryService
          .findByParCatIdAndCatName(category.getParCatId(), category.getCatName());
      if (duplicateCategory == null) {  // 중복되는 Category가 없는 경우 Insert 수행
        category.setCreatedAt(new Date());
        BasicQACategoryEntity insertCategory = basicQACategoryService.insertCategory(category);
        result.put("message", "INSERT_SUCESS");
        result.put("category", insertCategory);
      } else {
        result.put("message", "INSERT_DUPLICATED");
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      result.put("message", "INSERT_FAILED");
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "basicqa^D")
  @RequestMapping(
      value = "/deleteCategory",
      method = RequestMethod.POST)
  public ResponseEntity<?> deleteCategory(@RequestBody BasicQACategoryEntity categoryEntity) {
    logger.info("======= call api  [[/api/basicqa/category/deleteCategory]] =======");
    HashMap<String, String> result = new HashMap<>();
    try {
      // 해당 Category 삭제
      basicQACategoryService.deleteCategory(categoryEntity.getId());
      result.put("message", "DELETE_SUCESS");

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      result.put("message", "ERROR");
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "basicqa^U")
  @RequestMapping(
      value = "/updateCategory",
      method = RequestMethod.POST)
  public ResponseEntity<?> updateCategory(@RequestBody BasicQACategoryEntity category) {
    logger.info("======= call api  [[/api/basicqa/category/updateCategory]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {
      // 중복되는 Category가 있는지 확인
      BasicQACategoryEntity duplicateCategory = basicQACategoryService
          .findByParCatIdAndCatName(category.getParCatId(), category.getCatName());

      if (duplicateCategory == null) {  // 중복되는 Category가 없는 경우 Update 수행
        HashMap<String, List<BasicQACategoryEntity>> categoryTreeMap = basicQACategoryService
            .findCategoryTree(BASICQA_DATA_GRP);
        List<BasicQACategoryEntity> categoryEntityList = categoryTreeMap.get(category.getGroupCodeName());
        String id = category.getId();
        BasicQACategoryEntity currentCategoryEntity = getEntityById(categoryEntityList, id);
        String oldStr = currentCategoryEntity.getCatName();
        String newStr = category.getCatName();

        category.setUpdatedAt(new Date());
        BasicQACategoryEntity resultCategory = basicQACategoryService.updateCategory(category);
        updateChildEntity(currentCategoryEntity.getChildren(), oldStr, newStr);
        result.put("message", "UPDATE_SUCESS");
        result.put("category", resultCategory);
      } else {
        result.put("message", "UPDATE_DUPLICATED");
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      result.put("message", "UPDATE_FAILED");
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  // id로 Entity 찾기
  public BasicQACategoryEntity getEntityById(List<BasicQACategoryEntity> categoryEntities, String id) {
    BasicQACategoryEntity result = null;
    for (BasicQACategoryEntity categoryEntity : categoryEntities) {
      if (id.equals(categoryEntity.getId())) {
        result = categoryEntity;
        break;
      } else {
        result = getEntityById(categoryEntity.getChildren(), id);
        if (result != null) {
          break;
        }
      }
    }
    return result;
  }

  public void updateChildEntity(List<BasicQACategoryEntity> categoryEntityList, String oldStr,
      String newStr) {

    for (BasicQACategoryEntity categoryEntity : categoryEntityList) {
      String[] pathArr = categoryEntity.getCatPath().split(">");

      for (int i = 0; i < pathArr.length; i++) {
        if (oldStr.equals(pathArr[i])) {
          pathArr[i] = newStr;
        }
      }

      categoryEntity.setUpdatedAt(new Date());
      categoryEntity.setCatPath(String.join(">", pathArr));
      basicQACategoryService.updateCategory(categoryEntity);

      if (categoryEntity.getChildren().size() > 0) {
        updateChildEntity(categoryEntity.getChildren(), oldStr, newStr);
      }
    }
  }
}
