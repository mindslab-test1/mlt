package ai.maum.mlt.basicqa.service;

import ai.maum.mlt.basicqa.entity.BasicQAParaphraseWordEntity;
import ai.maum.mlt.basicqa.repository.BasicQAParaphraseWordRepository;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BasicQAParaphraseWordServiceImpl implements BasicQAParaphraseWordService {

  @Autowired
  private BasicQAParaphraseWordRepository basicQAParaphraseWordRepository;

  @Override
  public BasicQAParaphraseWordEntity selectOneByWord(String word) {
    return basicQAParaphraseWordRepository.findOneByWord(word);
  }

  @Override
  public List<BasicQAParaphraseWordEntity> selectListByWord(String word) {
    return basicQAParaphraseWordRepository.findAllByWord(word);
  }

  @Override
  public List<BasicQAParaphraseWordEntity> selectListByWordList(List<String> word) {
    return basicQAParaphraseWordRepository.findMainWordByInKeyword(word);
  }

  @Transactional
  @Override
  public List<BasicQAParaphraseWordEntity> insertWordsList(
      List<BasicQAParaphraseWordEntity> basicQAParaphraseWordEntities) {
    return basicQAParaphraseWordRepository.save(basicQAParaphraseWordEntities);
  }

  @Transactional
  @Override
  public void deleteWords(List<BasicQAParaphraseWordEntity> basicQAParaphraseWordEntities) {
    basicQAParaphraseWordRepository.delete(basicQAParaphraseWordEntities);
  }
}
