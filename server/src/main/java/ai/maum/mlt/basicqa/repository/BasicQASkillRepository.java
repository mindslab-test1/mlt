package ai.maum.mlt.basicqa.repository;

import ai.maum.mlt.basicqa.entity.BasicQASkillEntity;
import javax.transaction.Transactional;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface BasicQASkillRepository extends
    JpaRepository<BasicQASkillEntity, String> {

  List<BasicQASkillEntity> findAllByOrderByNameAsc();

  BasicQASkillEntity findByName(String name);

  BasicQASkillEntity findById(Integer id);
}
