package ai.maum.mlt.basicqa.repository;

import ai.maum.mlt.basicqa.entity.BasicQACategoryEntity;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface BasicQACategoryRepository extends JpaRepository<BasicQACategoryEntity, String> {

  /**
   * parCatId와 catName 으로 Category 정보 조회
   * get은 데이터가 없으면 Exception, find는 null
   * @param parCatId Parent Category Id
   * @param catName Category name
   * @return
   */
  BasicQACategoryEntity findByParCatIdAndCatName(String parCatId, String catName);
}
