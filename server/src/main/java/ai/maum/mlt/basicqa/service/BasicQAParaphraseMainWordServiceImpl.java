package ai.maum.mlt.basicqa.service;

import ai.maum.mlt.basicqa.entity.BasicQAParaphraseMainWordEntity;
import ai.maum.mlt.basicqa.entity.BasicQAParaphraseRelEntity;
import ai.maum.mlt.basicqa.entity.BasicQAParaphraseWordEntity;
import ai.maum.mlt.basicqa.repository.BasicQAParaphraseMainWordRepository;
import ai.maum.mlt.basicqa.repository.BasicQAParaphraseRelRepository;
import ai.maum.mlt.basicqa.repository.BasicQAParaphraseWordRepository;
import ai.maum.mlt.common.util.FileUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class BasicQAParaphraseMainWordServiceImpl implements BasicQAParaphraseMainWordService {

  @Autowired
  private BasicQAParaphraseMainWordRepository basicQAParaphraseMainWordRepository;

  @Autowired
  private BasicQAParaphraseWordRepository basicQAParaphraseWordRepository;

  @Autowired
  private BasicQAParaphraseRelRepository basicQAParaphraseRelRepository;

  @Autowired
  private FileUtils fileUtils;

  @Value("#{'${excel.bsqa.paraphrase.columns}'.split(',')}")
  private List<String> bsqaColumns;

  @Value("#{'${excel.bsqa.paraphrase.headers}'.split(',')}")
  private List<String> bsqaHeaders;

  @Value("${excel.bsqa.paraphrase.filename}")
  private String basqFileName;

  @Override
  public List<BasicQAParaphraseMainWordEntity> selectListByWord(String word) {
    return basicQAParaphraseMainWordRepository.findAllByWord(word);
  }

  @Override
  public Page<BasicQAParaphraseMainWordEntity> selectList(String useYn, String searchMainWord,
      String searchWords, Pageable pageable) {
    return basicQAParaphraseMainWordRepository
        .findByKeywordAndUseYn(useYn, searchMainWord, searchWords, pageable);
  }

  @Override
  public List<BasicQAParaphraseMainWordEntity> selectListByWordList(List<String> word) {
    return basicQAParaphraseMainWordRepository.findMainWordByInKeyword(word);
  }

  @Transactional
  @Override
  public BasicQAParaphraseMainWordEntity insertWords(
      BasicQAParaphraseMainWordEntity basicQAParaphraseMainWordEntity) {
    Integer maxId = basicQAParaphraseMainWordRepository.selectMaxId();
    if ((maxId == null) || (maxId.equals("undefined"))) {
      maxId = 0;
    }
    basicQAParaphraseMainWordEntity.setCreatedAt(new Date());
    basicQAParaphraseMainWordEntity.setUpdatedAt(new Date());
    basicQAParaphraseMainWordEntity.setId(maxId + 1);
    return basicQAParaphraseMainWordRepository.save(basicQAParaphraseMainWordEntity);
  }

  @Transactional
  @Override
  public BasicQAParaphraseMainWordEntity updateWord(
      BasicQAParaphraseMainWordEntity basicQAParaphraseMainWordEntity) {
    basicQAParaphraseMainWordEntity.setUpdatedAt(new Date());
    return basicQAParaphraseMainWordRepository.save(basicQAParaphraseMainWordEntity);
  }

  @Transactional
  @Override
  public void deleteWord(
      List<BasicQAParaphraseMainWordEntity> basicQAParaphraseMainWordEntities) {
    basicQAParaphraseMainWordRepository.delete(basicQAParaphraseMainWordEntities);
  }

  @Transactional
  @Override
  public void uploadFiles(List<MultipartFile> files) throws Exception {

    List<List> fileObjectList = null;
    List<BasicQAParaphraseMainWordEntity> bsqaMainWordEntities = new ArrayList<>();
    List<BasicQAParaphraseWordEntity> bsqaWordEntities = new ArrayList<>();
    List<String> mainWordList = new ArrayList<>();
    List<String> wordList = new ArrayList<>();
    List<String> duplicatedWordList = new ArrayList<>();
    Date now = new Date();

    int i = 1;
    for (MultipartFile file : files) {
      fileObjectList = fileUtils.excelFileToObject(file, bsqaColumns);
      for (List<HashMap> fileObjects : fileObjectList) {
        for (HashMap<String, Object> object : fileObjects) {
          if (object.size() > 0) {
            if (!mainWordList.contains(object.get("mainWord"))) {
              BasicQAParaphraseMainWordEntity bsqaMainWordEntity = new BasicQAParaphraseMainWordEntity();
              bsqaMainWordEntity.setId(i++);
              bsqaMainWordEntity.setWord((String) object.get("mainWord"));
              bsqaMainWordEntity.setUseYn((String) object.get("useYn"));
              bsqaMainWordEntity.setCreatedAt(now);
              bsqaMainWordEntities.add(bsqaMainWordEntity);
              mainWordList.add((String) object.get("mainWord"));
            }
            if ((!mainWordList.contains(object.get("word"))) && (!object.get("word").equals(""))
                && (object.get("word") != null)) {
              BasicQAParaphraseWordEntity bsqaWordEntity = new BasicQAParaphraseWordEntity();
              bsqaWordEntity.setWord((String) object.get("word"));
              bsqaWordEntity.setCreatedAt(now);
              bsqaWordEntity.setMainWord((String) object.get("mainWord"));
              bsqaWordEntities.add(bsqaWordEntity);
              if (wordList.contains(object.get("word"))) {
                duplicatedWordList.add((String) object.get("word"));
              } else {
                wordList.add((String) object.get("word"));
              }
            }
          }
        }
      }
    }
    basicQAParaphraseRelRepository.deleteAll();
    basicQAParaphraseWordRepository.deleteAll();
    basicQAParaphraseMainWordRepository.deleteAll();
    List<BasicQAParaphraseWordEntity> filteredBsqaWordEntities = new ArrayList<>();

    List<BasicQAParaphraseMainWordEntity> insertedBsqaMainWordEntities = basicQAParaphraseMainWordRepository
        .save(bsqaMainWordEntities);

    List<String> checkInsertedWord = new ArrayList<>();

    List<BasicQAParaphraseWordEntity> insertedInList = new ArrayList<>();

    for (BasicQAParaphraseMainWordEntity bsqaMainWordEntity : insertedBsqaMainWordEntities) {
      for (BasicQAParaphraseWordEntity bsqaWordEntity : bsqaWordEntities) {
        if (bsqaWordEntity.getMainWord()
            .equals(bsqaMainWordEntity.getWord())) {
          bsqaWordEntity.setMainId(bsqaMainWordEntity.getId());
          bsqaWordEntity.setCreatedAt(now);
          if (duplicatedWordList.contains(bsqaWordEntity.getWord())) {
            if (checkInsertedWord.contains(bsqaWordEntity.getWord())) {
              insertedInList.add(bsqaWordEntity);
            } else {
              filteredBsqaWordEntities.add(bsqaWordEntity);
              checkInsertedWord.add(bsqaWordEntity.getWord());
            }
          } else {
            filteredBsqaWordEntities.add(bsqaWordEntity);
          }
        }
      }
    }

    List<BasicQAParaphraseWordEntity> insertedInDB = basicQAParaphraseWordRepository
        .save(filteredBsqaWordEntities);
    for (BasicQAParaphraseWordEntity wordInList : insertedInList) {
      for (BasicQAParaphraseWordEntity wordInDB : insertedInDB) {
        if (wordInList.getWord().equals(wordInDB.getWord())) {
          wordInList.setId(wordInDB.getId());
        }
      }
    }

    insertedInList.addAll(insertedInDB);

    List<BasicQAParaphraseRelEntity> relEntities = new ArrayList<>();
    for (BasicQAParaphraseWordEntity word : insertedInList) {
      BasicQAParaphraseRelEntity rel = new BasicQAParaphraseRelEntity();
      rel.setParaphraseId(word.getId());
      rel.setMainId(word.getMainId());
      relEntities.add(rel);
    }

    basicQAParaphraseRelRepository.save(relEntities);
  }

  @Override
  public InputStreamResource downloadFile() throws Exception {

    List<BasicQAParaphraseMainWordEntity> basicQAParaphraseMainWordEntities = basicQAParaphraseMainWordRepository
        .findAll();

    for (BasicQAParaphraseMainWordEntity basicQAParaphraseMainWordEntity : basicQAParaphraseMainWordEntities) {
      basicQAParaphraseMainWordEntity.setBasicQAParaphraseRelEntities(
          basicQAParaphraseRelRepository.findAllByMainId(basicQAParaphraseMainWordEntity.getId()));
    }

    List<BasicQAParaphraseWordEntity> basicQAParaphraseWordEntities = new ArrayList<>();
    for (BasicQAParaphraseMainWordEntity basicQAParaphraseMainWordEntity : basicQAParaphraseMainWordEntities) {
      List<BasicQAParaphraseWordEntity> wordEntities = new ArrayList<>();
      for (BasicQAParaphraseRelEntity rel : basicQAParaphraseMainWordEntity
          .getBasicQAParaphraseRelEntities()) {
        wordEntities.add(rel.getBasicQAParaphraseWordEntity());
      }
      if (wordEntities.size() == 0) {
        BasicQAParaphraseWordEntity emptyWordEntity = new BasicQAParaphraseWordEntity();
        emptyWordEntity.setMainWord(basicQAParaphraseMainWordEntity.getWord());
        emptyWordEntity.setUseYn(basicQAParaphraseMainWordEntity.getUseYn());
        basicQAParaphraseWordEntities.add(emptyWordEntity);
      } else {
        for (BasicQAParaphraseWordEntity basicQAParaphraseWordEntity : wordEntities) {
          basicQAParaphraseWordEntity.setMainWord(basicQAParaphraseMainWordEntity.getWord());
          basicQAParaphraseWordEntity.setUseYn(basicQAParaphraseMainWordEntity.getUseYn());
          basicQAParaphraseWordEntities.add(basicQAParaphraseWordEntity);
        }
      }
    }

    return fileUtils
        .ObjectToExcel(basicQAParaphraseWordEntities, bsqaHeaders, bsqaColumns, basqFileName);
  }
}
