package ai.maum.mlt.basicqa.service;

import ai.maum.mlt.basicqa.entity.BasicQAIndexingHistoryEntity;
import ai.maum.mlt.basicqa.repository.BasicQAIndexingHistoryRepository;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BasicQAIndexingHistoryServiceImpl implements BasicQAIndexingHistoryService {

  @Autowired
  BasicQAIndexingHistoryRepository basicQAIndexingHistoryRepository;

  @Override
  public BasicQAIndexingHistoryEntity selectLastIndexingHistory(String address) {
    return basicQAIndexingHistoryRepository.findTopByAddressOrderByCreatedAt(address);
  }


  @Transactional
  @Override
  public BasicQAIndexingHistoryEntity insertIndexingHistory(
      BasicQAIndexingHistoryEntity basicQAIndexingHistoryEntity) {
    return basicQAIndexingHistoryRepository.save(basicQAIndexingHistoryEntity);
  }

  @Transactional
  @Override
  public BasicQAIndexingHistoryEntity updateIndexingHistory(
      BasicQAIndexingHistoryEntity basicQAIndexingHistoryEntity) {
    return basicQAIndexingHistoryRepository.save(basicQAIndexingHistoryEntity);
  }
}
