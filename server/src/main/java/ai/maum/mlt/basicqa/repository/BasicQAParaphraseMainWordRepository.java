package ai.maum.mlt.basicqa.repository;

import ai.maum.mlt.basicqa.entity.BasicQAParaphraseMainWordEntity;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface BasicQAParaphraseMainWordRepository extends
    JpaRepository<BasicQAParaphraseMainWordEntity, String> {


  @Query(value = "SELECT DISTINCT BQAMW "
      + "         FROM BasicQAParaphraseMainWordEntity BQAMW "
      + "         LEFT OUTER JOIN BQAMW.basicQAParaphraseRelEntities BQAPRE "
      + "         LEFT OUTER JOIN BQAPRE.basicQAParaphraseWordEntity BQAW "
      + "         WHERE (BQAMW.useYn = :useYn OR :useYn IS NULL) "
      + "         AND (BQAMW.word LIKE CONCAT('%',:searchMainWord,'%') OR :searchMainWord IS NULL) "
      + "         AND (BQAW.word LIKE CONCAT('%',:searchWords,'%') OR :searchWords IS NULL)"
  )
  Page<BasicQAParaphraseMainWordEntity> findByKeywordAndUseYn(
      @Param("useYn") String useYn,
      @Param("searchMainWord") String searchMainWord,
      @Param("searchWords") String searchWords,
      Pageable pageable
  );

  @Query(value = "SELECT MAX(BQA.id)"
      + "         FROM BasicQAParaphraseMainWordEntity BQA "
  )
  Integer selectMaxId();

  List<BasicQAParaphraseMainWordEntity> findAllByWord(String word);

  @Query(value = "SELECT BQA "
      + "         FROM BasicQAParaphraseMainWordEntity BQA "
      + "         WHERE BQA.word in(:keyword)"
  )
  List<BasicQAParaphraseMainWordEntity> findMainWordByInKeyword(@Param("keyword") List keyword);
}
