package ai.maum.mlt.basicqa.service;

import ai.maum.mlt.basicqa.entity.BasicQACategoryEntity;
import java.util.HashMap;
import java.util.List;


public interface BasicQACategoryService {

  /**
   * parCatId와 catName 으로 Category 정보 조회
   * @param parCatId Parent Category Id
   * @param catName Category name
   * @return
   */
  BasicQACategoryEntity findByParCatIdAndCatName(String parCatId, String catName);

  /**
   * Category 전체 조회
   * @return
   */
  List<BasicQACategoryEntity> findCategoryAllList();

  /**
   * MLT_CODE에 등록된 root category만 추출하여 resultMap에 저장
   * @param rootGrpCd group Category Code 정보
   * @return
   */
  HashMap<String, List<BasicQACategoryEntity>> findCategoryTree(String rootGrpCd);

  /**
   * Category 정보 update
   * @param category
   * @return
   */
  BasicQACategoryEntity updateCategory(BasicQACategoryEntity category);

  /**
   * Category 정보 insert
   * @param category
   * @return
   */
  BasicQACategoryEntity insertCategory(BasicQACategoryEntity category);

  /**
   * Category 정보 delete
   * @param id
   * @return
   */
  boolean deleteCategory(String id);
}
