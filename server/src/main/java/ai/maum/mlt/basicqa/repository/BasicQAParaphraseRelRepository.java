package ai.maum.mlt.basicqa.repository;

import ai.maum.mlt.basicqa.entity.BasicQAParaphraseRelEntity;
import ai.maum.mlt.basicqa.entity.BasicQAParaphraseWordEntity;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface BasicQAParaphraseRelRepository extends
    JpaRepository<BasicQAParaphraseRelEntity, String> {

  List<BasicQAParaphraseRelEntity> findAllByMainId(Integer mainId);
}
