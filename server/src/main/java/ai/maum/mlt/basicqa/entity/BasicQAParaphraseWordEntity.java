package ai.maum.mlt.basicqa.entity;

import ai.maum.mlt.common.auth.entity.UserEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Date;
import javax.persistence.*;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;

@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "BRAIN_BQA_PRPRS_WD")
public class BasicQAParaphraseWordEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BQA_PRPRS_WORD_GEN")
  @SequenceGenerator(name = "BQA_PRPRS_WORD_GEN", sequenceName = "BRAIN_BQA_PRPRS_WORD_SEQ", allocationSize = 5)
  @Column(name = "ID")
  private Integer id;

  @Column(name = "WORD", length = 40, nullable = false)
  private String word;

  @Column(name = "CREATOR_ID", length = 40)
  private String creatorId;

  @CreatedDate
  @Column(name = "CREATED_AT")
  private Date createdAt;

  @ManyToOne
  @JoinColumn(name = "CREATOR_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity createUserEntity;

  @Transient
  @JsonIgnore
  private String mainWord;

  @Transient
  private Integer mainId;

  @Transient
  @JsonIgnore
  private String useYn;
}
