package ai.maum.mlt.nqa.service;

import ai.maum.mlt.nqa.entity.IndexingHistoryEntity;
import ai.maum.mlt.nqa.repository.IndexingHistoryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NQAIndexingHistoryServiceImpl implements NQAIndexingHistoryService {

  private static final Logger logger = LoggerFactory
      .getLogger(NQAIndexingHistoryServiceImpl.class);

  @Autowired
  private IndexingHistoryRepository indexingHistoryRepository;

  /**
   * address 기준 가장 마지막 history DB에서 조회
   * @param address
   * @return
   * @throws Exception
   */
  @Override
  public IndexingHistoryEntity selectLastIndexingHistory(String address) throws Exception {
    logger.debug("===== selectLastIndexingHistory address {}", address);
    try {
      return indexingHistoryRepository.findTopByAddressOrderByCreatedAtDesc(address);
    } catch (Exception e) {
      logger.error("===== selectLastIndexingHistory :: {}", e.getMessage());
      throw e;
    }
  }

  /**
   * indexingHistory DB Insert
   * @param indexingHistoryEntity
   * @return
   * @throws Exception
   */
  @Override
  public IndexingHistoryEntity insertIndexingHistory(IndexingHistoryEntity indexingHistoryEntity) throws Exception {
    logger.debug("===== insertIndexingHistory indexingHistoryEntity {}", indexingHistoryEntity);
    try {
      return indexingHistoryRepository.save(indexingHistoryEntity);
    } catch (Exception e) {
      logger.error("===== insertIndexingHistory :: {}", e.getMessage());
      throw e;
    }
  }

  /**
   * indexingHistory DB Update
   * @param indexingHistoryEntity
   * @return
   * @throws Exception
   */
  @Override
  public IndexingHistoryEntity updateIndexingHistory(IndexingHistoryEntity indexingHistoryEntity) throws Exception {
    logger.debug("===== updateIndexingHistory indexingHistoryEntity {}", indexingHistoryEntity);
    try {
      return indexingHistoryRepository.save(indexingHistoryEntity);
    } catch (Exception e) {
      logger.error("===== updateIndexingHistory :: {}", e.getMessage());
      throw e;
    }
  }
}
