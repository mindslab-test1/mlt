package ai.maum.mlt.nqa.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.common.util.MSExcelFileParser;
import ai.maum.mlt.nqa.entity.Answer;
import ai.maum.mlt.nqa.entity.Category;
import ai.maum.mlt.nqa.entity.Layer;
import ai.maum.mlt.nqa.entity.Question;
import ai.maum.mlt.nqa.service.NQAGrpcService;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("api/nqa/qa-sets")
public class NQAQASetController {

  private static final Logger logger = LoggerFactory.getLogger(NQAQASetController.class);

  @Autowired
  private NQAGrpcService nqaGrpcService;

  @Value("${excel.nqa.item.filename}")
  private String nqaFileName;

  @Value("#{'${excel.nqa.item.headers}'.split(',')}")
  private String[] nqaHeaders;

  /**
   * Index 된 keyword 조회
   */
  @UriRoleDesc(role = "nqa^R")
  @RequestMapping(
      value = "/indexed-keywords/{channelId}/{categoryId}",
      method = RequestMethod.GET)
  public ResponseEntity<?> getIndexedKeywords(@PathVariable int channelId,
      @PathVariable int categoryId) {
    logger
        .info("===== call api GET [[/api/nqa/qa-sets/indexed-keywords]] getIndexedKeywords");
    try {
      HashMap<String, Object> result = nqaGrpcService.getIndexedKeywords(channelId, categoryId);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getIndexedKeywords e : ", e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * 전체 Layer를 조회.
   */
  @UriRoleDesc(role = "nqa^R")
  @RequestMapping(
      value = "/layers/category-id/{categoryId}",
      method = RequestMethod.GET)
  public ResponseEntity<?> getLayerList(@PathVariable int categoryId) {
    logger.info("===== call api GET [[/api/nqa/qa-sets/layers/category-id]] categoryId {}",
        categoryId);
    try {
      List<Layer> result = nqaGrpcService.getLayerListByCategory(categoryId);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getLayerList e : ", e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * 카테고리 ID로 answer 목록을 조회
   */
  @UriRoleDesc(role = "nqa^R")
  @RequestMapping(
      value = "/answers/category-id/{categoryId}/{startRow}/{endRow}/{sortModel}/{sortType}",
      method = RequestMethod.POST)
  public ResponseEntity<?> getAnswerList(@PathVariable int categoryId, @PathVariable int startRow,
      @PathVariable int endRow,
      @PathVariable String sortModel, @PathVariable String sortType,
      @RequestBody List<HashMap<String, String>> searchList) {
    logger.info("===== call api [[/api/nqa/qa-sets/answers/category-id]] :: categoryId {}"
    );
    try {
      List<Answer> list = nqaGrpcService
          .getAnswerList(categoryId, startRow, endRow, sortModel, sortType, searchList);
      return new ResponseEntity<>(list, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getAnswerList e : ", e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * answerId로 answer 정보 조회
   */
  @UriRoleDesc(role = "nqa^R")
  @RequestMapping(
      value = "/answers/{id}",
      method = RequestMethod.GET)
  public ResponseEntity<?> getAnswerById(@PathVariable int id) {
    logger.info("===== call api GET [[/api/nqa/qa-sets/answers/{id}]] :: id {}", id);
    try {
      List<Answer> answerList = nqaGrpcService.getAnswerById(id);
      return new ResponseEntity<>(answerList, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getAnswerById e : ", e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * answer 추가
   */
  @UriRoleDesc(role = "nqa^C")
  @RequestMapping(
      value = "/answers/add",
      method = RequestMethod.POST)
  public ResponseEntity<?> addAnswer(@RequestBody Answer answer) {
    logger.info("===== call api [[/api/nqa/qa-sets/answers/add]] :: answer {}", answer);
    try {
      Answer result = nqaGrpcService.addAnswer(answer);
      // todo : id 가 없으면 throw Exception;
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("addAnswer e : ", e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * answer 수정
   */
  @UriRoleDesc(role = "nqa^U")
  @RequestMapping(
      value = "/answers/edit",
      method = RequestMethod.POST)
  public ResponseEntity<?> editAnswer(@RequestBody Answer answer) {
    logger.info("===== call api [[/api/nqa/qa-sets/answers/edit]] :: answer {}", answer);
    try {
      Answer result = nqaGrpcService.editAnswer(answer);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("editAnswer e : ", e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * answer 삭제
   *
   * @param answerIdSets (answer Id, answer Copy Id 배열)
   */
  @UriRoleDesc(role = "nqa^D")
  @RequestMapping(
      value = "/answers/remove",
      method = RequestMethod.POST)
  public ResponseEntity<?> removeAnswer(@RequestBody List<Map<String, Integer>> answerIdSets) {
    logger.info("===== call api [[/api/nqa/qa-sets/answers/remove]] :: answerIdSets {}",
        answerIdSets);
    try {
      int result = nqaGrpcService.removeAnswers(answerIdSets);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("removeAnswer e : ", e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /* 카테고리별 업로드 기능 */

  @UriRoleDesc(role = "nqa^C")
  @RequestMapping(
      value = "/upload-files/{channelId}/{categoryId}",
      method = RequestMethod.POST)
  public ResponseEntity<?> insertFile(@RequestParam("file") MultipartFile file,
      @PathVariable Integer channelId, @PathVariable Integer categoryId) {

    // file : 엑셀 파일 획득
    logger.info("======= call api  [[/api/nqa/qa-sets/upload-files/{channelId}/{categoryId}]] :: "
            + "channelId : {}, categoryId : {}, excel file : {}", channelId, categoryId,
        file.getOriginalFilename());

    int targetSheetNum = 0;

    try {
      InputStream inputStream = file.getInputStream();

      // 엑셀 파일 파싱
      MSExcelFileParser excelParser = new MSExcelFileParser(inputStream, targetSheetNum,
          nqaHeaders, false);
      // todo: category이름을 key로 쓰는 경우 많으므로, 중복 insert 안나게 추가 처리해줘야 함.
      Map<String, Integer> categoryMap = new HashMap<>();
      List<Map<String, String>> dataList = excelParser.getData();
      List<Map<String, Object>> qaSetList = new ArrayList<>();

      Boolean categoryValied = false;
      List<Category> categoryList = nqaGrpcService.getCategoryListByChannelId(channelId);
      for (Category category : categoryList) {
        if (categoryId.equals(category.getId())) {
          categoryValied = true;
        }
      }

      if (!categoryValied) {
        logger.error("===== No category ID [{}]", categoryId);
        throw new Exception();
      }

      // data : 1 Row
      for (Map<String, String> data : dataList) {
//        int tempCategoryId = 0;
//        String categoryName = data.get("Category").trim();
        Answer answer = new Answer();
        Question question = new Question();
        Map<String, Object> qaSetMap = new HashMap<>();

//        todo : 채널별 upload 시 이용할 코드
        // category Name valid check
//        if (categoryMap.containsKey(categoryName)) {
//          tempCategoryId = categoryMap.get(categoryName);
//        } else {
//          for (Category category : categoryList) {
//            if (category.getName().equals(categoryName)) {
//              tempCategoryId = category.getId();
//              categoryMap.put(category.getName(), tempCategoryId);
//            }
//          }
//        }
//
//        if (tempCategoryId == 0) {
//          // todo : 후에 에러가 아니라 validation check로 거르기
//          logger.error("===== No category named [{}]", data.get("Category"));
//          throw new Exception();
//        }
        answer.setCategoryId(categoryId);
        answer.setAnswer(data.get("Answer"));
        answer.setAnswerView(data.get("AnswerView").replaceAll("\n", "<br>"));
        if (data.get("Layer1") != null) {
          answer.setLayerNameAndSection(categoryId, 1, data.get("Layer1"));
        }
        if (data.get("Layer2") != null) {
          answer.setLayerNameAndSection(categoryId, 2, data.get("Layer2"));
        }
        if (data.get("Layer3") != null) {
          answer.setLayerNameAndSection(categoryId, 3, data.get("Layer3"));
        }
        if (data.get("Layer4") != null) {
          answer.setLayerNameAndSection(categoryId, 4, data.get("Layer4"));
        }
        if (data.get("Layer5") != null) {
          answer.setLayerNameAndSection(categoryId, 5, data.get("Layer5"));
        }
        if (data.get("Layer6") != null) {
          answer.setLayerNameAndSection(categoryId, 6, data.get("Layer6"));
        }
        if (data.get("Attribute1") != null) {
          answer.setAttr1(data.get("Attribute1"));
        }
        if (data.get("Attribute2") != null) {
          answer.setAttr2(data.get("Attribute2"));
        }
        if (data.get("Attribute3") != null) {
          answer.setAttr3(data.get("Attribute3"));
        }
        if (data.get("Attribute4") != null) {
          answer.setAttr4(data.get("Attribute4"));
        }
        if (data.get("Attribute5") != null) {
          answer.setAttr5(data.get("Attribute5"));
        }
        if (data.get("Attribute5") != null) {
          answer.setAttr6(data.get("Attribute6"));
        }
        String[] tags = data.get("Tags").split(",");
        List<String> tagList = new ArrayList<>();
        for (String tag : tags) {
          tagList.add(tag.trim());
        }
        answer.setTags(tagList);
        answer.setSource(data.get("Source"));
        question.setQuestion(data.get("Question"));

        qaSetMap.put("answer", answer);
        qaSetMap.put("question", question);
        qaSetList.add(qaSetMap);
      }

      nqaGrpcService.uploadFiles(qaSetList, channelId, categoryId);

      HashMap<String, Object> result = new HashMap<>();
      result.put("status", "success");

      return new ResponseEntity<>(result, HttpStatus.OK);

    } catch (Exception e) {
      logger.error("upload-files: {}", e.getMessage());
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "nqa^R")
  @RequestMapping(
      value = "/download-file/{channelId}/{categoryId}/{listCount}/{sortModel}/{sortType}",
      method = RequestMethod.POST)
  public ResponseEntity<?> downloadFile(@PathVariable Integer channelId,
      @PathVariable Integer categoryId, @PathVariable Integer listCount,
      @PathVariable String sortModel, @PathVariable String sortType, @RequestBody List<HashMap<String, String>> searchList) throws IOException {
    logger.info("======= call api  [[/api/nqa/qa-sets/download-file/"
        + "{channelId}/{categoryId}]] ::: channelId : {}, categoryId : {}", channelId, categoryId);

    try {
      HttpHeaders httpHeaders = new HttpHeaders();
      httpHeaders
          .add(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + nqaFileName
              + "-" + nqaGrpcService.getCategoryById(categoryId).getName() + ".xlsx");
      httpHeaders.add(HttpHeaders.CONTENT_TYPE, "application/octet-stream");

      return new ResponseEntity<>(nqaGrpcService.downloadFile(categoryId, listCount, sortModel, sortType, searchList), httpHeaders,
          HttpStatus.OK);
    } catch (Exception e) {
      logger.error("downloadFile e : ", e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}
