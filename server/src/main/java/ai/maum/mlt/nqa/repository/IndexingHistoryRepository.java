package ai.maum.mlt.nqa.repository;

import ai.maum.mlt.nqa.entity.IndexingHistoryEntity;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface IndexingHistoryRepository extends
    JpaRepository<IndexingHistoryEntity, String> {

  IndexingHistoryEntity findTopByAddressOrderByCreatedAtDesc(String address);

}
