package ai.maum.mlt.nqa.controller;

import ai.maum.mlt.nqa.entity.Category;
import ai.maum.mlt.nqa.entity.Channel;
import ai.maum.mlt.nqa.entity.IndexStatusEntity;
import ai.maum.mlt.nqa.service.NQAGrpcService;
import ai.maum.mlt.common.annotation.UriRoleDesc;
import java.util.HashMap;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/nqa/indexing")
public class NQAIndexingController {

  private static final Logger logger = LoggerFactory.getLogger(NQAIndexingController.class);

  @Autowired
  private NQAGrpcService nqaGrpcService;

  /**
   * Indexing 페이지에서 채널 전체 목록 을 가져오는 API
   * @return
   */
  @UriRoleDesc(role = "nqa^R")
  @RequestMapping(
      value = "/channels",
      method = RequestMethod.GET)
  public ResponseEntity<?> getChannelList() {
    logger.info("===== call api GET [[/api/nqa/indexing/channels]] getChannelList");
    try {
      List<Channel> result = nqaGrpcService.getChannelList();
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getChannelList e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * Indexing 페이지에서 카테고리 전체 목록 을 가져오는 API
   * @return
   */
  @UriRoleDesc(role = "nqa^R")
  @RequestMapping(
      value = "/categories",
      method = RequestMethod.POST)
  public ResponseEntity<?> getCategoryListByChannelId(@RequestBody Integer channelId) {
    logger.info("===== call api GET [[/api/nqa/indexing/categories]] getCategoryListByChannelId");
    try {
      List<Category> result = nqaGrpcService.getCategoryListByChannelId(channelId);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getCategoryListByChannelId e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * fullIndexing 요청 API
   * @param channelId
   * @param categoryId
   * @param collectionType
   * @return
   */
  @UriRoleDesc(role = "nqa^X")
  @RequestMapping(
      value = "/fullIndexing/{channelId}/{categoryId}/{collectionType}",
      method = RequestMethod.GET)
  public ResponseEntity<?> fullIndexing(@PathVariable Integer channelId,
      @PathVariable Integer categoryId, @PathVariable Integer collectionType) {
    logger.info("======= call api  [[/api/nqa/indexing/fullIndexing/"
        + "{channelId}/{categoryId}/{collectionType}]] =======");
    try {
      IndexStatusEntity result = nqaGrpcService.fullIndexing(channelId, categoryId, collectionType);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("fullIndexing e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * additionalIndexing 요청
   * @param channelId
   * @param categoryId
   * @param collectionType
   * @return
   */
  @UriRoleDesc(role = "nqa^X")
  @RequestMapping(
      value = "/additionalIndexing/{channelId}/{categoryId}/{collectionType}",
      method = RequestMethod.GET)
  public ResponseEntity<?> additionalIndexing(@PathVariable Integer channelId,
      @PathVariable Integer categoryId, @PathVariable Integer collectionType) {
    logger.info("======= call api  [[/api/nqa/indexing/additionalIndexing/"
            + "{channelId}/{categoryId}/{collectionType}]] =======");
    try {
      IndexStatusEntity result = nqaGrpcService.additionalIndexing(channelId, categoryId, collectionType);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("additionalIndexing e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * 현재 Indexing 정보 조회
   * @return
   */
  @UriRoleDesc(role = "nqa^X")
  @RequestMapping(
      value = "/getIndexingStatus",
      method = RequestMethod.GET)
  public ResponseEntity<?> getIndexingStatus() {
    logger.info("======= call api  [[/api/nqa/indexing/getIndexingStatus]] =======");
    try {
      IndexStatusEntity result = nqaGrpcService.getIndexingStatus();
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getIndexingStatus e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * abortIndexing 요청
   * @param collectionType
   * @return
   */
  @UriRoleDesc(role = "nqa^X")
  @RequestMapping(
      value = "/abortIndexing/{collectionType}",
      method = RequestMethod.GET)
  public ResponseEntity<?> abortIndexing(@PathVariable Integer collectionType) {
    logger.info("======= call api  [[/api/nqa/indexing/abortIndexing/{collectionType}]] =======");
    try {
      IndexStatusEntity result = nqaGrpcService.abortIndexing(collectionType);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("abortIndexing e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
