package ai.maum.mlt.nqa.entity;

import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
public class ExcelQASet implements Serializable {
  private String Category;
  private String Answer;
  private String AnswerView;
  private String Question;
  private String Layer1;
  private String Layer2;
  private String Layer3;
  private String Layer4;
  private String Layer5;
  private String Layer6;
  private String Attribute1;
  private String Attribute2;
  private String Attribute3;
  private String Attribute4;
  private String Attribute5;
  private String Attribute6;
  private String Tags;
  private String Source;

  public ExcelQASet getClone() {
    ExcelQASet result = new ExcelQASet();
    result.setCategory(this.getCategory());
    result.setAnswer(this.getAnswer());
    result.setAnswerView(this.getAnswerView());
    result.setQuestion(this.getQuestion());
    result.setLayer1(this.getLayer1());
    result.setLayer2(this.getLayer2());
    result.setLayer3(this.getLayer3());
    result.setLayer4(this.getLayer4());
    result.setLayer5(this.getLayer5());
    result.setLayer6(this.getLayer6());
    result.setAttribute1(this.getAttribute1());
    result.setAttribute2(this.getAttribute2());
    result.setAttribute3(this.getAttribute3());
    result.setAttribute4(this.getAttribute4());
    result.setAttribute5(this.getAttribute5());
    result.setAttribute6(this.getAttribute6());
    result.setTags(this.getTags());
    result.setSource(this.getSource());
    return result;
  }
}
