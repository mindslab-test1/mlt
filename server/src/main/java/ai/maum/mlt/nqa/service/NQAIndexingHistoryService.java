package ai.maum.mlt.nqa.service;

import ai.maum.mlt.nqa.entity.IndexingHistoryEntity;

public interface NQAIndexingHistoryService {

  IndexingHistoryEntity selectLastIndexingHistory(String address) throws Exception;

  IndexingHistoryEntity insertIndexingHistory(IndexingHistoryEntity indexingHistoryEntity) throws Exception;

  IndexingHistoryEntity updateIndexingHistory(IndexingHistoryEntity indexingHistoryEntity) throws Exception;
}
