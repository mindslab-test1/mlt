package ai.maum.mlt.nqa.entity;

import ai.maum.mlt.common.auth.entity.UserEntity;
import ai.maum.mlt.common.pagenation.PageParameters;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Date;
import javax.persistence.*;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "MAI_NQA_INDEX_HISTORY")
public class IndexingHistoryEntity extends PageParameters {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "NQA_INDEX_HISTORY_GEN")
  @SequenceGenerator(name = "NQA_INDEX_HISTORY_GEN", sequenceName = "MAI_NQA_IDX_HISTORY_SEQ", allocationSize = 5)
  @Column(name = "ID")
  private Integer id;

  /**
   * fullIndexing OR additionalIndexing
   */
  @Column(name = "TYPE", length = 30)
  private String type;

  /**
   * 색인상태
   */
  @Column(name = "STATUS")
  private Boolean status;

  /**
   * 총 색인할 기존 문장 갯수
   */
  @Column(name = "TOTAL")
  private Integer total;

  /**
   * 색인된 기존 문장 갯수
   */
  @Column(name = "FETCHED")
  private Integer fetched;

  /**
   * 색인된 문장 갯수
   */
  @Column(name = "PROCESSED")
  private Integer processed;

  /**
   * 상태 메세지
   */
  @Column(name = "MESSAGE", length = 100)
  private String message;

  /**
   * nqa 엔진 주소 (ip:port) 추후 db 1대 mlt 서버 2대 이상 nqa 2대 이상 일 경우 구분을 위해 필요
   */
  @Column(name = "ADDRESS")
  private String address;

  /**
   * abortIndexing 여부
   */
  @Column(name = "STOP_YN")
  private Boolean stopYn;

  @Column(name = "CREATOR_ID", length = 40)
  private String creatorId;

  @CreatedDate
  @Column(name = "CREATED_AT")
  private Date createdAt;

  @Column(name = "UPDATER_ID", length = 40)
  private String updaterId;

  @LastModifiedDate
  @Column(name = "UPDATED_AT")
  private Date updatedAt;

  @ManyToOne
  @JoinColumn(name = "CREATOR_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity createUserEntity;

  @ManyToOne
  @JoinColumn(name = "UPDATER_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity updateUserEntity;
}
