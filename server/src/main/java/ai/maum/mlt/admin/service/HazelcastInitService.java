package ai.maum.mlt.admin.service;

import ai.maum.m2u.common.portable.AuthticationPolicyPortable;
import ai.maum.m2u.common.portable.ChatbotPortable;
import ai.maum.m2u.common.portable.DialogAgentActivationInfoPortable;
import ai.maum.m2u.common.portable.DialogAgentInstanceExecutionInfoPortable;
import ai.maum.m2u.common.portable.DialogAgentInstancePortable;
import ai.maum.m2u.common.portable.DialogAgentManagerPortable;
import ai.maum.m2u.common.portable.DialogAgentPortable;
import ai.maum.m2u.common.portable.IntentFinderInstancePortable;
import ai.maum.m2u.common.portable.IntentFinderPolicyPortable;
import ai.maum.m2u.common.portable.PortableClassId;
import ai.maum.m2u.common.portable.SimpleClassifierPortable;
import ai.maum.mlt.common.hzc.HazelcastConnector;
import ai.maum.mlt.itfc.DamGrpcInterfaceManager;
import com.google.protobuf.util.JsonFormat;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import maum.m2u.console.ChatbotOuterClass.AuthticationPolicy;
import maum.m2u.console.ChatbotOuterClass.Chatbot;
import maum.m2u.console.Classifier.SimpleClassifier;
import maum.m2u.console.Da.DialogAgent;
import maum.m2u.console.Da.DialogAgentActivationInfo;
import maum.m2u.console.Da.DialogAgentManager;
import maum.m2u.console.DaInstance.DialogAgentInstance;
import maum.m2u.console.DaInstance.DialogAgentInstanceExecutionInfo;
import maum.m2u.router.v3.Intentfinder.IntentFinderInstance;
import maum.m2u.router.v3.Intentfinder.IntentFinderPolicy;
import maum.m2u.server.Dam.DamKey;
import maum.m2u.server.Dam.DialogAgentManagerRegisterResult;
import maum.m2u.server.Dam.DialogAgentManagerStat;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class HazelcastInitService {

  private HazelcastInstance hzc = HazelcastConnector.getInstance().client;

  private static void delay(long millis) {
    try {
      Thread.sleep(millis);
    } catch (InterruptedException e) {
      log.error("delay e : ", e);
    }
  }

  public void setHazelcastData(String param) throws Exception{
    log.debug("*** setHazelcastData  ***");

    JSONParser parser = new JSONParser();
    Object obj = null;

    try {
      obj = parser.parse(param);
    } catch (ParseException e) {
      throw e;
    }
    JSONObject jsonObject = (JSONObject) obj;
    JSONObject value = null;

    value = (JSONObject) jsonObject.get(PortableClassId.NsDAManager);

    try {
      if (value != null) {
        if (!ImportDialogAgentManager(hzc, value)) {
          return;
        }
        ;
        delay(500);
      }
    } catch (Exception e) {
      throw e;
    }

    value = (JSONObject) jsonObject.get(PortableClassId.NsDA);
    if (value != null) {
      ImportDialogAgent(hzc, value);
      delay(500);
    }
    value = (JSONObject) jsonObject.get(PortableClassId.NsDAActivation);
    if (value != null) {
      ImportDialogAgentActivation(hzc, value);
      delay(500);
    }
    value = (JSONObject) jsonObject.get(PortableClassId.NsSC);
    if (value != null) {
      ImportSimpleClassifier(hzc, value);
      delay(500);
    }
    value = (JSONObject) jsonObject.get(PortableClassId.NsIntentFinderPolicy);
    if (value != null) {
      ImportIntentFinderPolicy(hzc, value);
      delay(500);
    }
    value = (JSONObject) jsonObject.get(PortableClassId.NsIntentFinderPolicy);
    if (value != null) {
      ImportIntentFinderInstance(hzc, value);
      delay(500);
    }
    value = (JSONObject) jsonObject.get(PortableClassId.NsIntentFinderPolicy);
    if (value != null) {
      ImportDialogAgentInstance(hzc, value);
      delay(500);
    }
    value = (JSONObject) jsonObject.get(PortableClassId.NsDAInstExec);
    if (value != null) {
      delay(1500);
      ImportDialogAgentInstanceExec(hzc, value);
      delay(500);
    }
    value = (JSONObject) jsonObject.get(PortableClassId.NsChatbot);
    if (value != null) {
      ImportChatbot(hzc, value);
      delay(500);
    }
    value = (JSONObject) jsonObject.get(PortableClassId.NsChatbotDetail);
    if (value != null) {
      ImportChatbotDetail(hzc, value);
      delay(500);
    }
    value = (JSONObject) jsonObject.get(PortableClassId.NsAuthPolicy);
    if (value != null) {
      ImportAuthPolicy(hzc, value);
      delay(500);
    }
    //Common.invokeBackupSignal("all");
    log.debug("*** Completed setHazelcastData ***");
  }

  private static boolean ImportIntentFinderInstance(HazelcastInstance hzc, JSONObject jsonObject) {
    log.debug("ImportIntentFinderInstance");
    IMap<String, IntentFinderInstancePortable> hzccMap = hzc.getMap(PortableClassId.NsIntentFinderInstance);
    //먼저깨끗이 지우자.
    hzccMap.clear();

    for (Object keyObject : jsonObject.keySet()) {
      String key = (String) keyObject;
      JSONObject value = (JSONObject) jsonObject.get(key);
      IntentFinderInstancePortable portable = new
          IntentFinderInstancePortable();
      IntentFinderInstance.Builder builder = IntentFinderInstance.newBuilder();

      String input = value.toJSONString();
      JsonFormat.Parser parser3 = JsonFormat.parser();
      try {
        parser3.ignoringUnknownFields().merge(input, builder);
        portable.setProtobufObj(builder.build());
        log.debug("  + " + "intentFinderInstance=" + portable.toString());
        hzccMap.set(builder.getName(), portable);
      } catch (Exception e) {
        log.error("ImportIntentFinderInstance e : ", e);
      }
    }

    return true;
  }

  private static boolean ImportIntentFinderPolicy(HazelcastInstance hzc, JSONObject jsonObject) {
    log.debug("ImportIntentFinderPolicy");
    IMap<String, IntentFinderPolicyPortable> hzccMap = hzc.getMap(PortableClassId.NsIntentFinderPolicy);
    //먼저깨끗이 지우자.
    hzccMap.clear();

    for (Object keyObject : jsonObject.keySet()) {
      String key = (String) keyObject;
      JSONObject value = (JSONObject) jsonObject.get(key);
      IntentFinderPolicyPortable portable = new IntentFinderPolicyPortable();
      IntentFinderPolicy.Builder builder = IntentFinderPolicy
          .newBuilder();

      String input = value.toJSONString();
      JsonFormat.Parser parser3 = JsonFormat.parser();
      try {
        parser3.ignoringUnknownFields().merge(input, builder);
        portable.setProtobufObj(builder.build());
        hzccMap.set(builder.getName(), portable);
      } catch (Exception e) {
        log.error("ImportIntentFinderPolicy e : ", e);
      }
    }

    return true;
  }

  private static boolean ImportSimpleClassifier(HazelcastInstance hzc, JSONObject jsonObject) {
    log.debug("ImportSimpleClassifier");
    IMap<String, SimpleClassifierPortable> hzccMap = hzc.getMap(PortableClassId.NsSC);
    //먼저깨끗이 지우자.
    hzccMap.clear();

    for (Object keyObject : jsonObject.keySet()) {
      String key = (String) keyObject;
      JSONObject value = (JSONObject) jsonObject.get(key);
      SimpleClassifierPortable portable = new SimpleClassifierPortable();
      SimpleClassifier.Builder builder = SimpleClassifier.newBuilder();

      String input = value.toJSONString();
      JsonFormat.Parser parser3 = JsonFormat.parser();
      try {
        parser3.ignoringUnknownFields().merge(input, builder);
        portable.setProtobufObj(builder.build());
        hzccMap.set(builder.getName(), portable);
      } catch (Exception e) {
        log.error("ImportSimpleClassifier e : ", e);
      }
    }

    return true;
  }

  private static boolean ImportChatbot(HazelcastInstance hzc, JSONObject jsonObject) {
    log.debug("ImportChatbot");
    IMap<String, ChatbotPortable> hzccMap = hzc.getMap(PortableClassId.NsChatbot);
    //먼저깨끗이 지우자.
    hzccMap.clear();

    for (Object keyObject : jsonObject.keySet()) {
      String key = (String) keyObject;
      JSONObject value = (JSONObject) jsonObject.get(key);
      ChatbotPortable portable = new ChatbotPortable();
      Chatbot.Builder builder = Chatbot.newBuilder();

      String input = value.toJSONString();
      JsonFormat.Parser parser3 = JsonFormat.parser();
      try {
        parser3.ignoringUnknownFields().merge(input, builder);
        portable.setProtobufObj(builder.build());
        hzccMap.set(builder.getName(), portable);
      } catch (Exception e) {
        log.error("ImportChatbot e : ", e);
      }
    }

    return true;
  }

  private static boolean ImportDialogAgentManager(HazelcastInstance hzc, JSONObject jsonObject) throws Exception{
    log.debug("ImportDialogAgentManager");

    IMap<String, DialogAgentManagerPortable> hzccMap = hzc.getMap(PortableClassId.NsDAManager);
    List<DialogAgentManagerPortable> dams = new ArrayList<>();
    boolean nameCheckPassed = true;

    hzccMap.clear();
    for (Object keyObject : jsonObject.keySet()) {
      String key = (String) keyObject;
      JSONObject value = (JSONObject) jsonObject.get(key);
      DialogAgentManagerPortable portable = new DialogAgentManagerPortable();
      DialogAgentManager.Builder builder = DialogAgentManager.newBuilder();

      String input = value.toJSONString();
      JsonFormat.Parser parser3 = JsonFormat.parser();
      try {
        parser3.ignoringUnknownFields().merge(input, builder);
        String damName = builder.getName();
        if (hzccMap.containsKey(damName)) {
          String errMsg = "\n**You may need to delete 'dam.state.json' from ${MAUM_ROOT}/run"
              + " and execute 'svctl restart m2u-dam' on the "
              + damName + "[" + builder.getIp() + ":" + builder.getPort() + "] server.";
          log.debug(errMsg);
          nameCheckPassed = false;
          throw new Exception(errMsg);
        }
        portable.setProtobufObj(builder.build());
        dams.add(portable);
      } catch (Exception e) {
        log.debug("Failed to read DAM json.");
        log.error("ImportDialogAgentManager e : ", e);
        throw e;
      }
    }

    if (!nameCheckPassed) {
      return false;
    }

    for (DialogAgentManagerPortable portable : dams) {
      try {
        DialogAgentManager dam = portable.getProtobufObj();
        log.debug("Call setDamName to "
            + dam.getName() + "[" + dam.getIp() + ":" + dam.getPort() + "]");
        DamGrpcInterfaceManager client = new DamGrpcInterfaceManager(dam.getIp(), dam.getPort());
        DamKey.Builder damKey = DamKey.newBuilder();
        damKey.setName(dam.getName());
        DialogAgentManagerStat stat = client.setDamName(damKey.build());
        if (stat.getResultCode() !=
            DialogAgentManagerRegisterResult.DIALOG_AGENT_MANAGER_ADD_SUCCESS) {
          String errMsg = "\n**You may need to delete 'dam.state.json' from ${MAUM_ROOT}/run"
              + " and execute 'svctl restart m2u-dam' on the "
              + dam.getName() + "[" + dam.getIp() + ":" + dam.getPort() + "] server.";
          log.debug("Failed setDamName: " + stat.getDetailMessage()
              + " [" + stat.getResultCode() + "]");
          log.debug(errMsg);
          throw new Exception(errMsg);
        }
        hzccMap.set(dam.getName(), portable);
      } catch (Exception e) {
        log.debug("Failed setDamName: " + e.getMessage());
        throw e;
      }
    }

    return true;
  }

  private static boolean ImportAuthPolicy(HazelcastInstance hzc, JSONObject jsonObject) {
    log.debug("ImportAuthPolicy");
    IMap<String, AuthticationPolicyPortable> hzccMap = hzc.getMap(PortableClassId.NsAuthPolicy);
    //먼저깨끗이 지우자.
    hzccMap.clear();

    for (Object keyObject : jsonObject.keySet()) {
      String key = (String) keyObject;
      JSONObject value = (JSONObject) jsonObject.get(key);
      AuthticationPolicyPortable portable = new AuthticationPolicyPortable();
      AuthticationPolicy.Builder builder = AuthticationPolicy.newBuilder();

      String input = value.toJSONString();
      JsonFormat.Parser parser3 = JsonFormat.parser();
      try {
        parser3.ignoringUnknownFields().merge(input, builder);
        portable.setProtobufObj(builder.build());
        hzccMap.set(builder.getName(), portable);
      } catch (Exception e) {
        log.error("ImportAuthPolicy e : ", e);
      }
    }

    return true;
  }

  private static boolean ImportDialogAgent(HazelcastInstance hzc, JSONObject jsonObject) {
    log.debug("ImportDialogAgent");
    IMap<String, DialogAgentPortable> hzccMap = hzc.getMap(PortableClassId.NsDA);
    //먼저깨끗이 지우자.
    hzccMap.clear();

    for (Object keyObject : jsonObject.keySet()) {
      String key = (String) keyObject;
      JSONObject value = (JSONObject) jsonObject.get(key);
      DialogAgentPortable portable = new DialogAgentPortable();
      DialogAgent.Builder builder = DialogAgent.newBuilder();

      String input = value.toJSONString();
      JsonFormat.Parser parser3 = JsonFormat.parser();
      try {
        parser3.ignoringUnknownFields().merge(input, builder);
        portable.setProtobufObj(builder.build());
        hzccMap.set(builder.getName(), portable);
      } catch (Exception e) {
        log.error("ImportDialogAgent e : ", e);
      }
    }

    return true;
  }

  private static boolean ImportDialogAgentActivation(HazelcastInstance hzc, JSONObject jsonObject) {
    log.debug("ImportDialogAgentActivation");
    IMap<String, DialogAgentActivationInfoPortable> hzccMap = hzc.getMap(PortableClassId.NsDAActivation);
    //먼저깨끗이 지우자.
    hzccMap.clear();

    for (Object keyObject : jsonObject.keySet()) {
      String key = (String) keyObject;
      JSONObject value = (JSONObject) jsonObject.get(key);
      DialogAgentActivationInfoPortable portable = new DialogAgentActivationInfoPortable();
      DialogAgentActivationInfo.Builder builder = DialogAgentActivationInfo.newBuilder();

      String input = value.toJSONString();
      JsonFormat.Parser parser3 = JsonFormat.parser();
      try {
        parser3.ignoringUnknownFields().merge(input, builder);
        portable.setProtobufObj(builder.build());
        hzccMap.set(builder.getDaName(), portable);
      } catch (Exception e) {
        log.error("ImportDialogAgentActivation e : ", e);
      }
    }

    return true;
  }

  private static boolean ImportChatbotDetail(HazelcastInstance hzc, JSONObject jsonObject) {
    log.debug("ImportChatbotDetail");
    IMap<String, ChatbotPortable> hzccMap = hzc.getMap(PortableClassId.NsChatbotDetail);
    //먼저깨끗이 지우자.
    hzccMap.clear();

    for (Object keyObject : jsonObject.keySet()) {
      String key = (String) keyObject;
      JSONObject value = (JSONObject) jsonObject.get(key);
      ChatbotPortable portable = new ChatbotPortable();
      Chatbot.Builder builder = Chatbot.newBuilder();

      String input = value.toJSONString();
      JsonFormat.Parser parser3 = JsonFormat.parser();
      try {
        parser3.ignoringUnknownFields().merge(input, builder);
        portable.setProtobufObj(builder.build());
        hzccMap.set(builder.getName(), portable);
      } catch (Exception e) {
        log.error("ImportChatbotDetail e : ", e);
      }
    }

    return true;
  }

  private static boolean ImportDialogAgentInstance(HazelcastInstance hzc, JSONObject jsonObject) {
    log.debug("ImportDialogAgentInstance");
    IMap<String, DialogAgentInstancePortable> hzccMap = hzc.getMap(PortableClassId.NsDAInstance);
    //먼저깨끗이 지우자.
    hzccMap.clear();

    for (Object keyObject : jsonObject.keySet()) {
      String key = (String) keyObject;
      JSONObject value = (JSONObject) jsonObject.get(key);
      DialogAgentInstancePortable portable = new DialogAgentInstancePortable();
      DialogAgentInstance.Builder builder = DialogAgentInstance.newBuilder();

      String input = value.toJSONString();
      JsonFormat.Parser parser3 = JsonFormat.parser();
      try {
        parser3.ignoringUnknownFields().merge(input, builder);
        portable.setProtobufObj(builder.build());
        hzccMap.set(builder.getDaiId(), portable);
      } catch (Exception e) {
        log.error("ImportDialogAgentInstance e : ", e);
      }
    }

    return true;
  }

  private static boolean ImportDialogAgentInstanceExec(HazelcastInstance hzc,
      JSONObject jsonObject) {
    log.debug("ImportDialogAgentInstanceExec");
    IMap<String, DialogAgentInstanceExecutionInfoPortable> hzccMap = hzc.getMap(PortableClassId.NsDAInstExec);
    //먼저깨끗이 지우자.
    hzccMap.clear();

    for (Object keyObject : jsonObject.keySet()) {
      String key = (String) keyObject;
      JSONObject value = (JSONObject) jsonObject.get(key);
      DialogAgentInstanceExecutionInfoPortable portable = new DialogAgentInstanceExecutionInfoPortable();
      DialogAgentInstanceExecutionInfo.Builder builder = DialogAgentInstanceExecutionInfo
          .newBuilder();

      String input = value.toJSONString();
      JsonFormat.Parser parser3 = JsonFormat.parser();
      try {
        parser3.ignoringUnknownFields().merge(input, builder);
        portable.setProtobufObj(builder.build());
        hzccMap.set(builder.getDaiId(), portable);
      } catch (Exception e) {
        log.error("ImportDialogAgentInstanceExec e : ", e);
      }
    }

    return true;
  }

}
