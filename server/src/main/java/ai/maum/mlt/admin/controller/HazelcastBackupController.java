package ai.maum.mlt.admin.controller;

import ai.maum.mlt.admin.service.HazelcastBackupService;
import ai.maum.mlt.common.annotation.UriRoleDesc;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RequestMapping("/api/admin/backup/hzc")
@RestController
public class HazelcastBackupController {

  @Autowired
  private HazelcastBackupService hazelcastBackupService;

  @UriRoleDesc(role = "adminbackup^R")
  @RequestMapping(
      value = "/all",
      method = RequestMethod.POST)
  public ResponseEntity<?> getHzcData() {
    log.info("===== call api POST [[/api/admin/backup/hzc/all]]");

    try {

      return new ResponseEntity<>(hazelcastBackupService.getHazelcastData(), HttpStatus.OK);
    } catch (Exception e) {
      log.error("getHzcData: {}", e.getMessage());
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
