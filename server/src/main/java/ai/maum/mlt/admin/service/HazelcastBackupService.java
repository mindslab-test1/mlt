package ai.maum.mlt.admin.service;

import ai.maum.m2u.common.portable.ChatbotPortable;
import ai.maum.m2u.common.portable.DialogAgentActivationInfoPortable;
import ai.maum.m2u.common.portable.DialogAgentInstanceExecutionInfoPortable;
import ai.maum.m2u.common.portable.DialogAgentInstancePortable;
import ai.maum.m2u.common.portable.DialogAgentInstanceResourcePortable;
import ai.maum.m2u.common.portable.DialogAgentManagerPortable;
import ai.maum.m2u.common.portable.DialogAgentPortable;
import ai.maum.m2u.common.portable.IntentFinderInstancePortable;
import ai.maum.m2u.common.portable.IntentFinderPolicyPortable;
import ai.maum.m2u.common.portable.PortableClassId;
import ai.maum.m2u.common.portable.SimpleClassifierPortable;
import ai.maum.mlt.common.hzc.HazelcastConnector;
import ai.maum.mlt.common.util.StringUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.google.protobuf.util.JsonFormat;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Slf4j
@Service
public class HazelcastBackupService {

  @Autowired
  private HazelcastConnector hazelcastConnector;
  private HazelcastInstance hzc;
  private StringUtils stringUtils = new StringUtils();

  @PostConstruct
  public void init() {
    this.hzc = this.hazelcastConnector.getInstance().client;
  }

  public Map<String, Object> getHazelcastData() {
    log.debug("*** getHazelcastData ***");

        JSONObject root_json = new JSONObject();
    ArrayList<String> mapId = new ArrayList<>();
    mapId.add(PortableClassId.NsChatbot);
    mapId.add(PortableClassId.NsChatbotDetail);
    mapId.add(PortableClassId.NsDAManager);
    mapId.add(PortableClassId.NsDA);
    mapId.add(PortableClassId.NsDAInstance);
    mapId.add(PortableClassId.NsDAActivation);
    mapId.add(PortableClassId.NsDAIR);
    mapId.add(PortableClassId.NsDAInstExec);
    mapId.add(PortableClassId.NsIntentFinderInstance);
    mapId.add(PortableClassId.NsIntentFinderPolicy);
    mapId.add(PortableClassId.NsIntentFinderInstExec);
    mapId.add(PortableClassId.NsSC);
    mapId.add(PortableClassId.NsAuthPolicy);

    for(String id : mapId) {
      log.debug("*** get Map [id : {}] ***", id);

      IMap<String, ChatbotPortable> hzMap = hzc.getMap(id);
      JSONObject temp_json = new JSONObject();

      for (String key : hzMap.keySet()) {
        try {
          temp_json.put(key,
              stringUtils.makeJsonString(JsonFormat.printer().print(hzMap.get(key).getProtobufObj())));
        } catch (Exception e) {
          log.error("*** get Map [id : {}] *** {}", id, e);
        }
      }

      root_json.put(id, temp_json);
    }

    Gson gson = new GsonBuilder().setPrettyPrinting().create();
    JsonParser jp = new JsonParser();
    JsonElement je = jp.parse(root_json.toString());
    String prettyJsonString = gson.toJson(je);

    Map<String, Object> retMap = new Gson().fromJson(
        prettyJsonString, new TypeToken<HashMap<String, Object>>() {}.getType()
    );

    return retMap;
  }

}
