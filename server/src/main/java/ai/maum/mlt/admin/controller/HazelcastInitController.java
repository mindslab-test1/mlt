package ai.maum.mlt.admin.controller;

import ai.maum.mlt.admin.service.HazelcastInitService;
import ai.maum.mlt.common.annotation.UriRoleDesc;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RequestMapping("/api/admin/init/hzc")
@RestController
public class HazelcastInitController {

  @Autowired
  private HazelcastInitService hazelcastInitService;

  @UriRoleDesc(role = "admininit^R")
  @RequestMapping(
      value = "/all",
      method = RequestMethod.POST)
  public ResponseEntity<?> initHzcData(@RequestBody HashMap<String, Object> param) {
    log.info("===== call api POST [[/api/admin/init/hzc/all]]");
    log.debug("params :: {}", param);

    try {

      if (!param.containsKey("jsonStr")) {
        HashMap<String, String> result = new HashMap<>();
        result.put("message", "FAIL");
        result.put("error", "Invalid Argument [key : jsonStr]");
        return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
      }

      String jsonStr = (String) param.get("jsonStr");

      this.hazelcastInitService.setHazelcastData(jsonStr);
      return new ResponseEntity<>(null, HttpStatus.OK);
    } catch (Exception e) {
      log.error("initHzcData: {}", e.getMessage());
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}
