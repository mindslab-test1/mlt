package ai.maum.mlt.chatmonitoring.service;

import ai.maum.mlt.chatmonitoring.entity.SessionV3Entity;
import ai.maum.mlt.chatmonitoring.entity.TalkV3Entity;
import ai.maum.mlt.chatmonitoring.repository.SessionRepository;
import ai.maum.mlt.chatmonitoring.repository.TalkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DBTestServiceImpl implements DBTestService {

  @Autowired
  private SessionRepository sessionRepository;

  @Autowired
  private TalkRepository talkRepository;

  @Override
  public List<Long> getSessionList() { return this.sessionRepository.findIdList(); }

  @Override
  public List<SessionV3Entity> getSessionsInfo() {
    return this.sessionRepository.findSessionsInfo();
  }

  @Override
  public List<TalkV3Entity> getAllTalks(Long sessionId) {
    return this.talkRepository.findAllBySessionId(sessionId);
  }

  @Override
  public List<TalkV3Entity> getTalks(Long sessionId, int startSeq, int endSeq) {
    return this.talkRepository.findBySessionIdAndSeqGreaterThanEqualAndSeqLessThanEqual(sessionId, startSeq, endSeq);
  }
}
