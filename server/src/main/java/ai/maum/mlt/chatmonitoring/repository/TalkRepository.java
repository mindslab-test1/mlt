package ai.maum.mlt.chatmonitoring.repository;

import ai.maum.mlt.chatmonitoring.entity.TalkV3Entity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TalkRepository extends JpaRepository<TalkV3Entity, String> {

  List<TalkV3Entity> findAllBySessionId(Long sessionId);

  List<TalkV3Entity> findBySessionIdAndSeqGreaterThanEqualAndSeqLessThanEqual(Long sessionId, int startSeq, int endSeq);
}
