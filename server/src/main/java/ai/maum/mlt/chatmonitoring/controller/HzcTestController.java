package ai.maum.mlt.chatmonitoring.controller;

import ai.maum.m2u.common.portable.DialogSessionV3Portable;
import ai.maum.m2u.common.portable.SessionTalkV3Portable;
import ai.maum.mlt.common.annotation.UriRoleDesc;
import java.text.SimpleDateFormat;
import java.util.*;

import ai.maum.mlt.common.hzc.HazelcastConnector;
import ai.maum.mlt.common.model.NameSpace;
import ai.maum.mlt.common.util.StringUtils;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.query.EntryObject;
import com.hazelcast.query.Predicate;
import com.hazelcast.query.PredicateBuilder;
import maum.m2u.router.v3.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;

@RestController
@RequestMapping("/api/chatmonitoring")
public class HzcTestController {

  static final Logger logger = LoggerFactory.getLogger(HzcTestController.class);
  SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss", Locale.KOREA);

  // hzc 객체 선언
  @Autowired
  private HazelcastConnector hazelcastConnector;
  private HazelcastInstance hzc;

  @PostConstruct
  public void init() {
    this.hzc = this.hazelcastConnector.getInstance().client;
  }

  @UriRoleDesc(role = "chatmonitoring^R")
  @GetMapping(value = "/getHzcSessions")
  public ResponseEntity<?> getHzcSessions() {
    logger.info("===== call api GET [[/api/chatmonitoring/getHzcSessions/]]");
    try {
      IMap<Long, DialogSessionV3Portable> sessionMap = hzc.getMap(NameSpace.MAP.SESSION_V3);

      logger.info("sessionMap: [{}]", sessionMap.keySet());
      return new ResponseEntity<>(sessionMap.keySet(), HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getHzcSessions e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "chatmonitoring^R")
  @GetMapping(value = "/getHzcSessionInfo")
  public ResponseEntity<?> getHzcSessionInfo() {
    logger.info("===== call api GET [[/api/chatmonitoring/getHzcSessionInfo/]]");
    try {
      IMap<Long, DialogSessionV3Portable> sessionMap = hzc.getMap(NameSpace.MAP.SESSION_V3);
      List<Object> result = new ArrayList<>();
      Session.DialogSession dialogSession;

      for (Long sessionId : sessionMap.keySet()) {
        Map<String, String> sessData = new HashMap<>();
        dialogSession = sessionMap.get(sessionId).getProtobufObj();
        sessData.put("id", Long.toString(dialogSession.getId()));
        sessData.put("deviceId", dialogSession.getDeviceId());
        sessData.put("chatbot", dialogSession.getChatbot());
        sessData.put("channel", dialogSession.getChannel());
        sessData.put("vaild", Boolean.toString(dialogSession.getValid()));
        sessData.put("startedAt", Long.toString(dialogSession.getStartedAt().getSeconds()));
        sessData.put("laskTalkedAt", Long.toString(dialogSession.getLastTalkedAt().getSeconds()));

        result.add(sessData);
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getHzcSessionInfo e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "chatmonitoring^R")
  @PostMapping(value = "/getHzcAllTalks")
  public ResponseEntity<?> getHzcAllTalks(@RequestBody Map<String, String> param) {
    logger.info("===== call api POST [[/api/chatmonitoring/getHzcAllTalks/]]");
    try {
      String sessionId = param.get("sessionId");
      EntryObject sessEntry = new PredicateBuilder().getEntryObject();
      final Predicate predicate = sessEntry.get("session_id").equal(sessionId);

      IMap<Long, SessionTalkV3Portable> sessionMap = hzc.getMap(NameSpace.MAP.TALK_V3);
      Collection<SessionTalkV3Portable> scol = sessionMap.values(predicate);

      List<Object> result = new ArrayList<>();
      for (SessionTalkV3Portable session : scol) {
        Map<String, String> sess = new HashMap<>();
          sess.put("seq", Integer.toString(session.getProtobufObj().getSeq()));
          sess.put("in", session.getProtobufObj().getIn());
          sess.put("out", session.getProtobufObj().getOut());
          result.add(sess);
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getHzcAllTalks e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "chatmonitoring^R")
  @PostMapping(value = "/getHzcTalks")
  public ResponseEntity<?> getHzcTalks(@RequestBody Map<String, String> param) {
    logger.info("===== call api POST [[/api/chatmonitoring/getHzcTalks/]]");
    try {
      String sessionId = param.get("sessionId");
      int start = Integer.parseInt(param.get("startSeq"));
      int end = param.get("endSeq") != null ? Integer.parseInt(param.get("endSeq")) : 1000;

      EntryObject sessEntry = new PredicateBuilder().getEntryObject();
      final Predicate predicate = sessEntry.get("session_id").equal(sessionId);

      IMap<Long, SessionTalkV3Portable> sessionMap = hzc.getMap(NameSpace.MAP.TALK_V3);
      Collection<SessionTalkV3Portable> scol = sessionMap.values(predicate);

      List<Object> result = new ArrayList<>();
      for (SessionTalkV3Portable session : scol) {
        int seq = session.getProtobufObj().getSeq();
        if (seq >= start && seq <= end) {
          Map<String, String> sess = new HashMap<>();
          sess.put("seq", Integer.toString(seq));
          sess.put("in", session.getProtobufObj().getIn());
          sess.put("out", session.getProtobufObj().getOut());
          result.add(sess);
        }
      }

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getHzcTalks e : " , e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
