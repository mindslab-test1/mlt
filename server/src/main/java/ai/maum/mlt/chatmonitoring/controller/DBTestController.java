package ai.maum.mlt.chatmonitoring.controller;

import ai.maum.mlt.chatmonitoring.entity.SessionV3Entity;
import ai.maum.mlt.chatmonitoring.entity.SessionV3Keys;
import ai.maum.mlt.chatmonitoring.entity.TalkV3Entity;
import ai.maum.mlt.chatmonitoring.entity.TalkV3Keys;
import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.chatmonitoring.service.DBTestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping("/api/chatmonitoring")
public class DBTestController {

  static final Logger logger = LoggerFactory.getLogger(DBTestController.class);
  SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss", Locale.KOREA);

  @Autowired
  private DBTestService dbTestService;


  @UriRoleDesc(role = "chatmonitoring^R")
  @GetMapping(value = "/getSessions")
  public ResponseEntity<?> getSessions() {
    logger.info("===== call api GET [[/api/chatmonitoring/getSessions/]]");
    try {
      List<Long> entities = dbTestService.getSessionList();

      return new ResponseEntity<>(entities, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getSessions e : ", e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "chatmonitoring^R")
  @GetMapping(value = "/getSessionsInfo")
  public ResponseEntity<?> getSessionsInfo() {
    logger.info("===== call api GET [[/api/chatmonitoring/getSessionsInfo/]]");
    try {
      List<SessionV3Entity> entities = dbTestService.getSessionsInfo();

      return new ResponseEntity<>(entities, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getSessionsInfo e : ", e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "chatmonitoring^R")
  @PostMapping(value = "/getAllTalks")
  public ResponseEntity<?> getAllTalks(@RequestBody TalkV3Entity param) {
    logger.info("===== call api POST [[/api/chatmonitoring/getAllTalks/]]");
    try {
      Long sessionId = param.getSessionId();
      List<TalkV3Entity> entities = dbTestService.getAllTalks(sessionId);

      return new ResponseEntity<>(entities, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getAllTalks e : ", e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "chatmonitoring^R")
  @PostMapping(value = "/getTalks")
  public ResponseEntity<?> getTalks(@RequestBody TalkV3Entity param) {
    logger.info("===== call api POST [[/api/chatmonitoring/getTalks/]]");
    try {
      Long sessionId = param.getSessionId();
      int startSeq = param.getStartSeq();
      int endSeq = param.getEndSeq() > 0 ? param.getEndSeq() : 1000;

      List<TalkV3Entity> entities = dbTestService.getTalks(sessionId, startSeq, endSeq);

      return new ResponseEntity<>(entities, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getTalks e : ", e);
      HashMap<String, String> result = new HashMap<>();
      result.put("message", "FAIL");
      result.put("error", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
