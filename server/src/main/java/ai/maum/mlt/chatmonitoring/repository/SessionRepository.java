package ai.maum.mlt.chatmonitoring.repository;

import ai.maum.mlt.chatmonitoring.entity.SessionV3Entity;
import ai.maum.mlt.chatmonitoring.entity.SessionV3Keys;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface SessionRepository extends JpaRepository<SessionV3Entity, String> {

  @Query(value = "SELECT s.id FROM SessionV3Entity s")
  List<Long> findIdList();

  @Query(value = "SELECT s FROM SessionV3Entity s")
  List<SessionV3Entity> findSessionsInfo();
}
