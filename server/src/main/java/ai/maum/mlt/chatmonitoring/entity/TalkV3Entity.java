package ai.maum.mlt.chatmonitoring.entity;

import ai.maum.mlt.common.pagenation.PageParameters;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@IdClass(TalkV3Keys.class)
@Table(name = "TalkV3")
public class TalkV3Entity extends PageParameters implements Serializable {

  @Id
  @Column(name = "sessSec", nullable = false)
  private Integer sessSec;

  @Id
  @Column(name = "seq", nullable = false)
  private Integer seq;

  @Id
  @Column(name = "sessionId", nullable = false)
  private Long sessionId;

  @Column(name = "opSyncId")
  private String opSyncId;

  @Column(name = "lang")
  private String lang;

  @Column(name = "skill")
  private String skill;

  @Column(name = "intent")
  private String intent;

  @Column(name = "closeSkill")
  private Byte closeSkill;

  @Column(name = "inText")
  private String inText;

  @Column(name = "inputType")
  private String inputType;

  @Column(name = "outText")
  private String outText;

  @Column(name = "speechOut")
  private String speechOut;

  @Column(name = "reprompt")
  private Byte reprompt;

  @Column(name = "extraOut")
  private String extraOut;

  @Column(name = "statusCode")
  private Integer statusCode;

  @Column(name = "dialogResult")
  private String dialogResult;

  @Column(name = "startTime", nullable = false)
  private Timestamp startTime;

  @Column(name = "endTime")
  private Timestamp endTime;

  @Column(name = "feedback")
  private Integer feedback;

  @Column(name = "accuracy")
  private Integer accuracy;

  @Column(name = "transitChatbot")
  private String transitChatbot;

  @Column(name = "opinion")
  private String opinion ;

  @Column(name = "score")
  private Integer score;

  @Column(name = "isIntervened")
  private Byte isIntervened;

  @Column(name = "category1")
  private String category1;

  @Column(name = "category2")
  private String category2;

  @Column(name = "category3")
  private String category3;

  @Column(name = "category4")
  private String category4;

  @Column(name = "category5")
  private String category5;

  @Column(name = "sentiment")
  private String sentiment;

  @Column(name = "reasonOfFail")
  private String reasonOfFail;

  @Column(name = "remarks")
  private String remarks;

  @Transient
  private int startSeq;

  @Transient
  private int endSeq;
}
