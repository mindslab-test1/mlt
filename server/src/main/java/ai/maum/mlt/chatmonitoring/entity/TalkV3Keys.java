package ai.maum.mlt.chatmonitoring.entity;

import java.io.Serializable;

public class TalkV3Keys implements Serializable {

  private Integer sessSec;
  private Integer seq;
  private Long sessionId;

  public TalkV3Keys() {
  }

  public TalkV3Keys(Integer sessSec, Integer seq, Long sessionId) {
    this.sessSec = sessSec;
    this.seq = seq;
    this.sessionId = sessionId;
  }

}
