package ai.maum.mlt.chatmonitoring.entity;

import java.io.Serializable;

public class SessionV3Keys implements Serializable {

  private Integer sessSec;
  private Long id;

  public SessionV3Keys() {
  }

  public SessionV3Keys(Integer sessSec, Long id) {
    this.sessSec = sessSec;
    this.id = id;
  }

}
