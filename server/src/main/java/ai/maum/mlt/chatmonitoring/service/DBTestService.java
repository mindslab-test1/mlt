package ai.maum.mlt.chatmonitoring.service;

import ai.maum.mlt.chatmonitoring.entity.SessionV3Entity;
import ai.maum.mlt.chatmonitoring.entity.SessionV3Keys;
import ai.maum.mlt.chatmonitoring.entity.TalkV3Entity;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface DBTestService {

  List<Long> getSessionList();
  List<SessionV3Entity> getSessionsInfo();

  List<TalkV3Entity> getAllTalks(Long sessionId);
  List<TalkV3Entity> getTalks(Long sessionId, int startSeq, int endSeq);
}
