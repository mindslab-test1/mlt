package ai.maum.mlt.chatmonitoring.entity;

import ai.maum.mlt.common.pagenation.PageParameters;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@IdClass(SessionV3Keys.class)
@Table(name = "SessionV3")
public class SessionV3Entity extends PageParameters implements Serializable {

  @Id
  @Column(name = "sessSec", nullable = false)
  private Integer sessSec;

  @Id
  @Column(name = "id", nullable = false)
  private Long id;

  @Column(name = "userId")
  private String userId;

  @Column(name = "deviceId")
  private String deviceId;

  @Column(name = "deviceType")
  private String deviceType;

  @Column(name = "deviceVersion")
  private String deviceVersion;

  @Column(name = "channel")
  private String channel;

  @Column(name = "peer")
  private String peer;

  @Column(name = "chatbot")
  private String chatbot;

  @Column(name = "intentFinderPolicyName")
  private String intentFinderPolicyName;

  @Column(name = "lastSkill")
  private String lastSkill;

  @Column(name = "lastAgentKey")
  private String lastAgentKey;

  @Column(name = "startAt", nullable = false)
  private Timestamp startAt;

  @Column(name = "lastTalkedAt")
  private Timestamp lastTalkedAt;
}

