package ai.maum.mlt.mrc.service;

import ai.maum.mlt.mrc.entity.AnswerEntity;
import java.util.List;

public interface AnswerService {

  List<AnswerEntity> selectListByQuestionId(String questionId);

  AnswerEntity insertAnswer(AnswerEntity answerEntity);

  List<AnswerEntity> insertAnswerList(List<AnswerEntity> answerEntities);

  AnswerEntity updateAnswer(AnswerEntity answerEntity);

  void deleteAnswer(List<AnswerEntity> answerEntities);
}
