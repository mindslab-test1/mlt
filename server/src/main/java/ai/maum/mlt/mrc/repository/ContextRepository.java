package ai.maum.mlt.mrc.repository;

import ai.maum.mlt.mrc.entity.ContextEntity;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface ContextRepository extends JpaRepository<ContextEntity, String> {

  @Query("SELECT c\n"
      + "FROM  ContextEntity c\n"
      + "WHERE c.workspaceId = :workspaceId\n"
      + "  AND (c.context LIKE CONCAT('%', :context ,'%') OR :context IS NULL)")
  Page<ContextEntity> findAll(Pageable pageable, @Param("workspaceId") String workspaceId,
      @Param("context") String context);


  ContextEntity findById(String id);

  ContextEntity findByIdAndWorkspaceId(String cId, String workspaceId);

  ContextEntity findByPassageId(String passageId);
}
