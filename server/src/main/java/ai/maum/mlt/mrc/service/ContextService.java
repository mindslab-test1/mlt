package ai.maum.mlt.mrc.service;

import ai.maum.mlt.mrc.entity.ContextEntity;
import java.util.List;
import org.springframework.data.domain.Page;

public interface ContextService {

  Page<ContextEntity> selectContextList(ContextEntity contextEntity);

  ContextEntity selectContextById(String id);

  ContextEntity insertContext(ContextEntity contextEntity);

  ContextEntity updateContext(ContextEntity contextEntity);

  void deleteContexts(List<ContextEntity> contextEntities);
}
