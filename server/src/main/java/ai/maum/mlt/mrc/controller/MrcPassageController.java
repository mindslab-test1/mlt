package ai.maum.mlt.mrc.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.common.util.MltException;
import ai.maum.mlt.common.util.StringUtils;
import ai.maum.mlt.mrc.entity.PassageEntity;
import ai.maum.mlt.mrc.service.PassageService;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamSource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("api/mrc/passage")
public class MrcPassageController {

  static final Logger logger = LoggerFactory.getLogger(MrcPassageController.class);

  @Autowired
  private PassageService passageService;

  @Value("${excel.mrc.pqa-list.filename}")
  private String pqaFileName;

  @UriRoleDesc(role = "mrc^R")
  @RequestMapping(
      value = "/list",
      method = RequestMethod.POST)
  public ResponseEntity<?> getPassageList(@RequestBody PassageEntity passageEntityParam) {
    logger.info("======= call api  [[/api/mrc/passage/list]] =======");
    try {
      HashMap<String, Object> result = new HashMap<>();

      Page<PassageEntity> list = passageService.selectPassageList(passageEntityParam);
      result.put("list", list);

      String summary = passageService.getSummary(passageEntityParam.getWorkspaceId());
      result.put("summary", summary);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getPassageList e : ", e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "mrc^R")
  @RequestMapping(
      value = "/id/{id}",
      method = RequestMethod.GET)
  public ResponseEntity<?> getPassageById(@PathVariable String id) {
    logger.info("======= call api  [[/api/mrc/passage/id]] =======");
    try {
      PassageEntity passage = passageService.selectPassageById(id);
      ResponseEntity<PassageEntity> responseEntity = new ResponseEntity<>(passage, HttpStatus.OK);
      return responseEntity;
    } catch (Exception e) {
      logger.error("getPassageById e : ", e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "mrc^C")
  @RequestMapping(
      value = "/add",
      method = RequestMethod.POST)
  public ResponseEntity<?> addPassage(@RequestBody PassageEntity passageEntityParam) {
    logger.info("======= call api  [[/api/mrc/passage/add]] =======");
    try {
//      PassageEntity inserted = passageService.insertPassage(passageEntityParam);
//      return new ResponseEntity<>(inserted, HttpStatus.OK);
      return new ResponseEntity<>("Method is not Implemented.", HttpStatus.NOT_IMPLEMENTED);
    } catch (Exception e) {
      logger.error("addPassage e : ", e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "mrc^U")
  @RequestMapping(
      value = "/edit",
      method = RequestMethod.POST)
  public ResponseEntity<?> editPassage(@RequestBody PassageEntity passageEntityParam) {
    logger.info("======= call api  [[/api/mrc/passage/edit]] =======");
    try {
      PassageEntity updated = passageService.updatePassage(passageEntityParam);
      return new ResponseEntity<>(updated, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("editPassage e : ", e);
      String message = e.getMessage() != null && !e.getMessage().isEmpty() ?
          e.getMessage() :
          "Failed to edit passage.";
      return new ResponseEntity<>(message, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "mrc^D")
  @RequestMapping(
      value = "/remove/list",
      method = RequestMethod.POST)
  public ResponseEntity<?> removePassageList(
      @RequestBody List<PassageEntity> passageEntitiesParam) {
    logger.info("======= call api  [[/api/mrc/passage/remove]] =======");
    try {
//      passageService.deletePassageList(passageEntitiesParam);
      return new ResponseEntity<>(HttpStatus.METHOD_NOT_ALLOWED);
    } catch (Exception e) {
      logger.error("removePassageList e : ", e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "mrc^C")
  @RequestMapping(
      value = "/{workspaceId}/upload-file",
      method = RequestMethod.POST)
  public ResponseEntity<?> uploadFile(@RequestParam("file") List<MultipartFile> files,
      @RequestParam("userId") String creatorId, @PathVariable String workspaceId) {
    logger.info("======= call api  [[/api/mrc/passage/upload-file]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {
      if (workspaceId.isEmpty() || creatorId.isEmpty()) {
        result.put("status", "error");
        result.put("message", "Workspace ID or User ID is missing.");
        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      String report = passageService.uploadFiles(files, creatorId, workspaceId);
      result.put("status", "success");
      result.put("message", report);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("uploadFile e : ", e);
      StringBuilder message = new StringBuilder()
          .append(StringUtils.isUseful(e.getMessage()) ? e.getMessage() : "Failed to upload.");
      if (e instanceof MltException) {
        String detail = ((MltException) e).getDetail();
        if (StringUtils.isUseful(detail)) {
          message.append("\nDetail:\n").append(detail);
        }
      }
      result.put("status", "error");
      result.put("message", message.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "mrc^U")
  @RequestMapping(
      value = "/{workspaceId}/merge-file",
      method = RequestMethod.POST)
  public ResponseEntity<?> mergeFile(@RequestParam("file") List<MultipartFile> files,
      @RequestParam("userId") String creatorId, @PathVariable String workspaceId) {
    logger.info("======= call api  [[/api/mrc/passage/merge-file]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {
      if (workspaceId.isEmpty() || creatorId.isEmpty()) {
        result.put("status", "error");
        result.put("message", "Workspace ID or User ID is missing.");
        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
      }

      String report = passageService.mergeFiles(files, creatorId, workspaceId);
      result.put("status", "success");
      result.put("message", report);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("mergeFile e : ", e);
      StringBuilder message = new StringBuilder()
          .append(StringUtils.isUseful(e.getMessage()) ? e.getMessage() : "Failed to merge.");
      if (e instanceof MltException) {
        String detail = ((MltException) e).getDetail();
        if (StringUtils.isUseful(detail)) {
          message.append("\nDetail:\n").append(detail);
        }
      }
      result.put("status", "error");
      result.put("message", message.toString());

      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "mrc^R")
  @RequestMapping(
      value = "/{workspaceId}/download-passage-set",
      method = RequestMethod.GET)
  public ResponseEntity<?> downloadPassageSet(@PathVariable String workspaceId) throws IOException {
    logger.info(
        "======= call api  [[/api/mrc/passage/{workspaceId}/download-file]] =======");
    try {
      InputStreamSource inputStreamSource = passageService.downloadPassageSet(workspaceId);

      HttpHeaders httpHeaders = new HttpHeaders();
      httpHeaders.add(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename="
          + "passage-set-" + StringUtils.fileDateFormat.format(new Date()) + ".json");
      httpHeaders.add(HttpHeaders.CONTENT_TYPE, "application/octet-stream");

      return new ResponseEntity<>(inputStreamSource, httpHeaders, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("downloadFile e : ", e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "mrc^R")
  @RequestMapping(
      value = "/{workspaceId}/download-learning-set",
      method = RequestMethod.GET)
  public ResponseEntity<?> downloadKorQuadSet(@PathVariable String workspaceId) throws IOException {
    logger.info(
        "======= call api  [[/api/mrc/passage/{workspaceId}/download-korquad]] =======");
    try {
      InputStreamSource inputStreamSource = passageService.downloadLearningSet(workspaceId);

      HttpHeaders httpHeaders = new HttpHeaders();
      httpHeaders.add(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename="
          + "learning-set-" + StringUtils.fileDateFormat.format(new Date()) + ".json");
      httpHeaders.add(HttpHeaders.CONTENT_TYPE, "application/octet-stream");

      return new ResponseEntity<>(inputStreamSource, httpHeaders, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("downloadFile e : ", e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "mrc^R")
  @RequestMapping(
      value = "/{workspaceId}/download-pqa-xlsx",
      method = RequestMethod.GET)
  public ResponseEntity<?> downloadPQAXlsx(@PathVariable String workspaceId) throws IOException {
    logger.info(
        "======= call api  [[/api/mrc/question/{workspaceId}/download-pqa-xlsx]] =======");
    try {
      InputStreamSource inputStreamSource = passageService.downloadPQAXlsx(workspaceId);

      HttpHeaders httpHeaders = new HttpHeaders();
      httpHeaders.add(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename="
          + pqaFileName + "_" + StringUtils.fileDateFormat.format(new Date()) + ".xlsx");
      httpHeaders.add(HttpHeaders.CONTENT_TYPE, "application/octet-stream");

      return new ResponseEntity<>(inputStreamSource, httpHeaders, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("downloadPQAXlsx e : ", e);
      HashMap<String, Object> result = new HashMap<>();
      result.put("message", "Internal Server Error");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}
