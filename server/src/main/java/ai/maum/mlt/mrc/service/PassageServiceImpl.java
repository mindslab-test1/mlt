package ai.maum.mlt.mrc.service;

import ai.maum.brain.mrc.korQuad.Answer;
import ai.maum.brain.mrc.korQuad.KorQuad;
import ai.maum.brain.mrc.korQuad.Paragraph;
import ai.maum.brain.mrc.korQuad.QuestionAndAnswer;
import ai.maum.brain.mrc.passageSet.Passage;
import ai.maum.brain.mrc.passageSet.PassageSet;
import ai.maum.brain.mrc.passageSet.QaPair;
import ai.maum.mlt.common.util.FileUtils;
import ai.maum.mlt.common.util.MltException;
import ai.maum.mlt.common.util.StringUtils;
import ai.maum.mlt.mrc.dto.QuestionAnswerDTO;
import ai.maum.mlt.mrc.entity.AnswerEntity;
import ai.maum.mlt.mrc.entity.ContextEntity;
import ai.maum.mlt.mrc.entity.PassageEntity;
import ai.maum.mlt.mrc.entity.QuestionEntity;
import ai.maum.mlt.mrc.repository.AnswerRepository;
import ai.maum.mlt.mrc.repository.ContextRepository;
import ai.maum.mlt.mrc.repository.PassageRepository;
import ai.maum.mlt.mrc.repository.QuestionRepository;
import com.google.protobuf.util.JsonFormat;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

@Service
public class PassageServiceImpl implements PassageService {

  static final Logger logger = LoggerFactory.getLogger(PassageServiceImpl.class);
  static final Charset UTF_8 = StandardCharsets.UTF_8;

  private static final String mySqlAllPQAQuery = "SELECT\n"
      + " p.ID as passageId, p.WORKSPACE_ID as workspaceId,\n"
      + " p.MAJOR_HEADING AS majorHeading, p.MID_HEADING as midHeading, p.MINOR_HEADING as minorHeading,\n"
      + " p.SEGMENT as segment,\n"
      + " p.MAJOR_REVISION AS majorRevision, p.MID_REVISION as midRevision, p.MINOR_REVISION as minorRevision,\n"
      + " p.LINK as link, p.PASSAGE as passage, q.QUESTION AS question, q.ID as questionId, q.IS_ANCHOR as isAnchor,\n"
      + " a.ANSWER as answer, a.ID as answerId\n"
      + "FROM MAI_MRC_PASSAGE p\n"
      + "  LEFT JOIN MAI_MRC_QUESTION q ON q.PASSAGE_ID = p.ID\n"
      + "  LEFT JOIN MAI_MRC_ANSWER a ON a.QUESTION_ID = q.ID\n"
      + "WHERE p.WORKSPACE_ID = :workspaceId\n"
      + "ORDER BY\n"
      + "    regexp_substr(p.MAJOR_HEADING, '^[a-zA-Z가-힣]*') ASC\n"
      + "  , CAST(regexp_substr(p.MAJOR_HEADING, '[0-9]+') AS UNSIGNED) ASC\n"
      + "  , CAST(RTRIM(regexp_substr(p.MID_HEADING, '[0-9]+ ')) AS UNSIGNED) ASC\n"
      + "  , CAST(RTRIM(regexp_substr(p.MINOR_HEADING, '[0-9]+ ')) AS UNSIGNED) ASC\n"
      + "  , p.SEGMENT IS NULL DESC, p.SEGMENT ASC\n"
      + "  , q.ID ASC, a.ID ASC";

  private static final String oracleAllPQAQuery = "SELECT\n"
      + " p.ID as passageId, p.WORKSPACE_ID as workspaceId,\n"
      + " p.MAJOR_HEADING AS majorHeading, p.MID_HEADING as midHeading, p.MINOR_HEADING as minorHeading,\n"
      + " p.SEGMENT as segment,\n"
      + " p.MAJOR_REVISION AS majorRevision, p.MID_REVISION as midRevision, p.MINOR_REVISION as minorRevision,\n"
      + " p.LINK as link, p.PASSAGE as passage, q.QUESTION AS question, q.ID as questionId, q.IS_ANCHOR as isAnchor,\n"
      + " a.ANSWER as answer, a.ID as answerId\n"
      + "FROM MAI_MRC_PASSAGE p\n"
      + "  LEFT JOIN MAI_MRC_QUESTION q ON q.PASSAGE_ID = p.ID\n"
      + "  LEFT JOIN MAI_MRC_ANSWER a ON a.QUESTION_ID = q.ID\n"
      + "WHERE p.WORKSPACE_ID = :workspaceId\n"
      + "ORDER BY\n"
      + "    regexp_substr(MAJOR_HEADING, '^[a-zA-Z가-힣]*') ASC\n"
      + "  , TO_NUMBER(regexp_substr(MAJOR_HEADING, '[0-9]+')) ASC\n"
      + "  , TO_NUMBER(regexp_substr(p.MID_HEADING, '[0-9]+ ')) ASC\n"
      + "  , TO_NUMBER(regexp_substr(p.MINOR_HEADING, '[0-9]+ ')) ASC\n"
      + "  , p.SEGMENT ASC NULLS FIRST\n"
      + "  , q.ID ASC, a.ID ASC";

  @PersistenceContext
  private EntityManager em;

  @Autowired
  private PassageRepository passageRepository;

  @Autowired
  private ContextRepository contextRepository;

  @Autowired
  private QuestionRepository questionRepository;

  @Autowired
  private AnswerRepository answerRepository;

  @Autowired
  private FileUtils fileUtils;

  @Value("#{'${excel.mrc.pqa-list.columns}'.split(',')}")
  private List<String> pqaColumns;

  @Value("#{'${excel.mrc.pqa-list.headers}'.split(',')}")
  private List<String> pqaHeaders;

  @Value("${excel.mrc.pqa-list.filename}")
  private String pqaFileName;

  @Value("${spring.jpa.database}")
  private String dbKind;

  private Page<PassageEntity> findAll(PassageEntity passageEntityParam) {
    Page<PassageEntity> list;

    passageEntityParam.escapeSearchWords();
    passageEntityParam.setOrderDirection(null);

    switch (dbKind.toLowerCase()) {
      case "mysql":
      case "mariadb":
        list = passageRepository.findAllMySql(
            passageEntityParam.getPageRequest(),
            passageEntityParam.getWorkspaceId(),
            passageEntityParam.getMajorHeading(),
            passageEntityParam.getSearchHeading(),
            passageEntityParam.getPassage()
        );
        break;
      case "oracle":
        list = passageRepository.findAllOracle(
            passageEntityParam.getPageRequest(),
            passageEntityParam.getWorkspaceId(),
            passageEntityParam.getMajorHeading(),
            passageEntityParam.getSearchHeading(),
            passageEntityParam.getPassage());
        break;
      default:
        list = passageRepository.findAllJpa(
            passageEntityParam.getPageRequest(),
            passageEntityParam.getWorkspaceId(),
            passageEntityParam.getMajorHeading(),
            passageEntityParam.getSearchHeading(),
            passageEntityParam.getPassage());
        break;
    }

    return list;
  }

  private Page<PassageEntity> findAll(String workspaceId) {
    PassageEntity passageEntityParam = new PassageEntity();
    passageEntityParam.setPageIndex(0);
    passageEntityParam.setPageSize(Integer.MAX_VALUE);
    passageEntityParam.setWorkspaceId(workspaceId);
    return findAll(passageEntityParam);
  }

  List<QuestionAnswerDTO> findAllPQA(String workspaceId) {
    List<QuestionAnswerDTO> list;

    switch (dbKind.toLowerCase()) {
      case "mysql":
      case "mariadb": {
        Query query = em.createNativeQuery(mySqlAllPQAQuery, "QuestionAnswerDTOMapping")
            .setParameter("workspaceId", workspaceId);
        list = query.getResultList();
        break;
      }
      case "oracle": {
        Query query = em.createNativeQuery(oracleAllPQAQuery, "QuestionAnswerDTOMapping")
            .setParameter("workspaceId", workspaceId);
        list = query.getResultList();
        break;
      }
      default:
        return passageRepository.findAllPQA(workspaceId);
    }

    return list;
  }

  private PassageEntity postProcessPassage(PassageEntity passage, boolean withRevision) {
    em.detach(passage);
    em.detach(passage.getContextEntity());
    passage.setPassage(StringUtils.unescapeMs949(passage.getPassage()));
    passage.setContextEntity(null);
    StringBuilder headings = new StringBuilder()
        .append(passage.getMajorHeading())
        .append(withRevision && StringUtils.isUseful(passage.getMajorRevision()) ?
            passage.getMajorRevision() : "")
        .append(" ")
        .append(passage.getMidHeading())
        .append(withRevision && StringUtils.isUseful(passage.getMidRevision()) ?
            passage.getMidRevision() : "")
        .append(" ")
        .append(passage.getMinorHeading())
        .append(withRevision && StringUtils.isUseful(passage.getMinorRevision()) ?
            passage.getMinorRevision() : "")
        .append(StringUtils.isUseful(passage.getSegment()) ?
            " [" + passage.getSegment() + "]" : "");
    passage.setHeadings(StringUtils.unescapeMs949(headings.toString()));
    passage.setQuestionCnt(String.valueOf(passage.getQuestionEntities().size()));
    for (QuestionEntity questionEntity : passage.getQuestionEntities()) {
      em.detach(questionEntity);
      questionEntity.setQuestion(StringUtils.unescapeMs949(questionEntity.getQuestion()));
      questionEntity.setPassageEntity(null);
      if (questionEntity.getAnswerEntity() != null) {
        em.detach(questionEntity.getAnswerEntity());
        questionEntity.getAnswerEntity().setQuestionEntity(null);
        String answer = questionEntity.getAnswerEntity().getAnswer();
        if (StringUtils.isUseful(answer)) {
          questionEntity.getAnswerEntity().setAnswer(StringUtils.unescapeMs949(answer));
        }
      }
    }

    return passage;
  }

  @Override
  public Page<PassageEntity> selectPassageList(PassageEntity passageEntityParam) {
    Page<PassageEntity> list = findAll(passageEntityParam);

    for (PassageEntity passage : list.getContent()) {
      postProcessPassage(passage, false);
    }

    return list;
  }

  @Override
  public PassageEntity selectPassageById(String id) {
    return postProcessPassage(passageRepository.findOne(id), true);
  }

  @Override
  public String getSummary(String workspaceId) {
    return new StringBuilder()
        .append("Passages:&nbsp;")
        .append(passageRepository.countByWorkspaceId(workspaceId))
        .append("&nbsp;&nbsp;&nbsp;Questions:&nbsp;")
        .append(questionRepository.countByWorkspaceId(workspaceId))
        .append("&nbsp;&nbsp;&nbsp;Answers:&nbsp;")
        .append(answerRepository.countByWorkspaceId(workspaceId))
        .toString();
  }

  @Override
  @Transactional
  public PassageEntity insertPassage(PassageEntity passageEntity) {
    Date now = new Date();
    passageEntity.setCreatedAt(now);
    passageEntity.setUpdatedAt(now);
    // Morph 만들때 버그 발생할수있어서 비슷한 다른문자로 변경함
    passageEntity.setPassage(passageEntity.getPassage().replace("[", "〔"));
    passageEntity.setPassage(passageEntity.getPassage().replace("]", "〕"));
    passageEntity.setPassage(passageEntity.getPassage().replace("'", "`"));
    passageEntity.setPassage(passageEntity.getPassage().replace("’", "`"));
    passageEntity.setPassage(passageEntity.getPassage().replace("‘", "`"));
    passageEntity.setPassage(passageEntity.getPassage().replace("+", "＋"));
    PassageEntity inserted = passageRepository.save(passageEntity);
    return inserted;
  }

  @Override
  @Transactional
  public PassageEntity updatePassage(PassageEntity passageEntity) throws Exception {
    PassageEntity passage = passageRepository.findOne(passageEntity.getId());
    if (passage == null) {
      throw new IndexOutOfBoundsException("ID is not exist.");
    }
    if (!passage.getId().equals(passageEntity.getId())
        || !passage.getWorkspaceId().equals(passageEntity.getWorkspaceId())) {
      throw new IndexOutOfBoundsException("ID or Workspace ID is not match.");
    }
    passage.setContextEntity(null);

    final String passageText = StringUtils.unescapeMs949(passage.getPassage());
    final String workspaceId = passageEntity.getWorkspaceId();
    final String userId = passageEntity.getUpdaterId();
    final Date now = new Date();
    for (QuestionEntity question : passageEntity.getQuestionEntities()) {
      question.setQuestion(StringUtils.escapeMs949(question.getQuestion()));
      if (!StringUtils.isUseful(question.getId())) {
        List<QuestionEntity> questions = questionRepository
            .findAllByPassageIdAndWorkspaceIdAndQuestion(passage.getId(), workspaceId,
                question.getQuestion());
        if (questions.size() == 0) {
          question.setCreatorId(userId);
          question.setCreatedAt(now);
        } else if (questions.size() == 1) {
          if (questions.get(0).getAnswerEntity() != null) {
            AnswerEntity oldAnswer = questions.get(0).getAnswerEntity();
            AnswerEntity newAnswer = question.getAnswerEntity();
            newAnswer.setId(oldAnswer.getId());
            newAnswer.setCreatorId(oldAnswer.getCreatorId());
            newAnswer.setCreatedAt(oldAnswer.getCreatedAt());
          }
          question.getAnswerEntity().setQuestionId(questions.get(0).getId());
          questions.get(0).setAnswerEntity(question.getAnswerEntity());
          question = questions.get(0);
          question.setUpdaterId(userId);
          question.setUpdatedAt(now);
        } else {
          logger.warn("Question[{}] is duplicated.", question.getQuestion());
          continue;
        }
      } else {
        question.setUpdaterId(userId);
        question.setUpdatedAt(now);
      }

      AnswerEntity answer = question.getAnswerEntity();
      if (answer == null || !StringUtils.isUseful(answer.getAnswer())) {
        logger.warn("Answer is null or empty.");
        continue;
      }

      final String answerText = answer.getAnswer();
      int start = passageText.indexOf(answerText);

      if (start < 0) {
        if (answerText.equals(StringUtils.unescapeMs949(passage.getMinorHeading()))) {
          start = 0;
        }
      }
      if (start > -1) {
        answer.setAnswerStart(start);
        answer.setAnswerEnd(start + answerText.length());
        if (!StringUtils.isUseful(answer.getId())) {
          if (!StringUtils.isUseful(question.getId())) {
            question.setAnswerEntity(null);
            question = questionRepository.save(question);
          }
          answer.setQuestionId(question.getId());
          answer.setCreatorId(userId);
          answer.setCreatedAt(now);
        } else {
          question.setAnswerEntity(null);
          questionRepository.save(question);
          answer.setUpdaterId(userId);
          answer.setUpdatedAt(now);
        }
        answer.setAnswer(StringUtils.escapeMs949(answerText));
        answerRepository.save(answer);
      } else {
        logger.warn("Answer is not in passage.");
        continue;
      }
    }

    passage.setUpdaterId(userId);
    passage.setUpdatedAt(now);

    passage = passageRepository.save(passage);
    passage.setContextEntity(null);
    for (QuestionEntity questionEntity : passage.getQuestionEntities()) {
      questionEntity.setPassageEntity(null);
      if (questionEntity.getAnswerEntity() != null) {
        questionEntity.getAnswerEntity().setQuestionEntity(null);
      }
    }

    passageRepository.flush();
    questionRepository.flush();
    answerRepository.flush();

    return passage;
  }

  @Override
  @Transactional
  public void deletePassageList(List<PassageEntity> passageEntities) {
    passageRepository.delete(passageEntities);
  }

  private PassageSet parsePassageSet(List<MultipartFile> files) throws Exception {
    PassageSet.Builder passageSetBuilder = PassageSet.newBuilder();

    for (MultipartFile file : files) {
      Reader reader = new InputStreamReader(file.getInputStream(), UTF_8);
      JsonFormat.parser().ignoringUnknownFields().merge(reader, passageSetBuilder);
      reader.close();
    }

    return passageSetBuilder.build();
  }

  private boolean getHeadingsAndRevisions(String tag, PassageEntity passageEntity) {
    String[] headings;
    {
      headings = StringUtils.isUseful(tag) ? tag.split("\t") : null;
      if (headings == null || (headings.length != 3 && headings.length != 4)) {
        return false;
      }
    }

    for (int i = 0; i < headings.length; i++) {
      String heading = headings[i];
      String revision = null;
      int index = heading.indexOf("<");
      if (index > -1) {
        revision = StringUtils.escapeMs949(heading.substring(index).trim());
        heading = StringUtils.escapeMs949(heading.substring(0, index).trim());
      }
      switch (i) {
        case 0:
          passageEntity.setMajorHeading(heading);
          if (revision != null) {
            passageEntity.setMajorRevision(revision);
          }
          break;
        case 1:
          passageEntity.setMidHeading(heading);
          if (revision != null) {
            passageEntity.setMidRevision(revision);
          }
          break;
        case 2:
          passageEntity.setMinorHeading(heading);
          if (revision != null) {
            passageEntity.setMinorRevision(revision);
          }
          break;
        case 3:
          passageEntity.setSegment(heading);
          break;
      }
    }

    return true;
  }

  /*
   * json 형식의 파일을 파싱하여 passage table에 insert한다.
   * context는 context에 저장한다.
   *
   */
  @Override
  @Transactional
  public String uploadFiles(List<MultipartFile> files, String creatorId, String workspaceId)
      throws Exception {
    StringBuilder report = new StringBuilder();
    String message;
    int invalidHeadings = 0, duplications = 0;

    PassageSet passageSet = parsePassageSet(files);
    int passageSetSize = passageSet.getPassagesCount();
    message = String.format("uploadFiles passages: %d", passageSetSize);
    logger.info(message);
    report.append(message).append("\n");

    List<PassageEntity> passageEntities = new ArrayList<>();
    List<ContextEntity> contextEntities = new ArrayList<>();
    Map<String, List<QaPair>> qas = new HashMap<>();
    List<Passage> passages = passageSet.getPassagesList();
    int num = 0;
    for (Passage passage : passages) {
      num++;
      if (num % 1000 == 0) {
        logger.info("uploadFiles progress: " + num + "/" + passageSetSize);
      }

      PassageEntity passageEntity = new PassageEntity();
      ContextEntity contextEntity = new ContextEntity();

      if (!getHeadingsAndRevisions(passage.getTag(), passageEntity)) {
        message = String.format("Invalid heading [%d] : [%s]", num, passage.getTag());
        logger.debug(message);
        report.append(message).append("\n");
        invalidHeadings++;
        continue;
      }

      passageEntity.setPassage(StringUtils.escapeMs949(passage.getPassage()));
      passageEntity.setLink(passage.getLink());
      passageEntity.setWorkspaceId(workspaceId);
      passageEntity.setCreatorId(creatorId);
      passageEntity.setCreatedAt(new Date());

      passageEntities.add(passageEntity);

      contextEntity.setContext(StringUtils.escapeMs949(passage.getContext()));
      contextEntity.setWorkspaceId(workspaceId);
      contextEntity.setCreatorId(creatorId);
      contextEntity.setCreatedAt(passageEntity.getCreatedAt());

      {
        final String passageKey = passageEntity.getMajorHeading() + "/"
            + passageEntity.getMidHeading() + "/"
            + passageEntity.getMinorHeading() + "/"
            + (StringUtils.isUseful(passageEntity.getSegment()) ?
            passageEntity.getSegment() : "");

        if (qas.containsKey(passageKey)) {
          message = String.format("%d - [%s] is duplicated.", num, passageKey);
          logger.info(message);
          report.append(message).append("\n");
          duplications++;
        } else {
          qas.put(passageKey, passage.getQasList());
        }
      }

      contextEntities.add(contextEntity);
    }

    if (invalidHeadings > 0) {
      throw new MltException("Invalid Heading : " + invalidHeadings + " time(s) found!",
          report.toString());
    }

    if (duplications > 0) {
      throw new MltException("Passage Duplication : " + duplications + " time(s) occurred!",
          report.toString());
    }

    logger.info("uploadFiles Delete All");

    passageRepository.deleteAll();
    contextRepository.deleteAll();
    questionRepository.deleteAll();
    answerRepository.deleteAll();
    passageRepository.flush();
    contextRepository.flush();
    questionRepository.flush();
    answerRepository.flush();

    logger.info("uploadFiles Insert Passage");
    passageEntities = passageRepository.save(passageEntities);

    logger.info("uploadFiles Insert Context");
    for (int i = 0; i < passageEntities.size(); i++) {
      contextEntities.get(i).setPassageId(passageEntities.get(i).getId());
    }
    contextRepository.save(contextEntities);

    logger.info("uploadFiles Insert Q-A");
    int questionCount = 0, answerCount = 0;
    num = 0;
    for (PassageEntity passageEntity : passageEntities) {
      num++;
      if (num % 1000 == 0) {
        logger.info("uploadFiles Insert Q-A: " + num + "/" + passageSetSize);
      }
      final String passageKey = passageEntity.getMajorHeading() + "/"
          + passageEntity.getMidHeading() + "/"
          + passageEntity.getMinorHeading() + "/"
          + (StringUtils.isUseful(passageEntity.getSegment()) ?
          passageEntity.getSegment() : "");

      List<QaPair> qaPairs = qas.get(passageKey);
      for (QaPair qaPair : qaPairs) {
        if (StringUtils.isUseless(qaPair.getQuestion())) {
          continue;
        }
        QuestionEntity questionEntity = new QuestionEntity();
        questionEntity.setWorkspaceId(workspaceId);
        questionEntity.setPassageId(passageEntity.getId());
        questionEntity.setQuestion(StringUtils.escapeMs949(qaPair.getQuestion()));
        questionEntity.setIsAnchor(qaPair.getIsAnchor());
        questionEntity.setCreatorId(passageEntity.getCreatorId());
        questionEntity.setCreatedAt(passageEntity.getCreatedAt());
        questionEntity = questionRepository.save(questionEntity);
        questionCount++;

        if (StringUtils.isUseful(qaPair.getAnswer())) {
          AnswerEntity answerEntity = new AnswerEntity();
          answerEntity.setWorkspaceId(workspaceId);
          answerEntity.setQuestionId(questionEntity.getId());
          answerEntity.setAnswer(StringUtils.escapeMs949(qaPair.getAnswer()));
          answerEntity.setAnswerStart(qaPair.getAnswerStart());
          answerEntity.setAnswerEnd(qaPair.getAnswerEnd());
          answerEntity.setCreatorId(passageEntity.getCreatorId());
          answerEntity.setCreatedAt(passageEntity.getCreatedAt());
          answerRepository.save(answerEntity);
          answerCount++;
        }
      }
    }

    passageRepository.flush();
    contextRepository.flush();
    questionRepository.flush();
    answerRepository.flush();

    report.append("Uploaded:\n")
        .append(String.format("Passage(s): %d   ", passageEntities.size()))
        .append(String.format("Question(s): %d   Answer(s): %d", questionCount, answerCount));

    return report.toString();
  }

  /**
   * json 형식의 파일을 파싱하여 기존 패시지와 병합한다.
   * <pre>
   *  1. headings key로 진행
   *  2. 동일한 headings가 있는 경우
   *    - Passage 비교 후 업데이트
   *    - Context 비교 후 업데이트
   *    - TODO: Q-A set 비교 후 업데이트
   *  3. 동일한 headings가 없는 경우
   *    - Passage, Context, Q, A insert
   *  4. TODO: 패시지가 분절 되거나 통합된 경우
   * </pre>
   */
  @Override
  @Transactional
  public String mergeFiles(List<MultipartFile> files, String creatorId, String workspaceId)
      throws Exception {
    StringBuilder report = new StringBuilder();
    String message;
    int invalidHeadings = 0, inserted = 0, skipped = 0, updated = 0;

    PassageSet passageSet = parsePassageSet(files);
    int passageSetSize = passageSet.getPassagesCount();

    message = String.format("mergeFiles passages: %d", passageSetSize);
    logger.info(message);
    report.append(message).append("\n");

    List<Passage> passages = passageSet.getPassagesList();
    int num = 0;
    for (Passage passage : passages) {
      num++;
      if (num % 1000 == 0) {
        logger.info("mergeFiles progress: " + num + "/" + passageSetSize);
      }

      PassageEntity mergePassage = new PassageEntity();
      boolean insert = false, skip = true;

      if (!getHeadingsAndRevisions(passage.getTag(), mergePassage)) {
        message = String.format("Invalid heading [%d] : [%s]", num, passage.getTag());
        logger.debug(message);
        report.append(message).append("\n");
        invalidHeadings++;
        continue;
      }
      mergePassage.setLink(passage.getLink());
      mergePassage.setPassage(StringUtils.escapeMs949(passage.getPassage()));

      PassageEntity passageEntity = passageRepository
          .findByWorkspaceIdAndMajorHeadingAndMidHeadingAndMinorHeadingAndSegment(
              workspaceId,
              mergePassage.getMajorHeading(),
              mergePassage.getMidHeading(),
              mergePassage.getMinorHeading(),
              mergePassage.getSegment());

      // 패시지가 없는 경우 insert
      if (passageEntity == null) {
        mergePassage.setWorkspaceId(workspaceId);
        mergePassage.setCreatorId(creatorId);
        mergePassage.setCreatedAt(new Date());
        passageEntity = mergePassage;
        insert = true;
        skip = false;
      }
      // 변경 사항이 있는 경우 update
      else if (!passageEntity.contentEquals(mergePassage)) {
        mergePassage.setId(passageEntity.getId());
        mergePassage.setWorkspaceId(passageEntity.getWorkspaceId());
        mergePassage.setCreatorId(passageEntity.getCreatorId());
        mergePassage.setCreatedAt(passageEntity.getCreatedAt());
        mergePassage.setUpdaterId(creatorId);
        mergePassage.setUpdatedAt(new Date());
        mergePassage.setContextEntity(passageEntity.getContextEntity());
        mergePassage.setQuestionEntities(passageEntity.getQuestionEntities());

        passageEntity = mergePassage;
        skip = false;
      }

      if (!skip) {
        passageEntity = passageRepository.save(passageEntity);
      }

      // Context가 없으면 insert, 값이 다르면 update
      ContextEntity contextEntity = passageEntity.getContextEntity();
      String escapedContext = StringUtils.escapeMs949(passage.getContext());
      if (contextEntity == null ||
          !StringUtils.equals(contextEntity.getContext(), escapedContext)) {
        if (contextEntity == null) {
          contextEntity = new ContextEntity();
          contextEntity.setWorkspaceId(workspaceId);
          contextEntity.setPassageId(passageEntity.getId());
          contextEntity.setCreatorId(creatorId);
          contextEntity.setCreatedAt(new Date());
        } else {
          contextEntity.setUpdaterId(creatorId);
          contextEntity.setUpdatedAt(new Date());
        }
        contextEntity.setContext(escapedContext);
        contextRepository.save(contextEntity);
        if (skip) {
          skip = false;
        }
      }

      if (skip) {
        skipped++;
      } else if (insert) {
        inserted++;
      } else {
        updated++;
      }
    }

    passageRepository.flush();
    contextRepository.flush();

    report.append(String.format("Inserted: %d passage(s)\n", inserted))
        .append(String.format("Updated: %d passage(s)\n", updated))
        .append(String.format("Skipped: %d passage(s)\n", skipped))
        .append(String.format("Invalid Heading(s): %d", invalidHeadings));

    return report.toString();
  }

  /*
   * Passage Set을 json 형식으로 전달한다.
   */
  @Override
  public InputStreamResource downloadPassageSet(String workspaceId) throws Exception {
    PassageSet passageSet;
    {
      logger.info("downloadLearningSet passages: {}",
          passageRepository.countByWorkspaceId(workspaceId));

      List<PassageEntity> passageEntities = findAll(workspaceId).getContent();
      int passageSetSize = passageEntities.size();
      logger.info("downloadPassageSet queried: {}", passageSetSize);

      PassageSet.Builder passageSetBuilder = PassageSet.newBuilder();
      StringBuilder tagBuilder = new StringBuilder();
      int num = 0;
      for (PassageEntity passage : passageEntities) {
        num++;
        if (num % 1000 == 0) {
          logger.info("downloadPassageSet progress: " + num + "/" + passageSetSize);
        }
        tagBuilder.setLength(0);
        Passage.Builder psBuilder = Passage.newBuilder();
        psBuilder
            .setTag(StringUtils.unescapeMs949(tagBuilder
                .append(passage.getMajorHeading())
                .append(StringUtils.isUseful(passage.getMajorRevision()) ?
                    passage.getMajorRevision() : "")
                .append("\t")
                .append(passage.getMidHeading())
                .append(StringUtils.isUseful(passage.getMidRevision()) ?
                    passage.getMidRevision() : "")
                .append("\t")
                .append(passage.getMinorHeading())
                .append(StringUtils.isUseful(passage.getMinorRevision()) ?
                    passage.getMinorRevision() : "")
                .append(StringUtils.isUseful(passage.getSegment()) ?
                    "\t" + passage.getSegment() : "")
                .toString()))
            .setPassage(StringUtils.unescapeMs949(passage.getPassage()));
        if (passage.getContextEntity() != null
            && StringUtils.isUseful(passage.getContextEntity().getContext())) {
          psBuilder.setContext(StringUtils.unescapeMs949(passage.getContextEntity().getContext()));
        }
        if (StringUtils.isUseful(passage.getLink())) {
          psBuilder.setLink(passage.getLink());
        }
        for (QuestionEntity questionEntity : passage.getQuestionEntities()) {
          AnswerEntity answerEntity = questionEntity.getAnswerEntity();
          psBuilder.addQas(QaPair.newBuilder()
              .setQuestion(StringUtils.unescapeMs949(questionEntity.getQuestion()))
              .setIsAnchor(questionEntity.getIsAnchor())
              .setAnswer(StringUtils.unescapeMs949(answerEntity.getAnswer()))
              .setAnswerStart(answerEntity.getAnswerStart())
              .setAnswerEnd(answerEntity.getAnswerEnd()));
        }
        passageSetBuilder.addPassages(psBuilder);
      }
      passageSet = passageSetBuilder.build();
    }

    String passageJson = JsonFormat.printer().includingDefaultValueFields().print(passageSet);

    return new InputStreamResource(new ByteArrayInputStream(passageJson.getBytes(UTF_8)));
  }

  /*
   * korQuad 학습 규격의 json파일을 전달한다.
   */
  @Override
  public InputStreamResource downloadLearningSet(String workspaceId) throws Exception {
    KorQuad korQuad;
    {
      logger.info("downloadLearningSet passages: {}",
          passageRepository.countByWorkspaceId(workspaceId));

      List<PassageEntity> passageEntities = findAll(workspaceId).getContent();
      int passageSetSize = passageEntities.size();
      logger.info("downloadLearningSet queried: {}", passageSetSize);

      KorQuad.Builder kqBuilder = KorQuad.newBuilder();
      StringBuilder contextBuilder = new StringBuilder();
      int num = 0;
      for (PassageEntity passage : passageEntities) {
        num++;
        if (num % 1000 == 0) {
          logger.info("downloadLearningSet progress: " + num + "/" + passageSetSize);
        }
        contextBuilder.setLength(0);
        Paragraph.Builder paragraphBuilder = Paragraph.newBuilder();
        String context = StringUtils.unescapeMs949(contextBuilder
            .append(passage.getMajorHeading())
            .append(StringUtils.isUseful(passage.getMajorRevision()) ?
                passage.getMajorRevision() : "")
            .append("\t")
            .append(passage.getMidHeading())
            .append(StringUtils.isUseful(passage.getMidRevision()) ?
                passage.getMidRevision() : "")
            .append("\t")
            .append(passage.getMinorHeading())
            .append(StringUtils.isUseful(passage.getMinorRevision()) ?
                passage.getMinorRevision() : "")
            .append(StringUtils.isUseful(passage.getSegment()) ?
                "\t" + passage.getSegment() : "")
            .append("\n")
            .append(passage.getPassage()).toString());
        paragraphBuilder.setContext(context);
        QuestionAndAnswer.Builder qas = QuestionAndAnswer.newBuilder();
        for (QuestionEntity questionEntity : passage.getQuestionEntities()) {
          AnswerEntity answerEntity = questionEntity.getAnswerEntity();
          if (answerEntity != null && StringUtils.isUseful(answerEntity.getAnswer())) {
            String answer = StringUtils.unescapeMs949(answerEntity.getAnswer());
            qas.addAnswers(Answer.newBuilder()
                .setId(answerEntity.getId())
                .setQuestion(StringUtils.unescapeMs949(questionEntity.getQuestion()))
                .setText(answer)
                .setAnswerStart(context.indexOf(answer)));
          }
        }
        if (qas.getAnswersCount() > 0) {
          paragraphBuilder.addQas(qas);
          kqBuilder.addParagraphs(paragraphBuilder);
        }
      }
      korQuad = kqBuilder.build();
    }

    String passageJson = JsonFormat.printer().includingDefaultValueFields().print(korQuad);

    return new InputStreamResource(new ByteArrayInputStream(passageJson.getBytes(UTF_8)));
  }

  public InputStreamResource downloadPQAXlsx(String workspaceId) throws Exception {
    List<QuestionAnswerDTO> list = findAllPQA(workspaceId);

    for (QuestionAnswerDTO qa : list) {
      if (StringUtils.isUseful(qa.getMajorRevision())) {
        qa.setMajorHeading(qa.getMajorHeading() + qa.getMajorRevision());
      }
      if (StringUtils.isUseful(qa.getMidRevision())) {
        qa.setMidHeading(qa.getMidHeading() + qa.getMidRevision());
      }
      if (StringUtils.isUseful(qa.getMinorRevision())) {
        qa.setMinorHeading(qa.getMinorHeading() + qa.getMinorRevision());
      }
    }

//// 같은 헤딩 및 패시지를 하나만 출력하도록 함.
//    for (int i = list.size() - 1; i > 0; i--) {
//      if (list.get(i).isSamePassage(list.get(i - 1))) {
//        QuestionAnswerDTO pqa = list.get(i);
//        pqa.setMajorHeading(null);
//        pqa.setMidHeading(null);
//        pqa.setMinorHeading(null);
//        pqa.setSegment(null);
//        pqa.setPassage(null);
//      }
//    }

    return fileUtils.ObjectToExcel(list, pqaHeaders, pqaColumns, pqaFileName);
  }
}
