package ai.maum.mlt.mrc.dto;

import ai.maum.mlt.common.pagenation.PageParameters;
import ai.maum.mlt.common.util.StringUtils;
import ai.maum.mlt.mrc.entity.AnswerEntity;
import ai.maum.mlt.mrc.entity.PassageEntity;
import ai.maum.mlt.mrc.entity.QuestionEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import javax.persistence.Transient;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class QuestionAnswerDTO extends PageParameters implements Serializable {

  private String workspaceId;

  private String question;

  private String questionId;

  private Boolean isAnchor = false;

  private String answer;

  @JsonIgnore
  private String answerId;

  private String majorHeading;

  @JsonIgnore
  private String midHeading;

  @JsonIgnore
  private String minorHeading;

  @JsonIgnore
  private String segment;

  @JsonIgnore
  private String majorRevision;

  @JsonIgnore
  private String midRevision;

  @JsonIgnore
  private String minorRevision;

  @Transient
  private String headings;

  @JsonIgnore
  private String link;

  @JsonIgnore
  private String passage;

  private String passageId;

  @Transient
  private String searchQA;

  @Transient
  private String searchHeading;

  @Transient
  private Boolean doValidation = false;

  public QuestionAnswerDTO() {
    super();
  }

  public QuestionAnswerDTO(String passageId, String workspaceId,
      String majorHeading, String midHeading, String minorHeading, String segment,
      String majorRevision, String midRevision, String minorRevision,
      String link, String passage,
      String question, String questionId, Boolean isAnchor,
      String answer, String answerId) {
    super();
    this.passageId = passageId;
    this.workspaceId = workspaceId;
    this.majorHeading = StringUtils.unescapeMs949(majorHeading);
    this.midHeading = StringUtils.unescapeMs949(midHeading);
    this.minorHeading = StringUtils.unescapeMs949(minorHeading);
    this.segment = StringUtils.unescapeMs949(segment);
    this.majorRevision = StringUtils.unescapeMs949(majorRevision);
    this.midRevision = StringUtils.unescapeMs949(midRevision);
    this.minorRevision = StringUtils.unescapeMs949(minorRevision);
    this.headings = getCombinedHeadings();
    this.link = link;
    this.passage = StringUtils.unescapeMs949(passage);
    this.question = StringUtils.unescapeMs949(question);
    this.questionId = questionId;
    this.isAnchor = isAnchor;
    this.answer = StringUtils.unescapeMs949(answer);
    this.answerId = answerId;
  }

  public QuestionAnswerDTO(PassageEntity p, QuestionEntity q) {
    super();
    if (p != null) {
      this.passageId = p.getId();
      this.workspaceId = p.getWorkspaceId();
      this.majorHeading = StringUtils.unescapeMs949(p.getMajorHeading());
      this.midHeading = StringUtils.unescapeMs949(p.getMidHeading());
      this.minorHeading = StringUtils.unescapeMs949(p.getMinorHeading());
      this.segment = StringUtils.unescapeMs949(p.getSegment());
      this.headings = getCombinedHeadings();
      this.majorRevision = StringUtils.unescapeMs949(p.getMajorRevision());
      this.midRevision = StringUtils.unescapeMs949(p.getMidRevision());
      this.minorRevision = StringUtils.unescapeMs949(p.getMinorRevision());
      this.link = p.getLink();
      this.passage = StringUtils.unescapeMs949(p.getPassage());
    }
    if (q != null) {
      this.question = StringUtils.unescapeMs949(q.getQuestion());
      this.questionId = q.getId();
      this.isAnchor = q.getIsAnchor();
      AnswerEntity a = q.getAnswerEntity();
      if (a != null && StringUtils.isUseful(a.getAnswer())) {
        this.answer = StringUtils.unescapeMs949(a.getAnswer());
        this.answerId = a.getId();
      }
    }
  }

  private String getCombinedHeadings() {
    return majorHeading + " " + midHeading + " " + minorHeading
        + (StringUtils.isUseful(segment) ? " [" + segment + "]" : "");
  }

  public void escapeSearchWords() {
    majorHeading = StringUtils.escapeMs949(majorHeading);
    searchHeading = StringUtils.escapeMs949(searchHeading);
    searchQA = StringUtils.escapeMs949(searchQA);
  }

  public boolean isSamePassage(QuestionAnswerDTO qa) {
    return StringUtils.equals(segment, qa.getSegment())
        && StringUtils.equals(minorHeading, qa.getMinorHeading())
        && StringUtils.equals(midHeading, qa.getMidHeading())
        && StringUtils.equals(majorHeading, qa.getMajorHeading())
        && StringUtils.equals(passage, qa.getPassage());
  }
}
