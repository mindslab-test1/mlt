package ai.maum.mlt.mrc.controller;


import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.common.util.StringUtils;
import ai.maum.mlt.mrc.dto.QuestionAnswerDTO;
import ai.maum.mlt.mrc.entity.QuestionEntity;
import ai.maum.mlt.mrc.service.PassageService;
import ai.maum.mlt.mrc.service.QuestionService;
import java.util.HashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/mrc/question")
public class MrcQuestionController {

  static final Logger logger = LoggerFactory.getLogger(MrcQuestionController.class);

  @Autowired
  private QuestionService questionService;

  @Autowired
  private PassageService passageService;

  @UriRoleDesc(role = "mrc^R")
  @RequestMapping(
      value = "/list",
      method = RequestMethod.POST)
  public ResponseEntity<?> getQuestionAnswerList(
      @RequestBody QuestionAnswerDTO questionAnswerParam) {
    logger.info("======= call api  [[/api/mrc/question/list]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {
      Page<QuestionAnswerDTO> list = questionService.selectQuestionAnswerList(questionAnswerParam);
      result.put("list", list);

      String summary = questionAnswerParam.getDoValidation() ?
          "Invalid Answers: " + list.getTotalElements() :
          passageService.getSummary(questionAnswerParam.getWorkspaceId());
      result.put("summary", summary);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getQuestionAnswerList e : ", e);
      StringBuilder message = new StringBuilder()
          .append(StringUtils.isUseful(e.getMessage()) ? e.getMessage() : "Failed to merge.");
      result.put("status", "error");
      result.put("message", message.toString());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "mrc^D")
  @RequestMapping(
      value = "/remove",
      method = RequestMethod.POST)
  public ResponseEntity<?> removeQuestion(@RequestBody QuestionEntity questionEntity) {
    logger.info("======= call api  [[/api/mrc/question/remove]] =======");
    try {
      questionService.deleteQuestion(questionEntity);
      return new ResponseEntity<>(HttpStatus.OK);
    } catch (Exception e) {
      logger.error("e : ", e);
      String message = e.getMessage() != null && !e.getMessage().isEmpty() ?
          e.getMessage() :
          "Failed to remove a question.";
      return new ResponseEntity<>(message, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
