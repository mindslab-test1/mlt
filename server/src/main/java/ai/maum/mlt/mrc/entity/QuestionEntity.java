package ai.maum.mlt.mrc.entity;

import ai.maum.mlt.common.auth.entity.UserEntity;
import ai.maum.mlt.common.auth.entity.WorkspaceEntity;
import ai.maum.mlt.mrc.dto.QuestionAnswerDTO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;


@SqlResultSetMapping(
    name = "QuestionAnswerDTOMapping",
    classes = @ConstructorResult(
        targetClass = QuestionAnswerDTO.class,
        columns = {
            @ColumnResult(name = "passageId", type = String.class),
            @ColumnResult(name = "workspaceId", type = String.class),
            @ColumnResult(name = "majorHeading", type = String.class),
            @ColumnResult(name = "midHeading", type = String.class),
            @ColumnResult(name = "minorHeading", type = String.class),
            @ColumnResult(name = "segment", type = String.class),
            @ColumnResult(name = "majorRevision", type = String.class),
            @ColumnResult(name = "midRevision", type = String.class),
            @ColumnResult(name = "minorRevision", type = String.class),
            @ColumnResult(name = "link", type = String.class),
            @ColumnResult(name = "passage", type = String.class),
            @ColumnResult(name = "question", type = String.class),
            @ColumnResult(name = "questionId", type = String.class),
            @ColumnResult(name = "isAnchor", type = Boolean.class),
            @ColumnResult(name = "answer", type = String.class),
            @ColumnResult(name = "answerId", type = String.class),
        })
)
@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "MAI_MRC_QUESTION")
public class QuestionEntity implements Serializable {

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid")
  @Column(name = "ID", length = 40)
  private String id;

  @Column(name = "WORKSPACE_ID", length = 40, nullable = false)
  private String workspaceId;

  @Column(name = "PASSAGE_ID", length = 40)
  private String passageId;

  @Column(name = "QUESTION", length = 500)
  private String question;

  // 패시지의 대표가 되는 질문인지 여부. 오직 한 개만 true(1)를 가질 수 있다.
  @Column(name = "IS_ANCHOR", nullable = false)
  private Boolean isAnchor = false;

  @Column(name = "CREATOR_ID", length = 40)
  private String creatorId;

  @CreatedDate
  @Column(name = "CREATED_AT")
  private Date createdAt;

  @Column(name = "UPDATER_ID", length = 40)
  private String updaterId;

  @LastModifiedDate
  @Column(name = "UPDATED_AT")
  private Date updatedAt;

  @OneToOne(mappedBy = "questionEntity", cascade = CascadeType.REMOVE, orphanRemoval = true, fetch = FetchType.EAGER)
  private AnswerEntity answerEntity;

  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "WORKSPACE_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private WorkspaceEntity workspaceEntity;

  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "PASSAGE_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private PassageEntity passageEntity;

  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "CREATOR_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity createUserEntity;

  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "UPDATER_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity updateUserEntity;
}
