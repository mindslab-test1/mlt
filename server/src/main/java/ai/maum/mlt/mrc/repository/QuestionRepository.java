package ai.maum.mlt.mrc.repository;

import ai.maum.mlt.mrc.dto.QuestionAnswerDTO;
import ai.maum.mlt.mrc.entity.QuestionEntity;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface QuestionRepository extends JpaRepository<QuestionEntity, String> {

  List<QuestionEntity> findAllByPassageId(String passageId);

  List<QuestionEntity> findAllByPassageIdAndWorkspaceIdAndQuestion(String passageId,
      String workspaceId, String question);

  @Query("SELECT new ai.maum.mlt.mrc.dto.QuestionAnswerDTO(p, q)\n"
      + "FROM QuestionEntity q\n"
      + "  INNER JOIN q.passageEntity p\n"
      + "  INNER JOIN q.answerEntity a\n"
      + "WHERE q.workspaceId = :workspaceId\n"
      + "  AND (:searchQA IS NULL OR q.question LIKE CONCAT('%', :searchQA, '%')\n"
      + "    OR a.answer LIKE CONCAT('%', :searchQA, '%'))\n"
      + "  AND (:majorHeading IS NULL OR p.majorHeading LIKE CONCAT('%', :majorHeading, '%'))\n"
      + "  AND (:searchHeading IS NULL OR p.midHeading LIKE CONCAT('%', :searchHeading, '%')\n"
      + "    OR p.minorHeading LIKE CONCAT('%', :searchHeading, '%'))\n"
      + "  AND (:doValidation <> 'true'\n"
      + "    OR (p.passage NOT LIKE CONCAT('%', a.answer,'%')\n"
      + "      AND p.minorHeading NOT LIKE CONCAT('%', a.answer,'%')))\n"
      + "ORDER BY p.majorHeading ASC, p.midHeading ASC, p.minorHeading ASC, p.segment ASC NULLS FIRST, a.id ASC")
  Page<QuestionAnswerDTO> findAll(Pageable pageable,
      @Param("workspaceId") String workspaceId,
      @Param("majorHeading") String majorHeading,
      @Param("searchHeading") String searchHeading,
      @Param("searchQA") String searchQA,
      @Param("doValidation") String doValidation);

  void deleteByPassageId(String passageId);

  int countByWorkspaceId(String workspaceId);
}
