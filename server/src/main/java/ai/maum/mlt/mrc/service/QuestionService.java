package ai.maum.mlt.mrc.service;

import ai.maum.mlt.mrc.dto.QuestionAnswerDTO;
import ai.maum.mlt.mrc.entity.QuestionEntity;
import java.util.List;
import org.springframework.data.domain.Page;

public interface QuestionService {

  List<QuestionEntity> selectQuestionListByPassageId(String passageId);

  Page<QuestionAnswerDTO> selectQuestionAnswerList(QuestionAnswerDTO questionAnswerParam)
      throws Exception;

  QuestionEntity insertQuestion(QuestionEntity questionEntity);

  QuestionEntity updateQuestion(QuestionEntity questionEntity);

  void deleteQuestion(QuestionEntity questionEntity);

  void deleteQuestion(List<QuestionEntity> questionEntities);
}
