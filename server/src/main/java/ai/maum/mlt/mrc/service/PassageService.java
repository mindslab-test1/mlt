package ai.maum.mlt.mrc.service;

import ai.maum.mlt.mrc.entity.PassageEntity;
import java.util.List;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

public interface PassageService {

  Page<PassageEntity> selectPassageList(PassageEntity passageEntity) throws Exception;

  PassageEntity selectPassageById(String id) throws Exception;

  String getSummary(String workspaceId) throws Exception;

  PassageEntity insertPassage(PassageEntity passageEntity) throws Exception;

  PassageEntity updatePassage(PassageEntity passageEntity) throws Exception;

  void deletePassageList(List<PassageEntity> passageEntities) throws Exception;

  String uploadFiles(List<MultipartFile> files, String creatorId, String workspaceId)
      throws Exception;

  String mergeFiles(List<MultipartFile> files, String creatorId, String workspaceId)
      throws Exception;

  InputStreamResource downloadPassageSet(String workspaceId) throws Exception;

  InputStreamResource downloadLearningSet(String workspaceId) throws Exception;

  InputStreamResource downloadPQAXlsx(String workspaceId) throws Exception;
}
