package ai.maum.mlt.mrc.repository;

import ai.maum.mlt.mrc.dto.QuestionAnswerDTO;
import ai.maum.mlt.mrc.entity.PassageEntity;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
@Transactional
public interface PassageRepository extends JpaRepository<PassageEntity, String> {

  @Query("SELECT p\n"
      + "FROM PassageEntity p\n"
      + "WHERE p.workspaceId = :workspaceId\n"
      + "  AND (:majorHeading IS NULL OR p.majorHeading LIKE CONCAT('%', :majorHeading, '%'))\n"
      + "  AND (:searchHeading IS NULL OR p.midHeading LIKE CONCAT('%', :searchHeading, '%')\n"
      + "    OR p.minorHeading LIKE CONCAT('%', :searchHeading, '%'))\n"
      + "  AND (:passage IS NULL OR p.passage LIKE CONCAT('%', :passage, '%'))"
      + "ORDER BY p.majorHeading ASC\n"
      + "  , p.midHeading ASC\n"
      + "  , p.minorHeading ASC\n"
      + "  , p.segment ASC NULLS FIRST")
  Page<PassageEntity> findAllJpa(Pageable pageable,
      @Param("workspaceId") String workspaceId,
      @Param("majorHeading") String majorHeading,
      @Param("searchHeading") String searchHeading,
      @Param("passage") String passage);

  @Query(value = "SELECT *\n"
      + "FROM MAI_MRC_PASSAGE p\n"
      + "WHERE p.WORKSPACE_ID = :workspaceId\n"
      + "  AND (:majorHeading IS NULL OR p.MAJOR_HEADING LIKE CONCAT('%', :majorHeading, '%'))\n"
      + "  AND (:searchHeading IS NULL OR p.MID_HEADING LIKE CONCAT('%', :searchHeading, '%')\n"
      + "    OR p.MINOR_HEADING LIKE CONCAT('%', :searchHeading, '%'))\n"
      + "  AND (:passage IS NULL OR p.PASSAGE LIKE CONCAT('%', :passage, '%'))"
      + "\n-- #pageable\n"
      + "ORDER BY\n"
      + "    regexp_substr(p.MAJOR_HEADING, '^[a-zA-Z가-힣]*') ASC\n"
      + "  , CAST(regexp_substr(p.MAJOR_HEADING, '[0-9]+') AS UNSIGNED) ASC\n"
      + "  , CAST(RTRIM(regexp_substr(p.MID_HEADING, '[0-9]+ ')) AS UNSIGNED) ASC\n"
      + "  , CAST(RTRIM(regexp_substr(p.MINOR_HEADING, '[0-9]+ ')) AS UNSIGNED) ASC\n"
      + "  , p.SEGMENT IS NULL DESC, p.SEGMENT ASC",
      countQuery = "SELECT count(*)\n"
          + "FROM MAI_MRC_PASSAGE p\n"
          + "WHERE p.WORKSPACE_ID = :workspaceId\n"
          + "  AND (:majorHeading IS NULL OR p.MAJOR_HEADING LIKE CONCAT('%', :majorHeading, '%'))\n"
          + "  AND (:searchHeading IS NULL OR p.MID_HEADING LIKE CONCAT('%', :searchHeading, '%')\n"
          + "    OR p.MINOR_HEADING LIKE CONCAT('%', :searchHeading, '%'))\n"
          + "  AND (:passage IS NULL OR p.PASSAGE LIKE CONCAT('%', :passage, '%'))",
      nativeQuery = true)
  Page<PassageEntity> findAllMySql(Pageable pageable,
      @Param("workspaceId") String workspaceId,
      @Param("majorHeading") String majorHeading,
      @Param("searchHeading") String searchHeading,
      @Param("passage") String passage);

  @Query(value = "SELECT *\n"
      + "FROM MAI_MRC_PASSAGE p\n"
      + "WHERE p.WORKSPACE_ID = :workspaceId\n"
      + "  AND (:majorHeading IS NULL OR p.MAJOR_HEADING LIKE '%' || :majorHeading || '%')\n"
      + "  AND (:searchHeading IS NULL OR p.MID_HEADING LIKE '%' || :searchHeading || '%'\n"
      + "    OR p.MINOR_HEADING LIKE '%' || :searchHeading || '%')\n"
      + "  AND (:passage IS NULL OR p.PASSAGE LIKE '%' || :passage || '%')"
      + "\n-- #pageable\n"
      + "ORDER BY\n"
      + "    regexp_substr(MAJOR_HEADING, '^[a-zA-Z가-힣]*') ASC\n"
      + "  , TO_NUMBER(regexp_substr(MAJOR_HEADING, '[0-9]+')) ASC\n"
      + "  , TO_NUMBER(regexp_substr(p.MID_HEADING, '[0-9]+ ')) ASC\n"
      + "  , TO_NUMBER(regexp_substr(p.MINOR_HEADING, '[0-9]+ ')) ASC\n"
      + "  , p.SEGMENT ASC NULLS FIRST",
      countQuery = "SELECT count(*)\n"
          + "FROM MAI_MRC_PASSAGE p\n"
          + "WHERE p.WORKSPACE_ID = :workspaceId\n"
          + "  AND (:majorHeading IS NULL OR p.MAJOR_HEADING LIKE '%' || :majorHeading || '%')\n"
          + "  AND (:searchHeading IS NULL OR p.MID_HEADING LIKE '%' || :searchHeading || '%'\n"
          + "    OR p.MINOR_HEADING LIKE '%' || :searchHeading || '%')\n"
          + "  AND (:passage IS NULL OR p.PASSAGE LIKE '%' || :passage || '%')",
      nativeQuery = true)
  Page<PassageEntity> findAllOracle(Pageable pageable,
      @Param("workspaceId") String workspaceId,
      @Param("majorHeading") String majorHeading,
      @Param("searchHeading") String searchHeading,
      @Param("passage") String passage);


  @Query("SELECT new ai.maum.mlt.mrc.dto.QuestionAnswerDTO(p, q)\n"
      + "FROM PassageEntity p\n"
      + "  LEFT JOIN p.questionEntities q\n"
      + "  LEFT JOIN q.answerEntity a\n"
      + "WHERE p.workspaceId = :workspaceId\n"
      + "ORDER BY p.majorHeading ASC, p.midHeading ASC, p.minorHeading ASC, p.segment ASC NULLS FIRST\n"
      + " , q.id ASC, a.id ASC")
  List<QuestionAnswerDTO> findAllPQA(@Param("workspaceId") String workspaceId);


  PassageEntity findByWorkspaceIdAndMajorHeadingAndMidHeadingAndMinorHeadingAndSegment(
      String workspaceId,
      String majorHeading,
      String midHeading,
      String minorHeading,
      String segment);

  int countByWorkspaceId(String workspaceId);
}
