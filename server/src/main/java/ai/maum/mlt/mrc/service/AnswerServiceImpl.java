package ai.maum.mlt.mrc.service;

import ai.maum.mlt.mrc.entity.AnswerEntity;
import ai.maum.mlt.mrc.repository.AnswerRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AnswerServiceImpl implements AnswerService {

  @Autowired
  private AnswerRepository answerRepository;

  @Override
  public List<AnswerEntity> selectListByQuestionId(String questionId) {
    return answerRepository.findAllByQuestionId(questionId);
  }

  @Override
  public AnswerEntity insertAnswer(AnswerEntity answerEntity) {
    return answerRepository.save(answerEntity);
  }

  @Override
  public List<AnswerEntity> insertAnswerList(List<AnswerEntity> answerEntities) {
    return answerRepository.save(answerEntities);
  }

  @Override
  public AnswerEntity updateAnswer(AnswerEntity answerEntity) {
    return answerRepository.save(answerEntity);
  }

  @Override
  public void deleteAnswer(List<AnswerEntity> answerEntities) {
    answerRepository.delete(answerEntities);
  }
}
