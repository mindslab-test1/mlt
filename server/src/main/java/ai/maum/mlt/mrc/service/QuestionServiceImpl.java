package ai.maum.mlt.mrc.service;

import ai.maum.mlt.mrc.dto.QuestionAnswerDTO;
import ai.maum.mlt.mrc.entity.QuestionEntity;
import ai.maum.mlt.mrc.repository.AnswerRepository;
import ai.maum.mlt.mrc.repository.QuestionRepository;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class QuestionServiceImpl implements QuestionService {

  static final Logger logger = LoggerFactory.getLogger(QuestionServiceImpl.class);

  private static final String mySqlAllQAQuery = "SELECT\n"
      + " p.ID as passageId, p.WORKSPACE_ID as workspaceId,\n"
      + " p.MAJOR_HEADING AS majorHeading, p.MID_HEADING as midHeading, p.MINOR_HEADING as minorHeading,\n"
      + " p.SEGMENT as segment,\n"
      + " p.MAJOR_REVISION AS majorRevision, p.MID_REVISION as midRevision, p.MINOR_REVISION as minorRevision,\n"
      + " p.LINK as link, p.PASSAGE as passage, q.QUESTION AS question, q.ID as questionId, q.IS_ANCHOR as isAnchor,\n"
      + " a.ANSWER as answer, a.ID as answerId\n"
      + "FROM MAI_MRC_QUESTION q\n"
      + "  JOIN MAI_MRC_PASSAGE p ON q.PASSAGE_ID = p.ID\n"
      + "  JOIN MAI_MRC_ANSWER a ON a.QUESTION_ID = q.ID\n"
      + "WHERE q.WORKSPACE_ID = :workspaceId\n"
      + "  AND (:searchQA IS NULL OR q.QUESTION LIKE CONCAT('%', :searchQA, '%')\n"
      + "    OR a.ANSWER LIKE CONCAT('%', :searchQA, '%'))\n"
      + "  AND (:majorHeading IS NULL OR p.MAJOR_HEADING LIKE CONCAT('%', :majorHeading, '%'))\n"
      + "  AND (:searchHeading IS NULL OR p.MID_HEADING LIKE CONCAT('%', :searchHeading, '%')\n"
      + "    OR p.MINOR_HEADING LIKE CONCAT('%', :searchHeading, '%'))\n"
      + "  AND (:doValidation <> 'true'\n"
      + "    OR (p.PASSAGE NOT LIKE CONCAT('%', a.ANSWER,'%')\n"
      + "      AND p.MINOR_HEADING NOT LIKE CONCAT('%', a.ANSWER,'%')))\n"
      + "ORDER BY\n"
      + "    regexp_substr(p.MAJOR_HEADING, '^[a-zA-Z가-힣]*') ASC\n"
      + "  , CAST(regexp_substr(p.MAJOR_HEADING, '[0-9]+') AS UNSIGNED) ASC\n"
      + "  , CAST(RTRIM(regexp_substr(p.MID_HEADING, '[0-9]+ ')) AS UNSIGNED) ASC\n"
      + "  , CAST(RTRIM(regexp_substr(p.MINOR_HEADING, '[0-9]+ ')) AS UNSIGNED) ASC\n"
      + "  , p.SEGMENT IS NULL DESC, p.SEGMENT ASC\n"
      + "  , q.ID ASC, a.ID ASC";

  private static final String oracleAllQAQuery = "SELECT\n"
      + " p.ID as passageId, p.WORKSPACE_ID as workspaceId,\n"
      + " p.MAJOR_HEADING AS majorHeading, p.MID_HEADING as midHeading, p.MINOR_HEADING as minorHeading,\n"
      + " p.SEGMENT as segment,\n"
      + " p.MAJOR_REVISION AS majorRevision, p.MID_REVISION as midRevision, p.MINOR_REVISION as minorRevision,\n"
      + " p.LINK as link, p.PASSAGE as passage, q.QUESTION AS question, q.ID as questionId, q.IS_ANCHOR as isAnchor,\n"
      + " a.ANSWER as answer, a.ID as answerId\n"
      + "FROM MAI_MRC_QUESTION q\n"
      + "  INNER JOIN MAI_MRC_PASSAGE p ON q.PASSAGE_ID = p.ID\n"
      + "  INNER JOIN MAI_MRC_ANSWER a ON a.QUESTION_ID = q.ID\n"
      + "WHERE q.WORKSPACE_ID = :workspaceId\n"
      + "  AND (:searchQA IS NULL OR q.QUESTION LIKE '%' || :searchQA || '%'\n"
      + "    OR a.ANSWER LIKE '%' || :searchQA || '%')\n"
      + "  AND (:majorHeading IS NULL OR p.MAJOR_HEADING LIKE '%' || :majorHeading || '%')\n"
      + "  AND (:searchHeading IS NULL OR p.MID_HEADING LIKE '%' || :searchHeading || '%'\n"
      + "    OR p.MINOR_HEADING LIKE '%' || :searchHeading || '%')\n"
      + "  AND (:doValidation <> 'true'\n"
      + "    OR (p.PASSAGE NOT LIKE '%' || a.ANSWER || '%'\n"
      + "      AND p.MINOR_HEADING NOT LIKE '%' || a.ANSWER || '%'))\n"
      + "ORDER BY\n"
      + "    regexp_substr(MAJOR_HEADING, '^[a-zA-Z가-힣]*') ASC\n"
      + "  , TO_NUMBER(regexp_substr(MAJOR_HEADING, '[0-9]+')) ASC\n"
      + "  , TO_NUMBER(regexp_substr(p.MID_HEADING, '[0-9]+ ')) ASC\n"
      + "  , TO_NUMBER(regexp_substr(p.MINOR_HEADING, '[0-9]+ ')) ASC\n"
      + "  , p.SEGMENT ASC NULLS FIRST\n"
      + "  , q.ID ASC, a.ID ASC";

  @Autowired
  QuestionRepository questionRepository;

  @Autowired
  AnswerRepository answerRepository;

  @PersistenceContext
  private EntityManager em;

  @Value("${spring.jpa.database}")
  private String dbKind;

  @Override
  public List<QuestionEntity> selectQuestionListByPassageId(String passageId) {
    return questionRepository.findAllByPassageId(passageId);
  }

  @Override
  public Page<QuestionAnswerDTO>
  selectQuestionAnswerList(QuestionAnswerDTO questionAnswerParam) throws Exception {
    List<QuestionAnswerDTO> list;

    questionAnswerParam.escapeSearchWords();
    questionAnswerParam.setOrderDirection(null);

    switch (dbKind.toLowerCase()) {
      case "mysql":
      case "mariadb": {
        Query query = em.createNativeQuery(mySqlAllQAQuery, "QuestionAnswerDTOMapping")
            .setParameter("workspaceId", questionAnswerParam.getWorkspaceId())
            .setParameter("majorHeading", questionAnswerParam.getMajorHeading())
            .setParameter("searchHeading", questionAnswerParam.getSearchHeading())
            .setParameter("searchQA", questionAnswerParam.getSearchQA())
            .setParameter("doValidation",
                String.valueOf(questionAnswerParam.getDoValidation().booleanValue()));
        list = query.getResultList();
        break;
      }
      case "oracle": {
        Query query = em.createNativeQuery(oracleAllQAQuery, "QuestionAnswerDTOMapping")
            .setParameter("workspaceId", questionAnswerParam.getWorkspaceId())
            .setParameter("majorHeading", questionAnswerParam.getMajorHeading())
            .setParameter("searchHeading", questionAnswerParam.getSearchHeading())
            .setParameter("searchQA", questionAnswerParam.getSearchQA())
            .setParameter("doValidation",
                String.valueOf(questionAnswerParam.getDoValidation().booleanValue()));
        list = query.getResultList();
        break;
      }
      default:
        return questionRepository.findAll(
            questionAnswerParam.getPageRequest(),
            questionAnswerParam.getWorkspaceId(),
            questionAnswerParam.getMajorHeading(),
            questionAnswerParam.getSearchHeading(),
            questionAnswerParam.getSearchQA(),
            String.valueOf(questionAnswerParam.getDoValidation().booleanValue()));
    }

    PagedListHolder page = new PagedListHolder(list);
    page.setPage(questionAnswerParam.getPageIndex());
    page.setPageSize(questionAnswerParam.getPageSize());
    page.getPageCount();

    return new PageImpl<QuestionAnswerDTO>(page.getPageList(),
        new PageRequest(page.getPage(), page.getPageSize()),
        list.size());
  }

  @Override
  public QuestionEntity insertQuestion(QuestionEntity questionEntity) {
    questionEntity.setCreatedAt(new Date());
    return questionRepository.save(questionEntity);
  }

  @Override
  public QuestionEntity updateQuestion(QuestionEntity questionEntity) {
    return questionRepository.saveAndFlush(questionEntity);
  }

  @Override
  public void deleteQuestion(QuestionEntity questionEntity) {
    questionRepository.delete(questionEntity.getId());
  }

  @Override
  public void deleteQuestion(List<QuestionEntity> questionEntities) {
    questionRepository.delete(questionEntities);
  }
}
