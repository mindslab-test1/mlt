package ai.maum.mlt.mrc.entity;

import ai.maum.mlt.common.auth.entity.UserEntity;
import ai.maum.mlt.common.auth.entity.WorkspaceEntity;
import ai.maum.mlt.common.pagenation.PageParameters;
import ai.maum.mlt.common.util.StringUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "MAI_MRC_PASSAGE",
    uniqueConstraints = {
        @UniqueConstraint(
            columnNames = {"MAJOR_HEADING", "MID_HEADING", "MINOR_HEADING", "SEGMENT"}
        )
    }
)
public class PassageEntity extends PageParameters implements Serializable {

  static final Logger logger = LoggerFactory.getLogger(PassageEntity.class);

  @Transient
  String headings;

  @Transient
  String questionCnt;

  @Transient
  String searchHeading;

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid")
  @Column(name = "ID", length = 40)
  private String id;

  @Column(name = "WORKSPACE_ID", length = 40, nullable = false)
  private String workspaceId;

  @Column(name = "MAJOR_HEADING", length = 200)
  private String majorHeading;

  @JsonIgnore
  @Column(name = "MID_HEADING", length = 200)
  private String midHeading;

  @Column(name = "MINOR_HEADING", length = 200)
  private String minorHeading;

  @JsonIgnore
  @Column(name = "SEGMENT", length = 10)
  private String segment;

  @JsonIgnore
  @Column(name = "MAJOR_REVISION", length = 100)
  private String majorRevision;

  @JsonIgnore
  @Column(name = "MID_REVISION", length = 200)
  private String midRevision;

  @JsonIgnore
  @Column(name = "MINOR_REVISION", length = 300)
  private String minorRevision;

  @Column(name = "LINK")
  private String link;

  @Lob
  @Column(name = "PASSAGE")
  private String passage;

  @Column(name = "CREATOR_ID", length = 40)
  private String creatorId;

  @CreatedDate
  @Column(name = "CREATED_AT")
  private Date createdAt;

  @Column(name = "UPDATER_ID", length = 40)
  private String updaterId;

  @LastModifiedDate
  @Column(name = "UPDATED_AT")
  private Date updatedAt;

  @JsonIgnore
  @OneToOne(mappedBy = "passageEntity", cascade = CascadeType.REMOVE, orphanRemoval = true, fetch = FetchType.EAGER)
  private ContextEntity contextEntity;

  @OneToMany(mappedBy = "passageId", cascade = CascadeType.REMOVE, orphanRemoval = true, fetch = FetchType.EAGER)
  private List<QuestionEntity> questionEntities;

  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "WORKSPACE_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private WorkspaceEntity workspaceEntity;

  @ManyToOne
  @JoinColumn(name = "CREATOR_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity createUserEntity;

  @ManyToOne
  @JoinColumn(name = "UPDATER_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity updateUserEntity;

  public boolean contentEquals(PassageEntity passageEntity) {
    return StringUtils.equals(segment, passageEntity.getSegment())
        && StringUtils.equals(minorHeading, passageEntity.getMinorHeading())
        && StringUtils.equals(minorRevision, passageEntity.getMinorRevision())
        && StringUtils.equals(midHeading, passageEntity.getMidHeading())
        && StringUtils.equals(midRevision, passageEntity.getMidRevision())
        && StringUtils.equals(majorHeading, passageEntity.getMajorHeading())
        && StringUtils.equals(majorRevision, passageEntity.getMajorRevision())
        && StringUtils.equals(link, passageEntity.getLink())
        && StringUtils.equals(passage, passageEntity.getPassage());
  }

  public void escapeSearchWords() {
    majorHeading = StringUtils.escapeMs949(majorHeading);
    searchHeading = StringUtils.escapeMs949(searchHeading);
    passage = StringUtils.escapeMs949(passage);
  }
}
