package ai.maum.mlt.mrc.service;

import ai.maum.mlt.mrc.entity.ContextEntity;
import ai.maum.mlt.mrc.repository.ContextRepository;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ContextServiceImpl implements ContextService {

  @Autowired
  private ContextRepository contextRepository;

  @Override
  public Page<ContextEntity> selectContextList(ContextEntity contextEntity) {
    return contextRepository.findAll(contextEntity.getPageRequest(), contextEntity.getWorkspaceId(),
        contextEntity.getContext());
  }

  @Override
  public ContextEntity selectContextById(String id) {
    ContextEntity contextEntity = contextRepository.findById(id);
    return contextEntity;
  }

  @Override
  public ContextEntity insertContext(ContextEntity contextEntity) {
    contextEntity.setCreatedAt(new Date());
    ContextEntity insertedC = contextRepository.save(contextEntity);
//    // question Answer 위치 구하기
//    for (QuestionEntity q : contextEntity.getQuestionEntities()) {
//      q.setContextId(insertedC.getId());
//      List<AnswerEntity> aList = new ArrayList<>();
//      for (AnswerEntity a : q.getAnswerEntities()) {
//        HashMap<String, Integer> answerStartEnd =
//            stringUtils.getWordStartAndEndByMarker(contextEntity.getContext(), a.getAnswer());
//        a.setAnswerStart(answerStartEnd.get("answerStart"));
//        a.setAnswerEnd(answerStartEnd.get("answerEnd"));
//        aList.add(a);
//      }
//
//      q.setAnswerEntities(aList);
//    }
//    List<QuestionEntity> insertedQ = questionRepository.save(contextEntity.getQuestionEntities());
//    List<AnswerEntity> allAnswers = new ArrayList<>();
//    for (QuestionEntity q : insertedQ) {
//      List<AnswerEntity> aList = q.getAnswerEntities();
//      for (AnswerEntity a : aList) {
//        a.setQuestionId(q.getId());
//      }
//      allAnswers.addAll(aList);
//    }
//    answerRepository.save(allAnswers);
    return contextRepository.getOne(insertedC.getId());
  }

  @Override
  public ContextEntity updateContext(ContextEntity contextEntity) {
//    if (contextEntity.getDeletedQuestionEntities() != null) {
//      questionRepository.delete(contextEntity.getDeletedQuestionEntities());
//    }
//    for (QuestionEntity q : contextEntity.getQuestionEntities()) {
//      q.setContextId(contextEntity.getId());
//      List<AnswerEntity> aList = new ArrayList<>();
//      for (AnswerEntity a : q.getAnswerEntities()) {
//        HashMap<String, Integer> answerStartEnd =
//            stringUtils.getWordStartAndEndByMarker(contextEntity.getContext(), a.getAnswer());
//        a.setAnswerStart(answerStartEnd.get("answerStart"));
//        a.setAnswerEnd(answerStartEnd.get("answerEnd"));
//        aList.add(a);
//      }
//
//      q.setAnswerEntities(aList);
//    }
//    List<QuestionEntity> insertedQ = questionRepository.save(contextEntity.getQuestionEntities());
//    List<AnswerEntity> allAnswers = new ArrayList<>();
//    for (QuestionEntity q : insertedQ) {
//      List<AnswerEntity> aList = q.getAnswerEntities();
//      for (AnswerEntity a : aList) {
//        a.setQuestionId(q.getId());
//      }
//      allAnswers.addAll(aList);
//    }
//    answerRepository.save(allAnswers);
    ContextEntity updatedC = contextRepository.save(contextEntity);
    return contextRepository.getOne(updatedC.getId());
  }

  @Override
  @Transactional
  public void deleteContexts(List<ContextEntity> contextEntities) {
    contextRepository.delete(contextEntities);
  }
}
