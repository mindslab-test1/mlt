package ai.maum.mlt.mrc.repository;

import ai.maum.mlt.mrc.entity.AnswerEntity;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface AnswerRepository extends JpaRepository<AnswerEntity, String> {

  List<AnswerEntity> findAllByQuestionId(String questionId);

  void deleteByQuestionId(String questionId);

  long countByWorkspaceId(String workspaceId);
}
