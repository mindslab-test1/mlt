package ai.maum.mlt.management.service;

import ai.maum.mlt.management.entity.HistoryEntity;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface HistoryService {

  HistoryEntity insertHistory(HistoryEntity historyEntity);

  void updateHistory(HistoryEntity historyEntity);

  Page<HistoryEntity> findAllByWorkspaceId(String workspaceId, String code,
      Pageable pageable);

  List<HistoryEntity> findAllByWorkspaceIdOrderByUpdatedAtDesc(String workspaceId);
}
