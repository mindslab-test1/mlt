package ai.maum.mlt.management.repository;

import ai.maum.mlt.management.entity.HistoryEntity;
import java.util.Date;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface HistoryRepository extends JpaRepository<HistoryEntity, String> {

  @Query(value ="SELECT h "
      +         "FROM HistoryEntity h join h.workspaceEntity w "
      +         "WHERE h.workspaceId = :workspaceId "
      +         "AND (h.code LIKE CONCAT('%',:code, '%') OR :code IS NULL)"
  )
  Page<HistoryEntity> findAllByWorkspaceId(
      @Param("workspaceId") String workspaceId,
      @Param("code") String code,
      Pageable pageable
  );

  List<HistoryEntity> findAllByWorkspaceIdOrderByUpdatedAtDesc(String workspaceId);

  @Modifying
  @Query("UPDATE HistoryEntity h "
      + " SET h.endedAt= :endedAt, h.message= :message, h.data= :data, h.updatedAt= :updatedAt "
      + " WHERE h.id=:id")
  void updateHistory(@Param("endedAt") Date endedAt, @Param("message") String message,
      @Param("data") String data, @Param("updatedAt") Date updatedAt, @Param("id") String id);
}