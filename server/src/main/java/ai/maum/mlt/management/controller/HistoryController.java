package ai.maum.mlt.management.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.management.entity.HistoryEntity;
import ai.maum.mlt.management.service.HistoryService;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/management/histories")
public class HistoryController {

  static final Logger logger = LoggerFactory.getLogger(HistoryController.class);

  @Autowired
  private HistoryService historyService;

  @UriRoleDesc(role = "management^R")
  @RequestMapping(
      value = "/getAllHistories",
      method = RequestMethod.POST)
  public ResponseEntity<?> getAllHistories(@RequestBody HistoryEntity historyEntity) {
    logger.info("======= call api  [[/api/management/histories/getAllHistories]] =======");

    try {
      Page<HistoryEntity> historyEntities = this.historyService
          .findAllByWorkspaceId(historyEntity.getWorkspaceId(),
              historyEntity.getCode(), historyEntity.getPageRequest());
      return new ResponseEntity<>(historyEntities, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "management^R")
  @RequestMapping(
      value = "/getTotalHistories",
      method = RequestMethod.POST)
  public ResponseEntity<?> getTotalHistories(@RequestBody HistoryEntity historyEntity) {
    logger.info("======= call api  [[/api/management/histories/getTotalHistories]] =======");

    try {
      List historyEntities = this.historyService
          .findAllByWorkspaceIdOrderByUpdatedAtDesc(historyEntity.getWorkspaceId());
      return new ResponseEntity<>(historyEntities, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}
