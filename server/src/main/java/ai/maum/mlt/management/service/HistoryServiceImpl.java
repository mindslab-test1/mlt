package ai.maum.mlt.management.service;

import ai.maum.mlt.management.entity.HistoryEntity;
import ai.maum.mlt.management.repository.HistoryRepository;
import java.util.Date;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class HistoryServiceImpl implements HistoryService {

  @Autowired
  private HistoryRepository historyRepository;

  @Override
  public Page<HistoryEntity> findAllByWorkspaceId(String workspaceId,
      String code, Pageable pageable) {
    return this.historyRepository
        .findAllByWorkspaceId(workspaceId, code, pageable);
  }

  @Override
  public List<HistoryEntity> findAllByWorkspaceIdOrderByUpdatedAtDesc(String workspaceId) {
    return this.historyRepository.findAllByWorkspaceIdOrderByUpdatedAtDesc(workspaceId);
  }

  @Override
  public HistoryEntity insertHistory(HistoryEntity historyEntity) {
    historyEntity.setUpdatedAt(new Date());
    historyEntity.setStartedAt(new Date());
    historyEntity.setEndedAt(new Date());
    return this.historyRepository.save(historyEntity);
  }

  @Transactional
  @Override
  public void updateHistory(HistoryEntity historyEntity) {
    this.historyRepository.updateHistory(historyEntity.getEndedAt(), historyEntity.getMessage(),
        historyEntity.getData(), historyEntity.getUpdatedAt(), historyEntity.getId());
  }
}
