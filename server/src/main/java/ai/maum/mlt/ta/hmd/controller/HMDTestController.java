package ai.maum.mlt.ta.hmd.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.common.auth.entity.WorkspaceEntity;
import ai.maum.mlt.common.auth.service.WorkspaceService;
import ai.maum.mlt.itfc.TaGrpcInterfaceManager;
import ai.maum.mlt.ta.hmd.entity.HMDDicEntity;
import ai.maum.mlt.ta.hmd.entity.HMDResultEntity;
import ai.maum.mlt.ta.hmd.service.HMDDicService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import maum.brain.hmd.Hmd.HmdClassified;
import maum.brain.hmd.Hmd.HmdInputText;
import maum.brain.hmd.Hmd.HmdModel;
import maum.brain.hmd.Hmd.HmdResultDocument;
import maum.brain.nlp.Nlp.Sentence;
import maum.common.LangOuterClass.LangCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/ta/hmd/test")
public class HMDTestController {

  static final Logger logger = LoggerFactory.getLogger(HMDTestController.class);


  @Value("${hmd.train.server.ip}")
  private String ip;
  @Value("${hmd.train.server.port}")
  private int port;

  @Autowired
  private HMDDicService hmdDicService;

  @Autowired
  private WorkspaceService workspaceService;

  @UriRoleDesc(role = "ta/hmd^R")
  @RequestMapping(
      value = "/getHMDDicList",
      method = RequestMethod.POST)
  public ResponseEntity<?> getHMDDicList(@RequestBody HMDDicEntity hmdDicEntity) {

    logger.info("======= call api  [[/api/ta/hmd/getHMDDicList]] =======");
    try {

      HashMap<String, Object> result = new HashMap<>();
      result.put("hmdDicList", hmdDicService.getHMDDicList(hmdDicEntity));

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }


  /*
  * TA > HMD > Analysis > Test 페이지 관련 라우팅
  */
  @UriRoleDesc(role = "ta/hmd^R")
  @RequestMapping(
      value = "/getHMDAnalyzeTestResultList",
      method = RequestMethod.POST)
  public ResponseEntity<?> getHMDAnalyzeTestResultList(@RequestBody HMDDicEntity hmdDicEntity) {

    logger.info("======= call api  [[/api/ta/hmd/getHMDAnalyzeTestResultList]] =======");
    try {
      String[] lines = hmdDicEntity.getTextData().split("\n");
      final List<String> lineList = new ArrayList<>();
      Collections.addAll(lineList, lines);
      String text = "";
      for (String line : lineList) {
        text += line.trim().replaceAll("([^.])$", "$1.") + "\n";
        if (line.equals("")) {
          lineList.remove(line);
        }
      }

      TaGrpcInterfaceManager client = new TaGrpcInterfaceManager(ip, port);

      //1.0 버전에서 이걸 왜 했나 의문..
      HmdModel.Builder model = HmdModel.newBuilder();

      HmdInputText.Builder hmdInputText = HmdInputText.newBuilder();
      hmdInputText.setModel(hmdDicEntity.getName().replace("_hmd", ""));
      WorkspaceEntity workspaceEntity = workspaceService
          .getWorkspace(hmdDicEntity.getWorkspaceId());
      // Lang은 일단 항상 한국어로만 사용하도록 코딩
      hmdInputText.setLang(LangCode.kor);

      //개행 제거
      hmdInputText.setText(text);
      HmdResultDocument hmdResult = client.getClassByText(hmdInputText.build());
      HashMap<String, Object> result = new HashMap<>();
      List<HMDResultEntity> resultData = new ArrayList<>();
      List<HmdClassified> classifiedList = hmdResult.getClsList();
      List<Sentence> sentenceList = hmdResult.getDocument().getSentencesList();
      int failCount = 0;
      int successCount = 0;

      // Test 시 Success 결과
      for (int i = 0; i < classifiedList.size(); i++) {
        HMDResultEntity tempEntity = new HMDResultEntity();
        tempEntity.setSentence(classifiedList.get(i).getSentence());
        tempEntity.setId(classifiedList.get(i).getSentSeq() + "");

        String category = hmdResult.getCls(i).getCategory();
        String rule = hmdResult.getCls(i).getPattern();
        tempEntity.setCategory(category);
        tempEntity.setRule(rule);

        resultData.add(tempEntity);
      }

      // Test 시 Fail 결과
      for(Sentence sentence : sentenceList) {
        HMDResultEntity tempEntity = new HMDResultEntity();
        boolean resultFlag = false;

        for (int i = 0; i < classifiedList.size(); i++) {
          if (sentence.getText().equals(hmdResult.getCls(i).getSentence())) {
            resultFlag = true;
            break;
          }
        }

        if(!resultFlag) {
          tempEntity.setId("99999");
          tempEntity.setSentence(sentence.getText());
          tempEntity.setCategory("Category not found.");
          tempEntity.setRule("Not matched rule.");
          resultData.add(tempEntity);
          failCount++;
        } else {
          successCount++;
        }
      }

      result.put("fail", failCount);
      result.put("success", successCount);
      result.put("totalElement", classifiedList.size() + failCount);
      result.put("result", resultData);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
