package ai.maum.mlt.ta.dnn.service;

import ai.maum.mlt.ta.dnn.entity.DNNDicLineEntity;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface DNNDicLineService {

  List<DNNDicLineEntity> getAllDicLinesByVersionId(String versionId);

  List<DNNDicLineEntity> getAllDicLines(String id, String category);

  Page<DNNDicLineEntity> getAllDicLines(String id, String category, String sentence,
      Pageable pageable);

  List<DNNDicLineEntity> getDuplicatesLines(String versionId, String category);

  List<String> getAllDistinct(String versionId);

  DNNDicLineEntity insertLine(DNNDicLineEntity dnnDicLineEntity);

  boolean deleteDuplicatesLines(String versionId, String category);

  boolean deleteLine(DNNDicLineEntity dnnDicLineEntity);

  void deleteByVersionId(String versionId);

  void updateDicLineCategory(String category, String oldName);
}
