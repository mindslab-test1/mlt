package ai.maum.mlt.ta.common.entity;

import ai.maum.mlt.common.auth.entity.UserEntity;
import ai.maum.mlt.common.auth.entity.WorkspaceEntity;
import ai.maum.mlt.common.pagenation.PageParameters;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.LastModifiedDate;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Getter
@Setter
@Table(name = "MAI_TA_MODEL_MANAGEMENT")
public class ModelManagementEntity extends PageParameters implements Serializable {


  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid")
  @Column(name = "ID", length = 40, nullable = false)
  private String id;

  @Column(name = "WORKSPACE_ID", length = 40, nullable = false)
  protected String workspaceId;

  @Column(name = "NAME", length = 512, nullable = false)
  private String name;

  @Column(name = "MAN_DESC", length = 512)
  private String manDesc;

  @Column(name = "HMD_USE", nullable = false)
  private boolean hmdUse;

  @Column(name = "DNN_USE", nullable = false)
  private boolean dnnUse;

  @Column(name = "XDC_USE", nullable = false)
  private boolean xdcUse;

  @Column(name = "EXTERNAL_MODEL_SEQ", length = 512)
  protected String externalModelSeq;

  @Column(name = "HMD_DIC_ID", length = 40)
  private String hmdDicId;

  @Column(name = "DNN_DIC_ID", length = 40)
  private String dnnDicId;

  @Column(name = "XDC_DIC_ID", length = 40)
  private String xdcDicId;

  @Column(name = "GRPC_SERVER_SET_ID", length = 40)
  private String grpcServerSetId;

  @Column(name = "CREATOR_ID", length = 40)
  private String creatorId;

  @CreatedDate
  @Column(name = "CREATED_AT")
  private Date createdAt;

  @Column(name = "UPDATER_ID", length = 40)
  private String updaterId;

  @LastModifiedDate
  @Column(name = "UPDATED_AT")
  private Date updatedAt;

  @ManyToOne
  @JoinColumn(name = "CREATOR_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity createUserEntity;

  @ManyToOne
  @JoinColumn(name = "UPDATER_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity updateUserEntity;

  @ManyToOne
  @JoinColumn(name = "WORKSPACE_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private WorkspaceEntity workspaceEntity;


}
