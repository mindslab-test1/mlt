package ai.maum.mlt.ta.dnn.service;

import ai.maum.mlt.ta.dnn.entity.DNNDicEntity;
import ai.maum.mlt.ta.dnn.repository.DNNDicRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
public class DNNDicServiceImpl implements DNNDicService {

  @Autowired
  private DNNDicRepository dnnDicRepository;

  @Override
  public List<DNNDicEntity> getDnnDicAllList(String workspaceId) {
    return this.dnnDicRepository.getAllByWorkspaceIdOrderByNameAsc(workspaceId);
  }

  @Override
  public Page<DNNDicEntity> getDnnDicAllList(DNNDicEntity dnnDicEntity) {
    return dnnDicRepository
        .getAllByWorkspaceId(dnnDicEntity.getWorkspaceId(), dnnDicEntity.getPageRequest());
  }

  @Override
  public DNNDicEntity getDnnDic(String workspace, String name) {
    return dnnDicRepository.getDicByWorkspaceIdAndName(workspace, name);
  }
}
