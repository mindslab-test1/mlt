package ai.maum.mlt.ta.common.repository;

import ai.maum.mlt.ta.hmd.entity.HMDDicEntity;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface HmdR extends JpaRepository<HMDDicEntity, String> {
  void deleteById(String id);
}
