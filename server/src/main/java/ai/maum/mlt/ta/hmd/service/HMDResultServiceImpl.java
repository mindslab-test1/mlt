package ai.maum.mlt.ta.hmd.service;

import ai.maum.mlt.ta.hmd.entity.HMDResultEntity;
import ai.maum.mlt.ta.hmd.repository.HMDResultRepository;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
public class HMDResultServiceImpl implements HMDResultService {

  @Autowired
  private HMDResultRepository hmdResultRepository;

  @Override
  public HMDResultEntity insertHMDResult(HMDResultEntity hmdResultEntity) {
    hmdResultEntity.setCreatedAt(new Date());
    hmdResultEntity.setUpdatedAt(new Date());
    return hmdResultRepository.save(hmdResultEntity);
  }

  @Override
  public Page<HMDResultEntity> getHMDResultList(HMDResultEntity hmdResultEntity) {
    return hmdResultRepository
        .findByWorkspaceId(hmdResultEntity.getWorkspaceId(), hmdResultEntity.getPageRequest());
  }

  @Override
  public Page<HMDResultEntity> findByHmdResultEntity(HMDResultEntity hmdResultEntity) {
    return hmdResultRepository.findByHmdResultEntity(
        hmdResultEntity.getWorkspaceId(),
        hmdResultEntity.getModel(),
        hmdResultEntity.getSentence(),
        hmdResultEntity.getCategory(),
        hmdResultEntity.getRule(),
        hmdResultEntity.getPageRequest()
    );
  }

  public List<HMDResultEntity> findAllByHmdResultEntity(HMDResultEntity hmdResultEntity) {
    return hmdResultRepository.findAllByHmdResultEntity(
        hmdResultEntity.getWorkspaceId(),
        hmdResultEntity.getModel(),
        hmdResultEntity.getSentence(),
        hmdResultEntity.getCategory(),
        hmdResultEntity.getRule()
    );
  }


  @Override
  public Integer deleteByHmdResultEntity(HMDResultEntity hmdResultEntity) {
    List<HMDResultEntity> hmdResultEntities = findAllByHmdResultEntity(hmdResultEntity);
    hmdResultRepository.deleteInBatch(hmdResultEntities);

    return hmdResultEntities.size();
  }


}
