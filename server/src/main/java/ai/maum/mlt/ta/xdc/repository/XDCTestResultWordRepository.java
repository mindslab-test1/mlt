package ai.maum.mlt.ta.xdc.repository;

import ai.maum.mlt.ta.xdc.entity.XdcTestResultWordEntity;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface XDCTestResultWordRepository extends JpaRepository<XdcTestResultWordEntity, String> {


  @Query(value = "SELECT w FROM XdcTestResultWordEntity w "
      + " where 1 = 1 and w.id in (:ids) order by w.weight desc")
  Page<XdcTestResultWordEntity> findAllByIds(@Param("ids") List<String> ids, Pageable pageable);
}
