package ai.maum.mlt.ta.xdc.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.ta.xdc.entity.XdcTestResultEntity;
import ai.maum.mlt.ta.xdc.entity.XdcTestResultWordEntity;
import ai.maum.mlt.ta.xdc.service.XDCTestResultService;
import java.util.HashMap;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/ta/xdc/result")
public class XDCResultController {

  static final Logger logger = LoggerFactory.getLogger(
      XDCResultController.class);

  @Autowired
  XDCTestResultService xdcTestResultService;

  @UriRoleDesc(role = "ta/xdc^R")
  @PostMapping("/getAllXdcResults")
  public ResponseEntity<?> getAllXdcResults(@RequestBody XdcTestResultEntity xdcTestResultEntity) {
    logger.info("======= call api  [[/api/ta/xdc/result/getAllXdcResults]] =======");
    try {
      Page<XdcTestResultEntity> xdcResultEntities = xdcTestResultService
          .findByXdcTestResultEntityPage(xdcTestResultEntity);
      return new ResponseEntity<>(xdcResultEntities, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/xdc^D")
  @PostMapping("/deleteByXdcResult" )
  public ResponseEntity<?> deleteByXdcResult(@RequestBody XdcTestResultEntity xdcTestResultEntity) {

    logger.info("======= call api  [[/api/ta/xdc/result/deleteByXdcResult]] =======");
    try {
      HashMap<String, Integer> result = new HashMap<>();
      result.put("deleteCount", xdcTestResultService.deleteByXdcResult(xdcTestResultEntity));
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/xdc^R")
  @PostMapping("/getWordsWithPage" )
  public ResponseEntity<?> getWordsWithPage(@RequestBody XdcTestResultWordEntity entity) {
    logger.info("======= call api  [[/api/ta/xdc/result/getWordsWithPage]] =======");
    try {
      List<String> ids = xdcTestResultService.getWordsIdBySentenceEntity(entity.getWorkspaceId(), entity.getSentenceId());
      Page<XdcTestResultWordEntity> xdcWordEntities = xdcTestResultService.findWordsByIds(ids, entity);
      return new ResponseEntity<>(xdcWordEntities, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
