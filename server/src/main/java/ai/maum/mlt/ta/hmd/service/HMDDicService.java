package ai.maum.mlt.ta.hmd.service;

import ai.maum.mlt.ta.common.entity.ModelManagementEntity;
import ai.maum.mlt.ta.hmd.entity.HMDDicEntity;
import java.util.List;
import org.springframework.data.domain.Page;

public interface HMDDicService {

  List<HMDDicEntity> getHMDDicList(HMDDicEntity hmdDicEntity);
  Page<HMDDicEntity> getHMDDicPageList(HMDDicEntity hmdDicEntity);
  void updateApplied(HMDDicEntity hmdDicEntity);
  List<ModelManagementEntity> getHMDDicInfo(ModelManagementEntity modelManagementEntity);
}