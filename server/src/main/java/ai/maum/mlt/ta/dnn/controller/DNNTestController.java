package ai.maum.mlt.ta.dnn.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.common.auth.entity.WorkspaceEntity;
import ai.maum.mlt.common.auth.service.WorkspaceService;
import ai.maum.mlt.common.util.FileUtils;
import ai.maum.mlt.itfc.TaGrpcInterfaceManager;
import ai.maum.mlt.ta.dnn.entity.DNNModelEntity;
import ai.maum.mlt.ta.dnn.service.DNNModelService;
import io.grpc.Metadata;
import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import maum.brain.cl.ClassifierOuterClass.ClassifiedSummary;
import maum.brain.cl.ClassifierOuterClass.Model;
import maum.brain.cl.ClassifierOuterClass.ServerState;
import maum.brain.cl.ClassifierOuterClass.ServerStatus;
import maum.common.LangOuterClass.LangCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/ta/dnn/test")
public class DNNTestController {

  static final Logger logger = LoggerFactory.getLogger(DNNTestController.class);

  @Value("${brain-ta.brain-cl.ip}")
  private String ip;
  @Value("${brain-ta.brain-cl.port}")
  private int port;
  @Value("${ta.file.path}")
  String taPath;

  @Autowired
  DNNModelService dnnModelService;

  @Autowired
  private WorkspaceService workspaceService;

  @Autowired
  private FileUtils fileUtils;

  @UriRoleDesc(role = "ta/dnn^R")
  @RequestMapping(
      value = "/getAllModels",
      method = RequestMethod.POST
  )
  public ResponseEntity<?> getAllModels(@RequestBody DNNModelEntity dnnModelEntity) {
    logger.info("======= call api  [[/api/ta/dnn/test/getAllModels]] =======");
    try {
      List<DNNModelEntity> dnnModelEntities = dnnModelService
          .getAllModels(dnnModelEntity.getWorkspaceId());
      return new ResponseEntity<>(dnnModelEntities, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }


  @UriRoleDesc(role = "ta/dnn^R")
  @RequestMapping(
          value = "/getModel",
          method = RequestMethod.POST
  )
  public ResponseEntity<?> getModel(@RequestBody DNNModelEntity param) {
    logger.info("======= call api  [[/api/ta/dnn/test/getAllModels]] =======");
    try {
      DNNModelEntity dnnModelEntity = dnnModelService.getModelByWorkspaceAndName(param.getWorkspaceId(), param.getName());
      return new ResponseEntity<>(dnnModelEntity, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/dnn^X")
  @RequestMapping(
      value = "/taDnnAnalyzeText",
      method = RequestMethod.POST
  )
  public ResponseEntity<?> taDnnAnalyzeText(@RequestBody DNNModelEntity param) {
    logger.info("======= call api  [[/api/ta/dnn/test/taDnnAnalyzeText]] =======");
    try {

      String[] lines = param.getTextData().split("\n");
      final List<String> lineList = new ArrayList<>();
      Collections.addAll(lineList, lines);

      for (String line : lineList) {
        line.trim();
        if (line.equals("")) {
          lineList.remove(line);
        }
      }

      TaGrpcInterfaceManager targetClient = new TaGrpcInterfaceManager(ip, port);
      WorkspaceEntity workspaceEntity = workspaceService
          .getWorkspace(param.getWorkspaceId());

      DNNModelEntity dnnModelEntity = dnnModelService.getModelByWorkspaceAndName(param.getWorkspaceId(), param.getName());

      // #1 SetModel 호출
      File clientBinary = new File(
          fileUtils.getSystemConfPath(
              taPath + "/" + workspaceEntity.getId() + "/dnn-model/" + dnnModelEntity
                  .getDnnBinary()));
      Metadata metadata = new Metadata();
      metadata.put(Metadata.Key.of("in.lang", Metadata.ASCII_STRING_MARSHALLER), String.valueOf(
          LangCode.valueOf(0)));
      metadata.put(Metadata.Key.of("in.filename", Metadata.ASCII_STRING_MARSHALLER),
          clientBinary.getName());
      metadata.put(Metadata.Key.of("in.model", Metadata.ASCII_STRING_MARSHALLER),
          dnnModelEntity.getName());

      Model.Builder model = Model.newBuilder();
      // Lang은 일단 항상 한국어로만 사용하도록 코딩
      // model.setLang(LangCode.valueOf(workspaceEntity.getLangCode()));
      model.setLang(LangCode.kor);
      model.setModel(dnnModelEntity.getName());
      ServerStatus findServerStatus = targetClient.find(model.build());

      //find에서 찾은 serverStatus가 running 상태가 아니면 failed
      if (!findServerStatus.getRunning()) {
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
      }

      String serverAddress = findServerStatus.getServerAddress();
      String[] host = serverAddress.split(":");
      TaGrpcInterfaceManager realClient = new TaGrpcInterfaceManager(host[0],
          Integer.parseInt(host[1]));

      logger.debug("===============serverStatus===========" + findServerStatus);

      int wait = 0;
      boolean runningFlag = false;
      ServerStatus pingStatus = null;
      while (true) {
        try {
          pingStatus = realClient.classifierPing(model.build());
          if (wait > 60) {
            break;
          }
          if (pingStatus != null && pingStatus.getRunning() && pingStatus.getState().equals(
              ServerState.SERVER_STATE_RUNNING)) {
            runningFlag = true;
            break;
          }
          wait++;
          Thread.sleep(1000);
        } catch (InterruptedException e) {
          logger.error("taDnnAnalyzeText e : " , e);
          return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
          logger.error("taDnnAnalyzeText e : " , e);
          return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
      }

      if (runningFlag) {
        List<ClassifiedSummary> classifiedSummaryList = realClient
            .getClassMultiple(lines, LangCode.valueOf(0),
                dnnModelEntity.getName());

        List<HashMap<String, Object>> resultList = new ArrayList<>();

        if (classifiedSummaryList != null) {
          for (ClassifiedSummary classifiedSummary : classifiedSummaryList) {
            HashMap<String, Object> resultMap = new HashMap<>();
            resultMap.put("probability", BigDecimal.valueOf(classifiedSummary.getProbabilityTop())
                .setScale(3, BigDecimal.ROUND_HALF_UP));
            resultMap.put("category", classifiedSummary.getC1Top());
            resultMap.put("sentence", classifiedSummary.getSentences(0).getText());

            resultList.add(resultMap);
          }
        }

        return new ResponseEntity<>(resultList, HttpStatus.OK);
      } else {
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
      }
    } catch (Exception e) {
      logger.error("taDnnAnalyzeText e : " , e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
