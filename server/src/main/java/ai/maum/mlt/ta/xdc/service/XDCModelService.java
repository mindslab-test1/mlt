package ai.maum.mlt.ta.xdc.service;

import ai.maum.mlt.ta.xdc.entity.XDCModelEntity;

import java.util.List;

public interface XDCModelService {

  XDCModelEntity selectModelByKey(String key);

  List<XDCModelEntity> getAllModels(String workspaceId);

  List<XDCModelEntity> getModelsByName(String workspaceId, String name);

  XDCModelEntity getModelByWorkspaceAndName(String workspaceId, String name);

  XDCModelEntity insertModel(XDCModelEntity xdcModelEntity);

  void deleteModel(String xdcKey);

}
