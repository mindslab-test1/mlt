package ai.maum.mlt.ta.xdc.service;

import ai.maum.mlt.ta.xdc.entity.XDCDicLineEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface XDCDicLineService {

  List<XDCDicLineEntity> getAllDicLinesByVersionId(String versionId);

  List<XDCDicLineEntity> getAllDicLines(String id, String category);

  Page<XDCDicLineEntity> getAllDicLines(String id, String category, String sentence,
                                        Pageable pageable);

  List<XDCDicLineEntity> getDuplicatesLines(String versionId, String category);

  List<String> getAllDistinct(String versionId);

  XDCDicLineEntity insertLine(XDCDicLineEntity xdcDicLineEntity);

  boolean deleteDuplicatesLines(String versionId, String category);

  boolean deleteLine(XDCDicLineEntity xdcDicLineEntity);

  void deleteByVersionId(String versionId);

  void updateDicLineCategory(String category, String oldName);
}
