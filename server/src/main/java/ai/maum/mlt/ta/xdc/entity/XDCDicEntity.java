package ai.maum.mlt.ta.xdc.entity;


import ai.maum.mlt.common.auth.entity.UserEntity;
import ai.maum.mlt.common.auth.entity.WorkspaceEntity;
import ai.maum.mlt.common.pagenation.PageParameters;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode(callSuper=false)
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "MAI_XDC_DIC")
public class XDCDicEntity extends PageParameters {

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid")
  @Column(name = "ID", length = 40, nullable = false)
  private String id;

  @Column(name = "WORKSPACE_ID", length = 40, nullable = false)
  private String workspaceId;

  @Column(name = "NAME", length = 40, nullable = false)
  private String name;

  @Lob
  @Column(name = "VERSIONS")
  private String versions;

  @Column(name = "APPLIED", length = 512)
  private String applied;

  @Column(name = "CREATOR_ID", length = 40)
  private String creatorId;

  @CreatedDate
  @Column(name = "CREATED_AT")
  private Date createdAt;

  @Column(name = "UPDATER_ID", length = 40)
  private String updaterId;

  @LastModifiedDate
  @Column(name = "UPDATED_AT")
  private Date updatedAt;

  @ManyToOne
  @JoinColumn(name = "CREATOR_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity createUserEntity;

  @ManyToOne
  @JoinColumn(name = "UPDATER_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity updateUserEntity;

  @ManyToOne
  @JoinColumn(name = "WORKSPACE_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private WorkspaceEntity workspaceEntity;

  @Transient
  private Integer epoch;

  @Transient
  private Integer maxBatchSz;

  @Transient
  private Integer trainMaxSz;

  @Transient
  private Integer testMaxSz;

  @Transient
  private String message;

  @Transient
  private String title;
}
