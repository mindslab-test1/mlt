package ai.maum.mlt.ta.hmd.service;

import ai.maum.mlt.ta.hmd.entity.HMDDicLineEntity;
import ai.maum.mlt.ta.hmd.repository.HMDDicLineRepository;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class HMDDicLineServiceImpl implements HMDDicLineService {

  @Autowired
  HMDDicLineRepository hmdDicLineRepository;

  @Override
  public List<HMDDicLineEntity> getCategoryList(HMDDicLineEntity hmdDicLineEntity) {
    return null;
  }

  @Override
  public List<HMDDicLineEntity> getAllDicLinesByVersionId(String versionId) {
    return hmdDicLineRepository.getAllByVersionId(versionId);
  }

  @Override
  public List<HMDDicLineEntity> getAllDicLines(String versionId, String category) {
    return hmdDicLineRepository.getAllByVersionIdAndCategoryOrderBySeqDesc(versionId, category);
  }

  @Override
  public Page<HMDDicLineEntity> getAllDicLines(String versionId, String category, String rule,
      Pageable pageable) {
    return hmdDicLineRepository
        .getAllByVersionIdAndCategoryAndRuleOrderBySeqDesc(versionId, category, rule, pageable);
  }

  @Override
  public HMDDicLineEntity insertLine(HMDDicLineEntity hmdDicLineEntity) {
    return hmdDicLineRepository.save(hmdDicLineEntity);
  }

  @Override
  @Transactional
  public void deleteByVersionId(HMDDicLineEntity hmdDicLineEntity) {
    hmdDicLineRepository.deleteByVersionId(hmdDicLineEntity.getVersionId());


  }

  @Override
  public boolean deleteLine(HMDDicLineEntity hmdDicLineEntity) {
    try {
      hmdDicLineRepository.delete(hmdDicLineEntity);
      return true;
    } catch (Exception e) {
      return false;
    }
  }

}
