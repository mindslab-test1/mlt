package ai.maum.mlt.ta.common.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.common.version.entity.CommitHistoryEntity;
import ai.maum.mlt.common.version.service.CommitHistoryService;
import ai.maum.mlt.ta.common.entity.ModelManagementEntity;
import ai.maum.mlt.ta.common.service.ModelManagementService;
import ai.maum.mlt.ta.xdc.entity.XDCModelEntity;
import ai.maum.mlt.ta.xdc.entity.XdcTestResultEntity;
import ai.maum.mlt.ta.xdc.service.XDCModelService;
import ai.maum.mlt.ta.xdc.service.XDCTestResultService;
import ai.maum.mlt.ta.xdc.service.XDCTrainingService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/ta")
public class ModelManagementController {

  static final Logger logger = LoggerFactory.getLogger(ModelManagementController.class);

  @Autowired
  private ModelManagementService modelManagementService;

  @Autowired
  private CommitHistoryService commitHistoryService;

  @Autowired
  private XDCTrainingService xdcTrainingService;

  @Autowired
  private XDCTestResultService xdcTestResultService;

  @Autowired
  private XDCModelService xdcModelService;

  @UriRoleDesc(role = "ta/common^C")
  @RequestMapping(
      value = "/insertHmdOrDnn",
      method = RequestMethod.POST)
  public ResponseEntity<?> insertHmdOrDnn(@RequestBody ModelManagementEntity modelManagementParam) {
    logger.info("======= call api  POST [[/api/ta/insertHmdOrDnn]] =======");
    HashMap<String, Object> result = new HashMap<>();
    if (modelManagementService
        .getCountByWorkspaceIdandName(modelManagementParam) > 0) {
      result.put("message", "INSERT_DUPLICATED");
      return new ResponseEntity<>(result, HttpStatus.OK);
    }
    try {
      modelManagementService.insertHmdorDnn(modelManagementParam);
      return new ResponseEntity<>(modelManagementParam, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/common^C")
  @RequestMapping(
    value = "/insertTaModel",
    method = RequestMethod.POST)
  public ResponseEntity<?> insertTaModel(@RequestBody ModelManagementEntity modelManagementParam) {
    logger.info("======= call api  POST [[/api/ta/insertTaModel]] =======");
    ModelManagementEntity res  = null;
    if (modelManagementService
      .getCountByWorkspaceIdandName(modelManagementParam) > 0) {
      return new ResponseEntity<>(null, HttpStatus.CONFLICT);
    }
    try {
      res = modelManagementService.insertTaModel(modelManagementParam);
      return new ResponseEntity<>(res, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/common^R")
  @RequestMapping(
      value = "/getHmdOrDnn",
      method = RequestMethod.POST)
  public ResponseEntity<?> getHmdOrDnn(@RequestBody ModelManagementEntity modelManagementEntity) {
    logger.info("======= call api  POST [[/api/ta/getHmdOrDnn]] =======");

    try {
      Page<ModelManagementEntity> pageModelManageMent = modelManagementService
          .getHmdorDnn(modelManagementEntity);
      return new ResponseEntity<>(pageModelManageMent, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/common^R")
  @RequestMapping(
      value = "/getHmdOrDnnWithCommitHistory",
      method = RequestMethod.POST
  )
  public ResponseEntity<?> getHmdOrDnnWithCommitHistory(@RequestBody ModelManagementEntity modelManagementEntity) {
    logger.info("======= call api  [[/api/ta/training/getHmdOrDnnWithCommitHistory]] =======");
    List<HashMap<String, Object>> result = new ArrayList<HashMap<String, Object>>();
    try {
      Page<ModelManagementEntity> pageModelManageMent = modelManagementService
          .getHmdorDnn(modelManagementEntity);
      for (ModelManagementEntity entity : pageModelManageMent) {
        int cnt = 0;
        CommitHistoryEntity cs = new CommitHistoryEntity();
        cs.setWorkspaceId(entity.getWorkspaceId());
        if (entity.getDnnDicId() == null || "".equals(entity.getDnnDicId())) {
          cs.setType("HMD");
          cs.setFileId(entity.getHmdDicId());
        } else {
          cs.setType("DNN");
          cs.setFileId(entity.getDnnDicId());
        }
        cnt = commitHistoryService.getCountCommit(cs);
        HashMap<String, Object> elem = new HashMap<String, Object>();
        elem.put("entity", entity);
        elem.put("cnt", cnt);
        result.add(elem);
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }


  @UriRoleDesc(role = "ta/common^R")
  @RequestMapping(
    value = "/getTaModelWithCommitHistory",
    method = RequestMethod.POST
  )
  public ResponseEntity<?> getTaModelWithCommitHistory(@RequestBody ModelManagementEntity modelManagementEntity) {
    logger.info("======= call api  [[/api/ta/training/getTaModelWithCommitHistory]] =======");
    List<HashMap<String, Object>> result = new ArrayList<HashMap<String, Object>>();
    try {
      Page<ModelManagementEntity> pageModelManageMent = modelManagementService
          .getTaModelPage(modelManagementEntity);
      for (ModelManagementEntity entity : pageModelManageMent) {
        int cnt = 0;
        CommitHistoryEntity cs = new CommitHistoryEntity();
        cs.setWorkspaceId(entity.getWorkspaceId());
        if (entity.getDnnDicId() != null && !"".equals(entity.getDnnDicId())) {
          cs.setType("DNN");
          cs.setFileId(entity.getDnnDicId());
        } else if (entity.getHmdDicId() != null && !"".equals(entity.getHmdDicId())) {
          cs.setType("HMD");
          cs.setFileId(entity.getHmdDicId());
        } else if (entity.getXdcDicId() != null && !"".equals(entity.getXdcDicId())) {
          cs.setType("XDC");
          cs.setFileId(entity.getXdcDicId());
        }
        cnt = commitHistoryService.getCountCommit(cs);
        HashMap<String, Object> elem = new HashMap<String, Object>();
        elem.put("entity", entity);
        elem.put("cnt", cnt);
        result.add(elem);
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/common^R")
  @RequestMapping(
      value = "/getHmdOrDnnModel",
      method = RequestMethod.POST)
  public ResponseEntity<?> getHmdOrDnnModel(@RequestBody ModelManagementEntity modelManagementEntity) {
    logger.info("======= call api  POST [[/api/ta/getHmdorDnnModel]] =======");

    try {
      ModelManagementEntity modelManagement = modelManagementService
          .getHmdOrDnnModel(modelManagementEntity);
      return new ResponseEntity<>(modelManagement, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/common^R")
  @RequestMapping(
    value = "/getTaModel",
    method = RequestMethod.POST)
  public ResponseEntity<?> getTaModel(@RequestBody ModelManagementEntity modelManagementEntity) {
    logger.info("======= call api  POST [[/api/ta/getTaModel]] =======");

    try {
      ModelManagementEntity modelManagement;
      modelManagement = modelManagementService.getTaModel(modelManagementEntity);
      return new ResponseEntity<>(modelManagement, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/common^D")
  @RequestMapping(
      value = "/deleteSelectedModels",
      method = RequestMethod.POST)
  public ResponseEntity<?> deleteSelectedModels(
      @RequestBody List<ModelManagementEntity> modelManagementEntity) {
    logger.info("======= call api  POST [[/api/ta/deleteSelectedModels]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {
      List<ModelManagementEntity> deleteModels =
          modelManagementService.deleteSelectedModels(modelManagementEntity);
      for (ModelManagementEntity entity : deleteModels) {
        CommitHistoryEntity cs = new CommitHistoryEntity();
        cs.setFileId(entity.getId());
        cs.setWorkspaceId(entity.getWorkspaceId());
        if (entity.getHmdDicId() != null) {
          cs.setType("HMD");
        } else if (entity.getDnnDicId() != null) {
          cs.setType("DNN");
        } else if (entity.getXdcDicId() != null) {
          cs.setType("XDC");
          // xdc trained 모델, 결과 데이터 삭제
          XDCModelEntity model = xdcModelService.getModelByWorkspaceAndName(entity.getWorkspaceId(), entity.getName());
          xdcModelService.deleteModel(model.getXdcKey());
          xdcTrainingService.deleteTraining(model.getXdcKey());
          XdcTestResultEntity testEntity = new XdcTestResultEntity();
          testEntity.setWorkspaceId(entity.getWorkspaceId());
          testEntity.setModel(entity.getName());
          xdcTestResultService.deleteByXdcResult(testEntity);
        }
        commitHistoryService.deleteCommit(cs);
      }
      result.put("resultCode", "SUCCESS");
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/common^U")
  @RequestMapping(
      value = "/updateHmdOrDnn",
      method = RequestMethod.POST)
  public ResponseEntity<?> updateHmdOrDnn(
      @RequestBody ModelManagementEntity modelManagementEntity) {
    logger.info("======= call api  POST [[/api/ta/updateHmdOrDnn]] =======");

    if (modelManagementService
        .getCountByWorkspaceIdandName(modelManagementEntity) > 0) {
      return new ResponseEntity<>(null, HttpStatus.CONFLICT);
    }

    modelManagementService.updateSelectedModels(modelManagementEntity);

    try {
      return new ResponseEntity<>(null, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }


}
