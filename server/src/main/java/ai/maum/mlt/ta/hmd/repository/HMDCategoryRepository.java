package ai.maum.mlt.ta.hmd.repository;

import ai.maum.mlt.ta.hmd.entity.HMDCategoryEntity;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface HMDCategoryRepository extends JpaRepository<HMDCategoryEntity, String> {

  @Query(name = "getAllByWorkspaceIdAndHmdDicId", nativeQuery = true)
  List<HMDCategoryEntity> getAllByWorkspaceIdAndHmdDicId(@Param("workspaceId") String workspaceId,
      @Param("hmdDicId") String hmdDicId);

  HMDCategoryEntity getByWorkspaceIdAndHmdDicIdAndName(String workspaceId, String hmdDicId,
      String name);

  HMDCategoryEntity findOneByNameAndHmdDicId(String name, String hmdDicId);

  @Modifying
  @Transactional
  void deleteByHmdDicId(String id);
}
