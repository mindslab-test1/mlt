package ai.maum.mlt.ta.dnn.repository;

import ai.maum.mlt.ta.dnn.entity.DNNModelEntity;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
@Transactional
public interface DNNModelRepository extends JpaRepository<DNNModelEntity, String> {
  DNNModelEntity findByDnnKey(String dnnKey);
  List<DNNModelEntity> findAllByWorkspaceId(String workspaceId);
  List<DNNModelEntity> findAllByWorkspaceIdAndName(String workspaceId, String name);
  DNNModelEntity findTopByWorkspaceIdAndNameOrderByCreatedAtDesc(String workspaceId, String name);
}
