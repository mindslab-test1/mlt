package ai.maum.mlt.ta.xdc.service;

import ai.maum.mlt.ta.xdc.entity.XDCDicEntity;

import java.util.List;

import org.springframework.data.domain.Page;

public interface XDCDicService {

  List<XDCDicEntity> getXdcDicAllList(String workspaceId);

  Page<XDCDicEntity> getXdcDicAllList(XDCDicEntity xdcDicEntity);

  XDCDicEntity getXdcDic(String workspace, String name);
}
