package ai.maum.mlt.ta.xdc.service;

import ai.maum.mlt.ta.xdc.entity.XdcTestResultEntity;
import ai.maum.mlt.ta.xdc.entity.XdcTestResultSentenceEntity;
import ai.maum.mlt.ta.xdc.entity.XdcTestResultWordEntity;
import org.springframework.data.domain.Page;

import java.util.List;

public interface XDCTestResultService {

  Page<XdcTestResultEntity> findByXdcTestResultEntityPage(XdcTestResultEntity xdcTestResultEntity);

  List<XdcTestResultEntity> findByXdcTestResultEntities(XdcTestResultEntity xdcTestResultEntity);

  void insertResult(XdcTestResultEntity xdcTestResultEntity);

  XdcTestResultSentenceEntity insertResultSentence(
      XdcTestResultSentenceEntity xdcTestResultSentenceEntity);

  XdcTestResultWordEntity insertResultWord(XdcTestResultWordEntity xdcTestResultWordEntity);

  List<XdcTestResultEntity> insertResults(List<XdcTestResultEntity> xdcResultEntities);

  Integer deleteByXdcResult(XdcTestResultEntity xdcTestResultEntity);

  Page<XdcTestResultWordEntity> findWordsByIds(List<String> ids,
      XdcTestResultWordEntity xdcTestResultWordEntity);

  List<String> getWordsIdBySentenceEntity(String workspaceId, String sentenceId);
}
