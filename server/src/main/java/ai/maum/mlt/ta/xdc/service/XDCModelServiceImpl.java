package ai.maum.mlt.ta.xdc.service;

import ai.maum.mlt.ta.xdc.entity.XDCModelEntity;
import ai.maum.mlt.ta.xdc.repository.XDCModelRepository;
import ai.maum.mlt.ta.xdc.service.XDCModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class XDCModelServiceImpl implements XDCModelService {

  @Autowired
  private XDCModelRepository xdcModelRepository;

  @Override
  public XDCModelEntity selectModelByKey(String key) {
    return xdcModelRepository.findByXdcKey(key);
  }

  @Override
  public XDCModelEntity getModelByWorkspaceAndName(String workspaceId, String name) {
    return xdcModelRepository.findTopByWorkspaceIdAndNameOrderByCreatedAtDesc(workspaceId, name);
  }

  @Override
  public List<XDCModelEntity> getAllModels(String workspaceId) {
    return this.xdcModelRepository.findAllByWorkspaceId(workspaceId);
  }

  @Override
  public List<XDCModelEntity> getModelsByName(String workspaceId, String name) {
    return this.xdcModelRepository.findAllByWorkspaceIdAndName(workspaceId, name);
  }

  @Override
  public XDCModelEntity insertModel(XDCModelEntity xdcModelEntity) {
    xdcModelEntity.setCreatedAt(new Date());
    return this.xdcModelRepository.save(xdcModelEntity);
  }

  @Override
  public void deleteModel(String xdcKey) {
    this.xdcModelRepository.delete(xdcKey);
  }
}
