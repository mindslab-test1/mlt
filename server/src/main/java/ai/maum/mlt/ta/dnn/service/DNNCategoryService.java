package ai.maum.mlt.ta.dnn.service;

import ai.maum.mlt.ta.dnn.entity.DNNCategoryEntity;
import java.util.List;
import org.springframework.web.multipart.MultipartFile;

public interface DNNCategoryService {

  List<DNNCategoryEntity> getCategoryTree(String workspaceId, String dnnDicId);

  List<DNNCategoryEntity> getCategoryAllList(String workspaceId, String DnnDicId);

  DNNCategoryEntity getCategory(String workspaceId, String dnnDicId, String name);

  DNNCategoryEntity findOneByNameAndDnnDicId(String name, String dnnDicId);

  DNNCategoryEntity insertCategory(DNNCategoryEntity dnnCategoryEntity);

  DNNCategoryEntity updateCategory(DNNCategoryEntity dnnCategoryEntity);

  boolean deleteCategory(String id);

  void dnnFileUpload(String dnnDicId, String workspaceId, MultipartFile file) throws Exception;
}
