package ai.maum.mlt.ta.xdc.repository;

import ai.maum.mlt.ta.xdc.entity.XDCDicEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface XDCDicRepository extends JpaRepository<XDCDicEntity, Integer> {

  List<XDCDicEntity> getAllByWorkspaceIdOrderByNameAsc(String workspaceId);

  @Query(value = "SELECT d "
    + "         FROM XDCDicEntity d "
    + "         WHERE d.workspaceId = :workspaceId "
    + "         ORDER BY d.updatedAt DESC")
  Page<XDCDicEntity> getAllByWorkspaceId(
    @Param("workspaceId") String workspaceId,
    Pageable pageable
  );


  @Query(value = "SELECT d "
    + "         FROM XDCDicEntity d "
    + "         WHERE d.workspaceId = :workspaceId "
    + "         And d.name = :name "
    + "         ORDER BY d.updatedAt DESC")
  XDCDicEntity getDicByWorkspaceIdAndName(
    @Param("workspaceId") String workspaceId,
    @Param("name") String name
  );

}
