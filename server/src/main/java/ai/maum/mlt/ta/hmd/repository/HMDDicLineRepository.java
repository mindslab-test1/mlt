package ai.maum.mlt.ta.hmd.repository;

import ai.maum.mlt.ta.hmd.entity.HMDDicLineEntity;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface HMDDicLineRepository extends JpaRepository<HMDDicLineEntity, String> {



  List<HMDDicLineEntity> getAllByVersionId(String versionId);
  List<HMDDicLineEntity> getAllByVersionIdAndCategoryOrderBySeqDesc(String versionId, String category);


  @Query(value = "SELECT h "
      + "         FROM HMDDicLineEntity h "
      + "         WHERE h.versionId = :versionId "
      + "         AND (h.category = :category OR :category IS NULL)"
      + "         AND (h.rule LIKE CONCAT('%',:rule,'%') OR :rule IS NULL) "
      + "         ORDER BY h.seq DESC")
  Page<HMDDicLineEntity> getAllByVersionIdAndCategoryAndRuleOrderBySeqDesc(
      @Param("versionId") String versionId,
      @Param("category") String category,
      @Param("rule") String rule,
      Pageable pageable
  );

  @Modifying
  @Query("UPDATE HMDDicLineEntity d SET d.category = :category WHERE d.category = :oldName")
  void updateDicLineCategory(@Param("category") String category,
      @Param("oldName") String oldName);

  @Query("SELECT d1 FROM HMDDicLineEntity d1 WHERE d1.versionId = :versionId AND d1.category = :category AND d1.seq NOT IN (SELECT MIN(d2.seq) AS seq FROM HMDDicLineEntity d2 GROUP BY d2.category, d2.rule)")
  List<HMDDicLineEntity> getDistinctByHMDDicLine(@Param("versionId") String versionId, @Param("category") String category);


  @Modifying
  @Transactional
  void deleteByVersionId(String versionId);

}
