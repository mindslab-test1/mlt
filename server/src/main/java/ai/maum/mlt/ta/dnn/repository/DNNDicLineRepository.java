package ai.maum.mlt.ta.dnn.repository;

import ai.maum.mlt.ta.dnn.entity.DNNDicLineEntity;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface DNNDicLineRepository extends JpaRepository<DNNDicLineEntity, String> {

  List<DNNDicLineEntity> getAllByVersionId(String versionId);

  List<DNNDicLineEntity> getAllByVersionIdAndCategoryOrderBySeqDesc(String versionId,
      String category);

  @Query(value = "SELECT d "
      + "         FROM DNNDicLineEntity d "
      + "         WHERE d.versionId = :versionId "
      + "         AND (d.category = :category OR :category IS NULL)"
      + "         AND (d.sentence LIKE CONCAT('%',:sentence,'%') OR :sentence IS NULL) "
      + "         ORDER BY d.seq DESC")
  Page<DNNDicLineEntity> getAllByVersionIdAndCategoryAndSentenceOrderBySeqDesc(
      @Param("versionId") String versionId,
      @Param("category") String category,
      @Param("sentence") String sentence,
      Pageable pageable
  );

  Page<DNNDicLineEntity> getAllByVersionIdOrderBySeqDesc(String versionId, Pageable pageable);

  @Query("SELECT new ai.maum.mlt.ta.dnn.entity.DNNDicLineEntity(d.sentence, count(d.sentence)) FROM DNNDicLineEntity d WHERE d.versionId = :versionId AND d.category = :category GROUP BY d.sentence having count(d.sentence)>1")
  List<DNNDicLineEntity> getDuplicatesLines(@Param("versionId") String versionId,
      @Param("category") String category);

  @Modifying
  @Query("UPDATE DNNDicLineEntity d SET d.category = :category WHERE d.category = :oldName")
  void updateDicLineCategory(@Param("category") String category,
      @Param("oldName") String oldName);

  @Query("SELECT d1 FROM DNNDicLineEntity d1 WHERE d1.versionId = :versionId AND d1.category = :category AND d1.seq NOT IN (SELECT MIN(d2.seq) as seq FROM DNNDicLineEntity d2 GROUP BY d2.category, d2.sentence)")
  List<DNNDicLineEntity> getDistinctByDNNDicLine(@Param("versionId") String versionId,
      @Param("category") String category);

  @Query("SELECT distinct (d1.category) FROM DNNDicLineEntity d1 WHERE d1.versionId = :versionId")
  List<String> getAllDistinct(@Param("versionId") String versionId);

  @Modifying
  void deleteByVersionId(String versionId);
}
