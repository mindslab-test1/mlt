package ai.maum.mlt.ta.common.repository;

import ai.maum.mlt.ta.common.entity.ModelManagementEntity;
import java.util.Date;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface ModelManagementRepository extends JpaRepository<ModelManagementEntity, String> {
  int deleteById(String id);

  ModelManagementEntity findById(String id);
  Integer countByNameAndWorkspaceId(String name, String worksapceId);
  Integer countByNameAndWorkspaceIdAndHmdUse(String name, String worksapceId, boolean hmduse);

  @Query(value = "SELECT m "
      + "         FROM ModelManagementEntity m "
      + "         WHERE m.workspaceId =:workspaceId "
      + "         AND ((true = :hmdUse AND m.hmdUse = 1) OR (true <> :hmdUse AND m.dnnUse = 1))"
      + "         AND (m.name like CONCAT('%',:name,'%') OR :name IS NULL)"
      + "         AND (m.manDesc like CONCAT('%',:manDesc,'%') OR :manDesc IS NULL)"
  )
  Page<ModelManagementEntity> findByHmdorDnnEntity(
      @Param("workspaceId") String workspaceId,
      @Param("name") String name,
      @Param("manDesc") String manDesc,
      @Param("hmdUse") boolean hmdUse,
      Pageable pageable
  );

  @Query(value = "SELECT m "
      + "         FROM ModelManagementEntity m "
      + "         WHERE m.workspaceId =:workspaceId "
      + "         AND (true = :hmdUse AND m.hmdUse = :hmdUse OR m.dnnUse = :dnnUse)"
      + "         AND (m.name like CONCAT('%',:name,'%') OR :name IS NULL)"
  )
  ModelManagementEntity findByHmdorDnnEntityOne(
      @Param("workspaceId") String workspaceId,
      @Param("name") String name,
      @Param("dnnUse") boolean dnnUse,
      @Param("hmdUse") boolean hmdUse
  );

  @Query(value = "SELECT m "
    + "         FROM ModelManagementEntity m "
    + "         WHERE m.workspaceId =:workspaceId "
    + "         AND (true = :hmdUse AND m.hmdUse = :hmdUse OR m.dnnUse = :dnnUse OR m.xdcUse = :xdcUse)"
    + "         AND (m.name like CONCAT('%',:name,'%') OR :name IS NULL)"
  )
  ModelManagementEntity findByHmdOrDnnOrXdcEntityOne(
    @Param("workspaceId") String workspaceId,
    @Param("name") String name,
    @Param("dnnUse") boolean dnnUse,
    @Param("hmdUse") boolean hmdUse,
    @Param("xdcUse") boolean xdcUse
  );

  @Query(value = "SELECT m "
    + "         FROM ModelManagementEntity m "
    + "         WHERE m.workspaceId =:workspaceId "
    + "         AND (m.hmdUse = :hmdUse AND m.dnnUse = :dnnUse AND m.xdcUse = :xdcUse)"
    + "         AND (m.name like CONCAT('%',:name,'%') OR :name IS NULL)"
  )
  Page<ModelManagementEntity> findByHmdOrDnnOrXdcEntity(
    @Param("workspaceId") String workspaceId,
    @Param("name") String name,
    @Param("dnnUse") boolean dnnUse,
    @Param("hmdUse") boolean hmdUse,
    @Param("xdcUse") boolean xdcUse,
    Pageable pageable
  );


  @Modifying
  @Query("UPDATE ModelManagementEntity m SET m.updatedAt=:updatedAt, m.updaterId=:updaterId, m.dnnDicId=:dnnDicId, m.dnnUse=:dnnUse, m.hmdDicId=:hmdDicId, m.hmdUse=:hmdUse, m.manDesc=:manDesc WHERE m.id=:id")
  void updateData(@Param("updatedAt") Date updatedAt, @Param("updaterId") String updaterId,
      @Param("id") String id, @Param("dnnDicId") String dnnDicId, @Param("dnnUse") boolean dnnUse,
      @Param("hmdDicId") String hmdDicId, @Param("hmdUse") boolean hmdUse, @Param("manDesc") String manDesc);
}
