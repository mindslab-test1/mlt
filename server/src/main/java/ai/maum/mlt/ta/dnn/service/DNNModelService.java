package ai.maum.mlt.ta.dnn.service;

import ai.maum.mlt.ta.dnn.entity.DNNModelEntity;
import java.util.List;

public interface DNNModelService {

  DNNModelEntity selectModelByKey(String key);

  List<DNNModelEntity> getAllModels(String workspaceId);

  List<DNNModelEntity> getModelsByName(String workspaceId, String name);

  DNNModelEntity getModelByWorkspaceAndName(String workspaceId, String name);

  DNNModelEntity insertModel(DNNModelEntity dnnModelEntity);

  void deleteModel(String dnnKey);

}
