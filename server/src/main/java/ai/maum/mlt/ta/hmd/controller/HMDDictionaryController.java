package ai.maum.mlt.ta.hmd.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.common.auth.entity.WorkspaceEntity;
import ai.maum.mlt.common.auth.service.WorkspaceService;
import ai.maum.mlt.common.git.GitManager;
import ai.maum.mlt.common.version.entity.CommitHistoryEntity;
import ai.maum.mlt.common.version.service.CommitHistoryService;
import ai.maum.mlt.itfc.TaGrpcInterfaceManager;
import ai.maum.mlt.management.entity.HistoryEntity;
import ai.maum.mlt.management.service.HistoryService;
import ai.maum.mlt.ta.common.entity.ModelManagementEntity;
import ai.maum.mlt.ta.hmd.entity.HMDCategoryEntity;
import ai.maum.mlt.ta.hmd.entity.HMDDicEntity;
import ai.maum.mlt.ta.hmd.entity.HMDDicLineEntity;
import ai.maum.mlt.ta.hmd.service.HMDCategoryService;
import ai.maum.mlt.ta.hmd.service.HMDDicLineService;
import ai.maum.mlt.ta.hmd.service.HMDDicService;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import maum.brain.hmd.Hmd.HmdModel;
import maum.brain.hmd.Hmd.HmdRule;
import maum.common.LangOuterClass.LangCode;
import org.eclipse.jgit.lib.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("api/ta/hmd/dic")
public class HMDDictionaryController {

  static final Logger logger = LoggerFactory.getLogger(HMDDictionaryController.class);

  @Value("${hmd.train.server.ip}")
  private String ip;

  @Value("${hmd.train.server.port}")
  private int port;

  @Value("${git.file.path}")
  private String gitPath;

  @Autowired
  private HMDDicLineService hmdDicLineService;

  @Autowired
  private HMDDicService hmdDicService;

  @Autowired
  private HMDCategoryService hmdCategoryService;

  @Autowired
  private HistoryService historyService;

  @Autowired
  private WorkspaceService workspaceService;

  @Autowired
  private CommitHistoryService commitHistoryService;


  @UriRoleDesc(role = "ta/hmd^R")
  @RequestMapping(
      value = "/getHmdCategoryList",
      method = RequestMethod.POST)
  public ResponseEntity<?> getHmdCategoryList(@RequestBody HMDDicEntity hmdDicEntity) {
    logger.debug("start : {}", hmdDicEntity);

    try {
      return new ResponseEntity<>(hmdCategoryService.getCategoryTree(hmdDicEntity), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }


  @UriRoleDesc(role = "ta/hmd^R")
  @RequestMapping(
      value = "/getHmdDicList",
      method = RequestMethod.POST)
  public ResponseEntity<?> getHmdDicList(@RequestBody HMDDicEntity hmdDicEntity) {
    logger.debug("start : {}", hmdDicEntity);

    try {
      HashMap<String, Object> result = new HashMap<>();
      result.put("hmdDicList", hmdDicService.getHMDDicList(hmdDicEntity));

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /*
  * TA > HMD > 상세보기
  */
  @UriRoleDesc(role = "ta/hmd^R")
  @RequestMapping(
      value = "/getHmdDicInfo",
      method = RequestMethod.POST)
  public ResponseEntity<?> getHmdDicInfo(@RequestBody ModelManagementEntity modelManagementEntity) {

    logger.info("======= call api  [[/api/ta/hmd/getHmdDicInfo]] =======");
    try {

      HashMap<String, Object> result = new HashMap<>();
      result.put("hmdDicInfo", hmdDicService.getHMDDicInfo(modelManagementEntity));

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/hmd^C")
  @RequestMapping(
      value = "/upload-files",
      method = RequestMethod.POST)
  public ResponseEntity<?> insertFile(@RequestParam("workspaceId") String workspaceId,
      @RequestParam("data") String hmdDicId,
      @RequestParam("file") MultipartFile file) {
    logger.debug("start workspaceId :{}, data :{}, files: {}", workspaceId, hmdDicId, file);
    try {
      hmdCategoryService.hmdFileUpload(hmdDicId, workspaceId, file);

      HashMap<String, Object> result = new HashMap<>();
      result.put("status", "success");

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

  }

  @UriRoleDesc(role = "ta/hmd^R")
  @RequestMapping(
      value = "/getAllDicLines",
      method = RequestMethod.POST)
  public ResponseEntity<?> getAllDicLines(@RequestBody HMDDicLineEntity hmdDicLineEntity) {
    logger.debug("start : {}", hmdDicLineEntity);
    HashMap<String, Object> result = new HashMap<>();
    try {

      Page<HMDDicLineEntity> pageList = hmdDicLineService
          .getAllDicLines(hmdDicLineEntity.getVersionId(), hmdDicLineEntity.getCategory(),
              hmdDicLineEntity.getRule(), hmdDicLineEntity.getPageRequest());
      List<HMDDicLineEntity> resultList = pageList.getContent();
      result.put("total", pageList.getTotalElements());
      result.put("dicLineList", resultList);
      result.put("message", "SUCCESS");
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      result.put("message", "ERROR");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/hmd^C")
  @RequestMapping(
      value = "/insertLine",
      method = RequestMethod.POST)
  public ResponseEntity<?> insertLine(@RequestBody HMDDicLineEntity hmdDicLineEntity) {
    logger.debug("start : {}", hmdDicLineEntity);
    HashMap<String, Object> result = new HashMap<>();
    try {
      HMDDicLineEntity resultEntity = hmdDicLineService.insertLine(hmdDicLineEntity);
      result.put("dicLine", resultEntity);
      result.put("message", "SUCCESS");

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      result.put("message", "ERROR");
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/hmd^D")
  @RequestMapping(
      value = "/deleteCategory",
      method = RequestMethod.POST)
  public ResponseEntity<?> deleteCategory(@RequestBody HMDCategoryEntity hmdCategoryEntity) {
    logger.debug("start : {}", hmdCategoryEntity);
    HashMap<String, Object> result = new HashMap<>();
    try {
      if (hmdDicLineService
          .getAllDicLines(hmdCategoryEntity.getHmdDicId(), hmdCategoryEntity.getName()).size()
          == 0) {
        if (hmdCategoryService.deleteCategory(hmdCategoryEntity.getId())) {
          result.put("message", "DELETE_SUCCESS");
        } else {
          result.put("message", "DELETE_FAILED");
        }
      } else {
        result.put("message", "RULE_EXISTENCE");
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      result.put("message", "ERROR");
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/hmd^C")
  @RequestMapping(
      value = "/insertCategory",
      method = RequestMethod.POST)
  public ResponseEntity<?> insertCategory(@RequestBody HMDCategoryEntity hmdCategoryEntity) {
    logger.debug("start : {}", hmdCategoryEntity);
    HashMap<String, Object> result = new HashMap<>();
    try {
      HMDCategoryEntity duplicateCategoryEntity = isDuplicate(hmdCategoryEntity);
      if (duplicateCategoryEntity == null) {
        hmdCategoryEntity.setCreatedAt(new Date());
        HMDCategoryEntity resultCategoryEntity = hmdCategoryService
            .insertCategory(hmdCategoryEntity);
        result.put("message", "INSERT_SUCCESS");
        result.put("category", resultCategoryEntity);
      } else {
        result.put("message", "INSERT_DUPLICATED");
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      result.put("message", "INSERT_FAILED");
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/hmd^U")
  @RequestMapping(
      value = "/updateCategory",
      method = RequestMethod.POST)
  public ResponseEntity<?> updateCategory(@RequestBody HMDCategoryEntity hmdCategoryEntity) {
    logger.debug("start : {}", hmdCategoryEntity);
    HashMap<String, Object> result = new HashMap<>();
    try {
      HMDCategoryEntity duplicateCategoryEntity = isDuplicate(hmdCategoryEntity);
      if (duplicateCategoryEntity == null) {
        hmdCategoryEntity.setUpdatedAt(new Date());
        HMDCategoryEntity resultCategoryEntity = hmdCategoryService
            .updateCategory(hmdCategoryEntity);

        result.put("category", resultCategoryEntity);
        result.put("message", "UPDATE_SUCCESS");
      } else {
        result.put("message", "UPDATE_DUPLICATED");
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("updateCategory e : " , e);
      result.put("message", "UPDATE_FAILED");
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/hmd^D")
  @RequestMapping(
      value = "/deleteLines",
      method = RequestMethod.POST)
  public ResponseEntity<?> deleteLines(@RequestBody List<HMDDicLineEntity> hmdDicLineEntityList) {
    logger.debug("start : {}", hmdDicLineEntityList);
    HashMap<String, String> result = new HashMap<>();
    try {
      for (HMDDicLineEntity hmdDicLineEntity : hmdDicLineEntityList) {
        hmdDicLineService.deleteLine(hmdDicLineEntity);
      }
      result.put("message", "SUCCESS");
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      result.put("message", "ERROR");
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/hmd^R")
  @RequestMapping(
      value = "/hasLines",
      method = RequestMethod.POST)
  public ResponseEntity<?> hasLines(@RequestBody HMDDicLineEntity hmdDicLineEntity) {
    logger.debug("start : {}", hmdDicLineEntity);
    try {
      boolean result = true;
      if (hmdDicLineService
          .getAllDicLines(hmdDicLineEntity.getVersionId(), hmdDicLineEntity.getCategory()).size()
          == 0) {
        result = false;
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/hmd^X")
  @RequestMapping(
      value = "/hmdApply",
      method = RequestMethod.POST)
  public ResponseEntity<?> hmdApply(@RequestBody HMDDicEntity hmdDicEntity) {
    logger.debug("start : {}", hmdDicEntity);
    try {

      // To do
      // 1. 선택된 commit id로 rule,category를 가져와 HMDRule를 만들기 (version 1 1798Line)
      // 2. syncronization of commit data and engine data (version 1 1833Line)

      TaGrpcInterfaceManager client = new TaGrpcInterfaceManager(ip, port);

      //create hmdModel
      HmdModel.Builder hmdModel = HmdModel.newBuilder();

      WorkspaceEntity workspaceEntity = workspaceService
          .getWorkspace(hmdDicEntity.getWorkspaceId());

      // Lang은 일단 항상 한국어로만 사용하도록 코딩
      // hmdModel.setLang(LangCode.valueOf(workspaceEntity.getLangCode()));
      hmdModel.setLang(LangCode.kor);
      hmdModel.setModel(hmdDicEntity.getName().replace("_hmd", ""));

      List<HMDDicLineEntity> hmdDicLineEntityList = hmdDicLineService
          .getAllDicLinesByVersionId(hmdDicEntity.getId());

      for (HMDDicLineEntity hmdDicLineEntity : hmdDicLineEntityList) {
        try {
          //create hmdRule
          HmdRule.Builder hmdRule = HmdRule.newBuilder();
          hmdRule.setRule(hmdDicLineEntity.getRule());

          hmdRule.addCategories(hmdDicLineEntity.getCategory());

          hmdModel.addRules(hmdRule);
        } catch (Exception e) {
          logger.error(e.getMessage());
        }
      }

      //HMDDic table update
      hmdDicService.updateApplied(hmdDicEntity);

      // History table create
      HistoryEntity historyEntity = new HistoryEntity();
      historyEntity.setWorkspaceId(hmdDicEntity.getWorkspaceId());

      //Message의 원본코드
      // message = `${this.name} (${commitId.substring(0, 8)}) has been applied.`;
      historyEntity.setMessage(
          hmdDicEntity.getName() + " " + hmdDicEntity.getApplied().substring(0, 8)
              + " has been applied.");
      historyEntity.setCode("DICTIONARY_APPLY");
      historyEntity.setStartedAt(new Date());
      historyEntity.setEndedAt(new Date());
      historyService.insertHistory(historyEntity);

      client.setModel(hmdModel.build());
      client.setMatrix(hmdModel.build());

      HashMap<String, String> result = new HashMap<>();
      result.put("message", "SUCCESS");

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }


  private HMDCategoryEntity isDuplicate(HMDCategoryEntity hmdCategoryEntity) {
    if (hmdCategoryEntity.getUpdateState() == null) {
      return hmdCategoryService
          .getCategory(hmdCategoryEntity.getWorkspaceId(), hmdCategoryEntity.getHmdDicId(),
              hmdCategoryEntity.getName());
    } else {
      return null;
    }
  }

  @UriRoleDesc(role = "ta/hmd^U")
  @RequestMapping(
      value = "/commit",
      method = RequestMethod.POST)
  public ResponseEntity<?> gitCommit(@RequestBody HMDDicEntity hmdDicEntity) {
    logger.debug("start : {}", hmdDicEntity);
    HashMap<String, Object> result = new HashMap<>();
    try {
      GitManager gs = new GitManager();

      String content = "";

      List<HMDDicLineEntity> hmdDicLineEntities = hmdDicLineService
          .getAllDicLinesByVersionId(hmdDicEntity.getId());

    /*내용 commit 리스트를 파일 하나로 파싱 */
      for (HMDDicLineEntity hmdDicLineEntity : hmdDicLineEntities) {
        content =
            content + hmdDicLineEntity.getRule() + "\t" + hmdDicLineEntity.getCategory()
                + "\n";
      }

      HashMap<String, String> value = new HashMap<String, String>();
      value.put("gitPath", gitPath + "/ta/hmd");
      value.put("workspaceId", hmdDicEntity.getWorkspaceId());
      value.put("name", "admin");
      value.put("email", "mlt@ai");
      value.put("fileName", hmdDicEntity.getId());
      value.put("content", content);
      value.put("messsage", hmdDicEntity.getMessage());
      gs.updateFile(value);

      CommitHistoryEntity commitHistoryEntity = new CommitHistoryEntity();
      commitHistoryEntity.setFileId(hmdDicEntity.getId());
      commitHistoryEntity.setWorkspaceId(hmdDicEntity.getWorkspaceId());
      commitHistoryEntity.setType("HMD");
      commitHistoryService.insertCommit(commitHistoryEntity);

      return new ResponseEntity<>(value, HttpStatus.OK);
    } catch (Exception e) {
      result.put("message", "INSERT_FAILED");
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/hmd^R")
  @RequestMapping(
      value = "/getCommitList",
      method = RequestMethod.POST)
  public ResponseEntity<?> getCommitList(@RequestBody HMDDicEntity param) {
    logger.debug("start : {}", param);

    try {
      GitManager gs = new GitManager();
      gs.setWorkingDirectory(gitPath + "/ta/hmd");

      List<HashMap<String, Object>> result = gs
          .getCommitList(param.getWorkspaceId(), param.getId());

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getCommitList e : " , e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/hmd^R")
  @RequestMapping(
      value = "/getDiff",
      method = RequestMethod.POST)
  public ResponseEntity<?> getDiff(@RequestBody HashMap<String, String> param) {
    logger.debug("start : {}", param);

    try {
      GitManager gs = new GitManager();
      gs.setWorkingDirectory(gitPath + "/ta/hmd");

      String result = gs.diffFile(param.get("workspaceId"), param.get("id"), param.get("source"),
          param.get("target"));
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getDiff e : " , e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/hmd^R")
  @RequestMapping(
      value = "/getFileByCommit",
      method = RequestMethod.POST)
  public ResponseEntity<?> getFileByCommit(@RequestBody HashMap<String, String> param) {
    logger.debug("start : {}", param);

    try {
      GitManager gs = new GitManager();
      gs.setWorkingDirectory(gitPath + "/ta/hmd");

      String result = gs.readFileFromCommit(param.get("workspaceId"), param.get("id"),
          ObjectId.fromString(param.get("source")));

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getFileByCommit e : " , e);
      return new ResponseEntity<>(HttpStatus.FAILED_DEPENDENCY);
    }
  }
}
