package ai.maum.mlt.ta.dnn.repository;

import ai.maum.mlt.ta.dnn.entity.DNNTrainingEntity;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface DNNTrainingRepository extends JpaRepository<DNNTrainingEntity, String> {

  List<DNNTrainingEntity> findAllByWorkspaceId(String workspaceId);
}
