package ai.maum.mlt.ta.hmd.entity;

import ai.maum.mlt.common.pagenation.PageParameters;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@Table(name = "MAI_HMD_DIC_LINE")
public class HMDDicLineEntity extends PageParameters implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "HMD_DIC_LINE_GEN")
  @SequenceGenerator(name = "HMD_DIC_LINE_GEN", sequenceName = "MAI_HMD_LINE_SEQ", allocationSize = 5)
  @Column(name = "SEQ", length = 40, nullable = false)
  private Integer seq;

  @Lob
  @Column(name = "RULE")
  private String rule;

  @Column(name = "CATEGORY", length = 40, nullable = false)
  private String category;

  @Column(name = "VERSION_ID", length = 40, nullable = false)
  private String versionId;

  @ManyToOne
  @JoinColumn(name = "VERSION_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private HMDDicEntity hmdDicEntity;
}
