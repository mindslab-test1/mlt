package ai.maum.mlt.ta.xdc.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.itfc.TaGrpcInterfaceManager;
import ai.maum.mlt.ta.xdc.entity.XDCModelEntity;
import ai.maum.mlt.ta.xdc.entity.XdcTestResultEntity;
import ai.maum.mlt.ta.xdc.entity.XdcTestResultSentenceEntity;
import ai.maum.mlt.ta.xdc.entity.XdcTestResultWordEntity;
import ai.maum.mlt.ta.xdc.service.XDCModelService;
import ai.maum.mlt.ta.xdc.service.XDCTestResultService;
import java.io.IOException;
import java.util.Map;
import maum.brain.han.run.HanRun.HanResult;
import maum.brain.han.run.HanRun;
import maum.brain.nlp.Nlp;
import maum.common.LangOuterClass.LangCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("api/ta/xdc/test")
public class XDCTestController {

  static final Logger logger = LoggerFactory.getLogger(XDCTestController.class);

  @Value("${ta.xdc.classify.server.ip}")
  private String ip;
  @Value("${brain-ta.nlp3kor.ip}")
  private String nlp3KorIp;
  @Value("${brain-ta.nlp3kor.port}")
  private int nlp3KorPort;
  @Value("${brain-ta.nlp2eng.ip}")
  private String nlp2EngIp;
  @Value("${brain-ta.nlp2eng.port}")
  private int nlp2EngPort;
  @Value("${ta.xdc.run.file}")
  private String xdcRunFile;
  @Value("${ta.xdc.virtual.python}")
  private String virtualPy;

  @Autowired
  XDCModelService xdcModelService;

  @Autowired
  XDCTestResultService xdcTestResultService;

  @UriRoleDesc(role = "ta/xdc^R")
  @PostMapping("/getModel")
  public ResponseEntity<?> getModel(@RequestBody Map<String, String> map) {
    logger.info("======= call api  [[/api/ta/xdc/test/getModel]] =======");
    try {
      XDCModelEntity entity = xdcModelService
          .getModelByWorkspaceAndName(map.get("workspaceId"), map.get("name"));
      return new ResponseEntity<>(entity, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/xdc^X")
  @PostMapping("/taXdcAnalyzeText")
  public ResponseEntity<?> taXdcAnalyzeText(@RequestBody XDCModelEntity param) {
    logger.info("======= call api  [[/api/ta/xdc/test/taXdcAnalyzeText]] =======");
    HashMap<String, Object> result = new HashMap<>();
    Process process = null;
    String taIp;
    int taPort;
    int port = -1;
    try {
      if (xdcRunFile == null || xdcRunFile.equals("")) {
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
      }
      if (virtualPy == null || virtualPy.equals("")) {
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
      }
      if (param.getXdcKey() == null || param.getXdcKey() == "") {
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
      }
      String[] lines = param.getTextData().split("\n");
      final List<String> lineList = new ArrayList<>();
      Collections.addAll(lineList, lines);
      if ("kor".equals("kor")) {
        taIp = this.nlp3KorIp;
        taPort = this.nlp3KorPort;
      } else {
        // eng2
        taIp = nlp2EngIp;
        taPort = nlp2EngPort;
      }

      port = getAvailablePort();
      TaGrpcInterfaceManager taClient = new TaGrpcInterfaceManager(taIp, taPort);
      TaGrpcInterfaceManager client = new TaGrpcInterfaceManager(ip, port);

      if (port < 0) {
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
      }
      // run python script 띄우기(한국어 기준으로만 작업)
      String modelOption = param.getXdcKey() + "/" + param.getName() + "-kor-" + param.getXdcKey() + ".pt";
      String command = virtualPy + " " + xdcRunFile + " -m " + modelOption + " -p " + port;
      process = runXdcInstance(command);
      if (process == null) {
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
      }
      // 프로세스 띄우고 잠시 대기
      Thread.sleep(3000);

      // Lang은 일단 항상 한국어로만 사용하도록 코딩
      maum.brain.nlp.Nlp.Document taDoc;
      Nlp.InputText.Builder taInput = Nlp.InputText.newBuilder();
      taInput.setLang(LangCode.kor);
      taInput.setText(param.getTextData());
      taInput.setLevel(Nlp.NlpAnalysisLevel.NLP_ANALYSIS_NAMED_ENTITY);
      taInput.setSplitSentence(true);
      List<XdcTestResultEntity> entityList = new ArrayList<>();
      List<XdcTestResultEntity> resultEntities;
      for (String line : lineList) {
        line = line.trim();
        if (line.equals("")) {
          continue;
        }
        taInput.setText(line);
        taDoc = taClient.analyze(taInput.build());
        if (taDoc == null) {
          if (process != null) {
            process.destroy();
          }
          return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        HanRun.HanInputDocument.Builder doc = HanRun.HanInputDocument.newBuilder();
        doc.mergeDocument(taDoc);
        HanRun.HanInputCommon.Builder common = HanRun.HanInputCommon.newBuilder();
        common.setLang(LangCode.kor);
        common.setTopN(1);
        common.setUseAttnOutput(true);
        doc.mergeCommon(common.build());
        HanResult res = client.getClass(doc.build());
        if (res == null) {
          if (process != null) {
            process.destroy();
          }
          return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (res.getClsCount() > 0) {
          XdcTestResultEntity entity = new XdcTestResultEntity();
          entity.setWorkspaceId(param.getWorkspaceId());
          entity.setModel(param.getName());
          entity.setFileGroup("");
          entity.setCreatorId(param.getCreatorId());
          entity.setTextData(line);
          entity.setCategory(res.getClsOrBuilder(0).getLabel());
          entity.setProbability(new BigDecimal(res.getClsOrBuilder(0).getProbability()));
          List<XdcTestResultSentenceEntity> xdcResList = new ArrayList<>();
          for (int i = 0; i < res.getSentAttnsCount(); i++) {
            HanRun.SentenceAttention sent = res.getSentAttns(i);
            List<XdcTestResultWordEntity> wordList = new ArrayList<>();
            XdcTestResultSentenceEntity resultSentenceEntity = new XdcTestResultSentenceEntity();
            resultSentenceEntity.setWorkspaceId(entity.getWorkspaceId());
            resultSentenceEntity.setModel(entity.getModel());
            resultSentenceEntity.setSentence(doc.getDocument().getSentences(i).getText());
            resultSentenceEntity.setWeight(new BigDecimal(sent.getWeight()).setScale(3, RoundingMode.HALF_DOWN));
            for (HanRun.WordAttention word : sent.getWordAttnsList()) {
              XdcTestResultWordEntity wordEntity = new XdcTestResultWordEntity();
              wordEntity.setWord(resultSentenceEntity.getSentence().substring(word.getStart(), word.getEnd()));
              wordEntity.setWeight(new BigDecimal(word.getWeight()).setScale(3, RoundingMode.HALF_DOWN));
              wordEntity.setStart(word.getStart());
              wordEntity.setEnd(word.getEnd());
              wordEntity = xdcTestResultService.insertResultWord(wordEntity);
              wordList.add(wordEntity);
            }
            resultSentenceEntity.setXdcWords(wordList);
            resultSentenceEntity.setCreatorId(entity.getCreatorId());
            resultSentenceEntity = xdcTestResultService.insertResultSentence(resultSentenceEntity);
            xdcResList.add(resultSentenceEntity);
          }
          entity.setXdcSentences(xdcResList);
          entityList.add(entity);
        }
      }
      resultEntities = xdcTestResultService.insertResults(entityList);
      result.put("xdc_result", resultEntities);
      // run python script 죽이기
      if (process != null) {
        process.destroy();
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      // run python script 죽이기
      if (process != null) {
        process.destroy();
      }
      logger.error("taXdcAnalyzeText e : ", e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  private int getAvailablePort() {
    ServerSocket socket = null;
    int port = -1;
    try {
      socket = new ServerSocket(0);
      port = socket.getLocalPort();
    } catch (Exception e) {
      logger.error("getAvailablePort e : ", e);
    } finally {
      if (socket != null) {
        try {
          socket.close();
        } catch (IOException e) {
          logger.error("getAvailablePort e : ", e);
        }
      }
    }
    return port;
  }

  private Process runXdcInstance(String command) {
    Process process = null;
    try {
      // 사용가능한 port 받아오기
      process = Runtime.getRuntime().exec(command);
    } catch (Exception e) {
      logger.error("runXdcInstance e : ", e);
    }
    return process;
  }
}
