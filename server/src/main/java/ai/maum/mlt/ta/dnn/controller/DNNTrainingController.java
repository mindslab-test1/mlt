package ai.maum.mlt.ta.dnn.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.common.auth.entity.WorkspaceEntity;
import ai.maum.mlt.common.auth.service.WorkspaceService;
import ai.maum.mlt.common.util.FileUtils;
import ai.maum.mlt.itfc.TaGrpcInterfaceManager;
import ai.maum.mlt.management.entity.HistoryEntity;
import ai.maum.mlt.management.service.HistoryService;
import ai.maum.mlt.ta.dnn.entity.DNNDicEntity;
import ai.maum.mlt.ta.dnn.entity.DNNDicLineEntity;
import ai.maum.mlt.ta.dnn.entity.DNNModelEntity;
import ai.maum.mlt.ta.dnn.entity.DNNTrainingEntity;
import ai.maum.mlt.ta.dnn.service.DNNDicLineService;
import ai.maum.mlt.ta.dnn.service.DNNDicService;
import ai.maum.mlt.ta.dnn.service.DNNModelService;
import ai.maum.mlt.ta.dnn.service.DNNTrainingService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.protobuf.Empty;
import io.grpc.Metadata;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import maum.brain.cl.ClassifierOuterClass.Model;
import maum.brain.cl.ClassifierOuterClass.ServerState;
import maum.brain.cl.ClassifierOuterClass.ServerStatus;
import maum.brain.cl.ClassifierOuterClass.SetModelResponse;
import maum.brain.cl.train.Cltrainer.ClCategory;
import maum.brain.cl.train.Cltrainer.ClModel;
import maum.brain.cl.train.Cltrainer.ClTrainBinary;
import maum.brain.cl.train.Cltrainer.ClTrainKey;
import maum.brain.cl.train.Cltrainer.ClTrainStatus;
import maum.brain.cl.train.Cltrainer.ClTrainStatusList;
import maum.brain.cl.train.Cltrainer.TextLemma;
import maum.common.LangOuterClass.LangCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/ta/dnn/training")
public class DNNTrainingController {

  static final Logger logger = LoggerFactory.getLogger(DNNTrainingController.class);

  @Value("${ta.train.server.ip}")
  private String taTrainIp;
  @Value("${ta.train.server.port}")
  private int taTrainPort;
  @Value("${brain-ta.brain-cl.ip}")
  private String taBrainIp;
  @Value("${brain-ta.brain-cl.port}")
  private int taBrainPort;
  @Value("${server.protocol}")
  private String serverProtocol;
  @Value("${server.host}")
  private String serverHost;
  @Value("${server.port}")
  private int serverPort;
  @Value("${mlt.api.key}")
  private String apiKey;

  @Value("${ta.file.path}")
  private String taPath;

  @Autowired
  private DNNDicService dnnDicService;

  @Autowired
  private DNNTrainingService dnnTrainingService;

  @Autowired
  private DNNModelService dnnModelService;

  @Autowired
  private DNNDicLineService dnnDicLineService;

  @Autowired
  private WorkspaceService workspaceService;

  @Autowired
  private HistoryService historyService;

  @Autowired
  private FileUtils fileUtils;

  @UriRoleDesc(role = "ta/dnn^R")
  @RequestMapping(
      value = "/getDnnDicAllList",
      method = RequestMethod.POST
  )
  public ResponseEntity<?> getDnnDicAllList(@RequestBody DNNDicEntity dnnDicEntity) {
    logger.info("======= call api  [[/api/ta/dnn/training/getDnnDicAllList]] =======");
    try {
      List<DNNDicEntity> dnnDicEntities = dnnDicService
          .getDnnDicAllList(dnnDicEntity.getWorkspaceId());
      return new ResponseEntity<>(dnnDicEntities, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/dnn^R")
  @RequestMapping(
      value = "/getDnnDicByName",
      method = RequestMethod.POST
  )
  public ResponseEntity<?> getDnnDicByName(@RequestBody DNNDicEntity dnnDicEntity) {
    logger.info("======= call api  [[/api/ta/dnn/training/getDnnDicByName]] =======");
    try {
      DNNDicEntity entity = dnnDicService.getDnnDic(dnnDicEntity.getWorkspaceId(), dnnDicEntity.getName());
      return new ResponseEntity<>(entity, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/dnn^R")
  @RequestMapping(
      value = "/getAllModels",
      method = RequestMethod.POST
  )
  public ResponseEntity<?> getAllModels(@RequestBody DNNTrainingEntity dnnTrainingEntity) {
    logger.info("======= call api  [[/getAllModels]] =======");
    try {
      List<DNNModelEntity> dnnModelEntities = dnnModelService
          .getAllModels(dnnTrainingEntity.getWorkspaceId());
      return new ResponseEntity<>(dnnModelEntities, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/dnn^R")
  @RequestMapping(
      value = "/getModelsByName",
      method = RequestMethod.POST
  )
  public ResponseEntity<?> getModelsByName(@RequestBody DNNTrainingEntity dnnTrainingEntity) {
    logger.info("======= call api  [[/getModelsByName]] =======");
    try {
      List<DNNModelEntity> dnnModelEntities = dnnModelService
          .getModelsByName(dnnTrainingEntity.getWorkspaceId(), dnnTrainingEntity.getName());
      return new ResponseEntity<>(dnnModelEntities, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/dnn^X")
  @RequestMapping(
      value = "/open",
      method = RequestMethod.POST
  )
  public ResponseEntity<?> open(@RequestBody DNNDicEntity dnnDicEntity) {
    logger.info("======= call api  [[/Open]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {

      HistoryEntity historyEntity = new HistoryEntity();
      historyEntity.setWorkspaceId(dnnDicEntity.getWorkspaceId());
      historyEntity.setCode("DNN_TRAINING");
      historyEntity.setMessage("Started");

      historyEntity = historyService.insertHistory(historyEntity);

      WorkspaceEntity workspaceEntity = workspaceService
          .getWorkspace(dnnDicEntity.getWorkspaceId());

      /* dnnDic 으로 정보조회 */
      TaGrpcInterfaceManager client = new TaGrpcInterfaceManager(taTrainIp, taTrainPort);
      ClModel.Builder clModel = ClModel.newBuilder();

      clModel.setModel(dnnDicEntity.getName().replace("_dnn", ""));
      // Lang은 일단 항상 한국어로만 사용하도록 코딩
      // clModel.setLang(LangCode.valueOf(workspaceEntity.getLangCode()));
      clModel.setLang(LangCode.kor);
      clModel.setNodeCount(dnnDicEntity.getNode());
      clModel.setRunCount(dnnDicEntity.getRepeat());
      clModel.setCallbackUrl(
          this.serverProtocol + "://" + this.serverHost + ":" + this.serverPort
              + "/api/ta/dnn/training/"
              + historyEntity.getId() + "/" + historyEntity.getWorkspaceId() + "/update/");
      clModel.setHasLemma(false);

      //git에서 데이터 가져와야함 현재는 디비에서 데이터 가져옴
      List<DNNDicLineEntity> dnnDicLineEntities = dnnDicLineService
          .getAllDicLinesByVersionId(dnnDicEntity.getId());

      List<String> categoryList = dnnDicLineService
          .getAllDistinct(dnnDicEntity.getId());

      for (String category : categoryList) {
        ClCategory.Builder clCategory = ClCategory.newBuilder();
        clCategory.addLabels(category);

        //문장이 단순하게 2개 이하인 경우
        if (dnnDicLineService.getAllDicLines(dnnDicEntity.getId(), category).size() < 2) {
          throw new Exception("There must be at least two sentences in " + category);
        }

        for (DNNDicLineEntity dnnDicLineEntity : dnnDicLineEntities) {
          if (category.equals(dnnDicLineEntity.getCategory())) {

            //sentence null 인 경우, 해당 sentence 제외 시키고 training 시킴
            if (!dnnDicLineEntity.getSentence().trim().equals("")) {
              TextLemma.Builder textLemma = TextLemma.newBuilder();
              textLemma.setText(dnnDicLineEntity.getSentence());
              clCategory.addTextLemmas(textLemma.build());
            }
          }
        }
        //빈 문장을 제외하고, 문장이 2개 이하인 경우
        if (clCategory.getTextLemmasCount() < 2) {
          throw new Exception(
              "Except for empty sentences, there must be two sentences in " + category);
        }
        clModel.addCategories(clCategory.build());
      }

      ClTrainKey trainKey = client.open(clModel.build());
      if (trainKey == null) {
        throw new Exception("Server Error. train engine not started");
      }
      DNNTrainingEntity dnnTrainingEntity = new DNNTrainingEntity();
      dnnTrainingEntity.setDnnKey(trainKey.getTrainId());
      dnnTrainingEntity.setWorkspaceId(dnnDicEntity.getWorkspaceId());
      dnnTrainingEntity = dnnTrainingService.insertTraining(dnnTrainingEntity);

      result.put("resultCode", "SUCCESS");
      result.put("trainingEntity", dnnTrainingEntity);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("open e : " , e);
      result.put("message", e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /*
   id = history.id
   key = apiKey
  */
  @UriRoleDesc(role = "ta/dnn^U")
  @RequestMapping(
      value = "/{id}/{workspaceId}/update/{key}",
      method = RequestMethod.POST
  )
  public void update(
      @PathVariable("key") String key,
      @PathVariable("id") String id,
      @PathVariable("workspaceId") String workspaceId,
      @RequestBody Object data) {
    logger.info("======= call api  [[/update]] =======" + key);
    try {
      HistoryEntity historyEntity = new HistoryEntity();
      HashMap<String, Object> dataMap = (HashMap) data;
      ObjectMapper mapper = new ObjectMapper();

      if (apiKey.equals(key)) {
        HashMap<String, String> dataModelMap = (HashMap) dataMap.get("data");
        historyEntity.setData(dataModelMap.toString());
        historyEntity.setId(id);
        historyEntity.setMessage((String) dataMap.get("message"));
        historyEntity.setEndedAt(new Date());
        historyEntity.setUpdatedAt(new Date());

        historyService.updateHistory(historyEntity);

        if (!dataModelMap.get("result").equals("failed")) {
          DNNModelEntity dnnModelEntity = new DNNModelEntity();
          dnnModelEntity.setDnnBinary(dataModelMap.get("binary"));
          dnnModelEntity.setName(dataModelMap.get("name"));
          dnnModelEntity.setDnnKey(dataModelMap.get("key"));
          dnnModelEntity.setWorkspaceId(workspaceId);
          dnnModelService.insertModel(dnnModelEntity);

          TaGrpcInterfaceManager taTrainClient = new TaGrpcInterfaceManager(taTrainIp, taTrainPort);
          ClTrainKey.Builder clTrainKey = ClTrainKey.newBuilder();
          clTrainKey.setTrainId(dataModelMap.get("key"));
          List<ClTrainBinary> clientBinary = taTrainClient.getBinary(clTrainKey.build());

          fileUtils.mkdirs(fileUtils.getSystemConfPath(taPath + "/" + workspaceId + "/dnn-model"));

          FileOutputStream dnnModelStream = new FileOutputStream(
              fileUtils.getSystemConfPath(
                  taPath + "/" + workspaceId + "/dnn-model/" + dnnModelEntity.getDnnBinary()));
          try {
            for (ClTrainBinary tar : clientBinary) {
              dnnModelStream.write(tar.getTar().toByteArray());
            }
          } finally {
            dnnModelStream.close();
            taTrainClient.close(clTrainKey.build());
            logger.debug("====== DNN getBinary finish");
          }
          WorkspaceEntity workspaceEntity = workspaceService
              .getWorkspace(workspaceId);

          TaGrpcInterfaceManager taBrainClient = new TaGrpcInterfaceManager(taBrainIp, taBrainPort);
          Model.Builder model = Model.newBuilder();
          // Lang은 일단 항상 한국어로만 사용하도록 코딩
          // model.setLang(LangCode.valueOf(workspaceEntity.getLangCode()));
          model.setLang(LangCode.kor);
          model.setModel(dnnModelEntity.getName());

          ServerStatus findServerInitStatus = taBrainClient.ping(model.build());

          if ((findServerInitStatus.getState().equals(ServerState.SERVER_STATE_INITIALIZING))
              || (findServerInitStatus.getState().equals(ServerState.SERVER_STATE_RUNNING))) {
            ServerStatus stopStatus = taBrainClient.stop(model.build());
          }

          File clientBinary2 = new File(
              fileUtils.getSystemConfPath(
                  taPath + "/" + workspaceId + "/dnn-model/" + dnnModelEntity.getDnnBinary()));
          Metadata metadata = new Metadata();
          metadata.put(Metadata.Key.of("in.lang", Metadata.ASCII_STRING_MARSHALLER), String.valueOf(
              LangCode.valueOf(0)));
          metadata.put(Metadata.Key.of("in.filename", Metadata.ASCII_STRING_MARSHALLER),
              clientBinary2.getName());
          metadata.put(Metadata.Key.of("in.model", Metadata.ASCII_STRING_MARSHALLER),
              dnnModelEntity.getName());

          List<SetModelResponse> setModelResponses = taBrainClient
              .setModel(clientBinary2, metadata);
        }
      }
    } catch (Exception e) {
      logger.debug("update e : " + e);
    }
  }

  @UriRoleDesc(role = "ta/dnn^X")
  @RequestMapping(
      value = "/getAllProgress",
      method = RequestMethod.POST
  )
  public ResponseEntity<?> getAllProgress(@RequestBody DNNDicEntity dnnDicEntity) {
    logger.info("======= call api  [[/getAllProgress]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {

      List<DNNTrainingEntity> dnnTrainingEntities = new ArrayList<>();
      TaGrpcInterfaceManager client = new TaGrpcInterfaceManager(taTrainIp, taTrainPort);

      Empty.Builder req = Empty.newBuilder();
      ClTrainStatusList trainStatusList = client.getAllProgress(req.build());

      if (trainStatusList == null) {
        throw new Exception("NOT_STARTED");
      }

      if (trainStatusList != null) {
        for (ClTrainStatus trainStatus : trainStatusList.getClTrainsList()) {
          DNNTrainingEntity dnnTrainingEntity = dnnTrainingService
              .getTraining(trainStatus.getKey());

          if (dnnTrainingEntity != null) {

            dnnTrainingEntity.setDnnKey(trainStatus.getKey());
            dnnTrainingEntity.setName(trainStatus.getModel());
            dnnTrainingEntity.setNode(trainStatus.getNodeCount());
            dnnTrainingEntity.setRepeat(trainStatus.getRunCount());
            dnnTrainingEntity.setRunCur(trainStatus.getRunCur());
            dnnTrainingEntity.setStatus(trainStatus.getResult().toString());
            dnnTrainingEntity.setProgress(trainStatus.getValue() * 100 / trainStatus.getMaximum());
            dnnTrainingEntity.setWorkspaceId(dnnDicEntity.getWorkspaceId());

            if (dnnTrainingEntity.getWorkspaceId().equals(dnnDicEntity.getWorkspaceId())) {
              dnnTrainingEntities.add(dnnTrainingEntity);
            }
          }
        }
      }

      logger.debug("dtList : " + dnnTrainingEntities);
      result.put("dtList", dnnTrainingEntities);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      if (e.getMessage().equals("NOT_STARTED")) {
        result.put("message", e.getMessage());
      }
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/dnn^X")
  @RequestMapping(
      value = "/stop",
      method = RequestMethod.POST
  )
  public ResponseEntity<?> stop(@RequestBody DNNTrainingEntity dnnTrainingEntity) {

    logger.info("======= call api  [[/dnnStop]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {
      TaGrpcInterfaceManager client = new TaGrpcInterfaceManager(taTrainIp, taTrainPort);

      ClTrainKey.Builder req = ClTrainKey.newBuilder();
      req.setTrainId(dnnTrainingEntity.getDnnKey());
      ClTrainStatus trainStatus = client.stop(req.build());
      logger.debug("=============stop==========" + trainStatus);

      dnnTrainingService.deleteTraining(dnnTrainingEntity.getDnnKey());

      return new ResponseEntity<>(trainStatus.getResult(), HttpStatus.OK);
    } catch (Exception e) {
      logger.debug("getProgress e : " + e);
      result.put("message", "FAIL");
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/dnn^D")
  @RequestMapping(
      value = "/deleteModel",
      method = RequestMethod.POST
  )
  public ResponseEntity<?> deleteModel(@RequestBody DNNTrainingEntity dnnTrainingEntity) {
    logger.info("======= call api  [[/deleteModel]] =======");
    try {
      dnnModelService.deleteModel(dnnTrainingEntity.getDnnKey());
      return new ResponseEntity<>(null, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/dnn^D")
  @RequestMapping(
      value = "/deleteModels",
      method = RequestMethod.POST
  )
  public ResponseEntity<?> deleteModels(@RequestBody List<DNNTrainingEntity> dnnTrainingEntities) {
    logger.info("======= call api  [[/deleteModel]] =======");
    try {
      for (DNNTrainingEntity dnnTrainingEntity : dnnTrainingEntities) {
        dnnModelService.deleteModel(dnnTrainingEntity.getDnnKey());
      }
      return new ResponseEntity<>(null, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}
