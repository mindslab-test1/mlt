package ai.maum.mlt.ta.hmd.service;

import ai.maum.mlt.common.util.FileUtils;
import ai.maum.mlt.ta.hmd.entity.HMDCategoryEntity;
import ai.maum.mlt.ta.hmd.entity.HMDDicEntity;
import ai.maum.mlt.ta.hmd.entity.HMDDicLineEntity;
import ai.maum.mlt.ta.hmd.repository.HMDCategoryRepository;
import ai.maum.mlt.ta.hmd.repository.HMDDicLineRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;


@Service
public class HMDCategoryServiceImpl implements HMDCategoryService {

  @Autowired
  HMDCategoryRepository hmdCategoryRepository;

  @Autowired
  HMDDicLineRepository hmdDicLineRepository;

  @Autowired
  private HMDDicLineService hmdDicLineService;

  @Autowired
  private HMDCategoryService hmdCategoryService;

  @Autowired
  private FileUtils fileUtils;

  @Override
  public List<HMDCategoryEntity> getCategoryAllList(HMDDicEntity hmdDicEntity) {
    return hmdCategoryRepository
        .getAllByWorkspaceIdAndHmdDicId(hmdDicEntity.getWorkspaceId(), hmdDicEntity.getId());
  }

  @Override
  public List<HMDCategoryEntity> getCategoryTree(HMDDicEntity hmdDicEntity) {

    List<HMDCategoryEntity> categoryList = getCategoryAllList(hmdDicEntity);
    List<HMDCategoryEntity> rootCategoryEntity = new ArrayList<>();

    for (HMDCategoryEntity hmdCategoryEntity : categoryList) {
      if (hmdCategoryEntity.getParentId() == null) {
        rootCategoryEntity
            .add(getChildCategory(categoryList, hmdCategoryEntity, hmdCategoryEntity.getId()));
      }
    }

    return rootCategoryEntity;
  }

  private HMDCategoryEntity getChildCategory(List<HMDCategoryEntity> categoryList,
      HMDCategoryEntity currentCategoryEntity, String parentCategoryId) {

    for (HMDCategoryEntity hmdCategoryEntity : categoryList) {
      List<HMDCategoryEntity> childrenObject = currentCategoryEntity.getChildren();
      if (parentCategoryId.equals(hmdCategoryEntity.getParentId())) {
        childrenObject
            .add(getChildCategory(categoryList, hmdCategoryEntity, hmdCategoryEntity.getId()));
      }
    }

    return currentCategoryEntity;
  }

  @Override
  public boolean deleteCategory(String id) {
    try {
      hmdCategoryRepository.delete(id);
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  @Override
  public HMDCategoryEntity insertCategory(HMDCategoryEntity hmdCategoryEntity) {
    hmdCategoryEntity.setCreatedAt(new Date());
    hmdCategoryEntity.setUpdatedAt(new Date());

    return hmdCategoryRepository.save(hmdCategoryEntity);
  }

  @Override
  public HMDCategoryEntity getCategory(String workspaceId, String hmdDicId, String name) {
    return hmdCategoryRepository.getByWorkspaceIdAndHmdDicIdAndName(workspaceId, hmdDicId, name);
  }

  @Override
  @Transactional
  public HMDCategoryEntity updateCategory(HMDCategoryEntity hmdCategoryEntity) {
    HMDCategoryEntity result = hmdCategoryRepository.saveAndFlush(hmdCategoryEntity);

    //category dicLine update
    if (hmdCategoryEntity.getOldName() != null) {
      hmdDicLineRepository
          .updateDicLineCategory(hmdCategoryEntity.getName(), hmdCategoryEntity.getOldName());
    }
    return result;
  }

  @Override
  @Transactional
  public void deleteByHmdDicId(HMDCategoryEntity hmdDicEntity) {
    hmdCategoryRepository.deleteByHmdDicId(hmdDicEntity.getHmdDicId());
  }

  @Override
  public HMDCategoryEntity findOneByNameAndHmdDicId(HMDCategoryEntity hmdCategoryEntity) {
    return hmdCategoryRepository
        .findOneByNameAndHmdDicId(hmdCategoryEntity.getName(), hmdCategoryEntity.getHmdDicId());
  }

  @Transactional
  @Override
  public void hmdFileUpload(String hmdDicId, String workspaceId, MultipartFile file)
      throws Exception {
    List<Map> fileObjects = null;
    String rule;
    String category;

    fileObjects = fileUtils.formatFileToObject(file, "\t");

    HMDCategoryEntity hmdCategoryEntity = new HMDCategoryEntity();
    hmdCategoryEntity.setHmdDicId(hmdDicId);
    hmdCategoryService.deleteByHmdDicId(hmdCategoryEntity);

    HMDDicLineEntity hmdDicLineEntity = new HMDDicLineEntity();
    hmdDicLineEntity.setVersionId(hmdDicId);
    hmdDicLineService.deleteByVersionId(hmdDicLineEntity);

    for (Map object : fileObjects) {
      rule = (String) object.get("column1");
      category = (String) object.get("column2");

      HMDCategoryEntity tempHmdCategoryEntity = new HMDCategoryEntity();
      tempHmdCategoryEntity.setName(category);
      tempHmdCategoryEntity.setHmdDicId(hmdDicId);
      tempHmdCategoryEntity.setWorkspaceId(workspaceId);

      if (hmdCategoryService.findOneByNameAndHmdDicId(tempHmdCategoryEntity) == null) {
        hmdCategoryService.insertCategory(tempHmdCategoryEntity);
      }

      HMDDicLineEntity tempHmdDicLineEntity = new HMDDicLineEntity();
      tempHmdDicLineEntity.setRule(rule);
      tempHmdDicLineEntity.setCategory(category);
      tempHmdDicLineEntity.setVersionId(hmdDicId);
      hmdDicLineService.insertLine(tempHmdDicLineEntity);

    }
  }

}
