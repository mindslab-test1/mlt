package ai.maum.mlt.ta.dnn.repository;

import ai.maum.mlt.ta.dnn.entity.DNNDicEntity;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface DNNDicRepository extends JpaRepository<DNNDicEntity, String> {
  List<DNNDicEntity> getAllByWorkspaceIdOrderByNameAsc(String workspaceId);

  @Query(value = "SELECT d "
      + "         FROM DNNDicEntity d "
      + "         WHERE d.workspaceId = :workspaceId "
      + "         ORDER BY d.updatedAt DESC")
  Page<DNNDicEntity> getAllByWorkspaceId(
      @Param("workspaceId") String workspaceId,
      Pageable pageable
  );


  @Query(value = "SELECT d "
      + "         FROM DNNDicEntity d "
      + "         WHERE d.workspaceId = :workspaceId "
      + "         And d.name = :name "
      + "         ORDER BY d.updatedAt DESC")
  DNNDicEntity getDicByWorkspaceIdAndName(
      @Param("workspaceId") String workspaceId,
      @Param("name") String name
  );
}
