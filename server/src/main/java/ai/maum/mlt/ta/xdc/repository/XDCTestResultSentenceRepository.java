package ai.maum.mlt.ta.xdc.repository;

import ai.maum.mlt.ta.xdc.entity.XdcTestResultSentenceEntity;
import ai.maum.mlt.ta.xdc.entity.XdcTestResultWordEntity;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface XDCTestResultSentenceRepository extends JpaRepository<XdcTestResultSentenceEntity, String> {
  @Query(value = "SELECT d "
      + "         FROM XdcTestResultSentenceEntity d " //XdcTestResultSentenceEntity
      + "         WHERE d.id =:id "
  )
  Page<XdcTestResultWordEntity> findAllByResultId(
  @Param("id") String id,
  Pageable pageable
  );

  @Query(value = "SELECT d "
      + "         FROM XdcTestResultSentenceEntity d " //XdcTestResultSentenceEntity
      + "         WHERE d.workspaceId = :workspaceId and d.id =:sentenceId "
  )
  XdcTestResultSentenceEntity findAllByWorkspaceIdAndSentenceId(@Param("workspaceId") String workspaceId, @Param("sentenceId") String sentenceId);
}
