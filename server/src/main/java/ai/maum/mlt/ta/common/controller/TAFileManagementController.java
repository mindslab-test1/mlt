package ai.maum.mlt.ta.common.controller;


import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.common.util.FileUtils;
import ai.maum.mlt.common.file.entity.FileEntity;
import ai.maum.mlt.common.file.entity.FileGroupEntity;
import ai.maum.mlt.common.file.entity.FileGroupRelEntity;
import ai.maum.mlt.common.file.service.FileGroupService;
import ai.maum.mlt.common.file.service.FileService;
import ai.maum.mlt.common.system.SystemCode;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("api/ta/file-manage")
public class TAFileManagementController {

  static final Logger logger = LoggerFactory.getLogger(TAFileManagementController.class);

  @Autowired
  private FileService fileService;

  @Autowired
  private FileGroupService fileGroupService;

  @Autowired
  private FileUtils fileUtils;

  @Value("${ta.file.path}")
  String taPath;

  @Value("${multipart.maxFileSize}")
  int maxFileSize;

  @UriRoleDesc(role = "ta/common^R")
  @RequestMapping(
      value = "/getFileGroups",
      method = RequestMethod.POST)
  public ResponseEntity<?> getFileGroups(@RequestBody FileGroupEntity param) {
    logger.info("======= call api  GET [[/api/ta/getFileGroups]] =======");

    try {
      List<FileGroupEntity> fileGroupEntities = fileGroupService
          .getFileGroupList(SystemCode.FILE_GRP_PPOS_TA, param.getWorkspaceId());

      return new ResponseEntity<>(fileGroupEntities, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getFileGroups e : " , e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/common^C")
  @RequestMapping(
      value = "/insertGroup",
      method = RequestMethod.POST)
  public ResponseEntity<?> insertGroup(@RequestBody FileGroupEntity param) {
    logger.info("======= call api  POST [[/api/ta/insertGroup]] =======");

    try {

      fileGroupService.insertFileGroup(param, SystemCode.FILE_GRP_PPOS_TA);

      return new ResponseEntity<>(HttpStatus.OK);
    } catch (Exception e) {
      logger.error("insertGroup e : " , e);
      return new ResponseEntity<>( HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/common^U")
  @RequestMapping(
      value = "/updateGroup",
      method = RequestMethod.POST)
  public ResponseEntity<?> updateGroup(@RequestBody FileGroupEntity param) {
    logger.info("======= call api  PUT [[/api/ta/updateGroup]] =======");

    try {

      fileGroupService.updateFileGroup(param);

      return new ResponseEntity<>(HttpStatus.OK);
    } catch (Exception e) {
      logger.error("startAnalyze e : " , e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/common^D")
  @RequestMapping(
      value = "/deleteGroup",
      method = RequestMethod.POST)
  public ResponseEntity<?> deleteGroup(@RequestBody FileGroupEntity param) {
    logger.info("======= call api  DELETE [[/api/ta/deleteGroup]] =======");

    try {

      fileGroupService.deleteFileGroup(param.getId());

      return new ResponseEntity<>(HttpStatus.OK);
    } catch (Exception e) {
      logger.error("deleteGroup e : " , e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/common^R")
  @RequestMapping(
      value = "/getGroupFileList",
      method = RequestMethod.POST)
  public ResponseEntity<?> getGroupFileList(@RequestBody FileEntity param) {
    logger.info("======= call api  [[/api/ta/getGroupFileList]] =======");

    try {
      param.setPurpose(SystemCode.FILE_GRP_PPOS_TA);
      Page<FileEntity> pageResult = fileService.getGroupFileList(param);

      return new ResponseEntity<>(pageResult, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getGroupFileList e : " , e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

  }

  @UriRoleDesc(role = "ta/common^R")
  @RequestMapping(
      value = "/getFileList",
      method = RequestMethod.POST)
  public ResponseEntity<?> getFileList(@RequestBody FileEntity param) {
    logger.info("======= call api  [[/api/ta/getFileList]] =======");

    try {
      param.setPurpose(SystemCode.FILE_GRP_PPOS_TA);
      Page<FileEntity> pageResult = fileService.getFileList(param);

      return new ResponseEntity<>(pageResult, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getFileList e : " , e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/common^R")
  @RequestMapping(
      value = "/getFileListWithType",
      method = RequestMethod.POST)
  public ResponseEntity<?> getFileListWithType(@RequestBody FileEntity param) {
    logger.info("======= call api  [[/api/ta/getFileListWithType]] =======");
    try {
      if (param.getPurpose().toUpperCase().equals(SystemCode.FILE_GRP_PPOS_TA_HMD )) {
        param.setPurpose(SystemCode.FILE_GRP_PPOS_TA_HMD);
      } else if (param.getPurpose().toUpperCase().equals(SystemCode.FILE_GRP_PPOS_TA_DNN)) {
        param.setPurpose(SystemCode.FILE_GRP_PPOS_TA_DNN);
      } else if (param.getPurpose().toUpperCase().equals(SystemCode.FILE_GRP_PPOS_TA_XDC)) {
        param.setPurpose(SystemCode.FILE_GRP_PPOS_TA_XDC);
      } else if (param.getPurpose().toUpperCase().equals(SystemCode.FILE_GRP_PPOS_STT_LM)) {
        param.setPurpose(SystemCode.FILE_GRP_PPOS_STT_LM);
      } else if (param.getPurpose().toUpperCase().equals(SystemCode.FILE_GRP_PPOS_STT_AM)) {
        param.setPurpose(SystemCode.FILE_GRP_PPOS_STT_AM);
      }
      List<FileEntity> listResult = fileService.getFiles(param);
      return new ResponseEntity<>(listResult, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getFileListWithType e : " , e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/common^C")
  @RequestMapping(
      value = "/upload-files",
      method = RequestMethod.POST)
  public ResponseEntity<?> insertFile(@RequestParam("workspaceId") String workspaceId,
      @RequestParam("file") MultipartFile file) {
    logger.info("======= call api  [[/api/ta/getSttList]] =======");

    try {

      HashMap<String, Object> resultMap = fileService
          .insertFile(file, workspaceId, taPath, SystemCode.FILE_GRP_PPOS_TA, SystemCode.FILE_EXTENSION_TEXT);

      return new ResponseEntity<>(resultMap, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("insertFile e : " , e);
      HashMap<String, String> map = new HashMap<>();
      map.put("error", e.getMessage());
      return new ResponseEntity<>(map, HttpStatus.INTERNAL_SERVER_ERROR);
    }

  }

  @UriRoleDesc(role = "ta/common^C")
  @RequestMapping(
      value = "/uploadFile",
      method = RequestMethod.POST)
  public ResponseEntity<?> uploadFile(@RequestParam("workspaceId") String workspaceId,
      @RequestParam("type") String type,
      @RequestParam("creatorId") String creatorId,
      @RequestParam("file") MultipartFile file) {
    logger.info("======= call api  [[/api/ta/uploadFile]] =======");
    HashMap<String, Object> resultMap = new HashMap<String, Object>();
    try {
      if (type.toUpperCase().equals(SystemCode.FILE_GRP_PPOS_TA_HMD)) {
        resultMap = fileService
            .insertFileWithoutChecksum(file, workspaceId, taPath, SystemCode.FILE_GRP_PPOS_TA_HMD, SystemCode.FILE_EXTENSION_TEXT, creatorId);
      } else if (type.toUpperCase().equals(SystemCode.FILE_GRP_PPOS_TA_DNN)) {
        resultMap = fileService
            .insertFileWithoutChecksum(file, workspaceId, taPath, SystemCode.FILE_GRP_PPOS_TA_DNN, SystemCode.FILE_EXTENSION_TEXT, creatorId);
      } else if (type.toUpperCase().equals(SystemCode.FILE_GRP_PPOS_TA_XDC)) {
        resultMap = fileService
            .insertFileWithoutChecksum(file, workspaceId, taPath, SystemCode.FILE_GRP_PPOS_TA_XDC, SystemCode.FILE_EXTENSION_TEXT, creatorId);
      } else {
        logger.error("uploadFile failed, not match type ");
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
      return new ResponseEntity<>(resultMap, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("uploadFile e : " , e);
      HashMap<String, String> map = new HashMap<>();
      map.put("error", e.getMessage());
      return new ResponseEntity<>(map, HttpStatus.INTERNAL_SERVER_ERROR);
    }

  }

  @UriRoleDesc(role = "ta/common^R")
  @RequestMapping(
      value = "/download-file/{workspaceId}/{id}",
      method = RequestMethod.GET)
  public ResponseEntity<?> downloadFile(@PathVariable("workspaceId") String workspaceId, @PathVariable("id") String id) throws IOException{
    logger.info("======= call api  [[/api/ta/downloadFile]] =======");

    try{
      String fileName = id + SystemCode.FILE_EXTENSION_TEXT;
      String path = fileUtils.getSystemConfPath(taPath + "/" + workspaceId) + "/" + fileName;
      File file  = new File(path);
      InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
      HttpHeaders httpHeaders = new HttpHeaders();
      httpHeaders.add(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + fileName);
      httpHeaders.add(HttpHeaders.CONTENT_TYPE, "application/octet-stream");

      return new ResponseEntity<>(resource, httpHeaders, HttpStatus.OK);
    }catch (Exception e){
      logger.error("downloadFile e : " , e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/common^D")
  @RequestMapping(
      value = "/deleteFiles",
      method = RequestMethod.POST)
  public ResponseEntity<?> deleteFiles(@RequestBody FileEntity param) {
    logger.info("======= call api  [[/api/ta/deleteFiles]] =======");

    try {

      fileService.deleteFile(param);
      fileUtils.deleteFile(param.getIds(), taPath + param.getWorkspaceId(), SystemCode.FILE_EXTENSION_TEXT);

      return new ResponseEntity<>(HttpStatus.OK);
    } catch (Exception e) {
      logger.error("deleteFiles e : " , e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/common^D")
  @RequestMapping(
      value = "/deleteFileList",
      method = RequestMethod.POST)
  public ResponseEntity<?> deleteFileList(@RequestBody FileEntity param) {
    logger.info("======= call api  [[/api/ta/deleteFileList]] =======");
    HashMap<String, Object> map = new HashMap<String, Object>();
    try {
      map = fileService.deleteFileWithIds(param.getIds());
      fileUtils.deleteFile(param.getIds(), taPath + param.getWorkspaceId(), SystemCode.FILE_EXTENSION_TEXT);

      return new ResponseEntity<>(map, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("deleteFileList e : " , e);
      return new ResponseEntity<>(map, HttpStatus.INTERNAL_SERVER_ERROR);
    }

  }

  @UriRoleDesc(role = "ta/common^C")
  @RequestMapping(
      value = "/include-files",
      method = RequestMethod.POST)
  public ResponseEntity<?> includeFiles(@RequestBody FileEntity fileEntity) {
    logger.info("======= call api  [[/api/ta/includeFiles]] =======");

    try {
      List<FileGroupRelEntity> fileGroupRelEntities = new ArrayList<>();

      for (String id : fileEntity.getIds()) {
        FileGroupRelEntity fileGroupRelEntity = new FileGroupRelEntity();
        fileGroupRelEntity.setFileId(id);
        fileGroupRelEntity.setFileGroupId(fileEntity.getFileGroupId());
        fileGroupRelEntities.add(fileGroupRelEntity);
      }

      fileGroupService.insertFileGroupRel(fileGroupRelEntities);

      return new ResponseEntity<>(HttpStatus.OK);
    } catch (Exception e) {
      logger.error("includeFiles e : " , e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

  }

  @UriRoleDesc(role = "ta/common^D")
  @RequestMapping(
      value = "/exclude-files",
      method = RequestMethod.POST)
  public ResponseEntity<?> excludeFiles(@RequestBody FileEntity fileEntity) {
    logger.info("======= call api  [[/api/ta/getSttList]] =======");

    try {

      List<FileGroupRelEntity> fileGroupRelEntities = new ArrayList<>();

      for (String id : fileEntity.getIds()) {
        FileGroupRelEntity fileGroupRelEntity = new FileGroupRelEntity();
        fileGroupRelEntity.setFileId(id);
        fileGroupRelEntity.setFileGroupId(fileEntity.getFileGroupId());
        fileGroupRelEntities.add(fileGroupRelEntity);
      }

      fileGroupService.deleteFileGroupRel(fileGroupRelEntities);

      return new ResponseEntity<>(HttpStatus.OK);
    } catch (Exception e) {
      logger.error("excludeFiles e : " , e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

  }
}
