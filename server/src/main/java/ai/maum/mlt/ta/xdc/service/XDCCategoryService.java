package ai.maum.mlt.ta.xdc.service;

import ai.maum.mlt.ta.xdc.entity.XDCCategoryEntity;
import java.io.IOException;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface XDCCategoryService {

  List<XDCCategoryEntity> getCategoryTree(String workspaceId, String xdcDicId);

  List<XDCCategoryEntity> getCategoryAllList(String workspaceId, String XdcDicId);

  XDCCategoryEntity getCategory(String workspaceId, String xdcDicId, String name);

  XDCCategoryEntity findOneByNameAndXdcDicId(String name, String xdcDicId);

  XDCCategoryEntity insertCategory(XDCCategoryEntity xdcCategoryEntity);

  XDCCategoryEntity updateCategory(XDCCategoryEntity xdcCategoryEntity);

  boolean deleteCategory(String id);

  void xdcFileUpload(String xdcDicId, String workspaceId, MultipartFile file) throws IOException;
}
