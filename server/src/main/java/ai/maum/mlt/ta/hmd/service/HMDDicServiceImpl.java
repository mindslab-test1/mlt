package ai.maum.mlt.ta.hmd.service;

import ai.maum.mlt.ta.common.entity.ModelManagementEntity;
import ai.maum.mlt.ta.hmd.entity.HMDDicEntity;
import ai.maum.mlt.ta.hmd.repository.HMDDicInfoRepository;
import ai.maum.mlt.ta.hmd.repository.HMDDicRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
public class HMDDicServiceImpl implements HMDDicService {

  @Autowired
  private HMDDicRepository hmdDicRepository;

  @Autowired
  private HMDDicInfoRepository hmdDicInfoRepository;

  @Override
  public List<HMDDicEntity> getHMDDicList(HMDDicEntity hmdDicEntity) {
    return hmdDicRepository.findByWorkspaceIdOrderByNameAsc(hmdDicEntity.getWorkspaceId());
  }

  @Override
  public void updateApplied(HMDDicEntity hmdDicEntity) {
    hmdDicRepository.save(hmdDicEntity);
  }

  @Override
  public Page<HMDDicEntity> getHMDDicPageList(HMDDicEntity hmdDicEntity) {
    return hmdDicRepository.getAllByWorkspaceId(hmdDicEntity.getWorkspaceId(), hmdDicEntity.getPageRequest());
  }

  @Override
  public List<ModelManagementEntity> getHMDDicInfo(ModelManagementEntity modelManagementEntity) {
    return hmdDicInfoRepository.getHMDDicInfo(modelManagementEntity.getWorkspaceId(), modelManagementEntity.getId());
  }
}