package ai.maum.mlt.ta.hmd.service;

import ai.maum.mlt.ta.hmd.entity.HMDCategoryEntity;
import ai.maum.mlt.ta.hmd.entity.HMDDicEntity;
import java.util.List;
import org.springframework.web.multipart.MultipartFile;

public interface HMDCategoryService {

  List<HMDCategoryEntity> getCategoryAllList(HMDDicEntity hmdDicEntity);
  List<HMDCategoryEntity> getCategoryTree(HMDDicEntity hmdDicEntity);

  HMDCategoryEntity insertCategory(HMDCategoryEntity hmdCategoryEntity);
  HMDCategoryEntity getCategory(String workspaceId, String hmdDicId, String name);
  HMDCategoryEntity updateCategory(HMDCategoryEntity hmdCategoryEntity);

  boolean deleteCategory(String id);

  void deleteByHmdDicId(HMDCategoryEntity hmdCategoryEntity);

  HMDCategoryEntity findOneByNameAndHmdDicId(HMDCategoryEntity hmdDicEntity);

  void hmdFileUpload(String hmdDicId, String workspaceId, MultipartFile file) throws Exception;
}
