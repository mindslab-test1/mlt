package ai.maum.mlt.ta.hmd.repository;

import ai.maum.mlt.ta.hmd.entity.HMDResultEntity;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface HMDResultRepository extends JpaRepository<HMDResultEntity, String> {

  Page<HMDResultEntity> findByWorkspaceId(String id, Pageable pageable);

  @Query(value = "SELECT h "
      + "         FROM HMDResultEntity h "
      + "         WHERE h.workspaceId =:workspaceId "
      + "         AND (h.model LIKE CONCAT('%',:model,'%') OR :model IS NULL)"
      + "         AND (h.sentence LIKE CONCAT('%',:sentence,'%') OR :sentence IS NULL)"
      + "         AND (h.category LIKE CONCAT('%',:category,'%') OR :category IS NULL)"
      + "         AND (h.rule LIKE CONCAT('%',:rule,'%') OR :rule IS NULL)"
  )
  Page<HMDResultEntity> findByHmdResultEntity(
      @Param("workspaceId") String workspaceId,
      @Param("model") String model,
      @Param("sentence") String sentence,
      @Param("category") String category,
      @Param("rule") String rule,
      Pageable pageable
  );

  @Query(value = "SELECT h "
      + "         FROM HMDResultEntity h "
      + "         WHERE h.workspaceId =:workspaceId "
      + "         AND (h.model LIKE CONCAT('%',:model,'%') OR :model IS NULL)"
      + "         AND (h.sentence LIKE CONCAT('%',:sentence,'%') OR :sentence IS NULL)"
      + "         AND (h.category LIKE CONCAT('%',:category,'%') OR :category IS NULL)"
      + "         AND (h.rule LIKE CONCAT('%',:rule,'%') OR :rule IS NULL)"
  )
  List<HMDResultEntity> findAllByHmdResultEntity(
      @Param("workspaceId") String workspaceId,
      @Param("model") String model,
      @Param("sentence") String sentence,
      @Param("category") String category,
      @Param("rule") String rule
  );

}
