package ai.maum.mlt.ta.dnn.service;


import ai.maum.mlt.common.util.FileUtils;
import ai.maum.mlt.ta.dnn.entity.DNNCategoryEntity;
import ai.maum.mlt.ta.dnn.entity.DNNDicLineEntity;
import ai.maum.mlt.ta.dnn.repository.DNNCategoryRepository;
import ai.maum.mlt.ta.dnn.repository.DNNDicLineRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class DNNCategoryServiceImpl implements DNNCategoryService {

  @Autowired
  private DNNCategoryRepository dnnCategoryRepository;

  @Autowired
  private DNNDicLineRepository dnnDicLineRepository;

  @Autowired
  private FileUtils fileUtils;

  @Autowired
  private DNNCategoryService dnnCategoryService;

  @Autowired
  private DNNDicLineService dnnDicLineService;

  @Override
  public List<DNNCategoryEntity> getCategoryAllList(String workspaceId, String dnnDicId) {
    return dnnCategoryRepository.getAllByWorkspaceIdAndDnnDicId(workspaceId, dnnDicId);
  }

  @Override
  public List<DNNCategoryEntity> getCategoryTree(String workspaceId, String dnnDicId) {

    List<DNNCategoryEntity> categoryList = getCategoryAllList(workspaceId, dnnDicId);
    List<DNNCategoryEntity> rootCategoryEntity = new ArrayList<>();

    for (DNNCategoryEntity dnnCategoryEntity : categoryList) {
      if (dnnCategoryEntity.getParentId() == null) {
        rootCategoryEntity
            .add(getChildCategory(categoryList, dnnCategoryEntity, dnnCategoryEntity.getId()));
      }
    }

    return rootCategoryEntity;
  }

  private DNNCategoryEntity getChildCategory(List<DNNCategoryEntity> categoryList,
      DNNCategoryEntity currentCategoryEntity, String parentCategoryId) {

    for (DNNCategoryEntity dnnCategoryEntity : categoryList) {
      List<DNNCategoryEntity> childrenObject = currentCategoryEntity.getChildren();
      if (parentCategoryId.equals(dnnCategoryEntity.getParentId())) {
        childrenObject
            .add(getChildCategory(categoryList, dnnCategoryEntity, dnnCategoryEntity.getId()));
      }
    }

    return currentCategoryEntity;
  }

  @Override
  public DNNCategoryEntity getCategory(String workspaceId, String dnnDicId, String name) {
    return this.dnnCategoryRepository
        .getByWorkspaceIdAndDnnDicIdAndName(workspaceId, dnnDicId, name);
  }

  @Override
  public DNNCategoryEntity findOneByNameAndDnnDicId(String name, String dnnDicId) {
    return this.dnnCategoryRepository.findOneByNameAndDnnDicId(name, dnnDicId);
  }

  @Transactional
  @Override
  public DNNCategoryEntity insertCategory(DNNCategoryEntity dnnCategoryEntity) {
    return this.dnnCategoryRepository.save(dnnCategoryEntity);
  }

  @Transactional
  @Override
  public DNNCategoryEntity updateCategory(DNNCategoryEntity dnnCategoryEntity) {
    DNNCategoryEntity result = this.dnnCategoryRepository.saveAndFlush(dnnCategoryEntity);

    //category dicLine update
    if (dnnCategoryEntity.getOldName() != null) {
      this.dnnDicLineRepository
          .updateDicLineCategory(dnnCategoryEntity.getName(), dnnCategoryEntity.getOldName());
    }
    return result;
  }

  @Override
  public boolean deleteCategory(String id) {
    try {
      this.dnnCategoryRepository.delete(id);
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  @Transactional
  @Override
  public void dnnFileUpload(String dnnDicId, String workspaceId, MultipartFile file) throws Exception {
    List<Map> fileObjects = null;
    String sentence;
    String category;

    fileObjects = fileUtils.formatFileToObject(file, "\t");

    this.dnnDicLineRepository.deleteByVersionId(dnnDicId);
    this.dnnCategoryRepository.deleteByDnnDicId(dnnDicId);

    for (Map object : fileObjects) {
      sentence = (String) object.get("column1");
      category = (String) object.get("column2");

      if (dnnCategoryService.findOneByNameAndDnnDicId(category, dnnDicId) == null) {
        DNNCategoryEntity dnnCategoryEntity = new DNNCategoryEntity();
        dnnCategoryEntity.setWorkspaceId(workspaceId);
        dnnCategoryEntity.setDnnDicId(dnnDicId);
        dnnCategoryEntity.setName(category);
        this.dnnCategoryService.insertCategory(dnnCategoryEntity);
      }

      DNNDicLineEntity dnnDicLineEntity = new DNNDicLineEntity();
      dnnDicLineEntity.setSentence(sentence);
      dnnDicLineEntity.setCategory(category);
      dnnDicLineEntity.setVersionId(dnnDicId);
      this.dnnDicLineService.insertLine(dnnDicLineEntity);
    }
  }
}
