package ai.maum.mlt.ta.hmd.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.ta.hmd.entity.HMDDicEntity;
import ai.maum.mlt.ta.hmd.entity.HMDDicLineEntity;
import ai.maum.mlt.ta.hmd.entity.HMDResultEntity;
import ai.maum.mlt.ta.hmd.service.HMDDicLineService;
import ai.maum.mlt.ta.hmd.service.HMDResultService;
import java.util.HashMap;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/ta/hmd/result/")
public class HMDResultController {

  static final Logger logger = LoggerFactory.getLogger(HMDResultController.class);


  @Value("${hmd.train.server.ip}")
  private String ip;

  @Value("${hmd.train.server.port}")
  private int port;

  @Autowired
  private HMDDicLineService hmdDicLineService;

  @Autowired
  private HMDResultService hmdResultService;

  @UriRoleDesc(role = "ta/hmd^R")
  @RequestMapping(
      value = "/getCategoryList",
      method = RequestMethod.POST)
  public ResponseEntity<?> getScheduleList(@RequestBody HMDDicEntity hmdDicEntity) {
    logger.info("======= call api  [[/api/ta/hmd/getCategoryList]] =======");
    try {

      HMDDicLineEntity hmdDicLineEntity = new HMDDicLineEntity();
      hmdDicLineEntity.setVersionId(hmdDicEntity.getId());
      List<HMDDicLineEntity> categoryList = hmdDicLineService.getCategoryList(hmdDicLineEntity);

      HashMap<String, Object> result = new HashMap<>();
      result.put("categoryList", categoryList);

      return new ResponseEntity<>(null, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/hmd^R")
  @RequestMapping(
      value = "/getHmdAnalyzeResult",
      method = RequestMethod.POST)
  public ResponseEntity<?> getHmdAnalyzeResult(@RequestBody HMDResultEntity hmdResultEntity) {

    logger.info("======= call api  [[/api/ta/hmd/result/getHmdAnalyzeResult]] =======");
    try {
      Page<HMDResultEntity> pageResult = hmdResultService.findByHmdResultEntity(hmdResultEntity);

      return new ResponseEntity<>(pageResult, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/hmd^D")
  @RequestMapping(
      value = "/deleteByHmdAnalyzeResultEntity",
      method = RequestMethod.POST)
  public ResponseEntity<?> deleteByHmdResultEntity(@RequestBody HMDResultEntity hmdResultEntity) {

    logger.info("======= call api  [[/api/ta/hmd/result/deleteByHmdAnalyzeResultEntity]] =======");
    try {

      HashMap<String, Object> result = new HashMap<>();
      result.put("deletedData", hmdResultService.deleteByHmdResultEntity(hmdResultEntity));

      return new ResponseEntity<>(result,
          HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }


}
