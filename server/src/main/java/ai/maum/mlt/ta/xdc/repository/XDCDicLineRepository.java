package ai.maum.mlt.ta.xdc.repository;

import ai.maum.mlt.ta.xdc.entity.XDCDicLineEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface XDCDicLineRepository extends JpaRepository<XDCDicLineEntity, String> {

  List<XDCDicLineEntity> getAllByVersionId(String versionId);

  List<XDCDicLineEntity> getAllByVersionIdAndCategoryOrderBySeqDesc(String versionId,
                                                                    String category);

  @Query(value = "SELECT d "
    + "         FROM XDCDicLineEntity d "
    + "         WHERE d.versionId = :versionId "
    + "         AND (d.category = :category OR :category IS NULL)"
    + "         AND (d.sentence LIKE CONCAT('%',:sentence,'%') OR :sentence IS NULL) "
    + "         ORDER BY d.seq DESC")
  Page<XDCDicLineEntity> getAllByVersionIdAndCategoryAndSentenceOrderBySeqDesc(
    @Param("versionId") String versionId,
    @Param("category") String category,
    @Param("sentence") String sentence,
    Pageable pageable
  );

  Page<XDCDicLineEntity> getAllByVersionIdOrderBySeqDesc(String versionId, Pageable pageable);

  @Query("SELECT new ai.maum.mlt.ta.xdc.entity.XDCDicLineEntity(d.sentence, count(d.sentence)) FROM XDCDicLineEntity d WHERE d.versionId = :versionId AND d.category = :category GROUP BY d.sentence having count(d.sentence)>1")
  List<XDCDicLineEntity> getDuplicatesLines(@Param("versionId") String versionId,
                                            @Param("category") String category);

  @Modifying
  @Query("UPDATE XDCDicLineEntity d SET d.category = :category WHERE d.category = :oldName")
  void updateDicLineCategory(@Param("category") String category,
                             @Param("oldName") String oldName);

  @Query("SELECT d1 FROM XDCDicLineEntity d1 WHERE d1.versionId = :versionId AND d1.category = :category AND d1.seq NOT IN (SELECT MIN(d2.seq) as seq FROM XDCDicLineEntity d2 GROUP BY d2.category, d2.sentence)")
  List<XDCDicLineEntity> getDistinctByXDCDicLine(@Param("versionId") String versionId,
                                                 @Param("category") String category);

  @Query("SELECT distinct (d1.category) FROM XDCDicLineEntity d1 WHERE d1.versionId = :versionId")
  List<String> getAllDistinct(@Param("versionId") String versionId);

  @Modifying
  void deleteByVersionId(String versionId);
}
