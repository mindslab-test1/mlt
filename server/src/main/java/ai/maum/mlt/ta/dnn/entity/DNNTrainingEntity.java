package ai.maum.mlt.ta.dnn.entity;

import ai.maum.mlt.common.auth.entity.WorkspaceEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "MAI_DNN_TRAINING")
public class DNNTrainingEntity {

  @Id
  @Column(name = "DNN_KEY", length = 40, nullable = false)
  private String dnnKey;

  @Column(name = "WORKSPACE_ID", length = 40, nullable = false)
  private String workspaceId;

  @ManyToOne
  @JoinColumn(name = "WORKSPACE_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private WorkspaceEntity workspaceEntity;

  @Transient
  private String name;

  @Transient
  private String status;

  @Transient
  private Integer progress;

  @Transient
  private Integer node;

  @Transient
  private Integer repeat;

  @Transient
  private Integer runCur;

  @Transient
  private Integer stopped = 0;
}
