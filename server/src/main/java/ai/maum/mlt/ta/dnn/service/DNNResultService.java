package ai.maum.mlt.ta.dnn.service;

import ai.maum.mlt.ta.dnn.entity.DNNResultEntity;
import java.util.List;
import org.springframework.data.domain.Page;

public interface DNNResultService {

  Page<DNNResultEntity> findByDnnResultsPage(DNNResultEntity dnnResultEntity);

  List<DNNResultEntity> findByDnnResultsList(DNNResultEntity dnnResultEntity);

  void insertResult(DNNResultEntity dnnResultEntity);

  Integer deleteByDnnResult(DNNResultEntity dnnResultEntity);
}
