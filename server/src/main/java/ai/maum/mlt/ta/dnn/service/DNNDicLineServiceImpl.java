package ai.maum.mlt.ta.dnn.service;

import ai.maum.mlt.ta.dnn.entity.DNNDicLineEntity;
import ai.maum.mlt.ta.dnn.repository.DNNDicLineRepository;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class DNNDicLineServiceImpl implements DNNDicLineService {

  @Autowired
  private DNNDicLineRepository dnnDicLineRepository;

  @Override
  public List<DNNDicLineEntity> getAllDicLinesByVersionId(String versionId) {
    return this.dnnDicLineRepository.getAllByVersionId(versionId);
  }

  @Override
  public List<DNNDicLineEntity> getAllDicLines(String versionId, String category) {
    return this.dnnDicLineRepository
        .getAllByVersionIdAndCategoryOrderBySeqDesc(versionId, category);
  }

  @Override
  public Page<DNNDicLineEntity> getAllDicLines(String versionId, String category, String sentence,
      Pageable pageable) {
    return this.dnnDicLineRepository
        .getAllByVersionIdAndCategoryAndSentenceOrderBySeqDesc(versionId, category, sentence,
            pageable);
  }

  @Override
  public List<DNNDicLineEntity> getDuplicatesLines(String versionId, String category) {
    return this.dnnDicLineRepository.getDuplicatesLines(versionId, category);
  }

  @Override
  public List<String> getAllDistinct(String versionId) {
    return this.dnnDicLineRepository.getAllDistinct(versionId);
  }

  @Transactional
  @Override
  public DNNDicLineEntity insertLine(DNNDicLineEntity dnnDicLineEntity) {
    return this.dnnDicLineRepository.save(dnnDicLineEntity);
  }

  @Override
  public boolean deleteDuplicatesLines(String versionId, String category) {
    try {
      List<DNNDicLineEntity> dnnDicLineEntities = this.dnnDicLineRepository
          .getDistinctByDNNDicLine(versionId, category);
      this.dnnDicLineRepository.deleteInBatch(dnnDicLineEntities);
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  @Override
  public boolean deleteLine(DNNDicLineEntity dnnDicLineEntity) {
    try {
      this.dnnDicLineRepository.delete(dnnDicLineEntity);
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  @Override
  @Transactional
  public void deleteByVersionId(String versionId) {
    this.dnnDicLineRepository.deleteByVersionId(versionId);
  }

  @Override
  @Transactional
  public void updateDicLineCategory(String category, String oldName) {
    this.dnnDicLineRepository.updateDicLineCategory(category, oldName);
  }
}
