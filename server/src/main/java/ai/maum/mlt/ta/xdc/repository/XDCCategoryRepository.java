package ai.maum.mlt.ta.xdc.repository;

import ai.maum.mlt.ta.xdc.entity.XDCCategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface XDCCategoryRepository extends JpaRepository<XDCCategoryEntity, String> {

  @Query(name = "getAllByWorkspaceIdAndXdcDicId", nativeQuery = true)
  List<XDCCategoryEntity> getAllByWorkspaceIdAndXdcDicId(@Param("workspaceId") String workspaceId,
                                                         @Param("xdcDicId") String xdcDicId);

  XDCCategoryEntity getByWorkspaceIdAndXdcDicIdAndName(String workspaceId, String xdcDIc,
                                                       String name);

  XDCCategoryEntity findOneByNameAndXdcDicId(String name, String xdcDicId);

  void deleteByXdcDicId(String xdcDicId);
}
