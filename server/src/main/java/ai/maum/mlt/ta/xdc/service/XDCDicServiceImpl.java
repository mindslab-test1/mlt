package ai.maum.mlt.ta.xdc.service;

import ai.maum.mlt.ta.xdc.entity.XDCDicEntity;
import ai.maum.mlt.ta.xdc.repository.XDCDicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class XDCDicServiceImpl implements XDCDicService {

  @Autowired
  private XDCDicRepository xdcDicRepository;

  @Override
  public List<XDCDicEntity> getXdcDicAllList(String workspaceId) {
    return this.xdcDicRepository.getAllByWorkspaceIdOrderByNameAsc(workspaceId);
  }

  @Override
  public Page<XDCDicEntity> getXdcDicAllList(XDCDicEntity xdcDicEntity) {
    return xdcDicRepository
      .getAllByWorkspaceId(xdcDicEntity.getWorkspaceId(), xdcDicEntity.getPageRequest());
  }

  @Override
  public XDCDicEntity getXdcDic(String workspace, String name) {
    return xdcDicRepository.getDicByWorkspaceIdAndName(workspace, name);
  }
}
