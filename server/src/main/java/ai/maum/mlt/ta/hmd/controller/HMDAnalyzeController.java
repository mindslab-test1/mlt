package ai.maum.mlt.ta.hmd.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.common.auth.entity.RoleMappingEntity;
import ai.maum.mlt.common.auth.entity.WorkspaceEntity;
import ai.maum.mlt.common.auth.service.WorkspaceService;
import ai.maum.mlt.common.file.entity.FileEntity;
import ai.maum.mlt.common.file.entity.FileGroupEntity;
import ai.maum.mlt.common.file.service.FileGroupService;
import ai.maum.mlt.common.file.service.FileService;
import ai.maum.mlt.common.system.SystemCode;
import ai.maum.mlt.common.util.FileUtils;
import ai.maum.mlt.itfc.TaGrpcInterfaceManager;
import ai.maum.mlt.ta.hmd.entity.HMDDicEntity;
import ai.maum.mlt.ta.hmd.entity.HMDResultEntity;
import ai.maum.mlt.ta.hmd.service.HMDDicService;
import ai.maum.mlt.ta.hmd.service.HMDResultService;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import maum.brain.hmd.Hmd.HmdClassified;
import maum.brain.hmd.Hmd.HmdInputText;
import maum.brain.hmd.Hmd.HmdResultDocument;
import maum.brain.nlp.Nlp.Sentence;
import maum.common.LangOuterClass.LangCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/ta/hmd/analyze/")
public class HMDAnalyzeController {

  static final Logger logger = LoggerFactory.getLogger(HMDAnalyzeController.class);

  @Value("${hmd.train.server.ip}")
  private String ip;

  @Value("${hmd.train.server.port}")
  private int port;

  @Value("${ta.file.path}")
  private String taPath;

  @Autowired
  private FileService fileService;

  @Autowired
  private HMDDicService hmdDicService;

  @Autowired
  private FileGroupService fileGroupService;

  @Autowired
  private HMDResultService hmdResultService;

  @Autowired
  private FileUtils fileUtils;

  @Autowired
  private WorkspaceService workspaceService;

  /*
   * HMD > Analysis 페이지 관련 라우팅
   */
  @UriRoleDesc(role = "ta/hmd^X")
  @RequestMapping(
      value = "/startAnalyze",
      method = RequestMethod.POST)
  public ResponseEntity<?> startAnalyze(@RequestBody HashMap<String, Object> hashMap) {

    logger.info("======= call api  [[/api/ta/hmd/analyze/startAnalyze]] =======");
    try {
      TaGrpcInterfaceManager client = new TaGrpcInterfaceManager(ip, port);
      HmdInputText.Builder hmdInputText = HmdInputText.newBuilder();

      WorkspaceEntity workspaceEntity = workspaceService
          .getWorkspace(hashMap.get("workspaceId").toString());
      // Lang은 일단 항상 한국어로만 사용하도록 코딩
      // hmdInputText.setLang(LangCode.valueOf(workspaceEntity.getLangCode()));
      hmdInputText.setLang(LangCode.kor);
      hmdInputText.setModel(((String) hashMap.get("dicName")).replace("_hmd", ""));

      List<String> ids = new ArrayList<>();
      ids = (List<String>) hashMap.get("fileList");

      List<FileEntity> fileList = fileService.getFileLists(ids);
      int failCount = 0;
      int successCount = 0;

      String totalLine;
      for (FileEntity fileEntity : fileList) {
        totalLine = "";
        String fileName = fileEntity.getId() + SystemCode.FILE_EXTENSION_TEXT;
        String path =
            fileUtils.getSystemConfPath(taPath + "/" + hashMap.get("workspaceId").toString()) + "/"
                + fileName;
        File file = new File(path);

        BufferedReader br;
        try {
          br = new BufferedReader(new FileReader(file));
          String line;
          while ((line = br.readLine()) != null) {
            if (!(line.equals(""))) {
              line = line.trim().replaceAll("([^.])$", "$1.") + '\n';
              totalLine += line;
            }
          }
        } catch (Exception e) {
          logger.error("startAnalyze e : " , e);
        }

        // 파일 리스트를 workspaceId,fileGroupId로 읽어오고 파일의 line들을 읽어 setText메소드를 호출한 후 getClassByText메소드를 반복하여 호출
        hmdInputText.setText(totalLine);
        HmdResultDocument hmdResultDocument = client.getClassByText(hmdInputText.build());
        List<HmdClassified> classifiedList = hmdResultDocument.getClsList();
        List<Sentence> sentenceList = hmdResultDocument.getDocument().getSentencesList();
        for (int i = 0; i < classifiedList.size(); i++) {
          //HMDResult 테이블에 insert
          HMDResultEntity hmdResultEntity = new HMDResultEntity();
          hmdResultEntity.setWorkspaceId(hashMap.get("workspaceId").toString());
          hmdResultEntity.setModel(hashMap.get("dicName").toString());

          String category = hmdResultDocument.getCls(i).getCategory();
          String rule = hmdResultDocument.getCls(i).getPattern();
          hmdResultEntity.setRule(rule);
          hmdResultEntity.setCategory(category);
          hmdResultEntity.setSentence(classifiedList.get(i).getSentence());
          hmdResultService.insertHMDResult(hmdResultEntity);
        }

        for(Sentence sentence : sentenceList) {
          HMDResultEntity hmdResultEntity = new HMDResultEntity();
          boolean resultFlag = false;

          for (int i = 0; i < classifiedList.size(); i++) {
            if (sentence.getText().equals(hmdResultDocument.getCls(i).getSentence())) {
              resultFlag = true;
              break;
            }
          }

          if(!resultFlag) {
            hmdResultEntity.setWorkspaceId(hashMap.get("workspaceId").toString());
            hmdResultEntity.setModel(hashMap.get("dicName").toString());
            hmdResultEntity.setSentence(sentence.getText());
            hmdResultEntity.setRule("Not matched rule.");
            hmdResultEntity.setCategory("Category not found.");
            hmdResultService.insertHMDResult(hmdResultEntity);
            failCount++;
          } else {
            successCount++;
          }
        }
      }

      HashMap<String, Integer> countMap = new HashMap<>();
      countMap.put("fail", failCount);
      countMap.put("success", successCount);

      return new ResponseEntity<>(countMap, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/hmd^R")
  @RequestMapping(
      value = "/getFileGroupList",
      method = RequestMethod.POST)
  public ResponseEntity<?> getFileGroupList(@RequestBody FileGroupEntity param) {
    logger.info("======= call api  [[/api/ta/hmd/analyze/getFileGroupList]] =======");
    try {

      List<FileGroupEntity> fileGroupEntities = fileGroupService
          .getFileGroupList(SystemCode.FILE_GRP_PPOS_TA, param.getWorkspaceId());

      return new ResponseEntity<>(fileGroupEntities, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/hmd^R")
  @RequestMapping(
      value = "/getFileList",
      method = RequestMethod.POST)
  public ResponseEntity<?> getFileList(@RequestBody FileEntity param) {
    logger.info("======= call api  [[/api/ta/hmd/analyze/getFileList]] =======");
    try {
      Page<FileEntity> pageResult = fileService.getGroupFileList(param);
      return new ResponseEntity<>(pageResult, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

  }

  @UriRoleDesc(role = "ta/hmd^R")
  @RequestMapping(
      value = "/getHMDDicList",
      method = RequestMethod.POST)
  public ResponseEntity<?> getHMDDicList(@RequestBody HMDDicEntity hmdDicEntity) {

    logger.info("======= call api  [[/api/ta/hmd/analyze/getHMDDicList]] =======");
    try {

      HashMap<String, Object> result = new HashMap<>();
      result.put("hmdDicList", hmdDicService.getHMDDicList(hmdDicEntity));

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }


}
