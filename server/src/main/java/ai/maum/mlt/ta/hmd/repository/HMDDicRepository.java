package ai.maum.mlt.ta.hmd.repository;

import ai.maum.mlt.ta.hmd.entity.HMDDicEntity;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface HMDDicRepository extends JpaRepository<HMDDicEntity, Integer> {

  List<HMDDicEntity> findByWorkspaceIdOrderByNameAsc(String workspaceId);

  @Query(value = "SELECT h "
      + "         FROM HMDDicEntity h "
      + "         WHERE h.workspaceId = :workspaceId "
      + "         ORDER BY h.updatedAt DESC")
  Page<HMDDicEntity> getAllByWorkspaceId(
      @Param("workspaceId") String workspaceId,
      Pageable pageable
  );

}