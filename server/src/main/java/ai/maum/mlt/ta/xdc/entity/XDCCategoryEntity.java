package ai.maum.mlt.ta.xdc.entity;

import ai.maum.mlt.common.auth.entity.UserEntity;
import ai.maum.mlt.common.auth.entity.WorkspaceEntity;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NamedNativeQuery;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SqlResultSetMapping(
  name = "XDCCategoryMapping",
  classes = @ConstructorResult(
    targetClass = ai.maum.mlt.ta.xdc.entity.XDCCategoryEntity.class,
    columns = {
      @ColumnResult(name = "id", type = String.class),
      @ColumnResult(name = "createdAt", type = Date.class),
      @ColumnResult(name = "creatorId", type = String.class),
      @ColumnResult(name = "xdcDicId", type = String.class),
      @ColumnResult(name = "name", type = String.class),
      @ColumnResult(name = "parentId", type = String.class),
      @ColumnResult(name = "updatedAt", type = Date.class),
      @ColumnResult(name = "updaterId", type = String.class),
      @ColumnResult(name = "workspaceId", type = String.class),
      @ColumnResult(name = "count", type = Long.class)
    }
  )
)
@NamedNativeQuery(name = "getAllByWorkspaceIdAndXdcDicId",
  query =
    "SELECT category.ID as id, category.CREATED_AT as createdAt, category.CREATOR_ID as creatorId, category.XDC_DIC_ID as xdcDicId"
      + ", category.NAME as name, category.PARENT_ID as parentId, category.UPDATED_AT as updatedAt, "
      + "category.UPDATER_ID as updaterId, category.WORKSPACE_ID as workspaceId, "
      + "(SELECT COUNT(dicLine.SEQ) "
      + " FROM MAI_XDC_DIC_LINE dicLine "
      + " WHERE category.NAME = dicLine.CATEGORY and dicLine.VERSION_ID = :xdcDicId) AS count "
      + "FROM MAI_XDC_CATEGORY category "
      + "where category.WORKSPACE_ID = :workspaceId and category.XDC_DIC_ID = :xdcDicId",
  resultSetMapping = "XDCCategoryMapping"
)
@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode
@ToString
@Table(name = "MAI_XDC_CATEGORY")
public class XDCCategoryEntity {

  public XDCCategoryEntity() {
  }

  // Native Query(name="getAllByWorkspaceIdAndXdcDicId") 결과값이 담긴다.
  public XDCCategoryEntity(String id, Date createdAt, String creatorId, String xdcDicId,
                           String name, String parentId, Date updatedAt, String updaterId, String workspaceId,
                           Long count) {
    this.setId(id);
    this.setCreatedAt(createdAt);
    this.setCreatorId(creatorId);
    this.setXdcDicId(xdcDicId);
    this.setName(name);
    this.setParentId(parentId);
    this.setUpdatedAt(updatedAt);
    this.setUpdaterId(updaterId);
    this.setWorkspaceId(workspaceId);
    this.setCount(count);
  }

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid")
  @Column(name = "ID", length = 40, nullable = false)
  private String id;

  @Column(name = "XDC_DIC_ID", length = 40, nullable = false)
  private String xdcDicId;

  @Column(name = "WORKSPACE_ID", length = 40, nullable = false)
  private String workspaceId;

  @Column(name = "PARENT_ID", length = 40)
  private String parentId;

  @Column(name = "NAME", length = 512, nullable = false)
  private String name;

  @Column(name = "CREATOR_ID", length = 40)
  private String creatorId;

  @CreatedDate
  @Column(name = "CREATED_AT")
  private Date createdAt;

  @Column(name = "UPDATER_ID", length = 40)
  private String updaterId;

  @LastModifiedDate
  @Column(name = "UPDATED_AT")
  private Date updatedAt;

  @ManyToOne
  @JoinColumn(name = "CREATOR_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity createUserEntity;

  @ManyToOne
  @JoinColumn(name = "UPDATER_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity updateUserEntity;

  @ManyToOne
  @JoinColumn(name = "XDC_DIC_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private XDCDicEntity xdcDicEntity;

  @ManyToOne
  @JoinColumn(name = "WORKSPACE_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private WorkspaceEntity workspaceEntity;

  @Transient
  private List<ai.maum.mlt.ta.xdc.entity.XDCCategoryEntity> children = new ArrayList<>();

  @Transient
  private String updateState;

  @Transient
  private String oldName;

  @Transient
  private long count;
}
