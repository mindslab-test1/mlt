package ai.maum.mlt.ta.dnn.service;

import ai.maum.mlt.ta.dnn.entity.DNNModelEntity;
import ai.maum.mlt.ta.dnn.repository.DNNModelRepository;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DNNModelServiceImpl implements DNNModelService {

  @Autowired
  private DNNModelRepository dnnModelRepository;

  @Override
  public DNNModelEntity selectModelByKey(String key) {
    return dnnModelRepository.findByDnnKey(key);
  }

  @Override
  public DNNModelEntity getModelByWorkspaceAndName(String workspaceId, String name) {
    return dnnModelRepository.findTopByWorkspaceIdAndNameOrderByCreatedAtDesc(workspaceId, name);
  }

  @Override
  public List<DNNModelEntity> getAllModels(String workspaceId) {
    return this.dnnModelRepository.findAllByWorkspaceId(workspaceId);
  }

  @Override
  public List<DNNModelEntity> getModelsByName(String workspaceId, String name) {
    return this.dnnModelRepository.findAllByWorkspaceIdAndName(workspaceId, name);
  }

  @Override
  public DNNModelEntity insertModel(DNNModelEntity dnnModelEntity) {
    dnnModelEntity.setCreatedAt(new Date());
    return this.dnnModelRepository.save(dnnModelEntity);
  }

  @Override
  public void deleteModel(String dnnKey) {
    this.dnnModelRepository.delete(dnnKey);
  }
}
