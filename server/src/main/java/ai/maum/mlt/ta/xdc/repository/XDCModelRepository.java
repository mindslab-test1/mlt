package ai.maum.mlt.ta.xdc.repository;

import ai.maum.mlt.ta.xdc.entity.XDCModelEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;


@Repository
@Transactional
public interface XDCModelRepository extends JpaRepository<XDCModelEntity, String> {
  XDCModelEntity findByXdcKey(String xdcKey);
  List<XDCModelEntity> findAllByWorkspaceId(String workspaceId);
  List<XDCModelEntity> findAllByWorkspaceIdAndName(String workspaceId, String name);
  XDCModelEntity findTopByWorkspaceIdAndNameOrderByCreatedAtDesc(String workspaceId, String name);
}
