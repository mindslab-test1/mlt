package ai.maum.mlt.ta.common.repository;

import ai.maum.mlt.ta.common.entity.ModelManagementEntity;
import ai.maum.mlt.ta.dnn.entity.DNNDicEntity;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface DnnR extends JpaRepository<DNNDicEntity, String> {
  void deleteById(String id);
}
