package ai.maum.mlt.ta.hmd.service;

import ai.maum.mlt.ta.hmd.entity.HMDResultEntity;
import java.util.List;
import org.springframework.data.domain.Page;

public interface HMDResultService {

  HMDResultEntity insertHMDResult(HMDResultEntity hmdResultEntity);

  Page<HMDResultEntity> getHMDResultList(HMDResultEntity hmdResultEntity);

  Page<HMDResultEntity> findByHmdResultEntity(HMDResultEntity hmdResultEntity);

  Integer deleteByHmdResultEntity(HMDResultEntity hmdResultEntity);
}
