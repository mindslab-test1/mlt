package ai.maum.mlt.ta.dnn.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.ta.dnn.entity.DNNResultEntity;
import ai.maum.mlt.ta.dnn.service.DNNResultService;
import java.util.HashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/ta/dnn/result")
public class DNNResultController {

  static final Logger logger = LoggerFactory.getLogger(DNNResultController.class);

  @Autowired
  DNNResultService dnnResultService;

  @UriRoleDesc(role = "ta/dnn^R")
  @RequestMapping(
      value = "/getAllDnnResults",
      method = RequestMethod.POST
  )
  public ResponseEntity<?> getAllDnnResults(@RequestBody DNNResultEntity dnnResultEntity) {
    logger.info("======= call api  [[/api/ta/dnn/result/getAllDnnResults]] =======");
    try {
      Page<DNNResultEntity> dnnResultEntities = dnnResultService
          .findByDnnResultsPage(dnnResultEntity);
      return new ResponseEntity<>(dnnResultEntities, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/dnn^D")
  @RequestMapping(
      value = "/deleteByDnnResult",
      method = RequestMethod.POST)
  public ResponseEntity<?> deleteByDnnResult(@RequestBody DNNResultEntity dnnResultEntity) {

    logger.info("======= call api  [[/api/ta/hmd/result/deleteByDnnResult]] =======");
    try {
      HashMap<String, Integer> result = new HashMap<>();
      result.put("deleteCount", dnnResultService.deleteByDnnResult(dnnResultEntity));
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }


}
