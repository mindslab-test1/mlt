package ai.maum.mlt.ta.dnn.repository;

import ai.maum.mlt.ta.dnn.entity.DNNResultEntity;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface DNNResultRepository extends JpaRepository<DNNResultEntity, String> {

  @Query(value = "SELECT d "
      + "         FROM DNNResultEntity d "
      + "         WHERE d.workspaceId =:workspaceId "
      + "         AND (d.model LIKE CONCAT('%',:model,'%') OR :model IS NULL)"
      + "         AND (d.fileGroup LIKE CONCAT('%',:fileGroup,'%') OR :fileGroup IS NULL)"
      + "         AND (d.sentence LIKE CONCAT('%',:sentence,'%') OR :sentence IS NULL)"
      + "         AND (d.category LIKE CONCAT('%',:category,'%') OR :category IS NULL)"
  )
  Page<DNNResultEntity> findByDnnResultsPage(
      @Param("workspaceId") String workspaceId,
      @Param("model") String model,
      @Param("fileGroup") String fileGroup,
      @Param("sentence") String sentence,
      @Param("category") String category,
      Pageable pageable
  );


  @Query(value = "SELECT d "
      + "         FROM DNNResultEntity d "
      + "         WHERE d.workspaceId =:workspaceId "
      + "         AND (d.model LIKE CONCAT('%',:model,'%') OR :model IS NULL)"
      + "         AND (d.fileGroup LIKE CONCAT('%',:fileGroup,'%') OR :fileGroup IS NULL)"
      + "         AND (d.sentence LIKE CONCAT('%',:sentence,'%') OR :sentence IS NULL)"
      + "         AND (d.category LIKE CONCAT('%',:category,'%') OR :category IS NULL)"
      + "         ORDER BY d.updatedAt DESC"
  )
  List<DNNResultEntity> findByDnnResultsList(
      @Param("workspaceId") String workspaceId,
      @Param("model") String model,
      @Param("fileGroup") String fileGroup,
      @Param("sentence") String sentence,
      @Param("category") String category);

}
