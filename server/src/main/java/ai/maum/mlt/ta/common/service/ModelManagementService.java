package ai.maum.mlt.ta.common.service;

import ai.maum.mlt.ta.common.entity.ModelManagementEntity;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ModelManagementService {

  void insertHmdorDnn(ModelManagementEntity modelManagementEntity);
  ModelManagementEntity insertTaModel(ModelManagementEntity modelManagementEntity);
  Page<ModelManagementEntity> getHmdorDnn(ModelManagementEntity modelManagementEntity);
  ModelManagementEntity getHmdOrDnnModel(ModelManagementEntity modelManagementEntity);
  Page<ModelManagementEntity> getTaModelPage(ModelManagementEntity modelManagementEntity);
  ModelManagementEntity getTaModel(ModelManagementEntity modelManagementEntity);
  List<ModelManagementEntity> deleteSelectedModels(List<ModelManagementEntity> modelManagementEntity);
  void updateSelectedModels(ModelManagementEntity modelManagementEntity);
  Integer getCountByWorkspaceIdandName(ModelManagementEntity modelManagementEntity);
}
