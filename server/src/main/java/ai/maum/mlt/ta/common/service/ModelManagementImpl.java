package ai.maum.mlt.ta.common.service;

import ai.maum.mlt.ta.common.entity.ModelManagementEntity;
import ai.maum.mlt.ta.common.repository.DnnR;
import ai.maum.mlt.ta.common.repository.HmdR;
import ai.maum.mlt.ta.common.repository.ModelManagementRepository;
import ai.maum.mlt.ta.common.repository.XdcR;
import ai.maum.mlt.ta.dnn.entity.DNNDicEntity;
import ai.maum.mlt.ta.dnn.repository.DNNCategoryRepository;
import ai.maum.mlt.ta.dnn.repository.DNNDicLineRepository;
import ai.maum.mlt.ta.hmd.entity.HMDDicEntity;

import ai.maum.mlt.ta.hmd.repository.HMDCategoryRepository;
import ai.maum.mlt.ta.hmd.repository.HMDDicLineRepository;
import ai.maum.mlt.ta.xdc.repository.XDCCategoryRepository;
import ai.maum.mlt.ta.xdc.repository.XDCDicLineRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.transaction.Transactional;

import ai.maum.mlt.ta.xdc.entity.XDCDicEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class ModelManagementImpl implements ModelManagementService {

  static final Logger logger = LoggerFactory.getLogger(ModelManagementImpl.class);

  @Autowired
  private ModelManagementRepository modelManagementRepository;

  @Autowired
  private DnnR dnnR;

  @Autowired
  private HmdR hmdR;

  @Autowired
  private XdcR xdcR;

  @Autowired
  private DNNDicLineRepository dnnLine;

  @Autowired
  private DNNCategoryRepository dnnCategory;

  @Autowired
  private HMDDicLineRepository hmdLine;

  @Autowired
  private HMDCategoryRepository hmdCategory;

  @Autowired
  private XDCDicLineRepository xdcLine;

  @Autowired
  private XDCCategoryRepository xdcCategory;

  @Override
  @Transactional
  public void insertHmdorDnn(ModelManagementEntity modelManagementEntity) {
    modelManagementEntity.setCreatedAt(new Date());
    modelManagementEntity.setUpdatedAt(new Date());

    if (modelManagementEntity.isHmdUse() && modelManagementEntity.isDnnUse()) {

      String dnnId = setDnn(modelManagementEntity);
      String hmdId = setHmd(modelManagementEntity);

      modelManagementEntity.setDnnDicId(dnnId);
      modelManagementEntity.setHmdDicId(hmdId);
      modelManagementRepository.save(modelManagementEntity);

    } else if (modelManagementEntity.isDnnUse()) {

      String dnnId = setDnn(modelManagementEntity);
      modelManagementEntity.setDnnDicId(dnnId);
      modelManagementRepository.save(modelManagementEntity);

    } else if (modelManagementEntity.isHmdUse()) {

      String hmdId = setHmd(modelManagementEntity);

      modelManagementEntity.setHmdDicId(hmdId);
      modelManagementRepository.save(modelManagementEntity);

    }
  }

  @Override
  @Transactional
  public ModelManagementEntity insertTaModel(ModelManagementEntity modelManagementEntity) {
    ModelManagementEntity res = null;
    modelManagementEntity.setCreatedAt(new Date());
    modelManagementEntity.setUpdatedAt(new Date());

    if (modelManagementEntity.isDnnUse()) {
      String dnnId = setDnn(modelManagementEntity);
      modelManagementEntity.setDnnDicId(dnnId);
    }
    if (modelManagementEntity.isHmdUse()) {
      String hmdId = setHmd(modelManagementEntity);
      modelManagementEntity.setHmdDicId(hmdId);
    }
    if (modelManagementEntity.isXdcUse()) {
      String xdcId = setXdc(modelManagementEntity);
      modelManagementEntity.setXdcDicId(xdcId);
    }
    if (modelManagementEntity.isHmdUse() || !modelManagementEntity.isDnnUse() || !modelManagementEntity.isXdcUse()) {
      res = modelManagementRepository.save(modelManagementEntity);
    }
    return res;
  }

  @Override
  public Page<ModelManagementEntity> getHmdorDnn(ModelManagementEntity modelManagementEntity) {
    return modelManagementRepository.findByHmdorDnnEntity(modelManagementEntity.getWorkspaceId(),
        modelManagementEntity.getName(), modelManagementEntity.getManDesc(),
        modelManagementEntity.isHmdUse(),
        modelManagementEntity.getPageRequest());
  }

  @Override
  public ModelManagementEntity getHmdOrDnnModel(ModelManagementEntity modelManagementEntity) {
    ModelManagementEntity list = modelManagementRepository.findByHmdorDnnEntityOne(modelManagementEntity.getWorkspaceId(),
        modelManagementEntity.getName(), modelManagementEntity.isDnnUse(),
        modelManagementEntity.isHmdUse());
    return list;
  }

  @Override
  public ModelManagementEntity getTaModel(ModelManagementEntity modelManagementEntity) {
    ModelManagementEntity list = modelManagementRepository.findByHmdOrDnnOrXdcEntityOne(modelManagementEntity.getWorkspaceId(),
      modelManagementEntity.getName(), modelManagementEntity.isDnnUse(),
      modelManagementEntity.isHmdUse(), modelManagementEntity.isXdcUse());
    return list;
  }

  @Override
  public Page<ModelManagementEntity> getTaModelPage (ModelManagementEntity modelManagementEntity) {
    return modelManagementRepository.findByHmdOrDnnOrXdcEntity(modelManagementEntity.getWorkspaceId(),
      modelManagementEntity.getName(), modelManagementEntity.isDnnUse(),
      modelManagementEntity.isHmdUse(), modelManagementEntity.isXdcUse(), modelManagementEntity.getPageRequest());
  }

  @Override
  @Transactional
  public List<ModelManagementEntity> deleteSelectedModels(List<ModelManagementEntity> modelManagementEntities) {

    List<ModelManagementEntity> result = new ArrayList<>();
    for (ModelManagementEntity model : modelManagementEntities) {
      // maum-admin 에선 dnn, hmd, xdc 모두 개별적으로 생성하며 관리되므로 주석처리함(191004)
      /*if (model.getHmdDicId() != null && model.getDnnDicId() != null) {
        dnnLine.deleteByVersionId(model.getDnnDicId());
        dnnCategory.deleteByDnnDicId(model.getDnnDicId());
        dnnR.deleteById(model.getDnnDicId());
        dnnR.deleteById(model.getDnnDicId());
        hmdLine.deleteByVersionId(model.getHmdDicId());
        hmdCategory.deleteByHmdDicId(model.getHmdDicId());
        hmdR.deleteById(model.getHmdDicId());
        modelManagementRepository.deleteById(model.getId());
      } else */
      if (model.getHmdDicId() != null) {
        hmdLine.deleteByVersionId(model.getHmdDicId());
        hmdCategory.deleteByHmdDicId(model.getHmdDicId());
        hmdR.deleteById(model.getHmdDicId());
        modelManagementRepository.deleteById(model.getId());
      } else if (model.getDnnDicId() != null) {
        dnnLine.deleteByVersionId(model.getDnnDicId());
        dnnCategory.deleteByDnnDicId(model.getDnnDicId());
        dnnR.deleteById(model.getDnnDicId());
        modelManagementRepository.deleteById(model.getId());
      } else if (model.getXdcDicId() != null) {
        model = modelManagementRepository.getOne(model.getId());
        if (modelManagementRepository.deleteById(model.getId()) > 0) {
          xdcLine.deleteByVersionId(model.getXdcDicId());
          xdcCategory.deleteByXdcDicId(model.getXdcDicId());
          xdcR.deleteById(model.getXdcDicId());
        }
        result.add(model);
      }
    }
    return result;
  }

  @Override
  @Transactional
  public void updateSelectedModels(ModelManagementEntity model) {

    if (!model.getHmdDicId().equals("") && !model.getDnnDicId().equals("")) {
      if (model.isHmdUse() && model.isDnnUse()) {
        updateModel(model, "");
      } else if (model.isHmdUse()) {
        dnnR.deleteById(model.getDnnDicId());
        updateModel(model, "");

      } else if (model.isDnnUse()) {
        hmdR.deleteById(model.getHmdDicId());
        updateModel(model, "");

      }
    } else if (!model.getHmdDicId().equals("") && model.getDnnDicId().equals("")) {
      if (model.isHmdUse() && model.isDnnUse()) {
        String dnnId = setDnn(model);
        updateModel(model, dnnId);
      } else if (model.isHmdUse()) {
        updateModel(model, "");
      } else if (model.isDnnUse()) {
        hmdR.deleteById(model.getHmdDicId());
        String DnnId = setDnn(model);
        updateModel(model, DnnId);

      }
    } else if (model.getHmdDicId().equals("") && !model.getDnnDicId().equals("")) {
      if (model.isHmdUse() && model.isDnnUse()) {
        String hmdId = setHmd(model);
        updateModel(model, hmdId);
      } else if (model.isHmdUse()) {
        dnnR.deleteById(model.getDnnDicId());
        String hmdId = setHmd(model);
        updateModel(model, hmdId);

      } else if (model.isDnnUse()) {
        updateModel(model, "");
      }
    }
  }

  @Override
  public Integer getCountByWorkspaceIdandName(ModelManagementEntity model) {
    return modelManagementRepository.countByNameAndWorkspaceId(model.getName(), model.getWorkspaceId());
  }


  private String setHmd(ModelManagementEntity mme) {
    HMDDicEntity hmdDicEntity = new HMDDicEntity();
    hmdDicEntity.setCreatedAt(new Date());
    hmdDicEntity.setCreatorId(mme.getCreatorId());
    if (mme.getName() == null) {
      hmdDicEntity
          .setName(modelManagementRepository.findById(mme.getId()).getName());
    } else if (mme.getName() != null) {
      hmdDicEntity.setName(mme.getName());
    }
    hmdDicEntity.setUpdatedAt(new Date());
    hmdDicEntity.setUpdaterId(mme.getUpdaterId());
    hmdDicEntity.setWorkspaceId(mme.getWorkspaceId());

    hmdDicEntity = hmdR.save(hmdDicEntity);

    return hmdDicEntity.getId();
  }

  private String setDnn(ModelManagementEntity mme) {
    DNNDicEntity dnnDicEntity = new DNNDicEntity();
    dnnDicEntity.setCreatedAt(new Date());
    dnnDicEntity.setCreatorId(mme.getCreatorId());
    if (mme.getName() == null) {
      dnnDicEntity.setName(modelManagementRepository.findById(mme.getId()).getName());
    } else if (mme.getName() != null) {
      dnnDicEntity.setName(mme.getName());
    }
    dnnDicEntity.setUpdatedAt(new Date());
    dnnDicEntity.setUpdaterId(mme.getUpdaterId());
    dnnDicEntity.setWorkspaceId(mme.getWorkspaceId());

    dnnDicEntity = dnnR.save(dnnDicEntity);

    return dnnDicEntity.getId();
  }

  private String setXdc(ModelManagementEntity mme) {
    XDCDicEntity xdcDicEntity = new XDCDicEntity();
    xdcDicEntity.setCreatedAt(new Date());
    xdcDicEntity.setCreatorId(mme.getCreatorId());
    if (mme.getName() == null) {
      xdcDicEntity.setName(modelManagementRepository.findById(mme.getId()).getName());
    } else if (mme.getName() != null) {
      xdcDicEntity.setName(mme.getName());
    }
    xdcDicEntity.setUpdatedAt(new Date());
    xdcDicEntity.setUpdaterId(mme.getUpdaterId());
    xdcDicEntity.setWorkspaceId(mme.getWorkspaceId());

    xdcDicEntity = xdcR.save(xdcDicEntity);

    return xdcDicEntity.getId();
  }



  private void updateModel(ModelManagementEntity mme, String id) {

    if (mme.getDnnDicId().equals("") && mme.isDnnUse()) {
      mme.setDnnDicId(id);
    }

    if (mme.getHmdDicId().equals("") && mme.isHmdUse()) {
      mme.setHmdDicId(id);
    }

    if (!mme.isDnnUse()) {
      mme.setDnnDicId("");
    }

    if (!mme.isHmdUse()) {
      mme.setHmdDicId("");
    }

    modelManagementRepository
        .updateData(new Date(), mme.getUpdaterId(), mme.getId(), mme.getDnnDicId()
            , mme.isDnnUse(), mme.getHmdDicId(), mme.isHmdUse(), mme.getManDesc());
  }
}
