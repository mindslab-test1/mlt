package ai.maum.mlt.ta.xdc.service;

import ai.maum.mlt.ta.xdc.entity.XDCTrainingEntity;
import ai.maum.mlt.ta.xdc.repository.XDCTrainingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class XDCTrainingServiceImpl implements XDCTrainingService {

  @Autowired
  private XDCTrainingRepository xdcTrainingRepository;

  @Override
  public XDCTrainingEntity getTraining(String xdcKey) {
    return this.xdcTrainingRepository.findOne(xdcKey);
  }

  @Override
  public XDCTrainingEntity insertTraining(XDCTrainingEntity xdcModelEntity) {
    return this.xdcTrainingRepository.save(xdcModelEntity);
  }

  @Override
  public void deleteTraining(String xdcKey) {
    this.xdcTrainingRepository.delete(xdcKey);
  }
}
