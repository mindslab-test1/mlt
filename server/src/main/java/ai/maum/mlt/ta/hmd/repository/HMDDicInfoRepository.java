package ai.maum.mlt.ta.hmd.repository;

import ai.maum.mlt.ta.common.entity.ModelManagementEntity;
import ai.maum.mlt.ta.hmd.entity.HMDDicEntity;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface HMDDicInfoRepository extends JpaRepository<ModelManagementEntity, Integer> {

  @Query(value = "SELECT h "
      + "         FROM ModelManagementEntity h "
      + "         WHERE h.workspaceId = :workspaceId "
      + "         AND h.id = :id ")
  List<ModelManagementEntity> getHMDDicInfo(
      @Param("workspaceId") String workspaceId,
      @Param("id") String id
  );

}