package ai.maum.mlt.ta.dnn.entity;

import ai.maum.mlt.common.auth.entity.UserEntity;
import ai.maum.mlt.common.auth.entity.WorkspaceEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NamedNativeQuery;
import org.springframework.data.annotation.LastModifiedDate;

@SqlResultSetMapping(
    name = "DNNCategoryMapping",
    classes = @ConstructorResult(
        targetClass = DNNCategoryEntity.class,
        columns = {
            @ColumnResult(name = "id", type = String.class),
            @ColumnResult(name = "createdAt", type = Date.class),
            @ColumnResult(name = "creatorId", type = String.class),
            @ColumnResult(name = "dnnDicId", type = String.class),
            @ColumnResult(name = "name", type = String.class),
            @ColumnResult(name = "parentId", type = String.class),
            @ColumnResult(name = "updatedAt", type = Date.class),
            @ColumnResult(name = "updaterId", type = String.class),
            @ColumnResult(name = "workspaceId", type = String.class),
            @ColumnResult(name = "count", type = Long.class)
        }
    )
)
@NamedNativeQuery(name = "getAllByWorkspaceIdAndDnnDicId",
    query =
        "SELECT category.ID as id, category.CREATED_AT as createdAt, category.CREATOR_ID as creatorId, category.DNN_DIC_ID as dnnDicId"
            + ", category.NAME as name, category.PARENT_ID as parentId, category.UPDATED_AT as updatedAt, "
            + "category.UPDATER_ID as updaterId, category.WORKSPACE_ID as workspaceId, "
            + "(SELECT COUNT(dicLine.SEQ) "
            + " FROM MAI_DNN_DIC_LINE dicLine "
            + " WHERE category.NAME = dicLine.CATEGORY and dicLine.VERSION_ID = :dnnDicId) AS count "
            + "FROM MAI_DNN_CATEGORY category "
            + "where category.WORKSPACE_ID = :workspaceId and category.DNN_DIC_ID = :dnnDicId",
    resultSetMapping = "DNNCategoryMapping"
)
@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "MAI_DNN_CATEGORY")
public class DNNCategoryEntity {

  public DNNCategoryEntity() {
  }

  // Native Query(name="getAllByWorkspaceIdAndDnnDicId") 결과값이 담긴다.
  public DNNCategoryEntity(String id, Date createdAt, String creatorId, String dnnDicId,
      String name, String parentId, Date updatedAt, String updaterId, String workspaceId,
      Long count) {
    this.setId(id);
    this.setCreatedAt(createdAt);
    this.setCreatorId(creatorId);
    this.setDnnDicId(dnnDicId);
    this.setName(name);
    this.setParentId(parentId);
    this.setUpdatedAt(updatedAt);
    this.setUpdaterId(updaterId);
    this.setWorkspaceId(workspaceId);
    this.setCount(count);
  }

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid")
  @Column(name = "ID", length = 40, nullable = false)
  private String id;

  @Column(name = "DNN_DIC_ID", length = 40, nullable = false)
  private String dnnDicId;

  @Column(name = "WORKSPACE_ID", length = 40, nullable = false)
  private String workspaceId;

  @Column(name = "PARENT_ID", length = 40)
  private String parentId;

  @Column(name = "NAME", length = 512, nullable = false)
  private String name;

  @Column(name = "CREATOR_ID", length = 40)
  private String creatorId;

  @CreatedDate
  @Column(name = "CREATED_AT")
  private Date createdAt;

  @Column(name = "UPDATER_ID", length = 40)
  private String updaterId;

  @LastModifiedDate
  @Column(name = "UPDATED_AT")
  private Date updatedAt;

  @ManyToOne
  @JoinColumn(name = "CREATOR_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity createUserEntity;

  @ManyToOne
  @JoinColumn(name = "UPDATER_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity updateUserEntity;

  @ManyToOne
  @JoinColumn(name = "DNN_DIC_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private DNNDicEntity dnnDicEntity;

  @ManyToOne
  @JoinColumn(name = "WORKSPACE_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private WorkspaceEntity workspaceEntity;

  @Transient
  private List<DNNCategoryEntity> children = new ArrayList<>();

  @Transient
  private String updateState;

  @Transient
  private String oldName;

  @Transient
  private long count;
}
