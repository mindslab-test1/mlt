package ai.maum.mlt.ta.xdc.entity;

import ai.maum.mlt.common.pagenation.PageParameters;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "MAI_XDC_TEST_RESULT_WORD")
public class XdcTestResultWordEntity extends PageParameters implements Serializable {

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid")
  @Column(name = "ID", length = 40, nullable = false)
  private String id;

  @Lob
  @Column(name = "WORD", nullable = false)
  private String word;

  @Column(name = "POS_START")
  private Integer start;

  @Column(name = "POS_END")
  private Integer end;

  @Column(name = "WEIGHT", scale = 16, precision = 17)
  private BigDecimal weight;

  @Transient
  private String workspaceId;

  @Transient
  private String sentenceId;

  public void setWeight(BigDecimal weight) {
    this.weight = weight.setScale(3, BigDecimal.ROUND_HALF_UP);
  }
}
