package ai.maum.mlt.ta.dnn.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.common.git.GitManager;
import ai.maum.mlt.common.version.entity.CommitHistoryEntity;
import ai.maum.mlt.common.version.service.CommitHistoryService;
import ai.maum.mlt.ta.dnn.entity.DNNCategoryEntity;
import ai.maum.mlt.ta.dnn.entity.DNNDicEntity;
import ai.maum.mlt.ta.dnn.entity.DNNDicLineEntity;
import ai.maum.mlt.ta.dnn.service.DNNCategoryService;
import ai.maum.mlt.ta.dnn.service.DNNDicLineService;
import ai.maum.mlt.ta.dnn.service.DNNDicService;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.eclipse.jgit.lib.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("api/ta/dnn/training-data")
public class DNNTrainingDataController {

  static final Logger logger = LoggerFactory.getLogger(DNNTrainingDataController.class);

  @Value("${git.file.path}")
  private String gitPath;

  @Autowired
  private DNNDicService dnnDicService;

  @Autowired
  private DNNCategoryService dnnCategoryService;

  @Autowired
  private DNNDicLineService dnnDicLineService;

  @Autowired
  private CommitHistoryService commitHistoryService;

  @UriRoleDesc(role = "ta/dnn^R")
  @RequestMapping(
      value = "/getDnnDicAllList/{workspaceId}",
      method = RequestMethod.GET)
  public ResponseEntity<?> getDnnDicAllList(
      @PathVariable("workspaceId") final String workspaceId) {
    logger.debug("start workspaceId: {}", workspaceId);
    try {
      return new ResponseEntity<>(dnnDicService.getDnnDicAllList(workspaceId), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/dnn^R")
  @RequestMapping(
      value = "/getDnnCategoryAllList/{workspaceId}/dnnDicId/{dnnDicId}",
      method = RequestMethod.GET)
  public ResponseEntity<?> getDnnCategoryAllList(
      @PathVariable("workspaceId") final String workspaceId,
      @PathVariable("dnnDicId") final String dnnDicId) {
    logger.debug("start workspaceId :{}, dnnDicId :{}", workspaceId, dnnDicId);
    try {
      //List<DNNCategoryEntity> list =
      List<DNNCategoryEntity> list = new ArrayList<>();
      list = dnnCategoryService.getCategoryTree(workspaceId, dnnDicId);
      return new ResponseEntity<>(list,
          HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getDnnCategoryAllList e : " , e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/dnn^C")
  @RequestMapping(
      value = "/insertCategory",
      method = RequestMethod.POST)
  public ResponseEntity<?> insertCategory(@RequestBody DNNCategoryEntity dnnCategoryEntity) {
    logger.debug("start : {}", dnnCategoryEntity);
    HashMap<String, Object> result = new HashMap<>();
    try {
      DNNCategoryEntity duplicateCategoryEntity = isDuplicate(dnnCategoryEntity);
      if (duplicateCategoryEntity == null) {
        dnnCategoryEntity.setCreatedAt(new Date());
        DNNCategoryEntity resultCategoryEntity = dnnCategoryService
            .insertCategory(dnnCategoryEntity);
        result.put("message", "INSERT_SUCCESS");
        result.put("category", resultCategoryEntity);
      } else {
        result.put("message", "INSERT_DUPLICATED");
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      result.put("message", "INSERT_FAILED");
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/dnn^U")
  @RequestMapping(
      value = "/updateCategory",
      method = RequestMethod.POST)
  public ResponseEntity<?> updateCategory(@RequestBody DNNCategoryEntity dnnCategoryEntity) {
    logger.debug("start : {}", dnnCategoryEntity);
    HashMap<String, Object> result = new HashMap<>();
    try {
      DNNCategoryEntity duplicateCategoryEntity = isDuplicate(dnnCategoryEntity);
      if (duplicateCategoryEntity == null) {
        dnnCategoryEntity.setUpdatedAt(new Date());
        DNNCategoryEntity resultCategoryEntity = dnnCategoryService
            .updateCategory(dnnCategoryEntity);

        result.put("category", resultCategoryEntity);
        result.put("message", "UPDATE_SUCCESS");
      } else {
        result.put("message", "UPDATE_DUPLICATED");
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("updateCategory e : " , e);
      result.put("message", "UPDATE_FAILED");
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/dnn^D")
  @RequestMapping(
      value = "/deleteCategory",
      method = RequestMethod.POST)
  public ResponseEntity<?> deleteCategory(@RequestBody DNNCategoryEntity dnnCategoryEntity) {
    logger.debug("start : {}", dnnCategoryEntity);
    HashMap<String, Object> result = new HashMap<>();
    try {
      if (dnnDicLineService
          .getAllDicLines(dnnCategoryEntity.getDnnDicId(), dnnCategoryEntity.getName()).size()
          == 0) {
        if (dnnCategoryService.deleteCategory(dnnCategoryEntity.getId())) {
          result.put("message", "DELETE_SUCCESS");
        } else {
          result.put("message", "DELETE_FAILED");
        }
      } else {
        result.put("message", "SENTENCE_EXISTENCE");
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      result.put("message", "ERROR");
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/dnn^R")
  @RequestMapping(
      value = "/hasLines",
      method = RequestMethod.POST)
  public ResponseEntity<?> hasLines(@RequestBody DNNDicLineEntity dnnDicLineEntity) {
    logger.debug("start : {}", dnnDicLineEntity);
    try {
      boolean result = true;
      if (dnnDicLineService
          .getAllDicLines(dnnDicLineEntity.getVersionId(), dnnDicLineEntity.getCategory()).size()
          == 0) {
        result = false;
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/dnn^R")
  @RequestMapping(
      value = "/getAllDicLines",
      method = RequestMethod.POST)
  public ResponseEntity<?> getAllDicLines(@RequestBody DNNDicLineEntity dnnDicLineEntity) {
    logger.debug("start : {}", dnnDicLineEntity);
    HashMap<String, Object> result = new HashMap<>();
    try {

      Page<DNNDicLineEntity> pageList = dnnDicLineService
          .getAllDicLines(dnnDicLineEntity.getVersionId(), dnnDicLineEntity.getCategory(),
              dnnDicLineEntity.getSentence(), dnnDicLineEntity.getPageRequest());

      List<DNNDicLineEntity> resultList = pageList.getContent();

      result.put("total", pageList.getTotalElements());
      result.put("dicLineList", resultList);
      result.put("message", "SUCCESS");
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      result.put("message", "ERROR");
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/dnn^R")
  @RequestMapping(
      value = "/getDuplicatesLines/{versionId}/category/{category}",
      method = RequestMethod.GET)
  public ResponseEntity<?> getDuplicatesLines(@PathVariable("versionId") String versionId,
      @PathVariable("category") String category) {
    logger.debug("start versionId :{}, category :{}", versionId, category);
    HashMap<String, Object> result = new HashMap<>();
    try {
      List<DNNDicLineEntity> resultEntity = dnnDicLineService
          .getDuplicatesLines(versionId, category);
      if (resultEntity.size() == 0) {
        result.put("resultCode", "NO_DUPLICATION");
      } else {
        result.put("dicLine", resultEntity);
        result.put("resultCode", "SUCCESS");
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      result.put("resultCode", "ERROR");
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/dnn^D")
  @RequestMapping(
      value = "/deleteDuplicatesLines",
      method = RequestMethod.POST)
  public ResponseEntity<?> deleteDuplicatesLines(@RequestBody DNNDicLineEntity dnnDicLineEntity) {
    logger.debug("start : {}", dnnDicLineEntity);

    try {
      boolean result = dnnDicLineService
          .deleteDuplicatesLines(dnnDicLineEntity.getVersionId(), dnnDicLineEntity.getCategory());
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/dnn^C")
  @RequestMapping(
      value = "/insertLine",
      method = RequestMethod.POST)
  public ResponseEntity<?> insertLine(@RequestBody DNNDicLineEntity dnnDicLineEntity) {
    logger.debug("start : {}", dnnDicLineEntity);
    HashMap<String, Object> result = new HashMap<>();
    try {

      DNNDicLineEntity resultEntity = dnnDicLineService.insertLine(dnnDicLineEntity);
      result.put("dicLine", resultEntity);
      result.put("message", "SUCCESS");

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      result.put("message", "ERROR");
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/dnn^D")
  @RequestMapping(
      value = "/deleteLines",
      method = RequestMethod.POST)
  public ResponseEntity<?> deleteLines(@RequestBody List<DNNDicLineEntity> dnnDicLineEntityList) {
    logger.debug("start : {}", dnnDicLineEntityList);
    HashMap<String, String> result = new HashMap<>();
    try {
      for (DNNDicLineEntity dnnDicLineEntity : dnnDicLineEntityList) {
        dnnDicLineService.deleteLine(dnnDicLineEntity);
      }
      result.put("message", "SUCCESS");
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      result.put("message", "ERROR");
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/dnn^C")
  @RequestMapping(
      value = "/upload-files",
      method = RequestMethod.POST)
  public ResponseEntity<?> insertFile(@RequestParam("workspaceId") String workspaceId,
      @RequestParam("data") String dnnDicId,
      @RequestParam("file") MultipartFile file) {
    logger.debug("start workspaceId :{}, data :{}, files: {}", workspaceId, dnnDicId, file);
    try {
      dnnCategoryService.dnnFileUpload(dnnDicId, workspaceId, file);

      HashMap<String, Object> result = new HashMap<>();
      result.put("status", "success");

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/dnn^U")
  @RequestMapping(
      value = "/commit",
      method = RequestMethod.POST)
  public ResponseEntity<?> commit(@RequestBody DNNDicEntity dnnDicEntity) {
    logger.debug("start : {}", dnnDicEntity);
    try {
      GitManager gs = new GitManager();
      String content = "";
      List<DNNDicLineEntity> dnnDicLineEntities = dnnDicLineService
          .getAllDicLinesByVersionId(dnnDicEntity.getId());

    /*내용 commit 리스트를 파일 하나로 파싱 */
      for (DNNDicLineEntity dnnDicLineEntity : dnnDicLineEntities) {
        content =
            content + dnnDicLineEntity.getSentence() + "\t" + dnnDicLineEntity.getCategory()
                + "\n";
      }

      HashMap<String, String> value = new HashMap<String, String>();

      value.put("gitPath", gitPath + "/ta/dnn");
      value.put("workspaceId", dnnDicEntity.getWorkspaceId());
      value.put("name", "admin");
      value.put("email", "mlt@ai");
      value.put("fileName", dnnDicEntity.getId());
      value.put("content", content);
      value.put("messsage", dnnDicEntity.getMessage());
      gs.updateFile(value);

      // 커밋히스토리에 적재
      CommitHistoryEntity entity = new CommitHistoryEntity();
      entity.setFileId(dnnDicEntity.getId());
      entity.setWorkspaceId(dnnDicEntity.getWorkspaceId());
      entity.setType("DNN");
      commitHistoryService.insertCommit(entity);

      return new ResponseEntity<>(null, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/dnn^R")
  @RequestMapping(
      value = "/getCommitList",
      method = RequestMethod.POST)
  public ResponseEntity<?> getCommitList(@RequestBody DNNDicEntity param) {
    logger.debug("start : {}", param);

    try {
      GitManager gs = new GitManager();
      gs.setWorkingDirectory(gitPath + "/ta/dnn");

      List<HashMap<String, Object>> result = gs
          .getCommitList(param.getWorkspaceId(), param.getId());

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getCommitList e : " , e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/dnn^R")
  @RequestMapping(
      value = "/getDiff",
      method = RequestMethod.POST)
  public ResponseEntity<?> getDiff(@RequestBody HashMap<String, String> param) {
    logger.debug("start : {}", param);

    try {
      GitManager gs = new GitManager();
      gs.setWorkingDirectory(gitPath + "/ta/dnn");

      String result = gs.diffFile(param.get("workspaceId"), param.get("id"), param.get("source"),
          param.get("target"));

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getDiff e : " , e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/dnn^R")
  @RequestMapping(
      value = "/getFileByCommit",
      method = RequestMethod.POST)
  public ResponseEntity<?> getFileByCommit(@RequestBody HashMap<String, String> param) {
    logger.debug("start : {}", param);

    try {
      GitManager gs = new GitManager();
      gs.setWorkingDirectory(gitPath + "/ta/dnn");

      String result = gs.readFileFromCommit(param.get("workspaceId"), param.get("id"),
          ObjectId.fromString(param.get("source")));

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getFileByCommit e : " , e);
      return new ResponseEntity<>(HttpStatus.FAILED_DEPENDENCY);
    }
  }

  private DNNCategoryEntity isDuplicate(DNNCategoryEntity dnnCategoryEntity) {
    if (dnnCategoryEntity.getUpdateState() == null) {
      return dnnCategoryService
          .getCategory(dnnCategoryEntity.getWorkspaceId(), dnnCategoryEntity.getDnnDicId(),
              dnnCategoryEntity.getName());
    } else {
      return null;
    }
  }
}
