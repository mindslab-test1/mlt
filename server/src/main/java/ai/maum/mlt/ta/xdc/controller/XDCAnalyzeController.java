package ai.maum.mlt.ta.xdc.controller;


import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.common.auth.service.WorkspaceService;
import ai.maum.mlt.common.file.entity.FileEntity;
import ai.maum.mlt.common.file.entity.FileGroupEntity;
import ai.maum.mlt.common.file.service.FileGroupService;
import ai.maum.mlt.common.file.service.FileService;
import ai.maum.mlt.common.system.SystemCode;
import ai.maum.mlt.common.util.FileUtils;
import ai.maum.mlt.itfc.TaGrpcInterfaceManager;
import ai.maum.mlt.ta.xdc.entity.XDCModelEntity;
import ai.maum.mlt.ta.xdc.entity.XdcTestResultEntity;
import ai.maum.mlt.ta.xdc.entity.XdcTestResultWordEntity;
import ai.maum.mlt.ta.xdc.entity.XdcTestResultSentenceEntity;
import ai.maum.mlt.ta.xdc.service.XDCModelService;
import ai.maum.mlt.ta.xdc.service.XDCTestResultService;
import java.io.IOException;
import java.math.RoundingMode;
import java.net.ServerSocket;
import java.util.Map;
import maum.brain.han.run.HanRun;
import maum.brain.han.run.HanRun.HanResult;
import maum.brain.nlp.Nlp.Document;
import maum.brain.nlp.Nlp.InputText;
import maum.common.LangOuterClass.LangCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("api/ta/xdc/analyze")
public class XDCAnalyzeController {

  @Value("${ta.xdc.classify.server.ip}")
  private String ip;
  @Value("${brain-ta.nlp3kor.ip}")
  private String nlp3KorIp;
  @Value("${brain-ta.nlp3kor.port}")
  private int nlp3KorPort;
  @Value("${brain-ta.nlp2eng.ip}")
  private String nlp2EngIp;
  @Value("${brain-ta.nlp2eng.port}")
  private int nlp2EngPort;
  @Value("${ta.file.path}")
  String taPath;
  @Value("${ta.xdc.run.file}")
  private String xdcRunFile;
  @Value("${ta.xdc.virtual.python}")
  private String virtualPy;

  static final Logger logger = LoggerFactory.getLogger(XDCAnalyzeController.class);

  private static final String WORKSPACEID = "workspaceId";
  private static final String XDCKEY = "xdcKey";

  @Autowired
  XDCModelService xdcModelService;

  @Autowired
  XDCTestResultService xdcTestResultService;

  @Autowired
  FileGroupService fileGroupService;

  @Autowired
  FileService fileService;

  @Autowired
  private FileUtils fileUtils;

  @Autowired
  WorkspaceService workspaceService;

  @UriRoleDesc(role = "ta/xdc^R")
  @PostMapping("/getModel")
  public ResponseEntity<?> getModel(@RequestBody Map<String, String> map) {
    logger.info("======= call api  [[/api/ta/xdc/analyze/getModel]] =======");
    try {
      XDCModelEntity entity = xdcModelService
          .getModelByWorkspaceAndName(map.get(WORKSPACEID), map.get("name"));
      return new ResponseEntity<>(entity, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/xdc^R")
  @PostMapping("/getFileGroups")
  public ResponseEntity<?> getFileGroupList(@RequestBody FileGroupEntity fileGroupEntity) {
    logger.info("======= call api  [[/api/ta/hmd/getFileGroupList]] =======");
    try {
      List<FileGroupEntity> fileGroupEntities = fileGroupService
          .getFileGroupList(SystemCode.FILE_GRP_PPOS_TA, fileGroupEntity.getWorkspaceId());
      return new ResponseEntity<>(fileGroupEntities, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/xdc^R")
  @PostMapping(value = "/getFileList")
  public ResponseEntity<?> getFileList(@RequestBody FileEntity fileEntity) {
    logger.info("======= call api  [[/api/ta/xdc/analyze/getFileList]] =======");
    try {
      Page<FileEntity> pageResult = fileService.getGroupFileList(fileEntity);
      return new ResponseEntity<>(pageResult, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/xdc^X")
  @PostMapping("/startAnalyze")
  public ResponseEntity<?> startAnalyze(@RequestBody Map<String, Object> map) {
    HashMap<String, Object> result = new HashMap<>();
    Process process = null;
    List<XdcTestResultEntity> entityList = new ArrayList<>();
    List<XdcTestResultEntity> resultEntities = new ArrayList<>();
    logger.info("======= call api  [[/api/ta/xdc/analyze/startAnalyze]] =======");
    int port;
    try {
      if (xdcRunFile == null || xdcRunFile.equals("")) {
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
      }
      if (virtualPy == null || virtualPy.equals("")) {
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
      }
      if (!map.containsKey(XDCKEY) || map.get(XDCKEY).equals("")) {
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
      }

      String taIp;
      int taPort;
      if ("kor".equals("kor")) {
        taIp = this.nlp3KorIp;
        taPort = this.nlp3KorPort;
      } else {
        // eng2
        taIp = nlp2EngIp;
        taPort = nlp2EngPort;
      }
      port = getAvailablePort();
      TaGrpcInterfaceManager nlpClient = new TaGrpcInterfaceManager(taIp, taPort);
      TaGrpcInterfaceManager client = new TaGrpcInterfaceManager(ip, port);

      if (port < 0) {
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
      }
      // run python script 띄우기(한국어 기준으로만 작업)
      String modelOption = map.get(XDCKEY).toString() + "/" + map.get("name").toString()
          + "-kor-" + map.get(XDCKEY).toString()  + ".pt";
      String command = virtualPy + " " + xdcRunFile + " -m " + modelOption + " -p " + port;
//      process = runXdcInstance(command);
      process = Runtime.getRuntime().exec(command);
      if (process == null) {
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
      }
      // 프로세스 띄우고 잠시 대기
      Thread.sleep(2000);

      InputText.Builder inputText = InputText.newBuilder();
      FileEntity paramFileEntity = new FileEntity();
      paramFileEntity.setWorkspaceId(map.get(WORKSPACEID).toString());
      paramFileEntity.setPurpose(SystemCode.FILE_GRP_PPOS_TA_XDC);

      List<String> ids = (List<String>) map.get("fileList");
      List<FileEntity> fileList = fileService
          .getFilesWithIds(map.get(WORKSPACEID).toString(), ids);

      Document taDoc;
      for (FileEntity fileEntity : fileList) {
        String fileName = fileEntity.getId() + SystemCode.FILE_EXTENSION_TEXT;
        String path =
            fileUtils.getSystemConfPath(taPath + "/" + map.get(WORKSPACEID).toString()) + "/"
                + fileName;
        File file = new File(path);
        BufferedReader br = new BufferedReader(new FileReader(file));
        String line;

        try {
          while ((line = br.readLine()) != null) {
            if (!line.equals("")) {
              line = line.trim().replaceAll("([^.])$", "$1.");
              inputText.setSplitSentence(true);
              // Lang은 일단 항상 한국어로만 사용하도록 코딩
              inputText.setLang(LangCode.kor);
              inputText.setText(line);
              taDoc = nlpClient.analyze(inputText.build());
              if (taDoc == null) {
                if (process != null) {
                  process.destroy();
                }
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
              }
              HanRun.HanInputDocument.Builder doc = HanRun.HanInputDocument.newBuilder();
              doc.mergeDocument(taDoc);
              HanRun.HanInputCommon.Builder common = HanRun.HanInputCommon.newBuilder();
              common.setLang(LangCode.kor);
              common.setTopN(1);
              common.setUseAttnOutput(true);
              doc.mergeCommon(common.build());
              HanResult res = client.getClass(doc.build());
              if (res == null) {
                if (process != null) {
                  process.destroy();
                }
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
              }
              if (res.getClsCount() > 0) {
                XdcTestResultEntity entity = new XdcTestResultEntity();
                entity.setWorkspaceId(map.get(WORKSPACEID).toString());
                entity.setModel(map.get("name").toString());
                entity.setFileGroup("");
                entity.setCreatorId(map.get("creatorId").toString());
                entity.setTextData(line);
                entity.setCategory(res.getClsOrBuilder(0).getLabel());
                entity.setProbability(new BigDecimal(res.getClsOrBuilder(0).getProbability()));
                List<XdcTestResultSentenceEntity> xdcResList = new ArrayList<>();
                for (int i = 0; i < res.getSentAttnsCount(); i++) {
                  HanRun.SentenceAttention sent = res.getSentAttns(i);
                  List<XdcTestResultWordEntity> wordList = new ArrayList<>();
                  XdcTestResultSentenceEntity resultSentenceEntity = new XdcTestResultSentenceEntity();
                  resultSentenceEntity.setWorkspaceId(entity.getWorkspaceId());
                  resultSentenceEntity.setModel(entity.getModel());
                  resultSentenceEntity.setSentence(doc.getDocument().getSentences(i).getText());
                  resultSentenceEntity.setWeight(
                      new BigDecimal(sent.getWeight()).setScale(3, RoundingMode.HALF_DOWN));
                  for (HanRun.WordAttention word : sent.getWordAttnsList()) {
                    XdcTestResultWordEntity wordEntity = new XdcTestResultWordEntity();
                    wordEntity.setWord(
                        resultSentenceEntity.getSentence().substring(word.getStart(), word.getEnd()));
                    wordEntity.setWeight(
                        new BigDecimal(word.getWeight()).setScale(3, RoundingMode.HALF_DOWN));
                    wordEntity.setStart(word.getStart());
                    wordEntity.setEnd(word.getEnd());
                    wordEntity = xdcTestResultService.insertResultWord(wordEntity);
                    wordList.add(wordEntity);
                  }
                  resultSentenceEntity.setXdcWords(wordList);
                  resultSentenceEntity.setCreatorId(entity.getCreatorId());
                  resultSentenceEntity = xdcTestResultService.insertResultSentence(resultSentenceEntity);
                  xdcResList.add(resultSentenceEntity);
                }
                entity.setXdcSentences(xdcResList);
                entityList.add(entity);
              }
            }
          }
          resultEntities = xdcTestResultService.insertResults(entityList);
          result.put("xdc_result", resultEntities);
          // run python script 죽이기
          if (process != null) {
            process.destroy();
          }
        } catch (Exception e) {
          logger.error("startAnalyze e : ", e);
        } finally {
          br.close();
        }
      }
    } catch (Exception e) {
      logger.error("startAnalyze e : ", e);
      if (process != null) {
        process.destroy();
      }
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  private int getAvailablePort() {
    ServerSocket socket = null;
    int port = -1;
    try {
      socket = new ServerSocket(0);
      port = socket.getLocalPort();
    } catch (Exception e) {
      logger.error("getAvailablePort e : ", e);
    } finally {
      if (socket != null) {
        try {
          socket.close();
        } catch (IOException e) {
          logger.error("getAvailablePort e : ", e);
        }
      }
    }
    return port;
  }

  private Process runXdcInstance(String command) {
    Process process = null;
    try {
      // 사용가능한 port 받아오기
      process = Runtime.getRuntime().exec(command);
    } catch (Exception e) {
      logger.error("runXdcInstance e : ", e);
    }
    return process;
  }
}
