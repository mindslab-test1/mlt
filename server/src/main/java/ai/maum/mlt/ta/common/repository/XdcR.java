package ai.maum.mlt.ta.common.repository;

import ai.maum.mlt.ta.xdc.entity.XDCDicEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface XdcR extends JpaRepository<XDCDicEntity, String> {
  void deleteById(String id);
}
