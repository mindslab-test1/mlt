package ai.maum.mlt.ta.xdc.repository;

import ai.maum.mlt.ta.xdc.entity.XdcTestResultEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface XDCTestResultRepository extends JpaRepository<XdcTestResultEntity, String> {

  @Query(value = "SELECT d "
    + "         FROM XdcTestResultEntity d "
    + "         WHERE d.workspaceId =:workspaceId "
    + "         AND (d.model LIKE CONCAT('%',:model,'%') OR :model IS NULL)"
    + "         AND (d.fileGroup LIKE CONCAT('%',:fileGroup,'%') OR :fileGroup IS NULL)"
    + "         AND (d.textData LIKE CONCAT('%',:textData,'%') OR :textData IS NULL)"
    + "         AND (d.category LIKE CONCAT('%',:category,'%') OR :category IS NULL)"
  )
  Page<XdcTestResultEntity> findByXdcResultsPage(
    @Param("workspaceId") String workspaceId,
    @Param("model") String model,
    @Param("fileGroup") String fileGroup,
    @Param("textData") String textData,
    @Param("category") String category,
    Pageable pageable
  );


  @Query(value = "SELECT d "
    + "         FROM XdcTestResultEntity d "
    + "         WHERE d.workspaceId =:workspaceId "
    + "         AND (d.model LIKE CONCAT('%',:model,'%') OR :model IS NULL)"
    + "         AND (d.fileGroup LIKE CONCAT('%',:fileGroup,'%') OR :fileGroup IS NULL)"
    + "         ORDER BY d.createdAt DESC"
  )
  List<XdcTestResultEntity> findByXdcResultsList(
    @Param("workspaceId") String workspaceId,
    @Param("model") String model,
    @Param("fileGroup") String fileGroup
  );
}
