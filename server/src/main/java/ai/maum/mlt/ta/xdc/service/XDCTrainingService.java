package ai.maum.mlt.ta.xdc.service;

import ai.maum.mlt.ta.xdc.entity.XDCTrainingEntity;

public interface XDCTrainingService {

  XDCTrainingEntity getTraining(String xdcKey);

  XDCTrainingEntity insertTraining(XDCTrainingEntity xdcTrainingEntity);

  void deleteTraining(String xdcKey);
}
