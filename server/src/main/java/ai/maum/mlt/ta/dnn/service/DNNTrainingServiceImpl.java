package ai.maum.mlt.ta.dnn.service;

import ai.maum.mlt.ta.dnn.entity.DNNTrainingEntity;
import ai.maum.mlt.ta.dnn.repository.DNNTrainingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DNNTrainingServiceImpl implements DNNTrainingService {

  @Autowired
  private DNNTrainingRepository dnnTrainingRepository;

  @Override
  public DNNTrainingEntity getTraining(String dnnKey) {
    return this.dnnTrainingRepository.findOne(dnnKey);
  }

  @Override
  public DNNTrainingEntity insertTraining(DNNTrainingEntity dnnModelEntity) {
    return this.dnnTrainingRepository.save(dnnModelEntity);
  }

  @Override
  public void deleteTraining(String dnnKey) {
    this.dnnTrainingRepository.delete(dnnKey);
  }
}
