package ai.maum.mlt.ta.dnn.service;

import ai.maum.mlt.ta.dnn.entity.DNNResultEntity;
import ai.maum.mlt.ta.dnn.repository.DNNResultRepository;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
public class DNNResultServiceImpl implements DNNResultService {

  @Autowired
  private DNNResultRepository dnnResultRepository;

  @Override
  public Page<DNNResultEntity> findByDnnResultsPage(DNNResultEntity dnnResultEntity) {
    return this.dnnResultRepository.findByDnnResultsPage(
        dnnResultEntity.getWorkspaceId(),
        dnnResultEntity.getModel(),
        dnnResultEntity.getFileGroup(),
        dnnResultEntity.getSentence(),
        dnnResultEntity.getCategory(),
        dnnResultEntity.getPageRequest()
    );
  }

  @Override
  public List<DNNResultEntity> findByDnnResultsList(DNNResultEntity dnnResultEntity) {
    return this.dnnResultRepository.findByDnnResultsList(
        dnnResultEntity.getWorkspaceId(),
        dnnResultEntity.getModel(),
        dnnResultEntity.getFileGroup(),
        dnnResultEntity.getSentence(),
        dnnResultEntity.getCategory()
    );
  }

  @Override
  public void insertResult(DNNResultEntity dnnResultEntity) {
    dnnResultEntity.setCreatedAt(new Date());
    dnnResultEntity.setUpdatedAt(new Date());
    this.dnnResultRepository.save(dnnResultEntity);
  }

  @Override
  public Integer deleteByDnnResult(DNNResultEntity dnnResultEntity) {
    List<DNNResultEntity> dnnResultEntities = this.findByDnnResultsList(dnnResultEntity);
    this.dnnResultRepository.deleteInBatch(dnnResultEntities);

    return dnnResultEntities.size();
  }
}
