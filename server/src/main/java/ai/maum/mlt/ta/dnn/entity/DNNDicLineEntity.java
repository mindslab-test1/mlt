package ai.maum.mlt.ta.dnn.entity;

import ai.maum.mlt.common.pagenation.PageParameters;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "MAI_DNN_DIC_LINE")
public class DNNDicLineEntity extends PageParameters {

  public DNNDicLineEntity() {
  }

  public DNNDicLineEntity(String sentence, long duplicateCount) {
    this.sentence = sentence;
    this.duplicateCount = duplicateCount;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DNN_DIC_LINE_GEN")
  @SequenceGenerator(name = "DNN_DIC_LINE_GEN", sequenceName = "MAI_DNN_LINE_SEQ", allocationSize = 5)
  @Column(name = "SEQ", length = 40, nullable = false)
  private Integer seq;

  @Lob
  @Column(name = "SENTENCE")
  private String sentence;

  @Column(name = "CATEGORY", length = 512, nullable = false)
  private String category;

  @Column(name = "VERSION_ID", length = 40, nullable = false)
  private String versionId;

  @ManyToOne
  @JoinColumn(name = "VERSION_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private DNNDicEntity dnnDicEntity;

  @Transient
  private long duplicateCount;

}
