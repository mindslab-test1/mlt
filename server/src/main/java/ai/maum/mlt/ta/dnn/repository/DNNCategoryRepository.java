package ai.maum.mlt.ta.dnn.repository;


import ai.maum.mlt.ta.dnn.entity.DNNCategoryEntity;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface DNNCategoryRepository extends JpaRepository<DNNCategoryEntity, String> {

  @Query(name="getAllByWorkspaceIdAndDnnDicId", nativeQuery = true)
  List<DNNCategoryEntity> getAllByWorkspaceIdAndDnnDicId(@Param("workspaceId") String workspaceId,
      @Param("dnnDicId") String dnnDicId);

  DNNCategoryEntity getByWorkspaceIdAndDnnDicIdAndName(String workspaceId, String dnnDIc,
      String name);

  DNNCategoryEntity findOneByNameAndDnnDicId(String name, String dnnDicId);

  void deleteByDnnDicId(String dnnDicId);
}
