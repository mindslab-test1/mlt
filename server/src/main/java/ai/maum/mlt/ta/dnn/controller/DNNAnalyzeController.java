package ai.maum.mlt.ta.dnn.controller;


import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.common.auth.entity.WorkspaceEntity;
import ai.maum.mlt.common.auth.service.WorkspaceService;
import ai.maum.mlt.common.file.entity.FileEntity;
import ai.maum.mlt.common.file.entity.FileGroupEntity;
import ai.maum.mlt.common.file.service.FileGroupService;
import ai.maum.mlt.common.file.service.FileService;
import ai.maum.mlt.common.system.SystemCode;
import ai.maum.mlt.common.util.FileUtils;
import ai.maum.mlt.itfc.TaGrpcInterfaceManager;
import ai.maum.mlt.ta.dnn.entity.DNNModelEntity;
import ai.maum.mlt.ta.dnn.entity.DNNResultEntity;
import ai.maum.mlt.ta.dnn.service.DNNModelService;
import ai.maum.mlt.ta.dnn.service.DNNResultService;
import io.grpc.Metadata;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import maum.brain.cl.ClassifierOuterClass.ClassifiedSummary;
import maum.brain.cl.ClassifierOuterClass.Model;
import maum.brain.cl.ClassifierOuterClass.ServerState;
import maum.brain.cl.ClassifierOuterClass.ServerStatus;
import maum.brain.nlp.Nlp.Document;
import maum.brain.nlp.Nlp.InputText;
import maum.common.LangOuterClass.LangCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/ta/dnn/analyze")
public class DNNAnalyzeController {

  @Value("${brain-ta.nlp3kor.ip}")
  private String nlp3KorIp;
  @Value("${brain-ta.nlp3kor.port}")
  private int nlp3KorPort;
  @Value("${brain-ta.nlp2eng.ip}")
  private String nlp2EngIp;
  @Value("${brain-ta.nlp2eng.port}")
  private int nlp2EngPort;
  @Value("${brain-ta.brain-cl.ip}")
  private String brainClIp;
  @Value("${brain-ta.brain-cl.port}")
  private int brainClPort;
  @Value("${ta.file.path}")
  String taPath;

  static final Logger logger = LoggerFactory.getLogger(DNNAnalyzeController.class);

  @Autowired
  DNNModelService dnnModelService;

  @Autowired
  DNNResultService dnnResultService;

  @Autowired
  FileGroupService fileGroupService;

  @Autowired
  FileService fileService;

  @Autowired
  private FileUtils fileUtils;

  @Autowired
  WorkspaceService workspaceService;

  @UriRoleDesc(role = "ta/dnn^R")
  @RequestMapping(
      value = "/getAllModels",
      method = RequestMethod.POST
  )
  public ResponseEntity<?> getAllModels(@RequestBody DNNModelEntity dnnModelEntity) {
    logger.info("======= call api  [[/getAllModels]] =======");
    try {
      List<DNNModelEntity> dnnModelEntities = dnnModelService
          .getAllModels(dnnModelEntity.getWorkspaceId());
      return new ResponseEntity<>(dnnModelEntities, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/dnn^R")
  @RequestMapping(
      value = "/getFileGroups",
      method = RequestMethod.POST)
  public ResponseEntity<?> getFileGroupList(@RequestBody FileGroupEntity fileGroupEntity) {
    logger.info("======= call api  [[/api/ta/hmd/getFileGroupList]] =======");
    try {
      List<FileGroupEntity> fileGroupEntities = fileGroupService
          .getFileGroupList(SystemCode.FILE_GRP_PPOS_TA, fileGroupEntity.getWorkspaceId());
      return new ResponseEntity<>(fileGroupEntities, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/dnn^R")
  @RequestMapping(
      value = "/getFileList",
      method = RequestMethod.POST)
  public ResponseEntity<?> getFileList(@RequestBody FileEntity fileEntity) {
    logger.info("======= call api  [[/api/ta/dnn/analyze/getFileList]] =======");
    try {
      Page<FileEntity> pageResult = fileService.getGroupFileList(fileEntity);
      return new ResponseEntity<>(pageResult, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/dnn^X")
  @RequestMapping(
      value = "/startAnalyze",
      method = RequestMethod.POST)
  public ResponseEntity<?> startAnalyze(@RequestBody HashMap<String, Object> hashMap) {

    logger.info("======= call api  [[/api/ta/dnn/analyze/startAnalyze]] =======");
    try {
      WorkspaceEntity workspaceEntity = workspaceService
          .getWorkspace(hashMap.get("workspaceId").toString());

      String ip;
      int port;
      if ("kor".equals("kor")) {
        ip = this.nlp3KorIp;
        port = this.nlp3KorPort;
      } else {
        // eng2
        ip = nlp2EngIp;
        port = nlp2EngPort;
      }

      TaGrpcInterfaceManager nlpClient = new TaGrpcInterfaceManager(ip, port);
      InputText.Builder inputText = InputText.newBuilder();

      FileEntity paramFileEntity = new FileEntity();
      paramFileEntity.setWorkspaceId(hashMap.get("workspaceId").toString());
      paramFileEntity.setPurpose(SystemCode.FILE_GRP_PPOS_TA_DNN);

      List<String> ids = (List<String>) hashMap.get("fileList");
      List<FileEntity> fileList = fileService.getFilesWithIds(hashMap.get("workspaceId").toString(),ids);

      Document document = null;
      String[] lineList;
      ArrayList<String> totalLine = new ArrayList<>();
      for (FileEntity fileEntity : fileList) {
        String fileName = fileEntity.getId() + SystemCode.FILE_EXTENSION_TEXT;
        String path =
            fileUtils.getSystemConfPath(taPath + "/" + hashMap.get("workspaceId").toString()) + "/"
                + fileName;
        File file = new File(path);
//        ArrayList<String> totalLine = new ArrayList<>();
        BufferedReader br = new BufferedReader(new FileReader(file));
        String line;

        try {
          while ((line = br.readLine()) != null) {
            if (!(line.equals(""))) {
              line = line.trim().replaceAll("([^.])$", "$1.");
              totalLine.add(line);
            }
          }
          String text = String.join("\n", totalLine);
          inputText.setSplitSentence(true);
          // Lang은 일단 항상 한국어로만 사용하도록 코딩
          // inputText.setLang(LangCode.valueOf(workspaceEntity.getLangCode()));
          inputText.setLang(LangCode.kor);
          inputText.setText(text);

//          document = nlpClient.analyze(inputText.build());
        } catch (Exception e) {
          logger.error("startAnalyze e : " , e);
        } finally {
          br.close();
        }
      }
      lineList = totalLine.toArray(new String[0]);
      TaGrpcInterfaceManager clClient = new TaGrpcInterfaceManager(this.brainClIp,
          this.brainClPort);

      DNNModelEntity dnnModelEntity = dnnModelService
              .getModelByWorkspaceAndName(hashMap.get("workspaceId").toString(), hashMap.get("name").toString());


      File clientBinary = new File(
          fileUtils.getSystemConfPath(
              taPath + "/" + workspaceEntity.getId() + "/dnn-model/" + dnnModelEntity
                  .getDnnBinary()));
      Metadata metadata = new Metadata();
      metadata.put(Metadata.Key.of("in.lang", Metadata.ASCII_STRING_MARSHALLER), String.valueOf(
          LangCode.valueOf(0)));
      metadata.put(Metadata.Key.of("in.filename", Metadata.ASCII_STRING_MARSHALLER),
          clientBinary.getName());
      metadata.put(Metadata.Key.of("in.model", Metadata.ASCII_STRING_MARSHALLER),
          dnnModelEntity.getName());

      Model.Builder model = Model.newBuilder();
      // Lang은 일단 항상 한국어로만 사용하도록 코딩
      // model.setLang(LangCode.valueOf(workspaceEntity.getLangCode()));
      model.setLang(LangCode.kor);
      model.setModel((String) hashMap.get("name"));
      ServerStatus findServerStatus = clClient.find(model.build());

      //find에서 찾은 serverStatus가 running 상태가 아니면 failed
      if (!findServerStatus.getRunning()) {
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
      }

      String serverAddress = findServerStatus.getServerAddress();
      String[] host = serverAddress.split(":");

      //find에서 찾은 ip, port 로 실제 실행 중인 프로세스의 ip, port 가져옴
      TaGrpcInterfaceManager realClient = new TaGrpcInterfaceManager(host[0],
          Integer.parseInt(host[1]));

      int wait = 0;
      boolean runningFlag = false;
      ServerStatus pingStatus = null;
      while (true) {
        try {
          pingStatus = realClient.classifierPing(model.build());
          if (wait > 60) {
            break;
          }
          if (pingStatus != null && pingStatus.getRunning() && pingStatus.getState().equals(
              ServerState.SERVER_STATE_RUNNING)) {
            runningFlag = true;
            break;
          }
          wait++;
          Thread.sleep(1000);
        } catch (InterruptedException e) {
          logger.error("startAnalyze e : " , e);
        } catch (Exception e) {
          logger.error("startAnalyze e : " , e);
        }
      }
      if (runningFlag) {
        List<ClassifiedSummary> classifiedSummaryList = realClient
            .getClassMultiple(lineList, LangCode.valueOf(0),
                (String) hashMap.get("name"));

        if (classifiedSummaryList.size() > 0) {
          for (ClassifiedSummary classifiedSummary : classifiedSummaryList) {
            DNNResultEntity dnnResultEntity = new DNNResultEntity();
            dnnResultEntity.setWorkspaceId(hashMap.get("workspaceId").toString());
            dnnResultEntity.setModel((String) hashMap.get("name"));
            dnnResultEntity.setFileGroup("");
            dnnResultEntity.setCreatorId((String) hashMap.get("creatorId"));
            dnnResultEntity
                .setSentence(classifiedSummary.getSentences(0).getText());
            dnnResultEntity.setCategory(classifiedSummary.getC1Top());
            dnnResultEntity.setProbability(new BigDecimal(classifiedSummary.getProbabilityTop()));

            dnnResultService.insertResult(dnnResultEntity);
          }
        }
        return new ResponseEntity<>(null, HttpStatus.OK);
      } else {
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
      }

    } catch (Exception e) {
      logger.error("startAnalyze e : " , e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}
