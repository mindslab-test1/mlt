package ai.maum.mlt.ta.common.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.common.auth.service.WorkspaceService;
import ai.maum.mlt.itfc.TaGrpcInterfaceManager;
import com.google.protobuf.util.JsonFormat;
import java.util.HashMap;
import maum.brain.nlp.Nlp.NlpAnalysisLevel;
import maum.common.LangOuterClass.LangCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("api/ta")
public class NLPTestController {

  static final Logger logger = LoggerFactory.getLogger(NLPTestController.class);

  @Value("${brain-ta.nlp1kor.ip}")
  private String nlp1KorIp;

  @Value("${brain-ta.nlp1kor.port}")
  private int nlp1KorPort;

  @Value("${brain-ta.nlp2kor.ip}")
  private String nlp2KorIp;

  @Value("${brain-ta.nlp2kor.port}")
  private int nlp2KorPort;

  @Value("${brain-ta.nlp3kor.ip}")
  private String nlp3KorIp;

  @Value("${brain-ta.nlp3kor.port}")
  private int nlp3KorPort;

  @Value("${brain-ta.nlp2eng.ip}")
  private String nlp2EngIp;

  @Value("${brain-ta.nlp2eng.port}")
  private int nlp2EngPort;

  @Autowired
  private WorkspaceService workspaceService;

  /**
   * NLP1, NLP2, NP3 TEST API
   *
   * @param request => {
   * nlpName: 사용할 nlp 이름,
   * workspaceId: 작업공간,
   * context: 문장,
   * level: AnalysisLevel값,
   * isSplit: SplitSentence 사용 유무
   * @param request => {Document} => Analyze 후 얻은 Documnet를 String으로 리턴
   * @return
   */
  @UriRoleDesc(role = "ta/common^X")
  @RequestMapping(
      value = "/analyze",
      method = RequestMethod.POST
  )
  public ResponseEntity<?> analyze(@RequestBody HashMap<String, Object> request) {
    logger.debug("request : {}", request);

    try {
      String nlp = (String) request.get("nlpName");
      String ip = null;
      int port = 0;
      String workspaceId = (String) request.get("workspaceId");
      // 일단 한국어로 지정하여 analyze
      //String workspaceLangCode = workspaceService.getWorkspace(workspaceId).getLangCode();
      String workspaceLangCode = LangCode.kor.toString();

      if (workspaceLangCode.equals("kor")) {
        if (nlp.equals("nlp1")) {
          ip = this.nlp1KorIp;
          port = this.nlp1KorPort;
        } else if (nlp.equals("nlp2")) {
          ip = this.nlp2KorIp;
          port = this.nlp2KorPort;
        } else if (nlp.equals("nlp3")) {
          ip = this.nlp3KorIp;
          port = this.nlp3KorPort;
        } else {
          throw new Exception("Not available " + nlp);
        }
      } else if (workspaceLangCode.equals("eng")) {
        if (nlp.equals("nlp2")) {
          ip = this.nlp2EngIp;
          port = this.nlp2EngPort;
        } else {
          throw new Exception("Not available " + nlp);
        }
      }
      String context = (String) request.get("context");

      TaGrpcInterfaceManager client = new TaGrpcInterfaceManager(ip, port);
      maum.brain.nlp.Nlp.InputText.Builder inputText = maum.brain.nlp.Nlp.InputText.newBuilder();
      inputText.setLang(LangCode.valueOf(workspaceLangCode));
      inputText.setText(context);
      inputText.setLevel(NlpAnalysisLevel.valueOf((String) request.get("level")));
      inputText.setSplitSentence((boolean) request.get("isSplit"));

      maum.brain.nlp.Nlp.Document trainKey;
      // nlp3 의 경우에는 치환사전 결과를 반영한 analyzeWithSpace를 사용
      if (nlp.equals("nlp3")) {
        trainKey = client.analyzeWithSpace(inputText.build());
      } else {
        trainKey = client.analyze(inputText.build());
      }
      logger.debug("analyze : {}", trainKey);

      // proto data parser 적용 ASCII code print 해결.
      String result = JsonFormat.printer().print(trainKey);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("error : " + e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * 외부 업체에 nlpTest를 제공하기 위하여, nlp2Kor 로 고정함
   *
   * @param request => {context, workspaceId}
   * @return => {Document 객체를 String type 으로 변환한 결과}
   */
  @UriRoleDesc(role = "ta/common^X")
  @RequestMapping(
      value = "/nlpTest",
      method = RequestMethod.POST
  )
  public ResponseEntity<?> nlpTest(@RequestBody HashMap<String, String> request) {
    logger.debug("request : {}", request);
    try {
      String context = request.get("context");
      String workspceLangCode = request.get("langCode");

      TaGrpcInterfaceManager client = new TaGrpcInterfaceManager(nlp2KorIp, nlp2KorPort);
      maum.brain.nlp.Nlp.InputText.Builder inputText = maum.brain.nlp.Nlp.InputText.newBuilder();
      inputText.setLang(LangCode.valueOf(workspceLangCode));
      inputText.setText(context);
      inputText.setTextBytes(inputText.getTextBytes());

      maum.brain.nlp.Nlp.Document trainKey = client.analyze(inputText.build());
      logger.debug("analyze : {}", trainKey);

      // proto data parser 적용 ASCII code print 해결.
      String result = JsonFormat.printer().print(trainKey);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("analyze e : " + e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

    }
  }
}
