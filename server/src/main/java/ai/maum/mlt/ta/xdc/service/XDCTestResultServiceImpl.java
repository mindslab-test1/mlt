package ai.maum.mlt.ta.xdc.service;

import ai.maum.mlt.ta.xdc.entity.XdcTestResultEntity;
import ai.maum.mlt.ta.xdc.entity.XdcTestResultSentenceEntity;
import ai.maum.mlt.ta.xdc.entity.XdcTestResultWordEntity;
import ai.maum.mlt.ta.xdc.repository.XDCTestResultRepository;
import ai.maum.mlt.ta.xdc.repository.XDCTestResultWordRepository;
import ai.maum.mlt.ta.xdc.repository.XDCTestResultSentenceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class XDCTestResultServiceImpl implements XDCTestResultService {

  @Autowired
  private XDCTestResultRepository xdcTestResultRepository;
  @Autowired
  private XDCTestResultWordRepository xdcTestResultWordRepository;
  @Autowired
  private XDCTestResultSentenceRepository xdcTestResultSentenceRepository;

  @Override
  public Page<XdcTestResultEntity> findByXdcTestResultEntityPage(
      XdcTestResultEntity xdcTestResultEntity) {
    return this.xdcTestResultRepository.findByXdcResultsPage(
        xdcTestResultEntity.getWorkspaceId(),
        xdcTestResultEntity.getModel(),
        xdcTestResultEntity.getFileGroup(),
        xdcTestResultEntity.getTextData(),
        xdcTestResultEntity.getCategory(),
        xdcTestResultEntity.getPageRequest()
    );
  }

  @Override
  public List<XdcTestResultEntity> findByXdcTestResultEntities(
      XdcTestResultEntity xdcTestResultEntity) {
    return this.xdcTestResultRepository.findByXdcResultsList(
        xdcTestResultEntity.getWorkspaceId(),
        xdcTestResultEntity.getModel(),
        xdcTestResultEntity.getFileGroup()
    );
  }

  @Transactional
  @Override
  public void insertResult(XdcTestResultEntity xdcTestResultEntity) {
    xdcTestResultEntity.setCreatedAt(new Date());
    this.xdcTestResultRepository.save(xdcTestResultEntity);
  }

  @Transactional
  @Override
  public XdcTestResultSentenceEntity insertResultSentence(
      XdcTestResultSentenceEntity xdcTestResultSentenceEntity) {
    XdcTestResultSentenceEntity result;
    xdcTestResultSentenceEntity.setCreatedAt(new Date());
    result = this.xdcTestResultSentenceRepository.save(xdcTestResultSentenceEntity);
    return result;
  }

  @Transactional
  @Override
  public XdcTestResultWordEntity insertResultWord(XdcTestResultWordEntity xdcTestResultWordEntity) {
    XdcTestResultWordEntity result = new XdcTestResultWordEntity();
    if (xdcTestResultWordEntity != null) {
      result = this.xdcTestResultWordRepository.save(xdcTestResultWordEntity);
    }
    return result;
  }

  @Transactional
  @Override
  public List<XdcTestResultEntity> insertResults(List<XdcTestResultEntity> xdcResultEntities) {
    List<XdcTestResultEntity> resultEntities = new ArrayList<>();

    if (!xdcResultEntities.isEmpty()) {
      resultEntities = this.xdcTestResultRepository.save(xdcResultEntities);
    }
    return resultEntities;
  }

  @Override
  public Integer deleteByXdcResult(XdcTestResultEntity xdcTestResultEntity) {
    List<XdcTestResultEntity> xdcResultEntities = this
        .findByXdcTestResultEntities(xdcTestResultEntity);
    for (XdcTestResultEntity res : xdcResultEntities) {
      List<XdcTestResultSentenceEntity> sents = res.getXdcSentences();
      for (XdcTestResultSentenceEntity sent : sents) {
        List<XdcTestResultWordEntity> words= sent.getXdcWords();
        this.xdcTestResultWordRepository.deleteInBatch(words);
      }
      this.xdcTestResultSentenceRepository.deleteInBatch(sents);
    }
    this.xdcTestResultRepository.deleteInBatch(xdcResultEntities);

    return xdcResultEntities.size();
  }

  @Transactional
  @Override
  public Page<XdcTestResultWordEntity> findWordsByIds(List<String> ids,
      XdcTestResultWordEntity xdcTestResultWordEntity) {
    Page<XdcTestResultWordEntity> entities = null;
    if (!ids.isEmpty() ) {
      entities = this.xdcTestResultWordRepository.findAllByIds(ids, xdcTestResultWordEntity.getPageRequest());
    }
    return entities;
  }

  @Transactional
  @Override
  public List<String> getWordsIdBySentenceEntity(String workspaceId, String sentenceId) {
    List<String> list = new ArrayList<>();
    XdcTestResultSentenceEntity xdcTestResultSentenceEntity = this.xdcTestResultSentenceRepository
        .findAllByWorkspaceIdAndSentenceId(workspaceId, sentenceId);
    List<XdcTestResultWordEntity> entity = xdcTestResultSentenceEntity.getXdcWords();
    for (XdcTestResultWordEntity word : entity) {
      list.add(word.getId());
    }
    return list;
  }
}
