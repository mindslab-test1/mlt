package ai.maum.mlt.ta.xdc.entity;

import ai.maum.mlt.common.auth.entity.UserEntity;
import ai.maum.mlt.common.auth.entity.WorkspaceEntity;
import ai.maum.mlt.common.pagenation.PageParameters;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "MAI_XDC_TEST_RESULT")
public class XdcTestResultEntity extends PageParameters implements Serializable {

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid")
  @Column(name = "ID", length = 40, nullable = false)
  private String id;

  @Column(name = "WORKSPACE_ID", length = 40, nullable = false)
  private String workspaceId;

  @Column(name = "MODEL", length = 512, nullable = false)
  private String model;

  @Column(name = "FILE_GROUP", length = 512)
  private String fileGroup;

  @Lob
  @Column(name = "TEXT_DATA", nullable = false)
  private String textData;

  @Column(name = "CATEGORY", length = 512)
  private String category;

  @Column(name = "PROBABILITY", scale = 16, precision = 17)
  private BigDecimal probability;

  @Column(name = "CREATOR_ID", length = 40)
  private String creatorId;

  @CreatedDate
  @Column(name = "CREATED_AT")
  private Date createdAt;

  @OneToMany
  @JoinColumn(name="XDC_TEST_RESULT_ID")
  private List<XdcTestResultSentenceEntity> xdcSentences = new ArrayList<>();

  @ManyToOne
  @JoinColumn(name = "CREATOR_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private UserEntity createUserEntity;

  @ManyToOne
  @JoinColumn(name = "WORKSPACE_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private WorkspaceEntity workspaceEntity;

  public void setProbability(BigDecimal probability) {
    this.probability = probability.setScale(3, BigDecimal.ROUND_HALF_UP);
  }
}
