package ai.maum.mlt.ta.xdc.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.common.util.FileUtils;
import ai.maum.mlt.itfc.TaGrpcInterfaceManager;
import ai.maum.mlt.management.entity.HistoryEntity;
import ai.maum.mlt.management.service.HistoryService;
import ai.maum.mlt.ta.xdc.entity.XDCDicEntity;
import ai.maum.mlt.ta.xdc.entity.XDCDicLineEntity;
import ai.maum.mlt.ta.xdc.entity.XDCModelEntity;
import ai.maum.mlt.ta.xdc.entity.XDCTrainingEntity;
import ai.maum.mlt.ta.xdc.service.XDCDicLineService;
import ai.maum.mlt.ta.xdc.service.XDCDicService;
import ai.maum.mlt.ta.xdc.service.XDCModelService;
import ai.maum.mlt.ta.xdc.service.XDCTrainingService;
import com.google.protobuf.Empty;
import maum.brain.han.core.HanCore;
import maum.brain.han.data.HanData;
import maum.brain.han.train.HanTrain;
import maum.brain.han.train.HanTrain.HanModel;
import maum.brain.han.train.HanTrain.HanTrainKey;
import maum.brain.han.train.HanTrain.HanTrainStatus;
import maum.brain.han.train.HanTrain.HanTrainBinary;
import maum.common.LangOuterClass.LangCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("api/ta/xdc/training")
public class XDCTrainingController {

  static final Logger logger = LoggerFactory.getLogger(XDCTrainingController.class);
  private static final String MESSAGE = "message";

  @Value("${ta.xdc.train.server.ip}")
  private String taTrainIp;
  @Value("${ta.xdc.train.server.port}")
  private int taTrainPort;
  @Value("${brain-ta.brain-cl.ip}")
  private String taBrainIp;
  @Value("${brain-ta.brain-cl.port}")
  private int taBrainPort;
  @Value("${server.protocol}")
  private String serverProtocol;
  @Value("${server.host}")
  private String serverHost;
  @Value("${server.port}")
  private int serverPort;
  @Value("${mlt.api.key}")
  private String apiKey;
  @Value("${ta.file.path}")
  private String taPath;
  @Value("${han.file.path}")
  private String hanPath;

  @Autowired
  private XDCDicService xdcDicService;

  @Autowired
  private XDCTrainingService xdcTrainingService;

  @Autowired
  private XDCModelService xdcModelService;

  @Autowired
  private XDCDicLineService xdcDicLineService;

  @Autowired
  private HistoryService historyService;

  @Autowired
  private FileUtils fileUtils;

  @UriRoleDesc(role = "ta/xdc^R")
  @PostMapping("/getXdcDicAllList")
  public ResponseEntity<?> getXdcDicAllList(@RequestBody XDCDicEntity xdcDicEntity) {
    logger.info("======= call api  [[/api/ta/xdc/training/getXdcDicAllList]] =======");
    try {
      List<XDCDicEntity> xdcDicEntities = xdcDicService
          .getXdcDicAllList(xdcDicEntity.getWorkspaceId());
      return new ResponseEntity<>(xdcDicEntities, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/xdc^R")
  @PostMapping("/getXdcDicByName")
  public ResponseEntity<?> getXdcDicByName(@RequestBody XDCDicEntity xdcDicEntity) {
    logger.info("======= call api  [[/api/ta/xdc/training/getXdcDicByName]] =======");
    try {
      XDCDicEntity entity = xdcDicService.getXdcDic(xdcDicEntity.getWorkspaceId(), xdcDicEntity.getName());
      return new ResponseEntity<>(entity, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/xdc^R")
  @PostMapping("/getAllModels")
  public ResponseEntity<?> getAllModels(@RequestBody XDCTrainingEntity xdcTrainingEntity) {
    logger.info("======= call api  [[/getAllModels]] =======");
    try {
      List<XDCModelEntity> xdcModelEntities = xdcModelService
        .getAllModels(xdcTrainingEntity.getWorkspaceId());
      return new ResponseEntity<>(xdcModelEntities, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/xdc^R")
  @PostMapping("/getModelsByName")
  public ResponseEntity<?> getModelsByName(@RequestBody XDCTrainingEntity xdcTrainingEntity) {
    logger.info("======= call api  [[/getModelsByName]] =======");
    try {
      List<XDCModelEntity> xdcModelEntities = xdcModelService
        .getModelsByName(xdcTrainingEntity.getWorkspaceId(), xdcTrainingEntity.getName());
      return new ResponseEntity<>(xdcModelEntities, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/xdc^X")
  @PostMapping("/open")
  public ResponseEntity<?> open(@RequestBody XDCDicEntity xdcDicEntity) {
    logger.info("======= call api  [[/Open]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {

      HistoryEntity historyEntity = new HistoryEntity();
      historyEntity.setWorkspaceId(xdcDicEntity.getWorkspaceId());
      historyEntity.setCode("XDC_TRAINING");
      historyEntity.setMessage("Started");

      historyEntity = historyService.insertHistory(historyEntity);

      /* xdcDic 으로 정보조회 */
      TaGrpcInterfaceManager client = new TaGrpcInterfaceManager(taTrainIp, taTrainPort);
      HanModel.Builder clModel = HanModel.newBuilder();

      clModel.setModel(xdcDicEntity.getName().replace("_xdc", ""));
      // Lang은 일단 항상 한국어만 사용
      clModel.setLang(LangCode.kor);

      //brain-xdc tests/train_client_test.json 데이터를 기준으로 적용
      clModel.getOptionBuilder().getNlpBuilder().getCommonBuilder().getWordBuilder().setDictFile("word/word.txt");
      clModel.getOptionBuilder().getNlpBuilder().getCommonBuilder().getWordBuilder().setUseEmb(true);
      clModel.getOptionBuilder().getNlpBuilder().getCommonBuilder().getWordBuilder().getEmbBuilder().setPreTrainFile("word/word_emb.txt");
      clModel.getOptionBuilder().getNlpBuilder().getCommonBuilder().getWordBuilder().getEmbBuilder().setDim(100);
      clModel.getOptionBuilder().getNlpBuilder().getCommonBuilder().getWordBuilder().getEmbBuilder().setTunePartial(0);
      clModel.getOptionBuilder().getNlpBuilder().getCommonBuilder().getWordBuilder().getEmbBuilder().setDropout(0.2f);

      clModel.getOptionBuilder().getNlpBuilder().getCommonBuilder().setUsePos(true);
      clModel.getOptionBuilder().getNlpBuilder().getCommonBuilder().getPosBuilder().setDictFile("pos/pos.txt");
      clModel.getOptionBuilder().getNlpBuilder().getCommonBuilder().getPosBuilder().setUseEmb(true);
      clModel.getOptionBuilder().getNlpBuilder().getCommonBuilder().getPosBuilder().getEmbBuilder().setPreTrainFile("");
      clModel.getOptionBuilder().getNlpBuilder().getCommonBuilder().getPosBuilder().getEmbBuilder().setDim(12);
      clModel.getOptionBuilder().getNlpBuilder().getCommonBuilder().getPosBuilder().getEmbBuilder().setTunePartial(0);
      clModel.getOptionBuilder().getNlpBuilder().getCommonBuilder().getPosBuilder().getEmbBuilder().setDropout(0.2f);

      clModel.getOptionBuilder().getNlpBuilder().getCommonBuilder().setUseNer(true);
      clModel.getOptionBuilder().getNlpBuilder().getCommonBuilder().getNerBuilder().setDictFile("ner/ner.txt");
      clModel.getOptionBuilder().getNlpBuilder().getCommonBuilder().getNerBuilder().setUseEmb(true);
      clModel.getOptionBuilder().getNlpBuilder().getCommonBuilder().getNerBuilder().getEmbBuilder().setPreTrainFile("");
      clModel.getOptionBuilder().getNlpBuilder().getCommonBuilder().getNerBuilder().getEmbBuilder().setDim(10);
      clModel.getOptionBuilder().getNlpBuilder().getCommonBuilder().getNerBuilder().getEmbBuilder().setTunePartial(0);
      clModel.getOptionBuilder().getNlpBuilder().getCommonBuilder().getNerBuilder().getEmbBuilder().setDropout(0.2f);

      clModel.getOptionBuilder().getNlpBuilder().getCommonBuilder().setUseChar(true);
      clModel.getOptionBuilder().getNlpBuilder().getCommonBuilder().getCharBuilder().setDictFile("char/char.txt");
      clModel.getOptionBuilder().getNlpBuilder().getCommonBuilder().getCharBuilder().setUseEmb(true);
      clModel.getOptionBuilder().getNlpBuilder().getCommonBuilder().getCharBuilder().getEmbBuilder().setPreTrainFile("");
      clModel.getOptionBuilder().getNlpBuilder().getCommonBuilder().getCharBuilder().getEmbBuilder().setDim(50);
      clModel.getOptionBuilder().getNlpBuilder().getCommonBuilder().getCharBuilder().getEmbBuilder().setTunePartial(0);
      clModel.getOptionBuilder().getNlpBuilder().getCommonBuilder().getCharBuilder().getEmbBuilder().setDropout(0.2f);
      clModel.getOptionBuilder().getNlpBuilder().getCommonBuilder().setUseTf(true);

      clModel.getOptionBuilder().getNlpBuilder().setNlpType(HanData.NlpType.BRAIN_NLP_KOR3);

      clModel.getOptionBuilder().getModelBuilder().getClassifierBuilder().getCharEncoderBuilder().setOutputSize(50);
      clModel.getOptionBuilder().getModelBuilder().getClassifierBuilder().getCharEncoderBuilder().addFilterSizes(2);
      clModel.getOptionBuilder().getModelBuilder().getClassifierBuilder().getCharEncoderBuilder().addFilterSizes(3);
      clModel.getOptionBuilder().getModelBuilder().getClassifierBuilder().getCharEncoderBuilder().addFilterSizes(4);
      clModel.getOptionBuilder().getModelBuilder().getClassifierBuilder().getCharEncoderBuilder().addFilterSizes(5);
      clModel.getOptionBuilder().getModelBuilder().getClassifierBuilder().getCharEncoderBuilder().addFilterSizes(6);
      clModel.getOptionBuilder().getModelBuilder().getClassifierBuilder().getCharEncoderBuilder().setNumFilters(30);
      clModel.getOptionBuilder().getModelBuilder().getClassifierBuilder().getCharEncoderBuilder().setDropoutRate(0.2f);

      clModel.getOptionBuilder().getModelBuilder().getClassifierBuilder().getWordEncoderBuilder().setHiddenSize(200);
      clModel.getOptionBuilder().getModelBuilder().getClassifierBuilder().getWordEncoderBuilder().setNumLayers(3);
      clModel.getOptionBuilder().getModelBuilder().getClassifierBuilder().getWordEncoderBuilder().setDropoutRate(0.2f);
      clModel.getOptionBuilder().getModelBuilder().getClassifierBuilder().getWordEncoderBuilder().setDropoutOutput(true);
      clModel.getOptionBuilder().getModelBuilder().getClassifierBuilder().getWordEncoderBuilder().setRnnType(HanCore.RnnType.SRU);
      clModel.getOptionBuilder().getModelBuilder().getClassifierBuilder().getWordEncoderBuilder().setOutputType(HanCore.OutputType.SUM);

      clModel.getOptionBuilder().getModelBuilder().getClassifierBuilder().getSentenceEncoderBuilder().setHiddenSize(200);
      clModel.getOptionBuilder().getModelBuilder().getClassifierBuilder().getSentenceEncoderBuilder().setNumLayers(3);
      clModel.getOptionBuilder().getModelBuilder().getClassifierBuilder().getSentenceEncoderBuilder().setDropoutRate(0.2f);
      clModel.getOptionBuilder().getModelBuilder().getClassifierBuilder().getSentenceEncoderBuilder().setDropoutOutput(true);
      clModel.getOptionBuilder().getModelBuilder().getClassifierBuilder().getSentenceEncoderBuilder().setRnnType(HanCore.RnnType.SRU);
      clModel.getOptionBuilder().getModelBuilder().getClassifierBuilder().getSentenceEncoderBuilder().setOutputType(HanCore.OutputType.SUM);

      clModel.getOptionBuilder().getModelBuilder().getClassifierBuilder().getDocEncoderBuilder().setHiddenSize(400);
      clModel.getOptionBuilder().getModelBuilder().getClassifierBuilder().getDocEncoderBuilder().setNumLayers(0);
      clModel.getOptionBuilder().getModelBuilder().getClassifierBuilder().getDocEncoderBuilder().setDropoutRate(0.2f);
      clModel.getOptionBuilder().getModelBuilder().getClassifierBuilder().getDocEncoderBuilder().setActivationType(HanCore.ActivationType.RELU);

      clModel.getOptionBuilder().getModelBuilder().getOptimizerBuilder().setOptimizerType(HanCore.OptimizerType.ADAMAX);
      clModel.getOptionBuilder().getModelBuilder().getOptimizerBuilder().getAdmaxBuilder().setLr(0.002f);
      clModel.getOptionBuilder().getModelBuilder().getOptimizerBuilder().getAdmaxBuilder().setBeta1(0.9f);
      clModel.getOptionBuilder().getModelBuilder().getOptimizerBuilder().getAdmaxBuilder().setBeta2(0.999f);
      clModel.getOptionBuilder().getModelBuilder().getOptimizerBuilder().getAdmaxBuilder().setEps(1e-08f);
      clModel.getOptionBuilder().getModelBuilder().getOptimizerBuilder().getAdmaxBuilder().setWeightDecay(0.001f);
      clModel.getOptionBuilder().getModelBuilder().getOptimizerBuilder().getSgdBuilder().setLr(0.1f);
      clModel.getOptionBuilder().getModelBuilder().getOptimizerBuilder().getSgdBuilder().setMomentum(0);
      clModel.getOptionBuilder().getModelBuilder().getOptimizerBuilder().getSgdBuilder().setDampening(0);
      clModel.getOptionBuilder().getModelBuilder().getOptimizerBuilder().getSgdBuilder().setWeightDecay(0);
      clModel.getOptionBuilder().getModelBuilder().getOptimizerBuilder().getSgdBuilder().setNesterov(false);

      clModel.getOptionBuilder().getModelBuilder().setGradClipping(5.0f);

      clModel.getDataBuilder().getCommonBuilder().setEncoding("utf-8");

      clModel.getDataBuilder().getCommonBuilder().setMaxBatchSize(xdcDicEntity.getMaxBatchSz());
      clModel.getDataBuilder().getCommonBuilder().setTrainMaxSizeProd(xdcDicEntity.getTrainMaxSz());
      clModel.getDataBuilder().getCommonBuilder().setTestMaxSizeProd(xdcDicEntity.getTestMaxSz());
      clModel.getDataBuilder().getCommonBuilder().setEpochSize(xdcDicEntity.getEpoch());
      clModel.getDataBuilder().getCommonBuilder().setLogPerUpdate(10);
      clModel.getDataBuilder().getCommonBuilder().setUseGraph(true);

      clModel.getDataBuilder().setRawDataType(HanTrain.RawDataType.TA);
      clModel.getDataBuilder().getTaBuilder().setTestRatio(0.5f);
      String callBackUrl = this.serverProtocol + "://" + this.serverHost + ":" + this.serverPort
        + "/api/ta/xdc/training/"
        + historyEntity.getId() + "/" + historyEntity.getWorkspaceId() + "/update/";
      clModel.setCallbackUrl(callBackUrl);

      // brain-xdc 학습을 위해 txt 파일로 만들어야하는 사전 작업
      // 형식: 내용\t라벨

      String dir = fileUtils.getSystemConfPath(hanPath + "/" + xdcDicEntity.getWorkspaceId() + "/xdc-model");
      fileUtils.mkdirs(dir);
      String fileName = dir + '/' + xdcDicEntity.getId() + '_' + System.currentTimeMillis() + ".txt";
      File file = new File(fileName);
      FileWriter writer = null;
      BufferedWriter bWriter = null;
      List<XDCDicLineEntity> xdcDicLineEntities = xdcDicLineService
        .getAllDicLinesByVersionId(xdcDicEntity.getId());
      List<String> categoryList = xdcDicLineService
        .getAllDistinct(xdcDicEntity.getId());
      // 학습 시 카테고리별 매핑된  내용이 2 개이상이여야만 한다.
      if (!xdcDicLineEntities.isEmpty()) {
        for (String category : categoryList) {
          //문장이 단순하게 2개 이하인 경우
          if (xdcDicLineService.getAllDicLines(xdcDicEntity.getId(), category).size() < 2) {
            result.put(MESSAGE, "There must be at least two sentences in " + category);
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
          }
          clModel.getOptionBuilder().getRawDataLoaderBuilder().addLabels(category);
        }
        clModel.getOptionBuilder().getRawDataLoaderBuilder().setUseExtraFeature(false);
        file.createNewFile();
        writer = new FileWriter(file, false);
        bWriter = new BufferedWriter(writer);
        int i = 0;
        for (XDCDicLineEntity xdcDicLineEntity : xdcDicLineEntities) {
          if (!xdcDicLineEntity.getSentence().trim().equals("")) {
            bWriter.write(xdcDicLineEntity.getSentence() + '\t' + xdcDicLineEntity.getCategory() + '\n');
          }
        }
        bWriter.flush();
        try {
          if (bWriter != null) { bWriter.close(); }
          logger.trace("=======[[/Open]] make file for xdc train < {} >", fileName);
        } catch (IOException e) {
          logger.error("=======[[/Open]] Can't write file : {}", e);
          result.put(MESSAGE, e.getMessage());
          throw new IOException("Server Error.  Can't write file");
        }
      } else {
        logger.error("=======[[/Open]] No data for xdc training=======");
        return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
      }
      clModel.getDataBuilder().getTaBuilder().setFilePath(fileName);
      HanTrainKey trainKey = client.open(clModel.build());
      if (trainKey == null) {
        result.put(MESSAGE, "Server Error. train engine not started");
        return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
      }
      XDCTrainingEntity xdcTrainingEntity = new XDCTrainingEntity();
      xdcTrainingEntity.setXdcKey(trainKey.getTrainId());
      xdcTrainingEntity.setWorkspaceId(xdcDicEntity.getWorkspaceId());
      xdcTrainingEntity = xdcTrainingService.insertTraining(xdcTrainingEntity);

      result.put("resultCode", "SUCCESS");
      result.put("trainingEntity", xdcTrainingEntity);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("open e : {}", e);
      result.put(MESSAGE, e.getMessage());
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /*
   id = history.id
   key = apiKey
  */
  @UriRoleDesc(role = "ta/xdc^U")
  @PostMapping("/{id}/{workspaceId}/update/{key}")
  public void update(
    @PathVariable("key") String key,
    @PathVariable("id") String id,
    @PathVariable("workspaceId") String workspaceId,
    @RequestBody Object data) {
    logger.info("======= call api  [[/update]] ======= {} ", key);
    try {
      HistoryEntity historyEntity = new HistoryEntity();
      HashMap<String, Object> dataMap = (HashMap) data;

      if (apiKey.equals(key)) {
        HashMap<String, String> dataModelMap = (HashMap) dataMap.get("data");
        historyEntity.setData(dataModelMap.toString());
        historyEntity.setId(id);
        historyEntity.setMessage((String) dataMap.get(MESSAGE));
        historyEntity.setEndedAt(new Date());
        historyEntity.setUpdatedAt(new Date());

        historyService.updateHistory(historyEntity);

        if (!dataModelMap.get("result").equals("failed")) {
          XDCModelEntity xdcModelEntity = new XDCModelEntity();
          xdcModelEntity.setXdcBinary(dataModelMap.get("binary"));
          xdcModelEntity.setName(dataModelMap.get("name"));
          xdcModelEntity.setXdcKey(dataModelMap.get("key"));
          xdcModelEntity.setWorkspaceId(workspaceId);
          xdcModelService.insertModel(xdcModelEntity);

          TaGrpcInterfaceManager taTrainClient = new TaGrpcInterfaceManager(taTrainIp, taTrainPort);
          HanTrainKey.Builder clTrainKey = HanTrainKey.newBuilder();
          clTrainKey.setTrainId(dataModelMap.get("key"));
          List<HanTrainBinary> clientBinary = taTrainClient.getBinary(clTrainKey.build());

          fileUtils.mkdirs(fileUtils.getSystemConfPath(taPath + "/" + workspaceId + "/xdc-model"));

          FileOutputStream xdcModelStream = new FileOutputStream(
            fileUtils.getSystemConfPath(
              taPath + "/" + workspaceId + "/xdc-model/" + xdcModelEntity.getXdcBinary()));
          try {
            for (HanTrainBinary tar : clientBinary) {
              xdcModelStream.write(tar.getPt().toByteArray());
            }
          } finally {
            xdcModelStream.close();
            taTrainClient.close(clTrainKey.build());
            logger.debug("====== XDC getBinary finish");
          }
        }
      }
    } catch (Exception e) {
      logger.debug("update e : {}", e);
    }
  }

  @UriRoleDesc(role = "ta/xdc^X")
  @PostMapping("/getAllProgress")
  public ResponseEntity<?> getAllProgress(@RequestBody XDCDicEntity xdcDicEntity) {
    logger.info("======= call api  [[/getAllProgress]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {

      List<XDCTrainingEntity> xdcTrainingEntities = new ArrayList<>();
      TaGrpcInterfaceManager client = new TaGrpcInterfaceManager(taTrainIp, taTrainPort);

      Empty.Builder req = Empty.newBuilder();
      HanTrain.HanTrainStatusList trainStatusList = client.getXdcAllProgress(req.build());

      if (trainStatusList == null) {
        throw new Exception("NOT_STARTED");
      }

      if (trainStatusList != null) {
        for (HanTrainStatus trainStatus : trainStatusList.getHanTrainsList()) {
          XDCTrainingEntity xdcTrainingEntity = xdcTrainingService
              .getTraining(trainStatus.getKey());

          if (xdcTrainingEntity != null) {

            xdcTrainingEntity.setXdcKey(trainStatus.getKey());
            xdcTrainingEntity.setName(trainStatus.getModel());
            xdcTrainingEntity.setEpoch(trainStatus.getDataOrBuilder().getCommonOrBuilder().getEpochSize());
            xdcTrainingEntity.setMaxBatchSz(trainStatus.getDataOrBuilder().getCommonOrBuilder().getMaxBatchSize());
            xdcTrainingEntity.setTestMaxSz(trainStatus.getDataOrBuilder().getCommonOrBuilder().getTestMaxSizeProd());
            xdcTrainingEntity.setTrainMaxSz(trainStatus.getDataOrBuilder().getCommonOrBuilder().getTrainMaxSizeProd());
            xdcTrainingEntity.setStatus(trainStatus.getResult().toString());
            xdcTrainingEntity.setProgress(trainStatus.getValue() * 100 / trainStatus.getMaximum());
            xdcTrainingEntity.setWorkspaceId(xdcDicEntity.getWorkspaceId());

            if (xdcTrainingEntity.getWorkspaceId().equals(xdcDicEntity.getWorkspaceId())) {
              xdcTrainingEntities.add(xdcTrainingEntity);
            }
          }
        }
      }

      logger.debug("dtList : {}", xdcTrainingEntities);
      result.put("dtList", xdcTrainingEntities);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      if (e.getMessage().equals("NOT_STARTED")) {
        result.put(MESSAGE, e.getMessage());
      }
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/xdc^X")
  @PostMapping(value = "/stop")
  public ResponseEntity<?> stop(@RequestBody XDCTrainingEntity xdcTrainingEntity) {

    logger.info("======= call api  [[/xdcStop]] =======");
    HashMap<String, Object> result = new HashMap<>();
    try {
      TaGrpcInterfaceManager client = new TaGrpcInterfaceManager(taTrainIp, taTrainPort);

      HanTrainKey.Builder req = HanTrainKey.newBuilder();
      req.setTrainId(xdcTrainingEntity.getXdcKey());
      HanTrainStatus trainStatus = client.stop(req.build());
      logger.debug("=============stop========== {}", trainStatus);

      xdcTrainingService.deleteTraining(xdcTrainingEntity.getXdcKey());

      return new ResponseEntity<>(trainStatus.getResult(), HttpStatus.OK);
    } catch (Exception e) {
      logger.debug("getProgress e : {}", e);
      result.put(MESSAGE, "FAIL");
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/xdc^D")
  @PostMapping("/deleteModel")
  public ResponseEntity<?> deleteModel(@RequestBody XDCTrainingEntity xdcTrainingEntity) {
    logger.info("======= call api  [[/deleteModel]] =======");
    try {
      xdcModelService.deleteModel(xdcTrainingEntity.getXdcKey());
      return new ResponseEntity<>(null, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/xdc^D")
  @PostMapping("/deleteModels")
  public ResponseEntity<?> deleteModels(@RequestBody List<XDCTrainingEntity> xdcTrainingEntities) {
    logger.info("======= call api  [[/deleteModel]] =======");
    try {
      for (XDCTrainingEntity xdcTrainingEntity : xdcTrainingEntities) {
        xdcModelService.deleteModel(xdcTrainingEntity.getXdcKey());
      }
      return new ResponseEntity<>(null, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}
