package ai.maum.mlt.ta.dnn.service;

import ai.maum.mlt.ta.dnn.entity.DNNDicEntity;
import java.util.List;
import org.springframework.data.domain.Page;

public interface DNNDicService {

  List<DNNDicEntity> getDnnDicAllList(String workspaceId);

  Page<DNNDicEntity> getDnnDicAllList(DNNDicEntity dnnDicEntity);

  DNNDicEntity getDnnDic(String workspace, String name);
}
