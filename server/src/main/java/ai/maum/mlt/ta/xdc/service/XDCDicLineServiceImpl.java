package ai.maum.mlt.ta.xdc.service;

import ai.maum.mlt.ta.xdc.entity.XDCDicLineEntity;
import ai.maum.mlt.ta.xdc.repository.XDCDicLineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class XDCDicLineServiceImpl implements XDCDicLineService {

  @Autowired
  private XDCDicLineRepository xdcDicLineRepository;

  @Override
  public List<XDCDicLineEntity> getAllDicLinesByVersionId(String versionId) {
    return this.xdcDicLineRepository.getAllByVersionId(versionId);
  }

  @Override
  public List<XDCDicLineEntity> getAllDicLines(String versionId, String category) {
    return this.xdcDicLineRepository
      .getAllByVersionIdAndCategoryOrderBySeqDesc(versionId, category);
  }

  @Override
  public Page<XDCDicLineEntity> getAllDicLines(String versionId, String category, String sentence,
                                               Pageable pageable) {
    return this.xdcDicLineRepository
      .getAllByVersionIdAndCategoryAndSentenceOrderBySeqDesc(versionId, category, sentence,
        pageable);
  }

  @Override
  public List<XDCDicLineEntity> getDuplicatesLines(String versionId, String category) {
    return this.xdcDicLineRepository.getDuplicatesLines(versionId, category);
  }

  @Override
  public List<String> getAllDistinct(String versionId) {
    return this.xdcDicLineRepository.getAllDistinct(versionId);
  }

  @Transactional
  @Override
  public XDCDicLineEntity insertLine(XDCDicLineEntity xdcDicLineEntity) {
    return this.xdcDicLineRepository.save(xdcDicLineEntity);
  }

  @Override
  public boolean deleteDuplicatesLines(String versionId, String category) {
    try {
      List<XDCDicLineEntity> xdcDicLineEntities = this.xdcDicLineRepository
        .getDistinctByXDCDicLine(versionId, category);
      this.xdcDicLineRepository.deleteInBatch(xdcDicLineEntities);
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  @Override
  public boolean deleteLine(XDCDicLineEntity xdcDicLineEntity) {
    try {
      this.xdcDicLineRepository.delete(xdcDicLineEntity);
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  @Override
  @Transactional
  public void deleteByVersionId(String versionId) {
    this.xdcDicLineRepository.deleteByVersionId(versionId);
  }

  @Override
  @Transactional
  public void updateDicLineCategory(String category, String oldName) {
    this.xdcDicLineRepository.updateDicLineCategory(category, oldName);
  }
}
