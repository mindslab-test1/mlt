package ai.maum.mlt.ta.xdc.entity;

import ai.maum.mlt.common.auth.entity.WorkspaceEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;

@Entity
@Data
@Getter
@Setter
@EqualsAndHashCode
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "MAI_XDC_TRAINING")
public class XDCTrainingEntity {

  @Id
  @Column(name = "XDC_KEY", length = 40, nullable = false)
  private String xdcKey;

  @Column(name = "WORKSPACE_ID", length = 40, nullable = false)
  private String workspaceId;

  @ManyToOne
  @JoinColumn(name = "WORKSPACE_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  private WorkspaceEntity workspaceEntity;

  @Transient
  private String name;

  @Transient
  private String status;

  @Transient
  private Integer progress;

  @Transient
  private Integer epoch;

  @Transient
  private Integer maxBatchSz;

  @Transient
  private Integer trainMaxSz;

  @Transient
  private Integer testMaxSz;

  @Transient
  private Integer runCur;

  @Transient
  private Integer stopped = 0;
}
