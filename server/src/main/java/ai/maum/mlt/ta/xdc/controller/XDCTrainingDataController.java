package ai.maum.mlt.ta.xdc.controller;

import ai.maum.mlt.common.annotation.UriRoleDesc;
import ai.maum.mlt.common.git.GitManager;
import ai.maum.mlt.common.version.entity.CommitHistoryEntity;
import ai.maum.mlt.common.version.service.CommitHistoryService;
import ai.maum.mlt.ta.xdc.entity.XDCCategoryEntity;
import ai.maum.mlt.ta.xdc.entity.XDCDicEntity;
import ai.maum.mlt.ta.xdc.entity.XDCDicLineEntity;
import ai.maum.mlt.ta.xdc.service.XDCCategoryService;
import ai.maum.mlt.ta.xdc.service.XDCDicLineService;
import ai.maum.mlt.ta.xdc.service.XDCDicService;
import java.util.Map;
import org.eclipse.jgit.lib.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("api/ta/xdc/training-data")
public class XDCTrainingDataController {

  static final Logger logger = LoggerFactory.getLogger(XDCTrainingDataController.class);
  private static final String START = "start : {}";
  private static final String MESSAGE = "message";
  private static final String RSTCODE = "resultCode";
  private static final String SUCCESS = "SUCCESS";
  private static final String ERROR = "ERROR";
  private static final String TA_XDC = "/ta/xdc";
  private static final String WORKSPACEID = "workspaceId";

  @Value("${git.file.path}")
  private String gitPath;

  @Autowired
  private XDCDicService xdcDicService;

  @Autowired
  private XDCCategoryService xdcCategoryService;

  @Autowired
  private XDCDicLineService xdcDicLineService;

  @Autowired
  private CommitHistoryService commitHistoryService;

  @UriRoleDesc(role = "ta/xdc^R")
  @RequestMapping(
    value = "/getXdcDicAllList/{workspaceId}",
    method = RequestMethod.GET)
  public ResponseEntity<?> getXdcDicAllList(
      @PathVariable(WORKSPACEID) final String workspaceId) {
    logger.debug("start workspaceId: {}", workspaceId);
    try {
      return new ResponseEntity<>(xdcDicService.getXdcDicAllList(workspaceId), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/xdc^R")
  @RequestMapping(
    value = "/getXdcCategoryAllList/{workspaceId}/xdcDicId/{xdcDicId}",
    method = RequestMethod.GET)
  public ResponseEntity<?> getXdcCategoryAllList(
      @PathVariable(WORKSPACEID) final String workspaceId,
      @PathVariable("xdcDicId") final String xdcDicId) {
    logger.debug("start workspaceId :{}, xdcDicId :{}", workspaceId, xdcDicId);
    try {
      List<XDCCategoryEntity> list;
      list = xdcCategoryService.getCategoryTree(workspaceId, xdcDicId);
      return new ResponseEntity<>(list,
          HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getXdcCategoryAllList: {}", e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/xdc^C")
  @PostMapping("/insertCategory")
  public ResponseEntity<?> insertCategory(@RequestBody XDCCategoryEntity xdcCategoryEntity) {
    logger.debug(START, xdcCategoryEntity);
    HashMap<String, Object> result = new HashMap<>();
    try {
      XDCCategoryEntity duplicateCategoryEntity = isDuplicate(xdcCategoryEntity);
      if (duplicateCategoryEntity == null) {
        xdcCategoryEntity.setXdcDicId(xdcCategoryEntity.getXdcDicId());
        xdcCategoryEntity.setCreatedAt(new Date());
        XDCCategoryEntity resultCategoryEntity = xdcCategoryService
            .insertCategory(xdcCategoryEntity);
        result.put(MESSAGE, "INSERT_SUCCESS");
        result.put("category", resultCategoryEntity);
      } else {
        result.put(MESSAGE, "INSERT_DUPLICATED");
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      result.put(MESSAGE, "INSERT_FAILED");
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/xdc^U")
  @PostMapping("/updateCategory")
  public ResponseEntity<?> updateCategory(@RequestBody XDCCategoryEntity xdcCategoryEntity) {
    logger.debug(START, xdcCategoryEntity);
    HashMap<String, Object> result = new HashMap<>();
    try {
      XDCCategoryEntity duplicateCategoryEntity = isDuplicate(xdcCategoryEntity);
      if (duplicateCategoryEntity == null) {
        xdcCategoryEntity.setUpdatedAt(new Date());
        XDCCategoryEntity resultCategoryEntity = xdcCategoryService
            .updateCategory(xdcCategoryEntity);

        result.put("category", resultCategoryEntity);
        result.put(MESSAGE, "UPDATE_SUCCESS");
      } else {
        result.put(MESSAGE, "UPDATE_DUPLICATED");
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("updateCategory: {}", e.getMessage());
      result.put(MESSAGE, "UPDATE_FAILED");
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/xdc^D")
  @PostMapping("/deleteCategory")
  public ResponseEntity<?> deleteCategory(@RequestBody XDCCategoryEntity xdcCategoryEntity) {
    logger.debug(START, xdcCategoryEntity);
    HashMap<String, Object> result = new HashMap<>();
    try {
      if (xdcDicLineService
          .getAllDicLines(xdcCategoryEntity.getXdcDicId(), xdcCategoryEntity.getName()).size()
          == 0) {
        if (xdcCategoryService.deleteCategory(xdcCategoryEntity.getId())) {
          result.put(MESSAGE, "DELETE_SUCCESS");
        } else {
          result.put(MESSAGE, "DELETE_FAILED");
        }
      } else {
        result.put(MESSAGE, "SENTENCE_EXISTENCE");
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      result.put(MESSAGE, ERROR);
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/xdc^R")
  @PostMapping("/hasLines")
  public ResponseEntity<?> hasLines(@RequestBody XDCDicLineEntity xdcDicLineEntity) {
    logger.debug(START, xdcDicLineEntity);
    try {
      boolean result = true;
      if (xdcDicLineService
          .getAllDicLines(xdcDicLineEntity.getVersionId(), xdcDicLineEntity.getCategory()).size()
          == 0) {
        result = false;
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/xdc^R")
  @PostMapping("/getAllDicLines")
  public ResponseEntity<?> getAllDicLines(@RequestBody XDCDicLineEntity xdcDicLineEntity) {
    logger.debug(START, xdcDicLineEntity);
    HashMap<String, Object> result = new HashMap<>();
    try {

      Page<XDCDicLineEntity> pageList = xdcDicLineService
          .getAllDicLines(xdcDicLineEntity.getVersionId(), xdcDicLineEntity.getCategory(),
              xdcDicLineEntity.getSentence(), xdcDicLineEntity.getPageRequest());

      List<XDCDicLineEntity> resultList = pageList.getContent();

      result.put("total", pageList.getTotalElements());
      result.put("dicLineList", resultList);
      result.put(MESSAGE, SUCCESS);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      result.put(MESSAGE, ERROR);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/xdc^R")
  @RequestMapping(
    value = "/getDuplicatesLines/{versionId}/category/{category}",
    method = RequestMethod.GET)
  public ResponseEntity<?> getDuplicatesLines(@PathVariable("versionId") String versionId,
      @PathVariable("category") String category) {
    logger.debug("start versionId :{}, category :{}", versionId, category);
    HashMap<String, Object> result = new HashMap<>();
    try {
      List<XDCDicLineEntity> resultEntity = xdcDicLineService
          .getDuplicatesLines(versionId, category);
      if (!resultEntity.isEmpty()) {
        result.put(RSTCODE, "NO_DUPLICATION");
      } else {
        result.put("dicLine", resultEntity);
        result.put(RSTCODE, SUCCESS);
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      result.put(RSTCODE, ERROR);
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/xdc^D")
  @PostMapping("/deleteDuplicatesLines")
  public ResponseEntity<?> deleteDuplicatesLines(@RequestBody XDCDicLineEntity xdcDicLineEntity) {
    logger.debug(START, xdcDicLineEntity);

    try {
      boolean result = xdcDicLineService
          .deleteDuplicatesLines(xdcDicLineEntity.getVersionId(), xdcDicLineEntity.getCategory());
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/xdc^C")
  @PostMapping("/insertLine")
  public ResponseEntity<?> insertLine(@RequestBody XDCDicLineEntity xdcDicLineEntity) {
    logger.debug(START, xdcDicLineEntity);
    HashMap<String, Object> result = new HashMap<>();
    try {

      XDCDicLineEntity resultEntity = xdcDicLineService.insertLine(xdcDicLineEntity);
      result.put("dicLine", resultEntity);
      result.put(MESSAGE, SUCCESS);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      result.put(MESSAGE, ERROR);
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/xdc^D")
  @PostMapping("/deleteLines")
  public ResponseEntity<?> deleteLines(@RequestBody List<XDCDicLineEntity> xdcDicLineEntityList) {
    logger.debug(START, xdcDicLineEntityList);
    HashMap<String, String> result = new HashMap<>();
    try {
      for (XDCDicLineEntity xdcDicLineEntity : xdcDicLineEntityList) {
        xdcDicLineService.deleteLine(xdcDicLineEntity);
      }
      result.put(MESSAGE, SUCCESS);
      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      result.put(MESSAGE, ERROR);
      return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/xdc^C")
  @PostMapping("/upload-files")
  public ResponseEntity<?> insertFile(@RequestParam(WORKSPACEID) String workspaceId,
      @RequestParam("data") String xdcDicId,
      @RequestParam("file") MultipartFile file) {
    logger.debug("start workspaceId :{}, data :{}, files: {}", workspaceId, xdcDicId, file);
    try {
      xdcCategoryService.xdcFileUpload(xdcDicId, workspaceId, file);

      HashMap<String, Object> result = new HashMap<>();
      result.put("status", SUCCESS);

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/xdc^U")
  @PostMapping("/commit")
  public ResponseEntity<?> commit(@RequestBody XDCDicEntity xdcDicEntity) {
    logger.debug(START, xdcDicEntity);
    try {
      GitManager gs = new GitManager();
      String content = "";
      List<XDCDicLineEntity> xdcDicLineEntities = xdcDicLineService
          .getAllDicLinesByVersionId(xdcDicEntity.getId());

      /*내용 commit 리스트를 파일 하나로 파싱 */
      for (XDCDicLineEntity xdcDicLineEntity : xdcDicLineEntities) {
        content =
            content + xdcDicLineEntity.getSentence() + "\t" + xdcDicLineEntity.getCategory() + "\n";
      }

      HashMap<String, String> value = new HashMap<>();

      value.put("gitPath", gitPath + TA_XDC);
      value.put(WORKSPACEID, xdcDicEntity.getWorkspaceId());
      value.put("name", "admin");
      value.put("email", "mlt@ai");
      value.put("fileName", xdcDicEntity.getId());
      value.put("content", content);
      value.put("messsage", xdcDicEntity.getMessage());
      gs.updateFile(value);

      // 커밋히스토리에 적재
      CommitHistoryEntity entity = new CommitHistoryEntity();
      entity.setFileId(xdcDicEntity.getId());
      entity.setWorkspaceId(xdcDicEntity.getWorkspaceId());
      entity.setType("XDC");
      commitHistoryService.insertCommit(entity);

      return new ResponseEntity<>(null, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/xdc^R")
  @PostMapping("/getCommitList")
  public ResponseEntity<?> getCommitList(@RequestBody XDCDicEntity param) {
    logger.debug(START, param);

    try {
      GitManager gs = new GitManager();
      gs.setWorkingDirectory(gitPath + TA_XDC);

      List<HashMap<String, Object>> result = gs
          .getCommitList(param.getWorkspaceId(), param.getId());

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getCommitList: {}", e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/xdc^R")
  @PostMapping("/getDiff")
  public ResponseEntity<?> getDiff(@RequestBody Map<String, String> param) {
    logger.debug(START, param);

    try {
      GitManager gs = new GitManager();
      gs.setWorkingDirectory(gitPath + TA_XDC);

      String result = gs.diffFile(param.get(WORKSPACEID), param.get("id"), param.get("source"),
          param.get("target"));

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getDiff: {}", e.getMessage());
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UriRoleDesc(role = "ta/xdc^R")
  @PostMapping("/getFileByCommit")
  public ResponseEntity<?> getFileByCommit(@RequestBody Map<String, String> param) {
    logger.debug(START, param);

    try {
      GitManager gs = new GitManager();
      gs.setWorkingDirectory(gitPath + TA_XDC);

      String result = gs.readFileFromCommit(param.get(WORKSPACEID), param.get("id"),
          ObjectId.fromString(param.get("source")));

      return new ResponseEntity<>(result, HttpStatus.OK);
    } catch (Exception e) {
      logger.error("getFileByCommit: {}", e.getMessage());
      return new ResponseEntity<>(HttpStatus.FAILED_DEPENDENCY);
    }
  }

  private XDCCategoryEntity isDuplicate(XDCCategoryEntity xdcCategoryEntity) {
    if (xdcCategoryEntity.getUpdateState() == null) {
      return xdcCategoryService
          .getCategory(xdcCategoryEntity.getWorkspaceId(), xdcCategoryEntity.getXdcDicId(),
              xdcCategoryEntity.getName());
    } else {
      return null;
    }
  }
}
