package ai.maum.mlt.ta.hmd.service;

import ai.maum.mlt.ta.hmd.entity.HMDDicLineEntity;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface HMDDicLineService {

  List<HMDDicLineEntity> getCategoryList(HMDDicLineEntity hmdDicLineEntity);

  List<HMDDicLineEntity> getAllDicLinesByVersionId(String versionId);

  List<HMDDicLineEntity> getAllDicLines(String id, String category);

  Page<HMDDicLineEntity> getAllDicLines(String id, String category, String rule, Pageable pageable);

  HMDDicLineEntity insertLine(HMDDicLineEntity hmdDicLineEntity);

  void deleteByVersionId(HMDDicLineEntity hmdDicLineEntity);

  boolean deleteLine(HMDDicLineEntity hmdDicLineEntity);


}
