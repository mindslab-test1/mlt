package ai.maum.mlt.ta.xdc.repository;

import ai.maum.mlt.ta.xdc.entity.XDCTrainingEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface XDCTrainingRepository extends JpaRepository<XDCTrainingEntity, String> {

  List<XDCTrainingEntity> findAllByWorkspaceId(String workspaceId);
}
