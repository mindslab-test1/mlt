package ai.maum.mlt.ta.dnn.service;

import ai.maum.mlt.ta.dnn.entity.DNNTrainingEntity;

public interface DNNTrainingService {

  DNNTrainingEntity getTraining(String dnnKey);

  DNNTrainingEntity insertTraining(DNNTrainingEntity dnnTrainingEntity);

  void deleteTraining(String dnnKey);
}
