package ai.maum.mlt.ta.xdc.service;


import ai.maum.mlt.common.util.FileUtils;
import ai.maum.mlt.ta.xdc.entity.XDCCategoryEntity;
import ai.maum.mlt.ta.xdc.entity.XDCDicLineEntity;
import ai.maum.mlt.ta.xdc.repository.XDCCategoryRepository;
import ai.maum.mlt.ta.xdc.repository.XDCDicLineRepository;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class XDCCategoryServiceImpl implements XDCCategoryService {

  @Autowired
  private XDCCategoryRepository xdcCategoryRepository;

  @Autowired
  private XDCDicLineRepository xdcDicLineRepository;

  @Autowired
  private FileUtils fileUtils;

  @Autowired
  private XDCCategoryService xdcCategoryService;

  @Autowired
  private XDCDicLineService xdcDicLineService;

  @Override
  public List<XDCCategoryEntity> getCategoryAllList(String workspaceId, String xdcDicId) {
    return xdcCategoryRepository.getAllByWorkspaceIdAndXdcDicId(workspaceId, xdcDicId);
  }

  @Override
  public List<XDCCategoryEntity> getCategoryTree(String workspaceId, String xdcDicId) {

    List<XDCCategoryEntity> categoryList = getCategoryAllList(workspaceId, xdcDicId);
    List<XDCCategoryEntity> rootCategoryEntity = new ArrayList<>();

    for (XDCCategoryEntity xdcCategoryEntity : categoryList) {
      if (xdcCategoryEntity.getParentId() == null) {
        rootCategoryEntity
          .add(getChildCategory(categoryList, xdcCategoryEntity, xdcCategoryEntity.getId()));
      }
    }

    return rootCategoryEntity;
  }

  private XDCCategoryEntity getChildCategory(List<XDCCategoryEntity> categoryList,
                                             XDCCategoryEntity currentCategoryEntity, String parentCategoryId) {

    for (XDCCategoryEntity xdcCategoryEntity : categoryList) {
      List<XDCCategoryEntity> childrenObject = currentCategoryEntity.getChildren();
      if (parentCategoryId.equals(xdcCategoryEntity.getParentId())) {
        childrenObject
          .add(getChildCategory(categoryList, xdcCategoryEntity, xdcCategoryEntity.getId()));
      }
    }

    return currentCategoryEntity;
  }

  @Override
  public XDCCategoryEntity getCategory(String workspaceId, String xdcDicId, String name) {
    return this.xdcCategoryRepository
      .getByWorkspaceIdAndXdcDicIdAndName(workspaceId, xdcDicId, name);
  }

  @Override
  public XDCCategoryEntity findOneByNameAndXdcDicId(String name, String xdcDicId) {
    return this.xdcCategoryRepository.findOneByNameAndXdcDicId(name, xdcDicId);
  }

  @Transactional
  @Override
  public XDCCategoryEntity insertCategory(XDCCategoryEntity xdcCategoryEntity) {
    return this.xdcCategoryRepository.save(xdcCategoryEntity);
  }

  @Transactional
  @Override
  public XDCCategoryEntity updateCategory(XDCCategoryEntity xdcCategoryEntity) {
    XDCCategoryEntity result = this.xdcCategoryRepository.saveAndFlush(xdcCategoryEntity);

    //category dicLine update
    if (xdcCategoryEntity.getOldName() != null) {
      this.xdcDicLineRepository
        .updateDicLineCategory(xdcCategoryEntity.getName(), xdcCategoryEntity.getOldName());
    }
    return result;
  }

  @Override
  public boolean deleteCategory(String id) {
    try {
      this.xdcCategoryRepository.delete(id);
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  @Transactional
  @Override
  public void xdcFileUpload(String xdcDicId, String workspaceId, MultipartFile file) throws IOException {
    List<Map> fileObjects = null;
    String sentence;
    String category;

    fileObjects = fileUtils.formatFileToObject(file, "\t");

    this.xdcDicLineRepository.deleteByVersionId(xdcDicId);
    this.xdcCategoryRepository.deleteByXdcDicId(xdcDicId);

    for (Map object : fileObjects) {
      sentence = (String) object.get("column1");
      category = (String) object.get("column2");

      if (xdcCategoryService.findOneByNameAndXdcDicId(category, xdcDicId) == null) {
        XDCCategoryEntity xdcCategoryEntity = new XDCCategoryEntity();
        xdcCategoryEntity.setWorkspaceId(workspaceId);
        xdcCategoryEntity.setXdcDicId(xdcDicId);
        xdcCategoryEntity.setName(category);
        this.xdcCategoryService.insertCategory(xdcCategoryEntity);
      }

      XDCDicLineEntity xdcDicLineEntity = new XDCDicLineEntity();
      xdcDicLineEntity.setSentence(sentence);
      xdcDicLineEntity.setCategory(category);
      xdcDicLineEntity.setVersionId(xdcDicId);
      this.xdcDicLineService.insertLine(xdcDicLineEntity);
    }
  }
}
