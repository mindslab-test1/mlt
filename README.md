# MLT

## Prerequisites
- nodejs 8.10 or later
- angular-cli 6
- java 1.8: openjdk-8-jdk
- mysql
- oracle


### Install Prerequisites
```bash
# Ubuntu
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs
sudo apt-get install \
  openjdk-8-jdk \
  libssl-dev
(sudo) npm install -g angular-cli

# Centos
curl --silent --location https://rpm.nodesource.com/setup_8.x | bash -
sudo yum install -y nodejs
sudo yum install java-1.8.0-openjdk-devel.x86_64
(sudo) npm install -g angular-cli
```

## install cuda

### dkms
RedHat 7.3 & Centos7 CUDA 설치전에 의존성이 충족되지 않아 설치 오류 발생시 dkms 패키지 설치
```
rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
```

### Redhat 7.3 & Centos7 CUDA 설치
```
$ sudo rpm -Uvh cuda-repo-rhel7-8-0-local-ga2-8.0.61-1.x86_64.rpm
$ sudo yum install cuda
```

### get cuda
- https://developer.nvidia.com/cuda-downloads
- Select valid version for your OS

```bash
sudo dpkg -i cuda-repo-ubuntu1604_8.0.44-1_amd64.deb
sudo apt-get update
sudo apt-get install cuda
sudo apt-get install libarchive libarchive-dev
```


## How to build
- 아래의 모든 과정에서 ${MAUM_ROOT}가 사용됩니다. 기본값은 ~/maum 입니다.

- use './build.sh'
- ./build.sh ${MAUM_ROOT} or ./build.sh ~/maum
The following steps are more detail steps.
  - libmaum
  - brain-stt
  - brain-stt-train
  - brain-ta
  - brain-ta-train
  - brain-sds
  - brain-sds-train
  - paraphrase
  - server
  - client
  - config

### server (mlt-rest)

#### 서버 빌드
```bash
./build.sh ~$MAUM_ROOT rest
```

#### 환경 설정
```bash
cd $MAUM_ROOT/bin
./setup config
```

#### DB 환경 설정 
Mysql 기준
1. Mysql 계정 및 데이터 베이스 생성 
```bash
mysql> create user maum@localhost identified by 'ggoggoma';
mysql> create database mlt_tutor DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
mysql> grant all privileges on mlt_tutor.* to maum@localhost identified by 'ggoggoma';
mysql> flush privileges; 
```
2. Table 생성 
`$MAUM_ROOT/etc/mlt.conf`에
```
spring.jpa.hibernate.ddl-auto=create
``` 
수정후 mlt-rest 실행
정상적 실행후
`$MAUM_ROOT/etc/mlt.conf`에
```
spring.jpa.hibernate.ddl-auto=none
```
으로 변경  

3. 초기 데이터 생성
```
mysql> use mlt_tutor;
mysql> source <<init_data.sql 경로>>
```
`init_data.sql`은 `mlt소스경로/sql/init_data.sql`에서 받을수 있음
#### 서버 실행
```bash
svctl start mlt-rest
```
### client (mlt-proxy)
#### 패키지 설치
```bash
cd client
npm install
```

#### 실행
```bash
cd client
ng serve
```
기본적으로 --host localhost --port 4200으로 실행됩니다.
localhost에서 접속하는 경우가 아니라면 host 옵션에 공인 아이피를 입력해줍니다.

```bash
cd client
ng serve --host ... --port ...
```

http://hostname:port 에서 접속을 확인 할 수 있습니다.

#### 배포시 빋드 및 실행  
```bash
./build.sh $MAUM_ROOT web
svctl start mlt-proxy
```
### config
- supervisor를 위한 설정 파일들
- nginx 기본 설정 파일들
- mlt-rest 실행을 위한 설정 파일들
- 설정파일을 시스템의 설정에 맞게 배치하는 스크립트

### 빌드 완료 후 서비스 
~/maum/bin 디렉토리에서 아래 실행하면 서버를 재 부팅후 서비스 실행 
```
setup -p mlt -e prod config
svd
svctl
```
