node {
    // github status trigger
    properties([pipelineTriggers([[$class: "GitHubPushTrigger"]]), disableConcurrentBuilds()])

    // parameters
    def PROJECT_NAME = 'mlt'
    def MAUM_ROOT = "/CI/jenkins_build_space/${PROJECT_NAME}/${BRANCH_NAME}"

    try {
        timestamps {
            notifyBuild('STARTED')
            stage('Git Checkout') {
                timeout(60) {
                    checkout scm
                }
            }

            stage('Ubuntu Build') {
                withDockerContainer(image: 'maum-ubuntu-build:0.7', args: '-v /home/maum/.maum-build:/home/maum/.maum-build -v /home/maum/maum:/home/maum/maum -v /home/maum/git:/home/maum/git -v /CI/jenkins_build_space:/CI/jenkins_build_space -v /usr/lib/node_modules:/usr/lib/node_modules') {
                    timeout(time: 1, unit: 'HOURS') {
                        sh "LD_LIBRARY_PATH=${MAUM_ROOT}/lib ./build.sh ${MAUM_ROOT} all"
                    } 
                }
            }

            stage('CentOS tar Build') {
                withDockerContainer(image: 'maum-dev-centos:0.23', args: '-v /home/maum/.maum-build:/home/maum/.maum-build -v /home/maum/maum:/home/maum/maum -v /home/maum/git:/home/maum/git -v /CI/jenkins_build_space:/CI/jenkins_build_space -v /usr/lib/node_modules:/usr/lib/node_modules') {
                    timeout(time: 1, unit: 'HOURS') {
                        sh "LD_LIBRARY_PATH=${MAUM_ROOT}/lib ./build.sh tar"
                    }
                }
            }

            stage('CentOS tar Deploy') {
                sshPublisher(publishers: [
                    sshPublisherDesc(
                        configName: 'maum deploy',
                        transfers: [sshTransfer(
                                cleanRemote: false,
                                excludes: '',
                                // 파일 전송이 완료된 후 실행할 command를 추가
                                execCommand: '',
                                execTimeout: 120000,
                                flatten: false,
                                makeEmptyDirs: false,
                                noDefaultExcludes: false,
                                patternSeparator: '[, ]+',
                                // 다른 repo 빌드일 경우 배포 서버에 폴더 생성 후 해당 폴더로 변경
                                remoteDirectory: 'mlt',
                                remoteDirectorySDF: false,
                                removePrefix: '',
                                // Jenkins WORKSPACE가 기본 경로임
                                sourceFiles: "out/mlt*"
                        )],
                        usePromotionTimestamp: false,
                        useWorkspaceInPromotion: false,
                        // sshPublisher plug-in 진행 상세 내용을 console out에 표시해줌
                        verbose: true
                    )
                ])
            }
        }
    }
    catch (err) {
        currentBuild.result = "FAILED"
        throw err
    }
    finally {
        // Success or failure, always send notifications
        notifyBuild(currentBuild.result)
    }
}

// build notifications에 대한 설정
void notifyBuild(String buildStatus = 'STARTED') {
    // build status of null means successful
    buildStatus =  buildStatus ?: 'SUCCESSFUL'

    // Default values
    def colorName = 'RED'
    def colorCode = '#FF0000'
    def subject = "${JOB_NAME} Jenkins Build Result : ${buildStatus} (${BUILD_NUMBER})"
    def details = """
        <p>Hi,</p>
        <p>Your commit is ${buildStatus} at Jenkins build.</p>
        <p>Please check the console output : <a href="${env.BUILD_URL}consoleFull">Click here</a></p>
        <p>Best regard, Thanks.</p>
    """

    // Override default values based on build status
    if (buildStatus == 'STARTED') {
        color = 'YELLOW'
        colorCode = '#FFFF00'
    } else if (buildStatus == 'SUCCESSFUL') {
        color = 'GREEN'
        colorCode = '#00FF00'
    } else {
        color = 'RED'
        colorCode = '#FF0000'
    }

    // Send notifications
    emailext (
        subject: subject,
        body: details,
        recipientProviders: [[$class: 'DevelopersRecipientProvider']]
    )
}