-- basicqa는 brain-qa에서 관리하는 테이블이지만 mlt와도 밀접한 관계가 있으므로 형상관리 합니다

CREATE TABLE `BRAIN_BQA2_WORKSPACE` (
  `WORKSPACE_ID` int NOT NULL COMMENT '워크스페이스 아이디',
  `CATEGORY_ID` int NOT NULL COMMENT '카테고리 아이디'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='워크스페이스 매치';

CREATE TABLE `BRAIN_BQA2_ANSWER` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '아이디',
  `ANSWER` text NOT NULL COMMENT '답변',
  `SUMMARY` text DEFAULT NULL COMMENT '요약답변',
  `SRC` varchar(512) NULL COMMENT '출처',
  `CATEGORY_ID` int NOT NULL COMMENT '카테고리ID',
  `ATTR1` varchar(256) NULL COMMENT '속성1',
  `ATTR2` varchar(256) NULL COMMENT '속성2',
  `ATTR3` varchar(256) NULL COMMENT '속성3',
  `ATTR4` varchar(256) NULL COMMENT '속성4',
  `LAYER1_ID` int NULL COMMENT '계층1 ID',
  `LAYER2_ID` int NULL COMMENT '계층2 ID',
  `LAYER3_ID` int NULL COMMENT '계층3 ID',
  `LAYER4_ID` int NULL COMMENT '계층4 ID',
  `CREATOR_ID` varchar(40) NULL COMMENT '만든사람',
  `UPDATER_ID` varchar(40) NULL COMMENT '수정한사람',
  `CREATE_DTM` datetime NOT NULL COMMENT '만든날짜',
  `UPDATE_DTM` datetime NOT NULL COMMENT '수정날짜',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='답변필드';

CREATE TABLE `BRAIN_BQA2_QUESTION` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '아이디',
  `ANSWER_ID`int NOT NULL COMMENT '답변아이디',
  `QUESTION` text NOT NULL COMMENT '질문',
  `SRC` varchar(512) NULL COMMENT '출처',
  `CATEGORY_ID` int NOT NULL COMMENT '카테고리 ID',
  `ATTR1` varchar(256) NULL COMMENT '속성1',
  `ATTR2` varchar(256) NULL COMMENT '속성2',
  `ATTR3` varchar(256) NULL COMMENT '속성3',
  `ATTR4` varchar(256) NULL COMMENT '속성4',
  `CREATOR_ID` varchar(40) NULL COMMENT '만든사람',
  `UPDATER_ID` varchar(40) NULL COMMENT '수정한사람',
  `CREATE_DTM` datetime NOT NULL COMMENT '만든날짜',
  `UPDATE_DTM` datetime NOT NULL COMMENT '수정날짜',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='질문필드';

create table `BRAIN_BQA2_LAYER`
(
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '아이디',
  `NAME` varchar(256) not null comment 'Layer 이름',
  `LAYER_SECTION` int not null comment 'Layer 구분(1,2,3,4)',
  `CREATOR_ID` varchar(40) NULL COMMENT '만든사람',
  `UPDATER_ID` varchar(40) NULL COMMENT '수정한사람',
  `CREATE_DTM` datetime NOT NULL COMMENT '만든날짜',
  `UPDATE_DTM` datetime NOT NULL COMMENT '수정날짜',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Layer';

CREATE TABLE `BRAIN_BQA2_CATEGORY` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '아이디',
  `NAME` VARCHAR(256) not null comment '카데고리명',
  `CREATOR_ID` varchar(40) NULL COMMENT '만든사람',
  `UPDATER_ID` varchar(40) NULL COMMENT '수정한사람',
  `CREATE_DTM` datetime NOT NULL COMMENT '만든날짜',
  `UPDATE_DTM` datetime NOT NULL COMMENT '수정날짜',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='카테고리';

CREATE TABLE `BRAIN_BQA2_TAG` (
  `ID` int NOT NULL AUTO_INCREMENT COMMENT '아이디',
  `NAME` VARCHAR(256) not null comment '태그명',
  `CREATOR_ID` varchar(40) COMMENT '만든사람',
  `UPDATER_ID` varchar(40) COMMENT '수정한사람',
  `CREATE_DTM` datetime NOT NULL COMMENT '만든날짜',
  `UPDATE_DTM` datetime NOT NULL COMMENT '수정날짜',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='태그명';

CREATE TABLE `BRAIN_BQA2_ANSWER_TAG_REL` (
  `ANSWER_ID` int NOT NULL COMMENT '답변 ID',
  `TAG_ID` int NOT NULL COMMENT '태그 아이디',
  `CREATE_DTM` datetime NOT NULL COMMENT '만든날짜',
  `UPDATE_DTM` datetime NOT NULL COMMENT '수정날짜',
  PRIMARY KEY (`ANSWER_ID`, `TAG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='답변 태그 Rel';