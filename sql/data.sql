INSERT INTO MLT_MENU (id,path) VALUES ('0','/mlt/dashboard');
INSERT INTO MLT_MENU (id,path) VALUES ('1','/mlt/stt/lm/file-management');
INSERT INTO MLT_MENU (id,path) VALUES ('2','/mlt/stt/lm/transcription');
INSERT INTO MLT_MENU (id,path) VALUES ('3','/mlt/stt/lm/transcription/:id');
INSERT INTO MLT_MENU (id,path) VALUES ('4','/mlt/stt/lm/training');
INSERT INTO MLT_MENU (id,path) VALUES ('5','/mlt/stt/am/file-management');
INSERT INTO MLT_MENU (id,path) VALUES ('6','/mlt/stt/am/transcription');
INSERT INTO MLT_MENU (id,path) VALUES ('7','/mlt/stt/am/transcription/:id');
INSERT INTO MLT_MENU (id,path) VALUES ('8','/mlt/stt/am/quality-assurance');
INSERT INTO MLT_MENU (id,path) VALUES ('9','/mlt/stt/am/quality-assurance/:id');
INSERT INTO MLT_MENU (id,path) VALUES ('10','/mlt/stt/am/training');
INSERT INTO MLT_MENU (id,path) VALUES ('11','/mlt/stt/analysis/analyze');
INSERT INTO MLT_MENU (id,path) VALUES ('12','/mlt/stt/analysis/result');
INSERT INTO MLT_MENU (id,path) VALUES ('13','/mlt/stt/evaluation/evaluate');
INSERT INTO MLT_MENU (id,path) VALUES ('14','/mlt/stt/evaluation/result');
INSERT INTO MLT_MENU (id,path) VALUES ('15','/mlt/tripleWiki/schedule');
INSERT INTO MLT_MENU (id,path) VALUES ('16','/mlt/tripleWiki/history');
INSERT INTO MLT_MENU (id,path) VALUES ('17','/mlt/mrc/passage-list');
INSERT INTO MLT_MENU (id,path) VALUES ('18','/mlt/mrc/passage-detail');
INSERT INTO MLT_MENU (id,path) VALUES ('19','/mlt/mrc/passage-upsert');
INSERT INTO MLT_MENU (id,path) VALUES ('20','/mlt/mrc/passage-indexing');
INSERT INTO MLT_MENU (id,path) VALUES ('21','/mlt/mrc/training-data-list');
INSERT INTO MLT_MENU (id,path) VALUES ('22','/mlt/mrc/training-data-detail');
INSERT INTO MLT_MENU (id,path) VALUES ('23','/mlt/mrc/training-data-upsert');
INSERT INTO MLT_MENU (id,path) VALUES ('24','/mlt/mrc/training-data-upsert/:cId');
INSERT INTO MLT_MENU (id,path) VALUES ('25','/mlt/mrc/data-group');
INSERT INTO MLT_MENU (id,path) VALUES ('26','/mlt/mrc/training');
INSERT INTO MLT_MENU (id,path) VALUES ('27','/mlt/mrc/category');
INSERT INTO MLT_MENU (id,path) VALUES ('28','/mlt/management/users');
INSERT INTO MLT_MENU (id,path) VALUES ('29','/mlt/management/users-upsert');
INSERT INTO MLT_MENU (id,path) VALUES ('30','/mlt/management/roles');
INSERT INTO MLT_MENU (id,path) VALUES ('31','/mlt/management/profile');
INSERT INTO MLT_MENU (id,path) VALUES ('32','/mlt/management/histories');
INSERT INTO MLT_MENU (id,path) VALUES ('33','/mlt/ta/file-management');
INSERT INTO MLT_MENU (id,path) VALUES ('34','/mlt/ta/model-management');
INSERT INTO MLT_MENU (id,path) VALUES ('35','/mlt/ta/hmd/dictionary');
INSERT INTO MLT_MENU (id,path) VALUES ('36','/mlt/ta/hmd/analysis/analyze');
INSERT INTO MLT_MENU (id,path) VALUES ('37','/mlt/ta/hmd/analysis/result');
INSERT INTO MLT_MENU (id,path) VALUES ('38','/mlt/ta/hmd/analysis/test');
INSERT INTO MLT_MENU (id,path) VALUES ('39','/mlt/ta/dnn/training-data');
INSERT INTO MLT_MENU (id,path) VALUES ('40','/mlt/ta/dnn/training');
INSERT INTO MLT_MENU (id,path) VALUES ('41','/mlt/ta/dnn/analysis/analyze');
INSERT INTO MLT_MENU (id,path) VALUES ('42','/mlt/ta/dnn/analysis/result');
INSERT INTO MLT_MENU (id,path) VALUES ('43','/mlt/ta/dnn/analysis/test');
INSERT INTO MLT_MENU (id,path) VALUES ('44','/mlt/ta/tools/nlp-test');
INSERT INTO MLT_MENU (id,path) VALUES ('45','/mlt/basicqa/items');
INSERT INTO MLT_MENU (id,path) VALUES ('46','/mlt/basicqa/schedule');
INSERT INTO MLT_MENU (id,path) VALUES ('47','/mlt/basicqa/question');
INSERT INTO MLT_MENU (id,path) VALUES ('48','/mlt/basicqa/paraphrase');
INSERT INTO MLT_MENU (id,path) VALUES ('49','/mlt/basicqa/category');
INSERT INTO MLT_MENU (id,path) VALUES ('50','/mlt/dictionary/morpheme');
INSERT INTO MLT_MENU (id,path) VALUES ('51','/mlt/dictionary/compound_noun');
INSERT INTO MLT_MENU (id,path) VALUES ('52','/mlt/dictionary/preprocess');
INSERT INTO MLT_MENU (id,path) VALUES ('53','/mlt/dictionary/postprocess');
INSERT INTO MLT_MENU (id,path) VALUES ('54','/mlt/dictionary/wise-nlu-ner');
INSERT INTO MLT_MENU (id,path) VALUES ('55','/mlt/dictionary/wise-nlu-ner-category');
INSERT INTO MLT_MENU (id,path) VALUES ('56','/mlt/paraphrase/list');
INSERT INTO MLT_MENU (id,path) VALUES ('57','/mlt/paraphrase/step');
INSERT INTO MLT_MENU (id,path) VALUES ('58','/mlt/paraphrase/synonym');
INSERT INTO MLT_MENU (id,path) VALUES ('59','/mlt/paraphrase/ending-post-position');
INSERT INTO MLT_MENU (id,path) VALUES ('60','/mlt/dictionary/ner/manage/corpus');
INSERT INTO MLT_MENU (id,path) VALUES ('61', '/mlt/dictionary/ner/manage/tags');

INSERT INTO MLT_WORKSPACE (id,langCode,name) VALUES ('ff808181635c49e101635c4ceeaf0001', 'kor', 'korean');
INSERT INTO MLT_WORKSPACE (id,langCode,name) VALUES ('ff808181635c49e101635c50d2ed0003', 'eng', 'english');

INSERT INTO MLT_ROLE (id,roleName,workspaceId) VALUES ('dashboard','dashboard','ff808181635c49e101635c4ceeaf0001');
INSERT INTO MLT_ROLE (id,roleName,workspaceId) VALUES ('admin','admin','ff808181635c49e101635c4ceeaf0001');
INSERT INTO MLT_USER (id,password,username,activated, createdat, updatedat, PASSUPDATEDAT) VALUES ('admin','3c9909afec25354d551dae21590bb26e38d53f2173b8d3dc3eee4c047e7ab1c1eb8b85103e3be7ba613b31bb5c9c36214dc9f14a42fd7a2fdb84856bca5c44c2','어드민',1, SYSDATE, SYSDATE, SYSDATE);
INSERT INTO MLT_USER_ROLE_REL (userId, roleId) VALUES ('admin', 'admin');

INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('dashboard','0');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','0');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','1');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','2');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','3');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','4');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','5');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','6');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','7');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','8');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','9');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','10');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','11');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','12');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','13');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','14');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','15');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','16');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','17');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','18');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','19');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','20');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','21');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','22');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','23');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','24');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','25');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','26');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','27');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','28');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','29');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','30');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','31');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','32');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','33');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','34');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','35');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','36');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','37');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','38');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','39');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','40');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','41');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','42');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','43');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','44');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','45');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','46');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','47');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','48');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','49');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','50');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','51');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','52');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','53');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','54');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','55');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','56');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','57');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','58');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','59');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','60');
INSERT INTO MLT_MENU_ROLE (roleId,menuId) VALUES ('admin','61');

INSERT INTO MLT_CODE (id,name, groupCode) VALUES ('1','context', 'MRC_DATA_GRP');
INSERT INTO MLT_CODE (id,name, groupCode) VALUES ('2','dataType', 'MRC_DATA_GRP');
INSERT INTO MLT_CODE (id,name, groupCode) VALUES ('3','channel', 'BASICQA_DATA_GRP');

INSERT INTO MLT_MRC_CATEGORY(id,catName,catPath,parCatId,workspaceId) values ('ff808181635cfe0c01635cffcf880001','동계','동계','question','ff808181635c49e101635c4ceeaf0001');
INSERT INTO MLT_MRC_CATEGORY(id,catName,catPath,parCatId,workspaceId) values ('ff808181635cfe0c01635cffcf890002','빙상','동계>빙상','ff808181635cfe0c01635cffcf880001','ff808181635c49e101635c4ceeaf0001');
INSERT INTO MLT_MRC_CATEGORY(id,catName,catPath,parCatId,workspaceId) values ('ff808181635cfe0c01635cffcf890003','올림픽','동계>빙상>올림픽','ff808181635cfe0c01635cffcf890002','ff808181635c49e101635c4ceeaf0001');
INSERT INTO MLT_MRC_CATEGORY(id,catName,catPath,parCatId,workspaceId) values ('ff808181635cfe0c01635cffcf890004','컬링','동계>빙상>올림픽>컬링','ff808181635cfe0c01635cffcf890003','ff808181635c49e101635c4ceeaf0001');
INSERT INTO MLT_MRC_CATEGORY(id,catName,catPath,parCatId,workspaceId) values ('ff808181635cfe0c01635cffcf8a0005','하계','하계','context','ff808181635c49e101635c4ceeaf0001');
INSERT INTO MLT_MRC_CATEGORY(id,catName,catPath,parCatId,workspaceId) values ('ff808181635cfe0c01635cffcf8a0006','올림픽','하계>올림픽','ff808181635cfe0c01635cffcf8a0005','ff808181635c49e101635c4ceeaf0001');
INSERT INTO MLT_MRC_CATEGORY(id,catName,catPath,parCatId,workspaceId) values ('ff808181635cfe0c01635cffcf8a0007','육상','하계>올림픽>육상','ff808181635cfe0c01635cffcf8a0006','ff808181635c49e101635c4ceeaf0001');
INSERT INTO MLT_MRC_CATEGORY(id,catName,catPath,parCatId,workspaceId) values ('ff808181635cfe0c01635cffcf8a0008','단거리','하계>올림픽>육상>단거리','ff808181635cfe0c01635cffcf8a0007','ff808181635c49e101635c4ceeaf0001');
INSERT INTO MLT_MRC_CATEGORY(id,catName,catPath,parCatId,workspaceId) values ('ff808181635cfe0c01635cffcf8b0009','Wiki','Wiki','dataType','ff808181635c49e101635c4ceeaf0001');
INSERT INTO MLT_MRC_CATEGORY(id,catName,catPath,parCatId,workspaceId) values ('ff808181635cfe0c01635cffcf8b000a','d2','Wiki>d2','ff808181635cfe0c01635cffcf8b0009','ff808181635c49e101635c4ceeaf0001');
INSERT INTO MLT_MRC_CATEGORY(id,catName,catPath,parCatId,workspaceId) values ('ff808181635cfe0c01635cffcf8b000b','d3','Wiki>d2>d3','ff808181635cfe0c01635cffcf8b000a','ff808181635c49e101635c4ceeaf0001');
INSERT INTO MLT_MRC_CATEGORY(id,catName,catPath,parCatId,workspaceId) values ('ff808181635cfe0c01635cffcf7f0000','d4','Wiki>d2>d3>d4','ff808181635cfe0c01635cffcf8b000b','ff808181635c49e101635c4ceeaf0001');

INSERT INTO MLT_MRC_DATA_GROUP (id, dataGroupName, workspaceId) values ('ff808181635d65ee01635d68aa04003d', '올림픽', 'ff808181635c49e101635c4ceeaf0001');

INSERT INTO MLT_FILE_GROUP (id,name, purpose, workspaceId) VALUES ('ff808181635d445401635d4507ef0001','fileGroup1', 'STT', 'ff808181635c49e101635c4ceeaf0001');
INSERT INTO MLT_FILE_GROUP (id,name, purpose, workspaceId) VALUES ('ff808181635d445401635d4507ef0002','fileGroup2', 'STT', 'ff808181635c49e101635c4ceeaf0001');
INSERT INTO MLT_FILE_GROUP (id,name, purpose, workspaceId) VALUES ('ff808181635d445401635d4507ef0003','fileGroup3', 'STT', 'ff808181635c49e101635c4ceeaf0001');
INSERT INTO MLT_FILE_GROUP (id,name, purpose, workspaceId) VALUES ('ff808181635d445401635d4507f00004','fileGroup1', 'TA', 'ff808181635c49e101635c4ceeaf0001');
INSERT INTO MLT_FILE_GROUP (id,name, purpose, workspaceId) VALUES ('ff808181635d445401635d4507f00005','fileGroup2', 'TA', 'ff808181635c49e101635c4ceeaf0001');
INSERT INTO MLT_FILE_GROUP (id,name, purpose, workspaceId) VALUES ('ff808181635d445401635d4507f00006','fileGroup3', 'TA', 'ff808181635c49e101635c4ceeaf0001');


INSERT INTO MLT_FILE ( id ,  createdAt ,  duration ,  hash ,  name ,  purpose ,  fileSize ,  type ,  updatedAt ,  workspaceId ) VALUES ('20180207111446178', '2018-02-07 11:14:46', '369', '0d546ef312ef9e214907fff7b61273937d4c7e8b', '201800043610181I.wav', 'STT', '5900238', 'audio/wav', '2018-02-07 11:14:46', 'ff808181635c49e101635c4ceeaf0001');

-- INSERT INTO Transcript( id ,  createdAt ,  fileId ,  updatedAt ,  workspaceId ,  version ) VALUES ('1', '2018-02-07 11:14:46', '20180207111446178', '2018-02-07 11:14:46', '0', '행복을 드리는 신한카드 상담원 이진입니다 고갬 무엇을 도와드릴까요 예 안녕하세요 저 빅플러스카드 쓰고 있는데요 네 예 그 지에스칼텍스에서 저기 어 주유 하면 적립되는거요 네네
--     예 근데 구월 제가 얼마전 주유해보니까 구월부터 포인트가 전혀 적립 안되있더라구요 아 네 적립이 안되어있으신단 부분이신데요 제가 확인을 도와드릴게요 고객님 네 네 최보연고객님 본인이시구요 예
--     아 반갑습니다 결제계좌는 어느은행이세요 고객님 케이비입니다 네 결제하시는날짜 매달 며칠이세요 십 오일입니다 네 소중한정보확인감사드립니다 말씀주셨던 포인트적립부분 확인 해보겠는데요
--     잠시만기다려주세요 네 네 기다려주셔서 감사합니다 말씀주신 그 카드로해서 지에스칼텍스 포인트가 적립이되시기 위해서는 전월 일일부터 말일까지의 실적부분이 좀 충족이 되셔야지만 이번달에
--     예예 주유하시는 부분에 대해서 리터당 적립을 받아보실수 있는데요 현재 소지하고 계신 국방 복지카드라든가 케이티쿡센터카드로는 전월실적부분이 주유 금액을 제외한금액이 일일부터 말일까지
--     이십만원이상이 되셔야되요 네 주유 그 할인 할인도 포함아닌가요 네 고객님 주유하신금액은 아예 제외가 되시구 다른쪽으로 사용되신 금액이 일시불 할부건이 이십만원 이상이셔야지만 이번달에 지에스칼텍스
--     에서 주유하실때 적립을 받아보실수 있는 부분이세요 예 근데 제가 그 조회해보시면 알겠지만 매달 한 사십몇만원씩 해서 내고 있는데요 네 고객님께서결제하시는 금액 기준이 아니시구요 실적부분은
--     전월 일일부터 말일까지기준이시구요 네 예 최근에지금 십일월달까지 지금 쿡세트카드로서 조회를해보자면요 주유업종을 제외하시고는 금액부분이 이십만원이 채 안되시는 부분으로는 확인되셨어요
--     아 그 할인은요 그 할인 그까 할인이 아니라 그 잠깐만요 어 매달 할부금액 있었을텐데 이십만원씩 할부금액이요 할부 예예 사용건은 그 처음 사용하신 한달에 실적이 다 포함이 되세요 고객님께서
--     할부를 나눠논 나눠서 결제는 하시지만 그부분 실적이 달달이 들어가는게 아니구요 한꺼번에 사용하신 날짜에 그 달에 포함이 되시는거에요 아 첫 달에 그럼요 네네 맞습니다 이게 언제부터 이렇게 바꼈나요
--     빅플러스포인트가 적립되시는 그 내용 부분은 작년 시월달부터 저희가 변경이되었습니다 아 그래요 네네 그믄 음 그래요 그면 이게 이거와 비슷한 주유할때 혜택보는 이거 주유 비슷한 카드가 뭐있나요
--     어 주유하실때 그러시면 이부분은 인제 그 지에스칼텍스빅플러스 포인트로 적립을 받으시는 카드가 이렇게 있구요 이 카드 외에 전월실적 관계 없이 저희가 포인트 지급 해드리는 그 알피엠 카드라고는 있어요
--     예 네 알피엠카드는 말씀드린것처럼 전월 뭐 실적 상관없이 총 그달에 주유하시는 삼십만원의 주유금액 까지는 리터당 백원씩 저희가 적 포인트로 적립해드리구요 네 횟수로는 월 사회까지 금액은 말씀드렸던것처럼
--     삼십만원의 주유금액까지 적립해드리구 이부분 카드의 연회비부분은 조금 지금의 이 현재 카드와는 조금 더 나가시는 부분으로 국내전용으로 하시게되시면은 이만칠천원 확인되시구요 네 전월실적부분이 없는대신에
--     포인트지급은 저희가 더 많이 해드리는 카드로는 있으세요 어 그렇군요 일단은 무슨말씀인지 알겠습니다 카드는 어 예 하튼 그 다시 정리를 해주시면 할부빼고 주유량도 빼고 나머지는 다된다구요
--     네네 주유 지에스주유소에서 고객님께서 적립 받기위해서는요 주유하신금액은 제외하고 기준은 전월 일일부터 말일까지 일시불 할부실적이 이십만원이상 되셨을때 이번달에 지에스에서 주유하신금액
--     리터당 적립받으실수 있는 부분이세요 음 알겠습니다 그 이거 저기 리터당 얼마씩 적립이 되나요 다시한번 확인해드릴게요 예 리터당 팔시건 팔십원적립확인되십니다 고객님 팔십원은 그냥 고정이에요 뭐 적립시스
--     그까카드실적에 따라 이런게 아니구요 네네 고객님 전월실적만 충족이 되신다면 이번달에 주유하신 금액에 지에스 칼텍스 주유하신금액에 리터당 팔십원 적립이시구요 이제 다만 이부분도 좀 적립제한은 있는데요
--     일회주유시 십오만원까지에 사용금액에 대해서만 적립되시구 월 주유금액 삼십만원까지만 적립되시는부분도 참고부탁드리구요 별도로 포인트 유효기간도 오년 있으신걸로 확인되십니다 네 그 작년
--     시월에 바꼈다는 내용에 대해서 인지를 못하고 있었어요 아 그러세요 네 저희가 서비스가 변경이 되시거나 하실때에는 그 서비스 변경되시기 그 다섯달을 내지 여섯달 전부터는 명세서 부분을 통해서 고객님께
--     안내를 해드렸던 부분이신데 이부분 못받아보셨나요 아 예 알겠습니다 무슨말인지 알겠습니다 감사합니다 네 감사합니다 저는 상담원 이미진이였구요 오늘하루행복하세요 예 네');

INSERT INTO MLT_FILE_GROUP_REL ( id ,  createdAt ,  fileGroupId ,  fileId ) VALUES ('2', '2018-02-07 11:18:02', 'ff808181635d445401635d4507ef0001', '20180207111446178');

INSERT INTO MLT_DNN_DIC (id, workspaceId, name) values ('ff808181635d445401635d4507f1000a', 'ff808181635c49e101635c4ceeaf0001', 'dnndic1');
INSERT INTO MLT_DNN_DIC (id, workspaceId, name) values ('ff808181635d445401635d451bde000c', 'ff808181635c49e101635c4ceeaf0001', 'dnndic2');
INSERT INTO MLT_DNN_DIC (id, workspaceId, name) values ('ff808181635d445401635d4dbf5b000e', 'ff808181635c49e101635c4ceeaf0001', 'dnndic3');
INSERT INTO MLT_DNN_DIC (id, workspaceId, name) values ('ff808181635d445401635d4dbf5b000f', 'ff808181635c49e101635c4ceeaf0001', 'dnndic4');

INSERT INTO MLT_DNN_CATEGORY (id, dnnDicId, workspaceId, name) values ('ff808181635d445401635d4dbf5c0010', 'ff808181635d445401635d4507f1000a', 'ff808181635c49e101635c4ceeaf0001', 'cate1');
INSERT INTO MLT_DNN_CATEGORY (id, dnnDicId, parentId, workspaceId, name) values ('ff808181635d445401635d4dbf5d0011', 'ff808181635d445401635d4507f1000a', 'ff808181635d445401635d4dbf5c0010','ff808181635c49e101635c4ceeaf0001','cate2');
INSERT INTO MLT_DNN_CATEGORY (id, dnnDicId, parentId, workspaceId, name) values ('ff808181635d445401635d4dbf5d0012', 'ff808181635d445401635d4507f1000a', 'ff808181635d445401635d4dbf5d0011','ff808181635c49e101635c4ceeaf0001','cate3');
INSERT INTO MLT_DNN_CATEGORY (id, dnnDicId, parentId, workspaceId, name) values ('ff808181635d445401635d4dbf5e0013', 'ff808181635d445401635d4507f1000a', 'ff808181635d445401635d4dbf5d0012','ff808181635c49e101635c4ceeaf0001', 'cate4');

INSERT INTO MLT_DNN_CATEGORY (id, dnnDicId, workspaceId, name) values ('ff808181635d445401635d4dbf5f0014', 'ff808181635d445401635d4507f1000a', 'ff808181635c49e101635c4ceeaf0001', 'cate1-1');
INSERT INTO MLT_DNN_CATEGORY (id, dnnDicId, parentId, workspaceId, name) values ('ff808181635d445401635d4dbf5f0015', 'ff808181635d445401635d4507f1000a', 'ff808181635d445401635d4dbf5f0014','ff808181635c49e101635c4ceeaf0001','cate2-1');
INSERT INTO MLT_DNN_CATEGORY (id, dnnDicId, parentId, workspaceId, name) values ('ff808181635d445401635d4dbf600016', 'ff808181635d445401635d4507f1000a', 'ff808181635d445401635d4dbf5f0015','ff808181635c49e101635c4ceeaf0001','cate2-2');
INSERT INTO MLT_DNN_CATEGORY (id, dnnDicId, parentId, workspaceId, name) values ('ff808181635d445401635d4dbf610017', 'ff808181635d445401635d4507f1000a', 'ff808181635d445401635d4dbf600016','ff808181635c49e101635c4ceeaf0001','cate3-1');

INSERT INTO MLT_DNN_DIC_LINE (seq, category, sentence, versionId) values(1,'cate4', '안녕하세요.', 'ff808181635d445401635d4507f1000a');
INSERT INTO MLT_DNN_DIC_LINE (seq, category, sentence, versionId) values(2,'cate4', '테스트입니다.', 'ff808181635d445401635d4507f1000a');
INSERT INTO MLT_DNN_DIC_LINE (seq, category, sentence, versionId) values(3,'cate4', '더미입니다.', 'ff808181635d445401635d4507f1000a');

INSERT INTO MLT_STT_MODEL (id,name,status,sttKey,workspaceId)VALUES('ff80818160fd4a5c0160fd4be6730002', 'fileGroup1', 'success', 'c70a3ac6-802f-1f88-a32b-bc200efb909b', 'ff808181635c49e101635c4ceeaf0001');
INSERT INTO MLT_STT_MODEL (id,name,status,sttKey,workspaceId)VALUES('ff80818160fd4a5c0160fd4be6730102', 'test2', 'success', 'c70a3ac6-802f-2f88-a32b-bc200efb909b', 'ff808181635c49e101635c4ceeaf0001');
INSERT INTO MLT_STT_MODEL (id,name,status,sttKey,workspaceId)VALUES('ff80818160fd4a5c0160fd4be6730202', 'test3', 'success', 'c70a3ac6-802f-3f88-a32b-bc200efb909b', 'ff808181635c49e101635c4ceeaf0001');
INSERT INTO MLT_STT_MODEL (id,name,status,sttKey,workspaceId)VALUES('ff80818160fd4a5c0160fd4be6730302', 'test4', 'success', 'c70a3ac6-802f-4f88-a32b-bc200efb909b', 'ff808181635c49e101635c4ceeaf0001');

INSERT INTO MLT_HMD_DIC (id, workspaceId, name, creatorId, updaterId) values ('ff808181635d445401635d4dbf610018', 'ff808181635c49e101635c4ceeaf0001', 'hmddic1','admin','admin');
INSERT INTO MLT_HMD_DIC (id, workspaceId, name, creatorId, updaterId) values ('ff808181635d445401635d4dbf620019', 'ff808181635c49e101635c4ceeaf0001', 'hmddic2','admin','admin');
INSERT INTO MLT_HMD_DIC (id, workspaceId, name, creatorId, updaterId) values ('ff808181635d445401635d4dbf62001a', 'ff808181635c49e101635c4ceeaf0001', 'hmddic3','admin','admin');
INSERT INTO MLT_HMD_DIC (id, workspaceId, name, creatorId, updaterId) values ('ff808181635d445401635d4dbf63001b', 'ff808181635c49e101635c4ceeaf0001', 'hmddic4','admin','admin');

INSERT INTO MLT_HMD_CATEGORY (id, hmdDicId, workspaceId, name,updaterId) values ('ff808181635d445401635d4dbf64001c', 'ff808181635d445401635d4dbf610018', 'ff808181635c49e101635c4ceeaf0001', 'cate1','admin');
INSERT INTO MLT_HMD_CATEGORY (id, hmdDicId, parentId, workspaceId, name,updaterId) values ('ff808181635d445401635d4dbf65001d', 'ff808181635d445401635d4dbf610018', 'ff808181635d445401635d4dbf64001c','ff808181635c49e101635c4ceeaf0001','cate2','admin');
INSERT INTO MLT_HMD_CATEGORY (id, hmdDicId, parentId, workspaceId, name,updaterId) values ('ff808181635d445401635d4dbf66001e', 'ff808181635d445401635d4dbf610018', 'ff808181635d445401635d4dbf65001d','ff808181635c49e101635c4ceeaf0001','cate3','admin');
INSERT INTO MLT_HMD_CATEGORY (id, hmdDicId, parentId, workspaceId, name,updaterId) values ('ff808181635d445401635d4dbf66001f', 'ff808181635d445401635d4dbf610018', 'ff808181635d445401635d4dbf66001e','ff808181635c49e101635c4ceeaf0001', 'cate4','admin');

INSERT INTO MLT_HMD_CATEGORY (id, hmdDicId, workspaceId, name,updaterId) values ('ff808181635d445401635d4dbf660020', 'ff808181635d445401635d4dbf610018', 'ff808181635c49e101635c4ceeaf0001', 'cate1-1','admin');
INSERT INTO MLT_HMD_CATEGORY (id, hmdDicId, parentId, workspaceId, name,updaterId) values ('ff808181635d445401635d4dbf670021', 'ff808181635d445401635d4dbf610018', 'ff808181635d445401635d4dbf660020','ff808181635c49e101635c4ceeaf0001','cate2-1','admin');
INSERT INTO MLT_HMD_CATEGORY (id, hmdDicId, parentId, workspaceId, name,updaterId) values ('ff808181635d445401635d4dbf670022', 'ff808181635d445401635d4dbf610018', 'ff808181635d445401635d4dbf670021','ff808181635c49e101635c4ceeaf0001','cate2-2','admin');
INSERT INTO MLT_HMD_CATEGORY (id, hmdDicId, parentId, workspaceId, name,updaterId) values ('ff808181635d445401635d4dbf670023', 'ff808181635d445401635d4dbf610018', 'ff808181635d445401635d4dbf670022','ff808181635c49e101635c4ceeaf0001','cate3-1','admin');


INSERT INTO MLT_HMD_DIC_LINE (seq, category, rule, versionId) values(1, 'cate4', '(동해)', 'ff808181635d445401635d4dbf610018');
INSERT INTO MLT_HMD_DIC_LINE (seq, category, rule, versionId) values(2, 'cate4', '(백두산)', 'ff808181635d445401635d4dbf610018');

/*INSERT INTO STTAnalysisResult ( id ,  createdAt ,  fileId ,  model ,  text ,  transcriptId ,  updatedAt ,  workspaceId ) VALUES ('1', '2018-02-07 11:32:53', '20180207111446178', 'fileGroup1', '행복을 드리는 신한 카드 상담원이십니다\n고객님 무엇을 드릴까요\n예 안녕하세요\n저 키프로스 카드 질문데요\n네 예 그 GS 칼텍스 저기어 주요\n적립되는 거요\n네 네 예 금액 구월 제거 얼마 전 줄거리 구월부터 포인트로 적립안되더라구요\n아 네 적립이 안되예 신한 부분이신데요\n네 확인을 도와드릴께요\n고객님 네 칠 보유한 고객님 본인이시구요\n예 알겠습니다\n결제 계좌는 어느 은행이세요\n고객님 드립입니다\n네 결제하시는 날짜 기다려주세요\n십시오입니다\n네 사천번 확인 감사 드립니다\n말씀 드렸던 포인트 적립금번 확인해 보겠는데요\n잠시만 기다려주세요\n예 네는 기다려주셔서 감사합니다\n말씀 주신 그 카드로 해서 GS 칼텍스 포인트가 적립이 되시기 위해서는 전월 1일부터 말일까지 시작 부분이 좀 충전기되셔야지만 이번달에 뒤아시는 부분에 대해서 리터당 적립을 받아 보실 수 있는데요\n현재 또 사용 계신 국방 복지 카드라던가 KT 카드 포인트 카드로는 전월 실적 고객님 주유 금액을 제외한 금액이 1일부터 말일까지 20만원 이상이 되셔야데요\n네 주유 할인 카드 4만가요\n네 고객님 주유하신 금액은 아예 제외가 되시구 다른 쪽으로 사용되신 금액이 일시불할부 건이 십만원 이상이셔야 하지만 이번달에 GS 칼텍스에서 주유하실 때 적립을 받아 보실 수 있는 부분이세요\n예\n예 그게 그거 불법 사이트가 대단하다 지금 어떻게 되고 있는데요\n네 고객님께서 결제하시는 금액 기준이 아니시구요\n시작 부분은 전화 드릴부터 말일까지 기준이시구요\n예 예 최근에 지금 11월 달까지 지금 국세청 카드로에서 조회를 해보자면 주 요점을 제외하신 거는 금액도 분위기 십만 원이 체한듯이는 부분으로는 확인이 되셔써요\n고객님 그걸는요\n카드로 할인에서 그는요\n금 메달 할부 금액 있었는데 이 십만원씩 할 수 금액이요\n세번째 예 사용 거는 되 처음 사용하신한 카드실적니다\n포함이 되세요\n고객님께서 하입으로 나눠는 나눠서 결제는 하시지만 그 부분 실적이 발달이 들어가는 게 아니구요\n한꺼번에 사용하신 날짜에 그 달에 포함이 되신 거에요\n아 폭발이거든요\n네 맞습니다\n고 이게 어딜 그렇게 바꼈나요\n빅 플러스 포인트가 적립되시는 분 내용 부분은 작년 시월달부터 저희가 변경이 되었습니다\n아 그래요\n네 네 그렇 금네 그 이게 이거 비싼 그 주유할 때 핸드폰 이거 주유 시설 카드 가보 있나요\n아 주유하실 때 그렇 신한이 부분은 이제 그 GS 칼텍스 빅 플러스 포인트로 적립 웹하드 신한 카드가 이렇게 있구요\n이 카드 외에 전월 실적 관계 없이 저희가 포인트 지급해 드리는 그 rpm 카드라고는까요\n예 네 rpm 카드는 말씀드린 것처럼 전월 실적 상관없이 총 그 달에 주유하시는 30 만 원에 주유 금액까지는 리터당 100원씩 저희 가정 포인트를 적립을 해드리구요\n예 횟수 로는월 4회까지 금액은 말씀 드렸던 것처럼 30만 원에 주유 금액까지 적립해 드리고 이 부분 카드 연회비 오늘 조금이 지금 현재 카드와는 조금 더 나가시는 부분으로\n국내 전용으로 하시게 되시면 27000원 확인되시구요\n네 전화 예 전월 실적 부분이 없는 되시 네포인트 지금은 저희가 더 많이 해드리는 카드로는 있으세요\n음 그러면 그렇습니다\n이 생각하면은 뭐야 카드 그 사실도 미러리스 한도 떨구 그러면 더 빼고 나머지로 된다구요\n네 네 칠 GS 주유소에서 고객님께서 적립 받기 위해서는요\n주유하신 금액은 제외하고 기준은 전월 1일부터 말일까지 25십팔 보신 적이 이 십만원 이상 되셔 쓸 때 이번달에 GS에서 귀하신 금액이 타당 적립 받으실 수 있는 부분이세요\n예 알겠습니다\n그 이거 트위터 얼마씩 본인되나요\n예 다시 한번 확인해 드릴께요\n네 리터 당 8시간은 8시 반 적립 확인되십니다\n고객님할 수 이거는 저 뭐 적립십니까\n카드 실적에 따라있나요\n네 네 고객님 전화 실적만 충족이 되신다면 이번달에 주의하시는 금액에 GS 칼텍스주요\n신 금액에 리터당 80원 적립이시구요\n다만 이 부분도 좀 적립 제한은 있는데요\n1회 주유 시 15만 원까지에 사용 금액에 대해서 적립되시고 월 칠 금액 30만 원까지만 적립되시는 분들 참고 부탁 드리구요\n별도로 포인트 유효 기간 5년이 쓰신 건 확인되십니다\n그 작렬 조르바 그렇다는 내용에 대해서 인지를보톡스요\n아 그러세요\n저희가 서비스가 변경이 되시거나실 때네 그 서비스 변경되시길 그렇다서 다를 예지 여섯달 전부터는 명세서 부분을 통해서 금액이 안내를 해드렸던 부분이신가요\n도 못 받아 보셨나요\n아 예 알겠습니다\n부서장겠습니다\n감사합니다\n네 감사합니다\n전 상담원이신가요\n오늘도 행복하세요\n네 20\n', '1', '2018-02-07 11:32:53', '0');*/

/*INSERT INTO STTEvalResult ( id ,  createdAt ,  model ,  result ,  updatedAt ,  workspaceId ) VALUES ('1', '2018-02-07 11:39:27', 'fileGroup1', '{\"correctness\":\"75.51%\",\"total\":1727,\"hit\":1304,\"substitution\":287,\"deletion\":136,\"accuracy\":\"71.38%\",\"INSERTion\":28}', '2018-02-07 11:39:27', '0');*/

INSERT INTO MLT_BSQA_SKILL ( id , name , createAt , updateAt ) VALUES (1,'skill1','2018-02-07 16:08:19','2018-02-07 16:08:19');
INSERT INTO MLT_BSQA_SKILL ( id , name , createAt , updateAt ) VALUES (2,'skill2','2018-02-07 16:08:19','2018-02-07 16:08:19');
INSERT INTO MLT_BSQA_SKILL ( id , name , createAt , updateAt ) VALUES (3,'skill3','2018-02-07 16:08:19','2018-02-07 16:08:19');
INSERT INTO MLT_BSQA_SKILL ( id , name , createAt , updateAt ) VALUES (4,'skill4','2018-02-07 16:08:19','2018-02-07 16:08:19');
INSERT INTO MLT_BSQA_SKILL ( id , name , createAt , updateAt ) VALUES (5,'skill5','2018-02-07 16:08:19','2018-02-07 16:08:19');

INSERT INTO MLT_BSQA_CATEGORY(id,catName,catPath,parCatId) VALUES ('ff808181635d65ee01635d68aa010034','채널','채널','channel');
INSERT INTO MLT_BSQA_CATEGORY(id,catName,catPath,parCatId) VALUES ('ff808181635d65ee01635d68aa010035','대분류','채널>대분류','ff808181635d65ee01635d68aa010034');
INSERT INTO MLT_BSQA_CATEGORY(id,catName,catPath,parCatId) VALUES ('ff808181635d65ee01635d68aa020036','중분류','채널>대분류>중분류','ff808181635d65ee01635d68aa010035');
INSERT INTO MLT_BSQA_CATEGORY(id,catName,catPath,parCatId) VALUES ('ff808181635d65ee01635d68aa020037','소분류','채널>대분류>중분류>소분류','ff808181635d65ee01635d68aa020036');
INSERT INTO MLT_BSQA_CATEGORY(id,catName,catPath,parCatId) VALUES ('ff808181635d65ee01635d68aa020038','채널2','채널2','channel');
INSERT INTO MLT_BSQA_CATEGORY(id,catName,catPath,parCatId) VALUES ('ff808181635d65ee01635d68aa030039','대분류2','채널2>대분류2','ff808181635d65ee01635d68aa020038');
INSERT INTO MLT_BSQA_CATEGORY(id,catName,catPath,parCatId) VALUES ('ff808181635d65ee01635d68aa03003a','중분류2','채널2>대분류2>중분류2','ff808181635d65ee01635d68aa030039');
INSERT INTO MLT_BSQA_CATEGORY(id,catName,catPath,parCatId) VALUES ('ff808181635d65ee01635d68aa03003b','소분류2','채널2>대분류2>중분류2>소분류2','ff808181635d65ee01635d68aa03003a');

INSERT INTO MLT_BSQA_ITEM (ID,SKILL_ID,QUESTION,ANSWER,SRC,USE_YN,MAIN_YN,WEIGHT,CREATE_DTM,UPDATE_DTM, CHANNEL, CATEGORY, SUB_CATEGORY, SUB_SUB_CATEGORY) VALUES (1,1,'question', 'answer','src', 'Y', 'Y', 0.1,'2018-02-05 16:08:19','2018-02-05 16:08:19', 'ff808181635d65ee01635d68aa010034', 'ff808181635d65ee01635d68aa010035', 'ff808181635d65ee01635d68aa020036', 'ff808181635d65ee01635d68aa020037');
INSERT INTO MLT_BSQA_ITEM (ID,SKILL_ID,QUESTION,ANSWER,SRC,USE_YN,MAIN_YN,WEIGHT,CREATE_DTM,UPDATE_DTM, CHANNEL, CATEGORY, SUB_CATEGORY, SUB_SUB_CATEGORY) VALUES (2,2,'question2', 'answer2','src2', 'N', 'Y', 0.1,'2018-02-05 16:08:19','2018-02-05 16:08:19', 'ff808181635d65ee01635d68aa020038', 'ff808181635d65ee01635d68aa030039', 'ff808181635d65ee01635d68aa03003a', 'ff808181635d65ee01635d68aa03003b');
INSERT INTO MLT_BSQA_ITEM (ID,SKILL_ID,QUESTION,ANSWER,SRC,USE_YN,MAIN_YN,WEIGHT,CREATE_DTM,UPDATE_DTM, CHANNEL, CATEGORY, SUB_CATEGORY, SUB_SUB_CATEGORY) VALUES (3,3,'question3', 'answer3','src3', 'Y', 'Y', 0.1,'2018-02-07 16:08:19','2018-02-08 16:08:19', 'ff808181635d65ee01635d68aa010034', 'ff808181635d65ee01635d68aa010035', 'ff808181635d65ee01635d68aa020036', 'ff808181635d65ee01635d68aa020037');
INSERT INTO MLT_BSQA_ITEM (ID,SKILL_ID,QUESTION,ANSWER,SRC,USE_YN,MAIN_YN,WEIGHT,CREATE_DTM,UPDATE_DTM, CHANNEL, CATEGORY, SUB_CATEGORY, SUB_SUB_CATEGORY) VALUES (4,4,'question4', 'answer4','src4', 'N', 'Y', 0.1,'2018-02-07 16:08:19','2018-02-08 16:08:19', 'ff808181635d65ee01635d68aa020038', 'ff808181635d65ee01635d68aa030039', 'ff808181635d65ee01635d68aa03003a', 'ff808181635d65ee01635d68aa03003b');
INSERT INTO MLT_BSQA_ITEM (ID,SKILL_ID,QUESTION,ANSWER,SRC,USE_YN,MAIN_YN,WEIGHT,CREATE_DTM,UPDATE_DTM, CHANNEL, CATEGORY, SUB_CATEGORY, SUB_SUB_CATEGORY) VALUES (5,5,'question5', 'answer5','src5', 'Y', 'Y', 0.1,'2018-02-07 16:08:19','2018-02-08 16:08:19', 'ff808181635d65ee01635d68aa010034', 'ff808181635d65ee01635d68aa010035', 'ff808181635d65ee01635d68aa020036', 'ff808181635d65ee01635d68aa020037');
INSERT INTO MLT_BSQA_ITEM (ID,SKILL_ID,QUESTION,ANSWER,SRC,USE_YN,MAIN_YN,WEIGHT,CREATE_DTM,UPDATE_DTM, CHANNEL, CATEGORY, SUB_CATEGORY, SUB_SUB_CATEGORY) VALUES (6,1,'question6', 'answer6','src6', 'Y', 'Y', 0.1,'2018-02-07 16:08:19','2018-02-08 16:08:19', 'ff808181635d65ee01635d68aa020038', 'ff808181635d65ee01635d68aa030039', 'ff808181635d65ee01635d68aa03003a', 'ff808181635d65ee01635d68aa03003b');

INSERT INTO MLT_BSQA_PRPRS_MAIN_WD (id,word,useYn,createAt) VALUES (1,'안녕','Y','2018-02-07 16:08:19');
INSERT INTO MLT_BSQA_PRPRS_MAIN_WD (id,word,useYn,createAt) VALUES (2,'하세요','N','2018-02-07 16:08:19');

INSERT INTO MLT_BSQA_PRPRS_WD (id,word,createAt) VALUES  (1,'하이','2018-02-07 16:08:19');
INSERT INTO MLT_BSQA_PRPRS_WD (id,word,createAt) VALUES  (2,'봉주르','2018-02-07 16:08:19');
INSERT INTO MLT_BSQA_PRPRS_WD (id,word,createAt) VALUES  (3,'니하오','2018-02-07 16:08:19');
INSERT INTO MLT_BSQA_PRPRS_WD (id,word,createAt) VALUES  (4,'하셈','2018-02-07 16:08:19');
INSERT INTO MLT_BSQA_PRPRS_WD (id,word,createAt) VALUES  (5,'해','2018-02-07 16:08:19');

INSERT INTO MLT_BSQA_PRPRS_REL (mainId,paraphraseId) VALUES (1,1);
INSERT INTO MLT_BSQA_PRPRS_REL (mainId,paraphraseId) VALUES (1,2);
INSERT INTO MLT_BSQA_PRPRS_REL (mainId,paraphraseId) VALUES (1,3);
INSERT INTO MLT_BSQA_PRPRS_REL (mainId,paraphraseId) VALUES (2,4);
INSERT INTO MLT_BSQA_PRPRS_REL (mainId,paraphraseId) VALUES (2,5);

INSERT INTO MLT_TWE_SCHEDULE ( seq ,  hour ,  minute ,  period ,  type )VALUES  ('1',   '00',   '00',  '0',  'Weekly');
commit;