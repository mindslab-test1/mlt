-- we don't know how to generate schema mlt_tutor (class Schema) :(
create table MLT_BSQA_CATEGORY
(
	id varchar(40) not null
		primary key,
	catName varchar(40) not null,
	catPath varchar(200) not null,
	createdAt datetime null,
	creatorId varchar(40) null,
	parCatId varchar(40) null,
	updatedAt datetime null,
	updaterId varchar(40) null
)
;

create table MLT_BSQA_IDX_HISTORY_SEQ
(
	next_val bigint null
)
;

create table MLT_BSQA_INDEXING_HISTORY
(
	id int not null
		primary key,
	address varchar(255) null,
	createAt datetime null,
	createId varchar(255) null,
	fetched int null,
	message varchar(100) null,
	processed int null,
	status bit null,
	stopYn bit null,
	total int null,
	type varchar(30) null,
	updateAt datetime null,
	updaterId varchar(255) null
)
;

create table MLT_BSQA_PRPRS_MAIN_WD
(
	id int not null
		primary key,
	createAt datetime null,
	createId varchar(255) null,
	updateAt datetime null,
	updaterId varchar(255) null,
	useYn varchar(5) not null,
	word varchar(40) not null
)
;

create table MLT_BSQA_PRPRS_WD
(
	id int not null
		primary key,
	createAt datetime null,
	createId varchar(255) null,
	word varchar(40) not null
)
;

create table MLT_BSQA_PRPRS_REL
(
	mainId int not null,
	paraphraseId int not null,
	primary key (mainId, paraphraseId),
	constraint FKdc2sh08nsrmnfdygampiuykp
		foreign key (mainId) references MLT_BSQA_PRPRS_MAIN_WD (id),
	constraint FKrir2xh9i1gwj3aesmhexadhw3
		foreign key (paraphraseId) references MLT_BSQA_PRPRS_WD (id)
)
;

create index FKrir2xh9i1gwj3aesmhexadhw3
	on MLT_BSQA_PRPRS_REL (paraphraseId)
;

create table MLT_BSQA_PRPRS_WORD_SEQ
(
	next_val bigint null
)
;

create table MLT_BSQA_SKILL
(
	id int not null
		primary key,
	createAt datetime null,
	createId varchar(255) null,
	name varchar(40) not null,
	updateAt datetime null,
	updaterId varchar(255) null
)
;

create table MLT_BSQA_ITEM
(
	ID int not null
		primary key,
	ANSWER longtext null,
	C_ID int null,
	CATEGORY varchar(40) null,
	CHANNEL varchar(40) not null,
	CONFIRM_DTM datetime null,
	CONFIRMER varchar(40) null,
	CREATE_DTM datetime null,
	CREATOR varchar(40) null,
	MAIN_YN varchar(1) null,
	QNA_ID varchar(255) null,
	QUESTION longtext null,
	SKILL_ID int null,
	SRC varchar(20) null,
	SUB_CATEGORY varchar(40) null,
	SUB_SUB_CATEGORY varchar(40) null,
	UPDATE_DTM datetime null,
	UPDATOR varchar(40) null,
	USE_YN varchar(1) null,
	WEIGHT decimal(2,1) null,
	constraint FKl3u4hj0f2pted1dodqlxb1e3x
		foreign key (CATEGORY) references MLT_BSQA_CATEGORY (id),
	constraint FKlff1l7ihtyqfaivklkx9ctc9y
		foreign key (CHANNEL) references MLT_BSQA_CATEGORY (id),
	constraint FKsmv8cgoskfifr0l91hf8qvost
		foreign key (SKILL_ID) references MLT_BSQA_SKILL (id),
	constraint FKk2tx365j8f8x4694xxfhh3f6o
		foreign key (SUB_CATEGORY) references MLT_BSQA_CATEGORY (id),
	constraint FKo06fgoy8t3ii7oselil42qau
		foreign key (SUB_SUB_CATEGORY) references MLT_BSQA_CATEGORY (id)
)
;

create index FKk2tx365j8f8x4694xxfhh3f6o
	on MLT_BSQA_ITEM (SUB_CATEGORY)
;

create index FKl3u4hj0f2pted1dodqlxb1e3x
	on MLT_BSQA_ITEM (CATEGORY)
;

create index FKlff1l7ihtyqfaivklkx9ctc9y
	on MLT_BSQA_ITEM (CHANNEL)
;

create index FKo06fgoy8t3ii7oselil42qau
	on MLT_BSQA_ITEM (SUB_SUB_CATEGORY)
;

create index FKsmv8cgoskfifr0l91hf8qvost
	on MLT_BSQA_ITEM (SKILL_ID)
;

create table MLT_BSQA_SKILL_SEQ
(
	next_val bigint null
)
;

create table MLT_CODE
(
	id varchar(10) not null
		primary key,
	groupCode varchar(40) not null,
	name varchar(40) not null
)
;

create table MLT_DNN_LINE_SEQ
(
	next_val bigint null
)
;

create table MLT_HMD_LINE_SEQ
(
	next_val bigint null
)
;

create table MLT_MENU
(
	id varchar(40) not null
		primary key,
	path varchar(200) not null
)
;

create table MLT_MRC_WIKI_SEQ
(
	next_val bigint null
)
;

create table MLT_PASSAGE_IDX_HISTORY_SEQ
(
	next_val bigint null
)
;

create table MLT_PASSAGE_SKILL_SEQ
(
	next_val bigint null
)
;

create table MLT_STT_TRAINING
(
	id varchar(40) not null
		primary key,
	sttKey varchar(255) null,
	workspaceId varchar(40) not null
)
;

create table MLT_STT_TRANSCRIPT_SEQ
(
	next_val bigint null
)
;

create table MLT_TWE_HISTORY_SEQ
(
	next_val bigint null
)
;

create table MLT_TWE_INDEXING_HISTORY
(
	id int not null
		primary key,
	createdAt datetime null,
	deleteCnt int null,
	insertCnt int null,
	leadTime varchar(255) null,
	updateCnt int null
)
;

create table MLT_TWE_SCHEDULE
(
	seq int not null
		primary key,
	hour varchar(10) null,
	minute varchar(10) null,
	period varchar(10) null,
	type varchar(10) null
)
;

create table MLT_USER
(
	id varchar(40) not null
		primary key,
	activated int not null,
	createdAt datetime null,
	creatorId varchar(40) null,
	email varchar(40) null,
	emailVerified int null,
	passUpdatedAt datetime null,
	password varchar(512) not null,
	updatedAt datetime null,
	updaterId varchar(40) null,
	username varchar(40) not null,
	verificationToken varchar(512) null
)
;

create table MLT_MRC_DATA_GROUP
(
	id varchar(40) not null
		primary key,
	createdAt datetime null,
	creatorId varchar(40) null,
	dataGroupName varchar(40) not null,
	updatedAt datetime null,
	updaterId varchar(40) null,
	workspaceId varchar(40) not null,
	constraint FKstkb1fxx3o9nm3tn0aauojlq0
		foreign key (creatorId) references MLT_USER (id),
	constraint FK9l8ofw5e2na52xrpvurx94v72
		foreign key (updaterId) references MLT_USER (id)
)
;

create index FK9l8ofw5e2na52xrpvurx94v72
	on MLT_MRC_DATA_GROUP (updaterId)
;

create index FKstkb1fxx3o9nm3tn0aauojlq0
	on MLT_MRC_DATA_GROUP (creatorId)
;

create table MLT_WORKSPACE
(
	id varchar(40) not null
		primary key,
	createdAt datetime null,
	creatorId varchar(40) null,
	langCode varchar(10) not null,
	name varchar(40) not null,
	roleCode varchar(10) null,
	updatedAt datetime null,
	updaterId varchar(40) null,
	constraint FKfe6mp6euuy188pcyy2x5kcje
		foreign key (creatorId) references MLT_USER (id),
	constraint FKku6bvqdqmjrycxmh5ioxlq3w
		foreign key (updaterId) references MLT_USER (id)
)
;

create table MLT_COMPOUND_DIC
(
	id varchar(40) not null
		primary key,
	applied varchar(512) null,
	createdAt datetime null,
	creatorId varchar(40) null,
	name varchar(40) not null,
	updatedAt datetime null,
	updaterId varchar(40) null,
	version longtext null,
	workspaceId varchar(40) not null,
	constraint FKg5c9njyr042pii0arid26ju8r
		foreign key (creatorId) references MLT_USER (id),
	constraint FKrkm19lx2wbs6l49i80812hylo
		foreign key (updaterId) references MLT_USER (id),
	constraint FKovkqjb1ktw0elpp8d0o1ipkwu
		foreign key (workspaceId) references MLT_WORKSPACE (id)
)
;

create index FKg5c9njyr042pii0arid26ju8r
	on MLT_COMPOUND_DIC (creatorId)
;

create index FKovkqjb1ktw0elpp8d0o1ipkwu
	on MLT_COMPOUND_DIC (workspaceId)
;

create index FKrkm19lx2wbs6l49i80812hylo
	on MLT_COMPOUND_DIC (updaterId)
;

create table MLT_COMPOUND_DIC_LINE
(
	id varchar(40) not null
		primary key,
	createdAt datetime null,
	pos varchar(40) null,
	versionId varchar(40) not null,
	word longtext null,
	workspaceId varchar(40) not null,
	constraint FKp1vuirdhc7yci0c5s01t4kwu5
		foreign key (versionId) references MLT_COMPOUND_DIC (id),
	constraint FKtqsk17j9o81u6ot0vmpggvtng
		foreign key (workspaceId) references MLT_WORKSPACE (id)
)
;

create index FKp1vuirdhc7yci0c5s01t4kwu5
	on MLT_COMPOUND_DIC_LINE (versionId)
;

create index FKtqsk17j9o81u6ot0vmpggvtng
	on MLT_COMPOUND_DIC_LINE (workspaceId)
;

create table MLT_DNN_DIC
(
	id varchar(40) not null
		primary key,
	applied varchar(512) null,
	createdAt datetime null,
	creatorId varchar(40) null,
	name varchar(40) not null,
	updatedAt datetime null,
	updaterId varchar(40) null,
	versions longtext null,
	workspaceId varchar(40) not null,
	constraint FKq2chuae12vj08x6jc5943va0d
		foreign key (creatorId) references MLT_USER (id),
	constraint FKc4rhy2r42xbkwe634wtay24ng
		foreign key (updaterId) references MLT_USER (id),
	constraint FK48w8vlc83bd37ap8k1q4d1kfk
		foreign key (workspaceId) references MLT_WORKSPACE (id)
)
;

create table MLT_DNN_CATEGORY
(
	id varchar(40) not null
		primary key,
	createdAt datetime null,
	creatorId varchar(40) null,
	dnnDicId varchar(40) not null,
	name varchar(512) not null,
	parentId varchar(40) null,
	updatedAt datetime null,
	updaterId varchar(40) null,
	workspaceId varchar(40) not null,
	constraint FK2o1j2eeiugwjeindb34kqq3x4
		foreign key (creatorId) references MLT_USER (id),
	constraint FK1xjh7h5ycsx89bba2fcnkj3ha
		foreign key (dnnDicId) references MLT_DNN_DIC (id),
	constraint FKno459v6u1sta9l9wo361evhgj
		foreign key (updaterId) references MLT_USER (id),
	constraint FKt6h3hguwie640khm41chafhtt
		foreign key (workspaceId) references MLT_WORKSPACE (id)
)
;

create index FK1xjh7h5ycsx89bba2fcnkj3ha
	on MLT_DNN_CATEGORY (dnnDicId)
;

create index FK2o1j2eeiugwjeindb34kqq3x4
	on MLT_DNN_CATEGORY (creatorId)
;

create index FKno459v6u1sta9l9wo361evhgj
	on MLT_DNN_CATEGORY (updaterId)
;

create index FKt6h3hguwie640khm41chafhtt
	on MLT_DNN_CATEGORY (workspaceId)
;

create index FK48w8vlc83bd37ap8k1q4d1kfk
	on MLT_DNN_DIC (workspaceId)
;

create index FKc4rhy2r42xbkwe634wtay24ng
	on MLT_DNN_DIC (updaterId)
;

create index FKq2chuae12vj08x6jc5943va0d
	on MLT_DNN_DIC (creatorId)
;

create table MLT_DNN_DIC_LINE
(
	seq int not null
		primary key,
	category varchar(512) not null,
	sentence longtext null,
	versionId varchar(40) not null,
	constraint FKlw4tt4wg82crf93il0yt81ekl
		foreign key (versionId) references MLT_DNN_DIC (id)
)
;

create index FKlw4tt4wg82crf93il0yt81ekl
	on MLT_DNN_DIC_LINE (versionId)
;

create table MLT_DNN_MODEL
(
	dnnKey varchar(36) not null
		primary key,
	createdAt datetime null,
	creatorId varchar(40) null,
	dnnBinary varchar(512) not null,
	name varchar(512) not null,
	updatedAt datetime null,
	updaterId varchar(40) null,
	workspaceId varchar(40) not null,
	constraint FKd1o5dygpq3gi2bedvl1uq7yo
		foreign key (creatorId) references MLT_USER (id),
	constraint FKpue08srct7dwi4rpfxwf3x43
		foreign key (updaterId) references MLT_USER (id),
	constraint FK5stxsvaq9bjnmif8ihhpt05ss
		foreign key (workspaceId) references MLT_WORKSPACE (id)
)
;

create index FK5stxsvaq9bjnmif8ihhpt05ss
	on MLT_DNN_MODEL (workspaceId)
;

create index FKd1o5dygpq3gi2bedvl1uq7yo
	on MLT_DNN_MODEL (creatorId)
;

create index FKpue08srct7dwi4rpfxwf3x43
	on MLT_DNN_MODEL (updaterId)
;

create table MLT_DNN_RESULT
(
	id varchar(40) not null
		primary key,
	category varchar(512) null,
	createdAt datetime null,
	creatorId varchar(40) null,
	fileGroup varchar(512) not null,
	model varchar(512) not null,
	probability decimal(17,16) null,
	sentence longtext not null,
	updatedAt datetime null,
	updaterId varchar(40) null,
	workspaceId varchar(40) not null,
	constraint FK861v3hqipq52xlnlrrnqm8y8f
		foreign key (creatorId) references MLT_USER (id),
	constraint FKs0l8juns9s2ku2rxc9ta6nxyi
		foreign key (updaterId) references MLT_USER (id),
	constraint FKm6hufduwi24bu5t920bo5pwaf
		foreign key (workspaceId) references MLT_WORKSPACE (id)
)
;

create index FK861v3hqipq52xlnlrrnqm8y8f
	on MLT_DNN_RESULT (creatorId)
;

create index FKm6hufduwi24bu5t920bo5pwaf
	on MLT_DNN_RESULT (workspaceId)
;

create index FKs0l8juns9s2ku2rxc9ta6nxyi
	on MLT_DNN_RESULT (updaterId)
;

create table MLT_DNN_TRAINING
(
	dnnKey varchar(36) not null
		primary key,
	workspaceId varchar(40) not null,
	constraint FK9g3qerq6dsjiph8n8jf3yl5rj
		foreign key (workspaceId) references MLT_WORKSPACE (id)
)
;

create index FK9g3qerq6dsjiph8n8jf3yl5rj
	on MLT_DNN_TRAINING (workspaceId)
;

create table MLT_END_POST_DIC
(
	id varchar(40) not null
		primary key,
	createdAt datetime null,
	creatorId varchar(40) null,
	representation varchar(40) not null,
	type varchar(5) not null,
	updatedAt datetime null,
	updaterId varchar(40) null,
	workspaceId varchar(40) not null,
	constraint FKibmkp4qv8krs6a1kdjqe0dww7
		foreign key (creatorId) references MLT_USER (id),
	constraint FKrcpfiawhrycd1jlna8ui0uho3
		foreign key (updaterId) references MLT_USER (id),
	constraint FKe1y0ywcken84hgi8721wpdbs1
		foreign key (workspaceId) references MLT_WORKSPACE (id)
)
;

create index FKe1y0ywcken84hgi8721wpdbs1
	on MLT_END_POST_DIC (workspaceId)
;

create index FKibmkp4qv8krs6a1kdjqe0dww7
	on MLT_END_POST_DIC (creatorId)
;

create index FKrcpfiawhrycd1jlna8ui0uho3
	on MLT_END_POST_DIC (updaterId)
;

create table MLT_END_POST_DIC_REL
(
	id varchar(40) not null
		primary key,
	createdAt datetime null,
	creatorId varchar(40) null,
	representationId varchar(40) null,
	updatedAt datetime null,
	updaterId varchar(40) null,
	word varchar(40) not null,
	workspaceId varchar(40) not null,
	constraint FKanbj8lj54tw09xb3q6rtev2mr
		foreign key (creatorId) references MLT_USER (id),
	constraint FKrohnuhds82nj17pflmbu4vomo
		foreign key (representationId) references MLT_END_POST_DIC (id),
	constraint FK61cmf6xtoug2q6vs06mqm9l6p
		foreign key (updaterId) references MLT_USER (id),
	constraint FKl39t258bfnp3cmqq2unnha3g5
		foreign key (workspaceId) references MLT_WORKSPACE (id)
)
;

create index FK61cmf6xtoug2q6vs06mqm9l6p
	on MLT_END_POST_DIC_REL (updaterId)
;

create index FKanbj8lj54tw09xb3q6rtev2mr
	on MLT_END_POST_DIC_REL (creatorId)
;

create index FKl39t258bfnp3cmqq2unnha3g5
	on MLT_END_POST_DIC_REL (workspaceId)
;

create index FKrohnuhds82nj17pflmbu4vomo
	on MLT_END_POST_DIC_REL (representationId)
;

create table MLT_FILE
(
	id varchar(40) not null
		primary key,
	createdAt datetime null,
	creatorId varchar(40) null,
	duration int null,
	hash varchar(200) not null,
	meta varchar(255) null,
	name varchar(200) not null,
	purpose varchar(200) null,
	fileSize bigint not null,
	type varchar(200) not null,
	updatedAt datetime null,
	updaterId varchar(40) null,
	workspaceId varchar(40) not null,
	constraint FK32ch19r0kykc1216c9dinf75d
		foreign key (creatorId) references MLT_USER (id),
	constraint FK688bwmsj8brj2sbqfph2e8pei
		foreign key (updaterId) references MLT_USER (id),
	constraint FKsuwn9cbdktpmym61s4n9fqvon
		foreign key (workspaceId) references MLT_WORKSPACE (id)
)
;

create index FK32ch19r0kykc1216c9dinf75d
	on MLT_FILE (creatorId)
;

create index FK688bwmsj8brj2sbqfph2e8pei
	on MLT_FILE (updaterId)
;

create index FKsuwn9cbdktpmym61s4n9fqvon
	on MLT_FILE (workspaceId)
;

create table MLT_FILE_GROUP
(
	id varchar(40) not null
		primary key,
	createdAt datetime null,
	creatorId varchar(40) null,
	meta varchar(255) null,
	name varchar(40) not null,
	purpose varchar(40) not null,
	updatedAt datetime null,
	updaterId varchar(40) null,
	workspaceId varchar(40) not null,
	constraint FKl3xgrtmy07ibuma1at4wlksv
		foreign key (creatorId) references MLT_USER (id),
	constraint FKhhpkah1plgc2tog0dbjj767c3
		foreign key (updaterId) references MLT_USER (id),
	constraint FKd0ttvmadj5fdxd6ml6k3cojd8
		foreign key (workspaceId) references MLT_WORKSPACE (id)
)
;

create index FKd0ttvmadj5fdxd6ml6k3cojd8
	on MLT_FILE_GROUP (workspaceId)
;

create index FKhhpkah1plgc2tog0dbjj767c3
	on MLT_FILE_GROUP (updaterId)
;

create index FKl3xgrtmy07ibuma1at4wlksv
	on MLT_FILE_GROUP (creatorId)
;

create table MLT_FILE_GROUP_REL
(
	id varchar(40) not null
		primary key,
	createdAt datetime null,
	creatorId varchar(40) null,
	fileGroupId varchar(40) not null,
	fileId varchar(40) not null,
	constraint FKg8c5seqnfqrocloht0lv8rawt
		foreign key (creatorId) references MLT_USER (id),
	constraint FK31kyjjyof2xbdr7el9rb2gd7w
		foreign key (fileGroupId) references MLT_FILE_GROUP (id),
	constraint FKhetynfp90qlogidx832gw3asr
		foreign key (fileId) references MLT_FILE (id)
)
;

create index FK31kyjjyof2xbdr7el9rb2gd7w
	on MLT_FILE_GROUP_REL (fileGroupId)
;

create index FKg8c5seqnfqrocloht0lv8rawt
	on MLT_FILE_GROUP_REL (creatorId)
;

create index FKhetynfp90qlogidx832gw3asr
	on MLT_FILE_GROUP_REL (fileId)
;

create table MLT_HISTORY
(
	id varchar(40) not null
		primary key,
	code varchar(512) not null,
	createdAt datetime null,
	creatorId varchar(40) null,
	data longtext null,
	endedAt datetime null,
	message varchar(512) null,
	startedAt datetime not null,
	updatedAt datetime null,
	updaterId varchar(40) null,
	workspaceId varchar(40) null,
	constraint FKbeeyvv7kqdrh5mt6idskc5p6s
		foreign key (creatorId) references MLT_USER (id),
	constraint FK19g8meq6iym5h3dodfc2xsp89
		foreign key (updaterId) references MLT_USER (id),
	constraint FKkbutpfusf16gsa166jgjhh2lx
		foreign key (workspaceId) references MLT_WORKSPACE (id)
)
;

create index FK19g8meq6iym5h3dodfc2xsp89
	on MLT_HISTORY (updaterId)
;

create index FKbeeyvv7kqdrh5mt6idskc5p6s
	on MLT_HISTORY (creatorId)
;

create index FKkbutpfusf16gsa166jgjhh2lx
	on MLT_HISTORY (workspaceId)
;

create table MLT_HMD_DIC
(
	id varchar(40) not null
		primary key,
	applied varchar(512) null,
	createdAt datetime null,
	creatorId varchar(40) null,
	name varchar(40) not null,
	updatedAt datetime null,
	updaterId varchar(40) null,
	versions longtext null,
	workspaceId varchar(40) not null,
	constraint FKdoqiqrd48x2jfcbbt408tivyv
		foreign key (creatorId) references MLT_USER (id),
	constraint FKrb8asauioxwlup38r63w13ly4
		foreign key (updaterId) references MLT_USER (id),
	constraint FKckydqee5j2tqq5o5gbkd6p735
		foreign key (workspaceId) references MLT_WORKSPACE (id)
)
;

create table MLT_HMD_CATEGORY
(
	id varchar(40) not null
		primary key,
	createdAt datetime null,
	creatorId varchar(40) null,
	hmdDicId varchar(40) not null,
	name varchar(40) not null,
	parCatId varchar(40) null,
	parentId varchar(40) null,
	updatedAt datetime null,
	updaterId varchar(40) null,
	workspaceId varchar(40) not null,
	constraint FKpv5px7cgjumo9g377mcd1e1p6
		foreign key (creatorId) references MLT_USER (id),
	constraint FK1l1doul3cqeny6ekhg5pg7ngr
		foreign key (hmdDicId) references MLT_HMD_DIC (id),
	constraint FKa6udo65hy7ab3oghagrwnv9mu
		foreign key (updaterId) references MLT_USER (id),
	constraint FKa1bmvege6ib3fssxksj8mkwii
		foreign key (workspaceId) references MLT_WORKSPACE (id)
)
;

create index FK1l1doul3cqeny6ekhg5pg7ngr
	on MLT_HMD_CATEGORY (hmdDicId)
;

create index FKa1bmvege6ib3fssxksj8mkwii
	on MLT_HMD_CATEGORY (workspaceId)
;

create index FKa6udo65hy7ab3oghagrwnv9mu
	on MLT_HMD_CATEGORY (updaterId)
;

create index FKpv5px7cgjumo9g377mcd1e1p6
	on MLT_HMD_CATEGORY (creatorId)
;

create index FKckydqee5j2tqq5o5gbkd6p735
	on MLT_HMD_DIC (workspaceId)
;

create index FKdoqiqrd48x2jfcbbt408tivyv
	on MLT_HMD_DIC (creatorId)
;

create index FKrb8asauioxwlup38r63w13ly4
	on MLT_HMD_DIC (updaterId)
;

create table MLT_HMD_DIC_LINE
(
	seq int not null
		primary key,
	category varchar(40) not null,
	rule longtext null,
	versionId varchar(40) not null,
	constraint FKndnubsiposokevxpgmfmbvr0h
		foreign key (versionId) references MLT_HMD_DIC (id)
)
;

create index FKndnubsiposokevxpgmfmbvr0h
	on MLT_HMD_DIC_LINE (versionId)
;

create table MLT_HMD_RESULT
(
	id varchar(40) not null
		primary key,
	category varchar(512) not null,
	createdAt datetime null,
	creatorId varchar(40) null,
	fileGroup varchar(40) not null,
	model varchar(512) not null,
	rule longtext not null,
	sentence longtext not null,
	updatedAt datetime null,
	updaterId varchar(40) null,
	workspaceId varchar(40) not null,
	constraint FK3be3qujh5xipw2c0kq4mo6n6c
		foreign key (creatorId) references MLT_USER (id),
	constraint FKjf4abbm2vng0rfxlravr5s98r
		foreign key (updaterId) references MLT_USER (id),
	constraint FKpqv5gmt0coqnmy9rjbman9isy
		foreign key (workspaceId) references MLT_WORKSPACE (id)
)
;

create index FK3be3qujh5xipw2c0kq4mo6n6c
	on MLT_HMD_RESULT (creatorId)
;

create index FKjf4abbm2vng0rfxlravr5s98r
	on MLT_HMD_RESULT (updaterId)
;

create index FKpqv5gmt0coqnmy9rjbman9isy
	on MLT_HMD_RESULT (workspaceId)
;

create table MLT_MORP_DIC
(
	id varchar(40) not null
		primary key,
	applied varchar(512) null,
	createdAt datetime null,
	creatorId varchar(40) null,
	name varchar(40) not null,
	updatedAt datetime null,
	updaterId varchar(40) null,
	version longtext null,
	workspaceId varchar(40) not null,
	constraint FK8qa0jbm2wqewb0s3o161crnsr
		foreign key (creatorId) references MLT_USER (id),
	constraint FKiybgtuh47dr6ur270iddt6tcd
		foreign key (updaterId) references MLT_USER (id),
	constraint FK1kk5908u9si6c1exp5o133qlh
		foreign key (workspaceId) references MLT_WORKSPACE (id)
)
;

create index FK1kk5908u9si6c1exp5o133qlh
	on MLT_MORP_DIC (workspaceId)
;

create index FK8qa0jbm2wqewb0s3o161crnsr
	on MLT_MORP_DIC (creatorId)
;

create index FKiybgtuh47dr6ur270iddt6tcd
	on MLT_MORP_DIC (updaterId)
;

create table MLT_MORP_DIC_LINE
(
	id varchar(40) not null
		primary key,
	createdAt datetime null,
	pos varchar(40) null,
	versionId varchar(40) not null,
	word longtext null,
	workspaceId varchar(40) not null,
	constraint FKt1q6gmkigm2c2wciw2s7biqdw
		foreign key (versionId) references MLT_MORP_DIC (id),
	constraint FKm83bb4igb7s1bw4bivla38o1u
		foreign key (workspaceId) references MLT_WORKSPACE (id)
)
;

create index FKm83bb4igb7s1bw4bivla38o1u
	on MLT_MORP_DIC_LINE (workspaceId)
;

create index FKt1q6gmkigm2c2wciw2s7biqdw
	on MLT_MORP_DIC_LINE (versionId)
;

create table MLT_MRC_CATEGORY
(
	id varchar(40) not null
		primary key,
	catName varchar(40) null,
	catPath varchar(164) null,
	createdAt datetime null,
	creatorId varchar(40) null,
	parCatId varchar(40) null,
	updatedAt datetime null,
	updaterId varchar(40) null,
	workspaceId varchar(40) null,
	constraint FK3nrvf337e6b8jis4xutelm8j6
		foreign key (creatorId) references MLT_USER (id),
	constraint FKr5msc2i8o2g4yvg0saxvh136u
		foreign key (updaterId) references MLT_USER (id),
	constraint FK64bbg34gq9ml9or8npq7t0i4f
		foreign key (workspaceId) references MLT_WORKSPACE (id)
)
;

create index FK3nrvf337e6b8jis4xutelm8j6
	on MLT_MRC_CATEGORY (creatorId)
;

create index FK64bbg34gq9ml9or8npq7t0i4f
	on MLT_MRC_CATEGORY (workspaceId)
;

create index FKr5msc2i8o2g4yvg0saxvh136u
	on MLT_MRC_CATEGORY (updaterId)
;

create table MLT_MRC_PASSAGE_IDX_HISTORY
(
	id int not null
		primary key,
	address varchar(255) null,
	createdAt datetime null,
	creatorId varchar(255) null,
	fetched int null,
	message varchar(100) null,
	processed int null,
	status bit null,
	stopYn bit null,
	total int null,
	type varchar(30) null,
	updatedAt datetime null,
	updaterId varchar(255) null,
	workspaceId varchar(40) not null,
	constraint FKane8pkptosnkhlmhks9avi97u
		foreign key (workspaceId) references MLT_WORKSPACE (id)
)
;

create index FKane8pkptosnkhlmhks9avi97u
	on MLT_MRC_PASSAGE_IDX_HISTORY (workspaceId)
;

create table MLT_MRC_PASSAGE_SKILL
(
	ID int not null
		primary key,
	CREATED_AT datetime null,
	CREATOR_ID varchar(40) null,
	NAME varchar(40) not null,
	UPDATED_AT datetime null,
	UPDATER_ID varchar(40) null,
	WORKSPACE_ID varchar(40) not null,
	constraint FK3rrh3npdopyi33tf2evu0r14d
		foreign key (CREATOR_ID) references MLT_USER (id),
	constraint FKjw6cgvqo9s9y7fhhr9x5hxdr6
		foreign key (UPDATER_ID) references MLT_USER (id),
	constraint FKtel0vqhk66s9lx5t926uqt6bi
		foreign key (WORKSPACE_ID) references MLT_WORKSPACE (id)
)
;

create table MLT_MRC_PASSAGE
(
	ID varchar(40) not null
		primary key,
	CREATED_AT datetime null,
	CREATOR_ID varchar(40) null,
	PASSAGE longtext null,
	PASSAGE_MORPH longtext null,
	SECTION varchar(255) null,
	SKILL_ID int null,
	SRC varchar(255) null,
	UPDATED_AT datetime null,
	UPDATER_ID varchar(40) null,
	WORD_LIST longtext null,
	WORKSPACE_ID varchar(40) not null,
	constraint FK3g3r8lb8p03e23qpil1526xgy
		foreign key (CREATOR_ID) references MLT_USER (id),
	constraint FKi98g49d81b7xjnyguxcnyyo2o
		foreign key (SKILL_ID) references MLT_MRC_PASSAGE_SKILL (ID),
	constraint FKiecudgioip3pja71ibibs951k
		foreign key (UPDATER_ID) references MLT_USER (id),
	constraint FKe5hpwi3snvg7ge626ygj9dkfk
		foreign key (WORKSPACE_ID) references MLT_WORKSPACE (id)
)
;

create index FK3g3r8lb8p03e23qpil1526xgy
	on MLT_MRC_PASSAGE (CREATOR_ID)
;

create index FKe5hpwi3snvg7ge626ygj9dkfk
	on MLT_MRC_PASSAGE (WORKSPACE_ID)
;

create index FKi98g49d81b7xjnyguxcnyyo2o
	on MLT_MRC_PASSAGE (SKILL_ID)
;

create index FKiecudgioip3pja71ibibs951k
	on MLT_MRC_PASSAGE (UPDATER_ID)
;

create table MLT_MRC_PASSAGE_QUESTION
(
	ID varchar(40) not null
		primary key,
	CREATED_AT datetime null,
	CREATOR_ID varchar(40) null,
	PASSAGE_ID varchar(40) null,
	QUESTION varchar(255) null,
	QUESTION_MORPH longtext null,
	UPDATED_AT datetime null,
	UPDATER_ID varchar(40) null,
	WORKSPACE_ID varchar(40) not null,
	constraint FKgoye8uiiidb7tqfn3bjfyj82
		foreign key (CREATOR_ID) references MLT_USER (id),
	constraint FKbyek78rochk6xwsb2frulyp2o
		foreign key (PASSAGE_ID) references MLT_MRC_PASSAGE (ID),
	constraint FK6q3tfcqywl3md4dskp98tn3yx
		foreign key (UPDATER_ID) references MLT_USER (id),
	constraint FKhdhya2ag4a18xv7bnl9ivflu1
		foreign key (WORKSPACE_ID) references MLT_WORKSPACE (id)
)
;

create index FK6q3tfcqywl3md4dskp98tn3yx
	on MLT_MRC_PASSAGE_QUESTION (UPDATER_ID)
;

create index FKbyek78rochk6xwsb2frulyp2o
	on MLT_MRC_PASSAGE_QUESTION (PASSAGE_ID)
;

create index FKgoye8uiiidb7tqfn3bjfyj82
	on MLT_MRC_PASSAGE_QUESTION (CREATOR_ID)
;

create index FKhdhya2ag4a18xv7bnl9ivflu1
	on MLT_MRC_PASSAGE_QUESTION (WORKSPACE_ID)
;

create index FK3rrh3npdopyi33tf2evu0r14d
	on MLT_MRC_PASSAGE_SKILL (CREATOR_ID)
;

create index FKjw6cgvqo9s9y7fhhr9x5hxdr6
	on MLT_MRC_PASSAGE_SKILL (UPDATER_ID)
;

create index FKtel0vqhk66s9lx5t926uqt6bi
	on MLT_MRC_PASSAGE_SKILL (WORKSPACE_ID)
;

create table MLT_MRC_TRAINING_MODEL
(
	id varchar(255) not null
		primary key,
	batchSize int null,
	createdAt datetime null,
	creatorId varchar(40) null,
	epoches int null,
	model varchar(512) null,
	mrcBinary varchar(512) null,
	name varchar(512) null,
	status varchar(40) null,
	updatedAt datetime null,
	updaterId varchar(40) null,
	workspaceId varchar(40) not null,
	constraint FKca5qcs4bfspotku3u72dgx1oj
		foreign key (creatorId) references MLT_USER (id),
	constraint FKirihrnios84g3ilfta7kv0d2k
		foreign key (updaterId) references MLT_USER (id),
	constraint FKiv5mpupl1graiimblabvux0kf
		foreign key (workspaceId) references MLT_WORKSPACE (id)
)
;

create index FKca5qcs4bfspotku3u72dgx1oj
	on MLT_MRC_TRAINING_MODEL (creatorId)
;

create index FKirihrnios84g3ilfta7kv0d2k
	on MLT_MRC_TRAINING_MODEL (updaterId)
;

create index FKiv5mpupl1graiimblabvux0kf
	on MLT_MRC_TRAINING_MODEL (workspaceId)
;

create table MLT_MRC_WIKI
(
	id int not null
		primary key,
	createdAt datetime null,
	createrId varchar(255) null,
	name varchar(40) not null,
	updateAt datetime null,
	updaterId varchar(255) null,
	workspaceId varchar(40) not null,
	constraint FKdewt1rieuixx75o6df5v6btqa
		foreign key (workspaceId) references MLT_WORKSPACE (id)
)
;

create table MLT_MRC_CONTEXT
(
	id varchar(40) not null
		primary key,
	catId varchar(40) null,
	context longtext null,
	contextMorph longtext null,
	createdAt datetime null,
	creatorId varchar(40) null,
	season int null,
	title varchar(40) null,
	wikiId int null,
	workspaceId varchar(40) not null,
	constraint FK51fs59imrsij1gqiycrghxva6
		foreign key (catId) references MLT_MRC_CATEGORY (id),
	constraint FKm8735n87xn62nkfkdvrbt5otu
		foreign key (creatorId) references MLT_USER (id),
	constraint FKetw7b2rr5fws45km61r9vojh4
		foreign key (wikiId) references MLT_MRC_WIKI (id),
	constraint FKqevwao5rgh269cthsxsxia4m9
		foreign key (workspaceId) references MLT_WORKSPACE (id)
)
;

create index FK51fs59imrsij1gqiycrghxva6
	on MLT_MRC_CONTEXT (catId)
;

create index FKetw7b2rr5fws45km61r9vojh4
	on MLT_MRC_CONTEXT (wikiId)
;

create index FKm8735n87xn62nkfkdvrbt5otu
	on MLT_MRC_CONTEXT (creatorId)
;

create index FKqevwao5rgh269cthsxsxia4m9
	on MLT_MRC_CONTEXT (workspaceId)
;

create table MLT_MRC_DATA_GROUP_REL
(
	contextId varchar(40) not null,
	dataGroupId varchar(40) not null,
	primary key (contextId, dataGroupId),
	constraint FKkjs4qpptwy9hruv14uot3fdlc
		foreign key (contextId) references MLT_MRC_CONTEXT (id),
	constraint FKhj3vpfpqnwr4lutslti45m3gd
		foreign key (dataGroupId) references MLT_MRC_DATA_GROUP (id)
)
;

create index FKhj3vpfpqnwr4lutslti45m3gd
	on MLT_MRC_DATA_GROUP_REL (dataGroupId)
;

create table MLT_MRC_QUESTION
(
	id varchar(40) not null
		primary key,
	catId varchar(40) null,
	contextId varchar(40) null,
	createdAt datetime null,
	question longtext null,
	questionMorph longtext null,
	workspaceId varchar(40) not null,
	constraint FKfgy6hx19ael5lwo8ykvg3tijw
		foreign key (catId) references MLT_MRC_CATEGORY (id),
	constraint FKfi1iofifa1j3n66m7md9htsh
		foreign key (contextId) references MLT_MRC_CONTEXT (id),
	constraint FKd8gaswo5fegyolphe8796e9a
		foreign key (workspaceId) references MLT_WORKSPACE (id)
)
;

create table MLT_MRC_ANSWER
(
	id varchar(40) not null
		primary key,
	answer longtext null,
	answerEnd int null,
	answerEndMorph int null,
	answerMorph longtext null,
	answerStart int null,
	answerStartMorph int null,
	createdAt datetime null,
	questionId varchar(40) null,
	workspaceId varchar(40) not null,
	constraint FKiktsn61o4fdjb0ti1ihewa5a6
		foreign key (questionId) references MLT_MRC_QUESTION (id),
	constraint FK7bm8j9qc3l2galvost6ko6j9p
		foreign key (workspaceId) references MLT_WORKSPACE (id)
)
;

create index FK7bm8j9qc3l2galvost6ko6j9p
	on MLT_MRC_ANSWER (workspaceId)
;

create index FKiktsn61o4fdjb0ti1ihewa5a6
	on MLT_MRC_ANSWER (questionId)
;

create index FKd8gaswo5fegyolphe8796e9a
	on MLT_MRC_QUESTION (workspaceId)
;

create index FKfgy6hx19ael5lwo8ykvg3tijw
	on MLT_MRC_QUESTION (catId)
;

create index FKfi1iofifa1j3n66m7md9htsh
	on MLT_MRC_QUESTION (contextId)
;

create index FKdewt1rieuixx75o6df5v6btqa
	on MLT_MRC_WIKI (workspaceId)
;

create table MLT_POST_PROCESS_DIC
(
	id varchar(40) not null
		primary key,
	applied varchar(512) null,
	createdAt datetime null,
	creatorId varchar(40) null,
	name varchar(40) not null,
	updatedAt datetime null,
	updaterId varchar(40) null,
	version longtext null,
	workspaceId varchar(40) not null,
	constraint FKfpkigamd7lyf5mf08ilh7c5as
		foreign key (creatorId) references MLT_USER (id),
	constraint FK643cm960mtno5aof7h9bxmdv3
		foreign key (updaterId) references MLT_USER (id),
	constraint FK5xxtvd3fji8vbovgkf65h9g9k
		foreign key (workspaceId) references MLT_WORKSPACE (id)
)
;

create index FK5xxtvd3fji8vbovgkf65h9g9k
	on MLT_POST_PROCESS_DIC (workspaceId)
;

create index FK643cm960mtno5aof7h9bxmdv3
	on MLT_POST_PROCESS_DIC (updaterId)
;

create index FKfpkigamd7lyf5mf08ilh7c5as
	on MLT_POST_PROCESS_DIC (creatorId)
;

create table MLT_POST_PROCESS_DIC_LINE
(
	id varchar(40) not null
		primary key,
	category varchar(40) null,
	createdAt datetime null,
	versionId varchar(40) not null,
	word longtext null,
	workspaceId varchar(40) not null,
	constraint FKbm8i1rr7ylv7gl2r6gl5nf9xl
		foreign key (versionId) references MLT_POST_PROCESS_DIC (id),
	constraint FKomika8iohpuay4axiras7us7p
		foreign key (workspaceId) references MLT_WORKSPACE (id)
)
;

create index FKbm8i1rr7ylv7gl2r6gl5nf9xl
	on MLT_POST_PROCESS_DIC_LINE (versionId)
;

create index FKomika8iohpuay4axiras7us7p
	on MLT_POST_PROCESS_DIC_LINE (workspaceId)
;

create table MLT_PRE_PROCESS_DIC
(
	id varchar(40) not null
		primary key,
	applied varchar(512) null,
	createdAt datetime null,
	creatorId varchar(40) null,
	name varchar(40) not null,
	updatedAt datetime null,
	updaterId varchar(40) null,
	version longtext null,
	workspaceId varchar(40) not null,
	constraint FK5pvxl7rngl45tqb9m9gwjhle1
		foreign key (creatorId) references MLT_USER (id),
	constraint FK5epbgtqavme4ajo1ure6b10is
		foreign key (updaterId) references MLT_USER (id),
	constraint FKfih9132cweu011un2tiiwo46g
		foreign key (workspaceId) references MLT_WORKSPACE (id)
)
;

create index FK5epbgtqavme4ajo1ure6b10is
	on MLT_PRE_PROCESS_DIC (updaterId)
;

create index FK5pvxl7rngl45tqb9m9gwjhle1
	on MLT_PRE_PROCESS_DIC (creatorId)
;

create index FKfih9132cweu011un2tiiwo46g
	on MLT_PRE_PROCESS_DIC (workspaceId)
;

create table MLT_PRE_PROCESS_DIC_LINE
(
	id varchar(40) not null
		primary key,
	category longtext null,
	createdAt datetime null,
	versionId varchar(40) not null,
	word longtext null,
	workspaceId varchar(40) not null,
	constraint FK1vpbevx4y6s67qfnm9nh9me8d
		foreign key (versionId) references MLT_PRE_PROCESS_DIC (id),
	constraint FKo6eouxh8x5b2nlde59ntdytob
		foreign key (workspaceId) references MLT_WORKSPACE (id)
)
;

create index FK1vpbevx4y6s67qfnm9nh9me8d
	on MLT_PRE_PROCESS_DIC_LINE (versionId)
;

create index FKo6eouxh8x5b2nlde59ntdytob
	on MLT_PRE_PROCESS_DIC_LINE (workspaceId)
;

create table MLT_QUERY_PRPRS
(
	id varchar(40) not null
		primary key,
	allStep varchar(20) null,
	createdAt datetime null,
	creatorId varchar(40) null,
	currentStep varchar(10) null,
	title varchar(40) null,
	workspaceId varchar(40) not null,
	constraint FKiw4q9tqw38serwb8c8xy5xevy
		foreign key (creatorId) references MLT_USER (id),
	constraint FKkvkum6hev0ls2litf28ntuoc2
		foreign key (workspaceId) references MLT_WORKSPACE (id)
)
;

create index FKiw4q9tqw38serwb8c8xy5xevy
	on MLT_QUERY_PRPRS (creatorId)
;

create index FKkvkum6hev0ls2litf28ntuoc2
	on MLT_QUERY_PRPRS (workspaceId)
;

create table MLT_QUERY_PRPRS_STEP
(
	id varchar(40) not null
		primary key,
	createdAt datetime null,
	creatorId varchar(40) null,
	orgSentence varchar(512) null,
	queryParaphraseId varchar(40) null,
	sentence varchar(512) not null,
	stepCd varchar(10) null,
	workspaceId varchar(40) not null,
	constraint FKorb2q5klxe5rv152ass0w7vif
		foreign key (creatorId) references MLT_USER (id),
	constraint FK1jw2oetl6v8stdkl37dm9qqu6
		foreign key (queryParaphraseId) references MLT_QUERY_PRPRS (id),
	constraint FK68pbe4kkj3y4naxhek2yf2rcx
		foreign key (workspaceId) references MLT_WORKSPACE (id)
)
;

create index FK1jw2oetl6v8stdkl37dm9qqu6
	on MLT_QUERY_PRPRS_STEP (queryParaphraseId)
;

create index FK68pbe4kkj3y4naxhek2yf2rcx
	on MLT_QUERY_PRPRS_STEP (workspaceId)
;

create index FKorb2q5klxe5rv152ass0w7vif
	on MLT_QUERY_PRPRS_STEP (creatorId)
;

create table MLT_ROLE
(
	id varchar(40) not null
		primary key,
	createdAt datetime null,
	creatorId varchar(40) null,
	defRole int null,
	roleName varchar(40) not null,
	updatedAt datetime null,
	updaterId varchar(40) null,
	workspaceId varchar(40) not null,
	constraint FKonq6ik3bdaarauyyu4fbw3t1c
		foreign key (creatorId) references MLT_USER (id),
	constraint FK5i0bxot49x6pwem2or0vy8miw
		foreign key (updaterId) references MLT_USER (id),
	constraint FKm6yrj0h5l6g1xn42voj4q32xh
		foreign key (workspaceId) references MLT_WORKSPACE (id)
)
;

create table MLT_MENU_ROLE
(
	menuId varchar(40) not null,
	roleId varchar(40) not null,
	primary key (menuId, roleId),
	constraint FKfit41sq4ls8bdbtt2m523olg6
		foreign key (menuId) references MLT_MENU (id),
	constraint FKdpyj8ltafer3xynwivpvdm8hb
		foreign key (roleId) references MLT_ROLE (id)
)
;

create index FKdpyj8ltafer3xynwivpvdm8hb
	on MLT_MENU_ROLE (roleId)
;

create index FK5i0bxot49x6pwem2or0vy8miw
	on MLT_ROLE (updaterId)
;

create index FKm6yrj0h5l6g1xn42voj4q32xh
	on MLT_ROLE (workspaceId)
;

create index FKonq6ik3bdaarauyyu4fbw3t1c
	on MLT_ROLE (creatorId)
;

create table MLT_STT_MODEL
(
	id varchar(40) not null
		primary key,
	createdAt datetime null,
	creatorId varchar(40) null,
	name varchar(512) null,
	purpose varchar(40) null,
	rate int null,
	status varchar(40) null,
	sttBinary varchar(512) null,
	sttKey varchar(255) null,
	trainType varchar(40) null,
	updatedAt datetime null,
	updaterId varchar(40) null,
	workspaceId varchar(40) not null,
	constraint FKb9hr1lt67ru963ja0jo7jmppa
		foreign key (creatorId) references MLT_USER (id),
	constraint FKbxvlb46yq9g605odhci9y8370
		foreign key (updaterId) references MLT_USER (id),
	constraint FK9p97txe6kscxjf34w5jt90pea
		foreign key (workspaceId) references MLT_WORKSPACE (id)
)
;

create index FK9p97txe6kscxjf34w5jt90pea
	on MLT_STT_MODEL (workspaceId)
;

create index FKb9hr1lt67ru963ja0jo7jmppa
	on MLT_STT_MODEL (creatorId)
;

create index FKbxvlb46yq9g605odhci9y8370
	on MLT_STT_MODEL (updaterId)
;

create table MLT_STT_TRANSCRIPT
(
	id varchar(40) not null
		primary key,
	alert varchar(200) null,
	applied varchar(200) null,
	createdAt datetime null,
	creatorId varchar(40) null,
	fileGroupId varchar(255) null,
	fileId varchar(40) not null,
	name varchar(200) null,
	qaerId varchar(50) null,
	transcriberId int null,
	updatedAt datetime null,
	updaterId varchar(40) null,
	version longtext null,
	workspaceId varchar(40) not null,
	constraint FKng15sg934lidbqivlgla6htu7
		foreign key (creatorId) references MLT_USER (id),
	constraint FK9f7mofp7d72o3y1tm20sa8iji
		foreign key (fileGroupId) references MLT_FILE_GROUP (id),
	constraint FKotjxt6jgqm64i7001mdmm4wjy
		foreign key (fileId) references MLT_FILE (id),
	constraint FKdcvvj6umt5c36p4brslbpv2nt
		foreign key (updaterId) references MLT_USER (id),
	constraint FKopdjd4mtwbi755l81jp1x7i73
		foreign key (workspaceId) references MLT_WORKSPACE (id)
)
;

create table MLT_STT_ANALYSIS_RESULT
(
	id varchar(40) not null
		primary key,
	amModelId varchar(200) null,
	createdAt datetime null,
	creatorId varchar(40) null,
	fileGroupId varchar(40) not null,
	fileId varchar(40) not null,
	lmModelId varchar(200) null,
	text longtext null,
	transcriptId varchar(40) not null,
	updatedAt datetime null,
	updaterId varchar(40) null,
	workspaceId varchar(40) not null,
	constraint FK5jmntfjejfiu9cqptyfx07fmn
		foreign key (amModelId) references MLT_STT_MODEL (id),
	constraint FKhwu58mu9b216llg5m9oh0dnbu
		foreign key (creatorId) references MLT_USER (id),
	constraint FKknb1kgaynn0b7o91jd8cf15df
		foreign key (fileGroupId) references MLT_FILE_GROUP (id),
	constraint FK9q53ub1qn2bue55ieu52eo9yy
		foreign key (fileId) references MLT_FILE (id),
	constraint FKgntlkqy1pltefe5eb9vtmnppx
		foreign key (lmModelId) references MLT_STT_MODEL (id),
	constraint FKow3903pejjmm0okvhg2nxg5wv
		foreign key (transcriptId) references MLT_STT_TRANSCRIPT (id),
	constraint FKpwrh7ewcwjtm5kkkbhsn3w5et
		foreign key (updaterId) references MLT_USER (id),
	constraint FKgvymu8hb4n8kl9s0mam3x5ukd
		foreign key (workspaceId) references MLT_WORKSPACE (id)
)
;

create index FK5jmntfjejfiu9cqptyfx07fmn
	on MLT_STT_ANALYSIS_RESULT (amModelId)
;

create index FK9q53ub1qn2bue55ieu52eo9yy
	on MLT_STT_ANALYSIS_RESULT (fileId)
;

create index FKgntlkqy1pltefe5eb9vtmnppx
	on MLT_STT_ANALYSIS_RESULT (lmModelId)
;

create index FKgvymu8hb4n8kl9s0mam3x5ukd
	on MLT_STT_ANALYSIS_RESULT (workspaceId)
;

create index FKhwu58mu9b216llg5m9oh0dnbu
	on MLT_STT_ANALYSIS_RESULT (creatorId)
;

create index FKknb1kgaynn0b7o91jd8cf15df
	on MLT_STT_ANALYSIS_RESULT (fileGroupId)
;

create index FKow3903pejjmm0okvhg2nxg5wv
	on MLT_STT_ANALYSIS_RESULT (transcriptId)
;

create index FKpwrh7ewcwjtm5kkkbhsn3w5et
	on MLT_STT_ANALYSIS_RESULT (updaterId)
;

create table MLT_STT_EVAL_RESULT
(
	id varchar(40) not null
		primary key,
	analysisResultId varchar(40) null,
	createdAt datetime null,
	creatorId varchar(40) null,
	result longtext null,
	updatedAt datetime null,
	updaterId varchar(40) null,
	workspaceId varchar(40) not null,
	constraint FKswcs9skdwgojxq4002a7amt1s
		foreign key (analysisResultId) references MLT_STT_ANALYSIS_RESULT (id),
	constraint FKltca0r8v4f41vftq9152n6ede
		foreign key (creatorId) references MLT_USER (id),
	constraint FKagmqrlun4fc2e50et42dgkgt4
		foreign key (updaterId) references MLT_USER (id)
)
;

create index FKagmqrlun4fc2e50et42dgkgt4
	on MLT_STT_EVAL_RESULT (updaterId)
;

create index FKltca0r8v4f41vftq9152n6ede
	on MLT_STT_EVAL_RESULT (creatorId)
;

create index FKswcs9skdwgojxq4002a7amt1s
	on MLT_STT_EVAL_RESULT (analysisResultId)
;

create index FK9f7mofp7d72o3y1tm20sa8iji
	on MLT_STT_TRANSCRIPT (fileGroupId)
;

create index FKdcvvj6umt5c36p4brslbpv2nt
	on MLT_STT_TRANSCRIPT (updaterId)
;

create index FKng15sg934lidbqivlgla6htu7
	on MLT_STT_TRANSCRIPT (creatorId)
;

create index FKopdjd4mtwbi755l81jp1x7i73
	on MLT_STT_TRANSCRIPT (workspaceId)
;

create index FKotjxt6jgqm64i7001mdmm4wjy
	on MLT_STT_TRANSCRIPT (fileId)
;

create table MLT_STT_TRANSCRIPT_LINE
(
	seq int not null
		primary key,
	text longtext null,
	versionId varchar(40) not null,
	constraint FKggkbl9t9xicximoktha96rl4h
		foreign key (versionId) references MLT_STT_TRANSCRIPT (id)
)
;

create index FKggkbl9t9xicximoktha96rl4h
	on MLT_STT_TRANSCRIPT_LINE (versionId)
;

create table MLT_SYNONYM_DIC
(
	id varchar(40) not null
		primary key,
	createdAt datetime null,
	creatorId varchar(40) null,
	representation varchar(40) not null,
	updatedAt datetime null,
	updaterId varchar(40) null,
	workspaceId varchar(40) not null,
	constraint FKqwkfs1lqsl8hubb59qe7nxukt
		foreign key (creatorId) references MLT_USER (id),
	constraint FKp3qjfp30pyudiee75xwiy161j
		foreign key (updaterId) references MLT_USER (id),
	constraint FK2bdbr6fuienvu6cyev817utx
		foreign key (workspaceId) references MLT_WORKSPACE (id)
)
;

create index FK2bdbr6fuienvu6cyev817utx
	on MLT_SYNONYM_DIC (workspaceId)
;

create index FKp3qjfp30pyudiee75xwiy161j
	on MLT_SYNONYM_DIC (updaterId)
;

create index FKqwkfs1lqsl8hubb59qe7nxukt
	on MLT_SYNONYM_DIC (creatorId)
;

create table MLT_SYNONYM_DIC_REL
(
	id varchar(40) not null
		primary key,
	createdAt datetime null,
	creatorId varchar(40) null,
	representationId varchar(40) null,
	synonymWord varchar(40) not null,
	updatedAt datetime null,
	updaterId varchar(40) null,
	workspaceId varchar(40) not null,
	constraint FK4nqjymbff6982abvwi8jt8v0n
		foreign key (creatorId) references MLT_USER (id),
	constraint FKla7a8ogoekl6wat2xjkkibnbh
		foreign key (representationId) references MLT_SYNONYM_DIC (id),
	constraint FKeaiaahr85vt2bh60v3cn0klsm
		foreign key (updaterId) references MLT_USER (id),
	constraint FKkri6k5g29skguhj2cyinnakjg
		foreign key (workspaceId) references MLT_WORKSPACE (id)
)
;

create index FK4nqjymbff6982abvwi8jt8v0n
	on MLT_SYNONYM_DIC_REL (creatorId)
;

create index FKeaiaahr85vt2bh60v3cn0klsm
	on MLT_SYNONYM_DIC_REL (updaterId)
;

create index FKkri6k5g29skguhj2cyinnakjg
	on MLT_SYNONYM_DIC_REL (workspaceId)
;

create index FKla7a8ogoekl6wat2xjkkibnbh
	on MLT_SYNONYM_DIC_REL (representationId)
;

create table MLT_TA_MODEL_MANAGEMENT
(
	id varchar(40) not null
		primary key,
	createdAt datetime null,
	creatorId varchar(40) null,
	dnnDicId varchar(40) null,
	dnnUse bit not null,
	externalModelSeq varchar(512) null,
	grpcServerSetId varchar(40) null,
	hmdDicId varchar(40) null,
	hmdUse bit not null,
	manDesc varchar(512) null,
	name varchar(512) not null,
	updatedAt datetime null,
	updaterId varchar(40) null,
	workspaceId varchar(40) not null,
	constraint FK49516wllfmri91nldqres1q9d
		foreign key (creatorId) references MLT_USER (id),
	constraint FK45hk7a9u8a9hr84ir7buc2n6v
		foreign key (updaterId) references MLT_USER (id),
	constraint FKf7fp3h132sbx7fhjpu5iqdsd3
		foreign key (workspaceId) references MLT_WORKSPACE (id)
)
;

create index FK45hk7a9u8a9hr84ir7buc2n6v
	on MLT_TA_MODEL_MANAGEMENT (updaterId)
;

create index FK49516wllfmri91nldqres1q9d
	on MLT_TA_MODEL_MANAGEMENT (creatorId)
;

create index FKf7fp3h132sbx7fhjpu5iqdsd3
	on MLT_TA_MODEL_MANAGEMENT (workspaceId)
;

create table MLT_USER_ROLE_REL
(
	roleId varchar(40) not null,
	userId varchar(40) not null,
	primary key (roleId, userId),
	constraint FK5r3moh6nbj4g9uk0jnd3jr34b
		foreign key (roleId) references MLT_ROLE (id),
	constraint FKiredl6j1b08h5ivbpis1jkrqh
		foreign key (userId) references MLT_USER (id)
)
;

create index FKiredl6j1b08h5ivbpis1jkrqh
	on MLT_USER_ROLE_REL (userId)
;

create table MLT_WISE_NLU_NER_CATEGORY
(
	id varchar(40) not null
		primary key,
	applied varchar(512) null,
	createdAt datetime null,
	creatorId varchar(40) null,
	name varchar(40) not null,
	updatedAt datetime null,
	updaterId varchar(40) null,
	version longtext null,
	workspaceId varchar(40) not null,
	constraint FKftetlbevt84hohh0ypx77kh7i
		foreign key (creatorId) references MLT_USER (id),
	constraint FKqbx8um6bfwxbggpejg96a2qb3
		foreign key (updaterId) references MLT_USER (id),
	constraint FKj6owosnfju9bl3qdwd6pne2dq
		foreign key (workspaceId) references MLT_WORKSPACE (id)
)
;

create index FKftetlbevt84hohh0ypx77kh7i
	on MLT_WISE_NLU_NER_CATEGORY (creatorId)
;

create index FKj6owosnfju9bl3qdwd6pne2dq
	on MLT_WISE_NLU_NER_CATEGORY (workspaceId)
;

create index FKqbx8um6bfwxbggpejg96a2qb3
	on MLT_WISE_NLU_NER_CATEGORY (updaterId)
;

create table MLT_WISE_NLU_NER_CATEGORY_LINE
(
	id varchar(40) not null
		primary key,
	createdAt datetime null,
	newCategory varchar(40) null,
	oldCategory varchar(40) null,
	versionId varchar(40) not null,
	word longtext null,
	workspaceId varchar(40) not null,
	constraint FKcsihmq161myhjbfeigq1jw5yf
		foreign key (versionId) references MLT_WISE_NLU_NER_CATEGORY (id),
	constraint FKraiaejxl0wekgbhsqn4ray5xy
		foreign key (workspaceId) references MLT_WORKSPACE (id)
)
;

create index FKcsihmq161myhjbfeigq1jw5yf
	on MLT_WISE_NLU_NER_CATEGORY_LINE (versionId)
;

create index FKraiaejxl0wekgbhsqn4ray5xy
	on MLT_WISE_NLU_NER_CATEGORY_LINE (workspaceId)
;

create table MLT_WISE_NLU_NER_DIC
(
	id varchar(40) not null
		primary key,
	applied varchar(512) null,
	createdAt datetime null,
	creatorId varchar(40) null,
	name varchar(40) not null,
	processType varchar(40) not null,
	updatedAt datetime null,
	updaterId varchar(40) null,
	version longtext null,
	workspaceId varchar(40) not null,
	constraint FKdrxgbxddbe6qftumw8sjoi5pj
		foreign key (creatorId) references MLT_USER (id),
	constraint FK28smagy1rn4k1bnr2ta2jfych
		foreign key (updaterId) references MLT_USER (id),
	constraint FKgsiiutj8puxhg47vxlebnp7gc
		foreign key (workspaceId) references MLT_WORKSPACE (id)
)
;

create index FK28smagy1rn4k1bnr2ta2jfych
	on MLT_WISE_NLU_NER_DIC (updaterId)
;

create index FKdrxgbxddbe6qftumw8sjoi5pj
	on MLT_WISE_NLU_NER_DIC (creatorId)
;

create index FKgsiiutj8puxhg47vxlebnp7gc
	on MLT_WISE_NLU_NER_DIC (workspaceId)
;

create table MLT_WISE_NLU_NER_DIC_LINE
(
	id varchar(40) not null
		primary key,
	category varchar(40) null,
	createdAt datetime null,
	rule longtext null,
	versionId varchar(40) not null,
	workspaceId varchar(40) not null,
	constraint FK3wh89kd7yj7ein3mw69ie6n0r
		foreign key (versionId) references MLT_WISE_NLU_NER_DIC (id),
	constraint FKp5dd2da6y6h4bahr9l3c8ltr1
		foreign key (workspaceId) references MLT_WORKSPACE (id)
)
;

create index FK3wh89kd7yj7ein3mw69ie6n0r
	on MLT_WISE_NLU_NER_DIC_LINE (versionId)
;

create index FKp5dd2da6y6h4bahr9l3c8ltr1
	on MLT_WISE_NLU_NER_DIC_LINE (workspaceId)
;

create index FKfe6mp6euuy188pcyy2x5kcje
	on MLT_WORKSPACE (creatorId)
;

create index FKku6bvqdqmjrycxmh5ioxlq3w
	on MLT_WORKSPACE (updaterId)
;


create table MLT_NER_CORPUS
(
  id MEDIUMINT NOT NULL AUTO_INCREMENT,
  sentence varchar(2000),
  createdAt datetime,
  primary key (id)
)
;

create table MLT_NER_CORPUS_TAG
(
	id MEDIUMINT NOT NULL AUTO_INCREMENT,
	tag varchar(200),
	word varchar(200),
	createdAt datetime,
	primary key (id)
)
;