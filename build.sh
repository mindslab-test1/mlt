#!/usr/bin/env bash

function usage() {
  echo "build.sh help : this message"
  echo "buiid.sh MAUM_ROOT: build all targets"
  echo "buiid.sh MAUM_ROOT: [all | (libmaum | stt | stttrn | ta | xdc | tatrn | mrc | mrctrn | sds | sdstrn | rest | web | paraphrase | config | qa)] : build specific target"
  echo "  libmaum: build and install libmaum"
  echo "  stt: build and install stt"
  echo "  stttrn: build and install stt train"
  echo "  ta: build and install ta"
  echo "  xdc: build and install xdc"
  echo "  tatrn: build and install ta train"
  echo "  mrc: build and install mrc"
  echo "  mrctrn: build and install mrc train"
  echo "  sds: build and install sds"
  echo "  sdstrn: build and install sds train"
  echo "  rest: build and install mlt-rest"
  echo "  web: build and install mlt-web"
  echo "  paraphrase: build and install mlt-paraphrase"
  echo "  qa: copy basic qa proto file"
  echo "build.sh clean-deploy: clean tar temp directories."
  echo "build.sh clean-cache: clean build cache directories."
  echo "build.sh clean-cmake: clean build cmake directories."
}

if [ "$#" -lt 1 ]; then
  echo "Illegal number of parameters"
  echo
  usage
  exit 1
fi

if [ "$1" = "help" ]; then
  usage
  exit 0
fi

if [ "$1" = "clean-deploy" ]; then
  echo "Remove all deploy directories."
  rm -rf ${PWD}/deploy-*
  exit 0
fi

if [ "$1" = "clean-cache" ]; then
  echo "Remove all cache build outputs."
  echo rm -rf ${HOME}/.maum-build
  rm -rf ${HOME}/.maum-build

  test -e ${MAUM_ROOT}/.brain-han-train-installed \
  && echo rm -rf ${MAUM_ROOT}/.brain-han-train-installed \
  && rm -rf ${MAUM_ROOT}/.brain-han-train-installed/

   test -d ${HOME}/.virtualenvs/venv_han \
  && echo rm -rf ${HOME}/.virtualenv/venv_han \
  && rm -rf ${HOME}/.virtualenvs/venv_han/*
  exit 0
fi

function clean_build_dir() {
  echo "Remove all build directories named as $1"
  find ./ -type d -a -name "$1*" | xargs rm -rf
}

if [ "$1" = "clean-cmake" ]; then
  clean_build_dir build-debug
  clean_build_dir build-deploy-debug
  exit 0
fi


GLOB_BUILD_DIR=${HOME}/.maum-build

OS=
if [ -f /etc/lsb-release ]; then
  OS=ubuntu
elif [ -f /etc/centos-release ]; then
  OS=centos
elif [ -f /etc/redhat-release ]; then
  OS=centos
else
  echo "Illegal OS detected. Set centos as default."
  OS=centos
fi

if [ "${OS}" = "centos" ]; then
  __CC=/usr/bin/gcc
  __CXX=/usr/bin/g++
  CMAKE=/usr/bin/cmake3
else
  __CC=/usr/bin/gcc-4.8
  __CXX=/usr/bin/g++-4.8
  CMAKE=/usr/bin/cmake
fi


IS_TAR=0
if [ "$1" = "tar" ]; then
   temp_dir=$(mktemp -d ${PWD}/deploy-XXXXXX)
   export MAUM_ROOT=${temp_dir}/maum
   echo "temp dir ${MAUM_ROOT} created"
   IS_TAR=1
   export MAUM_BUILD_DEPLOY=true
else
  export MAUM_ROOT=$1
fi

test -d ${MAUM_ROOT} || mkdir -p ${MAUM_ROOT}


if [ ! -d ${MAUM_ROOT} ]; then
  echo ${MAUM_ROOT} is not directory.
  echo Use valid directory.
  exit 1
fi

## argument parameters
shift

TARGETS=()
if [ "$#" = 0 ]; then
  TARGETS+=('libmaum')
  TARGETS+=('stt')
  TARGETS+=('stttrn')
  TARGETS+=('ta')
  TARGETS+=('xdc')
  TARGETS+=('tatrn')
  TARGETS+=('mrc')
  TARGETS+=('mrctrn')
  TARGETS+=('sds')
  TARGETS+=('sdstrn')
  TARGETS+=('rest')
  TARGETS+=('web')
  TARGETS+=('paraphrase')
  TARGETS+=('config')
  TARGETS+=('qa')
elif [ "$1" = "all" ]; then
  TARGETS+=('libmaum')
  TARGETS+=('stt')
  TARGETS+=('stttrn')
  TARGETS+=('ta')
  TARGETS+=('xdc')
  TARGETS+=('tatrn')
  TARGETS+=('mrc')
  TARGETS+=('mrctrn')
  TARGETS+=('sds')
  TARGETS+=('sdstrn')
  TARGETS+=('rest')
  TARGETS+=('web')
  TARGETS+=('paraphrase')
  TARGETS+=('config')
  TARGETS+=('qa')
elif [ "$1" = "stt-only" ]; then
  TARGETS+=('libmaum')
  TARGETS+=('stt')
  TARGETS+=('stttrn')
  TARGETS+=('mrc')
  TARGETS+=('mrctrn')
  TARGETS+=('rest')
  TARGETS+=('web')
  TARGETS+=('paraphrase')
  TARGETS+=('config')
elif [ "$1" = "ta-only" ]; then
  TARGETS+=('libmaum')
  TARGETS+=('ta')
  TARGETS+=('tatrn')
  TARGETS+=('mrc')
  TARGETS+=('mrctrn')
  TARGETS+=('rest')
  TARGETS+=('web')
  TARGETS+=('paraphrase')
  TARGETS+=('config')
elif [ "$1" = "sds-only" ]; then
  TARGETS+=('libmaum')
  TARGETS+=('sds')
  TARGETS+=('sdstrn')
  TARGETS+=('mrc')
  TARGETS+=('mrctrn')
  TARGETS+=('rest')
  TARGETS+=('web')
  TARGETS+=('paraphrase')
  TARGETS+=('config')
else
  TARGETS=("${@:1}")
fi

HAS_STT=0
for e in "${TARGETS[@]}"
do
  if [ "$e" = "stt" ] ; then
    HAS_STT=1
    break
  fi
done

HAS_TA=0
for e in "${TARGETS[@]}"
do
  if [ "$e" = "ta" ] ; then
    HAS_TA=1
    break
  fi
done

if [ ${IS_TAR} = 1 ]; then
  TARGETS+=('tar')
fi

echo "Build targets are ${TARGETS[*]}"

IMAGE_NAME=all
if [ ${HAS_STT} = 1 -a ${HAS_TA} = 0 ]; then
  IMAGE_NAME=stt
fi

if [ ${HAS_STT} = 0 -a ${HAS_TA} = 1 ]; then
  IMAGE_NAME=ta
fi

maum_root=${MAUM_ROOT}

REPO_ROOT=$(pwd)
# repo root is used in submodule build scirpt
export REPO_ROOT

APPS_ROOT=${maum_root}/apps
PROTO_ROOT=${maum_root}/include/maum/brain

function make_folder() {
  echo ${APPS_ROOT}
  test -d ${APPS_ROOT}/$1 || mkdir -p ${APPS_ROOT}/$1
}

LAST_OUT=.last_build_outdir && [[ "${IS_TAR}" == "1" ]] && LAST_OUT=.last_deploy_outdir

if [ -f ${LAST_OUT} ]; then
  last_root=$(cat ${LAST_OUT})

  if [ "${last_root}" != "${maum_root}" ]; then
    build_base="build-debug" && [[ "${IS_TAR}" = "1" ]] && build_base="build-deploy-debug"

    echo "clean last ${build_base} directories"
    clean_build_dir ${build_base}
  fi
fi
echo ${maum_root} > ${LAST_OUT}

function build_libmaum() {
  # submodule build
  (cd ${REPO_ROOT}/submodule/libmaum && CC=${__CC} CXX=${__CXX} ./build.sh)

  if [ "${HAS_STT}" = "0" ]; then
    echo "build stt proto only"
    (cd ${REPO_ROOT}/submodule/brain-stt && CC=${__CC} CXX=${__CXX} ./build.sh proto)
    (cd ${REPO_ROOT}/submodule/brain-stt-train && CC=${__CC} CXX=${__CXX} ./build.sh proto)
  fi
  if [ "${HAS_TA}" = "0" ]; then
    echo "build ta proto only"
    (cd ${REPO_ROOT}/submodule/brain-ta && CC=${__CC} CXX=${__CXX} ./build.sh proto)
    (cd ${REPO_ROOT}/submodule/brain-ta-train && CC=${__CC} CXX=${__CXX} ./build.sh proto)
  fi
}

function build_stt() {
  (cd ${REPO_ROOT}/submodule/brain-stt && CC=${__CC} CXX=${__CXX} ./build.sh)
}
function build_stt_trn() {
  (cd ${REPO_ROOT}/submodule/brain-stt-train && CC=${__CC} CXX=${__CXX} ./build.sh)
}
function build_ta() {
  (cd ${REPO_ROOT}/submodule/brain-ta && CC=${__CC} CXX=${__CXX} ./build.sh)
}
function build_xdc() {
  (cd ${REPO_ROOT}/submodule/brain-xdc && CC=${__CC} CXX=${__CXX} ./build.sh)
}
function build_ta_trn() {
  (cd ${REPO_ROOT}/submodule/brain-ta-train && CC=${__CC} CXX=${__CXX} ./build.sh)
}
function build_mrc() {
  (cd ${REPO_ROOT}/submodule/brain-mrc && CC=${__CC} CXX=${__CXX} ./build.sh)
}
function build_mrc_trn() {
  (cd ${REPO_ROOT}/submodule/brain-mrc-train && CC=${__CC} CXX=${__CXX} ./build.sh)
}
function build_sds() {
  (cd ${REPO_ROOT}/submodule/brain-sds && CC=${__CC} CXX=${__CXX} ./build.sh)
}
function build_sds_trn() {
  (cd ${REPO_ROOT}/submodule/brain-sds-train && CC=${__CC} CXX=${__CXX} ./build.sh)
}
function copy_basic_qa_proto() {
  (cp -r ${REPO_ROOT}/submodule/brain-qa/modules/basic-qa/src/main/proto/maum/brain/qa ${PROTO_ROOT})
  (cp -r ${REPO_ROOT}/submodule/brain-qa/modules/nqa/src/main/proto/maum/brain/qa/ ${PROTO_ROOT})
}



## mlt-rest
function build_rest() {
  base_dir=$(pwd)
  build_base=${REPO_ROOT}/server
  shadow_dist_base=${REPO_ROOT}/server/build
  out_dir=${MAUM_ROOT}
  cd ${build_base}
  ../gradlew :server:clean shadowJar -Pconfig=prod
  test -d ${out_dir}/lib || mkdir -p ${out_dir}/lib
  test -d ${out_dir}/conf || mkdir -p ${out_dir}/conf
  test -d ${out_dir}/bin || mkdir -p ${out_dir}/bin
  cp ${shadow_dist_base}/libs/* ${out_dir}/lib
  cp ${shadow_dist_base}/conf/* ${out_dir}/conf
  cp ${shadow_dist_base}/bin/* ${out_dir}/bin
  cd ${base_dir}
}

## mlt-web
function build_web() {
  target=client
  dist=mlt-web
  echo
  echo "Build ${target} ---------"
  echo
  sudo rm -rf ${APPS_ROOT}/${dist}
  cd ${REPO_ROOT}
  make_folder ${dist}
  #(cd ${target} && npm install && ng build -pr=true -op=${APPS_ROOT}/${dist} --prod --env=prod --aot false --sourcemap=false)
  (cd ${target} && npm install && ng build --progress=true --output-path=${APPS_ROOT}/${dist} --prod)
  if [ "${OS}" = "centos" ]; then
    sudo chcon -Rv --type=httpd_sys_content_t ${APPS_ROOT}/${dist}
  #else
    #sudo chown -R www-data.www-data ${APPS_ROOT}/${dist}
  fi
  repo=${MAUM_ROOT}/run/repositories
  test -d ${repo} || mkdir -p ${repo}
  disk=${MAUM_ROOT}/run/storage
  test -d ${disk} || mkdir -p ${disk}
  proxy=${MAUM_ROOT}/run/mlt-proxy
  test -d ${proxy} || mkdir -p ${proxy}
}

function build_paraphrase() {
  base_dir=$(pwd)
  build_base=${REPO_ROOT}/paraphrase
  shadow_dist_base=${REPO_ROOT}/paraphrase/build
  out_dir=${MAUM_ROOT}
  cd ${build_base}
  ./gradlew :clean shadowJar -Pconfig=prod
  test -d ${out_dir}/lib || mkdir -p ${out_dir}/lib
  test -d ${out_dir}/conf || mkdir -p ${out_dir}/conf
  test -d ${out_dir}/bin || mkdir -p ${out_dir}/bin
  cp ${shadow_dist_base}/libs/* ${out_dir}/lib
  cp ${shadow_dist_base}/conf/* ${out_dir}/conf
  cp ${shadow_dist_base}/bin/* ${out_dir}/bin
  cd ${base_dir}
}

## config
function build_config() {
#  test -d ${MAUM_ROOT}/etc || mkdir -p ${MAUM_ROOT}/etc
#  (cd config/nginx && rsync -ar * ${MAUM_ROOT}/etc/nginx)
#  (cd config/recipe.d && rsync -ar * ${MAUM_ROOT}/etc/recipe.d)
#  (cd config/supervisor.conf.d && rsync -ar * ${MAUM_ROOT}/etc/supervisor/conf.d)
#  cp config/maum-admin-server.conf.in ${MAUM_ROOT}/etc
#  cp config/init.sh ${MAUM_ROOT}/mlt_init.sh
#  chmod +x ${MAUM_ROOT}/mlt_init.sh
#  if [ "${OS}" = "centos" ]; then
#    sudo chcon -Rv --type=httpd_log_t ${MAUM_ROOT}/logs
#    sudo chcon -Rv --type=httpd_config_t ${MAUM_ROOT}/etc/nginx
#    sudo chcon -Rv --type=httpd_config_t ${proxy}
#  fi
echo "config"
GCC_VER=$(${__CC} -dumpversion)
  build_base="build-debug" && [[ "${IS_TAR}" = "1" ]] && build_base="build-deploy-debug"
  build_dir=${REPO_ROOT}/${build_base}-${GCC_VER}
  test -d ${build_dir} || mkdir -p ${build_dir}
  cd ${build_dir}
  ${CMAKE} \
   -DCMAKE_PREFIX_PATH=${maum_root} \
   -DCMAKE_INSTALL_PREFIX=${maum_root} \
   -DCMAKE_BUILD_TYPE=Debug \
   -DCMAKE_CXX_COMPILER:FILEPATH=${__CXX} \
   -DCMAKE_C_COMPILER:FILEPATH=${__CC} ..
   make install -j 1
   #rm -rf ${build_dir}
}

function make_tar() {
  upperdir=$(dirname ${maum_root})
  tardir=$(basename ${maum_root})

  tag=$(git describe --abbrev=7 --tags)
  tarfile=mlt-${tag}-${IMAGE_NAME}-${OS}.tar.gz

  echo "------------------------------------"
  echo "Generating TAR ${tarfile} ..."
  echo "------------------------------------"

  GZIP=-1 tar -zcf ${upperdir}/${tarfile} -C ${upperdir} ${tardir} \
    --exclude "${tardir}/workspace/stt-training/*" \
    --exclude "${tardir}/workspace/classifier/*" \
    --exclude "${tardir}/workspace/sds-train/*" \
    --exclude "${tardir}/resources/*" \
    --exclude "${tardir}/lib/*.a" \
    --exclude "${tardir}/logs/*" \
    --exclude "${tardir}/trained/stt/*" \
    --exclude "${tardir}/trained/hmd/*" \
    --exclude "${tardir}/trained/classifier/*" \
    --exclude "${tardir}/trained/sds-model/*" \
    --exclude "${tardir}/share/cmake/*" \
    --exclude "${tardir}/apps/*/node_modules/*.o" \
    --exclude "${tardir}/apps/*/node_modules/*.c" \
    --exclude "${tardir}/apps/*/node_modules/*.cpp" \
    --exclude "${tardir}/apps/*/node_modules/*.cc" \
    --exclude "${tardir}/apps/*/node_modules/*.h" \
    --exclude "${tardir}/apps/*/node_modules/*.hh" \
    --exclude "${tardir}/apps/*/node_modules/*.hpp" \
    --exclude "${tardir}/apps/*/node_modules/*.in" \
    --exclude "${tardir}/apps/*/node_modules/*.am" \
    --exclude "${tardir}/apps/*/node_modules/*.ac" \
    --exclude "${tardir}/apps/*/node_modules/*.go" \
    --exclude "${tardir}/apps/*/node_modules/*.a" \
    --exclude "${tardir}/apps/*/node_modules/*.S" \
    --exclude "${tardir}/apps/*/node_modules/*.pas" \
    --exclude "${tardir}/apps/*/node_modules/*.sln" \
    --exclude "${tardir}/apps/*/node_modules/*.vcxproj" \
    --exclude "${tardir}/apps/*/node_modules/*.vcproj" \
    --exclude "${tardir}/apps/*/node_modules/*SConscript" \
    --exclude "${tardir}/apps/*/node_modules/*configure" \
    --exclude "${tardir}/apps/*/node_modules/grpc/third_party/*" \
    --exclude "${tardir}/bin/protoc" \
    --exclude "${tardir}/bin/grpc_*_plugin" \
    --exclude "${tardir}/*built" \
    --exclude "${tardir}/*extracted" \
    --exclude "${tardir}/*installed"

  echo mlt-${tag} > ${maum_root}/etc/revision
  test -d ${REPO_ROOT}/out || mkdir -p ${REPO_ROOT}/out
  mv ${upperdir}/${tarfile} ${REPO_ROOT}/out
  echo "------------------------------------"
  echo "TAR file out/${tarfile} created."
  echo "------------------------------------"

  # make_all_res_tar
  echo "please clean TEMP DIR : ${upperdir}"
}

### BUILD MAIN
./prerequisite.sh

for t in "${TARGETS[@]}"
do
  case "$t" in
    libmaum) build_libmaum;;
    stt) build_stt;;
    stttrn) build_stt_trn;;
    ta) build_ta;;
    xdc) build_xdc;;
    tatrn) build_ta_trn;;
    ### mrc 빌드 에러 해결이 안된 상황으로 mrc 빌드 임시 주석 처리
    # mrc) build_mrc;;
    # mrctrn) build_mrc_trn;;
    sds) build_sds;;
    sdstrn) build_sds_trn;;
    web) build_web;;
    rest) build_rest;;
    paraphrase) build_paraphrase;;
    config) build_config;;
    qa) copy_basic_qa_proto;;
    tar) make_tar;;
    *) echo "INVALID TARGET $t";;
  esac
done
